import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.less';
import './styles/main.less';
import { Provider } from 'react-redux';
import reportWebVitals from './reportWebVitals';
import { persistor, store } from './helpers';
import App from './App';
import { ApolloProvider } from '@apollo/client';
import { client } from './graphql/client';
import { ConnectedRouter } from 'connected-react-router';
import { PersistGate } from 'redux-persist/integration/react';
import history from 'utils/history';
import { Spin } from 'antd';

ReactDOM.render(
  <ApolloProvider client={client}>
    <Provider store={store}>
      <PersistGate loading={<Spin spinning />} persistor={persistor}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </PersistGate>
    </Provider>
  </ApolloProvider>,
  document.getElementById('root')
);
// hmr enable
if (module.hot && process.env.NODE_ENV === 'development') {
  module.hot.accept();
}
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
