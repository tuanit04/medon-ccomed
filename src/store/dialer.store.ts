import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { getAgentListVCC, saveAgentStatus, saveCallLog, savePatientDialog } from 'api/vcc.api';
import { AppState } from 'helpers';
import { SaveCallLogParams, SaveAgentStatusParams, VCCStatus } from 'interface/user/vcc';
import { convertPhoneNumber, openNotificationWithIcon } from 'utils/helper';
import moment from 'moment';
import { getMap } from 'utils/array';

interface IDialer {
  open?: boolean;
  openDetailDialog?: boolean;
  minimize?: boolean;
  initStatus: 'success' | 'failed' | 'loading';
  customerAccepted?: boolean;
  callData: CSVoice;
  patientId?: string;
  patientName?: string;
  vccStatus?: VCCStatus;
  phone?: string;
  agentList?: CSAgent[];
  agentListRequest?: CSAgentInfo[];
  callLogParams?: SaveCallLogParams;
  dialogCallLogParams?: SaveCallLogParams;
}

const dialerSlice = createSlice({
  name: 'dialers',
  initialState: {
    // openDetailDialog: true,
    // phone: '0346628475',
    initStatus: 'loading',
    callData: {}
  } as IDialer,
  reducers: {
    onOpenDialer: state => {
      state.open = true;
    },

    onUpdateCallStatus: (state, action: PayloadAction<VCCStatus | undefined>) => {
      state.vccStatus = action.payload;
    },

    onUpdatePatientData: (state, action: PayloadAction<Pick<IDialer, 'patientId' | 'patientName'>>) => {
      state.patientId = action.payload.patientId;
      state.patientName = action.payload.patientName;
    },

    onOpenDetailDialog: (state, action: PayloadAction<boolean>) => {
      state.openDetailDialog = action.payload;
      state.dialogCallLogParams = state.callLogParams;
      state.phone = state.callData?.callInfo?.caller;
      if (state.callData?.hasCall) {
        state.minimize = action.payload;
        state.open = !action.payload;
      }
    },

    onUpdateCallLogParams: (state, action: PayloadAction<SaveCallLogParams>) => {
      state.callLogParams = action.payload.vccStatus === VCCStatus.END ? undefined : action.payload;
      if (state.openDetailDialog) {
        state.dialogCallLogParams = action.payload;
      }
    },

    onCloseDialer: state => {
      state.open = false;
      state.minimize = false;
      state.customerAccepted = false;
      state.patientId = undefined;
    },

    onMinimize: (state, action: PayloadAction<boolean>) => {
      state.open = !action.payload;
      state.minimize = action.payload;
    },

    onInitStatus: (state, action: PayloadAction<'success' | 'failed' | 'loading'>) => {
      state.initStatus = action.payload;
    },

    onUpdateCallData: (state, action: PayloadAction<CSVoice>) => {
      state.callData = JSON.parse(JSON.stringify(action.payload));
    },

    onCustomerAccept: (state, action: PayloadAction<boolean>) => {
      state.customerAccepted = action.payload;
    },

    onUpdateAgentList: (state, action: PayloadAction<{ agentList?: CSAgent[]; agentListRequest?: CSAgentInfo[] }>) => {
      const agentListState = action.payload.agentList ?? state.agentList;
      const agentListRequestState = action.payload.agentListRequest ?? state.agentListRequest;
      const agentListRequestMap = getMap(agentListRequestState, 'agentId');

      state.agentList = agentListState?.map(val =>
        val.ipPhone ? { ...val, name: agentListRequestMap?.[val.ipPhone]?.name } : val
      );
      state.agentListRequest = agentListRequestState;
    }
  }
});

const isVCCStatusValid = (status?: VCCStatus, oldStatus?: VCCStatus) => {
  if (!status || status === oldStatus) {
    return false;
  }

  switch (status) {
    case VCCStatus.STARTING:
      return oldStatus == null;
    case VCCStatus.RINGING:
      return oldStatus === VCCStatus.STARTING;
    case VCCStatus.ANSWER:
      return oldStatus === VCCStatus.RINGING;
    case VCCStatus.HOLD:
      return oldStatus === VCCStatus.ANSWER || oldStatus === VCCStatus.UNHOLD;
    case VCCStatus.UNHOLD:
      return oldStatus === VCCStatus.ANSWER || oldStatus === VCCStatus.HOLD;
    case VCCStatus.END:
      return oldStatus != null;
    default:
      break;
  }

  return true;
};

export const saveCallLogAction = (params: Pick<SaveCallLogParams, 'vccStatus' | 'patientId'>) => {
  return async (dispatch: Dispatch, getState: () => AppState) => {
    const { userObject } = getState().user;
    const { callLogParams, vccStatus, callData, patientId } = getState().dialer;

    if (
      callData?.callInfo?.callId == null ||
      !isVCCStatusValid(params.vccStatus, vccStatus) ||
      callData?.isInternalCall
    ) {
      console.error(
        'No call id or duplicate status or is internal call',
        callData?.callInfo?.callId,
        params.vccStatus,
        vccStatus
      );
      return;
    }

    const callInfo = callData?.callInfo;

    const line = convertPhoneNumber(callInfo?.line);

    if (params.vccStatus != null) {
      await dispatch(
        dialerSlice.actions.onUpdateCallStatus(params.vccStatus === VCCStatus.END ? undefined : params.vccStatus)
      );
    }

    const _callLogParams: SaveCallLogParams =
      params.vccStatus === VCCStatus.STARTING
        ? {
            ...callLogParams,
            userId: userObject?.id,
            agentId: userObject?.agentId,
            ext: userObject?.agentExt,
            domain: userObject?.domain,
            direct: callData?.isCallout ? 'OUT' : 'IN',
            phone: callData?.callInfo?.caller,
            vccCallId: callData?.callInfo?.callId,
            vccTicketId: callData?.callInfo?.ticketId,
            patientId: params.patientId ?? patientId,
            vccStatus: params.vccStatus ?? vccStatus,
            line
          }
        : {
            ...callLogParams,
            patientId: params.patientId ?? callLogParams?.patientId,
            vccStatus: params.vccStatus ?? vccStatus
          };

    const { data, status, message, code } = await saveCallLog(_callLogParams);

    if (params.vccStatus != null) {
      if (params.vccStatus === VCCStatus.STARTING) {
        await dispatch(dialerSlice.actions.onUpdateCallLogParams({ ..._callLogParams, id: data?.id }));
        await dispatch(dialerSlice.actions.onUpdatePatientData({ patientName: data?.patientName }));
      } else if (params.vccStatus === VCCStatus.END) {
        await dispatch(dialerSlice.actions.onUpdateCallLogParams(_callLogParams));
        await dispatch(dialerSlice.actions.onUpdatePatientData({ patientName: undefined }));
      } else {
        await dispatch(dialerSlice.actions.onUpdateCallLogParams(_callLogParams));
      }
    }

    if (!status) {
      openNotificationWithIcon('error', code, message);
    }
  };
};

export const saveCallDialogAction = (params: IPatientDialogForm) => {
  return async (dispatch: Dispatch, getState: () => AppState) => {
    try {
      const { callLogParams, dialogCallLogParams } = getState().dialer;

      let patientId = params.patientId;

      if (!params.patientId) {
        const { data, status, code, message } = await savePatientDialog({
          address: params.address,
          birthDate: moment(params.patientBirthDate).format('DD/MM/YYYY'),
          name: params.patientName,
          phone: params.patientPhone,
          sex: params.patientSex
        });

        if (!status) {
          throw Error(message || code);
        }

        patientId = data?.id;
      }

      const _callLogParams: SaveCallLogParams = {
        ...dialogCallLogParams,
        patientId,
        callGroupId: params.callGroupId,
        callTypeId: params.callTypeId,
        content: params.content
      };

      const { data, status, message, code } = await saveCallLog(_callLogParams);

      if (callLogParams?.vccStatus !== VCCStatus.END) {
        await dispatch(dialerSlice.actions.onUpdateCallLogParams(_callLogParams));
      }

      openNotificationWithIcon('success', 'Lưu thành công');
      await dispatch(onOpenDetailDialog(false));
    } catch (error) {
      openNotificationWithIcon('error', error.code, error.message);
    }
  };
};

export const saveAgentStatusAction = (params: Pick<SaveAgentStatusParams, 'type' | 'reasonUnready'>) => {
  return async (dispatch: Dispatch, getState: () => AppState) => {
    const { userObject } = getState().user;
    const { data, status, message, code } = await saveAgentStatus({
      agentId: userObject?.agentId,
      domain: userObject?.domain,
      ext: userObject?.agentExt,
      userId: userObject?.id,
      type: params.type,
      reasonUnready: params.reasonUnready
    });
    if (!status) {
      openNotificationWithIcon('error', code, message);
    }
  };
};

export const getAgentListAction = () => {
  return async (dispatch: Dispatch, getState: () => AppState) => {
    try {
      const { data: agentListRequest } = await getAgentListVCC();
      dispatch(dialerSlice.actions.onUpdateAgentList({ agentListRequest }));
    } catch (err) {
      console.log('err', err);
    }
  };
};

export const {
  onOpenDialer,
  onOpenDetailDialog,
  onCloseDialer,
  onMinimize,
  onUpdateCallData,
  onInitStatus,
  onCustomerAccept,
  onUpdateAgentList
} = dialerSlice.actions;

export default dialerSlice.reducer;
