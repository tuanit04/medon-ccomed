import { createSlice, PayloadAction, Dispatch } from '@reduxjs/toolkit';
import { apiVCCLogin, saveAgentStatus } from 'api/vcc.api';
import { client } from 'graphql/client';
import { message } from 'antd';
import { AppState } from 'helpers';
import { openNotificationRight } from 'utils/notification';
import { changeCallStatus, reConfigDeviceType } from 'utils/vcc/actions';
import { apiLogin, apiLogout, apiPermission } from '../api/user.api';
import { LoginParams, LoginResult, Role } from '../interface/user/login';
import { Locale, UserState } from '../interface/user/user';
import { getGlobalState } from '../utils/getGloabal';
import { csUnregister } from 'utils/vcc/sipSocket';
import { closeSocket, initSocket } from 'utils/vcc/socket';
import { openNotificationWithIcon } from 'utils/helper';
import { functionCodeConstants } from 'constants/functions';

const initialState: UserState = {
  ...getGlobalState(),
  noticeCount: 0,
  locale: (localStorage.getItem('locale')! || 'en_US') as Locale,
  newUser: JSON.parse(localStorage.getItem('newUser')!) ?? true,
  logged: localStorage.getItem('t') ? true : false,
  menuList: [],
  username: localStorage.getItem('username') || '',
  role: (localStorage.getItem('username') || '') as Role,
  loadingFunction: true,
  loading: false
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUserItem(state, action: PayloadAction<Partial<UserState>>) {
      const { username } = action.payload;

      if (username !== state.username) {
        localStorage.setItem('username', action.payload.username || '');
      }

      Object.assign(state, action.payload);
    }
  }
});

const authInitialState: IUser = {};

const authSlice = createSlice({
  name: 'auth',
  initialState: authInitialState,
  reducers: {
    setAuth: (state, action: PayloadAction<IUser>) => {
      return {
        ...action.payload
      };
    },
    setAuthValue: (state, action: PayloadAction<Partial<IUser>>) => {
      return {
        ...state,
        ...action.payload
      };
    }
  }
});

export const { setUserItem } = userSlice.actions;

export const { setAuth, setAuthValue } = authSlice.actions;

export const userReducer = userSlice.reducer;

export const authReducer = authSlice.reducer;

export const loginAsync = (payload: LoginParams) => {
  return async dispatch => {
    await dispatch(
      setUserItem({
        loading: true,
        error: undefined
      })
    );
    const { status, data, code, message } = await apiLogin(payload);
    if (status === 1 && data != null) {
      localStorage.setItem('t', data.token);
      localStorage.setItem('username', data.username);
      localStorage.setItem('user', JSON.stringify(data));
      await dispatch(setAuth(data));

      dispatch(loginVCCAsync());

      await dispatch(
        setUserItem({
          logged: true,
          username: data.username,
          error: undefined,
          name: data['name'],
          loading: false
        })
      );

      return true;
    }
    await dispatch(
      setUserItem({
        loading: false,
        error: {
          name: code,
          message
        }
      })
    );
    openNotificationRight(message);
    return false;
  };
};

export const loginVCCAsync = () => {
  return async (dispatch: Dispatch, getState: () => AppState) => {
    const { callData } = getState().dialer;
    const { domain, tokenVcc, agentId, id } = getState().auth;
    try {
      if (tokenVcc == null || domain == null) {
        throw Error('Token hoặc domain không tồn tại');
      }
      const { code } = await apiVCCLogin(tokenVcc, domain);

      if (code !== 'ok') {
        throw Error('Lỗi status code');
      }

      dispatch(setAuthValue({ isVCCLoggedIn: true }));
      if (callData?.callStatus === 'Online') {
        changeCallStatus();
      }
      await saveAgentStatus({
        agentId,
        domain,
        ext: agentId?.split('_')[1],
        type: 'UNREADY',
        userId: id,
        reasonUnready: 2
      });

      await initSocket(tokenVcc, domain);
    } catch (err) {
      console.error('Login VCC error', err);
      openNotificationWithIcon('error', 'Đăng nhập VCC thất bại', err.message);
    }
  };
};

export const logoutAsync = (str?: string) => {
  if (str) {
    message.loading(str, 100);
  } else {
    message.loading('Đang đăng xuất..', 100);
  }
  return async (dispatch, getState: () => AppState) => {
    await dispatch(logoutVCCAsync());
    const { status, code, message: msg } = await apiLogout();

    localStorage.removeItem('username');
    localStorage.removeItem('user');
    localStorage.removeItem('t');
    dispatch(setAuth({}));
    client.resetStore();
    message.destroy();
    if (status === 1) {
      message.success('Đăng xuất thành công.', 2.5);
      dispatch(
        setUserItem({
          logged: false,
          functions: undefined,
          functionObject: undefined,
          userObject: undefined
        })
      );
      return true;
    }
    dispatch(
      setUserItem({
        logged: false,
        functions: undefined,
        functionObject: undefined,
        userObject: undefined,
        error: {
          name: code,
          message: msg
        }
      })
    );
    //openNotificationRight(message);
    return false;
  };
};

export const logoutVCCAsync = () => {
  return async (dispatch: Dispatch, getState: () => AppState) => {
    const { callData } = getState().dialer;
    const { userObject } = getState().user;
    try {
      if (callData.callStatus === 'Online') {
        changeCallStatus();
      }

      closeSocket();
      csUnregister();
      if (callData.enableVoice) {
        reConfigDeviceType();
      }

      if (userObject?.domain != null) {
        await saveAgentStatus({
          agentId: userObject.agentId,
          domain: userObject.domain,
          ext: userObject.agentExt,
          type: 'UNREADY',
          userId: userObject.id,
          reasonUnready: 2
        });
      }
    } catch (err) {}
  };
};

export const permissionAsync = () => {
  return async dispatch => {
    await dispatch(
      setUserItem({
        loadingFunction: true
      })
    );
    const { status, data, code, message } = await apiPermission();
    const functionObject = data?.functions?.reduce(
      (acc, val) => ({
        ...acc,
        [val.functionCode]: val
      }),
      {}
    );
    if (status === 1 && data != null) {
      await dispatch(
        setUserItem({
          functions: data.functions ?? [],
          functionObject: functionObject ?? {},
          userObject: data.user ?? {},
          loadingFunction: false
        })
      );
      return data;
    }
    await dispatch(
      setUserItem({
        loadingFunction: false,
        error: {
          name: code,
          message
        }
      })
    );
    openNotificationRight(message);
    return false;
  };
};
