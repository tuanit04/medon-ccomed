import React, { lazy, FC } from 'react';
import Dashboard from 'pages/dashboard';
import LayoutPage from 'pages/layout';
import LoginPage from 'pages/login';
import { PartialRouteObject } from 'react-router';
import WrapperRouteComponent from './config';
import { useRoutes } from 'react-router-dom';
import MessagePage from 'pages/appointment-manager/message';
import EmailPage from 'pages/appointment-manager/email';
import AdvisoryInputPage from 'pages/advisory-manager/advisory-input';
import ExaminationPackagePage from 'pages/category/examination-package';
import ServicePage from 'pages/category/services';
import { functionCodeConstants } from 'constants/functions';
import CallPage from 'pages/call-management/call';
import UserPage from 'pages/call-management/user';
import ProfilePage from 'pages/profile';
import TicketPage from 'pages/ticket-manager/tickets';
import SLAPage from 'pages/ticket-manager/sla';
import TicketClassificationPage from 'pages/ticket-manager/ticket-type';
import GroupProcessPage from 'pages/ticket-manager/group-process';
import AppointmentCovidPage from 'pages/appointment-manager/appointment-covid';
import AppointmentCovidPageSearch from 'pages/appointment-manager/appointment-covid/AppointmentCovidPageSearch';
import AppoimentCovidCreatePage from 'pages/appointment-manager/appointment-covid/appointment-covid-page/AppoimentCovidCreatePage';
import AppointmentCovidConfirmPage from 'pages/appointment-manager/appointment-covid-confirm';
import AppoimentCovidModifiPage from 'pages/appointment-manager/appointment-covid/appointment-covid-page/AppoimentCovidModifiPage';
import AppoimentEntryPage from 'pages/appointment-manager/appoiment-entry';

// const NotFound = lazy(() => import(/* webpackChunkName: "404'"*/ 'pages/404'));
// const Documentation = lazy(() => import(/* webpackChunkName: "404'"*/ 'pages/doucumentation'));
// const AppointmentSchedule = lazy(() => import('pages/appointment-manager/appointment'));
// const ChangePasswordForm = lazy(() => import('pages/account'));
// const SiteStaffPage = lazy(() => import(/* webpackChunkName: "account'"*/ 'pages/category/site-staff'));
// const WorkSchedulePage = lazy(() => import('pages/category/work-schedule'));
// const RoutePage = lazy(() => import('pages/category/route'));
// const StreetPage = lazy(() => import('pages/category/street'));
// const AssignmentPage = lazy(() => import('pages/appointment-manager/assign'));
// const BookingRevenueDetail = lazy(() => import('pages/report/BookingRevenueDetail'));
// const PackageRevenue = lazy(() => import('pages/report/PackageRevenue'));
// const PackageRevenueDetail = lazy(() => import('pages/report/PackageRevenueDetail'));
// const AppointmentHospital = lazy(() => import('pages/report/AppointmentHospital'));
// const BookingRevenue = lazy(() => import('pages/report/BookingRevenue'));

import NotFound from 'pages/404';
import Documentation from 'pages/doucumentation';
import AppointmentSchedule from 'pages/appointment-manager/appointment';
import ChangePasswordForm from 'pages/account';
import SiteStaffPage from 'pages/category/site-staff';
import WorkSchedulePage from 'pages/category/work-schedule';
import RoutePage from 'pages/category/route';
import StreetPage from 'pages/category/street';
import AssignmentPage from 'pages/appointment-manager/assign';
import BookingRevenueDetail from 'pages/report/BookingRevenueDetail';
import PackageRevenue from 'pages/report/PackageRevenue';
import PackageRevenueDetail from 'pages/report/PackageRevenueDetail';
import AppointmentHospital from 'pages/report/AppointmentHospital';
import BookingRevenue from 'pages/report/BookingRevenue';

const routeList: PartialRouteObject[] = [
  {
    path: '/',
    element: <WrapperRouteComponent element={<LayoutPage />} titleId="" auth />,
    children: [
      {
        path: 'dashboard',
        element: <WrapperRouteComponent element={<Dashboard />} titleId="title.dashboard" auth />
      },
      {
        path: 'user/change-password',
        element: <WrapperRouteComponent element={<ChangePasswordForm />} titleId="Đổi mật khẩu" auth />
      },
      {
        path: 'documentation',
        element: <WrapperRouteComponent element={<Documentation />} titleId="title.documentation" auth />
      },
      {
        path: 'category/services',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_DM_DICHVU}
            element={<ServicePage />}
            titleId="Dịch vụ"
            auth
          />
        )
      },
      {
        path: 'category/examination-package',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_DM_GOIKHAM}
            element={<ExaminationPackagePage />}
            titleId="Gói khám"
            auth
          />
        )
      },
      {
        path: 'category/site-staff',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_DM_CBTAINHA}
            element={<SiteStaffPage />}
            titleId="Cán bộ tại nhà"
            auth
          />
        )
      },
      {
        path: 'category/work-schedule',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_DM_LLV}
            element={<WorkSchedulePage />}
            titleId="Lịch làm việc"
            auth
          />
        )
      },
      {
        path: 'category/route',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_DM_CD}
            element={<RoutePage />}
            titleId="Cung đường"
            auth
          />
        )
      },
      {
        path: 'category/street',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_DM_DP}
            element={<StreetPage />}
            titleId="Đường phố"
            auth
          />
        )
      },
      {
        path: 'ticket-management/tickets',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_QLTK_DS}
            element={<TicketPage />}
            titleId="Danh sách Ticket"
            auth
          />
        )
      },
      {
        path: 'ticket-management/sla-management',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_QLTK_QLSLA}
            element={<SLAPage />}
            titleId="Quản lý SLA"
            auth
          />
        )
      },
      {
        path: 'ticket-management/ticket-classification',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_QLTK_PLTK}
            element={<TicketClassificationPage />}
            titleId="Phân loại Ticket"
            auth
          />
        )
      },
      {
        path: 'ticket-management/group-process',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_QLTK_NHOMXL}
            element={<GroupProcessPage />}
            titleId="Quản lý nhóm xử lý"
            auth
          />
        )
      },
      {
        path: 'appointment-management/appointment',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_QLLH_LH}
            element={<AppointmentSchedule />}
            titleId="Lịch hẹn"
            auth
          />
        )
      },
      {
        path: 'appointment-management/assign-appointment',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_QLLH_PL}
            element={<AssignmentPage />}
            titleId="Phân lịch"
            auth
          />
        )
      },
      {
        path: 'appointment-management/message',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_QLLH_GTN}
            element={<MessagePage />}
            titleId="Gửi tin nhắn"
            auth
          />
        )
      },
      {
        path: 'appointment-management/email',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_QLLH_GE}
            element={<EmailPage />}
            titleId="Gửi Email"
            auth
          />
        )
      },
      {
        path: '/advisory-management/advisory-input',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_TUVAN_DAUVAO}
            element={<AdvisoryInputPage />}
            titleId="Tư vấn đầu vào"
            auth
          />
        )
      },
      {
        path: '/advisory-management/advisory-output',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_TUVAN_DAURA}
            element={<EmailPage />}
            titleId="Tư vấn đầu ra"
            auth
          />
        )
      },
      {
        path: '/advisory-management/examination-results',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_TUVAN_KETQUA}
            element={<EmailPage />}
            titleId="Kết quả khám"
            auth
          />
        )
      },
      {
        path: '/call-management/call',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_CUOCGOI_DSCG}
            element={<CallPage />}
            titleId="Danh sách cuộc gọi"
            auth
          />
        )
      },
      {
        path: '/call-management/user',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_CUOCGOI_DSCBTD}
            element={<UserPage />}
            titleId="Danh sách cán bộ tổng đài"
            auth
          />
        )
      },
      {
        path: 'report/booking-revenue',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_BAOCAO_THDTDL}
            element={<BookingRevenue />}
            titleId="Tổng hợp doanh thu đặt lịch"
            auth
          />
        )
      },
      {
        path: 'report/booking-revenue-detail',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_BAOCAO_CHITIETDTDL}
            element={<BookingRevenueDetail />}
            titleId="Chi tiết doanh thu đặt lịch"
            auth
          />
        )
      },
      {
        path: 'report/examination-package-revenue',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_BAOCAO_THDTGK}
            element={<PackageRevenue />}
            titleId="Tổng hợp doanh thu gói khám"
            auth
          />
        )
      },
      {
        path: 'report/examination-package-revenue-detail',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_BAOCAO_CTDTGK}
            element={<PackageRevenueDetail />}
            titleId="Chi tiết doanh thu gói khám"
            auth
          />
        )
      },
      {
        path: 'report/appointment-hospital',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_BAOCAO_LHTV}
            element={<AppointmentHospital />}
            titleId="Thống kê lịch hẹn tại bệnh viện/phòng khám"
            auth
          />
        )
      },
      {
        path: '*',
        element: <WrapperRouteComponent element={<NotFound />} titleId="title.notFount" />
      },
      {
        path: 'profile/list',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_KHACHHANG_DS}
            element={<ProfilePage />}
            titleId="Khách hàng"
            auth
          />
        )
      },
      {
        path: 'appointment-management/appointment-covid',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_QLLH_LH_COVID}
            element={<AppointmentCovidPage />}
            titleId="Lịch hẹn covid tại nhà"
            auth
          />
        )
      },
      {
        path: 'appointment-management/appointment-covid/create',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_LH_TLH_COVID}
            element={<AppoimentCovidCreatePage />}
            titleId="Thêm mới lịch hẹn covid"
            auth
          />
        )
      },
      {
        path: 'appointment-management/appointment-covid/update/:groupId/:id',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_LH_SUALH_COVID}
            element={<AppoimentCovidModifiPage />}
            titleId="Xác nhận lịch hẹn covid"
            auth
          />
        )
      },
      {
        path: 'appointment-management/appointment-covid/confirm',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.TD_LH_XNLH_COVID}
            element={<AppointmentCovidConfirmPage />}
            titleId="Xác nhận lịch hẹn covid cán bộ tại nhà"
            auth
          />
        )
      },
      {
        path: 'appointment-management/appointment-entry',
        element: (
          <WrapperRouteComponent
            functionCode={functionCodeConstants.COVID_NC_DS}
            element={<AppoimentEntryPage />}
            titleId="Danh sách covid nhập cảnh"
            auth
          />
        )
      }
    ]
  },
  {
    path: 'login',
    element: <WrapperRouteComponent element={<LoginPage />} titleId="Đăng nhập hệ thống" auth={false} />
  }
];

const RenderRouter: FC = () => {
  // const functionOb = useAppState(state => state.user.functionObject);
  // console.log(functionOb);
  const element = useRoutes(routeList);
  return element;
};

export default RenderRouter;
