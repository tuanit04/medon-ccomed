import React, { FC, useState } from 'react';
import { Route } from 'react-router-dom';
import { RouteProps } from 'react-router';
import PrivateRoute from './pravateRoute';
import { useIntl } from 'react-intl';
import { useAppState } from 'helpers';

export interface WrapperRouteProps extends RouteProps {
  /** document title locale id */
  titleId: string;
  /** authorization？ */
  auth?: boolean;
  functionCode?: string;
}

const WrapperRouteComponent: FC<WrapperRouteProps> = ({ titleId, auth, functionCode, ...props }) => {
  const { functionObject } = useAppState(state => state.user);
  const permission = !functionCode || Boolean(functionCode && functionObject && functionObject?.[functionCode]);
  const { formatMessage } = useIntl();
  const WitchRoute: any = auth ? PrivateRoute : Route;
  if (titleId) {
    const mydiv = document.getElementById("tet_2022")!
    const hoamai = document.getElementById("snFlkDiv9")!
    if (titleId === 'Đăng nhập hệ thống') {
      if (mydiv) {
        mydiv.style.display = '';
      }
      if (hoamai) {
        hoamai.style.display = '';
      }
    } else {
      if ( mydiv) {
        mydiv.style.display = 'none';
      }
      if (hoamai) {
        hoamai.style.display = '';
      }
    }
    document.title = formatMessage({
      id: titleId
    });
  }
  return <WitchRoute {...props} isPermission={permission} />;
};

export default WrapperRouteComponent;
