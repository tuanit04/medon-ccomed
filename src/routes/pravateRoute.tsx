import React, { FC, useEffect } from 'react';
import { Route, useNavigate } from 'react-router-dom';
import { Result, Button } from 'antd';
import { useLocale } from 'locales';
import { RouteProps, useLocation } from 'react-router';
import { store, useAppState } from '../helpers';
import { logoutAsync } from 'store/user.store';

const PrivateRoute = props => {
  const { logged, loadingFunction } = useAppState(state => state.user);
  const navigate = useNavigate();
  const { formatMessage } = useLocale();
  const location = useLocation();

  useEffect(() => {
    if (!logged) {
      navigate('/login');
    }
  });

  return logged && props.isPermission ? (
    <Route {...props} />
  ) : loadingFunction ? null : (
    <Result
      status="403"
      title="403"
      subTitle={formatMessage({ id: 'gloabal.tips.unauthorized' })}
      extra={
        <Button
          type="primary"
          onClick={() => {
            navigate(`/login`);
            store.dispatch(logoutAsync());
          }}
        >
          {formatMessage({ id: 'gloabal.tips.goToLogin' })}
        </Button>
      }
    />
  );
};

export default PrivateRoute;
