export const enUS_globalTips = {
  'gloabal.tips.notfound': 'Sorry, the page you visited does not exist.',
  'gloabal.tips.unauthorized': 'Xin lỗi, bạn không có quyền truy cập nội dung này.',
  'gloabal.tips.loginResult': 'When you see this page, it means you are logged in.',
  'gloabal.tips.goToLogin': 'Quay lại trang đăng nhập',
  'gloabal.tips.username': 'Username',
  'gloabal.tips.password': 'Password',
  'gloabal.tips.login': 'Login',
  'gloabal.tips.backHome': 'Back Home',
  'gloabal.tips.operation': 'Operation',
  'gloabal.tips.authorize': 'Authorize',
  'gloabal.tips.delete': 'Delete',
  'gloabal.tips.create': 'Create',
  'gloabal.tips.modify': 'Modify',
  'gloabal.tips.search': 'Search',
  'gloabal.tips.reset': 'Reset',
  'gloabal.tips.deleteConfirm': 'Do you Want to delete these items?'
};
