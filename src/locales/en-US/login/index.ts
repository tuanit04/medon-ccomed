export const enUS_login = {
  'app.login.username': 'Tên đăng nhập',
  'app.login.password': 'Mật khẩu',
  'app.login.buttonLogin': 'Đăng nhập',
  'app.login.requiredUsername': 'Vui lòng nhập tên đăng nhập',
  'app.login.requiredPassword': 'Vui lòng nhập mật khẩu',
  'app.login.error': 'Nhập sai tên đăng nhập hoặc mật khẩu'
};
