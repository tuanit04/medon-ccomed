export const enUS_tagsViewDropMenu = {
  'tagsView.operation.closeCurrent': 'Đóng tab hiện tại',
  'tagsView.operation.closeOther': 'Đóng các tab ngoài tab hiện tại',
  'tagsView.operation.closeAll': 'Đóng tất cả các tab',
  'tagsView.operation.dashboard': 'Màn hình chính'
};
