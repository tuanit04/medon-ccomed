export const vnVN_schedule = {
  'app.schedule.table.name': '角色名称',
  'app.schedule.role.code': '角色编码',
  'app.schedule.role.status': '状态',
  'app.schedule.role.status.all': '全部',
  'app.schedule.role.status.enabled': '启用',
  'app.schedule.role.status.disabled': '禁用',
  'app.schedule.role.nameRequired': '请输入角色名称',
  'app.schedule.role.codeRequired': '请输入角色编码',
  'app.schedule.role.statusRequired': '请选择角色启用状态'
};
