import { Divider, Modal, Select, Space, Tag, Typography } from 'antd';
import React, { useEffect, useImperativeHandle, useState } from 'react';
import { csTransferCallAgent } from 'utils/vcc/actions';

const { Text } = Typography;

export interface AgentModalRefProps {
  visible?: boolean;
  ipPhone?: string;
  setVisible: (visible?: boolean) => void;
}

interface Props {
  agentList?: CSAgent[];
}

const AgentModal = React.forwardRef<AgentModalRefProps, Props>(({ agentList }, ref) => {
  const [visible, setVisible] = useState<boolean>();
  const [ipPhone, setIpPhone] = useState<string>();
  const [loading, setLoading] = useState(true);

  useImperativeHandle(ref, () => ({
    visible,
    setVisible,
    ipPhone
  }));

  useEffect(() => {
    if (agentList?.length) {
      setLoading(false);
    }
  }, [agentList]);

  useEffect(() => {
    if (visible) {
      setLoading(true);
    } else {
      setIpPhone(undefined);
    }
  }, [visible]);

  return (
    <Modal
      visible={visible}
      title="Chọn Agent chuyển tiếp"
      onOk={() => {
        if (ipPhone) {
          csTransferCallAgent(ipPhone);
          setVisible(false);
        }
      }}
      onCancel={() => {
        setVisible(false);
      }}
    >
      <Select
        value={ipPhone}
        onChange={setIpPhone}
        filterOption={(inputValue, option) => option?.title?.toLowerCase()?.includes(inputValue.toLowerCase())}
        style={{ width: '100%', margin: '10px 0' }}
        size="large"
        placeholder="Chọn Agent chuyển tiếp"
        loading={loading}
        showSearch
        allowClear
      >
        {agentList?.map(val => (
          <Select.Option
            key={val.userId}
            value={val.ipPhone ?? ''}
            title={`${val.name ?? ''} - ${val.userName ?? ''} - ${val.ipPhone}`}
          >
            <Space>
              <Text>{val.name ?? val.userName}</Text>
              <Tag>{val.ipPhone}</Tag>
              <Tag color={val.callStatus === 'READY' ? 'green' : 'red'}>
                {val.callStatus === 'READY' ? 'Sẵn sàng' : 'Không sẵn sàng'}
              </Tag>
            </Space>
          </Select.Option>
        ))}
      </Select>
    </Modal>
  );
});

export default AgentModal;
