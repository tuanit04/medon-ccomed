import React, { forwardRef, useEffect, useImperativeHandle, useRef, useState } from 'react';
import { Button, Radio, Space, Switch, Tooltip, TooltipProps, Typography } from 'antd';
import { SaveAgentStatusParams, SaveAgentStatusResult, VCCUnreadyReasonDetail } from 'interface/user/vcc';
import useRestQuery from 'hooks/useRestQuery';
import { saveAgentStatusUrl } from 'config/api';
import { useAppState } from 'helpers';
import './index.less';
import { changeCallStatus } from 'utils/vcc/actions';

const { Text } = Typography;

interface StatusButtonProps {
  showStatus?: boolean;
}

export interface StatusButtonRef {
  visible: boolean;
  setVisible: React.Dispatch<React.SetStateAction<boolean>>;
}

const StatusButton = forwardRef<StatusButtonRef, StatusButtonProps>(({ showStatus }, ref) => {
  const { callData } = useAppState(state => state.dialer);
  const { userObject } = useAppState(state => state.user);
  const { call: saveAgentStatus } = useRestQuery<IResponse<SaveAgentStatusResult>, SaveAgentStatusParams>({
    url: saveAgentStatusUrl,
    initialCall: false,
    method: 'post'
  });
  const [reasonId, setReasonId] = useState(2);
  const tooltipRef = useRef<{ props: TooltipProps }>(null);
  const [visible, setVisible] = useState(false);

  useImperativeHandle(ref, () => ({
    visible,
    setVisible
  }));

  function onKeyDownUnready(ev: KeyboardEvent) {
    if (ev.ctrlKey && ev.key.toLowerCase() === 'm') {
      setVisible(_visible => !_visible);
    }

    if ('12345'.includes(ev.key) && tooltipRef.current?.props.visible) {
      setReasonId(Number(ev.key));
      onChangeAgentStatus(Number(ev.key));
      setVisible(false);
    }
  }

  function onKeyDownReady(ev: KeyboardEvent) {
    if (ev.ctrlKey && ev.key.toLowerCase() === 'm') {
      onCheckReady();
    }
  }

  const addEventKeydownUnready = () => {
    window.removeEventListener('keydown', onKeyDownUnready);
    window.addEventListener('keydown', onKeyDownUnready);
  };

  const addEventKeydownReady = () => {
    window.removeEventListener('keydown', onKeyDownReady);
    window.addEventListener('keydown', onKeyDownReady);
  };

  const removeEventKeydown = () => {
    window.removeEventListener('keydown', onKeyDownUnready);
    window.removeEventListener('keydown', onKeyDownReady);
  };

  const onChangeAgentStatus = (_reasonId?: number) => {
    console.log(`_reasonId`, _reasonId);
    if (userObject != null) {
      saveAgentStatus({
        agentId: userObject.agentId,
        domain: userObject.domain,
        ext: userObject.agentExt,
        type: 'UNREADY',
        userId: userObject.id,
        reasonUnready: _reasonId ?? reasonId
      });
    }
    changeCallStatus();
  };

  const readyTooltip = (
    <Space direction="vertical" className="ready-tooltip">
      <Text>Chọn lý do không sẵn sàng nghe máy:</Text>
      <Radio.Group value={reasonId} onChange={e => setReasonId(e.target.value)} className="ready-radio-group">
        {Object.keys(VCCUnreadyReasonDetail).map(val => (
          <Radio key={val} value={+val}>
            {`${val} - ${VCCUnreadyReasonDetail[val]}`}
          </Radio>
        ))}
      </Radio.Group>
      <Space className="btn-ready-confirm">
        <Button size="small" type="default" onClick={() => setVisible(false)}>
          Huỷ bỏ
        </Button>
        <Button size="small" type="primary" onClick={() => onChangeAgentStatus()}>
          Xác nhận
        </Button>
      </Space>
    </Space>
  );

  const onCheckReady = () => {
    if (callData?.callStatus !== 'Online') {
      saveAgentStatus({
        agentId: userObject?.agentId,
        domain: userObject?.domain,
        ext: userObject?.agentExt,
        type: 'READY',
        userId: userObject?.id
      });
      changeCallStatus();
    }
  };

  useEffect(() => {
    if (callData.callStatus === 'Online') {
      addEventKeydownUnready();
    } else {
      addEventKeydownReady();
    }

    return () => {
      removeEventKeydown();
    };
  }, [callData.callStatus]);

  return (
    <Tooltip title={callData.callStatus === 'Online' ? 'Sẵn sàng nghe/gọi' : 'Không sẵn sàng nghe/gọi'}>
      <Tooltip
        ref={tooltipRef}
        trigger="click"
        title={callData.callStatus === 'Online' ? readyTooltip : null}
        placement="bottom"
        color="white"
        visible={callData.callStatus === 'Online' ? visible : false}
      >
        <Space>
          <Switch
            className="status-button"
            size="small"
            onChange={onCheckReady}
            checked={callData.callStatus === 'Online'}
          />
          {showStatus && (
            <span>
              {callData.callStatus === 'Online' ? (
                <>
                  Sẵn sàng
                  <span style={{ color: 'gray' }}> (Ctrl+M)</span>
                </>
              ) : callData?.callStatus === 'No Answer' ? (
                'Không trả lời'
              ) : (
                'Không sẵn sàng'
              )}
            </span>
          )}
        </Space>
      </Tooltip>
    </Tooltip>
  );
});

export default StatusButton;
