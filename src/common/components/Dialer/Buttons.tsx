import React, { Dispatch, useEffect, useRef, useState } from 'react';
import { onCloseDialer, onMinimize, onOpenDetailDialog, saveCallLogAction } from 'store/dialer.store';
import {
  ArrowRightOutlined,
  AudioMutedOutlined,
  AudioOutlined,
  CaretRightOutlined,
  CloseOutlined,
  CopyOutlined,
  InfoCircleOutlined,
  LoginOutlined,
  LogoutOutlined,
  PauseOutlined,
  PhoneOutlined
} from '@ant-design/icons';
import { Button, message, Space, Tooltip } from 'antd';
import { VCCStatus } from 'interface/user/vcc';
import { SizeType } from 'antd/es/config-provider/SizeContext';
import AgentModal, { AgentModalRefProps } from './AgentModal';
import { saveCallAsync } from 'sagas/callSaga';
import { csCallout, endCall, getTransferAgent, holdCall, muteCall, onAcceptCall } from 'utils/vcc/actions';

interface Props {
  callData: CSVoice;
  customerAccepted?: boolean;
  minimize?: boolean;
  phoneNumber: string;
  dispatch: Dispatch<any>;
  agentList?: CSAgent[];
}

const Buttons = ({ callData, customerAccepted, minimize, phoneNumber, dispatch, agentList }: Props) => {
  const [size, setSize] = useState<SizeType>('large');
  const modalRef = useRef<AgentModalRefProps>(null);

  const onCopyPhoneNumber = () => {
    if (callData.callInfo?.caller) {
      navigator.clipboard.writeText(callData.callInfo.caller);
      message.success('Đã sao chép số điện thoại');
    }
  };

  useEffect(() => {
    if (minimize) {
      setSize('small');
    } else {
      setSize('large');
    }
  }, [minimize]);

  return (
    <>
      {callData.hasCall ? (
        <Space wrap>
          {!callData.isCallout ? (
            customerAccepted ? (
              <>
                <Tooltip title={callData.isMute ? 'Bật mic' : 'Tắt mic'}>
                  <Button
                    shape="circle"
                    icon={callData.isMute ? <AudioMutedOutlined /> : <AudioOutlined />}
                    size={size}
                    onClick={() => muteCall()}
                  />
                </Tooltip>
                <Tooltip title={callData.isHold ? 'Tiếp tục' : 'Giữ máy'}>
                  <Button
                    className={callData.isHold ? 'btn-play' : 'btn-hold'}
                    shape="circle"
                    icon={callData.isHold ? <CaretRightOutlined /> : <PauseOutlined />}
                    size={size}
                    onClick={() => holdCall()}
                  />
                </Tooltip>
                <Tooltip title="Chuyển tiếp">
                  <Button
                    shape="circle"
                    icon={<ArrowRightOutlined />}
                    size={size}
                    onClick={() => {
                      getTransferAgent();
                      modalRef.current?.setVisible(true);
                    }}
                  />
                </Tooltip>
              </>
            ) : (
              <Tooltip title="Nhận cuộc gọi">
                <Button
                  className="btn-call"
                  type="primary"
                  shape="circle"
                  icon={<PhoneOutlined />}
                  size={size}
                  onClick={() => onAcceptCall()}
                />
              </Tooltip>
            )
          ) : callData.isAnswering && customerAccepted ? (
            <>
              {/* <Tooltip title={callData.isMute ? 'Bật mic' : 'Tắt mic'}>
            <Button
              shape="circle"
              icon={callData.isMute ? <AudioMutedOutlined /> : <AudioOutlined />}
              size={size}
              onClick={() => muteCall()}
            />
          </Tooltip> */}
              <Tooltip title={callData.isHold ? 'Tiếp tục' : 'Giữ máy'}>
                <Button
                  className={callData.isHold ? 'btn-play' : 'btn-hold'}
                  shape="circle"
                  icon={callData.isHold ? <CaretRightOutlined /> : <PauseOutlined />}
                  size={size}
                  onClick={() => holdCall()}
                />
              </Tooltip>
            </>
          ) : (
            <></>
          )}
          {minimize && (
            <Tooltip title="Sao chép số điện thoại">
              <Button shape="circle" size={size} onClick={onCopyPhoneNumber}>
                <CopyOutlined />
              </Button>
            </Tooltip>
          )}
          <Tooltip title="Hủy cuộc gọi">
            <Button
              shape="circle"
              type="primary"
              danger
              icon={
                <PhoneOutlined
                  style={{
                    transform: 'rotate(-135deg) translate(0px, 2px)'
                  }}
                />
              }
              size={size}
              onClick={() => {
                dispatch(saveCallAsync({ vccStatus: VCCStatus.END }));
                endCall();
              }}
            />
          </Tooltip>
          <Tooltip title="Chi tiết">
            <Button
              type="primary"
              shape="circle"
              icon={<InfoCircleOutlined />}
              size={size}
              onClick={() => dispatch(onOpenDetailDialog(true))}
            />
          </Tooltip>
          <Tooltip title={minimize ? 'Mở hộp thoại' : 'Ẩn hộp thoại'}>
            <Button
              shape="circle"
              icon={minimize ? <LoginOutlined /> : <LogoutOutlined />}
              size={size}
              onClick={() => dispatch(onMinimize(!minimize))}
            />
          </Tooltip>
        </Space>
      ) : (
        <Space wrap>
          <Tooltip title="Gọi">
            <Button
              className="btn-call"
              type="primary"
              shape="circle"
              icon={<PhoneOutlined />}
              size={size}
              onClick={() => {
                csCallout(phoneNumber);
                onMinimize(true);
              }}
            />
          </Tooltip>
          <Tooltip title="Hủy bỏ">
            <Button
              danger
              type="primary"
              shape="circle"
              icon={<CloseOutlined />}
              size={size}
              onClick={() => dispatch(onCloseDialer())}
            />
          </Tooltip>
          {minimize && (
            <Tooltip title={'Mở hộp thoại'}>
              <Button
                shape="circle"
                icon={<LoginOutlined />}
                size={size}
                onClick={() => dispatch(onMinimize(!minimize))}
              />
            </Tooltip>
          )}
        </Space>
      )}
      <AgentModal ref={modalRef} agentList={agentList} />
    </>
  );
};

export default Buttons;
