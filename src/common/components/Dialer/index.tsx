import { CloseCircleFilled, CopyOutlined, DownCircleOutlined, UpCircleOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Button, Input, message, Modal, notification, Select, Space, Switch, Tooltip, Typography } from 'antd';
import { useAppState } from 'helpers';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import './index.less';
import Buttons from './Buttons';
import useRestQuery from 'hooks/useRestQuery';
import { getPatientByPhoneUrl } from 'config/api';
import Draggable, { DraggableBounds, DraggableData, DraggableEvent } from 'react-draggable';
import { Patient } from 'common/interfaces/patient.interface';
import { autoAnswerCall, csCallout } from 'utils/vcc/actions';

const { Text, Title } = Typography;

const NOTIFICATION_KEY = 'dialer';

const NUMERIC_DIALER = '123456789*0#';

const DRAGGABLE_BOUNDS_KEY = 'draggable-bounds';

const MAX_LENGTH = 13;

const initialBounds: DraggableBounds = JSON.parse(localStorage.getItem(DRAGGABLE_BOUNDS_KEY) || 'null') ?? {
  left: 0,
  top: 0,
  bottom: 0,
  right: 0
};

const Dialer = () => {
  const { open, minimize, customerAccepted, callData, patientName, agentList } = useAppState(state => state.dialer);
  const { userObject } = useAppState(state => state.user);
  const [time, setTime] = useState(0);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [bounds, setBounds] = useState(initialBounds);
  const draggleRef = useRef<HTMLDivElement>(null);
  const inputRef = useRef<Input>(null);

  const timeText = moment.utc(time).format('HH:mm:ss');
  const dispatch = useDispatch();
  const { call: getPatient, data: patient } = useRestQuery<IResponse<Patient>>({
    url: getPatientByPhoneUrl,
    initialCall: false,
    method: 'post'
  });
  let interval: NodeJS.Timeout;

  const onPhoneNumberChange = (value: string) => {
    setPhoneNumber(phoneNumber => (phoneNumber.length > MAX_LENGTH ? phoneNumber : phoneNumber + value));
  };

  const onStart = (event: DraggableEvent, uiData: DraggableData) => {
    const { clientWidth, clientHeight } = window?.document?.documentElement;
    const targetRect = draggleRef?.current?.getBoundingClientRect();
    if (targetRect != null) {
      const _bounds = {
        left: -targetRect?.left + uiData?.x,
        right: clientWidth - (targetRect?.right - uiData?.x),
        top: -targetRect?.top + uiData?.y,
        bottom: clientHeight - (targetRect?.bottom - uiData?.y)
      };
      setBounds(_bounds);
    }
  };

  const onChangeAutoAnswer = () => {
    autoAnswerCall(callData.webAutoAnswerCall !== 1 ? 1 : 0);
  };

  const onCopyPhoneNumber = () => {
    if (callData.callInfo?.caller) {
      navigator.clipboard.writeText(callData.callInfo.caller);
      message.success('Đã sao chép số điện thoại');
    }
  };

  useEffect(() => {
    if (customerAccepted) {
      interval = setInterval(() => {
        setTime(time => time + 1000);
      }, 1000);
    } else {
      if (!callData.isAnswering) {
        setTime(0);
      }
      clearInterval(interval);
    }

    return () => {
      clearInterval(interval);
    };
  }, [callData, customerAccepted]);

  useEffect(() => {
    if (minimize) {
      notification.open({
        key: NOTIFICATION_KEY,
        message: (
          <Space>
            {callData.isCallout ? (
              <UpCircleOutlined className="icn-outgoing" />
            ) : (
              <DownCircleOutlined className="icn-incoming" />
            )}
            <Text>{callData.isCallout ? 'Cuộc gọi đi' : 'Cuộc gọi đến'}:</Text>
            <Tooltip title="Bấm để sao chép số điện thoại">
              <Text type="success" strong style={{ cursor: 'pointer' }} onClick={onCopyPhoneNumber}>
                {callData.callInfo?.caller ?? ''}
              </Text>
            </Tooltip>
          </Space>
        ),
        description: (
          <Space>
            {/* <Avatar size={44} icon={<UserOutlined />} src={patient?.data?.avatar} /> */}
            <Space direction="vertical" size={2}>
              <Tooltip title={patientName ?? ''}>
                <Text strong ellipsis style={{ maxWidth: 350 }}>
                  {patientName || 'Chưa xác định khách hàng'}
                </Text>
              </Tooltip>
              <Space>
                <Tooltip title="Bấm để sao chép số điện thoại">
                  <Text strong className="incoming-phone" onClick={onCopyPhoneNumber}>
                    {callData.callInfo?.caller ?? ''}
                  </Text>
                </Tooltip>
                {customerAccepted ? <Text type="danger">{`- ${timeText}`}</Text> : <Text>- Đang đổ chuông</Text>}
              </Space>
            </Space>
          </Space>
        ),
        className: 'dialer-minimize',
        placement: 'bottomRight',
        duration: 0,
        closeIcon: <div></div>,
        bottom: 8,
        btn: (
          <Buttons
            phoneNumber={phoneNumber}
            callData={callData}
            customerAccepted={customerAccepted}
            minimize={minimize}
            dispatch={dispatch}
            agentList={agentList}
          />
        )
      });
    } else {
      notification.close(NOTIFICATION_KEY);
    }
  }, [minimize, open, callData, time, customerAccepted, patientName]);

  useEffect(() => {
    if (open) {
      setTimeout(() => {
        inputRef.current?.focus();
      })
    } else {
      setPhoneNumber('');
    }
  }, [open]);

  useEffect(() => {
    if (callData.hasCall && callData.callInfo?.caller) {
      let formData = new FormData();
      formData.append('phone', callData.callInfo?.caller);
      getPatient(formData);
    }
  }, [callData.hasCall]);

  return (
    <Modal
      className="dialer"
      wrapClassName="dialer-wrapper"
      visible={open}
      maskClosable={false}
      footer={null}
      width={350}
      closable={false}
      keyboard={false}
      // mask={false}
      modalRender={modal => (
        <Draggable
          bounds={bounds}
          onStart={onStart}
          enableUserSelectHack={false}
          cancel={'input, button, .incoming-phone'}
          disabled={callData.hasCall}
        >
          <div ref={draggleRef}>{modal}</div>
        </Draggable>
      )}
    >
      {callData.hasCall ? (
        <Space className="info-container" direction="vertical" align="center">
          <Title level={5}>{callData.isCallout ? 'Cuộc gọi đi' : 'Cuộc gọi đến'}</Title>
          <Text type="success" strong className="user-phone">
            {''}
          </Text>
          <Avatar size={64} icon={<UserOutlined />} src={patient?.data?.avatar} />
          <Text strong>{patientName || 'Chưa xác định khách hàng'}</Text>
          <Space size={8}>
            <Text strong className="incoming-phone" onClick={onCopyPhoneNumber}>
              {callData.callInfo?.caller}
            </Text>
            <Button shape="circle" onClick={onCopyPhoneNumber}>
              <CopyOutlined size={5} />
            </Button>
          </Space>
          {customerAccepted ? (
            <Text type="danger">{timeText}</Text>
          ) : (
            !callData.isInternalCall && <Text>Đang đổ chuông</Text>
          )}
          {!callData.hasCall && !callData.isAnswering && !callData.isRinging && <Text>Kết thúc</Text>}
        </Space>
      ) : (
        <Space direction="vertical" align="center">
          <Space direction="horizontal" align="center">
            <Title level={5} style={{ margin: 0 }}>
              Đầu số gọi đi
            </Title>
            <Select defaultValue="" disabled style={{ width: 150 }}>
              {userObject?.outgoingDomainList?.split('||').map(val => (
                <Select.Option value={val}>{val}</Select.Option>
              ))}
            </Select>
          </Space>
          <Input
            ref={inputRef}
            onChange={e => setPhoneNumber(e.target.value)}
            type="text"
            value={phoneNumber}
            maxLength={MAX_LENGTH}
            onKeyPress={e => {
              if (e.key === 'Enter' && phoneNumber) {
                csCallout(phoneNumber);
              }
            }}
            addonAfter={
              <>
                <Text className="btn-backspace" onClick={() => setPhoneNumber(phoneNumber.slice(0, -1))}>
                  ⌫
                </Text>
                {phoneNumber && (
                  <span
                    className="btn-clear anticon anticon-close-circle ant-input-clear-icon"
                    onClick={() => setPhoneNumber('')}
                  >
                    <CloseCircleFilled />
                  </span>
                )}
              </>
            }
          />
          <Space wrap style={{ width: 144 }}>
            {NUMERIC_DIALER.split('').map(val => (
              <Button key={val} shape="circle" size="large" onClick={() => onPhoneNumberChange(val)}>
                {val}
              </Button>
            ))}
          </Space>
        </Space>
      )}

      <Buttons
        phoneNumber={phoneNumber}
        callData={callData}
        customerAccepted={customerAccepted}
        minimize={minimize}
        dispatch={dispatch}
        agentList={agentList}
      />

      <Space className="auto-answer">
        <Switch
          className="status-button"
          size="small"
          onClick={onChangeAutoAnswer}
          checked={callData.webAutoAnswerCall === 1}
        />
        <span onClick={onChangeAutoAnswer}>Tự động trả lời</span>
      </Space>

      <Space className="footer-container">
        <span className="footer-title">
          {`Ext: `}
          <span className="footer-detail">{userObject?.agentExt}</span>
        </span>
        <span className="footer-title">
          {`AgentID: `}
          <span className="footer-detail">{userObject?.agentId}</span>
        </span>
      </Space>
    </Modal>
  );
};

export default Dialer;
