export interface SiteStaffInput {
  name: string;
  code: string;
  staffHisId: string;
  birthDate: Date;
  sex: number;
  avatar: string;
  address: string;
  email: string;
  phone: string;
  hisUserName: string;
  userId: string;
  facilityId: string;
  parentFacilityId: string;
  isBotMessage: number;
  status: number;
  updateDate: Date;
  createDate: Date;
  createUser: string;
  updateUser: string;
  createUserId: string;
  updateUserId: string;
  id: string;
}
