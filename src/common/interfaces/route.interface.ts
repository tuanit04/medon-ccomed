export interface Route {
  code: string;
  createDate: Date;
  createUser: string;
  createUserId: string;
  description: string;
  facilityId: string;
  parentFacilityId: string;
  facilityName: string;
  parentFacilityName: string;
  id: string;
  name: string;
  status: number;
  updateDate: Date;
  updateUser: string;
  updateUserId: string;
}
