export interface SLA {
  level: string;
  value: string;
}
