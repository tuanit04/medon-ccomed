export interface VAppointmentWorkTimeStaff {
    staffId: string;
    staffCode: string;
    staffName: string;
    status: number
    appointmentDate: Date;
    amount: number;
    workTimeId: string;
    workTimeName: string;
    workTimeStaffList: [VAppointmentWorkTimeStaff]
    }