import { HealthDeclaration } from "./response/healthDeclaration";

export interface AppoimentCovidConfrim {
    preDoctorHisId?: string;
    specialistId?: string;
    relationType?: string;
    notificationTime?: string;
    nationality?: string;
    countryName?: string;
    appointmentObject?: string;
    id?: string;
    groupId?: string;
    patientName?: string;
    appointmentDate?: string | moment.Moment;
    patientBirthDate?: string | moment.Moment;
    patientSex?: string;
    phone?: string;
    patientIdNo?: string
    address?: string;
    provinceId?: string;
    districtId?: string;
    provinceCode?: string;
    districtCode?: string;
    reasonNote?: string;
    appointmentNote?: string;
    guardianPhone?: string;
    guardianName?: string;
    issuedDate?: string | moment.Moment;
    issuedPlace?: string;
    examType?: string;
    workTimeId?: string;
    workTimeName?: string;
    status?: string;
    patientLabel?: string;
    createDate?: string;
    createUser?: string;
    updateDate?: string;
    updateUser?: string;
    appointmentCode?: string;
    assignUser?: string;
    healthDeclaration?:Partial<HealthDeclaration>;
    guardianDate?:string | moment.Moment;
    examFacilityId?:string;
    appointmentHisId?:string;
    confirm?:any;
}