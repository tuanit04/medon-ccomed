import { AppointmentActionView } from '../appointmentActionView.interface';

export interface AppointmentActionListResponse {
  status: number;
  code: string;
  message: string;
  data?: AppointmentActionView[];
  page: number;
  records: number;
  pages: number;
}
