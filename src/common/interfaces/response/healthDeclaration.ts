export interface HealthDeclaration {
    id?: any
    typeCustomerKBYT: any
    dateOnset?: any | Date
    nationPassOther: any
    prognostic: any
    purpose?: any,
    location: any
    streetHasCovid: any
    humanHasCovid: any
    testedCovid: any
    invoice?: any
    addressProfile?: any
    nameCompany?: any
    emailProfile?: any
    taxCode?: any
    addressCompany?: any
    printName?: any
    typeInvoice?: any
    customerLabel?: any
    emailCompany?: any,
    hasCovid?: any,
    dateHascovid?: any | Date,
    hasVaccine?: any;
}