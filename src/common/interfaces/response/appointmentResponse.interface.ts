import { Appointment } from '../appointment.interface';

export interface AppointmentResponse {
  status: number;
  message: string;
  code: string;
  data: Partial<Appointment>;
}
