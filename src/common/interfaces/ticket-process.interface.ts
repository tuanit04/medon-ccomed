export interface TicketProcess {
  ticketId: String;
  opinionType: String;
  contactMethod: String;
  explaination: String;
  processContent: String;
  customerFeedback: String;
  importanceLevel: String;
  costType: String;
  processCost: number;
}
