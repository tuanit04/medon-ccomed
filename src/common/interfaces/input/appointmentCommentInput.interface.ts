export interface AppointmentCommentInput {
    id?: String
    userId: String
    content: String
}