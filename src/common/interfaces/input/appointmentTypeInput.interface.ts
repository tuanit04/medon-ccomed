export interface AppointmentTypeInput {
    id: string,
    name: string,
    status: number,
    createDate: string
}