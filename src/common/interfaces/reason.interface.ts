export interface Reason {
    id: string;
    name: string;
    code: string;
    status: number;
}