export interface Ward {
  code: string;
  createDate: Date;
  createUser: string;
  createUserId: string;
  districtId: string;
  id: string;
  name: string;
  provinceId: string;
  status: number;
  updateDate: Date;
  updateUserId: string;
}
