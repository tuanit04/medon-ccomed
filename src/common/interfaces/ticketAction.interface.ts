export interface TicketAction {
  status: number;
  createUser: String;
  createDate: string | moment.Moment;
  createUserId: string;
  updateUser: string;
  updateDate: string | moment.Moment;
  updateUserId: string;
  id: string;
  ticketId: string;
  actionType: string;
  objectId: string;
  note: string;
}
