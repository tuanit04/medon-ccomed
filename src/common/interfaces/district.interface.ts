import { Province } from './province.interface';

export interface District {
  code: string;
  createDate: Date;
  createUser: string;
  createUserId: string;
  id: string;
  name: string;
  province: Province;
  provinceId: string;
  status: number;
  updateDate: Date;
  updateUser: string;
  updateUserId: string;
}
