export interface Services {
  code: string;
  name: string;
  description: string;
  serviceHisId: string;
  serviceTypeId: string;
  generalMeaning: string;
  detailMeaning: string;
  unit: string;
  maxValue: string;
  minValue: string;
  parentId: string;
  approveDate: Date;
  approveUser: string;
  id: string;
  status: number;
  createDate: Date;
  createUser: string;
  createUserId: string;
  updateDate: Date;
  updateUser: string;
  updateUserId: string;
  isDisplay: number;
  isUsed: number;
  isProfile: number;
  returnDuration: string;
}
