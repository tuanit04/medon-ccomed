export interface PackageView {
  packageId: string;
  packageHisId: string;
  packageName: string;
  packageImage: string;
  originalPrice: number;
  discount: number;
}
