import { District } from './district.interface';
import { FacilityType } from './facilityType.interface';
import { Province } from './province.interface';

export interface Facility {
  address: string;
  createDate: Date;
  createUser: string;
  createUserId: string;
  description: string;
  district: District;
  districtId: string;
  facilityCode: string;
  facilityHisId: string;
  facilityType: FacilityType;
  facilityTypeId: string;
  id: string;
  code: string;
  image: string;
  name: string;
  parent: Facility;
  parentId: string;
  province: Province;
  provinceId: string;
  status: number;
  updateDate: Date;
  updateUser: string;
  updateUserId: string;
  wardId: string;
  provinceIds:string;
}
