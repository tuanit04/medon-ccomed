import { SLA } from './sla.interface';

export interface TicketType {
  status: number;
  createUser: string;
  createDate: string;
  createUserId: string;
  updateUser: string;
  updateDate: string;
  updateUserId: string;
  id: string;
  name: string;
  code: string;
  contactMethod: string;
  sla: SLA;
}
