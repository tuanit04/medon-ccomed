export interface ObjectOutput {
  workTimeId: string;
  workTimeName: string;
  status: number;
  count: number;
}
