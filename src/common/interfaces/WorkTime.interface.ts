import { ObjectOutput } from './objectInputWorkTime.interface';

export interface WorkTime {
  name: string;
  id: string;
  objectCountList: [ObjectOutput];
  total: number;
  daThucHien: number;
  chuaThucHien: number;
}
