export interface Channel {
  code: string;
  name: string;
  id: string;
  status: number;
}
