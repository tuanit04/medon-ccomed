export interface Country {
  countryCode: string,
  countryName: string,
  status: number,
  createDate: Date,
  createUser: string,
  id: string
}
