import { PackageParam } from './packageParam.interface';
import { PriceHistory } from './priceHistory.interface';
import { ServiceView } from './service.interface';

export interface Packages {
  id: string;
  name: string;
  sex: string;
  code: string;
  description: string;
  startDate: string;
  endDate: string;
  minAge: string;
  maxAge: string;
  maxNumBook: number;
  useQuantity: number;
  bookQuantity: number;
  preBookHour: number;
  packageHisId: string;
  packageTypeId: string;
  image: string;
  approveDate: string;
  approveUser: string;
  status: number;
  createDate: Date;
  genCode: string;
  createUser: string;
  createUserId: string;
  updateDate: Date;
  updateUser: string;
  updateUserId: string;
  paramList: [PackageParam];
  priceHistories: [PriceHistory];
  serviceViews: [ServiceView];
  methodType:string;
  display:string;
}
