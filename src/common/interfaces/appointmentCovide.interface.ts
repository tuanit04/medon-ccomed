import { PatienCovid } from "./patientCovid.interface";

export interface AppointmentCovid {
    appointmentDate:moment.Moment | string;
    packageType:string;
    workTimeId:string;
    workTimeName:string;
    examType:string;
    samplingAddress:string;
    covidServiceId:string;
    samplingProvinceId:string;
    samplingDistrictId:string;
    samplingStreetId:string;
    samplingRouteId:string;
    examFacilityId:string;
    patients:PatienCovid[];
    channel:string;
    groupId:string;
}