export interface FacilityType {
  createDate: Date;
  createUser: string;
  createUserId: string;
  id: string;
  name: string;
  status: number;
  updateDate: Date;
  updateUser: string;
  updateUserId: string;
}
