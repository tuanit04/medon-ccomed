export interface AppointmentActionView {
  id: string;
  appointmentId: string;
  appointmentHisId: string;
  actionName: string;
  actionUserId: string;
  actionUser: string;
  actionTime: string;
  actionNote: string;
}
