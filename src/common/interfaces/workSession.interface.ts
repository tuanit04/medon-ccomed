export interface WorkSession {
  name: string;
  type: string;
  id: string;
  status: number;
  createDate: string;
  createUser: string;
  createUserId: string;
  updateDate: string;
  updateUser: string;
  updateUserId: string;
}
