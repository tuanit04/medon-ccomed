export interface PackageParam {
  packageId: string;
  paramValue: string;
  paramType: string;
}
