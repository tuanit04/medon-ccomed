export interface ServiceView {
  description: string;
  serviceId: string;
  serviceCode: string;
  serviceName: string;
  originPrice: string;
  homeSalePrice: string;
  hosSalePrice: string;
  isDisplay: number;
  isUsed: number;
  isProfile: number;
  status: number;
  historyStatus: number;
  serviceTypeId: string;
  serviceType: string;
  facilityId: string;
  facilityName: string;
  contractCode: string;
  provinceId: string;
  generalMeaning: string;
  detailMeaning: string;
  createDate: Date;
  returnDuration: string;
}
