export interface AppointmentDetailInput {
    id: string;
    appointmentId: string;
    objectType: string;
    objectId: string;
    originalPrice: number;
    salePrice: number;
    note: string;
    genPackCode: string;
    parentId: string;
    result: string;
    resultNote: string;
    indicatedDoctorId: string;
    content: string;
}