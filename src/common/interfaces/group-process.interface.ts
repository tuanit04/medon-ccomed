export interface GroupProcess {
  status: number;
  createUser: string;
  createDate: string;
  createUserId: string;
  updateUser: string;
  updateDate: string;
  updateUserId: string;
  id: string;
  groupSize: number;
  name: string;
  code: string;
  detail: any;
}
