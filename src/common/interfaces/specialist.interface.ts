export interface Specialist {
  code: string;
  name: string;
  specialistHisId: string;
  parentId: string;
  id: string;
  status: number;
  createDate: Date;
  createUser: string;
  createUserId: string;
  updateDate: Date;
  updateUser: string;
  updateUserId: string;
}
