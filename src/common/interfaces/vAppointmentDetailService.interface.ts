export interface VAppointmentDetailService {
  id: string;
  objectType: number;
  originalPrice: number;
  salePrice: number;
  indicatedDoctorName: string;
  serviceId: string;
  serviceCode: string;
  serviceName: string;
}
