export interface AppointmentCommentInput {
    id: string;
    appointmentId: string;
    content: string;
}