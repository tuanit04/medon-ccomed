export interface FilteredInput {
  id: string;
  value: string;
  operation: string;
}

export interface FilteredInputNumber {
  id: string;
  value: string;
  operation: string;
}
