export interface Param {
  paramType: string;
  paramKey: string;
  paramValue: string;
}
