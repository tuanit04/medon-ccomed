export interface AppointmentStaff {
  appointmentId: string;
  appointmentType: string;
  appointmentHisId: string;
  appointmentAddress: string;
  appointmentNote: string;
  workTimeId: string;
  workTimeName: string;
  siteStaffId: string;
  siteStaffName: string;
  routeName: string;
  isCheck: number;
  sampleTakenDate: string;
  appointmentDate: string;
  facilityName: string;
  facilityId: string;
  parentFacilityName: string;
  parentFacilityId: string;
  groupId: string;
  classify: string;
  routeId: string;
  siteStaffPhone:string;
  patientPhone:string;
  patientName:string;
}
