export interface StaffWorkSession {
  objectId: string;
  scheduleId: string;
  objectName: string;
  workSessionId: string;
  workSessionName: string;
}
