export interface Business {
  createDate: Date;
  createUser: string;
  createUserId: string;
  description: string;
  id: string;
  name: string;
  status: number;
  type: number;
  updateDate: Date;
  updateUser: string;
  updateUserId: string;
}
