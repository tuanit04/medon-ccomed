import { Business } from "./business.interface";
import { Route } from "./route.interface";
import { WorkSession } from "./workSession.interface";
import { WorkTimes } from "./workTimes.interface";

export interface Schedule {
    business: Business
    businessId: string
    createDate: Date
    createUser: string
    createUserId: string
    id: string
    note: string
    object: [key: string]
    objectId: string
    objectType: number
    route: Route
    routeId: string
    scheduleDate: Date
    status: number
    updateDate: Date
    updateUser: string
    updateUserId: string
    workSession: WorkSession
    workSessionId: string
    workTime: WorkTimes
    workTimeId: string
}
