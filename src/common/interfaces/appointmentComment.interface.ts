export interface AppointmentComment {
  id: string;
  userId: string;
  content: string;
  status: number;
  updateDate: string;
  createDate: string;
  createUser: string;
  updateUser: string;
  createUserId: string;
  updateUserId: string;
}
