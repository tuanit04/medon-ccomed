import { Facility } from './facility.interface';

export interface SiteStaff {
  address: string;
  avatar: string;
  birthDate: Date;
  code: string;
  createDate: Date;
  createUser: string;
  createUserId: string;
  email: string;
  facility: Facility;
  facilityId: string;
  facilityParent: Facility;
  parentFacilityName;
  facilityName;
  hisUserName: string;
  id: string;
  scheduleId: string;
  isBotMessage: any;
  name: string;
  parentFacilityId: string;
  phone: string;
  sex: number;
  staffHisId: string;
  status: number;
  updateDate: Date;
  updateUser: string;
  updateUserId: string;
  userId: string;
  backgroundColor: string;
}
