import { Route } from '../route.interface';

export interface PagingRoute {
  code: string;
  data: Route[];
  message: string;
  page: number;
  pages: number;
  records: number;
}
