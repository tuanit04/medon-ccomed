import { Street } from '../street.interface';

export interface PagingStreet {
  code: string;
  data: Street[];
  message: string;
  page: number;
  pages: number;
  records: number;
}
