import { WorkSession } from './workSession.interface';
import { WorkTimeType } from './workTimeType.interface';

export interface WorkTimes {
  createDate: Date;
  createUserId: string;
  endTime: string;
  id: string;
  name: string;
  startTime: string;
  status: number;
  updateDate: Date;
  updateUser: string;
  updateUserId: string;
  workSession: WorkSession;
  workSessionId: string;
  workTimeType: WorkTimeType;
  workTimeTypeId: string;
}
