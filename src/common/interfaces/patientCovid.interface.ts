import { HealthDeclaration } from "./response/healthDeclaration";

export interface PatienCovid {
    patientName: any;
    preDoctorHisId: any;
    patientBirthDate: string | moment.Moment;
    patientSex: any;
    address: any;
    phone: any;
    provinceId: any;
    districtId: any;
    provinceCode: any;
    districtCode: any;
    patientIdNo: any;
    issuedDate: any;
    issuedPlace: any;
    id?: any;
    nationality: any;
    countryName?:any;
    reasonNote:any;
    appointmentObject:any;
    guardianName:any;
    guardianPhone:any;
    healthDeclaration:HealthDeclaration;
    confirm?:any;
    status:number;
    appointmentHisId?:any;
    patientLabel?:any;
    patientBirtYear?:any;
  }
