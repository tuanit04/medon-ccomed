export interface TicketAction {
    ticketId: string;
    actionType: string,
    objectId : string,
    note:string
}