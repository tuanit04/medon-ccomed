export interface VAppointmentOfStaff {
    staffId: string
    staffCode: string
    staffName: string
    workSessionName: string
    routeName: string
    status: number
    amount: number
    vAppointmentOfStaffList: [VAppointmentOfStaff]
    }