import { District } from './district.interface';
import { Province } from './province.interface';
import { Route } from './route.interface';

export interface Street {
  code: string;
  createDate: Date;
  createUser: string;
  createUserId: string;
  districtName: string;
  districtId: string;
  id: string;
  name: string;
  provinceName: string;
  provinceId: string;
  routeName: string;
  route: Route;
  facilityId: string;
  parentFacilityId: string;
  routeId: string;
  status: number;
  updateDate: Date;
  updateUser: string;
  updateUserId: string;
  wardId: string;
}
