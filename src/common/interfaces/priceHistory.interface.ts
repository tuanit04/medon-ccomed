export interface PriceHistory {
  objectType: string;
  objectId: string;
  facilityId: string;
  packageId: string;
  originPrice: string;
  homeSalePrice: string;
  hosSalePrice: string;
  contractCode: string;
  fromDate: string;
  toDate: string;
  status: string;
}
