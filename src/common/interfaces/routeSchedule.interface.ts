import { StaffWorkSession } from './staffWorkSession.interface';
import { WorkSession } from './workSession.interface';

export interface RouteSchedule {
  routeId: string;
  facilityName: string;
  routeName: string;
  scheduleDate: string;
  siteStaffs: [StaffWorkSession];
  workSessions: [WorkSession];
}
