import { cache } from './cache';
import { setContext } from '@apollo/client/link/context';
import { createHttpLink } from 'apollo-link-http';
import { ApolloClient, ApolloLink, fromPromise, Observable } from '@apollo/client';
import { MultiAPILink } from '@habx/apollo-multi-endpoint-link';
import { onError } from 'apollo-link-error';
import { store } from 'helpers';
import { logoutAsync } from 'store/user.store';
import { masterEndpoint, appointmentEndpoint, accountEndpoint } from 'config/api';
import { getLocalAccessToken } from 'api/token.service';
import { refreshToken } from 'api/request';

let isRefreshing = false;
let pendingRequests: Function[] = [];

const resolvePendingRequests = () => {
  pendingRequests.map(callback => callback());
  console.log('pendingRequests', pendingRequests);
  pendingRequests = [];
};

const getHeader = () => {
  return {
    'Accept-Language': 'vi',
    token: getLocalAccessToken()
  };
};

const authLink = setContext((request, { headers }) => {
  return {
    headers: {
      ...headers,
      'Accept-Language': 'vi',
      token: getLocalAccessToken()
    }
  };
});

const errorLink = onError(({ forward }) => {
  console.log(`forward`, forward);
}) as any;

const link = new MultiAPILink({
  endpoints: {
    masterEndpoint,
    appointmentEndpoint,
    accountEndpoint
  },
  createHttpLink: () => createHttpLink() as any
});

const activityMiddleware = new ApolloLink((operation, forward) => {
  return forward(operation)?.map(res => {
    const data = res.data?.[operation.operationName] ?? res.data?.response;
    if (
      data?.code === 'TOKEN_NOT_VALID' ||
      data?.code === 'TOKEN_NOT_EXIST_IN_REDIS' ||
      data?.code === 'REFRESH_TOKEN_EXPIRED' ||
      data?.code === 'TOKEN_VALID_EXCEPTION'
    ) {
      store.dispatch(logoutAsync('Token không hợp lệ. Vui lòng đăng nhập lại.'));
    } else if (data?.status === 0 && data?.code === 'TOKEN_EXPIRED') {
      let forward$: Observable<any>;
      if (!isRefreshing) {
        isRefreshing = true;
        forward$ = fromPromise(
          refreshToken()
            .then(token => {
              const headers = operation.getContext().headers;
              operation.setContext({
                headers: {
                  ...headers,
                  ...getHeader()
                }
              });
              if (!token) {
                throw Error('Refresh Token Failed');
              }
              resolvePendingRequests();
              return token;
            })
            .catch(err => {
              pendingRequests = [];
              return null;
            })
            .finally(() => {
              isRefreshing = false;
            })
        ).filter(value => Boolean(value));
      } else {
        forward$ = fromPromise(
          new Promise(resolve => {
            pendingRequests.push(() => resolve());
          })
        );
      }
    }
    return res;
  });
});

export const client = new ApolloClient({
  cache,
  link: authLink.concat(ApolloLink.from([activityMiddleware, link, errorLink]))
});
