import { updateLocalAccessToken } from 'api/token.service';
import { apiRefreshToken } from 'api/user.api';
import { store } from 'helpers';
import { RefreshTokenResult } from 'interface/user/login';
import { call, put, takeLeading } from 'redux-saga/effects';
import { logoutAsync } from 'store/user.store';

export const LOGOUT = 'logout/LOGOUT';
export const REFRESH_TOKEN = 'logout/REFRESH_TOKEN';

function* doLogout(action) {
  yield call(store.dispatch, logoutAsync(action));
}

function* doRefreshToken() {
  const { data } = yield call(apiRefreshToken);
  if (data?.code === 'REFRESH_TOKEN_EXPIRED' || data?.code === 'TOKEN_VALID_EXCEPTION') {
    yield put({
      type: LOGOUT,
      payload: {
        message: 'RefreshToken/Token hết hạn, vui lòng đăng nhập lại.'
      }
    });
  } else {
    const token: RefreshTokenResult = data?.data;
    updateLocalAccessToken(token?.token, token?.tokenVcc);
  }
}

export function* watchLogout() {
  yield takeLeading(LOGOUT, doLogout);
}

export function* watchRefreshToken() {
  yield takeLeading(REFRESH_TOKEN, doRefreshToken);
}
