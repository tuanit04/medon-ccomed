import { all } from 'redux-saga/effects';
import 'regenerator-runtime/runtime';
import { watchCallQueueRequests } from './callSaga';

// Root sagas
// Single entry point to start all sagas at once
export default function* rootSaga() {
  yield all([watchCallQueueRequests()]);
}
