import { store } from 'helpers';
import { take, actionChannel, call } from 'redux-saga/effects';
import { saveCallLogAction } from 'store/dialer.store';

const SAVE_CALL_LOG_REQUEST = 'dialers/SAVE_CALL_LOG_REQUEST';

export function* watchCallQueueRequests() {
  // 1- Create a channel for request actions
  const requestChannel = yield actionChannel(SAVE_CALL_LOG_REQUEST);
  while (true) {
    // 2- take from the channel
    const { payload } = yield take(requestChannel);
    // 3- Note that we're using a blocking call
    yield call(store.dispatch, saveCallLogAction(payload));
  }
}

export const saveCallAsync = (payload: Parameters<typeof saveCallLogAction>[0]) => ({
  type: SAVE_CALL_LOG_REQUEST,
  payload
});
