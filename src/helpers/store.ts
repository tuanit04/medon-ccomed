import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../reducers';
import mySaga from '../sagas';
import { useDispatch, useSelector } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { routerMiddleware as createRouterMiddleware } from 'connected-react-router';
import history from 'utils/history';
import { persistStore, persistReducer, PersistConfig } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { createLogger } from 'redux-logger';

const persistConfig: PersistConfig<any> = {
  key: 'root',
  storage,
  whitelist: ['auth']
};

const persistedReducer = persistReducer<AppState, any>(persistConfig, rootReducer);

const composeEnhancers =
  typeof window === 'object' &&
  process.env.NODE_ENV === 'development' &&
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

// Define middlewares to include
const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger({ collapsed: true });
const routerMiddleware = createRouterMiddleware(history);
// Add all middlewares into an array
const middleware = [sagaMiddleware, thunkMiddleware, routerMiddleware];

if (process.env.NODE_ENV === 'development') {
  middleware.push(loggerMiddleware);
}

// Add the Redux dev tools and middleware code together
const enhancers = composeEnhancers(
  applyMiddleware(...middleware)
  // window.devToolsExtension ? window.devToolsExtension() : f => f
);

// Create a store with the reducers and middleware
export const store = createStore(persistedReducer, enhancers);

export const persistor = persistStore(store);

// Run the Redux Saga middleware listeners
sagaMiddleware.run(mySaga);

export type AppState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;

export const useAppDispatch = () => useDispatch<AppDispatch>();

export const useAppState = <T extends (state: AppState) => any>(selector: T): ReturnType<T> => useSelector(selector);
