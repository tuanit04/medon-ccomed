import React from 'react';
import Loading from 'react-fullscreen-loading';

export default function MyLoader({ active }) {
  return <Loading loading={active} loaderColor="#3498db" />;
}
