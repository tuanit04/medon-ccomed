export interface IErrorHandlerResult<T> {
  readonly result?: T;
  readonly error?: Error;
}

export function delay<T>(runner: () => Promise<T>, delayTime: number): Promise<T> {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      runner()
        .then(resolve)
        .catch(reject);
    }, delayTime);
  });
}

export default class RetryAble<T> {
  private retryIndex: number = 0;
  private maxRetry = 5;
  constructor(
    private readonly runner: () => Promise<T>,
    private readonly errorHandler?: (error: any, index: number, final: boolean) => IErrorHandlerResult<T>,
    private readonly delayTime?: number
  ) {}

  defaultErrorHandler: (error: any, index: number, final: boolean) => IErrorHandlerResult<T> = (
    error: any,
    index: number,
    final: boolean
  ) => {
    if (final || !(error.code === 'INTERNAL_SERVER_ERROR' || error.name !== 'TimeoutError')) {
      return {
        error
      };
    }
    return {};
  };

  run: () => Promise<T> = async () => {
    try {
      const result: T = await this.runner();
      return result;
    } catch (error) {
      const isFinal = this.retryIndex >= this.maxRetry;
      const handlerResult: IErrorHandlerResult<T> = (this.errorHandler || this.defaultErrorHandler)(
        error,
        this.retryIndex,
        isFinal
      );
      if (handlerResult.error == null && handlerResult.result == null) {
        this.retryIndex++; // eslint-disable-line
        return delay<T>(this.run, (this.delayTime ?? 500) * this.retryIndex);
      } else if (handlerResult.result == null) {
        throw handlerResult.error || error;
      } else {
        return handlerResult.result;
      }
    }
  };
}
