import axios from 'axios';
import { store } from 'helpers';
import { getAgentListAction, onInitStatus, onUpdateCallData } from 'store/dialer.store';
import {
  csShowCallStatus,
  csAcceptCall,
  csCallRinging,
  csCurrentCallId,
  csEndCall,
  csHoldCall,
  csUnHoldCall,
  showCalloutError,
  csCustomerAccept,
  csListTransferAgent,
  csTransferCallResponse,
  csTransferCallSuccess,
  csTransferCallError,
  csLog,
  csNewCallTransferRequest,
  csTransferAlert,
  csInitError,
  csInitComplete
} from './events';
import * as actionTypes from './actionTypes';
import { init_ua, unregister, ua } from './sipSocket';
import { changeDevice } from './actions';
import { message } from 'antd';
import WebPush from './WebPush';
import RetryAble from 'utils/RetryAble';
import { loginVCCAsync } from 'store/user.store';

export let timeoutTransfer: NodeJS.Timeout;
export let timeoutToPing = 10; //s
export let lastSentTime = new Date().getTime();
export let ping = { objectId: 11111111 };
export let disconnectAs = false;
export let noRetry = 1;
export let totalRetry = 0;
export let retryTimeout = true;
export let pingreq: NodeJS.Timeout;
export let BaseRequest: {
  agentId?: string;
  extension?: string;
} = {
  agentId: '',
  extension: ''
};
export let csUser: { name?: string; agent_id?: string; role?: number; extension?: string; phone_no?: string };
export let csVoice: CSVoice = {
  callStatus: 'Offline',
  enableVoice: false,
  hasCall: false,
  isRinging: false,
  isAnswering: false,
  isMute: false,
  isHold: false,
  deviceType: 1,
  holding: false,
  isConnectToAgent: false,
  isCallout: false,
  disconnectAs: false,
  sip_socket: '',
  sip_address: '',
  agentserver_socket: '',
  webAutoAnswerCall: 0,
  isInternalCall: false
};
let agentWebPush: WebPush | null;

export function CallInfo(this: CallInfo) {
  this.callId = '';
  this.caller = '';
  this.agentId = '';
  this.line = '';
  this.callerName = '';
  this.callerId = '';
  this.tabIndex = '';
  this.startTime = '';
  this.endTime = '';
  this.ticketId = '';
}

/**
 * Kiểm tra quyền microphone của trình duyệt
 */
function checkMedia() {
  if (csVoice.deviceType === 1) {
    navigator.getUserMedia = navigator.getUserMedia || navigator['webkitGetUserMedia'] || navigator['mozGetUserMedia'];
    if (navigator.getUserMedia) {
      navigator.getUserMedia(
        { audio: true },
        function(stream) {
          console.log('Accessed the Microphone');
        },
        function(err) {
          console.log('Error: Cannot access the microphone');
        }
      );
    } else {
      console.log('Error: Cannot access the microphone');
    }
  }

  Notification.requestPermission().then(function(permission) {
    console.log('Notification', permission);
  });
}

export function sendAs(message: any) {
  BaseRequest.agentId = csUser.name;
  BaseRequest.extension = csUser.agent_id;
  Object.assign(message, BaseRequest);

  agentWebPush?.send(message);
  // csLog('Client send: ' + JSON.stringify(message));
  lastSentTime = new Date().getTime();
}

export async function initSocket(authToken: string, domain: string) {
  store.dispatch(onInitStatus('loading'));
  const retryAble = new RetryAble(
    () =>
      axios.get<VCCUser>(actionTypes.API_SERVER + '/api/login/check-partner', {
        params: {
          authToken,
          domain
        }
      }),
    (error, index, final) => {
      if (final) {
        if (error.message === 'Request failed with status code 401') {
          store.dispatch(loginVCCAsync());
        }
        csInitError(error.message);
        store.dispatch(onInitStatus('failed'));
        return { error };
      }

      return {};
    },
    1000
  );

  const { data } = await retryAble.run();

  if (data?.code === 200) {
    csUser = {
      name: data.user.username,
      agent_id: data.user.agent_id,
      extension: data.user.agent_id,
      phone_no: data.user.phone_no,
      role: Number(data.user.role)
    };
    csVoice.agentserver_socket = data.account.agent_server_url;
    csVoice.sip_address = data.account.sip_server_url;
    csVoice.sip_socket = data.account.sip_socket;
    createWs(authToken);
    store.dispatch(getAgentListAction());
  }
}

function reconnectAs() {
  csLog('Lost connect to socket server,reconnecting....(' + noRetry + '/' + totalRetry + ')');
  if (csVoice.agentserver_socket) {
    agentWebPush?.connect(csVoice.agentserver_socket, 0);
  }
}

export function getErrorMessage(errorCode?: string) {
  if (errorCode === 'IPCCERR0000') {
    return 'CANNOT_FIND_THE_CALL';
  }
  if (errorCode === 'IPCCERR0001') {
    return 'AGENT_IS_NOT_AVAILABLE';
  }
  if (errorCode === 'IPCCERR0002') {
    return 'CANNOT_FIND_THE_AGENT';
  }
  if (errorCode === 'IPCCERR0003') {
    return 'AGENT_IS_IN_MUTE_OR_HOLD_STATUS';
  }
  if (errorCode === 'IPCCERR0004') {
    return 'AGENT_IS_NOT_IN_MUTE_STATUS';
  }
  if (errorCode === 'IPCCERR0005') {
    return 'AGENT_IS_NOT_IN_HOLD_STATUS';
  }

  if (errorCode === 'IPCCERR0007') {
    return 'NUMBER_OF_PARAMETERS_ARE_NOT_SATISFIED';
  }
  if (errorCode === 'IPCCERR0008') {
    return 'CANNOT_HOLD_THE_RINGING_CALL';
  }
  if (errorCode === 'IPCCERR0009') {
    return 'CANNOT_MUTE_THE_RINGING_CALL';
  }

  if (errorCode === 'IPCCERR0011') {
    return 'CANNOT_INTERCEPT_A_MUTING_CALL';
  }
  if (errorCode === 'IPCCERR0012') {
    return 'NOT_CHOOSE_TYPE_TRANSFER';
  }
  if (errorCode === 'IPCCERR0013') {
    return 'TRANSFER_CALL_FAILED';
  }
  if (errorCode === 'IPCCERR0014') {
    return 'NOT_IN_A_CALL';
  }
  if (errorCode === 'IPCCERR0015') {
    return 'COULD_NOT_GET_CALL_INFO';
  }

  return errorCode;
}

/**
 * Xác nhận nhận cuộc gọi chuyển tiếp
 */
export function responseTransferAgent(val: number) {
  clearTimeout(timeoutTransfer);
  const mess = {
    callId: csVoice.transferRequest?.callId,
    dropAgentId: csVoice.transferRequest?.dropAgentId,
    newAgentId: csVoice.transferRequest?.newAgentId,
    status: val === 1 ? 'OK' : 'NOK',
    objectId: actionTypes.TRANSFER_AGENT_TO_AGENT_CONFIRM_RESPONSE_ID
  };
  sendAs(mess);
}

function createWs(token: string) {
  agentWebPush = new WebPush();

  agentWebPush.onConnected = function() {
    const refresh = { objectId: 1010003, authenToken: token };
    sendAs(refresh);
    noRetry = 1;
    disconnectAs = false;
    csLog('Socket connected, logging in..');

    message.destroy('vcc-loading');
    message.destroy('vcc-disconnect');
  };

  agentWebPush.onDisconnected = function() {
    if (noRetry <= totalRetry && retryTimeout) {
      retryTimeout = false;
      setTimeout(function() {
        retryTimeout = true;
        noRetry++;
        reconnectAs();

        message.loading({
          key: 'vcc-loading',
          content: `Mất kết nối VCC, đang kết nối lại... (${noRetry}/${totalRetry})`,
          duration: 0,
          style: {
            cursor: 'pointer'
          },
          onClick: () => {
            message.destroy('vcc-loading');
          }
        });
      }, 10000);
    }
    if (noRetry > totalRetry) {
      csLog('Disconnected from server due to network issue, please refresh your browser');
      message.error({
        key: 'vcc-disconnect',
        content: `Mất kết nối VCC. Vui lòng tải lại trang web`,
        duration: 0,
        style: {
          cursor: 'pointer'
        },
        onClick: () => {
          message.destroy('vcc-disconnect');
        }
      });
    }
    disconnectAs = true;
  };

  agentWebPush.connect(csVoice.agentserver_socket, 0);

  agentWebPush.onMessageReceived = function(responseResult) {
    const status = responseResult.status;

    try {
      switch (responseResult.objectId) {
        case actionTypes.CHANGE_AGENT_STATUS_RESPOND_ID:
          if (status !== 'OK') {
            if (csVoice.callStatus === 'Online') {
              csVoice.callStatus = 'Offline';
            } else {
              csVoice.callStatus = 'Online';
            }
          }
          csShowCallStatus(csVoice.callStatus);
          break;

        case actionTypes.CHANGE_DEVICE_TYPE_RESPONSE:
          if (Number(responseResult.result) === 0) {
            csVoice.deviceType = Number(responseResult.oldDeviceType);
          } else {
            csVoice.deviceType = Number(responseResult.newDeviceType);
            if (responseResult.newDeviceType === 1) {
              if (!ua || !ua.isRegistered()) {
                init_ua(csUser.agent_id, csVoice.sip_address, csVoice.sip_socket);
                checkMedia();
              }
            } else if (responseResult.oldDeviceType === 1) {
              unregister();
            }
          }
          break;

        case actionTypes.RING_AGENT_RESPOND_ID:
          if (responseResult.extension === csUser.agent_id) {
            if (csVoice.enableVoice) {
              csVoice.callInfo = new CallInfo();
              if (csVoice.callInfo) {
                csVoice.callInfo.callId = responseResult.callId;
                csVoice.callInfo.caller = responseResult.caller;
                csVoice.callInfo.callerName = responseResult.caller;
                csVoice.callInfo.line = responseResult.line;
                csVoice.callInfo.queueDescription = responseResult.queueDescription;
                csVoice.callInfo.callDirection = responseResult.callDirection;
                csVoice.callInfo.agentId = responseResult.extension;
                csVoice.callInfo.ticketId = responseResult.ticketId;
              }
              csVoice.hasCall = true;
              csVoice.isRinging = true;
              csVoice.missCall = false;
              csVoice.isAnswering = false;
              csVoice.isMute = false;
              csVoice.isHold = false;
              csVoice.isCallout = false;
              if (csVoice.deviceType !== 1) {
                csCallRinging(responseResult.caller);
              }
              csCurrentCallId(responseResult.callId);
            }
          }
          break;

        case actionTypes.AGENT_ANSWER_RESPOND_ID:
          if (responseResult.extension === csUser.agent_id) {
            if (csVoice.enableVoice) {
              if (csVoice.deviceType !== 1) {
                csVoice.isRinging = false;
                csVoice.isAnswering = true;
                csAcceptCall();
              }
            }
          }
          break;

        case actionTypes.AGENT_END_CALL_RESPOND_ID:
          if (responseResult.extension === csUser.agent_id) {
            if (csVoice.enableVoice) {
              if (csVoice.deviceType !== 1) {
                csVoice.hasCall = false;
                csVoice.holding = false;
                if (!csVoice.isAnswering) {
                  csVoice.missCall = true;
                  csVoice.isRinging = false;
                } else {
                  csVoice.isAnswering = false;
                }
                csVoice.callInfo = null;
                csEndCall();
              }
            }
          }
          break;

        case actionTypes.MUTE_RESPOND_ID:
          if (csVoice.enableVoice) {
            csVoice.isMute = true;
          }
          break;

        case actionTypes.UNMUTE_RESPOND_ID:
          if (csVoice.enableVoice) {
            csVoice.isMute = false;
          }
          break;

        case actionTypes.HOLD_RESPOND_ID:
        case actionTypes.HOLD_OUTCALL_RESPONSE_ID:
          if (csVoice.enableVoice) {
            if (status === 'OK') {
              csVoice.holding = false;
              csVoice.isHold = true;
            } else {
              csVoice.holding = false;
            }
            csHoldCall();
          }
          break;

        case actionTypes.UNHOLD_RESPOND_ID:
        case actionTypes.UNHOLD_OUTCALL_RESPONSE_ID:
          if (csVoice.enableVoice) {
            if (status === 'OK') {
              csVoice.isHold = false;
              csVoice.holding = false;
            } else {
              csVoice.holding = false;
            }
            csUnHoldCall();
          }
          break;

        case actionTypes.OUT_CALL_RESPONSE_ID:
          if (csVoice.enableVoice && responseResult.ipphone === csUser.agent_id) {
            if (status?.toUpperCase() === 'ERROR') {
              showCalloutError(responseResult.errorCode, responseResult.sipCode);
              csVoice.isConnectToAgent = false;
              csVoice.hasCall = false;
              csVoice.isRinging = false;
              csVoice.isAnswering = false;
              csVoice.callInfo = null;
              csVoice.isCallout = false;
            } else if (status?.toUpperCase() === 'STARTING') {
              if (csVoice.callInfo == null) {
                csVoice.callInfo = new CallInfo();
                csVoice.isCallout = true;
                if (csVoice.callInfo) {
                  csVoice.callInfo.callId = responseResult.callId;
                  csVoice.callInfo.line = responseResult.line;
                }
              } else {
                csVoice.callInfo.callId = responseResult.callId;
                csVoice.callInfo.line = responseResult.line;
              }
            } else if (status?.toUpperCase() === 'RINGING') {
              if (csVoice.deviceType !== 1) {
                csVoice.isConnectToAgent = false;
                csVoice.hasCall = true;
                csVoice.isRinging = true;
                csVoice.missCall = false;
                csVoice.isAnswering = false;
                csVoice.isMute = false;
                csVoice.isHold = false;
                if (csVoice.callInfo) {
                  csVoice.callInfo.caller = responseResult.telephoneNumber;
                  csVoice.callInfo.callerName = responseResult.telephoneNumber;
                  csVoice.callInfo.line = responseResult.line == '' ? ' ' : responseResult.line;
                  csVoice.callInfo.callDirection = 2;
                  csVoice.callInfo.agentId = responseResult.extension;
                  csVoice.callInfo.ticketId = responseResult.ticketId;
                }
              }
              csCurrentCallId(responseResult.callId);
              csCallRinging(responseResult.telephoneNumber);
            } else if (status?.toUpperCase() === 'ANSWER') {
              if (csVoice.deviceType !== 1) {
                csVoice.isRinging = false;
                csVoice.isAnswering = true;
              }
              csCustomerAccept();
            } else if (status?.toUpperCase() === 'ENDCALL') {
              if (csVoice.deviceType !== 1) {
                csVoice.hasCall = false;
                csVoice.isConnectToAgent = false;
                csVoice.isRinging = false;
                csVoice.isAnswering = false;
                csVoice.holding = false;
                csVoice.isAnswering = false;
                csVoice.isCallout = false;
                csVoice.callInfo = null;
              }
            } else if (status?.toUpperCase() === 'TERMINATE') {
              if (csVoice.deviceType !== 1) {
                csVoice.hasCall = false;
                csVoice.isConnectToAgent = false;
                csVoice.isRinging = false;
                csVoice.isAnswering = false;
                csVoice.holding = false;
                csVoice.isAnswering = false;
                csVoice.isCallout = false;
                csVoice.callInfo = null;
              }
            }
          }
          break;

        case actionTypes.ERROR_CALL_RESPONSE_ID:
          if (status !== 'OK') {
            alert(status);
          }
          if (status === actionTypes.ERROR_CODE_CALL_NOT_FOUND) {
            csVoice.hasCall = false;
            csVoice.missCall = false;
            csVoice.isAnswering = false;
            csVoice.isRinging = false;
            csVoice.isMute = false;
            csVoice.isHold = false;
            csVoice.isCallout = false;
            csVoice.callInfo = null;
          }
          break;

        case actionTypes.REFRESH_ID:
          if (responseResult.agentStatus !== 'AVAILABLE') {
            csVoice.callStatus = 'Offline';
          } else {
            csVoice.callStatus = 'Online';
          }
          csShowCallStatus(csVoice.callStatus);
          if (responseResult.isVoiceChannel === 1) {
            csVoice.enableVoice = true;
            if (responseResult.deviceType === 1) {
              if (!ua || !ua.isRegistered()) {
                init_ua(csUser.agent_id, csVoice.sip_address, csVoice.sip_socket);
                checkMedia();
              }
            } else {
              changeDevice(1, '');
            }
            csVoice.deviceType = Number(responseResult.deviceType);
          } else {
            csVoice.deviceType = Number(responseResult.deviceType);
          }
          csInitComplete();
          totalRetry = 20;
          clearInterval(pingreq);
          pingreq = setInterval(function() {
            if (disconnectAs) {
              clearInterval(pingreq);
            } else {
              const now = new Date().getTime() - lastSentTime;
              const itv = Math.round(now / 1000);
              if (itv > timeoutToPing) {
                sendAs(ping);
              }
            }
          }, 10000);
          csLog('Logged in successful');
          break;

        case actionTypes.LOGIN_RESPONSE_ID:
          csVoice.webAutoAnswerCall = responseResult.webAutoAnswerCall;
          break;

        case actionTypes.ENABLE_VOICE_CHANNEL_RESPONSE:
          if (Number(responseResult.result) === 1) {
            csVoice.deviceType = responseResult.deviceType;
            if (responseResult.deviceType === 1) {
              totalRetry = 20;
              if (!ua || !ua.isRegistered()) {
                init_ua(csUser.agent_id, csVoice.sip_address, csVoice.sip_socket);
              }
            } else {
              totalRetry = 20;
              changeDevice(1, '');
            }
            csVoice.enableVoice = true;
          }
          break;

        case actionTypes.CONFIRM_ENABLE_VOICE_CHANNEL_RESPONSE:
          break;

        case actionTypes.DISABLE_VOICE_CHANNEL_RESPONSE:
          csVoice.enableVoice = false;
          unregister();
          break;

        case actionTypes.PING_ID:
          sendAs(ping);
          break;

        case actionTypes.RELOAD_AGENT_STATUS_RESPONSE_ID:
          if (responseResult.agentStatus === 'NO ANSWER') {
            csVoice.callStatus = 'No Answer';
            csShowCallStatus(csVoice.callStatus);
          }
          break;

        case actionTypes.INTERNAL_CALL_RING_RESPONSE_ID:
          if (csVoice.enableVoice) {
            csVoice.isConnectToAgent = false;
            csVoice.hasCall = true;
            csVoice.isRinging = true;
            csVoice.missCall = false;
            csVoice.isAnswering = false;
            csVoice.isMute = false;
            csVoice.isHold = false;
            csVoice.callInfo = new CallInfo();
            if (csVoice.callInfo) {
              csVoice.callInfo.callId = responseResult.callId;
              csVoice.callInfo.caller = responseResult.called;
              csVoice.callInfo.callerName = responseResult.called;
              csVoice.callInfo.line = '';
              csVoice.callInfo.callDirection = 3;
              csVoice.callInfo.agentId = responseResult.called;
            }
          }
          break;

        case actionTypes.INTERNAL_CALL_ANSWER_RESPONSE_ID:
          if (csVoice.enableVoice) {
            if (csVoice.deviceType !== actionTypes.DEVICE_BROWSER) {
              csVoice.isRinging = false;
              csVoice.isAnswering = true;
            }
          }
          break;

        case actionTypes.INTERNAL_CALL_END_RESPONSE_ID:
          if (csVoice.enableVoice) {
            if (csVoice.deviceType !== actionTypes.DEVICE_BROWSER) {
              csVoice.hasCall = false;
              csVoice.isConnectToAgent = false;
              csVoice.isRinging = false;
              csVoice.isAnswering = false;
              csVoice.holding = false;
              csVoice.callInfo = null;
            }
          }
          break;

        case actionTypes.TERMINATE_RESPOND_ID: {
          const callId = responseResult.callId;
          if (status !== 'OK') {
            console.log('errorMessage', getErrorMessage(status));
          } else {
            const superAgent = responseResult.superAgent;
            const terminatedAgent = responseResult.terminatedAgent;
            //lay thong tin ve Agent extension
            const currentAgentId = csUser.agent_id;
            //neu du lieu nhan ve, ung voi man hinh cua Agent ra lenh ket thuc, thuong la Supervisor
            //hien thi thong bao ket thuc cuoc goi thanh cong
            if (currentAgentId !== terminatedAgent) {
              //neu du lieu nhan ve, ung voi man hinh cua Agent bi ket thuc cuoc goi,
              // hien thi thong bao nguyen nhan vi sao cuoc goi bi ket thuc
              console.error({ superAgent: superAgent, call: callId });
            }
          }
          break;
        }

        case actionTypes.AGENT_SEARCH_AGENT_RESPONSE_ID:
          csListTransferAgent(responseResult.listAgentSearch ?? []);
          break;

        case actionTypes.TRANSFER_AGENT_TO_AGENT_CONFIRM_REQUEST_ID:
          if (csVoice.enableVoice) {
            csNewCallTransferRequest(responseResult);
            csVoice.transferRequest = responseResult;
            timeoutTransfer = setTimeout(function() {
              responseTransferAgent(0);
            }, 10000);
          }
          break;

        case actionTypes.SHOW_MESSAGEBOX_REQUEST_ID:
          csTransferAlert(responseResult.message);
          break;

        case actionTypes.TRANSFER_AGENT_TO_AGENT_CONFIRM_RESPONSE_ID:
          if (status === 'OK') {
            csTransferCallResponse('OK');
          } else {
            csTransferCallResponse('NOK');
          }
          break;

        case actionTypes.TRANFER_TO_ACD_RESPONSE:
          if (status === 'OK') {
            csTransferCallResponse('OK');
          } else {
            csTransferCallResponse('NOK');
          }
          break;

        case actionTypes.TRANSFER_AGENT_TO_AGENT_RESPOND_ID: {
          const agentReceive = { superAgent: responseResult.superAgent, ipphone: responseResult.newAgent };
          if (responseResult.dropAgent === csUser.agent_id) {
            if (status !== 'OK') {
              const errMessage = getErrorMessage('IPCCERR0013');
              csTransferCallError(errMessage, agentReceive);
            } else {
              csTransferCallSuccess(agentReceive);
            }
          } else if (csUser.agent_id === responseResult.newAgent) {
            if (status !== 'OK') {
              const errorMessage = getErrorMessage(status);
              csTransferCallError(errorMessage);
            }
          }
          break;
        }

        case actionTypes.TICKET_CREATE_VOICE_IN_RESPOND_ID:
        case actionTypes.CALLER_INFO_VOICE_IN_RESPOND_ID:
          if (csVoice.callInfo) {
            csVoice.callInfo.ticketId = responseResult.ticketId;
          }
          break;

        case actionTypes.AUTO_ANSWER_CALL_RESPONSE_ID:
          csVoice.webAutoAnswerCall = responseResult.webAutoAnswerCall;
          break;
      }
      store.dispatch(onUpdateCallData(csVoice));
    } catch (ex) {
      console.log(ex.message);
    }
  };
}

export async function closeSocket() {
  agentWebPush?.disconnect();
  csUser = {};
  csVoice = {
    callStatus: 'Offline',
    enableVoice: false,
    hasCall: false,
    isRinging: false,
    isAnswering: false,
    isMute: false,
    isHold: false,
    deviceType: 1,
    holding: false,
    isConnectToAgent: false,
    isCallout: false,
    disconnectAs: false,
    sip_socket: '',
    sip_address: '',
    agentserver_socket: '',
    webAutoAnswerCall: 0,
    isInternalCall: false
  };
  store.dispatch(onUpdateCallData(csVoice));
}
