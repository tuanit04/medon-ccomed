import { Modal } from 'antd';
import { store } from 'helpers';
import { VCCStatus } from 'interface/user/vcc';
import { saveCallAsync } from 'sagas/callSaga';
import {
  onCloseDialer,
  onCustomerAccept,
  onInitStatus,
  onMinimize,
  onOpenDialer,
  onUpdateAgentList,
  saveAgentStatusAction
} from 'store/dialer.store';
import { openNotificationWithIcon } from 'utils/helper';
import { openNotificationRight } from 'utils/notification';
import { notifyMe, onAcceptCall, reConfigDeviceType } from './actions';
import { csUnregister } from './sipSocket';
import { csVoice, responseTransferAgent } from './socket';

const ERROR_CODE = {
  IPCC_NOT_CONNECT_CUSTOMER: 'Không kết nối được đến số thuê bao',
  IPCC_NOT_CONNECT_AGENT_ID:
    'Không thể kết nối được đến thiết bị nghe gọi của tư vấn viên kiểm tra lại IP Phone/SoftPhone và đường truyền Internet',
  CALLOUT_AGENT_BUSY: 'Vui lòng đăng xuất và đăng nhập lại để tiếp tục sử dụng',
  CALLOUT_PERMISSION_DENY: 'Không có quyền gọi ra. Liên hệ Admin để cấu hình',
  ERROR_CALLOUT_CONNECT: 'Có lỗi xảy ra',
  486: 'Khách hàng đang bận',
  408: 'Khách hàng không nghe máy hoặc có lỗi xảy ra',
  403: 'Đầu số này chưa được thanh toán cước',
  487: 'Khách hàng không nghe máy',
  480: 'Thuê bao khách hàng tạm thời không liên lạc được',
  '': 'Có lỗi xảy ra'
};
var audio = new Audio('https://vcc-web1.vinaphone.com.vn/sounds/incoming.mp3');
audio.loop = true;

/**
 * Sự kiện này xảy ra khi trạng thái cuộc gọi bị thay đổi
 */
export function csShowCallStatus(status?: string) {
  if (status === 'No Answer') {
    store.dispatch(
      saveAgentStatusAction({
        reasonUnready: 2,
        type: 'UNREADY'
      })
    );
  } else if (status === 'Online') {
    // store.dispatch(
    //   saveAgentStatusAction({
    //     type: 'READY'
    //   })
    // );
  }
}

/**
 * Sự kiện xảy ra khi có cuộc gọi đang diễn ra và trả về callId của cuộc gọi
 */
export function csCurrentCallId(callId?: string) {
  console.log('csCurrentCallId: ' + callId);
  // store.dispatch(onOpenDialer());
  store.dispatch(onMinimize(true));
}

/**
 * Sự kiện này được gọi khi có cuộc gọi đến nhân viên (phone là số điện thoại gọi đến)
 */
export function csCallRinging(phone?: string) {
  if (document.visibilityState === 'hidden') {
    notifyMe(phone);
  }

  if (!csVoice.isCallout) {
    playAudio();
  }

  if (!csVoice.isConnectToAgent) {
    setTimeout(async () => {
      store.dispatch(saveCallAsync({ vccStatus: VCCStatus.STARTING }));
      store.dispatch(saveCallAsync({ vccStatus: VCCStatus.RINGING }));
      if (csVoice.webAutoAnswerCall === 1) {
        onAcceptCall();
      }
    }, 1000);
  }
}

/**
 * Sự kiện này được gọi khi nhân viên tiếp nhận cuộc gọi
 */
export function csAcceptCall() {
  console.log('csAcceptCall');
  if (!csVoice.isCallout) {
    stopAudio();
    store.dispatch(onCustomerAccept(true));
    store.dispatch(saveCallAsync({ vccStatus: VCCStatus.ANSWER }));
  }
}

/**
 * Sự kiện này được gọi khi cuộc gọi kết thúc.
 */
export function csEndCall() {
  stopAudio();
  store.dispatch(saveCallAsync({ vccStatus: VCCStatus.END }));
  store.dispatch(onCustomerAccept(false));
  store.dispatch(onCloseDialer());
}

/**
 * Sự kiện này được gọi khi cuộc gọi bị Hold
 */
export function csHoldCall() {
  console.log('Call is holded');
  store.dispatch(saveCallAsync({ vccStatus: VCCStatus.HOLD }));
}

/**
 * Sự kiện này được gọi khi cuộc gọi bị UnHold
 */
export function csUnHoldCall() {
  console.log('Call is unholded');
  store.dispatch(saveCallAsync({ vccStatus: VCCStatus.UNHOLD }));
}

/**
 * Sự kiện này được gọi khi có xảy ra lỗi lúc gọi đến khách hàng (errorCode, sipCode sẽ được mô tả trong bảng dưới)
 */
export function showCalloutError(errorCode?: string, sipCode?: string) {
  console.log('showCalloutError ' + errorCode + ' - ' + sipCode);
  const code = sipCode || errorCode || '';
  openNotificationWithIcon('error', 'Lỗi cuộc gọi', ERROR_CODE[code]);
}

/**
 * Sự kiện này được gọi khi khách hàng nghe cuộc gọi đối với cuộc gọi ra
 */
export function csCustomerAccept() {
  store.dispatch(saveCallAsync({ vccStatus: VCCStatus.ANSWER }));
  store.dispatch(onCustomerAccept(true));
}

/**
 * Sự kiện lấy danh sách agent chuyển tiếp
 */
export function csListTransferAgent(listTransferAgent: CSAgent[]) {
  store.dispatch(onUpdateAgentList({ agentList: listTransferAgent }));
}

/**
 * Sự kiện thông báo chuyển tiếp cuộc gọi
 */
export function csTransferAlert(message?: string) {
  openNotificationRight(message);
}

/**
 * Sự kiện nhận yêu cầu chuyển tiếp cuộc gọi
 */
export function csNewCallTransferRequest(message: CSMessage) {
  Modal.confirm({
    title: 'Chuyển tiếp cuộc gọi',
    content: `Bạn nhận được yêu cầu chuyển cuộc gọi từ ${message.newAgentName}`,
    onOk: () => {
      responseTransferAgent(1);
    },
    onCancel: () => {
      responseTransferAgent(0);
    }
  });
}

/**
 * Sự kiện trạng thái yêu cầu chuyển tiếp cuộc gọi
 */
export function csTransferCallResponse(state: 'OK' | 'NOK') {
  if (state === 'OK') {
    openNotificationWithIcon('success', 'Chuyển tiếp cuộc gọi', 'Yêu cầu đã được gửi đi');
  } else {
    openNotificationWithIcon('error', 'Chuyển tiếp cuộc gọi', 'Yêu cầu chưa được gửi đi');
  }
}

/**
 * Sự kiện chuyển tiếp cuộc gọi lỗi
 */
export function csTransferCallError(errMessage?: string, agentReceive?: { superAgent?: string; ipphone?: string }) {
  openNotificationWithIcon('error', 'Chuyển tiếp cuộc gọi', `${agentReceive?.superAgent ?? ''} - ${errMessage}`);
}

/**
 * Sự kiện chuyển tiếp cuộc gọi thành công
 */
export function csTransferCallSuccess(agentReceive: { superAgent?: string; ipphone?: string }) {
  openNotificationWithIcon('success', 'Chuyển tiếp cuộc gọi', `Đã chuyển tiếp cuộc gọi cho ${agentReceive.superAgent}`);
}

export function csLog(message: string) {
  const debug = localStorage.getItem('csDebug');
  if (debug !== 'off') {
    console.log(message);
  }
}

/**
 * Sự kiện khởi tạo kết nối thất bại
 */
export function csInitError(message: string) {
  openNotificationWithIcon('error', 'Khởi tạo VCC thất bại', message);
}

/**
 * Sự kiện khởi tạo kết nối thành công
 */
export function csInitComplete() {
  if (!csVoice.enableVoice) {
    // csEnableCall();
  }
  store.dispatch(onInitStatus('success'));

  window.onbeforeunload = function() {
    if (csVoice.hasCall) {
      store.dispatch(saveCallAsync({ vccStatus: VCCStatus.END }));
    }
    csUnregister();
    if (csVoice.enableVoice) {
      reConfigDeviceType();
    }
  };
}

/**
 * Phát nhạc
 */
function playAudio() {
  audio.play();
}

/**
 * Dừng nhạc
 */
function stopAudio() {
  audio.pause();
  audio.currentTime = 0;
}
