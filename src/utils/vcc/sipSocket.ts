import JsSIP from 'jssip';
import { csAcceptCall, csCallRinging, csEndCall } from './events';
import { csVoice, CallInfo } from './socket';

const state: {
  status: 'connecting' | 'disconnected' | 'connected' | 'registered' | 'unregistered';
  session: any;
  incomingSession?: any;
} = {
  status: 'disconnected',
  session: null
};

// UA
export let ua: JsSIP.UA | null = null;

// HTML5 <video> elements in which local and remote video will be shown
let selfView: HTMLMediaElement | null = null;
let remoteView: HTMLMediaElement | null = null;

export function init_ua(agentId?: string, uri?: string, ws?: string) {
  const socket = new JsSIP.WebSocketInterface(ws ?? '');
  const configuration = {
    sockets: [socket],
    uri: 'sip:' + agentId + '@' + uri,
    password: '123456a@',
    register_expires: 360
  };
  ua = new JsSIP.UA(configuration);
  if (selfView == null) {
    selfView = document.getElementById('my-video') as HTMLMediaElement;
    remoteView = document.getElementById('peer-video') as HTMLMediaElement;
  }

  // events handler
  ua.on('connected', function(e) {
    /* Your code here */
    state.status = 'connected';
  });

  ua.on('disconnected', function(e) {
    /* Your code here */
    state.status = 'disconnected';
  });

  ua.on('registered', function(e) {
    /* Your code here */
    state.status = 'registered';
  });

  ua.on('unregistered', function(e) {
    /* Your code here */
    if (ua?.isConnected()) state.status = 'unregistered';
    else state.status = 'disconnected';
  });

  ua.on('registrationFailed', function(e) {
    /* Your code here */
    if (ua?.isConnected()) state.status = 'unregistered';
    else state.status = 'disconnected';
  });

  ua.on('newRTCSession', function(e) {
    /* Your code here */
    // Avoid if busy or other incoming
    if (state.session || state.incomingSession) {
      session.terminate({
        status_code: 486,
        reason_phrase: 'Busy Here'
      });
      return;
    }

    var session = (state.session = e.session);

    session.on('failed', function(e) {
      state.session = null;
      csVoice.isInternalCall = false;
      csEndCall();
      endedCall();
    });

    session.on('ended', function(e) {
      state.session = null;
      if (selfView) selfView.srcObject = null;
      if (remoteView) remoteView.srcObject = null;
      csVoice.isInternalCall = false;
      csEndCall();
      endedCall();
    });

    session.on('progress', function(e) {
      const remote_identity = session.remote_identity;
      if (remote_identity) {
        const uri = remote_identity.uri;
        if (uri) {
          const user = uri.user;
          if (isInternalCall(user)) {
            csVoice.isInternalCall = true;
          }
          csCallRinging(user);
          hasCallRinging(user);
        }
      }
    });

    session.on('accepted', function(e) {
      state.session = session;
      const peerconnection = session.connection;
      const localStream = peerconnection.getLocalStreams()[0];
      const remoteStream = peerconnection.getRemoteStreams()[0];
      //selfView.srcObject = localStream;
      if (remoteView) remoteView.srcObject = remoteStream;

      if (!csVoice.isInternalCall) {
        acceptedCall();
        csAcceptCall();
      }
    });

    session.on('confirmed', function(e) {
      if (!isCsCallout()) {
        acceptedCall();
        csAcceptCall();
      }
    });
  });

  ua.start();
}

export function register() {
  if (!ua || !ua.isRegistered()) {
    init_ua();
  }
  if (ua && typeof ua.stop == 'function') {
    ua.stop();
  }
}

export function unregister() {
  if (ua && ua.isRegistered()) {
    ua.unregister();
  }
  if (ua && typeof ua.stop == 'function') {
    ua.stop();
  }
}

export function dial_call(uri) {
  if (!ua || !ua.isConnected) return;

  // Register callbacks to desired call events
  const eventHandlers = {
    progress: function(data) {
      /* Your code here */
    },
    failed: function(data) {
      /* Your code here */
    },
    confirmed: function(data) {
      /* Your code here */
    },
    ended: function(data) {
      /* Your code here */
    }
  };

  ua.call(uri, {
    eventHandlers: eventHandlers,
    mediaConstraints: { audio: true, video: false },
    sessionTimersExpires: 1800,
    pcConfig: {
      rtcpMuxPolicy: 'require',
      iceServers: [
        { urls: ['stun:stun.l.google.com:19302', 'stun:stun1.l.google.com:19302', 'stun:stun2.l.google.com:19302'] }
      ]
    },
    rtcOfferConstraints: {
      offerToReceiveAudio: true,
      offerToReceiveVideo: false
    }
  });
}

export function answer_call() {
  console.log('Tryit: answer_call');
  const session = state.session;
  if (session) {
    session.answer({
      sessionTimersExpires: 1800,
      mediaConstraints: { audio: true, video: false },
      pcConfig: {
        rtcpMuxPolicy: 'require',
        iceServers: [
          { urls: ['stun:stun.l.google.com:19302', 'stun:stun1.l.google.com:19302', 'stun:stun2.l.google.com:19302'] }
        ]
      },
      rtcOfferConstraints: {
        offerToReceiveAudio: 1,
        offerToReceiveVideo: 0
      }
    });
  }
}

export function terminate_call() {
  const session = state.session;
  if (session) {
    session.terminate();
  }
}

export function sendDTMF(digit) {
  const session = state.session;
  if (session) {
    session.sendDTMF(digit);
  }
}

function hasCallRinging(phoneNumber: string | undefined) {
  if (csVoice.callInfo != null && csVoice.isCallout) {
    answer_call();
    csVoice.isConnectingCall = false;
  } else {
    csVoice.hasCall = true;
    csVoice.isRinging = true;
    csVoice.missCall = false;
    csVoice.isAnswering = false;
    csVoice.isMute = false;
    csVoice.isHold = false;
    csVoice.callInfo = new CallInfo();
    if (csVoice.callInfo) {
      csVoice.callInfo.caller = phoneNumber;
      csVoice.callInfo.callerName = phoneNumber;
      csVoice.callInfo.callDirection = 1;
    }
  }
}

function isCsCallout() {
  return csVoice.hasCall == true && csVoice.isCallout == true;
}

function isInternalCall(number: string) {
  return (
    (number == '' || !number.match(/^[0-9]{8,13}$/)) &&
    number != '' &&
    number.match(/^[0-9]{1,7}$/) &&
    number.match(/^[1-9].*/)
  );
}

function acceptedCall() {
  if (!csVoice.isCallout) {
    csVoice.isRinging = false;
    csVoice.isAnswering = true;
  } else {
    csVoice.isRinging = false;
    csVoice.isAnswering = true;
    csVoice.isConnectToAgent = false;
  }
}

function endedCall() {
  if (!csVoice.isCallout) {
    csVoice.hasCall = false;
    csVoice.holding = false;
    if (!csVoice.isAnswering) {
      csVoice.missCall = true;
      csVoice.isRinging = false;
    } else {
      csVoice.isAnswering = false;
    }
  } else {
    csVoice.hasCall = false;
    csVoice.isConnectToAgent = false;
    csVoice.isRinging = false;
    csVoice.isAnswering = false;
    csVoice.holding = false;
  }
  csVoice.isCallout = false;
  csVoice.callInfo = null;
  csVoice.isInternalCall = false;
}

/**
 * Hủy đăng ký
 */
export function csUnregister() {
  if (csVoice.deviceType == 1) {
    unregister();
  }
}
