interface IWebPush {
  onConnected: () => void;
  onDisconnected: () => void;
  onMessageReceived: (msg: CSMessage) => void;
  connect: (url?: string, forceLongPolling?: number) => void;
  poll: (url: string) => void;
  send: (msg: any) => void;
  sendWait: (msg: string, callback?: Function) => void;
  waitForConnection: (callback?: Function, interval?: number) => void;
  close: () => void;
}

declare var $: any;

class WebPush implements IWebPush {
  webSocket?: WebSocket;
  private url?: string;
  private requestDisconnect?: boolean;
  private id?: string;

  onConnected() {}

  onDisconnected() {}

  onMessageReceived(msg: any) {}

  connect(url?: string, forceLongPolling?: number) {
    const webPush = this;
    this.url = url;
    this.requestDisconnect = false;
    //fix IE AJAX cross domain
    $.support.cors = true;
    //setup disconnect khi chuyen trang
    $(window).unload(function() {
      webPush.close();
    });
    if (window.WebSocket && (forceLongPolling === undefined || !forceLongPolling)) {
      this.webSocket = new WebSocket('wss://' + url + '/websocket');
      this.webSocket.onopen = this.onConnected;
      this.webSocket.onclose = this.onDisconnected;
      this.webSocket.onerror = function() {
        webPush.onDisconnected();
      };
      this.webSocket.onmessage = function(evt) {
        var msg = JSON.parse(evt.data);
        webPush.onMessageReceived(msg);
      };
    } else {
      $.ajax({
        dataType: 'json',
        url: 'http://' + url + '/connect',
        data: {},
        cache: false, //fix loop IE
        success: function(data, textStatus, jqXHR) {
          if (data.r === 1) {
            webPush.id = data.id;
            webPush.onConnected();
            webPush.poll(url);
          } else {
            //                  cslog('connect error: ' + data.r);
            webPush.onDisconnected();
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          //              cslog('connect error: ' + textStatus);
          //              cslog(errorThrown);
          webPush.onDisconnected();
        }
      });
    }
  }

  poll(url?: string) {
    const webPush = this;
    $.ajax({
      dataType: 'json',
      url: 'http://' + url + '/poll/' + webPush.id,
      cache: false, //fix loop IE
      data: {},
      success: function(data, textStatus, jqXHR) {
        if (data.r === 1) {
          if (data.list) {
            for (var i = 0; i < data.list.length; i++) {
              var msg_i = data.list[i];
              webPush.onMessageReceived(msg_i);
            }
          }
          if (!webPush.requestDisconnect) {
            //                  cslog('poll success: +++++++++++');
            setTimeout(function() {
              webPush.poll(url);
            }, 1);
          } else {
            webPush.onDisconnected();
          }
        } else {
          //              cslog('poll error, code: ' + data.r);
          webPush.onDisconnected();
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        //          cslog('error: ' + textStatus);
        webPush.onDisconnected();
      },
      complete: function(jqXHR, textStatus) {
        if (textStatus === 'success') {
        }
      }
    });
  }

  send(msg: any) {
    const webPush = this;
    if (this.webSocket) {
      this.webSocket.send(JSON.stringify(msg));
    } else {
      $.ajax({
        dataType: 'json',
        url: 'http://' + webPush.url + '/send/' + webPush.id,
        type: 'POST',
        cache: false, //fix loop IE
        data: JSON.stringify(msg),
        success: function(data, textStatus, jqXHR) {
          //                cslog('success send: ' + data);
          //webPush.onMessageReceived(data);
          //webPush.poll(url);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          //webPush.onDisconnected();
        }
      });
    }
  }

  sendWait(msg: string, callback?: Function) {
    const webPush = this;
    if (this.webSocket) {
      this.waitForConnection(function() {
        webPush.webSocket?.send(JSON.stringify(msg));
        if (typeof callback !== 'undefined') {
          callback();
        }
      }, 1000);
    } else {
      $.ajax({
        dataType: 'json',
        url: 'http://' + webPush.url + '/send/' + webPush.id,
        type: 'POST',
        cache: false, //fix loop IE
        data: JSON.stringify(msg),
        success: function(data, textStatus, jqXHR) {
          //webPush.onMessageReceived(data);
          //webPush.poll(url);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          //webPush.onDisconnected();
        }
      });
    }
  }

  waitForConnection(callback?: Function, interval?: number) {
    if (this.webSocket?.readyState === 1) {
      callback?.();
    } else {
      var that = this;
      // optional: implement backoff for interval here
      setTimeout(function() {
        that.waitForConnection(callback, interval);
      }, interval);
    }
  }

  close() {
    if (this.webSocket) {
      this.webSocket.close();
    } else {
      this.requestDisconnect = true;
      $.ajax({
        dataType: 'json',
        url: 'http://' + this.url + '/disconnect/' + this.id,
        type: 'GET',
        cache: false, //fix loop IE
        data: {},
        success: function(data, textStatus, jqXHR) {
          //              cslog('success disconnect: ' + data);
          //webPush.onMessageReceived(data);
          //webPush.poll(url);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          //              cslog('error disconnect: ' + textStatus);
          //webPush.onDisconnected();
        }
      });
    }
  }

  disconnect() {
    if (this.webSocket) {
      this.webSocket.onopen = null;
      this.webSocket.onclose = null;
      this.webSocket.onmessage = null;
      this.webSocket.onerror = null;
      this.webSocket.close();
    }
  }
}

export default WebPush;
