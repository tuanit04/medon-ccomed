import { message } from 'antd';
import * as actionTypes from './actionTypes';
import { csTransferCallError } from './events';
import { answer_call, dial_call, terminate_call } from './sipSocket';
import { csVoice, csUser, CallInfo, sendAs, getErrorMessage } from './socket';

let notification: Notification;

/**
 * Thông báo cuộc gọi
 */
export function notifyMe(number?: string) {
  if (!Notification) {
    return;
  }

  if (Notification.permission !== 'granted') Notification.requestPermission();
  else {
    notification = new Notification('Cuộc gọi đến', {
      icon: '/logo.png',
      body: 'SĐT: ' + number,
      tag: 'new-phone-call-arrive-' + number,
      requireInteraction: true
    });

    notification.onclick = function() {
      window.focus();
      notification.close();
      if (csVoice.deviceType === 1) {
        // GUI.buttonAnswerClick(_Session);
        // answer_call();
      }
    };
  }
}

/**
 * Trả lời tự động
 */
export function autoAnswerCall(active: number) {
  const mess = {
    objectId: actionTypes.AUTO_ANSWER_CALL_REQUEST_ID,
    changeAgentId: csUser.agent_id,
    webAutoAnswerCall: active
  };

  sendAs(mess);
}

/**
 * Gọi điện thoại tới số điện thoại của khách hàng (number là số điện thoại cần gọi)
 */
export function csCallout(number: string) {
  if (csVoice.enableVoice == true) {
    if (!csVoice.isCallout) {
      if (number === '' || !number.match(/^[0-9]{8,13}$/)) {
        if (number !== '' && number.match(/^[0-9]{1,7}$/) && number.match(/^[1-9].*/)) {
          if (csVoice.deviceType == 1) {
            dial_call('sip:' + number + '@' + csVoice.sip_address);
            csVoice.isCallout = true;
            csVoice.hasCall = true;
            csVoice.isInternalCall = true;
          } else {
            console.log('error');
          }
        } else {
          console.log('wrong phone number');
        }
      } else {
        if (!csVoice.hasCall && !csVoice.isConnectToAgent) {
          csVoice.isCallout = true;
          const callOutMsg = {
            telephoneNumber: number,
            objectId: actionTypes.OUT_CALL_REQUEST_ID,
            ticketId: '',
            callerId: ''
          };
          sendAs(callOutMsg);
          csVoice.isRinging = true;
          if (csVoice.deviceType === 1) {
            csVoice.isConnectingCall = true;
          }
          csVoice.isConnectToAgent = true;
          csVoice.isCallout = true;
          csVoice.hasCall = true;
          csVoice.isRinging = true;
          csVoice.missCall = false;
          csVoice.isAnswering = false;
          csVoice.isMute = false;
          csVoice.isHold = false;
          csVoice.callInfo = new CallInfo();
          if (csVoice.callInfo) {
            csVoice.callInfo.callId = '';
            csVoice.callInfo.caller = number;
            csVoice.callInfo.callerName = number;
            csVoice.callInfo.line = ' ';
            csVoice.callInfo.callDirection = 2;
          }
        } else {
          console.log('Error callout');
        }
      }
    } else {
    }
  } else {
    message.error('Vui lòng bấm Kích hoạt để thực hiện cuộc gọi!');
  }
}

/**
 * VCC có hỗ trợ trạng thái tiếp nhận cuộc gọi là On/Off. Nếu để trạng thái là On thì cuộc gọi sẽ đổ về. Ngược lại thì sẽ không đổ về. Để chuyển qua lại 2 trạng thái thì sẽ dùng hàm này
 */
export function changeCallStatus() {
  let status: string;
  if (csVoice.callStatus !== 'Online') {
    status = 'AVAILABLE';
    csVoice.callStatus = 'Online';
  } else {
    status = 'NOT AVAILABLE';
    csVoice.callStatus = 'Offline';
  }
  const message = { status, objectId: 2013 };
  sendAs(message);
}

/**
 * Kết thúc cuộc gọi
 */
export function endCall() {
  if (csVoice.deviceType == 1) {
    csVoice.hasCall = false;
    csVoice.holding = false;
    csVoice.isConnectToAgent = false;
    csVoice.isRinging = false;
    csVoice.isAnswering = false;
    csVoice.callInfo = null;

    terminate_call();
  } else {
    if (csVoice.isHold) {
      return;
    }
    let objectId = actionTypes.TERMINATE_CALL_OUT_REQUEST_ID;
    if (csVoice.callInfo?.callDirection === 1) {
      objectId = actionTypes.REMOVE_AGENT_REQUEST_ID;
    } else if (csVoice.callInfo?.callDirection === 3) {
      objectId = actionTypes.TERMINATE_REQUEST_ID;
    }
    const interuptMsg = {
      callId: csVoice.callInfo?.callId,
      removeAgentId: csVoice.callInfo?.agentId,
      objectId: objectId
    };
    sendAs(interuptMsg);
  }
}

/**
 * Mute/Unmute cuộc gọi. Sự kiện này được gọi khi cuộc gọi bị Mute. Hàm này chỉ hoạt động cho cuộc gọi nhận từ khách hàng, không hoạt động cho cuộc gọi ra
 */
export function muteCall() {
  if (csVoice.isHold) {
    return;
  }
  let muteObjectID: number | undefined;
  if (csVoice.callInfo?.callDirection === 1) {
    if (csVoice.isMute) {
      muteObjectID = actionTypes.UNMUTE_REQUEST_ID;
    } else {
      muteObjectID = actionTypes.MUTE_REQUEST_ID;
    }
  } else {
    alert('Comming soon.');
  }

  const mess = { callId: csVoice.callInfo?.callId, callType: csVoice.callInfo?.callDirection, objectId: muteObjectID };
  sendAs(mess);
}

/**
 * Hold/Unhold cuộc gọi
 */
export function holdCall() {
  let holdIdObject: number;
  if (csVoice.callInfo?.callDirection === 1 || csVoice.callInfo?.callDirection === 3) {
    if (csVoice.isHold) {
      holdIdObject = actionTypes.UN_HOLD_REQUEST_ID;
    } else {
      holdIdObject = actionTypes.HOLD_REQUEST_ID;
    }
  } else {
    if (!csVoice.isRinging) {
      if (csVoice.isHold) {
        holdIdObject = actionTypes.UN_HOLD_OUTCALL_REQUEST_ID;
      } else {
        holdIdObject = actionTypes.HOLD_OUTCALL_REQUEST_ID;
      }
    } else {
      return;
    }
  }
  const mess = {
    callId: csVoice.callInfo?.callId,
    callType: csVoice.callInfo?.callDirection,
    objectId: holdIdObject
  };
  sendAs(mess);
}

/**
 * Tiếp nhận cuộc gọi đến
 */
export function onAcceptCall() {
  answer_call();
}

/**
 * VCC chỉ cho phép tại 1 thời điểm có 1 tab được quyền gọi. Vì thế nếu bật nhiều tab hoặc đăng nhập tại nhiều nơi thì cũng chỉ có duy nhất 1 tab được sử dụng chức năng thoại. Tab đầu tiên đăng nhập vào hệ thống sẽ có quyền thoại này. Các tab về sau muốn dùng thì sử dụng hàm này
 */
export function csEnableCall() {
  if (csVoice.enableVoice) {
    return;
  }
  const mess = {
    objectId: actionTypes.CONFIRM_ENABLE_VOICE_CHANNEL_REQUEST
  };
  sendAs(mess);
}

/**
 * Lấy danh sách agent chuyển tiếp
 */
export function getTransferAgent() {
  if (csVoice.callInfo != null) {
    if (csVoice.callInfo.callDirection == 1) {
      const mess = {
        username: '',
        agentStatus: 'AVAILABLE',
        callStatus: 'READY',
        operation: '',
        timeInStatus: '',
        objectId: actionTypes.AGENT_SEARCH_AGENT_REQUEST_ID
      };
      sendAs(mess);
    }
  }
}

/**
 * Thay đổi thiết bị tiếp nhận cuộc gọi
 */
export function changeDevice(tempDeviceType: number | undefined, phoneNo: string) {
  csVoice.deviceType = tempDeviceType;

  const mess = {
    objectId: actionTypes.CHANGE_DEVICE_TYPE_REQUEST,
    deviceType: tempDeviceType,
    phoneNum: phoneNo
  };

  sendAs(mess);
}

/**
 * Đối với nhân viên có vai trò Extension, Mobile hay VCC Softphone, khi đăng nhập vào để gọi điện sẽ gây bất đồng bộ khiến không thể tiếp nhận cuộc gọi trên các thiết bị mặc định được nữa. Gọi hàm này khi đăng xuất hoặc F5 web tích hợp để chuyển thiết bị nhân viên này về mặc định và tiếp nhận cuộc gọi như bình thường
 */
export function reConfigDeviceType() {
  if (csUser.role === 6) {
    //extension
    changeDevice(4, '');
  } else if (csUser.role === 7) {
    //mobile
    changeDevice(3, '');
  } else if (csUser.role === 9) {
    //3c Softphone
    changeDevice(6, '');
  }
}

/**
 * Chuyển tiếp cuộc gọi đến agent khác
 */
export function csTransferCallAgent(ipPhone: string) {
  const transfer = { agent: { ipPhone } };
  transferCallAgent(1, transfer);
}

export function csTransferCallAcd(queueId: any) {
  const transfer = { acd: { queueId: queueId } };
  transferCallAgent(2, transfer);
}

export function transferCallAgent(type: number, transferInfo: { agent?: any; acd?: any }) {
  if (!csVoice.isAnswering) {
    csTransferCallError(getErrorMessage('IPCCERR0014'));
    return;
  }
  if (!csVoice.callInfo) {
    csTransferCallError(getErrorMessage('IPCCERR0015'));
    return;
  }
  if (type) {
    if (type === 1) {
      if (transferInfo.agent !== '') {
        const mess = {
          callId: csVoice.callInfo.callId,
          newAgentId: transferInfo.agent.ipPhone,
          dropAgentId: csVoice.callInfo.agentId,
          objectId: actionTypes.TRANSFER_AGENT_TO_AGENT_CONFIRM_REQUEST_ID
        };
        sendAs(mess);
      }
    } else {
      if (transferInfo.acd !== '') {
        const mess = {
          callId: csVoice.callInfo.callId,
          acdId: transferInfo.acd.queueId,
          objectId: actionTypes.TRANSFER_TO_ACD_REQUEST_ID
        };
        sendAs(mess);
      }
    }
  } else {
    const errorMess = getErrorMessage('IPCCERR0012');
    csTransferCallError(errorMess);
  }
}
