import axios, { AxiosResponse, AxiosError, AxiosInstance, AxiosPromise } from 'axios';
import { getItem } from 'utils/localStorage';
import Qs from 'qs';
import { openNotificationWithIcon } from './helper';

const onSuccessInterceptorRequest = async config => {
  const token = await getItem('token');
  if (token) config.headers['token'] = `${token}`;
  config.paramsSerializer = (params: any) =>
    Qs.stringify(params, {
      arrayFormat: 'brackets',
      encode: false
    });
  return config;
};
const onErrorInterceptorRequest = (error: AxiosError) => Promise.reject(error);

const onErrorInterceptorResponse = (error: AxiosError) => {
  if (error.response && error.response.status) {
    if (error.response.status !== 200) {
      openNotificationWithIcon('error', 'Rất tiếc!', 'Error', 3, 'topRight');
    }
  }
  return Promise.reject(error);
};
const onSuccessInterceptorResponse = (response: AxiosResponse) => response;

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';

const _axios: AxiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL || '',
  timeout: 120 * 1000
  // withCredentials: true, // Check cross-site Access-Control
});

_axios.interceptors.request.use(onSuccessInterceptorRequest, onErrorInterceptorRequest);

_axios.interceptors.response.use(onSuccessInterceptorResponse, onErrorInterceptorResponse);

export function $get(url: string, params?: object): AxiosPromise {
  return _axios.get(url, {
    params
  });
}

export function $post(url: string, config?: object | string): AxiosPromise {
  return _axios.post(url, config);
}

export function $put(url: string, config?: object | string): AxiosPromise {
  return _axios.put(url, config);
}

export function $delete(url: string, config: object | string): AxiosPromise {
  return _axios.delete(url, { data: config });
}

export default _axios;
