const addLeadingZero = (value?: string | number) => (value ? ('0' + value).slice(-2) : '');

export const formatDateTyping = (key?: string, date?: string, separator = '/') => {
  if (!date) return '';

  if (key === 'Backspace' || (isNaN(Number(key)) && key !== separator)) return date;

  let result = date;
  const month30days = [4, 6, 9, 11];
  let [day, month, year] = date.split(separator);

  let parseDay = Number(day);
  let parseMonth = Number(month);
  let parseYear = Number(year);

  console.log('result', parseDay, parseMonth, parseYear);

  const isLeapYear = !!parseYear && parseYear % 4 === 0;
  const minDay = 1;
  const maxDay = month30days.includes(parseMonth) ? 30 : parseMonth === 2 ? (isLeapYear ? 29 : 28) : 31;
  const minMonth = 1;
  const maxMonth = 12;
  const maxYear = new Date().getFullYear();
  const minYear = 1900;

  if (day?.length >= 2) {
    if (parseDay > maxDay) {
      day = `${maxDay}`;
    } else if (parseDay < minDay) {
      day = `${addLeadingZero(minDay)}`;
    }
  } else if (day?.length === 1) {
    if (key === separator) {
      day = day && `${addLeadingZero(day)}`;
    }
  }

  if (month?.length >= 2) {
    if (parseMonth > maxMonth) {
      month = `${maxMonth}`;
    } else if (parseMonth < minMonth) {
      month = `${addLeadingZero(minMonth)}`;
    }
  } else if (month?.length === 1) {
    if (key === separator) {
      month = month && `${addLeadingZero(month)}`;
    }
  }

  if (year?.length >= 4) {
    if (parseYear > maxYear) {
      year = `${maxYear}`;
    } else if (parseYear < minYear) {
      year = `${minYear}`;
    }
  }

  console.log('result >', day, month, year);

  const firstSeparator = day?.length >= 2 || (key === separator && day?.length === 1) || month ? '/' : '';
  const secondSeparator = month?.length >= 2 || (key === separator && month?.length === 1) || year ? '/' : '';

  result = (day || '') + firstSeparator + (month || '') + secondSeparator + (year || '');

  return result;
};
