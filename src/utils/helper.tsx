import React from 'react';
import { notification, message, Modal, Tag } from 'antd';
import { WarningOutlined } from '@ant-design/icons';
const { confirm } = Modal;

type NotificationPlacement = 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight';

function hasKey<O>(obj: O, key: string | number | symbol): key is keyof O {
  return key in obj;
}
export const openNotificationWithIcon = (
  type: string,
  message?: string,
  description?: string,
  duration?: number,
  placement?: NotificationPlacement
) => {
  let config: any = {
    message: message ? message : '',
    description: description ? description : ''
  };
  notification.config({
    duration: duration && duration > 1 ? duration : 2,
    placement: placement ? placement : 'topRight',
    closeIcon: true
  });
  hasKey(notification, type) && notification[type](config);
};

export const openConfirmModal = (
  onOk: () => void,
  title: string = 'Cảnh báo',
  description: string = 'Bạn có chắc muốn thực hiện hành động này?',
  onCancel: () => void = () => {},
  okButtonText: string = 'Đồng ý',
  cancelButtonText: string = 'Huỷ bỏ'
) => {
  confirm({
    title: title,
    icon: <WarningOutlined />,
    content: description,
    okText: okButtonText,
    cancelText: cancelButtonText,
    onOk: () => onOk(),
    onCancel: () => onCancel()
  });
};

export const convertPhoneNumber = (str?: string, index?: number, replacement?: string) => {
  let result = str?.trim();
  if (result) {
    if (!result.startsWith('0') && !result.startsWith('84') && !result.startsWith('1900')) {
      result = '0' + result;
    }

    if (index != null && replacement != null) {
      return result.substr(0, index) + replacement + result.substr(index + replacement.length);
    } else {
      return result;
    }
  } else {
    return '';
  }
};

export const isEmptyObj = obj => {
  for (var prop in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, prop)) {
      return false;
    }
  }

  return JSON.stringify(obj) === JSON.stringify({});
};
