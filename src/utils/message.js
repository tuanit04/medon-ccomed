export const messageError = message => {
  message.error({
    content: message,
    className: 'custom-class',
    style: {
      marginTop: '20vh'
    }
  });
};
