export const findRecursive = <T extends { children?: T[] }, V = string>(
  list?: T[],
  searchField?: keyof T,
  searchValue?: V
) => {
  if (list == null || searchValue == null) {
    return;
  }
  let result: T | undefined;

  for (const item of list) {
    if (result != null) {
      break;
    }

    if (searchField && item[searchField as string] === searchValue) {
      result = item;
    } else {
      result = findRecursive(item.children as T[], searchField, searchValue);
    }
  }

  return result;
};

export const getMap = (menu?: any[], key = "id"): Record<string, any> => {
  return menu?.reduce((acc, val) => {
    const children = val.children ? getMap(val.children) : []

    return {
      ...acc,
      [val[key]]: { ...val, children }
    }
  }, {})
}
