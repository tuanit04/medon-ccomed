import { NotificationOutlined } from '@ant-design/icons';
import { notification } from 'antd';

/*export const openNotificationRight = message => {
  notification.open({
    message: 'Thông báo',
    style: { background: '#1c7cff' },
    description: message,
    icon: <NotificationOutlined type="smile" style={{ color: '#ffffff' }} />
  });
};*/
const getNotificationStyle = type => {
  return {
    success: {
      color: 'rgba(0, 0, 0, 0.65)',
      border: '1px solid #b7eb8f',
      backgroundColor: '#f6ffed'
    },
    warning: {
      color: 'rgba(0, 0, 0, 0.65)',
      border: '1px solid #ffe58f',
      backgroundColor: '#fffbe6'
    },
    error: {
      color: 'rgba(0, 0, 0, 0.65)',
      border: '1px solid #ffa39e',
      backgroundColor: '#fff1f0'
    },
    info: {
      color: 'rgba(0, 0, 0, 0.65)',
      border: '1px solid #91d5ff',
      backgroundColor: '#e6f7ff'
    }
  }[type];
};

export const openNotificationRight = (message, type = 'info') => {
  notification['info']({
    message: 'Thông báo',
    description: message,
    style: getNotificationStyle(type),
    duration: 3
  });
};
