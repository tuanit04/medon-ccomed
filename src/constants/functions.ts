export const functionCodeConstants = {
  TD_DM: 'TD_DM', //danh mục
  TD_DM_NHOMBACSI: 'TD_DM_NhomBacSi', //danh mục / nhóm bác sĩ
  TD_DM_BACSI: 'TD_DM_BacSi', //danh mục /  bác sĩ
  TD_DM_DICHVU: 'TD_DM_DichVu', // danh mục / dịch vụ
  TD_DM_DICHVU_TIMKIEM_THONGKE: 'TD_DM_DICHVU_TIMKIEM_THONGKE', // danh mục / dịch vụ / Tìm kiếm, thống kê
  TD_DM_GOIKHAM: 'TD_DM_GoiKham', //danh mục  / gói khám
  TD_DM_GOIKHAM_TIMKIEM_THONGKE: 'TD_DM_GOIKHAM_TIMKIEM_THONGKE', //danh mục  / gói khám / Tìm kiếm, thống kê
  TD_DM_CBTAINHA: 'TD_DM_CBTaiNha', // danh mục / cán bộ tại nhà
  TD_DM_CBTAINHA_TIMKIEM: 'TD_DM_CBTaiNha_TimKiem', //danh mục / cán bộ tại nhà / tìm kiếm
  TD_DM_CBTAINHA_UPDATE: 'TD_DM_CBTAINHA_UPDATE', // danh mục / cán bộ tại nhà / cập nhật
  TD_DM_CBTAINHA_CREATE: 'TD_DM_CBTAINHA_CREATE', // danh mục / cán bộ tại nhà / Thêm mới
  TD_DM_CBTAINHA_ACTIVE_INACTIVE: 'TD_DM_CBTAINHA_ACTIVE_INACTIVE', // danh mục / cán bộ tại nhà / Kích hoạt, hủy kích hoạt
  TD_DM_CBTAINHA_DELETE: 'TD_DM_CBTAINHA_DELETE', // danh mục / cán bộ tại nhà / Xóa
  TD_DM_LLV: 'TD_DM_LichLamViec', // danh mục / lịch làm việc
  TD_DM_LLV_CREATE: 'TD_DM_LLV_CREATE', // Danh mục / lịch làm việc/ Tạo lịch làm việc
  TD_DM_LLV_COPY_WORK_SCHEDULE: 'TD_DM_LLV_COPY_WORK_SCHEDULE', // Danh mục / lịch làm việc/ Copy lịch làm việc
  TD_DM_LLV_DELETE_ALL_WORK_SCHEDULE_OF_DAY: 'TD_DM_LLV_DELETE_ALL_WORK_SCHEDULE_OF_DAY', // Danh mục / lịch làm việc/ xóa lịch làm việc
  TD_DM_LLV_TIMKIEM_THONGKE: 'TD_DM_LLV_TIMKIEM_THONGKE', //danh mục / cán bộ tại nhà / tìm kiếm
  TD_DM_CD: 'TD_DM_CungDuong', // danh mục / cung đường
  TD_DM_CD_TIMKIEM_THONGKE: 'TD_DM_CD_TIMKIEM_THONGKE', // danh mục / cung đường
  TD_DM_CD_UPDATE: 'TD_DM_CD_UPDATE', // danh mục / cung đường / Cập nhật cung đường
  TD_DM_CD_CREATE: 'TD_DM_CD_CREATE', // danh mục / cung đường / thêm mới cung đường
  TD_DM_CD_ACTIVE_INACTIVE: 'TD_DM_CD_ACTIVE_INACTIVE', // danh mục / cung đường / Kích hoạt/Hủy kích hoạt cung đường
  TD_DM_CD_DELETE: 'TD_DM_CD_DELETE', // danh mục / cung đường / Xóa cung đường
  TD_DM_DP: 'TD_DM_DuongPho', // danh mục / Đường phố
  TD_DM_DP_TIMKIEM_THONGKE: 'TD_DM_DP_TIMKIEM_THONGKE', // danh mục / đường phố
  TD_DM_DP_UPDATE: 'TD_DM_CD_UPDATE', // danh mục / đường phố / Cập nhật đường phố
  TD_DM_DP_CREATE: 'TD_DM_CD_CREATE', // danh mục / đường phố / Thêm mới đường phố
  TD_DM_DP_ACTIVE_INACTIVE: 'TD_DM_CD_ACTIVE_INACTIVE', // danh mục / đường phố / Kích hoạt/Hủy kích hoạt đường phố
  TD_DM_DP_DELETE: 'TD_DM_CD_DELETE', // danh mục / đường phố / Xóa đường phố
  TD_QLTK: 'TD_QLTK', // Quản lý Ticket
  TD_QLTK_DS: 'TD_QLTK_DS', // Quản lý Ticket/Danh sách Ticket
  TD_QLTK_DS_THEMMOI_CAPNHAT: 'TD_QLTK_DS_THEMMOI_CAPNHAT', // Quản lý Ticket/Danh sách Ticket/Thêm mới và cập nhật
  TD_QLTK_DS_XULY: 'TD_QLTK_DS_XULY', // Quản lý Ticket/Danh sách Ticket/Xử lý Ticket
  TD_QLTK_DS_HUY: 'TD_QLTK_DS_HUY', // Quản lý Ticket/Danh sách Ticket/Huỷ Ticket
  TD_QLTK_DS_XOA: 'TD_QLTK_DS_XOA', // Quản lý Ticket/Danh sách Ticket/Xoá Ticket
  TD_QLTK_DS_EXPORT_EXCEL: 'TD_QLTK_DS_EXPORT_EXCEL', // Quản lý Ticket/Danh sách Ticket/Xuất Excel
  TD_QLTK_QLSLA: 'TD_QLTK_QLSLA', // Quản lý Ticket/Quản lý SLA
  TD_QLTK_QLSLA_CAPNHAT: 'TD_QLTK_QLSLA_CAPNHAT', // Quản lý Ticket/Quản lý SLA/Cập nhật
  TD_QLTK_PLTK: 'TD_QLTK_PLTK', // Quản lý Ticket/Phân loại Ticket
  TD_QLTK_PLTK_THEMMOI_CAPNHAT: 'TD_QLTK_PLTK_THEMMOI_CAPNHAT', // Quản lý Ticket/Phân loại Ticket/Thêm mới và cập nhật
  TD_QLTK_PLTK_XOA: 'TD_QLTK_PLTK_XOA', // Quản lý Ticket/Phân loại Ticket/Xoá
  TD_QLTK_NHOMXL: 'TD_QLTK_NHOMXL', // Quản lý Ticket/Nhóm xử lý
  TD_QLTK_NHOMXL_THEMMOI_CAPNHAT: 'TD_QLTK_NHOMXL_THEMMOI_CAPNHAT', // Quản lý Ticket/Nhóm xử lý/Thêm mới và cập nhật
  TD_QLTK_NHOMXL_XOA: 'TD_QLTK_NHOMXL_XOA', // Quản lý Ticket/Nhóm xử lý/Xoá
  TD_QLLH: 'TD_QLLH', // Quản lý lịch hẹn
  TD_QLLH_LH: 'TD_QLLH_LichHen', // Quản lý lịch hẹn / lịch hẹn
  TD_QLLH_LH_COVID: 'TD_QLLH_LH_COVID', // Quản lý lịch hẹn / lịch hẹn covid
  TD_LH_TIMKIEM: 'TD_LH_TimKiem', //Quản lý lịch hẹn / lịch hẹn / tìm kiếm
  TD_LH_TIMKIEM_COVID: 'TD_LH_TIMKIEM_COVID', //Quản lý lịch hẹn / lịch hẹn / tìm kiếm lịch covid
  TD_LH_HUY_PL_COVID: 'TD_LH_HUY_PL_COVID', //Quản lý lịch hẹn / lịch hẹn / tìm kiếm lịch covid
  TD_LH_BOLOC: 'TD_LH_BoLoc', //Quản lý lịch hẹn / lịch hẹn / bỏ lọc
  TD_LH_BOLOC_COVID: 'TD_LH_BOLOC_COVID', //Quản lý lịch hẹn / lịch hẹn / bỏ lọc
  TD_LH_TLH: 'TD_LH_ThemLH', //Quản lý lịch hẹn / lịch hẹn / thêm lịch hẹn
  TD_LH_TLH_COVID: 'TD_LH_TLH_COVID', //Quản lý lịch hẹn / lịch hẹn / thêm lịch hẹn covid
  TD_LH_SUALH_COVID: 'TD_LH_SUALH_COVID', //Quản lý lịch hẹn / lịch hẹn / thêm lịch hẹn covid
  TD_LH_XNLH_COVID: 'TD_LH_XNLH_COVID', //Quản lý lịch hẹn / lịch hẹn / Xác nhận lịch hẹn covid
  TD_LH_TNCN: 'TD_LH_TNCN', //Quản lý lịch hẹn / lịch hẹn / cập nhật tin nhắn
  TD_LH_CBS: 'TD_LH_CBS', //Quản lý lịch hẹn / lịch hẹn / chuyển bác sĩ tư vấn
  TD_LH_CHON_BSCTV: 'TD_LH_CHON_BSCTV', //Quản lý lịch hẹn / lịch hẹn / Chọn BS CTV
  TD_LH_CHON_GOI_KHAM: 'TD_LH_CHON_GOI_KHAM', //Quản lý lịch hẹn / lịch hẹn / Chọn gói khám
  TD_LH_CHON_XET_NGHIEM: 'TD_LH_CHON_XET_NGHIEM', //Quản lý lịch hẹn / lịch hẹn / Chọn xét nghiệm
  TD_LH_XOA: 'TD_LH_XOA', //Quản lý lịch hẹn / lịch hẹn / xóa lịch hẹn
  TD_LH_HUY: 'TD_LH_HUY', //Quản lý lịch hẹn / lịch hẹn / hủy lịch hẹn
  TD_LH_CHUYEN_LOAI_LICH: 'TD_LH_CHUYEN_LOAI_LICH', //Quản lý lịch hẹn / lịch hẹn / chuyển loại lịch hẹn
  TD_LH_HUY_COVID: 'TD_LH_HUY_COVID', //Quản lý lịch hẹn / lịch hẹn / hủy lịch hẹn covid
  TD_LH_DATNHIEU: 'TD_LH_DATNHIEU', //Quản lý lịch hẹn / lịch hẹn / đặt nhiều lịch
  TD_LH_SUALH: 'TD_LH_SuaLH', //Quản lý lịch hẹn / lịch hẹn / sửa lịch hẹn
  TD_QLLH_PL: 'TD_QLLH_PhanLich', // Quản lý lịch hẹn / Phân lịch
  TD_QLLH_GTN: 'TD_QLLH_GuiTinNhan', // Quản lý lịch hẹn / Gửi tin nhắn
  TD_QLLH_GE: 'TD_QLLH_GuiEmail', // Quản lý lịch hẹn / Gửi Email
  TD_TUVAN: 'TD_TUVAN', // Quản lý tư vấn
  TD_TUVAN_DAUVAO: 'TD_TUVAN_DauVao', // Quản lý tư vấn / Tư vấn đầu vào
  TD_TUVAN_DAUVAO_HUYTUVAN: 'TD_TUVAN_DAUVAO_HUYTUVAN', // Quản lý tư vấn / Tư vấn đầu vào/ Huy tu van
  TD_TUVAN_DAUVAO_CBS: 'TD_TUVAN_DAUVAO_CBS', //Quản lý tư vấn / Tư vấn đầu vào / chuyển bác sĩ tư vấn
  TD_TUVAN_DAUVAO_XOA: 'TD_TUVAN_DAUVAO_XOA', //Quản lý tư vấn / Tư vấn đầu vào / xóa lịch hẹn
  TD_TUVAN_DAUVAO_HUY: 'TD_TUVAN_DAUVAO_HUY', //Quản lý tư vấn / Tư vấn đầu vào / hủy lịch hẹn
  TD_TUVAN_DAUVAO_DATNHIEU: 'TD_TUVAN_DAUVAO_DATNHIEU', //Quản lý tư vấn / Tư vấn đầu vào / đặt nhiều lịch
  TD_TUVAN_DAUVAO_SUALH: 'TD_TUVAN_DAUVAO_SUALH', //Quản lý tư vấn / Tư vấn đầu vào / sửa lịch hẹn
  TD_TUVAN_DAUVAO_TIMKIEM: 'TD_TUVAN_DAUVAO_TIMKIEM', //Quản lý tư vấn / Tư vấn đầu vào / tìm kiếm
  TD_TUVAN_DAUVAO_CHON_BSCTV: 'TD_TUVAN_DAUVAO_CHON_BSCTV', //Quản lý tư vấn / Tư vấn đầu vào/ Chọn BS CTV
  TD_TUVAN_DAUVAO_CHON_GOI_KHAM: 'TD_TUVAN_DAUVAO_CHON_GOI_KHAM', //Quản lý tư vấn / Tư vấn đầu vào / Chọn gói khám
  TD_TUVAN_DAUVAO_CHON_XET_NGHIEM: 'TD_TUVAN_DAUVAO_CHON_XET_NGHIEM', //Quản lý tư vấn / Tư vấn đầu vào / Chọn xét nghiệm
  TD_CUOCGOI: 'TD_CUOCGOI', // Quản lý cuộc gọi
  TD_CUOCGOI_DSCG: 'TD_CUOCGOI_DSCG', // Quản lý cuộc gọi / Danh sách cuộc gọi
  TD_CUOCGOI_DSCBTD: 'TD_CUOCGOI_DSCBTD', // Quản lý cuộc gọi / Danh sách cán bộ tổng đài
  TD_CUOCGOI_THCG: 'TD_CUOCGOI_THCG', // Thực hiện cuộc gọi

  TD_TUVAN_DAURA: 'TD_TUVAN_DauRa', // Quản lý tư vấn / Tư vấn đầu ra
  TD_TUVAN_KETQUA: 'TD_TUVAN_KetQua', // Quản lý tư vấn / Kết quả khám
  TD_BAOCAO: 'TD_BAOCAO', //Báo cáo, thống kê
  TD_BAOCAO_LHTH: 'TD_BAOCAO_LHTH', //Báo cáo, thống kê / Lịch hẹn thực hiện
  TD_BAOCAO_GKTH: 'TD_BAOCAO_GKTH', //Báo cáo, thống kê / Gói khám thực hiện
  TD_BAOCAO_THDTDL: 'TD_BAOCAO_THDTDL', //Báo cáo, thống kê / Tổng hợp doanh thu đặt lịch
  TD_BAOCAO_CHITIETDTDL: 'TD_BAOCAO_CHITIETDTDL', //Báo cáo, thống kê / Chi tiết doanh thu đặt lịch
  TD_BAOCAO_THDTGK: 'TD_BAOCAO_THDTGK', //Báo cáo, thống kê / Tổng hợp doanh thu gói khám
  TD_BAOCAO_CTDTGK: 'TD_BAOCAO_CTDTGK', //Báo cáo, thống kê / Chi tiết doanh thu gói khám
  TD_BAOCAO_LHTV: 'TD_BAOCAO_LHTV',
  TD_KHACHHANG: 'TD_KHACHHANG',
  TD_KHACHHANG_DS: 'TD_KHACHHANG_DS',
  COVID_NC_DS: 'COVID_NC_DS', //Danh sách coTD_LH_XOA: 'TD_LH_XOA', //Quản lý lịch hẹn / lịch hẹn / xóa lịch hẹn
  TD_LH_XOA_COVID: 'TD_LH_XOA_COVID' //danh sach lich hen covid,// xoa lich hen covid
};
