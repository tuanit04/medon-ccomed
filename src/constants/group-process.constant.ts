//Trạng thái GroupProcess
export const GROUP_PROCESS_STATUS = {
  ALL: {
    key: '',
    value: '-- Tất cả --'
  },
  ACTIVE: {
    key: 1,
    value: 'Kích hoạt'
  },
  INACTIVE: {
    key: 2,
    value: 'Không kích hoạt'
  }
};
