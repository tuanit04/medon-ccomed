export const WORK_SESSION_COLOR = {
  MORNING: 'green',
  AFTER_NOON: 'gold',
  NIGHT: 'purple',
  DAY_LONG: 'blue'
};
