export const wardTypes = {
  FETCH_WARDS_START: 'FETCH_WARDS_START',
  SET_WARDS: 'SET_WARDS',
  FETCH_WARD_START: 'FETCH_WARD_START',
  SET_WARD: 'SET_WARD'
};
