export const CRM_TYPES = [
  {
    value: 'NEW',
    name: 'New - KH mới'
  },
  {
    value: 'LEAD',
    name: 'Lead - KH tiềm năng'
  },
  {
    value: 'CONTACT',
    name: 'Contact - KH cũ'
  }
];
