export const districtTypes = {
  FETCH_DISTRICTS_START: 'FETCH_DISTRICTS_START',
  SET_DISTRICTS: 'SET_DISTRICTS',
  FETCH_DISTRICTS_BY_PROVINCE_ID_START: 'FETCH_DISTRICTS_BY_PROVINCE_ID_START',
  SET_DISTRICTS_BY_PROVINCE_ID: 'SET_DISTRICTS_BY_PROVINCE_D'
};
