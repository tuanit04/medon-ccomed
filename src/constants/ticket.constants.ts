export const TICKET_STATUS = {
  WAITING: {
    key: 2,
    value: 'Chờ xử lý'
  },
  PROCESSING: {
    key: 3,
    value: 'Đang xử lý'
  },
  RESOLVED: {
    key: 4,
    value: 'Đã xử lý'
  },
  COMPLETED: {
    key: 5,
    value: 'Hoàn thành'
  },
  CANCEL: {
    key: 1,
    value: 'Hủy'
  }
};

export const TICKET_STATISTIC = {
  OTHER: {
    key: 1,
    lable: 'OTHER',
    value: 'Tất cả'
  },
  CANCEL: {
    key: 7,
    lable: 'CANCEL',
    value: 'Hủy'
  },
  COMPLETED: {
    key: 6,
    lable: 'COMPLETED',
    value: 'Hoàn thành'
  },
  WAITING: {
    key: 2,
    lable: 'WAITING',
    value: 'Chờ xử lý'
  },
  ME: {
    key: 3,
    lable: 'ME',
    value: 'Phân cho tôi'
  },
  MY_GROUP: {
    key: 4,
    lable: 'MY_GROUP',
    value: 'Phân cho nhóm của tôi'
  },
  OVER_SLA: {
    key: 8,
    lable: 'OVER_SLA',
    value: 'Quá SLA'
  }
};

//Mức độ Ticket
export const TICKET_LEVEL = {
  LOW: {
    key: 'LOW',
    value: 'Thấp'
  },
  HIGH: {
    key: 'HIGH',
    value: 'Cao'
  },
  MEDIUM: {
    key: 'MEDIUM',
    value: 'Trung bình'
  }
};

//Nhóm khách hàng
export const PATIENT_GROUP = {
  CUS_PERSONAL: {
    key: 'CUS_PERSONAL',
    value: 'Khách hàng lẻ'
  }, //KH lẻ
  CUS_COLLABORATORS: {
    key: 'CUS_COLLABORATORS',
    value: 'Khách hàng CTV'
  }, //KH CTV
  CUS_WARD: {
    key: 'CUS_WARD',
    value: 'Khách hàng phường'
  }, //KH  phường
  CUS_HEALTH_EXAM: {
    key: 'CUS_HEALTH_EXAM',
    value: 'Khách hàng khám sức khỏe'
  } //kh ksk
};

//Loại khách hàng
export const PATIENT_TYPE = {
  NEW: {
    key: 'NEW',
    value: 'NEW - Khách hàng mới'
  },
  LEAD: {
    key: 'LEAD',
    value: 'LEAD - Khách hàng tiềm năng'
  },
  CONTACT: {
    key: 'CONTACT',
    value: 'CONTACT - Khách hàng cũ'
  }
};

//Xử lý Ticket
//Loại chi phí
export const COST_TYPE = {
  MOVING_COST: {
    key: 'MOVING_COST',
    value: 'Chi phí di chuyển'
  },
  COMPETITIVE_COST: {
    key: 'COMPETITIVE_COST',
    value: 'Chi phí đền bù KCB'
  },
  VOUCHER: {
    key: 'VOUCHER',
    value: 'Voucher'
  },
  VISIT_COST: {
    key: 'VISIT_COST',
    value: 'Chi phí thăm hỏi (Hoa quả, đường sữa ...)'
  },
  OTHER_COST: {
    key: 'OTHER_COST',
    value: 'Chi phí khác (Ăn uống ...)'
  }
};
// Đánh giá mức độ quan trọng
export const IMPORTANCE_LEVEL = {
  IMPOTANT: {
    key: 'IMPOTANT',
    value: 'Quan trọng'
  },
  ORDINARY: {
    key: 'ORDINARY',
    value: 'Bình thường'
  }
};
// Phản hồi KH
export const CUSTOMER_FEEDBACK = {
  NORMAL: {
    key: 'NORMAL',
    value: 'Bình thường'
  },
  GOOD: {
    key: 'GOOD',
    value: 'Hài lòng'
  },
  NOTGOOD: {
    key: 'NOTGOOD',
    value: 'Không hài lòng'
  }
};
//Phân loại ý kiến
export const OPINION_TYPE = {
  PRAISE: {
    key: 'PRAISE',
    value: 'Khen'
  },
  FEEDBACK: {
    key: 'FEEDBACK',
    value: 'Góp ý'
  },
  REFLECT: {
    key: 'REFLECT',
    value: 'Phản ánh'
  },
  TROUBLE: {
    key: 'TROUBLE',
    value: 'Sự cố'
  }
};
//Phương thức liên hệ
export const CONTACT_METHOD = {
  CONTACT_SUCCESS: {
    key: 'CONTACT_SUCCESS',
    value: 'Đã liên lạc được'
  },
  CONTACT_FAILURE: {
    key: 'CONTACT_FAILURE',
    value: 'Không liên lạc được'
  }
};
