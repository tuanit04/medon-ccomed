//Trạng thái Lịch hẹn
export const APPOINTMENT_STATUS = {
  CHO_XAC_NHAN: {
    key: 1,
    value: 'Chờ xác nhận'
  },
  DA_XAC_NHAN: {
    key: 2,
    value: 'Đã xác nhận'
  },
  CHO_TU_VAN_DAU_VAO: {
    key: 3,
    value: 'Chờ tư vấn đầu vào'
  },
  DA_TU_VAN_DAU_VAO: {
    key: 4,
    value: 'Đã vấn đầu vào'
  },
  CHUA_CHECK: {
    key: 5,
    value: 'Chưa check'
  },
  DA_CHECK: {
    key: 6,
    value: 'Đã check'
  },
  DA_LAY_MAU: {
    key: 7,
    value: 'Đã lấy mẫu'
  },
  DA_CO_KQ: {
    key: 8,
    value: 'Đã có kết quả'
  },
  DA_DANG_KY_KHAM: {
    key: 9,
    value: 'Đã đăng ký khám'
  },
  DA_THANH_TOAN: {
    key: 10,
    value: 'Đã thanh toán'
  },
  DA_KHAM: {
    key: 11,
    value: 'Đã khám'
  },
  DA_HUY: {
    key: 13,
    value: 'Đã huỷ'
  }
};
