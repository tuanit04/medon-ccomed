export * from './alert.constants';
export * from './user.constants';
export * from './facility.constants';
export * from './province.types';
export * from './district.types';
export * from './ward.types';
export * from './response';
export * from './worksession.color.constant';

export const METHOD_TYPE = {
    HOME: 'HOME',
    HOSPITAL: 'HOSPITAL',
    BOTH: 'BOTH'
};

export const SEX = {
    MALE: 'MALE',
    FEMALE: 'FEMALE',
};
  
