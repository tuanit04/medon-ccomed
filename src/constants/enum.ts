export enum CallType {
  INCOMING = 'IN',
  OUTGOING = 'OUT'
}
