export const baseURL = process.env.REACT_APP_BASE_URL ?? '';
export const baseURL_RESTAPI = '10.78.4.207:8009/api/';
export const permissonUrl = process.env.REACT_APP_PERMISSION_URL ?? '';
export const masterEndpoint = (process.env.REACT_APP_MASTER_ENDPOINT ?? '') + '/graphql';
export const appointmentEndpoint = (process.env.REACT_APP_APPOINTMENT_ENDPOINT ?? '') + '/graphql';
export const accountEndpoint = (process.env.REACT_APP_ACCOUNT_ENDPOINT ?? '') + '/graphql';
export const callEndpoint = process.env.REACT_APP_CALL_ENDPOINT ?? '';

export const loginUrl = baseURL + '/auth/login';
export const refreshTokenUrl = baseURL + '/auth/refresh-token';
export const logoutUrl = baseURL + '/auth/logout';
export const taskURL = baseURL + '/tasks';
export const reportHospital = {
  html: process.env.REACT_APP_REPORT_APPOINTMENT + '/html',
  xls: process.env.REACT_APP_REPORT_APPOINTMENT + '/xls',
  pdf: process.env.REACT_APP_REPORT_APPOINTMENT + '/pdf'
};
export const serviceUrl = process.env.REACT_APP_SERVICE_URL ?? '';

export const vccLoginUrl = (domain: string) => `https://web.vcc-vinaphone.com.vn/${domain}/thirdParty/login`;
export const vccAgentListUrl = (process.env.REACT_APP_ACCOUNT_ENDPOINT ?? '') + '/api/v1/account/getUsers';
export const getPatientByPhoneUrl =
  (process.env.REACT_APP_MASTER_ENDPOINT ?? '') + '/api/v1/master/getInfoPatientByPhone';
export const saveCallLogUrl = callEndpoint + '/saveAndUpdateCallLog';
export const saveAgentStatusUrl = callEndpoint + '/saveAgentStatus';
export const getListCallUserUrl = callEndpoint + '/findListUserTD';
export const getListCallUrl = callEndpoint + '/findListCall';
export const getCallStatisticalByLineUrl = callEndpoint + '/getStatisticalByLine';
export const getListCallOngoingUrl = callEndpoint + '/findListUserTDOngoing';
