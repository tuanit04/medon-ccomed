import { functionCodeConstants } from 'constants/functions';
import { MenuList } from 'interface/layout/menu.interface';

const mockMenuList: MenuList = [
  // {
  //   name: 'account',
  //   label: {
  //     zh_CN: userLogined ? 'Hi, ' + userLogined['name'] : '',
  //     en_US: userLogined ? 'Hi, ' + userLogined['name'] : ''
  //   },
  //   icon: 'account',
  //   key: 'userlogined',
  //   path: '/account',
  //   children: [
  //     {
  //       name: 'account-logout',
  //       label: {
  //         zh_CN: 'Đăng xuất',
  //         en_US: 'Đăng xuất'
  //       },
  //       key: 'LOGOUT',
  //       path: '/user/logout',
  //     }
  //   ]
  // },
  {
    name: 'dashboard',
    label: {
      zh_CN: 'Dashboard',
      en_US: 'Dashboard'
    },
    icon: 'dashboard',
    key: '0',
    path: '/dashboard'
  },
  // {
  //   name: 'system',
  //   label: {
  //     zh_CN: 'Hệ thống',
  //     en_US: 'Hệ thống'
  //   },
  //   icon: 'system',
  //   key: '0',
  //   path: '/system',
  //   children: [
  //     {
  //       name: 'account-management',
  //       label: {
  //         zh_CN: 'Quản lý tài khoản',
  //         en_US: 'Quản lý tài khoản'
  //       },
  //       key: '0-0',
  //       path: '/system/account-management'
  //     },
  //     {
  //       name: 'assign-permiss',
  //       label: {
  //         zh_CN: 'Phân quyền',
  //         en_US: 'Phân quyền'
  //       },
  //       key: '0-1',
  //       path: '/system/assign-permiss'
  //     }
  //   ]
  // },
  {
    name: functionCodeConstants.TD_DM,
    label: {
      zh_CN: 'Danh mục',
      en_US: 'Danh mục'
    },
    icon: 'category',
    key: '1',
    path: '/category',
    children: [
      {
        name: functionCodeConstants.TD_DM_NHOMBACSI,
        label: {
          zh_CN: 'Nhóm bác sĩ',
          en_US: 'Nhóm bác sĩ'
        },
        key: '1-0',
        path: '/category/doctors'
      },
      {
        name: functionCodeConstants.TD_DM_BACSI,
        label: {
          zh_CN: 'Bác sĩ',
          en_US: 'Bác sĩ'
        },
        key: '1-1',
        path: '/category/doctor'
      },
      {
        name: functionCodeConstants.TD_DM_DICHVU,
        label: {
          zh_CN: 'Dịch vụ',
          en_US: 'Dịch vụ'
        },
        key: '1-2',
        path: '/category/services'
      },
      {
        name: functionCodeConstants.TD_DM_GOIKHAM,
        label: {
          zh_CN: 'Gói khám',
          en_US: 'Gói khám'
        },
        key: '1-3',
        path: '/category/examination-package'
      },
      {
        name: functionCodeConstants.TD_DM_CBTAINHA,
        label: {
          zh_CN: 'Cán bộ tại nhà',
          en_US: 'Cán bộ tại nhà'
        },
        key: '1-4',
        path: '/category/site-staff'
      },
      {
        name: functionCodeConstants.TD_DM_LLV,
        label: {
          zh_CN: 'Lịch làm việc',
          en_US: 'Lịch làm việc'
        },
        key: '1-5',
        path: '/category/work-schedule'
      },
      {
        name: functionCodeConstants.TD_DM_CD,
        label: {
          zh_CN: 'Cung đường',
          en_US: 'Cung đường'
        },
        key: '1-6',
        path: '/category/route'
      },
      {
        name: functionCodeConstants.TD_DM_DP,
        label: {
          zh_CN: 'Đường phố',
          en_US: 'Đường phố'
        },
        key: '1-7',
        path: '/category/street'
      }
    ]
  },
  {
    name: functionCodeConstants.TD_QLTK,
    label: {
      zh_CN: 'Quản lý Ticket',
      en_US: 'Quản lý Ticket'
    },
    icon: 'ticket',
    key: '7',
    path: '/ticket-management',
    children: [
      {
        name: functionCodeConstants.TD_QLTK_DS,
        label: {
          zh_CN: 'Danh sách Ticket',
          en_US: 'Danh sách Ticket'
        },
        key: '7-0',
        path: '/ticket-management/tickets'
      },
      {
        name: functionCodeConstants.TD_QLTK_QLSLA,
        label: {
          zh_CN: 'Quản lý SLA',
          en_US: 'Quản lý SLA'
        },
        key: '7-1',
        path: '/ticket-management/sla-management'
      },
      {
        name: functionCodeConstants.TD_QLTK_PLTK,
        label: {
          zh_CN: 'Phân loại Ticket',
          en_US: 'Phân loại Ticket'
        },
        key: '7-2',
        path: '/ticket-management/ticket-classification'
      },
      {
        name: functionCodeConstants.TD_QLTK_NHOMXL,
        label: {
          zh_CN: 'Quản lý nhóm xử lý',
          en_US: 'Quản lý nhóm xử lý'
        },
        key: '7-3',
        path: '/ticket-management/group-process'
      }
    ]
  },
  {
    name: functionCodeConstants.TD_QLLH,
    label: {
      zh_CN: 'Quản lý lịch hẹn',
      en_US: 'Quản lý lịch hẹn'
    },
    icon: 'appointment',
    key: '2',
    path: '/appointment-management',
    children: [
      {
        name: functionCodeConstants.TD_QLLH_LH,
        label: {
          zh_CN: 'Lịch hẹn thường',
          en_US: 'Lịch hẹn thường'
        },
        key: '2-0',
        path: '/appointment-management/appointment'
      },
      {
        name: functionCodeConstants.TD_QLLH_LH_COVID,
        label: {
          zh_CN: 'Lịch hẹn COVID',
          en_US: 'Lịch hẹn COVID'
        },
        key: '2-10',
        path: '/appointment-management/appointment-covid'
      },
      {
        name: functionCodeConstants.COVID_NC_DS,
        label: {
          zh_CN: 'COVID nhập cảnh',
          en_US: 'COVID nhập cảnh'
        },
        key: '2-7',
        path: '/appointment-management/appointment-entry'
      },
      {
        name: functionCodeConstants.TD_QLLH_PL,
        label: {
          zh_CN: 'Phân lịch',
          en_US: 'Phân lịch'
        },
        key: '2-3',
        path: '/appointment-management/assign-appointment'
      },
      {
        name: functionCodeConstants.TD_LH_XNLH_COVID,
        label: {
          zh_CN: 'Xác nhận lịch COVID',
          en_US: 'Xác nhận lịch COVID'
        },
        key: '2-2',
        path: '/appointment-management/appointment-covid/confirm'
      },
      {
        name: functionCodeConstants.TD_QLLH_GTN,
        label: {
          zh_CN: 'Gửi tin nhắn',
          en_US: 'Gửi tin nhắn'
        },
        key: '2-4',
        path: '/appointment-management/message'
      },
      {
        name: functionCodeConstants.TD_QLLH_GE,
        label: {
          zh_CN: 'Gửi Email',
          en_US: 'Gửi Email'
        },
        key: '2-5',
        path: '/appointment-management/email'
      }
    ]
  },
  {
    name: functionCodeConstants.TD_TUVAN,
    label: {
      zh_CN: 'Quản lý tư vấn',
      en_US: 'Quản lý tư vấn'
    },
    icon: 'advisory',
    key: '3',
    path: '/advisory-management',
    children: [
      {
        name: functionCodeConstants.TD_TUVAN_DAUVAO,
        label: {
          zh_CN: 'Tư vấn đầu vào',
          en_US: 'Tư vấn đầu vào'
        },
        key: '3-0',
        path: '/advisory-management/advisory-input'
      },
      {
        name: functionCodeConstants.TD_TUVAN_DAURA,
        label: {
          zh_CN: 'Tư vấn đầu ra',
          en_US: 'Tư vấn đầu ra'
        },
        key: '3-1',
        path: '/advisory-management/advisory-output'
      },
      {
        name: functionCodeConstants.TD_TUVAN_KETQUA,
        label: {
          zh_CN: 'Kết quả khám',
          en_US: 'Kết quả khám'
        },
        key: '3-2',
        path: '/advisory-management/examination-results'
      }
    ]
  },
  {
    name: functionCodeConstants.TD_CUOCGOI,
    label: {
      zh_CN: 'Quản lý cuộc gọi',
      en_US: 'Quản lý cuộc gọi'
    },
    key: '5',
    icon: 'call',
    path: '/call-management',
    children: [
      {
        name: functionCodeConstants.TD_CUOCGOI_DSCG,
        key: '5-0',
        label: {
          zh_CN: 'Danh sách cuộc gọi',
          en_US: 'Danh sách cuộc gọi'
        },
        path: '/call-management/call'
      },
      {
        name: functionCodeConstants.TD_CUOCGOI_DSCBTD,
        key: '5-1',
        label: {
          zh_CN: 'Danh sách cán bộ tổng đài',
          en_US: 'Danh sách cán bộ tổng đài'
        },
        path: '/call-management/user'
      }
    ]
  },
  {
    name: functionCodeConstants.TD_BAOCAO,
    label: {
      zh_CN: 'Báo cáo, thống kê',
      en_US: 'Báo cáo, thống kê'
    },
    icon: 'report',
    key: '4',
    path: '/report',
    children: [
      {
        name: functionCodeConstants.TD_BAOCAO_LHTH,
        label: {
          zh_CN: 'Lịch hẹn thực hiện',
          en_US: 'Lịch hẹn thực hiện'
        },
        key: '4-0',
        path: '/report/appointment'
      },
      {
        name: functionCodeConstants.TD_BAOCAO_GKTH,
        label: {
          zh_CN: 'Gói khám thực hiện',
          en_US: 'Gói khám thực hiện'
        },
        key: '4-1',
        path: '/report/medical-package'
      },
      {
        name: functionCodeConstants.TD_BAOCAO_THDTDL,
        label: {
          zh_CN: 'Tổng hợp doanh thu đặt lịch',
          en_US: 'Tổng hợp doanh thu đặt lịch'
        },
        key: '4-2',
        path: '/report/booking-revenue'
      },
      {
        name: functionCodeConstants.TD_BAOCAO_CHITIETDTDL,
        label: {
          zh_CN: 'Chi tiết doanh thu đặt lịch',
          en_US: 'Chi tiết doanh thu đặt lịch'
        },
        key: '4-3',
        path: '/report/booking-revenue-detail'
      },
      {
        name: functionCodeConstants.TD_BAOCAO_THDTGK,
        label: {
          zh_CN: 'Tổng hợp doanh thu gói khám',
          en_US: 'Tổng hợp doanh thu gói khám'
        },
        key: '4-4',
        path: '/report/examination-package-revenue'
      },
      {
        name: functionCodeConstants.TD_BAOCAO_CTDTGK,
        label: {
          zh_CN: 'Chi tiết doanh thu gói khám',
          en_US: 'Chi tiết doanh thu gói khám'
        },
        key: '4-5',
        path: '/report/examination-package-revenue-detail'
      },
      {
        name: functionCodeConstants.TD_BAOCAO_LHTV,
        label: {
          zh_CN: 'Thống kê lịch hẹn tại bệnh viện/phòng khám',
          en_US: 'Thống kê lịch hẹn tại bệnh viện/phòng khám'
        },
        key: '4-6',
        path: '/report/appointment-hospital'
      }
    ]
  },
  {
    name: functionCodeConstants.TD_KHACHHANG,
    label: {
      zh_CN: 'Khách hàng',
      en_US: 'Khách hàng'
    },
    icon: 'profile',
    key: '8',
    path: '/profile',
    children: [
      {
        name: functionCodeConstants.TD_KHACHHANG_DS,
        label: {
          zh_CN: 'Danh sách khách hàng',
          en_US: 'Danh sách khách hàng'
        },
        key: '8-0',
        path: '/profile/list'
      }
    ]
  }
];

// mock.mock('/user/menu', 'get', intercepter(mockMenuList));

export default mockMenuList;
