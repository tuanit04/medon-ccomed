export const ADD_PROVINCE = 'ADD_PROVINCE';
export const PROVINCE_ADDED = 'PROVINCE_ADDED';

export const DELETE_PROVINCE = 'DELETE_PROVINCE';
export const PROVINCE_DELETED = 'PROVINCE_DELETED';

export const LOAD_PROVINCES = 'LOAD_PROVINCES';
export const PROVINCES_LOADED = 'PROVINCES_LOADED';

export const LOGIN_USER = 'LOGIN_USER';
export const USER_LOGGEDIN = 'USER_LOGGEDIN';

export const LOGOUT_USER = 'LOGOUT_USER';
export const USER_LOGGEDOUT = 'USER_LOGGEDOUT';

export const SET_ALERT = 'SET_ALERT';
export const RESET_ALERT = 'RESET_ALERT';
