// Function to add our give data into cache
export const addDataIntoCache = (cacheName, key, data) => {
  // Converting our respons into Actual Response form
  const dataSaved = new Response(JSON.stringify(data));
  if ('caches' in window) {
    // Opening given cache and putting our data into it
    caches.open(cacheName).then(cache => {
      cache.put(key, dataSaved);
      alert('Data Added into cache!');
    });
  }
};

// Function to get single cache data
export const getSingleCacheData = async (cacheName, url) => {
  let cacheData = null;
  if (typeof caches === 'undefined') return false;

  const cacheStorage = await caches.open(cacheName);
  const cachedResponse: any = await cacheStorage.match(url);

  // If no cache exists
  if (!cachedResponse || !cachedResponse.ok) {
    return cacheData;
  }
  cachedResponse.json().then(item => {
    return item;
  });
};
