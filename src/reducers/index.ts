import { AnyAction, combineReducers } from 'redux';

import { connectRouter, RouterState } from 'connected-react-router';
import { authentication } from './authentication.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { facility } from './facility.reducer';
import { authReducer, userReducer } from '../store/user.store';
import tagsViewReducer from '../store/tags-view.store';
import dialerReducer from '../store/dialer.store';
import { Reducer } from 'react';
import history from 'utils/history';

const rootReducer = combineReducers({
  auth: authReducer,
  authentication,
  users,
  alert,
  facility,
  user: userReducer,
  tagsView: tagsViewReducer,
  dialer: dialerReducer,
  router: connectRouter(history) as Reducer<RouterState, AnyAction>
});

export default rootReducer;
