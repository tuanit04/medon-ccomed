import { facilityConstants } from '../constants/facility.constants';

const INITIAL_STATE = {
  facilities: [],
  facility: {}
};

export const facility = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case facilityConstants.SET_FACILITIES:
      return {
        ...state
        //facilities: action.payload
      };
    case facilityConstants.SET_FACILITY:
      return {
        ...state,
        facility: action.payload
      };
    default:
      return state;
  }
};
