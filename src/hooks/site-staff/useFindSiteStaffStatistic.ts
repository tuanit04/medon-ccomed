import { gql, useQuery } from '@apollo/client';

const FIND_SITE_STAFF_STATISTIC = gql`
  query findSiteStaffStatistic @api(name: "masterEndpoint") {
    findSiteStaffStatistic {
      code
      message
      data {
        totalRecords
        activeRecords
        inactiveRecords
      }
    }
  }
`;

export const useFindSiteStaffStatistic = () => {
  const { data, loading, error, refetch } = useQuery(FIND_SITE_STAFF_STATISTIC);
  return {
    siteStaffStatistic: data?.findSiteStaffStatistic.data || {},
    isLoadingSiteStaffStatistic: loading,
    errorSiteStaffStatistic: error?.graphQLErrors,
    refetchSiteStaffStatistic: refetch
  };
};
