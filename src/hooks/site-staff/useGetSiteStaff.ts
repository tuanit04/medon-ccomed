import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_SITE_STAFFS = gql`
  query siteStaffs($page: Int, $pageSize: Int, $filtered: [FilteredInput]) @api(name: "masterEndpoint") {
    siteStaffs(page: $page, pageSize: $pageSize, filtered: $filtered) {
      code
      message
      data {
        id
        code
        name
        phone
        status
        facilityName
        hisUserName
        parentFacilityName
        parentFacilityId
        isBotMessage
        facilityId
        updateUser
        updateDate
      }
      page
      pages
      records
    }
  }
`;

const GET_SITE_STAFF_DETAIL = gql`
  query siteStaff($siteStaffId: String!) @api(name: "masterEndpoint") {
    siteStaff(siteStaffId: $siteStaffId) {
      code
      message
      data {
        id
        code
        name
        status
        phone
        facilityId
        parentFacilityId
        hisUserName
      }
    }
  }
`;

export const useGetSiteStaffs = variables => {
  const { data, loading, error, refetch } = useQuery(GET_SITE_STAFFS, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    pagingSiteStaffs: data?.siteStaffs || {},
    isLoadingSiteStaffs: loading,
    errorSiteStaffs: error?.graphQLErrors,
    refetchSiteStaffs: refetch
  };
};

export const useGetSiteStaffsLazy = () => {
  const [loadSiteStaffs, { called, loading, data }] = useLazyQuery(GET_SITE_STAFFS, {
    fetchPolicy: 'network-only'
  });
  return {
    siteStaffs: data?.siteStaffs?.data,
    isLoadingSiteStaffs: loading,
    loadSiteStaffs: loadSiteStaffs
  };
};

export const useGetSiteStaffDetail = () => {
  const [loadSiteStaffDetail, { called, loading, data }] = useLazyQuery(GET_SITE_STAFF_DETAIL);
  return {
    siteStaff: data?.siteStaff?.data,
    isLoadingSiteStaff: loading,
    loadSiteStaffDetail: loadSiteStaffDetail
  };
};

export { GET_SITE_STAFFS, GET_SITE_STAFF_DETAIL };
