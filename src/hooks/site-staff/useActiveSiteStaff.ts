import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const ACTIVE_SITE_STAFF = gql`
  mutation activeSiteStaff($id: String!) @api(name: "masterEndpoint") {
    activeSiteStaff(id: $id) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useActiveSiteStaff = () => {
  const [activeSiteStaff, { data, loading, error }] = useMutation(ACTIVE_SITE_STAFF);
  const handleActiveSiteStaff = async (variables: any) => {
    try {
      const { data } = await activeSiteStaff({ variables });
      if (data.activeSiteStaff && data.activeSiteStaff.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện cập nhật trạng thái bản ghi thành công.');
      } else if (data.activeSiteStaff && data.activeSiteStaff.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.activeSiteStaff.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    activeSiteStaff: handleActiveSiteStaff,
    resultActiveSiteStaff: data,
    isLoadingActiveSiteStaff: loading,
    errorActiveSiteStaff: error
  };
};
