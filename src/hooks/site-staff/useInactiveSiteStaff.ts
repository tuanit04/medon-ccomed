import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const INACTIVE_SITE_STAFF = gql`
  mutation deActiveSiteStaff($id: String!) @api(name: "masterEndpoint") {
    deActiveSiteStaff(id: $id) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useInactiveSiteStaff = () => {
  const [inactiveSiteStaff, { data, loading, error }] = useMutation(INACTIVE_SITE_STAFF);
  const handleInactiveSiteStaff = async variables => {
    try {
      const { data } = await inactiveSiteStaff({ variables });
      if (data.deActiveSiteStaff && data.deActiveSiteStaff.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện cập nhật trạng thái bản ghi thành công.');
      } else if (data.deActiveSiteStaff && data.deActiveSiteStaff.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.deActiveSiteStaff.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    inactiveSiteStaff: handleInactiveSiteStaff,
    resultInactiveSiteStaff: data,
    isLoadingInactiveSiteStaff: loading,
    errorInactiveSiteStaff: error
  };
};
