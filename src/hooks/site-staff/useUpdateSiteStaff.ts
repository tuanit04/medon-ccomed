import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const UPDATE_SITE_STAFF = gql`
  mutation saveSiteStaff($data: SiteStaffInput!) @api(name: "masterEndpoint") {
    saveSiteStaff(data: $data) {
      code
      message
      data {
        id
        code
        name
        status
        phone
        facilityId
        isBotMessage
        parentFacilityId
        userId
      }
    }
  }
`;

export const useUpdateSiteStaff = onSetIdRes => {
  const [updateSiteStaff, { data, loading, error }] = useMutation(UPDATE_SITE_STAFF);
  const handleUpdateSiteStaff = async variables => {
    try {
      const { data } = await updateSiteStaff({ variables });
      onSetIdRes(data?.['saveSiteStaff']?.['data']?.['id']);
      if (data.saveSiteStaff && data.saveSiteStaff.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.saveSiteStaff.message, 'success');
      } else if (data.saveSiteStaff && data.saveSiteStaff.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.saveSiteStaff.message, 'error');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message, 'error');
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };

  return {
    updateSiteStaff: handleUpdateSiteStaff,
    resultUpdate: data,
    isLoadingUpdate: loading,
    errorUpdate: error
  };
};
