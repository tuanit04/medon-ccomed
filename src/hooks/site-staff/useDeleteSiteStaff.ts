import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const DELETE_SITE_STAFF = gql`
  mutation deleteSiteStaff($id: String!) @api(name: "masterEndpoint") {
    deleteSiteStaff(id: $id) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useDeleteSiteStaff = () => {
  const [deleteSiteStaff, { data, loading, error }] = useMutation(DELETE_SITE_STAFF);
  const handleDeleteSiteStaff = async variables => {
    try {
      const { data } = await deleteSiteStaff({ variables });
      if (data.deleteSiteStaff && data.deleteSiteStaff.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa bản ghi thành công.');
      } else if (data.deleteSiteStaff && data.deleteSiteStaff.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa bản ghi thất bại.');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    deleteSiteStaff: handleDeleteSiteStaff,
    result: data,
    isLoading: loading,
    error
  };
};
