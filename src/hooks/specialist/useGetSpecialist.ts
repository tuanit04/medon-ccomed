import { gql, useLazyQuery } from '@apollo/client';

const GET_SPECIALIST = gql`
  query specialistes($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "masterEndpoint") {
    specialistes(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      code
      message
      status
      page
      pages
      records
      data {
        id
        code
        name
      }
    }
  }
`;
export const useGetSpecialist = () => {
  const [loadSpecialist, { called, loading, data }] = useLazyQuery(GET_SPECIALIST);
  return {
    specialists: data?.specialistes?.data,
    isLoadingSpecialists: loading,
    loadSpecialist: loadSpecialist
  };
};
