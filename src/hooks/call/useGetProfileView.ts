import { gql, LazyQueryHookOptions, QueryHookOptions, useLazyQuery, useQuery } from '@apollo/client';

const GET_PROFILE_VIEW = gql`
  query findAllProfileViewPaging($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "masterEndpoint") {
    findAllProfileViewPaging(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      code
      message
      page
      pages
      records
      status
      data {
        id
        name
        phone
        crmStatus
        crmCode
        relationCount
        callCount
        ticketCount
        appointCount
        lastDate
        createDate
      }
    }
  }
`;

export const useQueryProfileView = (
  options?: QueryHookOptions<{ findAllProfileViewPaging: IResponse<IProfileView[]> }, IGraphQLRequest>
) => {
  return useQuery(GET_PROFILE_VIEW, options);
};

export const useLazyQueryProfileView = (
  options?: LazyQueryHookOptions<{ findAllProfileViewPaging: IResponse<IProfileView[]> }, IGraphQLRequest>
) => {
  return useLazyQuery(GET_PROFILE_VIEW, options);
};
