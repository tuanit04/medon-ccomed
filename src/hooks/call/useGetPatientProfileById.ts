import { gql, LazyQueryHookOptions, QueryHookOptions, useLazyQuery, useQuery } from '@apollo/client';

const GET_PATIENT_PROFILE_BY_ID = gql`
  query getPatientProfileById($id: String) @api(name: "masterEndpoint") {
    getPatientProfileById(id: $id) {
      code
      status
      message
      data {
        id
        pid
        patientHisId
        type
        name
        birthDate
        sex
        birthYear
        avatar
        address
        phone
        email
        provinceId
        districtId
        wardId
        streetId
        idNo
        idIssuedDate
        idIssuedPlace
        idImageFront
        idImageBack
        userId
        status
        createDate
        createUser
        createUserId
        updateDate
        updateUser
        updateUserId
        profileId
        provinceName
        districtName
        wardName
        nationality
        crmStatus
        crmCode
        relation {
          patientId
          relationType
          relationName
          profileInfo {
            pid
            name
            birthDate
            sex
            phone
          }
        }
      }
    }
  }
`;

export const useQueryPatientProfileById = (
  options?: QueryHookOptions<{ getPatientProfileById?: IResponse<IPatientProfile> }, { id: string }>
) => {
  return useQuery(GET_PATIENT_PROFILE_BY_ID, options);
};

export const useLazyQueryPatientProfileById = (
  options?: LazyQueryHookOptions<{ getPatientProfileById?: IResponse<IPatientProfile> }, { id: string }>
) => {
  return useLazyQuery(GET_PATIENT_PROFILE_BY_ID, options);
};
