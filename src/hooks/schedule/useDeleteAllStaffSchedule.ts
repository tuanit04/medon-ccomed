import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const DELETE_ALL_SITE_STAFF_SCHEDULE = gql`
  mutation deleteAllStaffSchedule($parentFacilityId: String!, $facilityId: String, $scheduleDate: String!)
    @api(name: "masterEndpoint") {
    deleteAllStaffSchedule(parentFacilityId: $parentFacilityId, facilityId: $facilityId, scheduleDate: $scheduleDate) {
      code
      status
      message
      data {
        id
      }
    }
  }
`;

export const useDeleteAllSiteStaffSchedule = () => {
  const [deleteAllSiteStaffSchedule, { data, loading, error }] = useMutation(DELETE_ALL_SITE_STAFF_SCHEDULE);
  const handleDeleteAllSiteStaffSchedule = async variables => {
    try {
      const { data } = await deleteAllSiteStaffSchedule({ variables });
      if (data.deleteAllStaffSchedule && data.deleteAllStaffSchedule.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa các bản ghi thành công.');
      } else if (data.deleteAllStaffSchedule && data.deleteAllStaffSchedule.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa các bản ghi thất bại.');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    deleteAllSiteStaffSchedule: handleDeleteAllSiteStaffSchedule,
    resultDeleteAllSiteStaffSchedule: data,
    isLoadingDeleteAllSiteStaffSchedule: loading,
    errorDeleteAllSiteStaffSchedule: error
  };
};
