import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const DELETE_SITE_STAFF_SCHEDULE = gql`
  mutation deleteStaffSchedule($data: String!) @api(name: "masterEndpoint") {
    deleteStaffSchedule(data: $data) {
      code
      status
      message
      data {
        id
      }
    }
  }
`;

export const useDeleteSiteStaffSchedule = () => {
  const [deleteSiteStaffSchedule, { data, loading, error }] = useMutation(DELETE_SITE_STAFF_SCHEDULE);
  const handleDeleteSiteStaffSchedule = async variables => {
    try {
      const { data } = await deleteSiteStaffSchedule({ variables });
      if (data.deleteStaffSchedule && data.deleteStaffSchedule.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa các bản ghi thành công.');
      } else if (data.deleteStaffSchedule && data.deleteStaffSchedule.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa các bản ghi thất bại.');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    deleteSiteStaffSchedule: handleDeleteSiteStaffSchedule,
    resultDeleteSiteStaffSchedule: data,
    isLoadingDeleteSiteStaffSchedule: loading,
    errorDeleteSiteStaffSchedule: error
  };
};
