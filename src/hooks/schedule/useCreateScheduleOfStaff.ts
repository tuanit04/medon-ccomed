import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const SAVE_SCHEDULE_OF_STAFF = gql`
  mutation saveScheduleOfStaff($data: ScheduleInput!) @api(name: "masterEndpoint") {
    saveScheduleOfStaff(data: $data) {
      status
      message
      code
      data {
        objectId
        id
      }
    }
  }
`;

const COPY_SCHEDULE_OF_STAFF = gql`
  mutation copyScheduleOfStaff($data: ScheduleInput!) @api(name: "masterEndpoint") {
    copyScheduleOfStaff(data: $data) {
      status
      message
      code
      data {
        objectId
        id
      }
    }
  }
`;

const SAVE_SCHEDULE_OF_STAFF_LIST = gql`
  mutation saveScheduleOfStaffList($data: ScheduleInput!) @api(name: "masterEndpoint") {
    saveScheduleOfStaffList(data: $data) {
      status
      message
      code
      data {
        objectId
        id
      }
    }
  }
`;

export const useCreateScheduleOfStaff = () => {
  const [createScheduleOfStaff, { data, loading, error }] = useMutation(SAVE_SCHEDULE_OF_STAFF);
  const handleCreateScheduleOfStaff = async variables => {
    try {
      const { data } = await createScheduleOfStaff({ variables });
      if (data.saveScheduleOfStaff && data.saveScheduleOfStaff.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.saveScheduleOfStaff.message);
      } else if (data.saveScheduleOfStaff && data.saveScheduleOfStaff.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.saveScheduleOfStaff.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
};
export const useCopyScheduleOfStaff = () => {
  const [copyScheduleOfStaff, { data, loading, error }] = useMutation(COPY_SCHEDULE_OF_STAFF);
  const handleCopyScheduleOfStaff = async variables => {
    try {
      const { data } = await copyScheduleOfStaff({ variables });
      if (data.copyScheduleOfStaff && data.copyScheduleOfStaff.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.copyScheduleOfStaff.message);
      } else if (data.copyScheduleOfStaff && data.copyScheduleOfStaff.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.copyScheduleOfStaff.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };

  return {
    copyScheduleOfStaff: handleCopyScheduleOfStaff,
    resultCopyScheduleOfStaff: data,
    isLoadingCopyScheduleOfStaff: loading,
    errorCopyScheduleOfStaff: error
  };
};

export const useCreateScheduleOfStaffList = () => {
  const [createScheduleOfStaffList, { data, loading, error }] = useMutation(SAVE_SCHEDULE_OF_STAFF_LIST);
  const handleCreateScheduleOfStaffList = async variables => {
    try {
      const { data } = await createScheduleOfStaffList({ variables });
      if (data.saveScheduleOfStaffList && data.saveScheduleOfStaffList.code === 'SUCCESS') {
        openNotificationRight(data.saveScheduleOfStaffList.message);
      } else if (data.saveScheduleOfStaffList && data.saveScheduleOfStaffList.code !== 'SUCCESS') {
        openNotificationRight(data.saveScheduleOfStaffList.message);
      } else if (data.response && data.response.code === 'ACCESS_DENIED') {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    createScheduleOfStaffList: handleCreateScheduleOfStaffList,
    resultCreateScheduleOfStaff: data,
    isLoadingCreateScheduleOfStaff: loading,
    errorCreateScheduleOfStaff: error
  };
};
