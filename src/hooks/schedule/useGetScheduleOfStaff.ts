import { gql, useQuery } from '@apollo/client';

const GET_SCHEDULE = gql`
  query staffSchedule(
    $page: Int
    $pageSize: Int
    $parentFacilityId: String
    $facilityId: String
    $scheduleDate: String
  ) @api(name: "masterEndpoint") {
    staffSchedule(
      page: $page
      pageSize: $pageSize
      parentFacilityId: $parentFacilityId
      facilityId: $facilityId
      scheduleDate: $scheduleDate
    ) {
      status
      message
      code
      data {
        routeId
        facilityName
        routeName
        scheduleDate
        siteStaffs {
          scheduleId
          objectId
          objectName
          workSessionId
          workSessionName
        }
        workSessions {
          name
          id
          type
          status
        }
      }
      page
      pages
      records
    }
  }
`;

export const useGetSchedules = variables => {
  const { data, loading, error, refetch } = useQuery(GET_SCHEDULE, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    pagingSchedules: data?.staffSchedule || {},
    isLoadingSchedules: loading,
    errorSchedules: error?.graphQLErrors,
    refetchSchedules: refetch
  };
};
