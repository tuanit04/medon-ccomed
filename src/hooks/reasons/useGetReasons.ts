import { gql, useLazyQuery } from '@apollo/client';

const GET_REASONS = gql`
  query reasons @api(name: "appointmentEndpoint"){
    reasons {
      code
      message
      data {
        name
        id
      }
    }
  }
`;
export const useGetReasons = () => {
  const [loadReasons, { called, loading, data }] = useLazyQuery(GET_REASONS);
  return {
    reasons: data?.reasons?.data,
    isLoadingReasons: loading,
    loadReasons: loadReasons
  };
};
