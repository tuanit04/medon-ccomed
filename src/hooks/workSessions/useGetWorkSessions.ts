import { gql, useQuery } from '@apollo/client';

const GET_WORK_SESSIONS = gql`
  query workSessions($type: String) @api(name: "masterEndpoint") {
    workSessions(type: $type) {
      code
      message
      status
      data {
        id
        name
      }
    }
  }
`;

export const useGetWorkSessions = variables => {
  const { data, loading, error, refetch } = useQuery(GET_WORK_SESSIONS, {
    variables: { ...variables }
  });
  return {
    pagingWorkSessions: data?.workSessions || {},
    isLoadingWorkSessions: loading,
    errorWorkSessions: error?.graphQLErrors,
    refetchWorkSessions: refetch
  };
};
