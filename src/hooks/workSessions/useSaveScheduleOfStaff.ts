import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const SAVE_SCHEDULE_OF_STAFF = gql`
  mutation saveScheduleOfStaff($data: StreetInput!) @api(name: "masterEndpoint") {
    saveStreet(data: $data) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useSaveScheduleOfStaff = onSetIdRes => {
  const [saveScheduleOfStaff, { data, loading, error }] = useMutation(SAVE_SCHEDULE_OF_STAFF);
  const handleUpdateStreet = async variables => {
    try {
      const { data } = await saveScheduleOfStaff({ variables });
      onSetIdRes(data?.['saveScheduleOfStaff']?.['data']?.['id']);
      if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      } else {
        if (data.saveScheduleOfStaff.data) {
          openNotificationRight(data.saveScheduleOfStaff.message, 'success');
        } else {
          openNotificationRight(data.saveScheduleOfStaff.message, 'error');
        }
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };

  return {
    saveScheduleOfStaff: handleUpdateStreet,
    resultSaveScheduleOfStaff: data,
    isLoadingSaveScheduleOfStaff: loading,
    errorSaveScheduleOfStaff: error
  };
};
