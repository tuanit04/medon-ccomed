import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';
import { GET_ROUTES } from './useGetRoute';

const DELETE_ROUTE = gql`
  mutation deleteRoute($id: String!) @api(name: "masterEndpoint") @api(name: "masterEndpoint") {
    deleteRoute(id: $id) {
      code
      message
      status
      data {
        id
      }
    }
  }
`;

export const useDeleteRoute = () => {
  const [deleteRoute, { data, loading, error }] = useMutation(DELETE_ROUTE, {
    refetchQueries: [
      {
        query: GET_ROUTES,
        variables: {
          page: 1,
          pageSize: 20
        }
      }
    ]
  });
  const handleDeleteRoute = async variables => {
    try {
      const { data } = await deleteRoute({ variables });
      if (data.deleteRoute.message) {
        openNotificationRight(data.deleteRoute.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    deleteRoute: handleDeleteRoute,
    result: data,
    isLoading: loading,
    error
  };
};
