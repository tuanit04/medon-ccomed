import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const ACTIVE_ROUTE = gql`
  mutation activeRoute($id: String!) @api(name: "masterEndpoint") {
    activeRoute(id: $id) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useActiveRoute = () => {
  const [activeRoute, { data, loading, error }] = useMutation(ACTIVE_ROUTE);
  const handleActiveRoute = async variables => {
    try {
      const { data } = await activeRoute({ variables });
      if (data.activeRoute && data.activeRoute.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện cập nhật trạng thái bản ghi thành công.');
      } else if (data.activeRoute && data.activeRoute.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.activeRoute.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    activeRoute: handleActiveRoute,
    resultActiveRoute: data,
    isLoadingActiveRoute: loading,
    errorActiveRoute: error
  };
};
