import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const UPDATE_ROUTE = gql`
  mutation saveRoute($data: RouteInput!) @api(name: "masterEndpoint") {
    saveRoute(data: $data) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useUpdateRoute = onSetIdRes => {
  const [updateRoute, { data, loading, error }] = useMutation(UPDATE_ROUTE);
  const handleUpdateRoute = async variables => {
    try {
      const { data } = await updateRoute({ variables });
      onSetIdRes(data?.['saveRoute']?.['data']?.['id']);
      if (data.saveRoute && data.saveRoute.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.saveRoute.message);
      } else if (data.saveRoute && data.saveRoute.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.saveRoute.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };

  return {
    updateRoute: handleUpdateRoute,
    resultUpdateRoute: data,
    isLoadingUpdateRoute: loading,
    errorUpdateRoute: error
  };
};
