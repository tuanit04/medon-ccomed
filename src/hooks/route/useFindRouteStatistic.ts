import { gql, useQuery } from '@apollo/client';

const FIND_ROUTE_STATISTIC = gql`
  query findRouteStatistic @api(name: "masterEndpoint") {
    findRouteStatistic {
      code
      message
      data {
        totalRecords
        activeRecords
        inactiveRecords
      }
    }
  }
`;

export const useFindRouteStatistic = () => {
  const { data, loading, error, refetch } = useQuery(FIND_ROUTE_STATISTIC);
  return {
    routeStatistic: data?.findRouteStatistic.data || {},
    isLoadingRouteStatistic: loading,
    errorRouteStatistic: error?.graphQLErrors,
    refetchRouteStatistic: refetch
  };
};
