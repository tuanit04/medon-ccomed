import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const INACTIVE_ROUTE = gql`
  mutation deActiveRoute($id: String!) @api(name: "masterEndpoint") {
    deActiveRoute(id: $id) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useInactiveRoute = () => {
  const [inactiveRoute, { data, loading, error }] = useMutation(INACTIVE_ROUTE);
  const handleAInactiveRoute = async variables => {
    try {
      const { data } = await inactiveRoute({ variables });
      if (data.deActiveRoute && data.deActiveRoute.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện cập nhật trạng thái bản ghi thành công.');
      } else if (data.deActiveRoute && data.deActiveRoute.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.deActiveRoute.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    inactiveRoute: handleAInactiveRoute,
    resultInactiveRoute: data,
    isLoadingInactiveRoute: loading,
    errorInactiveRoute: error
  };
};
