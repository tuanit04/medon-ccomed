import { gql, useLazyQuery, useQuery } from '@apollo/client';
import { Route } from 'common/interfaces/route.interface';

const GET_ROUTES = gql`
  query routes($page: Int, $pageSize: Int, $filtered: [FilteredInput]) @api(name: "masterEndpoint") {
    routes(page: $page, pageSize: $pageSize, filtered: $filtered) {
      code
      message
      data {
        id
        code
        name
        status
        updateUser
        updateDate
        createDate
        createUser
        facilityId
        parentFacilityId
        facilityName
        parentFacilityName
        facilityCode
      }
      page
      pages
      records
    }
  }
`;

const GET_ROUTE_DETAIL = gql`
  query getRouteDetail($routeId: String!) {
    route(routeId: $routeId) {
      code
      message
      data {
        id
        code
        name
        status
        facilityId
        parentFacilityId
      }
    }
  }
`;

export const useGetRoutes = variables => {
  const { data, loading, error, refetch } = useQuery(GET_ROUTES, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    pagingRoutes: data?.routes || {},
    isLoading: loading,
    error: error?.graphQLErrors,
    refetchRoutes: refetch
  };
};

export const useGetRoutesLazy = () => {
  const [loadRoutes, { called, loading, data }] = useLazyQuery(GET_ROUTES, {
    fetchPolicy: 'network-only'
  });
  return {
    routesData: data?.routes?.data,
    isLoadingRoutes: loading,
    loadRoutes: loadRoutes
  };
};

export const useGetRouteDetail = () => {
  const [loadRouteDetail, { called, loading, data }] = useLazyQuery(GET_ROUTE_DETAIL);
  return {
    routeDetail: data?.route?.data,
    isLoadingRouteDetail: loading,
    loadRouteDetail: loadRouteDetail
  };
};

export { GET_ROUTES, GET_ROUTE_DETAIL };
