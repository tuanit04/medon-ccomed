import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_USERS = gql`
  query getListUsers($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "accountEndpoint") {
    getUsers(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      status
      message
      data {
        id
        name
        userName
        password
        email
        phone
        address
        userTypeId
        sex
        facilityId
        birthDate
        userGroups {
          id
          name
          status
          userName
          userId
        }
        userFunctions {
          id
          functionCode
          name
          parentId
          userName
          userId
        }
        status
      }
      page
      records
    }
  }
`;

const GET_USER_TYPE = gql`
  query getUserTypes($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "accountEndpoint") {
    getUserTypes(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      status
      message
      data {
        id
        name
        status
        code
      }
      page
      records
    }
  }
`;

export const useGetUsers = variables => {
  const { data, loading, error, refetch } = useQuery(GET_USERS, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    pagingUsers: data?.getUsers || {},
    isLoadingUsers: loading,
    errorUsers: error?.graphQLErrors,
    refetchUsers: refetch
  };
};

export const useGetUsersLazy = () => {
  const [loadUsers, { called, loading, data }] = useLazyQuery(GET_USERS, {
    fetchPolicy: 'network-only'
  });
  return {
    users: data?.getUsers?.data,
    isLoadingUsers: loading,
    loadUsers: loadUsers
  };
};

export const useGetUserTypeLazy = () => {
  const [loadUserType, { called, loading, data }] = useLazyQuery(GET_USER_TYPE, {
    fetchPolicy: 'network-only',
    variables: {
      filtered: [
        {
          id: 'code',
          value: 'USER_TD',
          operation: '=='
        }
      ]
    }
  });
  return {
    userType: data?.getUserTypes?.data,
    isLoadingUserType: loading,
    loadUserType: loadUserType
  };
};
