import { gql, MutationHookOptions, useMutation } from '@apollo/client';
import { Patient } from 'common/interfaces/patient.interface';

const CREATE_PATIENT = gql`
  mutation savePatient($data: PatientInput!) @api(name: "masterEndpoint") {
    savePatient(data: $data) {
      code
      message
      status
      data {
        id
        name
        sex
        address
        birthDate
        phone
        email
        status
      }
    }
  }
`;

export const useCreatePatient = (
  options?: MutationHookOptions<{ savePatient?: IResponse<Patient> }, { data?: Partial<IPatientInput> }>
) => {
  return useMutation(CREATE_PATIENT, options);
};
