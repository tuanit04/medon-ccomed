import { gql, useLazyQuery } from '@apollo/client';

const GET_PATIENT = gql`
  query patients($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "masterEndpoint") {
    patients(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      code
      message
      status
      page
      pages
      records
      data {
        id
        patientHisId
        pid
        name
        birthDate
        birthYear
        sex
        phone
        idNo
        address
        provinceId
        districtId
        wardId
      }
    }
  }
`;
export const useGetPatients = () => {
  const [loadPatients, { called, loading, data }] = useLazyQuery(GET_PATIENT);
  return {
    patients: data?.patients?.data,
    isLoadingPatients: loading,
    loadPatients: loadPatients,
    pagingPatients: data?.patients
  };
};
