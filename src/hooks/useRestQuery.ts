import React from 'react';
import { request } from 'api/request';
import axios, { AxiosRequestConfig } from 'axios';

interface Props<P> {
  url: string;
  method?: 'get' | 'post';
  initialCall?: boolean;
  initialParams?: P;
  config?: AxiosRequestConfig;
  isBlob?: boolean;
}

const useRestQuery = <T = any, P = any>({
  url,
  initialCall = true,
  initialParams,
  method = 'get',
  config,
  isBlob
}: Props<P>) => {
  const [isLoading, setIsLoading] = React.useState(false);
  const [isCalled, setIsCalled] = React.useState(initialCall);
  const [isFirstCall, setIsFirstCall] = React.useState(true);
  const [isError, setIsError] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState('');
  const [data, setData] = React.useState<T>();
  const [params, setParams] = React.useState<P | undefined>(initialParams);
  const [callback, setCallback] = React.useState<(res: T) => void>();

  React.useEffect(() => {
    const fetchData = async () => {
      setData(undefined);
      setIsLoading(true);
      try {
        let res;

        if (isBlob && method === 'post') {
          res = await fetch(url, {
            method,
            headers: {
              Token: localStorage.getItem('t') ?? '',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
          }).then(response => response.blob());
        } else {
          res = await request(method, url, params, {
            ...config,
            headers: { ...config?.headers, Token: localStorage.getItem('t') ?? '' }
          });
        }

        callback?.(res);
        if (res.hasOwnProperty('status') && !res.status) {
          throw Error(res.message);
        }
        setData(res as any);
        setIsError(false);
        setErrorMessage('');
      } catch (err) {
        setIsError(true);
        setErrorMessage(err.message);
      }
      setIsLoading(false);
      setIsCalled(false);
      if (isFirstCall) {
        setIsFirstCall(false);
      }
    };

    if (isCalled) {
      fetchData();
    }
  }, [isCalled]);

  const call = (_params?: P, _callback?: (res?: T) => void) => {
    setParams(_params);
    setCallback(_callback);
    setIsCalled(true);
  };

  return { isLoading, isError, isFirstCall, data, call, errorMessage };
};

export default useRestQuery;
