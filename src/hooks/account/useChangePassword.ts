import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { useAppDispatch } from 'helpers';
import { logoutAsync } from 'store/user.store';
import { openNotificationRight } from 'utils/notification';
const Swal = require('sweetalert2');

const CHANGE_PASSWORD = gql`
  mutation changePassword($data: UserInput) @api(name: "accountEndpoint") {
    changePassword(data: $data) {
      status
      message
      code
      data {
        id
      }
    }
  }
`;

export const useChangePassword = () => {
  const dispatch = useAppDispatch();
  const [changePassword, { data, loading, error }] = useMutation(CHANGE_PASSWORD);
  const handleChangePassword = async variables => {
    try {
      const { data } = await changePassword({ variables });
      if (data.changePassword && data.changePassword.status === 1) {
        let timerInterval;
        Swal.fire({
          title: 'Đổi mật khẩu thành công !',
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          },
          position: 'top',
          html: 'Quay lại trang đăng nhập sau <strong></strong> giây nữa.<br/><br/>',
          timer: 5000,
          didOpen: () => {
            timerInterval = setInterval(() => {
              Swal.getHtmlContainer().querySelector('strong').textContent = (Swal.getTimerLeft() / 1000).toFixed(0);
            }, 100);
          },
          willClose: () => {
            dispatch(logoutAsync());
            clearInterval(timerInterval);
          }
        });
      } else if (data.changePassword && data.changePassword.code !== 1) {
        openNotificationRight(data.changePassword.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };

  return {
    changePassword: handleChangePassword,
    resultChangePassword: data,
    isLoadingChangePassword: loading,
    errorChangePassword: error
  };
};
