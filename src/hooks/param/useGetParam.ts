import { gql, useLazyQuery } from '@apollo/client';

const GET_PARAM = gql`
  query params($type: String) {
    params(type: $type) {
      code
      message
      data {
        paramKey
        paramType
        paramValue
      }
    }
  }
`;
export const useGetParams = () => {
  const [loadParams, { called, loading, data }] = useLazyQuery(GET_PARAM);
  return {
    params: data?.params?.data,
    isLoadingParams: loading,
    loadParams: loadParams
  };
};
