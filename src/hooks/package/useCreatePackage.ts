import { gql, useMutation } from '@apollo/client';
import { openNotificationRight } from 'utils/notification';
interface PackageInput {
  data: {
    name: string;
    code: string;
    routeId: string;
    wardId: string;
    districtId: string;
    provinceId: string;
    status: number;
    updateDate: Date;
    createDate: Date;
    createUser: string;
    updateUser: string;
    createUserId: string;
    updateUserId: string;
    id: string;
  };
}

const CREATE_PACKAGE = gql`
  mutation savePackage($data: PackageInput) {
    savePackage(data: $data) {
      code
      message
      data {
        name
        code
        id
      }
    }
  }
`;

export const useCreatePackage = () => {
  const [createPackage, { data, loading, error }] = useMutation(CREATE_PACKAGE);
  const handleCreatePackage = async variables => {
    try {
      const { data } = await createPackage({ variables });
      if (data.savePackage.data) {
        openNotificationRight(data.savePackage.message);
      } else {
        openNotificationRight(data.savePackage.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý');
    }
  };

  return {
    createPackage: handleCreatePackage,
    resultCreatePackage: data,
    isLoadingCreatePackage: loading,
    errorCreatePackage: error
  };
};
