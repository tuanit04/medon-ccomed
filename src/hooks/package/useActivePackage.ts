import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const ACTIVE_PACKAGE = gql`
  mutation activePackage($id: String!) @api(name: "masterEndpoint") {
    activePackage(id: $id) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useActivePackage = () => {
  const [activePackage, { data, loading, error }] = useMutation(ACTIVE_PACKAGE);
  const handleActivePackage = async (variables: any) => {
    try {
      const { data } = await activePackage({ variables });
      if (data.activePackage && data.activePackage.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện cập nhật trạng thái bản ghi thành công.');
      } else if (data.activePackage && data.activePackage.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.activePackage.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    activePackage: handleActivePackage,
    resultActivePackage: data,
    isLoadingActivePackage: loading,
    errorActivePackage: error
  };
};
