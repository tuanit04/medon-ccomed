import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const INACTIVE_PACKAGE = gql`
  mutation deActivePackage($id: String!) @api(name: "masterEndpoint") {
    deActivePackage(id: $id) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useInactivePackage = () => {
  const [inactivePackage, { data, loading, error }] = useMutation(INACTIVE_PACKAGE);
  const handleInactivePackage = async variables => {
    try {
      const { data } = await inactivePackage({ variables });
      if (data.deActivePackage && data.deActivePackage.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện cập nhật trạng thái bản ghi thành công.');
      } else if (data.deActivePackage && data.deActivePackage.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.deActivePackage.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    inactivePackage: handleInactivePackage,
    resultInactivePackage: data,
    isLoadingInactivePackage: loading,
    errorInactivePackage: error
  };
};
