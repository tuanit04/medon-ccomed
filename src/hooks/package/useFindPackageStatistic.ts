import { gql, useQuery } from '@apollo/client';

const FIND_PACKAGE_STATISTIC = gql`
  query findPackageStatistic($filtered: [FilteredInput]) @api(name: "masterEndpoint") {
    findPackageStatistic(filtered: $filtered) {
      code
      message
      data {
        totalRecords
        activeRecords
        inactiveRecords
      }
    }
  }
`;

export const useFindRouteStatistic = variables => {
  const { data, loading, error, refetch } = useQuery(FIND_PACKAGE_STATISTIC, {
    variables: {
      filtered: variables['filtered']
    }
  });
  return {
    packageStatistic: data?.findPackageStatistic.data || {},
    isLoadingPackageStatistic: loading,
    errorPackageStatistic: error?.graphQLErrors,
    refetchPackageStatistic: refetch
  };
};
