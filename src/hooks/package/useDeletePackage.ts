import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const DELETE_PACKAGE = gql`
  mutation deletePackage($id: String!) @api(name: "masterEndpoint") {
    deletePackage(id: $id) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useDeletePackage = () => {
  const [deletePackage, { data, loading, error }] = useMutation(DELETE_PACKAGE);
  const handleDeletePackage = async variables => {
    try {
      const { data } = await deletePackage({ variables });
      if (data.deletePackage && data.deletePackage.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa bản ghi thành công.');
      } else if (data.deletePackage && data.deletePackage.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa bản ghi thất bại.');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    deletePackage: handleDeletePackage,
    resultDelePackage: data,
    isLoadingDelePackage: loading,
    errorDelePackage: error
  };
};
