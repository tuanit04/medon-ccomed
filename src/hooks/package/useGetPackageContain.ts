import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_PACKAGE_CONTAIN = gql`
  query findPackageContain($serviceId: String) {
    findPackageContain(serviceId: $serviceId) {
      code
      message
      page
      pages
      records
      status
      data {
        packageId
        packageHisId
        packageName
        originalPrice
        discount
        packageImage
      }
    }
  }
`;

export const useGetPackagesContain = variables => {
  const { data, loading, error, refetch } = useQuery(GET_PACKAGE_CONTAIN, {
    variables: { ...variables }
  });
  return {
    pagingPackageContain: data?.findPackageContain || {},
    isLoadingPackageContain: loading,
    errorPackageContain: error?.graphQLErrors,
    refetchPackageContain: refetch
  };
};

export const useGetPackagesContainLazy = variables => {
  const [refetchPackageContain, { called, loading, data }] = useLazyQuery(GET_PACKAGE_CONTAIN, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    pagingPackageContain: data?.findAllServices || {},
    isLoadingPackageContain: loading,
    refetchPackageContain
  };
};
