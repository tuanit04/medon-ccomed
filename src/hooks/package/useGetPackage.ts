import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_PACKAGES = gql`
  query packages($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "masterEndpoint") {
    packages(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      code
      message
      page
      pages
      records
      data {
        id
        name
        code
        image
        startDate
        endDate
        methodType
        sex
        originPrice
        homeSalePrice
        hosSalePrice
        updateUser
        updateDate
        status
        homeSalePrice
        hosSalePrice
        originPrice
        display
      }
    }
  }
`;

const GET_PACKAGE = gql`
  query packagesById($id: String!) @api(name: "masterEndpoint") {
    packagesById(id: $id) {
      code
      message
      data {
        id
        name
        sex
        code
        description
        startDate
        endDate
        minAge
        maxAge
        maxNumBook
        useQuantity
        bookQuantity
        preBookHour
        packageHisId
        image
        display
        methodType
        approveDate
        approveUser
        status
        createDate
        createUser
        createUserId
        updateDate
        updateUser
        updateUserId
        paramList {
          packageId
          provinces
          weekDays
          packageTypes
          applyFacilities
        }
        priceHistories {
          objectType
          objectId
          facilityId
          packageId
          originPrice
          homeSalePrice
          hosSalePrice
          contractCode
          fromDate
          toDate
          status
          numService
        }
        services {
          id
          status
          originPrice
          serviceName
        }
        packageDetail {
          packageId
          provinces
          weekDays
          packageTypes
          applyFacilities
        }
        originPrice
        homeSalePrice
        hosSalePrice
        processNote
        warningNote
      }
    }
  }
`;

const GET_ALL_PACKAGE_CCOMED = gql`
  query getAllPackageCCOMED($filter: PackageFilterInput!, $page: Int, $pageSize: Int) @api(name: "masterEndpoint") {
    getAllPackageCCOMED(filter: $filter, page: $page, pageSize: $pageSize) {
      code
      message
      page
      pages
      records
      data {
        id
        code
        name
        sex
        originPrice
        hosSalePrice
        homeSalePrice
        discount
        numService
        minAge
        maxAge
        image
        description
        processNote
        warningNote
        rating
        packageHisId
        packageMapId
        status
        methodType
        startDate
        endDate
      }
    }
  }
`;

const GET_PACKAGE_TYPE = gql`
  query findPackageType @api(name: "masterEndpoint") {
    findPackageType {
      code
      message
      data {
        id
        name
        description
        image
        createDate
        createUser
        updateDate
        updateUser
        status
        orders
      }
    }
  }
`;

export const useGetPackage = () => {
  const [loadPackage, { called, loading, data }] = useLazyQuery(GET_PACKAGE, {
    fetchPolicy: 'network-only'
  });
  return {
    package: data?.packagesById?.data || {},
    isLoadingPackage: loading,
    loadPackage
  };
};

export const useGetPackageTypes = () => {
  const [loadPackageTypes, { called, loading, data }] = useLazyQuery(GET_PACKAGE_TYPE);
  return {
    packageTypes: data?.findPackageType?.data || [],
    isLoadingPackageTypes: loading,
    loadPackageTypes
  };
};

export const useGetPackages = variables => {
  const { data, loading, error, refetch } = useQuery(GET_PACKAGES, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    pagingPackages: data?.packages || {},
    isLoadingPackages: loading,
    errorPackages: error?.graphQLErrors,
    refetchPackages: refetch
  };
};

export const useGetAllPackageCCOMED = variables => {
  const { data, loading, error, refetch } = useQuery(GET_ALL_PACKAGE_CCOMED, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    allPackageCCOMED: data?.getAllPackageCCOMED || {},
    isLoadingPackages: loading,
    errorPackages: error?.graphQLErrors,
    refetchPackages: refetch
  };
};

export const useGetAllPackageCCOMEDLazy = () => {
  const [loadPackages, { called, loading, data }] = useLazyQuery(GET_ALL_PACKAGE_CCOMED, {
    fetchPolicy: 'network-only'
  });
  return {
    allPackageCCOMED: data?.getAllPackageCCOMED || {},
    isLoadingPackages: loading,
    loadPackages
  };
};
