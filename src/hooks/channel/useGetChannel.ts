import { gql, useLazyQuery } from '@apollo/client';

const GET_CHANNEL = gql`
  query channels @api(name: "masterEndpoint") {
    channels {
      code
      message
      records
      data {
        id
        name
        code
      }
    }
  }
`;
export const useGetChannels = () => {
  const [loadChannels, { called, loading, data }] = useLazyQuery(GET_CHANNEL);
  return {
    channels: data?.channels?.data,
    isLoadingChannels: loading,
    loadChannels: loadChannels
  };
};
