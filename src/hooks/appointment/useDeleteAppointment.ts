import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const DELETE_APPOINTMENT = gql`
  mutation deleteAppointment($id: String!) @api(name: "appointmentEndpoint") {
    deleteAppointment(id: $id) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useDeleteAppointment = () => {
  const [deleteAppointment, { data, loading, error }] = useMutation(DELETE_APPOINTMENT);
  const handleDeleteAppointment = async variables => {
    try {
      const { data } = await deleteAppointment({ variables });
      if (data.deleteAppointment && data.deleteAppointment.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.deleteAppointment.message);
      } else if (data.deleteAppointment && data.deleteAppointment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.deleteAppointment.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    deleteAppointment: handleDeleteAppointment,
    resultDeleteAppointment: data,
    isLoadingDeleteAppointment: loading,
    errorDeleteAppointment: error
  };
};
