import { gql, useLazyQuery } from '@apollo/client';

const GET_APPOINTMENT_WORK_TIME = gql`
  query findAppointmentByWorkTime($data: ObjectInput!) @api(name: "appointmentEndpoint") {
    findAppointmentByWorkTime(data: $data) {
      code
      message
      data {
        id
        name
        objectCountList {
          workTimeId
          workTimeName
          status
          count
        }
      }
    }
  }
`;
export const useGetAppointmentWorkTimeHospital = () => {
  const [loadAppointmentWorkTimeHospital, { called, loading, data }] = useLazyQuery(GET_APPOINTMENT_WORK_TIME, {
    fetchPolicy: "network-only"
  });
  return {
    appointmentWorkTimeHospital: data?.findAppointmentByWorkTime,
    isLoadingAppointmentWorkTimeHospital: loading,
    loadAppointmentWorkTimeHospital: loadAppointmentWorkTimeHospital
  };
};
