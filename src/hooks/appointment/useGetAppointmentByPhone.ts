import { gql, useLazyQuery } from '@apollo/client';
const GET_APPOINTMENT_BY_PHONE = gql`
  query appointmentByPhone($page: Int, $pageSize: Int, $phone: String) @api(name: "appointmentEndpoint") {
    appointmentByPhone(page: $page, pageSize: $pageSize, phone: $phone) {
      code
      status
      message
      page
      pages
      records
      data {
        appointmentDate
        appointmentId
        patientId
        patientName
        patientBirthDate
        patientBirthYear
        patientSex
        patientPhone
        patientIdNo
        patientAddress
        patientEmail
        pid
        provinceId
        provinceCode
        districtId
        provinceName
        districtCode
        districtName
        status
      }
    }
  }
`;

export const useGetAppointmentByPhone = () => {
  const [load, { data, loading, error, refetch }] = useLazyQuery(GET_APPOINTMENT_BY_PHONE, {
    fetchPolicy: 'network-only'
  });
  return {
    loadAppointmentByPhone: load,
    appointmentByPhone: data?.appointmentByPhone || {},
    isLoadingAppointmentByPhone: loading,
    erroAppointmentByPhone: error?.graphQLErrors,
    refetchAppointmentByPhone: refetch
  };
};
