import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const CANCLE_APPOINTMENT = gql`
  mutation cancelAppointment($id: String) @api(name: "appointmentEndpoint") {
    cancelAppointment(id: $id) {
      status
      code
      message
      data {
        id
      }
    }
  }
`;

export const useCancleAppointment = (onSetIdRes?, onSetModifyVisible?) => {
  const [cancleAppointment, { data, loading, error }] = useMutation(CANCLE_APPOINTMENT);
  const handleCancleAppointment = async variables => {
    try {
      const { data } = await cancleAppointment({ variables });
      if (data.cancelAppointment && data.cancelAppointment.code === CODE_RESPONSE.SUCCESS) {
        onSetIdRes(data['cancelAppointment']['data']['id']);
        openNotificationRight(data.cancelAppointment.message);
        onSetModifyVisible(false);
      } else if (data.cancelAppointment && data.cancelAppointment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.cancelAppointment.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    cancleAppointment: handleCancleAppointment,
    resultCancleAppointment: data,
    isLoadingCancleAppointment: loading,
    errorCancleAppointment: error
  };
};
