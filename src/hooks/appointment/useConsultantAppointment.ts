import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { useAppState } from 'helpers';
import { openNotificationRight } from 'utils/notification';

const SAVE_APPOINTMENT = gql`
  mutation consultantAppointment($data: AppointmentInput) @api(name: "appointmentEndpoint") {
    consultantAppointment(data: $data) {
      status
      message
      code
      data {
        id
        status
        patientName
      }
    }
  }
`;

export const useConsultantAppointment = (onSetIdRes, onSetCreateVisible) => {
  const userObject = useAppState(state => state.user.userObject);
  const [consultantAppointment, { data, loading, error }] = useMutation(SAVE_APPOINTMENT);
  const handleConsultantAppointment = async variables => {
    try {
      const { data } = await consultantAppointment({
        variables: { data: { ...variables?.data, staffCode: userObject?.staffCode } }
      });
      onSetIdRes(data?.['consultantAppointment']?.['data']?.['id']);
      if (data.consultantAppointment && data.consultantAppointment.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.consultantAppointment.message);
        onSetCreateVisible(false);
      } else if (data.consultantAppointment && data.consultantAppointment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Tư vấn thất bại. Lý do : ' + data.consultantAppointment.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      openNotificationRight(error?.message);
    }
  };
  return {
    consultantAppointment: handleConsultantAppointment,
    resultConsultantAppointment: data,
    isLoadingConsultantAppointment: loading,
    errorConsultantAppointment: error
  };
};
