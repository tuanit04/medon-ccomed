import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const DELETE_MULTI_APPOINTMENT = gql`
  mutation deleteMultiAppointment($ids: [String]) @api(name: "appointmentEndpoint") {
    deleteMultiAppointment(ids: $ids) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useDeleteMultiAppointment = () => {
  const [deleteMultiAppointment, { data, loading, error }] = useMutation(DELETE_MULTI_APPOINTMENT);
  const handleDeleteMultiAppointment = async variables => {
    try {
      const { data } = await deleteMultiAppointment({ variables });
      if (data.deleteMultiAppointment && data.deleteMultiAppointment.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.deleteMultiAppointment.message);
      } else if (data.deleteMultiAppointment && data.deleteMultiAppointment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.deleteMultiAppointment.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    deleteMultiAppointment: handleDeleteMultiAppointment,
    resultDeleteMultiAppointment: data,
    isLoadingDeleteMultiAppointment: loading,
    errorDeleteMultiAppointment: error
  };
};
