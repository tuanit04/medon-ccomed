import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const CANCLE_MULTI_APPOINTMENT = gql`
  mutation cancelMultiAppointment($ids: [String]) @api(name: "appointmentEndpoint") {
    cancelMultiAppointment(ids: $ids) {
      status
      code
      message
      data {
        id
      }
    }
  }
`;

export const useCancleMultiAppointment = (onSetIdRes?, onSetModifyVisible?) => {
  const [cancleMultiAppointment, { data, loading, error }] = useMutation(CANCLE_MULTI_APPOINTMENT);
  const handleCancleMultiAppointment = async variables => {
    try {
      const { data } = await cancleMultiAppointment({ variables });
      if (data.cancelMultiAppointment && data.cancelMultiAppointment.code === CODE_RESPONSE.SUCCESS) {
        //onSetIdRes(data['cancelAppointment']['data']['id']);
        openNotificationRight(data.cancelMultiAppointment.message);
        onSetModifyVisible(false);
      } else if (data.cancelMultiAppointment && data.cancelMultiAppointment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.cancelMultiAppointment.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    cancleMultiAppointment: handleCancleMultiAppointment,
    resultCancleMultiAppointment: data,
    isLoadingCancleMultiAppointment: loading,
    errorCancleMultiAppointment: error
  };
};
