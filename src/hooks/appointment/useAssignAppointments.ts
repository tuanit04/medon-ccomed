import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const ASSIGN_APPOINTMENTS = gql`
  mutation assignAppointments($appointmentIds: [String], $staffId: String) @api(name: "appointmentEndpoint") {
    assignAppointments(appointmentIds: $appointmentIds, staffId: $staffId) {
      status
      message
      code
      data {
        id
        appointmentDate
      }
    }
  }
`;

export const useAssignAppointments = () => {
  const [assignAppointments, { data, loading, error }] = useMutation(ASSIGN_APPOINTMENTS);
  const handleAssignAppointments = async variables => {
    try {
      const { data } = await assignAppointments({ variables });
      if (data.assignAppointments && data.assignAppointments.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện phân nhiều lịch thành công.');
      } else if (data.assignAppointments && data.assignAppointments.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.assignAppointmentNew.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    assignAppointments: handleAssignAppointments,
    resultAssignAppointments: data,
    isLoadingAssignAppointments: loading,
    errorAssignAppointments: error
  };
};
