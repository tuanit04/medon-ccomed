import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const ASSIGN_APPOINTMENTS_NEW = gql`
  mutation assignAppointmentNew($appointmentIds: [ObjectInput], $staffId: String!, $appointmentDate: String!)
    @api(name: "appointmentEndpoint") {
    assignAppointmentNew(appointmentIds: $appointmentIds, staffId: $staffId, appointmentDate: $appointmentDate) {
      status
      message
      code
      data {
        id
      }
    }
  }
`;

export const useAssignAppointmentNews = () => {
  const [assignAppointmentNew, { data, loading, error }] = useMutation(ASSIGN_APPOINTMENTS_NEW);
  const handleAssignAppointmentsNew = async variables => {
    try {
      const { data } = await assignAppointmentNew({ variables });
      if (data.assignAppointmentNew && data.assignAppointmentNew.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện phân lịch thành công.');
      } else if (data.assignAppointmentNew && data.assignAppointmentNew.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.assignAppointmentNew.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    assignAppointmentNew: handleAssignAppointmentsNew,
    resultAssignAppointments: data,
    isLoadingAssignAppointments: loading,
    errorAssignAppointments: error
  };
};
