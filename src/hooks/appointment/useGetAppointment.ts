import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_APPOINTMENTS = gql`
  query appointments($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "appointmentEndpoint") {
    appointments(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      code
      status
      message
      page
      pages
      records
      data {
        appointmentId
        appointmentType
        methodType
        appointmentHisId
        patientId
        patientName
        patientAddress
        patientPhone
        workTimeId
        workTimeName
        appointmentNote
        appointmentDate
        assignStaffId
        assignStaffName
        assignStaffDate
        channelId
        channelName
        createDate
        createUser
        updateDate
        updateUser
        status
        statusName
        customerLabel
        reasonNote
        appointmentCode
        assignStaffPhone
        assignStaffName
      }
    }
  }
`;

export const useGetAppointments = variables => {
  const { data, loading, error, refetch } = useQuery(GET_APPOINTMENTS, {
    variables: {
      ...variables,
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    }
  });
  return {
    pagingAppointments: data?.appointments || {},
    isLoadingAppointments: loading,
    errorAppointments: error?.graphQLErrors,
    refetchAppointments: refetch
  };
};

export const useGetAppointmentAsdvisoryInput = variables => {
  const { data, loading, error, refetch } = useQuery(GET_APPOINTMENTS, {
    fetchPolicy: 'network-only',
    variables: {
      ...variables,
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    }
  });
  return {
    pagingAppointments: data?.appointments || {},
    isLoadingAppointments: loading,
    errorAppointments: error?.graphQLErrors,
    refetchAppointments: refetch
  };
};

export const useGetAppointmentsLazy = variables => {
  const [load, { data, loading, error, refetch }] = useLazyQuery(GET_APPOINTMENTS, {
    fetchPolicy: 'network-only',
    variables: {
      ...variables,
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    }
  });
  return {
    loadAppointments: load,
    pagingAppointments: data?.appointments || {},
    isLoadingAppointments: loading,
    errorAppointments: error?.graphQLErrors,
    refetchAppointments: refetch
  };
};
