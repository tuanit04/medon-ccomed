import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const DELETE_APPOINTMENT_COMMENTS = gql`
  mutation deleteAppointmentComment($id: String) @api(name: "appointmentEndpoint") {
    deleteAppointmentComment(id: $id) {
      status
      message
      code
      data {
        id
      }
    }
  }
`;

export const useDeleteAppointmentComments = () => {
  const [deleteAppointmentComments, { data, loading, error }] = useMutation(DELETE_APPOINTMENT_COMMENTS);
  const handleDeleteAppointmentComments = async variables => {
    try {
      const { data } = await deleteAppointmentComments({ variables });
      if (data.deleteAppointmentComment && data.deleteAppointmentComment.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa bản ghi thành công.');
      } else if (data.deleteAppointmentComment && data.deleteAppointmentComment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa bản ghi thất bại.');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    deleteAppointmentComments: handleDeleteAppointmentComments,
    resultDeleteAppointmentComments: data,
    isLoadingDeleteAppointmentComments: loading,
    errorDeleteAppointmentComments: error
  };
};
