import { gql, useLazyQuery } from '@apollo/client';

const GET_APPOINTMENT_OF_ALL_STAFF = gql`
  query findAppointmentOfAllStaff($appointmentDate: String!, $facilityId: String, $parentFacilityId: String!)
    @api(name: "appointmentEndpoint") {
    findAppointmentOfAllStaff(
      appointmentDate: $appointmentDate
      facilityId: $facilityId
      parentFacilityId: $parentFacilityId
    ) {
      status
      code
      message
      page
      pages
      records
      data {
        staffId
        staffCode
        staffName
        workSessionName
        routeName
        vAppointmentOfStaffList {
          status
          amount
          type
        }
      }
    }
  }
`;

export const useGetAppointmentOfAllStaff = () => {
  const [loadAppointmentOfAllStaff, { called, loading, data }] = useLazyQuery(GET_APPOINTMENT_OF_ALL_STAFF, {
    fetchPolicy: 'network-only'
  });
  return {
    appointmentOfAllStaff: data?.findAppointmentOfAllStaff,
    isLoadingAppointmentOfAllStaff: loading,
    loadAppointmentOfAllStaff: loadAppointmentOfAllStaff
  };
};
