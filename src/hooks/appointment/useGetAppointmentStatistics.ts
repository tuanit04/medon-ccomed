import { gql, useQuery } from '@apollo/client';

const GET_APPOINTMENT_STATISTICS = gql`
  query appointmentStatistics($filtered: [FilteredInput], $type: String) @api(name: "appointmentEndpoint") {
    appointmentStatistics(filtered: $filtered, type: $type) {
      status
      code
      message
      data {
        totalRecords
        currentRecords
        futureRecords
      }
    }
  }
`;

export const useAppointmentStatistics = variables => {
  const { data, loading, error, refetch } = useQuery(GET_APPOINTMENT_STATISTICS, variables);
  return {
    appointmentStatistics: data?.appointmentStatistics.data || {},
    isLoadingAppointmentStatistics: loading,
    errorAppointmentStatistics: error?.graphQLErrors,
    refetchAppointmentStatistics: refetch
  };
};
