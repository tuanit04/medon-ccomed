import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_ALL_COUNTRIES = gql`
  query getAllCountries @api(name: "masterEndpoint") {
    getAllCountries {
      code
      message
      page
      pages
      records
      status
      data {
        countryCode
        countryName
        status
        id
      }
    }
  }
`;

export const useGetAllCountriesLazy = () => {
  const [load, { data, loading, error, refetch }] = useLazyQuery(GET_ALL_COUNTRIES);
  return {
    loadCountries: load,
    pagingCountries: data?.getAllCountries || {},
    isLoadingCountries: loading,
    errorCountries: error?.graphQLErrors,
    refetchCountries: refetch
  };
};
