import { gql, useLazyQuery } from '@apollo/client';

const GET_APPOINTMENT_OF_STAFF_BY_ID = gql`
  query findAppointmentOfStaffById($appointmentDate: String, $staffId: String) @api(name: "appointmentEndpoint") {
    findAppointmentOfStaffById(appointmentDate: $appointmentDate, staffId: $staffId) {
      status
      code
      message
      records
      page
      pages
      data {
        appointmentId
        routeName
        appointmentAddress
        appointmentNote
        siteStaffName
        workTimeName
        siteStaffId
        isCheck
        sampleTakenDate
        appointmentHisId
      }
    }
  }
`;

export const useGetAppointmentOfStaffById = () => {
  const [loadAppointmentOfStaffById, { called, loading, data }] = useLazyQuery(GET_APPOINTMENT_OF_STAFF_BY_ID, {
    fetchPolicy: 'network-only'
  });
  return {
    appointmentOfStaffById: data?.findAppointmentOfStaffById,
    isLoadingAppointmentOfStaffById: loading,
    loadAppointmentOfStaffById: loadAppointmentOfStaffById
  };
};
