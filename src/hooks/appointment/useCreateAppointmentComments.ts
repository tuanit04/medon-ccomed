import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const SAVE_APPOINTMENT_COMMENTS = gql`
  mutation saveAppointmentComment($data: AppointmentCommentInput) @api(name: "appointmentEndpoint") {
    saveAppointmentComment(data: $data) {
      status
      message
      code
      data {
        id
      }
    }
  }
`;

export const useSaveAppointmentComments = () => {
  const [saveAppointmentComments, { data, loading, error }] = useMutation(SAVE_APPOINTMENT_COMMENTS);
  const handleSaveAppointmentComments = async variables => {
    try {
      const { data } = await saveAppointmentComments({ variables });
      if (data.saveAppointmentComment && data.saveAppointmentComment.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện thêm bản ghi thành công.', 'success');
      } else if (data.saveAppointmentComment && data.saveAppointmentComment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện thêm bản ghi thất bại.', 'error');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    saveAppointmentComments: handleSaveAppointmentComments,
    resultSaveAppointmentComments: data,
    isLoadingSaveAppointmentComments: loading,
    errorSaveAppointmentComments: error
  };
};
