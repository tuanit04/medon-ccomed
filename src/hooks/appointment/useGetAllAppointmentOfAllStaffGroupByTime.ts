import { gql, useLazyQuery } from '@apollo/client';

const GET_ALL_APPOINTMENT_OF_ALL_STAFF_GROUP_BY_TIME = gql`
  query findAllAppointmentOfAllStaffGroupByTime(
    $appointmentDate: String
    $facilityId: String
    $parentFacilityId: String
  ) @api(name: "appointmentEndpoint") {
    findAllAppointmentOfAllStaffGroupByTime(
      appointmentDate: $appointmentDate
      facilityId: $facilityId
      parentFacilityId: $parentFacilityId
    ) {
      status
      code
      message
      page
      pages
      data {
        staffId
        staffCode
        staffName
        status
        appointmentDate
        amount
        workTimeId
        workTimeName
        workTimeStaffList {
          staffId
          staffName
          staffCode
          status
        }
      }
    }
  }
`;

export const useGetAllAppointmentOfAllStaffGroupByTime = () => {
  const [loadAllAppointmentOfAllStaffGroupByTime, { called, loading, data }] = useLazyQuery(
    GET_ALL_APPOINTMENT_OF_ALL_STAFF_GROUP_BY_TIME,
    {
      fetchPolicy: 'network-only'
    }
  );
  return {
    allAppointmentOfAllStaffGroupByTime: data?.findAllAppointmentOfAllStaffGroupByTime,
    isLoadingAllAppointmentOfAllStaffGroupByTime: loading,
    loadAllAppointmentOfAllStaffGroupByTime: loadAllAppointmentOfAllStaffGroupByTime
  };
};
