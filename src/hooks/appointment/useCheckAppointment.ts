import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const CHECK_APPOINTMENT = gql`
  mutation checkAppointment($appointmentIds: [String]) @api(name: "appointmentEndpoint") {
    checkAppointment(appointmentIds: $appointmentIds) {
      status
      code
      message
      data {
        id
      }
    }
  }
`;

export const useCheckAppointment = () => {
  const [checkAppointment, { data, loading, error }] = useMutation(CHECK_APPOINTMENT);
  const handleCheckAppointment = async variables => {
    try {
      const { data } = await checkAppointment({ variables });
      if (data.checkAppointment && data.checkAppointment.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện check lịch hẹn thành công.');
      } else if (data.checkAppointment && data.checkAppointment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện check lịch hẹn thất bại.');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    checkAppointment: handleCheckAppointment,
    resultCheckAppointment: data,
    isLoadingCheckAppointment: loading,
    errorCheckAppointment: error
  };
};
