import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const FORWARD_APPOINTMENT_DOCTOR_CONSULTANT = gql`
  mutation forwardAppointmentToDoctorConsultant($id: String) @api(name: "appointmentEndpoint") {
    forwardAppointmentToDoctorConsultant(id: $id) {
      code
      status
      message
      data {
        id
      }
    }
  }
`;

export const useForwardAppointment = (onSetIdRes?, onSetModifyVisible?) => {
  const [forwardAppointment, { data, loading, error }] = useMutation(FORWARD_APPOINTMENT_DOCTOR_CONSULTANT);
  const handleForwardAppointment = async variables => {
    try {
      const { data } = await forwardAppointment({ variables });
      if (
        data.forwardAppointmentToDoctorConsultant &&
        data.forwardAppointmentToDoctorConsultant.code === CODE_RESPONSE.SUCCESS
      ) {
        openNotificationRight(data.forwardAppointmentToDoctorConsultant.message);
        onSetIdRes(data?.forwardAppointmentToDoctorConsultant?.data?.id);
        onSetModifyVisible(false);
      } else if (
        data.forwardAppointmentToDoctorConsultant &&
        data.forwardAppointmentToDoctorConsultant.code !== CODE_RESPONSE.SUCCESS
      ) {
        openNotificationRight(data.forwardAppointmentToDoctorConsultant.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    forwardAppointment: handleForwardAppointment,
    resultForwardAppointment: data,
    isLoadingForwardAppointment: loading,
    errorForwardAppointment: error
  };
};
