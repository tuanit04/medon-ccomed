import { gql, useLazyQuery, useQuery } from '@apollo/client';
import { AppointmentResponse } from 'common/interfaces/response/appointmentResponse.interface';

const GET_APPOINTMENT = gql`
  query appointment($id: String) @api(name: "appointmentEndpoint") {
    appointment(id: $id) {
      code
      status
      message
      data {
        id
        appointmentType
        methodType
        appointmentHisId
        patientId
        sid
        pid
        reasonNote
        reasonId
        channelType
        channelId
        contactPhone
        examFacilityId
        sendFacilityId
        specialistId
        streetId
        routeId
        appointmentDate
        workTimeId
        appointmentNote
        indicatedDoctorId
        infoStaffId
        assignStaffId
        originalAmount
        totalAmount
        usingFacilityId
        testingFacilityId
        videoCallRequestId
        isResult
        isShowWeb
        isValid
        reExamDate
        resultNote
        status
        updateDate
        createDate
        createUser
        updateUser
        createUserId
        updateUserId
        appointmentObject
        healthInsuranceCard
        healthDeclaration {
          id
          typeCustomerKBYT
          dateOnset
          nationPassOther
          hasCovid
          dateHascovid
          hasVaccine
          prognostic
          location
          streetHasCovid
          humanHasCovid
          testedCovid
          invoice
          addressProfile
          nameCompany
          emailProfile
          purpose
          taxCode
          addressCompany
          printName
          typeInvoice
          customerLabel
          emailCompany
        }
        patient {
          id
          name
          phone
          address
          sex
          birthDate
          birthYear
          email
          provinceId
          districtId
          wardId
          idNo
          type
          nationality
          relationType
        }
        workTime {
          id
          name
        }
        facility {
          id
          code
          name
          hotline
          address
          image
        }
        packages {
          id
          name
          genCode
        }
        services {
          id
          originalPrice
          salePrice
          indicatedDoctorName
          serviceId
          serviceCode
          serviceName
        }
        comments {
          id
          appointmentId
          content
          status
          updateDate
          createDate
          createUser
          updateUser
          createUserId
          updateUserId
        }
        preDoctorId
        pid
        preDoctorHisId
        indicatedDoctor {
          id
          code
          name
        }
      }
    }
  }
`;

const GET_APPOINTMENT_CONSULTANT = gql`
  query appointmentDetailToConsultant($id: String) @api(name: "appointmentEndpoint") {
    appointmentDetailToConsultant(id: $id) {
      code
      status
      message
      data {
        id
        appointmentType
        methodType
        appointmentHisId
        patientId
        sid
        pid
        reasonNote
        reasonId
        channelType
        channelId
        contactPhone
        examFacilityId
        sendFacilityId
        specialistId
        streetId
        routeId
        appointmentDate
        workTimeId
        appointmentNote
        indicatedDoctorId
        infoStaffId
        assignStaffId
        originalAmount
        totalAmount
        usingFacilityId
        testingFacilityId
        videoCallRequestId
        isResult
        isShowWeb
        isValid
        reExamDate
        resultNote
        status
        updateDate
        createDate
        createUser
        updateUser
        createUserId
        updateUserId
        appointmentObject
        healthInsuranceCard
        patient {
          id
          name
          phone
          address
          sex
          birthDate
          birthYear
          email
          provinceId
          districtId
          wardId
          idNo
          type
          nationality
        }
        workTime {
          id
          name
        }
        facility {
          id
          code
          name
          hotline
          address
          image
        }
        packages {
          id
          name
          genCode
        }
        services {
          id
          originalPrice
          salePrice
          indicatedDoctorName
          serviceId
          serviceCode
          serviceName
        }
        comments {
          id
          appointmentId
          content
          status
          updateDate
          createDate
          createUser
          updateUser
          createUserId
          updateUserId
        }
        preDoctorId
        preDoctorHisId
        indicatedDoctor {
          id
          code
          name
        }
      }
    }
  }
`;

export const useGetAppointmentDetail = () => {
  const [loadAppointmentDetail, { called, loading, data }] = useLazyQuery<{ appointment?: AppointmentResponse }>(
    GET_APPOINTMENT,
    {
      fetchPolicy: 'network-only'
    }
  );
  return {
    appointmentDetail: data?.appointment?.data,
    isLoadingAppointment: loading,
    loadAppointmentDetail: loadAppointmentDetail,
    isCalledAppointment: called
  };
};

export const useAppointmentDetailToConsultant = () => {
  const [loadAppointmentDetailToConsultant, { called, loading, data }] = useLazyQuery(GET_APPOINTMENT_CONSULTANT, {
    fetchPolicy: 'network-only'
  });
  return {
    appointmentDetailToConsultant: data?.appointmentDetailToConsultant,
    isLoadingAppointmentDetailToConsultant: loading,
    loadAppointmentDetailToConsultant: loadAppointmentDetailToConsultant,
    isCalledAppointmentDetailToConsultant: called
  };
};
