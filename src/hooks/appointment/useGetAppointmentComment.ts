import { gql, useLazyQuery } from '@apollo/client';

const GET_APPOINTMENT_COMMENTS = gql`
  query appointmentComments($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "appointmentEndpoint") {
    appointmentComments(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      code
      status
      message
      data {
        id
        updateUser
        updateDate
        status
        appointmentId
        content
        createDate
        createUser
      }
    }
  }
`;

/*export const useGetAppointmentComments = variables => {
  const { data, loading, error, refetch } = useQuery(GET_APPOINTMENT_COMMENTS, {
    variables: { ...variables }
  });
  return {
    pagingAppointmentComments: data?.appointmentComments || {},
    isLoadingAppointmentComments: loading,
    errorAppointmentComments: error?.graphQLErrors,
    refetchAppointmentComments: refetch
  };
};*/

export const useGetAppointmentComments = () => {
  const [loadAppointmentComments, { called, loading, data }] = useLazyQuery(GET_APPOINTMENT_COMMENTS, {
    fetchPolicy: 'network-only'
  });
  return {
    appointmentComments: data?.appointmentComments,
    isLoadingAppointmentComments: loading,
    loadAppointmentComments: loadAppointmentComments
  };
};
