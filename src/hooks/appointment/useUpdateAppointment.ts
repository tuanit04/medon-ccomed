import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { useAppState } from 'helpers';
import { openNotificationRight } from 'utils/notification';

const SAVE_APPOINTMENT = gql`
  mutation updateAppointment($data: AppointmentInput) @api(name: "appointmentEndpoint") {
    updateAppointment(data: $data) {
      status
      message
      code
      data {
        id
        status
        patientName
        packages {
          id
        }
      }
    }
  }
`;

export const useUpdateAppointment = (onSetIdRes, onSetCreateVisible) => {
  const userObject = useAppState(state => state.user.userObject);
  const [updateAppointment, { data, loading, error }] = useMutation(SAVE_APPOINTMENT);
  const handleUpdateAppointment = async variables => {
    try {
      const { data } = await updateAppointment({
        variables: { data: { ...variables?.data, staffCode: userObject?.staffCode } }
      });
      onSetIdRes(data?.['updateAppointment']?.['data']?.['id']);
      if (data.updateAppointment && data.updateAppointment.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.updateAppointment.message, 'success');
        onSetCreateVisible(false);
      } else if (data.updateAppointment && data.updateAppointment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Cập nhật thất bại. Lý do : ' + data.updateAppointment.message, 'error');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message, 'error');
      }
      return data;
    } catch (error) {
      openNotificationRight(error?.message);
    }
  };
  return {
    updateAppointment: handleUpdateAppointment,
    resultUpdateAppointment: data,
    isLoadingUpdateAppointment: loading,
    errorUpdateAppointment: error
  };
};
