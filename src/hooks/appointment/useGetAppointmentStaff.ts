import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_APPOINTMENT_STAFF = gql`
  query findAppointmentStaff($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "appointmentEndpoint") {
    findAppointmentStaff(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      status
      code
      message
      page
      pages
      records
      data {
        sampleTakenDate
        isCheck
        siteStaffName
        workTimeName
        appointmentNote
        appointmentAddress
        routeName
        appointmentId
        appointmentDate
        facilityId
        facilityName
        parentFacilityId
        parentFacilityName
        examFacilityId
        examFacilityName
        workTimeId
        appointmentHisId
        groupId
        classify
        routeId
        siteStaffPhone
        patientName
        patientPhone
      }
    }
  }
`;

export const useGetAppointmentStaff = variables => {
  const { data, loading, error, refetch } = useQuery(GET_APPOINTMENT_STAFF, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    pagingAppointmentStaff: data?.findAppointmentStaff || {},
    isLoadingAppointmentStaff: loading,
    errorAppointmentStaff: error?.graphQLErrors,
    refetchAppointmentStaff: refetch
  };
};

export const useGetAppointmentStaffLazy = variables => {
  const [load, { data, loading, error, refetch }] = useLazyQuery(GET_APPOINTMENT_STAFF, {
    fetchPolicy: 'network-only',
    variables: {
      ...variables
    }
  });
  return {
    loadAppointments: load,
    pagingAppointmentStaff: data?.findAppointmentStaff || {},
    isLoadingAppointmentStaff: loading,
    errorAppointmentStaff: error?.graphQLErrors,
    refetchAppointmentStaff: refetch
  };
};
