import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_APPOINTMENTS = gql`
  query appointmentConsultants($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "appointmentEndpoint") {
    appointmentConsultants(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      code
      status
      message
      page
      pages
      records
      data {
        appointmentId
        appointmentType
        methodType
        appointmentHisId
        patientId
        patientName
        patientAddress
        patientPhone
        workTimeId
        workTimeName
        appointmentNote
        appointmentDate
        assignStaffId
        assignStaffName
        assignStaffDate
        channelId
        channelName
        createDate
        createUser
        updateDate
        updateUser
        status
        statusName
      }
    }
  }
`;

export const useGetAppointmentConsultants = variables => {
  const { data, loading, error, refetch } = useQuery(GET_APPOINTMENTS, {
    fetchPolicy: 'network-only',
    variables: {
      ...variables,
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    }
  });
  return {
    pagingAppointments: data?.appointmentConsultants || {},
    isLoadingAppointments: loading,
    errorAppointments: error?.graphQLErrors,
    refetchAppointments: refetch
  };
};

export const useGetAppointmentConsultantsAsdvisoryInput = variables => {
  const { data, loading, error, refetch } = useQuery(GET_APPOINTMENTS, {
    fetchPolicy: 'network-only',
    variables: {
      ...variables,
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    }
  });
  return {
    pagingAppointments: data?.appointmentConsultants || {},
    isLoadingAppointments: loading,
    errorAppointments: error?.graphQLErrors,
    refetchAppointments: refetch
  };
};

export const useGetAppointmentsConsultantsLazy = variables => {
  const [load, { data, loading, error, refetch }] = useLazyQuery(GET_APPOINTMENTS, {
    fetchPolicy: 'network-only',
    variables: {
      ...variables,
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    }
  });
  return {
    loadAppointments: load,
    pagingAppointments: data?.appointmentConsultants || {},
    isLoadingAppointments: loading,
    errorAppointments: error?.graphQLErrors,
    refetchAppointments: refetch
  };
};
