import { request } from 'api/request';
import { baseURL } from 'config/api';

var testUrl = 'http://58.186.85.189:30882';
//API chuyển lịch thường sang lich Covid 19
export const apiForwardAppointmentCovid = appointmentId =>
  request<any>('get', testUrl + '/medonService/appointment/chuyenLoaiLichHen/' + appointmentId);
