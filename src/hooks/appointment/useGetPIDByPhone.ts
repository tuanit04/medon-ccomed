import { gql, useLazyQuery } from '@apollo/client';
const GET_PID_BY_PHONE = gql`
  query getPIDByPhone($phone: String) @api(name: "masterEndpoint") {
    getPIDByPhone(phone: $phone) {
      code
      status
      message
      data {
        address
        name
        sex
        email
        districtCode
        provinceCode
        birthDate
        birthYear
        pid
        idNo
        phone
      }
    }
  }
`;

export const useGetPIDByPhone = () => {
  const [load, { data, loading, error, refetch }] = useLazyQuery(GET_PID_BY_PHONE, {
    fetchPolicy: 'network-only'
  });
  return {
    loadPIDByPhone: load,
    pIDByPhone: data?.getPIDByPhone || {},
    isLoadingPIDByPhone: loading,
    erroPIDByPhone: error?.graphQLErrors,
    refetchPIDByPhone: refetch
  };
};
