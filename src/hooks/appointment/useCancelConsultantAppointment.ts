import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const CANCLE_APPOINTMENT = gql`
  mutation cancelConsultantAppointment($id: String) @api(name: "appointmentEndpoint") {
    cancelConsultantAppointment(id: $id) {
      status
      code
      message
      data {
        id
      }
    }
  }
`;

export const useCancleConsultantAppointment = (onSetIdRes?, onSetModifyVisible?) => {
  const [cancelConsultantAppointment, { data, loading, error }] = useMutation(CANCLE_APPOINTMENT);
  const handleCancleConsultantAppointment = async variables => {
    try {
      const { data } = await cancelConsultantAppointment({ variables });
      if (data.cancelConsultantAppointment && data.cancelConsultantAppointment.code === CODE_RESPONSE.SUCCESS) {
        onSetIdRes(data['cancelConsultantAppointment']['data']['id']);
        openNotificationRight('Thực hiện hủy lịch hẹn thành công.');
        onSetModifyVisible(false);
      } else if (data.cancelConsultantAppointment && data.cancelConsultantAppointment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện hủy lịch hẹn thất bại.');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    cancleConsultantAppointment: handleCancleConsultantAppointment,
    resultCancleConsultantAppointment: data,
    isLoadingCancleConsultantAppointment: loading,
    errorCancleConsultantAppointment: error
  };
};
