import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { useAppState } from 'helpers';
import { openNotificationRight } from 'utils/notification';

const SAVE_APPOINTMENT = gql`
  mutation saveAppointment($data: AppointmentInput) @api(name: "appointmentEndpoint") {
    saveAppointment(data: $data) {
      status
      message
      code
      data {
        id
      }
    }
  }
`;

const SAVE_APPOINTMENT_AND_FORWARD_CONSULTANT = gql`
  mutation saveAppointmentAndForwardConsultant($data: AppointmentInput) @api(name: "appointmentEndpoint") {
    saveAppointmentAndForwardConsultant(data: $data) {
      status
      message
      code
      data {
        id
      }
    }
  }
`;

export const useCreateAppointment = (
  onSetIdRes,
  onSetCreateVisible,
  handleForwardAppointment?,
  forwardAppointment?
) => {
  const userObject = useAppState(state => state.user.userObject);
  const [saveAppointment, { data, loading, error }] = useMutation(SAVE_APPOINTMENT);
  const handleCreateAppointment = async variables => {
    try {
      const { data } = await saveAppointment({
        variables: { data: { ...variables?.data, staffCode: userObject?.staffCode } }
      });
      if (data.saveAppointment && data.saveAppointment.code === CODE_RESPONSE.SUCCESS) {
        localStorage.removeItem('IS_SHOW_CREATE_APPOINTMENT_DIALOG');
        localStorage.removeItem('appoiment-create-data');
        onSetIdRes(data['saveAppointment']['data']['id']);
        openNotificationRight(data.saveAppointment.message);
        onSetCreateVisible(false);
      } else if (data.saveAppointment && data.saveAppointment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thêm mới thất bại. Lý do : ' + data.saveAppointment.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    createAppointment: handleCreateAppointment,
    resultSaveAppointment: data,
    isLoadingSaveAppointment: loading,
    errorSaveAppointment: error
  };
};

export const useCreateAppointmentAndForwardConsultant = (
  onSetIdRes,
  onSetCreateVisible,
  handleForwardAppointment?,
  forwardAppointment?
) => {
  const [saveAppointmentAndForwardConsultant, { data, loading, error }] = useMutation(
    SAVE_APPOINTMENT_AND_FORWARD_CONSULTANT
  );
  const handleCreateAppointmentAndForwardConsultant = async variables => {
    try {
      const { data } = await saveAppointmentAndForwardConsultant({ variables });
      if (data.saveAppointmentAndForwardConsultant.code === CODE_RESPONSE.SUCCESS) {
        localStorage.removeItem('IS_SHOW_CREATE_APPOINTMENT_DIALOG');
        localStorage.removeItem('appoiment-create-data');
        onSetIdRes(data['saveAppointmentAndForwardConsultant']['data']['id']);
        openNotificationRight(data.saveAppointmentAndForwardConsultant.message);
        onSetCreateVisible(false);
      } else {
        openNotificationRight('Thêm mới thất bại. Lý do : ' + data.saveAppointmentAndForwardConsultant.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    createAppointmentAndForwardConsultant: handleCreateAppointmentAndForwardConsultant,
    resultSaveAppointmentAndForwardConsultant: data,
    isLoadingSaveAppointmentAndForwardConsultant: loading,
    errorSaveAppointmentAndForwardConsultant: error
  };
};
