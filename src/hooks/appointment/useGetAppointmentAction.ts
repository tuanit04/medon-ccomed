import { gql, useQuery } from '@apollo/client';
import { AppointmentActionListResponse } from 'common/interfaces/response/appointmentActionListResponse.interface';

const GET_APPOINTMENT_ACTION = gql`
  query actionByAppointmentId($appointmentId: String) @api(name: "appointmentEndpoint") {
    actionByAppointmentId(appointmentId: $appointmentId) {
      status
      code
      message
      data {
        id
        appointmentId
        appointmentHisId
        actionName
        actionUserId
        actionUser
        actionTime
        actionNote
      }
    }
  }
`;

export const useGetAppointmentActions = (variables: { appointmentId?: string }) => {
  const { data, loading, error, refetch } = useQuery<{ actionByAppointmentId: AppointmentActionListResponse }>(
    GET_APPOINTMENT_ACTION,
    {
      fetchPolicy: 'network-only',
      variables
    }
  );

  return {
    data: data?.actionByAppointmentId?.data,
    loading,
    error: error?.graphQLErrors,
    refetch
  };
};
