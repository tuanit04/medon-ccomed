import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const ASSIGN_APPOINTMENT = gql`
  mutation assignAppointment($appointmentId: String, $staffId: String) @api(name: "appointmentEndpoint") {
    assignAppointment(appointmentId: $appointmentId, staffId: $staffId) {
      status
      message
      code
      data {
        id
        appointmentDate
      }
    }
  }
`;

export const useAssignAppointment = () => {
  const [assignAppointment, { data, loading, error }] = useMutation(ASSIGN_APPOINTMENT);
  const handleAssignAppointment = async variables => {
    try {
      const { data } = await assignAppointment({ variables });
      if (data.assignAppointment && data.assignAppointment.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện phân lịch thành công.');
      } else if (data.assignAppointment && data.assignAppointment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.assignAppointmentNew.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    assignAppointment: handleAssignAppointment,
    resultAssignAppointment: data,
    isLoadingAssignAppointment: loading,
    errorAssignAppointment: error
  };
};
