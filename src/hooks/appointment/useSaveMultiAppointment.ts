import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { useAppState } from 'helpers';
import { openNotificationRight } from 'utils/notification';

const SAVE_MULTI_APPOINTMENT = gql`
  mutation saveMultiAppointment($data: AppointmentInput) @api(name: "appointmentEndpoint") {
    saveMultiAppointment(data: $data) {
      status
      message
      code
      data {
        id
      }
    }
  }
`;

export const useSaveMultiAppointment = onSetIdRes => {
  const userObject = useAppState(state => state.user.userObject);
  const [saveMultiAppointment, { data, loading, error }] = useMutation(SAVE_MULTI_APPOINTMENT);
  const handleSaveMultiAppointment = async variables => {
    try {
      const { data } = await saveMultiAppointment({
        variables: { data: { ...variables?.data, staffCode: userObject?.staffCode } }
      });
      onSetIdRes(data?.['saveMultiAppointment']?.['data']?.['id']);
      if (data.saveMultiAppointment && data.saveMultiAppointment.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.saveMultiAppointment.message, 'success');
      } else if (data.saveMultiAppointment && data.saveMultiAppointment.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thêm mới thất bại. Lý do : ' + data.saveMultiAppointment.message, 'error');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    saveMultiAppointment: handleSaveMultiAppointment,
    resultSaveMultiAppointment: data,
    isLoadingSaveMultiAppointment: loading,
    errorSaveMultiAppointment: error
  };
};
