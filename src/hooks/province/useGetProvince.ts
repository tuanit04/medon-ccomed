import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_PROVINCE = gql`
  query provinces @api(name: "masterEndpoint") {
    provinces {
      code
      message
      data {
        id
        code
        name
        status
      }
    }
  }
`;
export const useGetProvincesLazy = () => {
  const [loadProvinces, { called, loading, data }] = useLazyQuery(GET_PROVINCE);
  return {
    provinces: data?.provinces?.data,
    isLoadingProvince: loading,
    loadProvinces: loadProvinces
  };
};
export const useGetProvinces = () => {
  const { data, loading, error, refetch } = useQuery(GET_PROVINCE);
  return {
    pagingProvinces: data?.provinces || {},
    isLoadingProvinces: loading,
    errorProvinces: error?.graphQLErrors,
    refetchProvinces: refetch
  };
};
