import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_RELATE_SERVICES = gql`
  # Write your query or mutation here
  query findRelateServices($serviceId: String) @api(name: "masterEndpoint") {
    findRelateServices(serviceId: $serviceId) {
      code
      message
      page
      pages
      records
      data {
        serviceName
        serviceCode
        originPrice
        hosSalePrice
        homeSalePrice
      }
    }
  }
`;
/*export const useGetServices = () => {
  const [loadServices, { called, loading, data }] = useLazyQuery(GET_SERVICE);
  return {
    services: data?.findAllServices?.data,
    isLoadingServices: loading,
    loadServices: loadServices
  };
};*/

export const useGetRelateServices = variables => {
  const { data, loading, error, refetch } = useQuery(GET_RELATE_SERVICES, {
    variables: { ...variables }
  });
  return {
    pagingRelateServices: data?.findRelateServices || {},
    isLoadingRelateServices: loading,
    errorRelateServices: error?.graphQLErrors,
    refetchRelateServices: refetch
  };
};

export const useGetRelateServicesLazy = variables => {
  const [loadServices, { called, loading, data }] = useLazyQuery(GET_RELATE_SERVICES, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    pagingRelateServices: data?.findAllServices || {},
    isLoadingRelateServices: loading,
    loadServices
  };
};
