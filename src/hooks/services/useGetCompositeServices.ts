import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_COMPOSITE_SERVICES = gql`
  # Write your query or mutation here
  query findCompositeServices($serviceId: String) @api(name: "masterEndpoint") {
    findCompositeServices(serviceId: $serviceId) {
      code
      message
      page
      pages
      records
      data {
        name
        code
        generalMeaning
        detailMeaning
        isDisplay
        isUsed
      }
    }
  }
`;
/*export const useGetServices = () => {
  const [loadServices, { called, loading, data }] = useLazyQuery(GET_SERVICE);
  return {
    services: data?.findAllServices?.data,
    isLoadingServices: loading,
    loadServices: loadServices
  };
};*/

export const useGetCompositeServices = variables => {
  const { data, loading, error, refetch } = useQuery(GET_COMPOSITE_SERVICES, {
    variables: { ...variables }
  });
  return {
    pagingCompositeServices: data?.findCompositeServices || {},
    isLoadingCompositeServices: loading,
    errorCompositeServices: error?.graphQLErrors,
    refetchCompositeServices: refetch
  };
};

export const useGetCompositeServicesLazy = variables => {
  const [refetchCompositeServices, { called, loading, data }] = useLazyQuery(GET_COMPOSITE_SERVICES, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    pagingCompositeServices: data?.findAllServices || {},
    isLoadingCompositeServices: loading,
    refetchCompositeServices
  };
};
