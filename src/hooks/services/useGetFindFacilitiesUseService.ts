import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_FACILITY_USE_SERVICE = gql`
  # Write your query or mutation here
  query findFacilitiesUseService($serviceId: String) @api(name: "masterEndpoint") {
    findFacilitiesUseService(serviceId: $serviceId) {
      code
      status
      message
      page
      pages
      records
      data {
        facilityName
        facilityId
        originPrice
        hosSalePrice
        homeSalePrice
        returnDuration
      }
    }
  }
`;
/*export const useGetServices = () => {
  const [loadServices, { called, loading, data }] = useLazyQuery(GET_SERVICE);
  return {
    services: data?.findAllServices?.data,
    isLoadingServices: loading,
    loadServices: loadServices
  };
};*/

export const useGetFacilitiesUseService = variables => {
  const { data, loading, error, refetch } = useQuery(GET_FACILITY_USE_SERVICE, {
    variables: { ...variables }
  });
  return {
    pagingFacilitiesUseService: data?.findFacilitiesUseService || {},
    isLoadingFacilitiesUseService: loading,
    errorFacilitiesUseService: error?.graphQLErrors,
    refetchFacilitiesUseServices: refetch
  };
};

export const useGetFacilitiesUseServiceLazy = variables => {
  const [refetchFacilitiesUseServices, { called, loading, data }] = useLazyQuery(GET_FACILITY_USE_SERVICE, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    pagingFacilitiesUseService: data?.findAllServices || {},
    isLoadingFacilitiesUseService: loading,
    refetchFacilitiesUseServices
  };
};