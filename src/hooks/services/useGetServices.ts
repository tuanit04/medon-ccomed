import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_SERVICE = gql`
  query findAllServices($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "masterEndpoint") {
    findAllServices(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      code
      message
      page
      pages
      records
      data {
        serviceName
        serviceId
        serviceCode
        originPrice
        hosSalePrice
        homeSalePrice
        isProfile
        facilityId
        facilityName
        contractCode
        isDisplay
        isUsed
        serviceType
        returnDuration
        generalMeaning
        detailMeaning
        serviceTypeId
      }
    }
  }
`;

export const useGetServicesLazy = variables => {
  const [loadServices, { called, loading, data }] = useLazyQuery(GET_SERVICE, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    services: data?.findAllServices || {},
    isLoadingServices: loading,
    loadServices
  };
};

export const useGetServices = variables => {
  const { data, loading, error, refetch } = useQuery(GET_SERVICE, {
    fetchPolicy: 'network-only',
    variables: { ...variables }
  });
  return {
    pagingServices: data?.findAllServices || {},
    isLoadingServices: loading,
    errorServices: error?.graphQLErrors,
    refetchServices: refetch
  };
};
