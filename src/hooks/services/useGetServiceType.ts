import { gql, useQuery } from '@apollo/client';

const GET_SERVICE_TYPE = gql`
  query serviceTypes @api(name: "masterEndpoint") @api(name: "masterEndpoint") {
    serviceTypes {
      code
      message
      status
      page
      pages
      records
      data {
        id
        name
        description
      }
    }
  }
`;

export const useGetServiceType = () => {
  const { data, loading, error, refetch } = useQuery(GET_SERVICE_TYPE);
  return {
    pagingServiceType: data?.serviceTypes || {},
    isLoadingServiceType: loading,
    errorServiceType: error?.graphQLErrors,
    refetchServiceType: refetch
  };
};
