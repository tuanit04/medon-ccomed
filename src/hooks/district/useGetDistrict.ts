import { gql, useLazyQuery } from '@apollo/client';

const GET_DISTRICT = gql`
  query findDistrictByProvinceId($provinceId: String!) @api(name: "masterEndpoint") {
    findDistrictByProvinceId(provinceId: $provinceId) {
      code
      message
      data {
        id
        code
        name
        status
      }
    }
  }
`;
export const useGetDistricts = () => {
  const [loadDistricts, { called, loading, data }] = useLazyQuery(GET_DISTRICT, {fetchPolicy: 'network-only'});
  return {
    districts: data?.findDistrictByProvinceId?.data,
    isLoadingDistricts: loading,
    loadDistricts: loadDistricts
  };
};
