import { gql, useQuery } from '@apollo/client';

const FIND_STREET_STATISTIC = gql`
  query findStreetStatistic @api(name: "masterEndpoint") {
    findStreetStatistic {
      code
      message
      data {
        totalRecords
        activeRecords
        inactiveRecords
      }
    }
  }
`;

export const useFindStreetStatistic = () => {
  const { data, loading, error, refetch } = useQuery(FIND_STREET_STATISTIC);
  return {
    streetStatistic: data?.findStreetStatistic.data || {},
    isLoadingStreetStatistic: loading,
    errorStreetStatistic: error?.graphQLErrors,
    refetchStreetStatistic: refetch
  };
};
