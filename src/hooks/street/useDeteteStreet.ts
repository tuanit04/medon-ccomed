import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';
import { GET_STREETS } from './useGetStreet';

const DELETE_STREET = gql`
  mutation deleteStreet($id: String!) @api(name: "masterEndpoint") {
    deleteStreet(id: $id) {
      code
      status
      message
      data {
        id
      }
    }
  }
`;
export const useDeleteStreet = () => {
  const [deleteStreet, { data, loading, error }] = useMutation(DELETE_STREET);

  const handleDeleteStreet = async variables => {
    try {
      const { data } = await deleteStreet({ variables });
      if (data.deleteStreet && data.deleteStreet.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa bản ghi thành công.');
      } else if (data.deleteStreet && data.deleteStreet.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện xóa bản ghi thất bại.');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      openNotificationRight(error.graphQLErrors);
    }
  };

  return {
    deleteStreet: handleDeleteStreet,
    result: data,
    isLoading: loading,
    error
  };
};
