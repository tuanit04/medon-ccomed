import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';
interface StreetInput {
  data: {
    name: string;
    code: string;
    routeId: string;
    wardId: string;
    districtId: string;
    provinceId: string;
    status: number;
    updateDate: Date;
    createDate: Date;
    createUser: string;
    updateUser: string;
    createUserId: string;
    updateUserId: string;
    id: string;
  };
}

const CREATE_STREET = gql`
  mutation saveStreet($data: StreetInput!) @api(name: "masterEndpoint") {
    saveStreet(data: $data) {
      code
      message
      data {
        name
        code
        id
      }
    }
  }
`;

export const useCreateStreet = onSetIdRes => {
  const [createStreet, { data, loading, error }] = useMutation(CREATE_STREET);
  const handleCreateStreet = async variables => {
    try {
      const { data } = await createStreet({ variables });
      onSetIdRes(data?.['saveStreet']?.['data']?.['id']);
      if (data.saveStreet && data.saveStreet.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.saveStreet.message);
      } else if (data.saveStreet && data.saveStreet.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.saveStreet.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };

  return {
    createStreet: handleCreateStreet,
    resultCreateStreet: data,
    isLoadingCreateStreet: loading,
    errorCreateStreet: error
  };
};
