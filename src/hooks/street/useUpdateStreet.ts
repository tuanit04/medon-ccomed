import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const UPDATE_STREET = gql`
  mutation saveStreet($data: StreetInput!) @api(name: "masterEndpoint") {
    saveStreet(data: $data) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useUpdateStreet = onSetIdRes => {
  const [updateStreet, { data, loading, error }] = useMutation(UPDATE_STREET);
  const handleUpdateStreet = async variables => {
    try {
      const { data } = await updateStreet({ variables });
      onSetIdRes(data?.['saveStreet']?.['data']?.['id']);
      if (data.saveStreet && data.saveStreet.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.saveStreet.message, 'success');
      } else if (data.saveStreet && data.saveStreet.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.saveStreet.message, 'error');
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message, 'error');
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };

  return {
    updateStreet: handleUpdateStreet,
    resultUpdateStreet: data,
    isLoadingUpdateStreet: loading,
    errorUpdateStreet: error
  };
};
