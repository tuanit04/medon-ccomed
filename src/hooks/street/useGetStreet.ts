import { gql, useLazyQuery, useQuery } from '@apollo/client';
const GET_STREETS = gql`
  query streets($page: Int, $pageSize: Int, $filtered: [FilteredInput], $sorted: [SortedInput])
    @api(name: "masterEndpoint") {
    streets(page: $page, pageSize: $pageSize, filtered: $filtered, sorted: $sorted) {
      code
      message
      data {
        id
        code
        name
        provinceId
        districtId
        provinceName
        districtName
        wardId
        parentFacilityId
        facilityId
        routeId
        routeName
        status
        createDate
        createUser
        updateUser
        updateDate
      }
      page
      pages
      records
    }
  }
`;

const GET_STREET_DETAIL = gql`
  query getStreetDetail($streetId: String!) @api(name: "masterEndpoint") {
    street(streetId: $streetId) {
      code
      message
      data {
        id
        code
        name
        status
        routeId
        provinceId
        districtId
        wardId
      }
    }
  }
`;

export const useGetStreets = variables => {
  const { data, loading, error, refetch } = useQuery(GET_STREETS, {
    fetchPolicy: 'network-only',
    variables: {
      ...variables,
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    }
  });
  return {
    pagingStreets: data?.streets || {},
    isLoading: loading,
    error: error?.graphQLErrors,
    refetchStreets: refetch
  };
};

export const useGetStreetsLazy = () => {
  const [loadStreets, { called, loading, data }] = useLazyQuery(GET_STREETS, {
    fetchPolicy: 'network-only'
  });
  return {
    streets: data?.streets?.data,
    isLoadingStreets: loading,
    loadStreets: loadStreets
  };
};

export const useGetStreetDetail = () => {
  const [loadStreetDetail, { called, loading, data }] = useLazyQuery(GET_STREET_DETAIL);
  return {
    streetDetail: data?.street?.data,
    isLoadingStreetDetail: loading,
    loadStreetDetail: loadStreetDetail
  };
};

export { GET_STREETS, GET_STREET_DETAIL };
