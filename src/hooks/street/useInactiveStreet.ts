import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const INACTIVE_STREET = gql`
  mutation deActiveStreet($id: String!) @api(name: "masterEndpoint") {
    deActiveStreet(id: $id) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useInactiveStreet = () => {
  const [inactiveStreet, { data, loading, error }] = useMutation(INACTIVE_STREET);
  const handleInactiveStreet = async variables => {
    try {
      const { data } = await inactiveStreet({ variables });
      if (data.deActiveStreet && data.deActiveStreet.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện cập nhật trạng thái bản ghi thành công.');
      } else if (data.deActiveStreet && data.deActiveStreet.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.deActiveStreet.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    inactiveStreet: handleInactiveStreet,
    resultInactiveStreet: data,
    isLoadingInactiveStreet: loading,
    errorInactiveStreet: error
  };
};
