import { gql, useMutation } from '@apollo/client';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';

const ACTIVE_STREET = gql`
  mutation activeStreet($id: String!) @api(name: "masterEndpoint") {
    activeStreet(id: $id) {
      code
      message
      data {
        id
      }
    }
  }
`;

export const useActiveStreet = () => {
  const [activeStreet, { data, loading, error }] = useMutation(ACTIVE_STREET);
  const handleActiveStreet = async variables => {
    try {
      const { data } = await activeStreet({ variables });
      if (data.activeStreet && data.activeStreet.code === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Thực hiện cập nhật trạng thái bản ghi thành công.');
      } else if (data.activeStreet && data.activeStreet.code !== CODE_RESPONSE.SUCCESS) {
        openNotificationRight(data.activeStreet.message);
      } else if (data.response && data.response.code === CODE_RESPONSE.ACCESS_DENIED) {
        openNotificationRight(data.response.message);
      }
      return data;
    } catch (error) {
      console.log(error.graphQLErrors);
      openNotificationRight(error.graphQLErrors);
    }
  };
  return {
    activeStreet: handleActiveStreet,
    resultActiveStreet: data,
    isLoadingActiveStreet: loading,
    errorActiveStreet: error
  };
};
