import { gql, useLazyQuery, useQuery } from '@apollo/client';

const GET_DOCTORS = gql`
  query doctors($page: Int, $pageSize: Int, $sorted: [SortedInput], $filtered: [FilteredInput])
    @api(name: "masterEndpoint") {
    doctors(page: $page, pageSize: $pageSize, sorted: $sorted, filtered: $filtered) {
      code
      message
      page
      pages
      records
      data {
        id
        name
        status
        phone
        collaboratorCode
        doctorHisId
        email
        title
        organization
        experience
      }
    }
  }
`;

const GET_DOCTOR = gql`
  query doctor($id: String!) @api(name: "masterEndpoint") {
    doctor(id: $id) {
      code
      message
      status
      data {
        id
        name
        status
        phone
        email
        title
        organization
        experience
        collaboratorCode
        doctorHisId
      }
    }
  }
`;

export const useGetDoctors = variables => {
  const { data, loading, error, refetch } = useQuery(GET_DOCTORS, {
    fetchPolicy: 'network-only',
    variables: {
      ...variables,
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    }
  });
  return {
    pagingDoctors: data?.doctors || {},
    isLoadingDoctors: loading,
    errorDoctors: error?.graphQLErrors,
    refetchDoctors: refetch
  };
};

export const useGetDoctorsLazy = () => {
  const [loadDoctors, { called, loading, data }] = useLazyQuery(GET_DOCTORS, {
    fetchPolicy: 'network-only'
  });
  return {
    pagingDoctors: data?.doctors || {},
    isLoadingDoctors: loading,
    loadDoctors
  };
};

export const useGetDoctor = () => {
  const [loadDoctor, { called, loading, data }] = useLazyQuery(GET_DOCTOR, {
    fetchPolicy: 'network-only'
  });
  return {
    doctor: data?.doctor?.data || {},
    isLoadingDoctor: loading,
    loadDoctor
  };
};
