import { gql, useLazyQuery } from '@apollo/client';

const GET_WARD = gql`
  query findWardByDistrictId($districtId: String!) @api(name: "masterEndpoint") {
    findWardByDistrictId(districtId: $districtId) {
      code
      message
      data {
        code
        name
        id
      }
    }
  }
`;
export const useGetWards = () => {
  const [loadWards, { called, loading, data }] = useLazyQuery(GET_WARD,{fetchPolicy: 'network-only'});
  return {
    wards: data?.findWardByDistrictId?.data,
    isLoadingWards: loading,
    loadWards: loadWards
  };
};
