import { gql, useLazyQuery, useQuery } from '@apollo/client';
import { Facility } from '../../common/interfaces/facility.interface';

const GET_FACILITY = gql`
  query facilities($page: Int, $pageSize: Int, $filtered: [FilteredInput]) @api(name: "masterEndpoint") {
    facilities(page: $page, pageSize: $pageSize, filtered: $filtered) {
      code
      message
      data {
        id
        name
        address
        parentId
        status
        code
        provinceIds
        provinceId
      }
    }
  }
`;

export const useGetFacilitys = () => {
  const [loadFacilities, { called, loading, data }] = useLazyQuery(GET_FACILITY, {
    fetchPolicy: "network-only",   
    nextFetchPolicy: "cache-first" 
  });
  return {
    facilities: data?.facilities?.data,
    isLoadingFacilities: loading,
    loadFacilities: loadFacilities
  };
};

export const useGetFacilitysHos = () => {
  const [loadFacilitiesHos, { called, loading, data }] = useLazyQuery(GET_FACILITY, {
    fetchPolicy: "network-only",  
    nextFetchPolicy: "cache-first" 
  });
  return {
    facilitiesHos: data?.facilities?.data,
    isLoadingFacilitiesHos: loading,
    loadFacilitiesHos: loadFacilitiesHos
  };
};

export const useGetFacilityParents = (): Facility[] => {
  const { data } = useQuery(GET_FACILITY, {
    variables: {
      page: 1,
      filtered: [
        {
          id: 'status',
          value: '1',
          operation: '=='
        }
      ]
    }
  });
  return data?.facilities?.data;
};

export const useGetFacilityHome = (): Facility[] => {
  const { data } = useQuery(GET_FACILITY, {
    variables: {
      page: 1,
      filtered: [
        {
          id: 'isHome',
          value: '1',
          operation: '=='
        },
        {
          id: 'status',
          value: '1',
          operation: '=='
        },
        {
          id: 'parentId',
          value: '',
          operation: 'is_null'
        }
      ]
    }
  });
  return data?.facilities?.data;
};
