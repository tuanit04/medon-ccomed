import { gql, useLazyQuery } from '@apollo/client';

const GET_ALL_WORKTIME_FACILITY = gql`
  query findAllWorkTimeFacility($type: String, $facilityId: String) @api(name: "masterEndpoint") {
    findAllWorkTimeFacility(type: $type, facilityId: $facilityId) {
      code
      message
      page
      pages
      records
      status
      data {
        name
        type
        status
        startTime
        endTime
        id
      }
    }
  }
`;
export const useGetAllWorkTimeFacility = () => {
  const [loadAllWorkTimeFacility, { called, loading, data }] = useLazyQuery(GET_ALL_WORKTIME_FACILITY);
  return {
    allWorkTimeFacility: data?.findAllWorkTimeFacility?.data,
    isLoadingAllWorkTimeFacility: loading,
    loadAllWorkTimeFacility: loadAllWorkTimeFacility
  };
};


