import { gql, useLazyQuery } from '@apollo/client';
const GET_All_WORKTIME = gql`
  query workTimes($type: String) @api(name: "masterEndpoint") {
    workTimes(type:$type) {
      code
      message
      records
      data {
        id
        name
        startTime
        endTime
      }
    }
  }
`;
export const useGetAllWorktimes = () => {
  const [loadWorktimes, { called, loading, data }] = useLazyQuery(GET_All_WORKTIME);
  return {
    worktimes: data?.workTimes?.data,
    isLoadingWorktimes: loading,
    loadWorktimes: loadWorktimes
  };
};