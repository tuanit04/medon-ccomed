import { request } from 'api/request';
import { GroupProcess } from 'common/interfaces/group-process.interface';
import { TicketType } from 'common/interfaces/ticketType.interface';
import { baseURL } from 'config/api';

/** Get TicketType list API */
export const apiGetGroupProcessList = (page?, pageSize?) =>
  request<GroupProcess[]>('get', baseURL + '/ticket/findAllGroupProcess', {});
/** Create TicketType API */
export const apiCreateGroupProcess = groupProcessObj =>
  request<GroupProcess>('post', baseURL + '/ticket/saveGroupProcess', { ...groupProcessObj });
/** Delete TicketType API */
export const apiDeleteGroupProcess = id =>
  request<GroupProcess>('get', baseURL + '/ticket/deleteGroupProcess?id=' + id);
