import { request, requestLogin } from './request';
import {
  baseURL,
  getCallStatisticalByLineUrl,
  getListCallOngoingUrl,
  getListCallUrl,
  getListCallUserUrl,
  getPatientByPhoneUrl,
  saveAgentStatusUrl,
  saveCallLogUrl,
  vccAgentListUrl,
  vccLoginUrl
} from 'config/api';
import {
  LoginVCCResult,
  SaveAgentStatusParams,
  SaveAgentStatusResult,
  SaveCallLogParams,
  SaveCallLogResult,
  SavePatientDialogParams,
  SavePatientDialogResult
} from 'interface/user/vcc';
import { getLocalAccessToken } from './token.service';

export const apiVCCLogin = (token: string, domain: string) =>
  requestLogin<LoginVCCResult>(vccLoginUrl(domain), {
    token
  });

export const getAgentListVCC = () =>
  request<CSAgentInfo[]>(
    'get',
    vccAgentListUrl,
    {},
    {
      headers: { Token: localStorage.getItem('t') ?? '' }
    }
  );

export const saveCallLog = (data: SaveCallLogParams) =>
  request<SaveCallLogResult>('post', saveCallLogUrl, data, {
    headers: { Token: localStorage.getItem('t') ?? '' }
  });

export const savePatientDialog = (data: SavePatientDialogParams) =>
  request<SavePatientDialogResult>('post', baseURL + '/master/api/v1/master/saveInfoPatient', data, {
    headers: { Token: localStorage.getItem('t') ?? '' }
  });

export const saveAgentStatus = (data: SaveAgentStatusParams) =>
  request<SaveAgentStatusResult>('post', saveAgentStatusUrl, data, {
    headers: { Token: getLocalAccessToken() ?? '' }
  });

export const getPatientByPhone = (phone: string) =>
  request('post', getPatientByPhoneUrl, { phone }, { headers: { Token: getLocalAccessToken() ?? '' } });

export const getListCallUser = (data: IRequest) =>
  request<ListCall>('post', getListCallUserUrl, data, {
    headers: { Token: getLocalAccessToken() ?? '' }
  });

export const getListCall = (data: IRequest) =>
  request<ListCallUser>('post', getListCallUrl, data, {
    headers: { Token: getLocalAccessToken() ?? '' }
  });

export const getCallStatisticalByLine = () =>
  request<StatisticCallByLine>(
    'get',
    getCallStatisticalByLineUrl,
    {},
    {
      headers: { Token: getLocalAccessToken() ?? '' }
    }
  );

export const getListCallOngoing = (data: IRequest) =>
  request<ListCallUserOngoing>('post', getListCallOngoingUrl, data, {
    headers: { Token: getLocalAccessToken() ?? '' }
  });
