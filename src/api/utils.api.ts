import axios from 'axios';

export const AuthHeader = () => {
  let token = '';
  const data = localStorage.getItem('user');
  if (data) {
    const user = JSON.parse(data);
    token = user?.token;
  }
  if (token) {
    return {
      Token: token,
      'Accept-Language': 'vi',
      'Content-Type': 'application/json'
    };
  }
};

//audio format: ogg, oga, flac, wav, mp3
export const ApiUploadFile = {
  url: 'http://58.186.85.189:31492/utility/images/uploadAudio'
};

const header = AuthHeader();

export function uploadFile<T = any>(file) {
  let FormData = require('form-data');
  let data = new FormData();
  data.append('file', file);
  return axios.post<T>(ApiUploadFile.url, data, { headers: header });
}
