import { request } from 'api/request';
import { TicketProcess } from 'common/interfaces/ticket-process.interface';
import { TicketType } from 'common/interfaces/ticketType.interface';
import { baseURL } from 'config/api';

/** Get TicketType list API */
export const apiGetTicketTypeList = (page?, pageSize?) =>
  request<TicketType[]>('get', baseURL + '/ticket/findAllTicketType', {});
/** Lấy SLA theo TicketType ID */
export const apiGetSlaOfTicketType = id =>
  request<TicketType[]>('get', baseURL + '/ticket/findSlaOfTicketType?id=' + id);
/** Save SLA API */
export const apiSaveSlaOfTicketType = obj => request<TicketType>('post', baseURL + '/ticket/saveSlaOfTicketType', obj);
/** Create TicketType API */
export const apiCreateTicketType = ticketTypeObj =>
  request<TicketType>('post', baseURL + '/ticket/saveTicketType', { ...ticketTypeObj });
/** Save TicketProcess API */
export const apiSaveTicketProcess = ticketProcessObj =>
  request<TicketProcess>('post', baseURL + '/ticket/saveTicketProcess', { ...ticketProcessObj });
/** Delete TicketType API */
export const apiDeleteTicketType = id => request<TicketType>('get', baseURL + '/ticket/deleteTicketType?id=' + id);
