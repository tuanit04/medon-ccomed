import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { logoutAsync } from 'store/user.store';
import { store } from 'helpers';
import { getLocalAccessToken, getLocalRefreshToken, updateLocalAccessToken } from './token.service';
import { baseURL, refreshTokenUrl, serviceUrl } from 'config/api';
import { RefreshTokenResult } from 'interface/user/login';
import * as AxiosLogger from 'axios-logger';

const getHeader = () => {
  return {
    'Accept-Language': 'vi',
    Token: getLocalAccessToken()
  };
};

const axiosInstance = axios.create({
  timeout: 60000
});

const axiosInstanceRest = axios.create({
  timeout: 60000,
  baseURL: serviceUrl
});

export const refreshToken = async (instance: AxiosInstance = axiosInstance) => {
  try {
    const res: IResponse<RefreshTokenResult> = await instance.post(
      refreshTokenUrl,
      {
        Token: getLocalAccessToken(),
        'Refresh-Token': getLocalRefreshToken()
      },
      {
        headers: {
          Token: getLocalAccessToken(),
          'Refresh-Token': getLocalRefreshToken(),
          'Cache-Control': 'no-cache',
          'Access-Control-Allow-Origin': '*',
          'Accept-Language': 'vi'
        }
      }
    );
    const data = res?.data;
    updateLocalAccessToken(data?.token, data?.tokenVcc);
    return data?.token;
  } catch (err) {
    return null;
  }
};

axiosInstance.interceptors.request.use(
  config => {
    return AxiosLogger.requestLogger(config);
  },
  error => {
    Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  async (res: AxiosResponse<any>) => {
    if (
      res?.data?.code === 'TOKEN_NOT_VALID' ||
      res?.data?.code === 'TOKEN_NOT_EXIST_IN_REDIS' ||
      res?.data?.code === 'REFRESH_TOKEN_EXPIRED' ||
      res?.data?.code === 'TOKEN_VALID_EXCEPTION'
    ) {
      store.dispatch(logoutAsync('Token không hợp lệ, vui lòng đăng nhập lại.'));
    } else if (res?.data?.status === 0 && res?.data?.code === 'TOKEN_EXPIRED') {
      const token = await refreshToken();
      if (token) {
        return axiosInstance({
          ...res.config,
          headers: getHeader()
        });
      }
    }
    return res?.data;
  },
  async err => {
    const originalConfig = err.config;
    if (originalConfig?.url !== baseURL + '/auth/login' && err?.response) {
      if (err.response.status === 401 && !originalConfig._retry) {
        originalConfig._retry = true;
        try {
          await refreshToken();
          return axiosInstance({
            ...originalConfig,
            headers: getHeader()
          });
        } catch (_error) {
          return Promise.reject(_error);
        }
      }
    }

    return Promise.reject(err);
  }
);

axiosInstanceRest.interceptors.request.use(
  config => {
    return AxiosLogger.requestLogger(config);
  },
  error => {
    Promise.reject(error);
  }
);

axiosInstanceRest.interceptors.response.use(
  async (res: AxiosResponse<any>) => {
    if (
      res?.data?.code === 'TOKEN_NOT_VALID' ||
      res?.data?.code === 'TOKEN_NOT_EXIST_IN_REDIS' ||
      res?.data?.code === 'REFRESH_TOKEN_EXPIRED' ||
      res?.data?.code === 'TOKEN_VALID_EXCEPTION'
    ) {
      store.dispatch(logoutAsync('Token không hợp lệ, vui lòng đăng nhập lại.'));
    } else if (res?.data?.status === 0 && res?.data?.code === 'TOKEN_EXPIRED') {
      const token = await refreshToken();
      if (token) {
        return axiosInstance({
          ...res.config,
          headers: getHeader()
        });
      }
    }
    return res;
  },
  async err => {
    const originalConfig = err.config;
    if (originalConfig?.url !== baseURL + '/auth/login' && err?.response) {
      if (err.response.status === 401 && !originalConfig._retry) {
        originalConfig._retry = true;
        try {
          await refreshToken();
          return axiosInstance({
            ...originalConfig,
            headers: getHeader()
          });
        } catch (_error) {
          return Promise.reject(_error);
        }
      }
    }

    return Promise.reject(err);
  }
);

export const request = <T = any>(
  method: 'get' | 'post',
  url: string,
  data?: any,
  config?: AxiosRequestConfig
): Promise<IResponse<T>> => {
  if (method === 'post') {
    return axiosInstance.post(url, data, { ...config, headers: getHeader() });
  } else {
    return axiosInstance.get(url, {
      params: data,
      ...config,
      headers: getHeader()
    });
  }
};

export const requestLogin = <T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<IResponse<T>> => {
  return axiosInstance.post(url, data, config);
};

export const requestGetMe = <T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<IResponse<T>> => {
  return axiosInstance.get(url, {
    params: data,
    ...config,
    headers: getHeader()
  });
};

export const requestRestApiAppoimentCovid = <T = any>(
  method: 'get' | 'post',
  url: string,
  data?: any,
  config?: AxiosRequestConfig
): Promise<IResponse<T>> => {
  if (method === 'post') {
    return axiosInstanceRest.post(url, data, { ...config, headers: getHeader() });
  } else {
    return axiosInstanceRest.get(url, {
      params: data,
      ...config,
      headers: getHeader()
    });
  }
};
