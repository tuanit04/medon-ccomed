import { store } from 'helpers/store';
import { setAuthValue } from 'store/user.store';

export const getLocalRefreshToken = () => {
  const user = JSON.parse(window.localStorage.getItem('user'));
  return user?.refreshToken || store.getState().auth.refreshToken || '';
};

export const getLocalAccessToken = () => {
  const user = JSON.parse(window.localStorage.getItem('user'));
  return user?.token || store.getState().auth.token || '';
};

export const updateLocalAccessToken = (token, tokenVcc) => {
  let user = JSON.parse(window.localStorage.getItem('user'));
  if (token) user.token = token;
  if (tokenVcc) user.tokenVcc = tokenVcc;
  localStorage.setItem('user', JSON.stringify(user));
  store.dispatch(
    setAuthValue({
      ...(token != null && { token }),
      ...(tokenVcc != null && { tokenVcc })
    })
  );
};
