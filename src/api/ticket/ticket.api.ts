import { request } from 'api/request';
import { TicketAction } from 'common/interfaces/ticket-action.interface';
import { Ticket } from 'common/interfaces/ticket.interface';
import { baseURL } from 'config/api';

/** Get Ticket list API */
export const apiGetTicketList = (page, pageSize, conditions) =>
  request<Ticket[]>('post', baseURL + '/ticket/findAllTicket/' + page + '/' + pageSize, conditions);
/** Get Ticket Statistic */
export const apiGetStatisticTicket = conditions =>
  request<any>('get', baseURL + '/ticket/getStatisticTicket?' + conditions);
/** Get Ticket Detail API */
export const apiGetTicketByID = ticketId => request<any>('get', baseURL + '/ticket/findTicketById?id=' + ticketId);
/** Delete Ticket API */
export const apiDeleteTicket = ticketId => request<Ticket>('get', baseURL + '/ticket/deleteTicket?id=' + ticketId);
/** Cancel Ticket API */
export const apiCancelTicket = ticketId => request<Ticket>('get', baseURL + '/ticket/cancelTicket?id=' + ticketId);
/** Create Ticket API */
export const apiCreateTicket = ticketObj => request<Ticket>('post', baseURL + '/ticket/saveTicket', { ...ticketObj });
/** Save Ticket Action API */
export const apiSaveTicketAction = ticketActionObj =>
  request<TicketAction>('post', baseURL + '/ticket/saveTicketAction', { ...ticketActionObj });
/** Save Ticket Action API */
export const apiDeleteTicketAction = ticketActionId =>
  request<TicketAction>('get', baseURL + '/ticket/deleteTicketAction?id=' + ticketActionId);
/** Get All Ticket Action Of Ticket API */
export const apiGetAllTicketActionOfTicket = ticketId =>
  request<any>('get', baseURL + '/ticket/findAllTicketActionOfTicket?id=' + ticketId);
/** Update Ticket API */
export const apiUpdateTicket = ticketObj => request<Ticket>('post', baseURL + '/ticket/saveTicket', { ...ticketObj });
/** Get Users In Group Process API */
export const apiGetUsersInGroupProcess = id =>
  request<any>('get', baseURL + '/ticket/findUsersInGroupProcess?id=' + id);
/** Get All Active Group Process API */
export const apiGetAllActiveGroupProcess = conditions =>
  request<any>('get', baseURL + '/ticket/findAllActiveGroupProcess', conditions);
/** Get All Ticket Process Of Ticket API */
export const apiGetAllTicketProcessOfTicket = id =>
  request<any>('get', baseURL + '/ticket/findAllTicketProcessOfTicket?id=' + id);
