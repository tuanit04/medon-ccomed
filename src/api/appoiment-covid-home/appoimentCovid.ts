import { requestRestApiAppoimentCovid } from 'api/request';
import { AppointmentCovid } from 'common/interfaces/appointmentCovide.interface';

//lay tat ca lich hen
export const apiGetAllAppoimentCovidOfHome = (page, pageSize, conditions) =>
  requestRestApiAppoimentCovid<any[]>('post', 'appointment/findAllAppointment/' + page + '/' + pageSize, conditions);
//lay lich hen by gourpId
export const apiGetAppoimentCovidByGroupId = (groupId, id) =>
  requestRestApiAppoimentCovid<AppointmentCovid>(
    'get',
    'appointment/findAppointmentByGroupId?groupId=' + groupId + '&appointmentId=' + id
  );
//Xac nhan lich hen by gourpId
export const apiUpdateAppoumentCovid = data =>
  requestRestApiAppoimentCovid<any>('post', 'appointment/confirmAppointmentEmployee', { ...data });

export const apiSaveBookingCovidEmployee = data =>
  requestRestApiAppoimentCovid<any>('post', 'appointment/saveBookingCovidEmployee', { ...data });

//lay lich hen by code
export const apiGetAppoimentCovidByCode = code =>
  requestRestApiAppoimentCovid<AppointmentCovid>('get', 'appointment-extend/findAppointment/' + code);

//lay otp
export const confirmKbytKH = data =>
  requestRestApiAppoimentCovid<any>('post', 'appointment-extend/confirmKbytKH', { ...data });

//approveKbytKH
export const approveKbytKH = data =>
  requestRestApiAppoimentCovid<any>('post', 'appointment-extend/approveKbytKH', { ...data });

//lay lich phan cho cb tn
export const findAllAppointmentCBTN = (page, pageSize, conditions) =>
  requestRestApiAppoimentCovid<any[]>(
    'post',
    'appointment/findAllAppointmentCBTN/' + page + '/' + pageSize,
    conditions
  );

//huy phan lich hen
export const removeAssignAppointment = listAppoimentId =>
  requestRestApiAppoimentCovid<any>('post', 'appointment/removeAssignAppointment', listAppoimentId);

//huy lich hen covid
export const cancelCovidAppointment = appoimentId =>
  requestRestApiAppoimentCovid<any>('get', 'appointment/cancelCovidAppointment/' + appoimentId);

//xoa lich hen covid
export const deleteCovidAppointment = appoimentId =>
  requestRestApiAppoimentCovid<any>('get', 'appointment/deleteCovidAppointment/' + appoimentId);
