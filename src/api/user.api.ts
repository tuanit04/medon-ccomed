import { request, requestGetMe, requestLogin } from './request';
import { LoginResult, LoginParams, LogoutResult, RefreshTokenResult } from '../interface/user/login';
import { loginUrl, logoutUrl, permissonUrl, refreshTokenUrl } from 'config/api';
import { getLocalAccessToken, getLocalRefreshToken } from './token.service';

/** Login */
export const apiLogin = (data: LoginParams) => requestLogin<LoginResult>(loginUrl, data);

/** Logout */
export const apiLogout = () =>
  request<LogoutResult>(
    'post',
    logoutUrl,
    {},
    {
      headers: {
        Token: getLocalAccessToken() ?? ''
      }
    }
  );

/** Permission */
export const apiPermission = () =>
  requestGetMe<any>(permissonUrl, null, {
    headers: { Token: getLocalAccessToken() ?? '' }
  });

/** Refresh Token */
export const apiRefreshToken = () =>
  request<RefreshTokenResult>('post', refreshTokenUrl, {
    Token: getLocalAccessToken(),
    'Refresh-Token': getLocalRefreshToken()
  });
