declare class WebPush {
  webSocket: WebSocket;
  url: string;
  requestDisconnect: boolean;
  id: string;
  onConnected: () => void;
  onDisconnected: () => void;
  onMessageReceived: (msg: CSMessage) => void;
  connect: (url?: string, forceLongPolling?: number) => void;
  poll: (url: string) => void;
  send: (msg: any) => void;
  sendWait: (msg: string, callback?: Function) => void;
  waitForConnection: (callback?: Function, interval?: number) => void;
  close: () => void;
}

export default WebPush;
