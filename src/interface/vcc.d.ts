interface VCCUser {
  code: number;
  msg: string;
  user: {
    accountId: number;
    account_id: number;
    userId: string;
    email: string;
    name: string;
    agentId: string;
    altAgentId: string;
    username: string;
    agent_id: string;
    phone_no: string;
    ipAddress: string;
    groupId: string;
    role: string;
    active: string;
  };
  account: {
    host_name: string;
    host_name_mobile: string;
    solr_url: string;
    chat_websock_url: string;
    chat_user_websock_url: string;
    agent_server_url: string;
    file_server_url: string;
    sip_server_url: string;
    sip_socket: string;
    chat_client_url: string;
    account_id: number;
    domain: string;
    language: string;
    expiry_time: string;
    enable_popup: number;
    link_popup: string;
    partner_id: string;
  };
}

interface CSMessage extends CallInfo {
  objectId: number;
  idObj?: number;
  agentId?: string;
  agentStatus?: 'AVAILABLE' | 'NO ANSWER' | 'NOT AVAILABLE' | 'LOGIN';
  channelId?: string;
  chatStatus?: string;
  description?: string;
  deviceType?: number;
  isVoiceChannel?: number;
  multichannelStatus?: string;
  status?: string;
  type?: number;
  webAutoAnswerCall?: number;
  profList?: any[];
  newAgentId?: string;
  newAgentName?: string;
  newDeviceType?: number;
  oldDeviceType?: number;
  result?: string;
  extension?: string;
  errorCode?: string;
  sipCode?: string;
  telephoneNumber?: string;
  called?: string;
  superAgent?: string;
  terminatedAgent?: string;
  listAgentSearch?: CSAgent[];
  dropAgent?: string;
  newAgent?: string;
  message?: string;
  ipphone?: string;
}

interface CSVoice {
  callInfo?: CallInfo | null;
  callStatus?: 'Online' | 'Offline' | 'No Answer' | 'READY';
  enableVoice?: boolean;
  hasCall?: boolean;
  missCall?: boolean;
  isRinging?: boolean;
  isAnswering?: boolean;
  isMute?: boolean;
  isHold?: boolean;
  deviceType?: number;
  holding?: boolean;
  isConnectToAgent?: boolean;
  isConnectingCall?: boolean;
  isCallout?: boolean;
  disconnectAs?: boolean;
  sip_socket?: string;
  sip_address?: string;
  agentserver_socket?: string;
  webAutoAnswerCall?: number;
  isInternalCall?: boolean;
  transferRequest?: {
    callId?: string;
    dropAgentId?: string;
    newAgentId?: string;
  };
}

interface CSAgent {
  userName?: string;
  ipPhone?: string;
  startTime?: string;
  callStatus?: string;
  agentStatus?: string;
  ipAddress?: string;
  timeInStatus?: string;
  systemStatus?: string;
  timeAnswered?: string;
  totalIncomingCall?: string;
  priorityQueue?: string;
  typeOfService?: string;
  loginType?: string;
  groupId?: number;
  userId?: number;
  name?: string;
}

interface CSAgentInfo {
  agentId: string;
  name?: string;
  userName?: string;
}

interface CallInfo {
  callId?: string;
  caller?: string;
  agentId?: string;
  line?: string;
  callDirection?: number;
  callerName?: string;
  callerId?: string;
  tabIndex?: string;
  startTime?: string;
  endTime?: string;
  ticketId?: string;
  queueDescription?: string;
}

interface ListCall {
  agentId?: string;
  answerTime?: string;
  callGroupId?: string;
  callTime?: string;
  callTypeId?: string;
  callTypeName?: string;
  content?: string;
  createDate?: string;
  createUser?: string;
  createUserId?: string;
  direct?: 'IN' | 'OUT';
  domain?: string;
  endTime?: string;
  ext?: string;
  id?: string;
  line?: string;
  name?: string;
  path?: string;
  pathDownload?: string;
  patientId?: string;
  patientName?: string;
  phone?: string;
  status?: number;
  updateDate?: string;
  updateUser?: string;
  updateUserId?: string;
  userId?: string;
  vccCallId?: string;
  vccTicketId?: string;
}

interface ListCallUser {
  agentId?: string;
  name?: string;
  callTime?: string;
  cntIn?: number;
  cntMiss?: number;
  cntOut?: number;
  domain?: string;
  exeTime?: string;
  ext?: string;
  reasonUnready?: number;
  status?: number;
  type?: 'UNREADY' | 'READY';
  userId?: string;
}

interface ListCallUserOngoing {
  agentId?: string;
  ext?: string;
  uname?: string;
  patientName?: string;
  userId?: string;
  phone?: string;
  status?: number;
  direct?: 'IN' | 'OUT';
  line?: string;
  answerTime?: string;
  callTime?: string;
  domain?: string;
  endTime?: string;
}

interface StatisticCallByLine {
  avgChocuocgoi?: number;
  avgDamthoai?: number;
  cntApproved?: number;
  cntInDay?: number;
  cntMiss?: number;
  cntReady?: number;
  cntWaiting?: number;
  line?: string;
  ratioCall?: number;
}
