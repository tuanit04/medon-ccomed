export type ScheduleStatus = 'enabled' | 'disabled';
export interface Schedule {
  name: {
    zh_CN: string;
    en_US: string;
  };
  code: string;
  id: number;
  status: ScheduleStatus;
}

export type GetScheduleResult = Schedule[];
