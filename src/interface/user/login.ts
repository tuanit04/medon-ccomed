/** user's role */
export type Role = 'guest' | 'admin';

export interface LoginParams {
  username: string;
  password: string;
  loginType: number;
}

export interface LoginResult {
  /** auth token */
  id?: string;
  token: string;
  tokenVcc?: string;
  agentId?: string;
  domain?: string;
  username: string;
  role: Role;
}

export interface LogoutParams {
  token: string;
}

export interface LogoutResult {}

export interface RefreshTokenResult {
  token: string;
  tokenVcc: string;
}
