export enum VCCStatus {
  STARTING = 1,
  RINGING,
  HOLD,
  ANSWER,
  END,
  UNHOLD
}

export enum VCCOngoingStatus {
  ONGOING = 1,
  HOLD,
  DONE
}

export enum VCCUnreadyReason {
  MEETING = 1,
  BREAK,
  ASKING,
  PROBLEM,
  MEAL
}

export const VCCStatusDetail = {
  [VCCStatus.STARTING]: 'Bắt đầu',
  [VCCStatus.RINGING]: 'Đổ chuông',
  [VCCStatus.HOLD]: 'Giữ máy',
  [VCCStatus.ANSWER]: 'Trả lời',
  [VCCStatus.END]: 'Kết thúc'
};

export const VCCUnreadyReasonDetail = {
  [VCCUnreadyReason.MEETING]: 'Họp',
  [VCCUnreadyReason.BREAK]: 'Nghỉ ngắn',
  [VCCUnreadyReason.ASKING]: 'Hỏi thông tin',
  [VCCUnreadyReason.PROBLEM]: 'Xử lý sự cố',
  [VCCUnreadyReason.MEAL]: 'Nghỉ ăn'
};

export interface LoginVCCResult {
  code: string;
  user: {
    accountId: number;
    account_id: number;
    userId: string;
    email: string;
    name: string;
    agentId: string;
    altAgentId: string;
    username: string;
    agent_id: string;
    phone_no: string;
    ipAddress: string;
    groupId: string;
    role: string;
    active: string;
  };
}

export interface SaveCallLogParams {
  id?: string;
  direct?: 'IN' | 'OUT';
  domain?: string;
  userId?: string;
  ext?: string;
  agentId?: string;
  vccStatus?: VCCStatus;
  phone?: string;
  line?: string;
  patientId?: string;
  vccCallId?: string;
  vccTicketId?: string;
  content?: string;
  callTypeId?: string;
  callGroupId?: string;
}

export interface SaveCallLogResult extends ListCall {}

export interface SavePatientDialogParams {
  name: string;
  phone: string;
  birthDate: string;
  sex: string;
  address?: string;
}

export interface SavePatientDialogResult {
  id: string;
}

export interface SaveAgentStatusParams {
  type?: 'UNREADY' | 'READY';
  userId?: string;
  agentId?: string;
  ext?: string;
  domain?: string;
  reasonUnready?: number;
}

export interface SaveAgentStatusResult {
  agentId?: string;
  createDate?: string;
  createUser?: string;
  createUserId?: string;
  domain?: string;
  exeTime?: string;
  ext?: string;
  id?: string;
  reasonUnready?: string;
  status?: number;
  type?: 'UNREADY' | 'READY';
  updateDate?: string;
  updateUser?: string;
  updateUserId?: string;
  userId?: string;
}
