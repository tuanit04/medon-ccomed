interface IPatientRelation {
  patientId?: string;
  relationType?: string;
  relationName?: string;
  profileInfo?: IPatientProfile;
}

interface IPatientProfile {
  pid?: string;
  patientHisId?: string;
  type?: string;
  name?: string;
  birthDate?: string;
  sex?: string;
  birthYear?: number;
  avatar?: string;
  address?: string;
  phone?: string;
  email?: string;
  provinceId?: string;
  districtId?: string;
  wardId?: string;
  streetId?: string;
  idNo?: string;
  idIssuedDate?: string;
  idIssuedPlace?: string;
  idImageFront?: string;
  idImageBack?: string;
  userId?: string;
  id: string;
  status?: number;
  createDate?: string;
  createUser?: string;
  createUserId?: string;
  updateDate?: string;
  updateUser?: string;
  updateUserId?: string;
  profileId?: ID;
  provinceName?: string;
  districtName?: string;
  wardName?: string;
  nationality?: string;
  crmStatus?: number;
  crmCode?: string;
  patientCrmType?: string;
  relation?: IPatientRelation[];
}

interface IPatientInput {
  id: string;
  pid?: string;
  patientHisId?: string;
  type?: string;
  name: string;
  birthDate: string;
  sex: string;
  birthYear: number | string;
  avatar?: string;
  address: string;
  phone: string;
  email?: string;
  provinceId: string;
  districtId: string;
  provinceCode?: string;
  districtCode?: string;
  wardId?: string;
  streetId?: string;
  idNo?: string;
  idIssuedDate?: string;
  idIssuedPlace?: string;
  idImageFront?: string;
  idImageBack?: string;
  userId?: string;
  status?: number;
  nationality?: string;
  crmStatus?: number;
  crmCode?: string;
  relation?: IPatientRelation[];
}

interface IProfileView {
  id: string;
  name: string;
  phone: string;
  crmStatus: number;
  crmCode: string;
  relationCount: number;
  callCount: number;
  ticketCount: number;
  appointCount: number;
  lastDate: string;
  createDate: string;
}

interface ICallType {
  createDate: string;
  createUser: string;
  createUserId: string;
  id: string;
  name: string;
  status: number;
  updateDate: string;
  updateUser: string;
  updateUserId: string;
}

interface IPatientDialogForm {
  patientId?: string;
  patientName: string;
  patientPhone: string;
  patientBirthDate: import('moment').Moment;
  patientSex: string;
  address?: string;
  patientCrmType: string;
  content?: string;
  callTypeId?: string;
  callGroupId?: string;
}
