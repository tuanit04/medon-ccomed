interface IResponse<T = any> {
  status?: number;
  code?: string;
  message?: string;
  data?: T;
  page?: number;
  records?: number;
  pages?: number;
  result?: T;
}

interface IFiltered {
  id: string;
  value?: string;
  operation: string;
}

interface ISorted {
  id: string;
  desc?: boolean;
}

interface IRequest {
  pageNo?: number;
  pageSize?: number;
  filtered?: IFiltered[];
  sorted?: ISorted[];
  isSuccess?: number;
}

interface IGraphQLRequest {
  page?: number;
  pageSize?: number;
  filtered?: IFiltered[];
  sorted?: ISorted[];
}

interface IUser {
  id?: string;
  userTypeId?: string;
  name?: string;
  userName?: string;
  password?: string;
  phone?: string;
  email?: string;
  avatar?: string;
  address?: string;
  sex?: string;
  birthDate?: string;
  facilityId?: string;
  status?: number;
  createDate?: string;
  createUserId?: string;
  createUser?: string;
  updateUser?: string;
  updateUserId?: string;
  updateDate?: string;
  sessionCount?: number;
  staffCode?: string;
  userGroups?: string;
  userFunctions?: string;
  passWeb?: string;
  userWeb?: string;
  notifys?: {
    appType: number;
    isRead: number;
    numOfNotify: number;
  }[];
  agentId?: string;
  agentExt?: string;
  domain?: string;
  outgoingDomainList?: string;
  incomingDomainList?: string;
  token?: string;
  tokenVcc?: string;
  refreshToken?: string;
  isVCCLoggedIn?: boolean;
}
