import {
  CheckCircleOutlined,
  ClearOutlined,
  CloseOutlined,
  DeleteOutlined,
  DownOutlined,
  EditOutlined,
  ExclamationCircleOutlined,
  OrderedListOutlined,
  PlusCircleOutlined,
  SearchOutlined,
  StepForwardOutlined
} from '@ant-design/icons';
import { Button, Col, Dropdown, FormInstance, FormProps, Menu, Modal, Row, Table, Tag, Tooltip } from 'antd';
import {
  apiGetAllAppoimentCovidOfHome,
  apiGetAppoimentCovidByCode,
  apiGetAppoimentCovidByGroupId,
  apiUpdateAppoumentCovid,
  findAllAppointmentCBTN
} from 'api/appoiment-covid-home/appoimentCovid';
import { AppoimentCovidTable } from 'common/interfaces/appoimentCovidTableData.inteface';
import { AppointmentCovid } from 'common/interfaces/appointmentCovide.interface';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { functionCodeConstants } from 'constants/functions';
import { useGetFacilitys } from 'hooks/facility';
import useUpdateEffect from 'hooks/useUpdateEffect';
import moment from 'moment';
import React, { useCallback, useEffect, useState } from 'react';
import { FC } from 'react';
import { Link, useNavigate, useSearchParams } from 'react-router-dom';
import { openNotificationRight } from 'utils/notification';
import { getStatus } from '../appointment/utils';
interface AppointmentCovidConfirmTableProps {
  valueSearch: any;
  functionOb: any;
  setIsDetail;
  setItemE;
  appoimentDate;
}

const AppointmentCovidConfirmTable: FC<AppointmentCovidConfirmTableProps> = ({
  valueSearch,
  functionOb,
  setIsDetail,
  setItemE,
  appoimentDate
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(20);
  const [searchParams] = useSearchParams();
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const navigate = useNavigate();
  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };
  let sorted = [{ id: 'createDate', desc: 'true', value: '' }];
  const [listFactility, setListFactility] = useState([]);

  const [pagination, setPagination] = useState({});
  // const [appointmentDetail, setAppointmentDetail] = useState({} as AppoimentCovidTable);
  const [tableData, setTableData] = useState<AppoimentCovidTable[]>();

  const getAllAppoiment = useCallback(async (conditions, pageTmp, pageSizeTmp) => {
    const data: any = await findAllAppointmentCBTN(pageTmp, pageSizeTmp, conditions);
    if (data?.data?.status == 1) {
      setTableData(data?.data?.['data']);
      setPagination({ current: pageTmp, pageSize: pageSizeTmp, total: data?.data?.['records'] });
    } else {
    }
  }, []);

  const convertPhoneNumber = (str, index, replacement) => {
    if (str) {
      if (str.substr(0, 1) !== '0') {
        str = '0' + str;
        return str.substr(0, index) + replacement + str.substr(index + replacement.length);
      } else {
        return str.substr(0, index) + replacement + str.substr(index + replacement.length);
      }
    } else {
      return '';
    }
  };

  useUpdateEffect(() => {
    getAllAppoiment(
      {
        filtered: [
          {
            id: 'appointmentDate',
            operation: '==',
            value: moment(appoimentDate).format('DD/MM/YYYY')
          }
        ],
        sorted
      },
      page,
      pageSize
    );
  }, [page, pageSize]);

  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };

  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    if (pageSize !== pagination['pageSize']) {
      setPage(1);
    } else {
      setPage(pagination['current']);
    }
    setPageSize(pagination['pageSize']);
  };

  const getDetail = useCallback(async (groupId, id) => {
    const data: any = await apiGetAppoimentCovidByGroupId(groupId, id);
    if (data?.data.status == 1) {
      setItemE(data?.data?.data);
      setIsDetail(true);
    } else {
      openNotificationRight('Lỗi : ' + data?.data?.message);
    }
  }, []);

  useEffect(() => {
    if (appoimentDate) {
      if (page != 1) {
        setPage(1);
      } else {
        getAllAppoiment(
          {
            filtered: [
              {
                id: 'appointmentDate',
                operation: '==',
                value: moment(appoimentDate).format('DD/MM/YYYY')
              }
            ],
            sorted
          },
          1,
          pageSize
        );
      }
    }
  }, [appoimentDate]);

  return (
    <>
      <div
        className=""
        style={{
          width: '100%',
          textAlign: 'right'
        }}
      ></div>
      <Table
        className="table-appointment"
        rowKey={row => {
          return row['id'] + '#' + row['groupId'];
        }}
        // rowSelection={{
        //     selectedRowKeys,
        //     onChange: onSelectChange,
        //     type: 'checkbox'
        // }}
        pagination={{
          ...pagination,
          defaultPageSize: 20,
          showSizeChanger: true,
          pageSizeOptions: ['10', '20', '30', '50']
        }}
        scroll={{ x: 450, y: 'calc(100vh - 317px)' }}
        dataSource={tableData}
        onChange={handleTableChange}
        onRow={record => {
          return {
            onClick: () => {
              getDetail(record.groupId, record.id);
            }
          };
        }}
      >
        <Table.Column<AppoimentCovidTable>
          width={50}
          title="STT"
          align="center"
          render={(value, item, index) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">{(page - 1) * pageSize + index + 1}</Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          width={100}
          title="Mã lịch hẹn"
          render={(_, { appointmentHisId }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">{appointmentHisId}</Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          width={150}
          title="Trạng thái"
          render={(_, { status }) => <Tooltip title="Click đúp chuột để xem chi tiết">{getStatus(status)}</Tooltip>}
        />
        <Table.Column<AppoimentCovidTable>
          width={100}
          title="KBYT"
          align="center"
          render={(_, { patientLabel }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              {patientLabel ? (
                <Tag color={patientLabel == 'Xanh' ? 'green' : patientLabel == 'Vàng' ? 'yellow' : 'red'}>
                  {patientLabel}
                </Tag>
              ) : (
                ''
              )}
            </Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          width={120}
          align="center"
          title="Khung giờ"
          render={(_, { workTimeName }) => <Tooltip title="Click đúp chuột để xem chi tiết">{workTimeName}</Tooltip>}
        />
        <Table.Column<AppoimentCovidTable>
          title="Tên khách hàng"
          width={180}
          render={(_, { patientName }) => <Tooltip title="Click đúp chuột để xem chi tiết">{patientName}</Tooltip>}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="Số điện thoại"
          width={100}
          render={(_, { phone }) => <Tooltip title="Click đúp chuột để xem chi tiết">{phone}</Tooltip>}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="Giới tính"
          width={90}
          render={(_, { patientSex }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              {patientSex == 'FEMALE' ? 'Nữ' : patientSex == 'MALE' ? 'Nam' : 'Khác'}
            </Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="Ngày sinh"
          width={90}
          render={(_, { patientBirthDate }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">{moment(patientBirthDate).format('DD/MM/YYYY')}</Tooltip>
          )}
        />
      </Table>
    </>
  );
};
export default AppointmentCovidConfirmTable;
