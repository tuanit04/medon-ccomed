import { BorderOutlined, CloseSquareOutlined } from "@ant-design/icons";
import { AppoimentCovidConfrim } from "common/interfaces/appoimentCovidConfrim.interface";
import moment from "moment"


import React, { FC, useEffect, useState } from "react"
import Logo from "../../../assets/images/logo2.png"

interface KBYTENGGenPDProps {
  dataEdit: AppoimentCovidConfrim;
  provinces: any[];
  districs: any[];
  print: boolean
}

const KBYTENGGenPDF: FC<KBYTENGGenPDProps> = ({ provinces, districs, dataEdit, print }) => {
  const getProvice = id => {
    if (id && provinces?.length > 0) {
      for (var i = 0; i < provinces?.length; i++) {
        if (provinces[i]?.id == id) {
          return provinces[i]?.name
        }
      }
      return "......";
    }
  }

  const getDistric = id => {
    if (id && districs?.length > 0) {
      for (var i = 0; i < districs?.length; i++) {
        if (districs[i]?.id == id) {
          return districs[i]?.name
        }
      }
      return "......";
    }
  }
  return (
    <div
      id="KBYT"
      style={
        print
          ? {
            width: "794px",
            height: "1123px",
            fontFamily: '"Times New Roman", Times, serif'
          }
          : {
            // width: "794px",
            // height: "1123px",
            fontFamily: '"Times New Roman", Times, serif'
          }
      }
    >
      <div >
        <div
          style={{
            width: "100%",
            padding: "10px 20px"
          }}
        >
          {dataEdit?.appointmentHisId && <div style={{
            width: "100%",
            textAlign: "right"
          }}>
            Code : {dataEdit?.appointmentHisId}
          </div>}
          <div
            style={{
              width: "100%",
              display: "flex"
            }}
          >
            <div
              style={{
                width: "60%"
              }}
            >
              <div style={{ display: "flex" }}>
                <div
                  style={{
                    textAlign: "center",
                    marginLeft: 30
                  }}
                >
                  <div
                    style={{
                      fontWeight: "bold"
                    }}
                  >
                    MEDICAL LABORATORY AND
                    <br /> TECHNOLOGY LIMITED COMPANY
                  </div>
                </div>
              </div>
            </div>
            <div
              style={{
                width: "60%"
              }}
            >
              <div
                style={{
                  float: "right"
                }}
              >
                <div
                  style={{
                    padding: "1px 5px",
                    textAlign: "center"
                  }}
                >
                  <div>
                    <strong>
                      THE SOCIALIST REPUBLIC OF VIETNAM
                      <br /> Independence- Freedom - Happiness
                    </strong>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <p
                style={{
                  textAlign: "center"
                }}
              >
                <strong>HEALTH DECLARATION AND SAR-COV2 TESTING REQUEST FORM</strong>
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <p
                style={{
                  textAlign: "left",
                  marginBottom: "0"
                }}
              >
                <strong>1. 1. 1. Administrative information</strong>
                <br />
                <div style={{ paddingLeft: "10px", paddingBottom: "10px" }}>
                  1.1. Full name:&nbsp;
                  {dataEdit?.patientName || "……………………………………………………………………………………… "}
                  <br />
                  1.2. Date of birth:&nbsp;
                  {dataEdit?.patientBirthDate
                    ? dataEdit?.patientBirthDate
                    : "……… / ……… / ………… "}
                  &nbsp;&nbsp;<br/> 1.3. Gender:&nbsp;
                  {dataEdit?.patientSex == "MALE" ? <CloseSquareOutlined /> : <BorderOutlined />}
                  &nbsp; MALE &nbsp;
                  {dataEdit?.patientSex == "FEMALE" ? <CloseSquareOutlined /> : <BorderOutlined />}
                  &nbsp; FEMALE <br />
                  1.4. Phone: &nbsp;{dataEdit?.phone || "………………………… "} &nbsp;&nbsp; 1.5.
                  Nationality/ Ethic: &nbsp;{dataEdit?.countryName || "…………………………… "} <br />
                  1.6. Identity card number/ Passport number:&nbsp;
                  {dataEdit?.patientIdNo || "……………………… "} Date of issue: &nbsp;
                  {dataEdit?.issuedDate
                    ? dataEdit?.issuedDate
                    : "……………………… "}{" "}
                  Place of issue:&nbsp;
                  {dataEdit?.issuedPlace || "……………………… "} <br />
                  1.7. Address: &nbsp;
                  {dataEdit?.address +
                    ", " +
                    getDistric(dataEdit?.districtId) +
                    ", " +
                    getProvice(dataEdit?.provinceId) || "……………………… "}
                  <br />
                  1.8. Supervisor's name (Parent / relatives): &nbsp;
                  {dataEdit?.guardianName || "……………………… "}&nbsp;&nbsp; Phone: &nbsp;
                  {dataEdit?.guardianPhone || "……………………… "} <br />
                  1.9. Reasons for Testing: <br />
                  <div style={{ float: "left", padding: "0px 10px" }}>
                    &nbsp;&nbsp;
                    {dataEdit?.reasonNote == "DT" ? <CloseSquareOutlined /> : <BorderOutlined />}
                    &nbsp; Having been exposed to suspect factors <br /> &nbsp;&nbsp;
                    {dataEdit?.reasonNote == "TC" ? <CloseSquareOutlined /> : <BorderOutlined />}
                    &nbsp; Having COVID-19 symptoms <br />
                    &nbsp;&nbsp;
                    {dataEdit?.reasonNote == "KQXN" ? <CloseSquareOutlined /> : <BorderOutlined />}
                    &nbsp; Having a potential COVID-19 infection test result
                  </div>
                  <div style={{ float: "left", padding: "0px 10px" }}>
                    &nbsp;&nbsp;
                    {dataEdit?.reasonNote == "XNC" ? <CloseSquareOutlined /> : <BorderOutlined />}
                    &nbsp; Entry into Vietnam/ Entry into other countries; <br /> &nbsp;&nbsp;
                    {dataEdit?.reasonNote == "KB" ? <CloseSquareOutlined /> : <BorderOutlined />}
                    &nbsp; Having undergone a medical examination and treatment at a specialized hospital;
                    <br />
                    &nbsp;&nbsp;
                    {dataEdit?.reasonNote == "K" ? <CloseSquareOutlined /> : <BorderOutlined />}&nbsp;
                    Others:
                    {(dataEdit?.reasonNote == "K" && dataEdit?.appointmentNote) || "……………………… "}
                  </div>
                </div>
              </p>
              <br />
            </div>
          </div>
          <br />
          <br />
          <div className="row" style={{
            clear:"both"
          }}>
            <div className="col-md-12" style={{
              width:"100%"
            }}>
              <div
                style={{
                  textAlign: "left",
                  marginBottom: "5px !important",
                  width:"100%"
                }}
              >
                <strong style={{
              width:"100%",
              textAlign: "left",
            }}>1.10. Medical report: Circle Yes/No</strong>
              </div>
              <table
                style={{
                  // border: "1px solid black",
                  width: "100%",
                  paddingLeft: "5px",
                  borderCollapse: "separate",
                  borderSpacing: 0
                }}
              >
                <tr
                  style={{
                    // border: "1px solid black",
                    width: "100%"
                  }}
                >
                  <th
                    style={{
                      border: "1px solid black",
                      width: "70%",
                      textAlign: "center"
                    }}
                  >
                    Questions
                  </th>
                  <th
                    colSpan={2}
                    style={{
                      border: "1px solid black",
                      width: "30%",
                      textAlign: "center"
                    }}
                  >
                    Answers
                  </th>
                </tr>
                <tbody>
                  <tr
                    style={{
                      border: "1px solid black",
                      width: "100%"
                    }}
                  >
                    <td
                      style={{
                        padding: "1px 5px",
                        border: "1px solid black"
                      }}
                    >
                      <strong>A. Symptoms in the last 03 days</strong>
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                      colSpan={2}
                    ></td>
                  </tr>
                  <tr>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "1px 5px"
                      }}
                    >
                      Coughing, sore throat
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        width: "15%"
                      }}
                    >
                      &nbsp;&nbsp; Yes&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic && (dataEdit?.healthDeclaration?.prognostic?.includes("16")) ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; No&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic && !dataEdit?.healthDeclaration?.prognostic?.includes("16") ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "1px 5px"
                      }}
                    >
                      Fever
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        width: "15%"
                      }}
                    >
                      &nbsp;&nbsp; Yes&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic && (dataEdit?.healthDeclaration?.prognostic?.toString()?.includes("2")) ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; No&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic && !(dataEdit?.healthDeclaration?.prognostic?.toString()?.includes("2")) ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td
                      style={{
                        padding: "1px 5px",
                        border: "1px solid black"
                      }}
                    >
                      Chest tightness, difficulty breathing
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; Yes&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic && dataEdit?.healthDeclaration?.prognostic?.toString()?.includes("4") ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; No&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic && !dataEdit?.healthDeclaration?.prognostic?.toString()?.includes("4") ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                  </tr>
                  <tr
                    style={
                      {
                        // border: "1px solid black"
                      }
                    }
                  >
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "1px 5px"
                      }}
                    >
                      Symptom : , body aches, tired, chills,...
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; Yes&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic && (dataEdit?.healthDeclaration?.prognostic?.toString()?.includes("8")) ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; No&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic && !dataEdit?.healthDeclaration?.prognostic?.toString()?.includes("8") ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                  </tr>
                  <tr
                    style={
                      {
                        // border: "1px solid black"
                      }
                    }
                  >
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "1px 5px"
                      }}
                    >
                      Decrease or loss of taste or smell
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; Yes&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic && (dataEdit?.healthDeclaration?.prognostic?.toString()?.includes("32")) ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; No&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic && !dataEdit?.healthDeclaration?.prognostic?.toString()?.includes("32") ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                  </tr>
                  <tr
                    style={
                      {
                        // border: "1px solid black",
                      }
                    }
                  >
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "1px 5px"
                      }}
                    >
                      Date of symptom onset (if any)
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "1px 5px"
                      }}
                      colSpan={2}
                    >
                      {dataEdit?.healthDeclaration?.dateOnset && dataEdit?.healthDeclaration?.dateOnset != null
                        ? dataEdit?.healthDeclaration?.dateOnset
                        : "…………………"}
                    </td>
                  </tr>
                  <tr>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "1px 5px"
                      }}
                      colSpan={3}
                    >
                      <strong>B. Epidemilological factors</strong>
                    </td>
                  </tr>
                  <tr
                    style={
                      {
                        // border: "1px solid black"
                      }
                    }
                  >
                    <td
                      style={{
                        padding: "1px 5px",
                        border: "1px solid black"
                      }}
                    >
                      Have you ever been in close contact with a COVID-19 infected individual in the
                      02 days prior to the date F0 was confirmed, or since the date F0 of symptom
                      onset? (F1 is in close proximity with F0).
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; Yes&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.humanHasCovid == 1 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; No&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.humanHasCovid == 0 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr
                    style={
                      {
                        // border: "1px solid black"
                      }
                    }
                  >
                    <td
                      style={{
                        padding: "1px 5px",
                        border: "1px solid black"
                      }}
                    >
                      Traveling on the same means of transportation or at the same event, workplace,
                      class, or other location with a confirmed case (F0) within two days before the
                      day F0 was confirmed or counted from the date F0 symptom onset.
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; Yes&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.streetHasCovid == 1 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; No&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.streetHasCovid == 0 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr
                    style={{
                      border: "1px solid black"
                    }}
                  >
                    <td
                      style={{
                        padding: "1px 5px",
                        border: "1px solid black"
                      }}
                    >
                      Ever taken a SARS-CoV-2 Real-Time PCR test or a COVID-19 Ag Rapid Test and had
                      positive or suspected positive results (Date of the test:……………… ………).
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; Yes&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.testedCovid == 1 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td
                      style={{
                        border: "1px solid black"
                      }}
                    >
                      &nbsp;&nbsp; No&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.testedCovid == 0 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="col-md-12">
              <div
                className="row"
                style={{
                  textAlign: "left"
                }}
              >
                <p style={{
                  marginBottom: 0
                }}>
                  <strong
                    style={{
                      textAlign: "left",
                      marginBottom: "5px !important"
                    }}
                  >
                    1.11. Other information
                  </strong>
                </p>
                <div
                  style={{
                    textAlign: "left",
                    width: "100%"
                  }}
                >
                  1. Have you been infected with COVID-19 within the last 6 months?
                </div>
                <div
                  style={{
                    display: "flex",
                    width: "100%"
                  }}
                >
                  <div
                    style={{
                      width: "60%",
                      textAlign: "left",
                      paddingLeft: "10px"
                    }}
                  >
                    A. Yes&nbsp;&nbsp;
                    {dataEdit?.healthDeclaration?.hasCovid == "1" ? <CloseSquareOutlined /> : <BorderOutlined />}
                  </div>
                  <div
                    style={{
                      width: "60%",
                      textAlign: "left",
                      paddingLeft: "10px"
                    }}
                  >
                    B.No&nbsp;&nbsp;
                    {dataEdit?.healthDeclaration?.hasCovid == "2" ? <CloseSquareOutlined /> : <BorderOutlined />}
                  </div>
                  <div
                    style={{
                      width: "60%",
                      textAlign: "left",
                      paddingLeft: "10px"
                    }}
                  >
                    C.Following treatment.&nbsp;&nbsp;
                    {dataEdit?.healthDeclaration?.hasCovid == "3" ? <CloseSquareOutlined /> : <BorderOutlined />}
                  </div>
                </div>
                <div
                  style={{
                    textAlign: "left",
                    width: "80%"
                  }}
                >
                  (Specify the date of confirmed COVID-19 case:&nbsp;&nbsp;
                  {dataEdit?.healthDeclaration?.dateHascovid
                    ? dataEdit?.healthDeclaration?.dateHascovid
                    : "…….…………….."}
                  )<br />
                </div>
                <div
                  style={{
                    textAlign: "left",
                    width: "100%",
                    paddingLeft: "10px"
                  }}
                >
                  2. Have you been vaccinated against COVID-19?
                </div>
                <div
                  style={{
                    display: "flex",
                    width: "100%"
                  }}
                >
                  <div
                    style={{
                      width: "60%",
                      textAlign: "left",
                      paddingLeft: "10px"
                    }}
                  >
                    A.&nbsp;&nbsp; Not yet&nbsp;&nbsp;
                    {dataEdit?.healthDeclaration?.hasVaccine == "0" ? <CloseSquareOutlined /> : <BorderOutlined />}
                  </div>
                  <div
                    style={{
                      width: "60%",
                      textAlign: "left",
                      paddingLeft: "10px"
                    }}
                  >
                    B.&nbsp;&nbsp; 1 dose&nbsp;&nbsp;
                    {dataEdit?.healthDeclaration?.hasVaccine == "1" ? <CloseSquareOutlined /> : <BorderOutlined />}
                  </div>
                  <div
                    style={{
                      width: "60%",
                      textAlign: "left",
                      paddingLeft: "10px"
                    }}
                  >
                    C.&nbsp;&nbsp; 2 dose&nbsp;&nbsp;
                    {dataEdit?.healthDeclaration?.hasVaccine == "2" ? <CloseSquareOutlined /> : <BorderOutlined />}
                  </div>
                  <div
                    style={{
                      width: "60%",
                      textAlign: "left",
                      paddingLeft: "10px"
                    }}
                  >
                    D.&nbsp;&nbsp; &gt; 2 dose&nbsp;&nbsp;
                    {dataEdit?.healthDeclaration?.hasVaccine == "3" ? <CloseSquareOutlined /> : <BorderOutlined />}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <p >
              <strong>2. 2. Request for testing</strong> <br />
              <span>
                &nbsp;&nbsp; I request the unit that accepts the request, takes samples, and
                performs a SARS-CoV-2 test for me in order to screen and control the COVID-19
                epidemic.
                <br />
                <strong>
                  &nbsp;&nbsp; I hereby certify that the above statements are entirely true, and if
                  they are not, I will accept legal responsibility. At the same time, I am committed
                  to implementing COVID-19-compliant epidemic preventive measures.
                </strong>
              </span>
            </p>
          </div>
          <div className="row">
            <div
              style={{
                width: "100%",
                display: "flex"
              }}
            >
              <div
                style={{
                  width: "60%",
                  textAlign: "center"
                }}
              >
                <p>
                  <strong>Person declares information</strong>
                  <br />
                  <em>(Confirmed)</em>
                </p>
                <br />
                <p>{(dataEdit?.patientLabel && dataEdit?.patientName) || ""}</p>
                <br />
              </div>
              <div
                style={{
                  width: "60%",
                  textAlign: "center"
                }}
              >
                <p>
                  <em>..….:..….AM/PM, Date….……Month……...2022</em>
                  <br />
                  <strong>Healthcare staff</strong>
                  <br />
                </p>
                <br />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
export default KBYTENGGenPDF
