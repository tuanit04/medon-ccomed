import { AppoimentCovidConfrim } from "common/interfaces/appoimentCovidConfrim.interface";
import moment from "moment";
import React from "react";
import { FC } from "react";

interface CommitPDFProps {
    value: AppoimentCovidConfrim;
    provinces: any[];
    districs: any[];
    factilitys:any[]
}

const CommitPDF: FC<CommitPDFProps> = ({ value, provinces, districs,factilitys }) => {
    const getProvice = id => {
        if (id && provinces?.length > 0) {
            for (var i = 0; i < provinces?.length; i++) {
                if (provinces[i]?.id == id) {
                    return provinces[i]?.name
                }
            }
            return "......";
        }
    }

    const getDistric = id => {
        if (id && districs?.length > 0) {
            for (var i = 0; i < districs?.length; i++) {
                if (districs[i]?.id == id) {
                    return districs[i]?.name
                }
            }
            return "......";
        }
    }

    const getFacility = id => {
        if (id && factilitys?.length > 0) {
            for (var i = 0; i < factilitys?.length; i++) {
                if (factilitys[i]?.id == id) {
                    return factilitys[i]?.name
                }
            }
            return "......";
        }
    }
    return (
        <div id="commitPDF" style={{
            width: "794px",
            height: "1123px",
            textAlign: "center",
            fontFamily: '"Times New Roman", Times, serif',
        }}>
            <div style={{
                width: "100%",
                padding: "10px 20px",
            }}>
                <div style={{
                    width: "100%",
                    display: "flex",
                    fontSize: 16
                }}>
                    <div style={{
                        width: "60%",
                        textAlign: "center",
                    }}>
                        CÔNG TY TNHH CÔNG NGHỆ <br />
                        VÀ XÉT NGHIỆM Y HỌC
                        <div style={{
                            textAlign: "left",
                            fontWeight: "bold",
                            textDecoration: "underline"
                        }}>
                            CƠ SỞ DỊCH VỤ CHĂM SÓC SỨC KHỎE <br />
                            TẠI NHÀ MEDLATEC……………………..
                        </div>
                    </div>
                    <div style={{
                        width: "60%",
                        textAlign: "center",
                        fontWeight: "bold"
                    }}>
                        CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br />
                        <div style={{
                            textDecoration: "underline"
                        }}>
                            Độc lập – Tự do – Hạnh phúc
                        </div>
                    </div>
                </div>
                <div style={{
                    width: "100%",
                    textAlign: "center",
                    fontWeight: "bold",
                    fontSize: 16,
                    marginTop: 10
                }}>
                    Đơn đề nghị xét nghiệm SARS-CoV2 tại nhà và cam kết tuân thủ<br />
                    các biện pháp phòng, chống dịch COVID-19
                </div>
                <div style={{
                    width: "100%",
                    marginTop: 10,
                    fontSize: 16
                }}
                >
                    <b>Kính gửi: </b>Công ty TNHH Công nghệ và Xét nghiệm Y học
                </div>
                <div style={{
                    width: "100%",
                    marginTop: 20,
                    fontWeight: "bold",
                    textAlign: "left",
                    fontSize: 16
                }}>
                    1.	Thông tin người đề nghị xét nghiệm
                </div>
                <div style={{
                    width: "100%",
                    textAlign: 'left',
                    fontSize: 16
                }}>
                    Họ và tên khách hàng: {value?.patientName ? value?.patientName : "……………………………………."} Ngày tháng năm sinh {value?.patientBirthDate ? value?.patientBirthDate : "………………..."}<br />
                    CMND/CCCD/Hộ chiếu số :{value?.patientIdNo ? value?.patientIdNo : "…………………."} Ngày cấp : {value?.issuedDate ? moment(value?.issuedDate).format("DD/MM/YYYY") : "…………………."}    Nơi cấp :{value?.issuedPlace ? value?.issuedPlace : "…………………."}<br />
                    Họ và tên người giám hộ/đại diện (nếu có)  : {value?.guardianName ? value?.guardianName : "………….………………………………………….."}<br />
                    Ngày tháng năm sinh  : {value?.guardianDate ? moment(value?.guardianDate).format("DD/MM/YYYY") : "……………………………………."} Quan hệ:   {value?.relationType ? value?.relationType : "…………………………….."}<br />
                    CMND/CCCD/Hộ chiếu số  :………………….Ngày cấp :……………    Nơi cấp :……..……....<br />
                    Chỗ ở hiện tại: {value?.address ? value?.address+"," : ""} {getDistric(value?.districtId)}<br /> ,&nbsp; {getProvice(value?.provinceId)}<br/>
                    Số điện thoại liên hệ:{value?.phone ? value?.phone : "………………………………………………………………………………."}<br />
                    <b>2.      Đề nghị xét nghiệm</b><br />
                    &nbsp;&nbsp;&nbsp;&nbsp; Nhằm mục đích {value?.reasonNote=="KB" ? "Khám bệnh" : value?.reasonNote=="XN" ? "Làm xét nghiệm trước xuất cảnh" : value?.reasonNote=="GTH" ? "Giấy thông hành" : value?.reasonNote=="K" ? value?.appointmentNote : "……………………………………………………………………………."}<br />
                    tôi đề nghị Cơ sở chăm sóc sức khoẻ tại nhà MEDLATEC {getFacility(value?.examFacilityId)} <br />
                    tiếp nhận yêu cầu và lấy mẫu, thực hiện xét nghiệm SARS-CoV2 tại nhà cho tôi.<br />
                    <b>3.      Tôi cam kết </b><br />
                    3.1.	Đề nghị được lấy mẫu xét nghiệm SARS-CoV2 tại nhà của tôi là tự nguyện, được tôi quyết định khi tinh thần tỉnh táo và minh mẫn.<br />
                    3.2.	Trong quá trình chờ kết quả xét nghiệm, tôi cam kết sẽ tuân thủ 5K, các biện pháp phòng chống dịch và tạm thời cách ly tại nhà.<br />
                    3.3.	Nếu xét nghiệm của tôi cho kết quả nghi ngờ/dương tính với virus SARS-CoV2 tôi cam kết sẽ chủ động khai báo với y tế địa phương và tuân thủ các quy định phòng chống dịch theo hướng dẫn của TTYT/TYT địa phương và đơn vị thực hiện lấy mẫu xét nghiệm. <br />
                    3.4.	Cam kết chi trả chi phí xét nghiệm và phụ phí đầy đủ theo quy định của MEDLATEC. <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;Kính mong Cơ sở chăm sóc sức khoẻ tại nhà MEDLATEC {getFacility(value?.examFacilityId)} chấp nhận yêu cầu và thực hiện lấy mẫu xét nghiệm virus SARS-CoV2 cho tôi.
                </div>
                <div style={{
                    width: "100%",
                    display: "flex",
                    fontSize: 16,
                    marginTop: 20
                }}>
                    <div style={{
                        width: "30%",
                        textAlign: "center",
                        fontWeight: "bold"
                    }}>
                        Người cam kết
                    </div>
                    <div style={{
                        width: "70%",
                        textAlign: "center"
                    }}>
                        <i>Hà Nội,….giờ…...phút,  ngày…….tháng…..năm 2021</i><br />
                        <b>Đại diện gia đình (nếu có)</b>
                    </div>
                </div>
            </div>
            <div style={{
                width: "100%",
                padding: "10px 20px",
                marginTop: 800
            }}>
                <div style={{
                    width: "100%",
                    display: "flex",
                    fontSize: 16
                }}>
                    <div style={{
                        width: "60%",
                        textAlign: "center",
                    }}>
                        MEDICAL LABORATORY AND TECHNOLOGY  <br />
                        COMPANY LIMITED
                        <div style={{
                            textAlign: "center",
                            fontWeight: "bold",
                        }}>
                            HOME HEALTH CARE SERVICES FACILITIES <br />
                            ……………………..
                        </div>
                    </div>
                    <div style={{
                        width: "60%",
                        textAlign: "center",
                        fontWeight: "bold"
                    }}>
                        THE SOCIALIST REPUBLIC OF VIETNAM <br />
                        <div style={{
                            textDecoration: "underline"
                        }}>
                            Independence - Freedom - Happiness
                        </div>
                    </div>
                </div>
                <div style={{
                    width: "100%",
                    textAlign: "center",
                    fontWeight: "bold",
                    fontSize: 16,
                    marginTop: 10
                }}>
                    SARS-CoV-2 Home-testing Package Application Form and Commitment to follow Covid-19 Control <br />
                    and Preventative Measures
                </div>
                <div style={{
                    width: "100%",
                    marginTop: 10,
                    fontSize: 16
                }}
                >
                    <b>To: </b>Medical Laboratory and Technology Company Limited
                </div>
                <div style={{
                    width: "100%",
                    marginTop: 20,
                    fontWeight: "bold",
                    textAlign: "left",
                    fontSize: 16
                }}>
                    1.	Personal Information
                </div>
                <div style={{
                    width: "100%",
                    textAlign: 'left',
                    fontSize: 16
                }}>
                    Full Name: …………………………………. Date of Birth: ………………......................................................................<br />
                    ID card /Passport No: …………………. Issued on: ……………    issued by: ……..……………………………………<br />
                    Full name of the guardian/ representative (if any) :………….……………………………………………………………<br />
                    Date of Birth: …………………………………. Relationship: …………………………………………………………..<br />
                    ID number /Passport No. : ……………Date of Issue: …………Place of Issue: …..…………………………………….<br />
                    Current Address: …………………………………………………………………………….............................................<br />
                    Phone Number: …………………………………………………………………………………………………………...<br />
                    <b>2.	Application for testing</b><br />
                    For the purposes of ………..………………………………………………………………………, I would like MEDLATEC’s Home Health Care Services to accept my request to collect samples and administer the SARS-CoV-2 test to me at home<br />
                    <b>3.	Commitment </b><br />
                    3.1.	I agree to return to my current address immediately at …..……………………………………………………………………………………………after collecting the sample. On the way home, I will commit to implementing 5K, and epidemic prevention steps.<br />
                    3.2.	While waiting for test results, I commit to follow 5K, epidemic prevention measures and isolate at home temporarily after collecting samples for testing.<br />
                    3.3.	If my test results show I’m infected with the SARS-CoV-2, I agree to notify local health officials as soon as possible and follow all epidemic prevention measures as directed by the local medical center/ medical station and the sampling firm.<br />
                    3.4.	Commitment to pay for the cost of testing in accordance with MEDLATEC's policies.<br />
                    I hope MEDLATEC’s Home Health Care Services accept my request and collect my samples for SARS-CoV-2 virus testing.
                </div>
                <div style={{
                    width: "100%",
                    display: "flex",
                    fontSize: 16,
                    marginTop: 20
                }}>
                    <div style={{
                        width: "30%",
                        textAlign: "center",
                        fontWeight: "bold"
                    }}>
                        Individual Commitment
                    </div>
                    <div style={{
                        width: "70%",
                        textAlign: "center"
                    }}>
                        <i>Hanoi, date :…../…../2021, time : …... :……</i><br />
                        <b>Family member representative </b><i>(if any)</i>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CommitPDF;