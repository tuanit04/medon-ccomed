import { BorderOutlined, CheckSquareOutlined } from "@ant-design/icons";
import { Page, Document, Text, Font, Image, StyleSheet } from "@react-pdf/renderer";
import { Col, Row } from "antd";
import { AppoimentCovidConfrim } from "common/interfaces/appoimentCovidConfrim.interface";
import moment from "moment";
import { format } from "path";
import React, { FC } from "react";
import Logo from "../../../assets/logo/fiqrnlaqxepmybpgaqxy-logo_2.jpg";

interface KBYTGenPDFProps {
  value: AppoimentCovidConfrim;
  provinces: any[];
  districs: any[];
}

const KBYTGenPDF: FC<KBYTGenPDFProps> = ({ value, provinces, districs }) => {
  const getProvice = id => {
    if (id && provinces?.length > 0) {
      for (var i = 0; i < provinces?.length; i++) {
        if (provinces[i]?.id == id) {
          return provinces[i]?.name
        }
      }
      return "......";
    }
  }

  const getDistric = id => {
    if (id && districs?.length > 0) {
      for (var i = 0; i < districs?.length; i++) {
        if (districs[i]?.id == id) {
          return districs[i]?.name
        }
      }
      return "......";
    }
  }
  console.log(value);
  return (
    <div id="KBYT" style={{
      width: "794px",
      height: "1123px",
      textAlign: "center",
      fontFamily: '"Times New Roman", Times, serif',
    }} >
      <div className="p-5" >
        <div style={{
          width: "100%",
          padding: "10px 20px",
        }}>
          <div style={{
            width: "100%",
            display: "flex"
          }}>
            <div style={{
              width: "60%",
            }}>
              <div style={{ display: "flex" }}>
                <img style={{
                  width: 80
                }} src={Logo} />
                <div style={{
                  textAlign: "center",
                  fontSize: 12,
                  marginLeft: 30,
                  paddingTop: 26
                }} >
                  <div>
                    SỞ Y TẾ HÀ NỘI
                  </div>
                  <div style={{
                    fontWeight: "bold"
                  }}>
                    BỆNH VIỆN ĐA KHOA MEDLATEC
                  </div>
                </div>
              </div>
            </div>
            <div style={{
              width: "60%",
            }}>
              <div style={{
                float: "right",
              }}>
                <div style={{
                  border: "solid 1px black",
                  padding: "1px 5px",
                  textAlign: "center",

                }}>
                  <div style={{
                    fontWeight: "bold",
                    textTransform: "uppercase",
                  }}>Phân loại khách hàng</div>
                  <div>
                    <p style={{ marginBottom: 0 }}>( Xanh &nbsp;&nbsp; {value?.patientLabel=="Xanh" ? <CheckSquareOutlined /> : <BorderOutlined />  } &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Vàng &nbsp;&nbsp; {value?.patientLabel=="Vàng" ? <CheckSquareOutlined /> : <BorderOutlined />  } &nbsp;&nbsp; &nbsp;&nbsp;  Đỏ &nbsp;&nbsp; {value?.patientLabel=="Đỏ" ? <CheckSquareOutlined /> : <BorderOutlined />  } )</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Row>
            <Col span={24}>
              <p style={{
                textAlign: "center"
              }}><strong>PHIẾU KHAI BÁO THÔNG TIN Y TẾ</strong><br /> <em>(Áp dụng từ ngày 14/12/2021)</em></p>
              <div style={{
                textAlign: "left",
                fontSize: "16px",
                fontWeight: "bold",
                marginBottom: 10
              }}>
                Tôi xin cam đoan những thông tin khai báo dưới đây là sự thật và
                hoàn toàn chịu trách nhiệm trước pháp luật về tính xác thực của
                phiếu khai y tế này.
              </div>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <p style={{
                textAlign: "left",
                marginBottom: "5px !important"
              }}><strong>I. THÔNG TIN CHUNG</strong></p>
              <table style={{
                width: "100%",
                border: "1px solid black",
                borderCollapse: "separate",
                borderSpacing: 0
              }}>
                <tbody>
                  <tr style={{
                    width: "100%",
                  }}>
                    <td style={{
                      border: "1px solid black",
                      width: "60%",
                      padding: "1px 5px",
                    }}>
                      Họ và tên: {value?.patientName ? value?.patientName : "……………………"}
                    </td>
                    <td style={{
                      border: "1px solid black",
                      width: "40%",
                      padding: "1px 5px",
                    }}>
                      Giới tính: {value?.patientSex == "MALE" ? "Nam" : value?.patientSex == "FEMALE" ? "Nữ" : "Khác"}
                    </td>
                  </tr>
                  <tr style={{
                    border: "1px solid black",
                    width: "100%",
                  }}>
                    <td style={{
                      border: "1px solid black",
                      width: "60%",
                      padding: "1px 5px",
                    }}>Năm sinh: {value?.patientBirthDate ? moment(value?.patientBirthDate).format("YYYY") : "………"} </td>
                    <td style={{
                      border: "1px solid black",
                      width: "40%",
                      padding: "1px 5px",
                    }}>Số điện thoại:{value?.phone ? value?.phone : "…………"}  </td>
                  </tr>
                  <tr style={{
                    width: "100%"
                  }}>
                    <td style={{
                      border: "1px solid black",
                      width: "100%",
                      padding: "1px 5px",
                    }} colSpan={2}>
                      Nơi ở hiện tại:&nbsp;&nbsp; {value?.address}, <br />
                      Quận/huyện:&nbsp;&nbsp; {getDistric(value?.districtId)} Tỉnh/thành phố:&nbsp;&nbsp; {getProvice(value?.provinceId)}
                    </td>
                  </tr>
                  <tr style={{
                    border: "1px solid black",
                    width: "100%",
                  }}>
                    <td style={{
                      border: "1px solid black",
                      width: "100%",
                      padding: "1px 5px",
                    }} colSpan={2}>
                      Đối tượng :
                      <table style={{
                        border: "1px solid black",
                        width: "100%",
                        paddingLeft: "5px",
                        borderCollapse: "separate",
                        borderSpacing: 0
                      }}>
                        <tr style={{
                          border: "1px solid black",
                          width: "100%",
                          borderBottom: "none"
                        }}>
                          <td style={{
                            width: "60%",
                            padding: "1px 5px",
                          }}> Bệnh nhân &nbsp;&nbsp; {value?.healthDeclaration?.typeCustomerKBYT == "1" ? <CheckSquareOutlined /> : <BorderOutlined />} </td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            borderBottom: "none",
                            padding: "1px 5px",
                          }}>
                            Người nhà &nbsp;&nbsp; {value?.healthDeclaration?.typeCustomerKBYT == "2" ? <CheckSquareOutlined /> : <BorderOutlined />}
                          </td>
                        </tr>
                        <tr>
                          <td style={{
                            borderBottom: "none",
                            padding: "1px 5px",
                          }}> Lý do khám:………</td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            borderBottom: "none",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}> </td>
                        </tr>
                        <tr style={{
                          border: "1px solid black",
                          width: "100%",
                          borderBottom: "none",
                          borderTop: "none"
                        }}>
                          <td style={{
                            width: "60%",
                            borderBottom: "none",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}>  1.	Khám bệnh &nbsp;&nbsp; {value?.reasonNote == "KB" ? <CheckSquareOutlined /> : <BorderOutlined />}</td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}>

                          </td>
                        </tr>
                        <tr style={{
                          border: "1px solid black",
                          width: "100%",
                          borderBottom: "none",
                          borderTop: "none"
                        }}>
                          <td style={{
                            width: "60%",
                            borderBottom: "none",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}>  2.	Làm Xét nghiệm trước xuất cảnh &nbsp;&nbsp; {value?.reasonNote == "XN" ? <CheckSquareOutlined /> : <BorderOutlined />}</td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            borderTop: "1px solid black",
                            padding: "1px 5px",
                          }}>
                            Nhân viên/ Cộng tác viên   &nbsp;&nbsp;  {value?.healthDeclaration?.typeCustomerKBYT == "3" ? <CheckSquareOutlined /> : <BorderOutlined />}
                          </td>
                        </tr>
                        <tr style={{
                          border: "1px solid black",
                          width: "100%",
                          borderBottom: "none",
                          borderTop: "none"
                        }}>
                          <td style={{
                            width: "60%",
                            borderBottom: "none",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}>  3.	Giấy thông hành  &nbsp;&nbsp; {value?.reasonNote == "GTH" ? <CheckSquareOutlined /> : <BorderOutlined />}<br /></td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            padding: "1px 5px",
                          }}>
                            Khách đến liên hệ công tác   &nbsp;&nbsp; {value?.healthDeclaration?.typeCustomerKBYT == "4" ? <CheckSquareOutlined /> : <BorderOutlined />}
                          </td>
                        </tr>
                        <tr style={{
                          border: "1px solid black",
                          width: "100%",
                          borderBottom: "none",
                          borderTop: "none"
                        }}>
                          <td style={{
                            width: "60%",
                            borderBottom: "none",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}>  4.	Khác &nbsp;&nbsp; {value?.reasonNote == "K" ? <CheckSquareOutlined /> : <BorderOutlined />}   Ghi rõ: {value?.appointmentNote ? value?.appointmentNote : "....................."}<br /></td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            padding: "1px 5px",
                          }}>
                            Khác  &nbsp;&nbsp; <BorderOutlined />   Ghi rõ: .....................
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </Col>
            <Col span={24}>
              <p style={{
                textAlign: "left",
                marginBottom: "5px !important"
              }}>
                <strong>II. KHAI BÁO Y TẾ: Hãy tích dấu "X" vào ô trống trước phần lựa chọn</strong>
              </p>
              <table style={{
                // border: "1px solid black",
                width: "100%",
                paddingLeft: "5px",
                borderCollapse: "separate",
                borderSpacing: 0
              }}>
                <tr style={{
                  // border: "1px solid black",
                  width: "100%"
                }}>
                  <th style={{
                    border: "1px solid black",
                    width: "70%",
                    textAlign: "center"
                  }}>
                    Câu hỏi
                  </th>
                  <th colSpan={2} style={{
                    border: "1px solid black",
                    width: "30%",
                    textAlign: "center"
                  }}>
                    Trả lời
                  </th>
                </tr>
                <tbody>
                  <tr style={{
                    border: "1px solid black",
                    width: "100%"
                  }}>
                    <td style={{
                      padding: "1px 5px",
                      border: "1px solid black",
                    }}><strong> Mục A. Trong 14 ngày qua, có ít nhất 1 trong các triệu chứng
                      dưới đây</strong></td>
                    <td style={{
                      border: "1px solid black",
                    }} colSpan={2}></td>
                  </tr>
                  <tr>
                    <td style={{
                      border: "1px solid black",
                      padding: "1px 5px",
                    }}>Sốt, Ho, đau họng</td>
                    <td style={{
                      border: "1px solid black",
                      width: "15%"
                    }}>
                      &nbsp;&nbsp; Có&nbsp;&nbsp;  {value?.healthDeclaration?.prognostic.indexOf("16") != -1 || value?.healthDeclaration?.prognostic.indexOf("2") != -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Không&nbsp;&nbsp; {value?.healthDeclaration?.prognostic.indexOf("16") == -1 && value?.healthDeclaration?.prognostic.indexOf("2") == -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr>
                    <td style={{
                      padding: "1px 5px",
                      border: "1px solid black",
                    }}>Tức ngực, khó thở</td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Có&nbsp;&nbsp; {value?.healthDeclaration?.prognostic.indexOf("4") != -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Không&nbsp;&nbsp;{value?.healthDeclaration?.prognostic.indexOf("4") == -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr style={{
                    // border: "1px solid black"
                  }}>
                    <td style={{
                      border: "1px solid black",
                      padding: "1px 5px",
                    }}>
                      Các biểu hiện của hội chứng cúm: Đau mỏi người, đau đầu, gai
                      rét, giảm hoặc mất vị giác, khứu giác, chảy dịch mũi,...
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Có&nbsp;&nbsp; {value?.healthDeclaration?.prognostic.indexOf("8") != -1 || value?.healthDeclaration?.prognostic.indexOf("32") != -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Không&nbsp;&nbsp; {value?.healthDeclaration?.prognostic.indexOf("8") == -1 || value?.healthDeclaration?.prognostic.indexOf("32") == -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr style={{
                    // border: "1px solid black",
                  }}>
                    <td style={{
                      border: "1px solid black",
                      padding: "1px 5px",
                    }}>Ngày bắt đầu triệu chứng(nếu có)</td>
                    <td style={{
                      border: "1px solid black",
                      padding: "1px 5px",
                    }} colSpan={2}>{value?.healthDeclaration?.dateOnset ? moment(value?.healthDeclaration?.dateOnset).format("DD/MM/YYYY") : "………………"}</td>
                  </tr>
                  <tr>
                    <td style={{
                      border: "1px solid black",
                      padding: "1px 5px",
                    }} colSpan={3}><strong>Mục B. Yếu tố dịch tễ trong 14 ngày qua</strong></td>
                  </tr>
                  <tr style={{
                    // border: "1px solid black"
                  }}>
                    <td style={{
                      padding: "1px 5px",
                      border: "1px solid black",
                    }}>
                      Có tiền sử tiếp xúc gần ≤ 2m hoặc trong cùng không gian kín
                      với người nhiễm COVID – 19 (đối tượng F1 tiếp xúc gần F0)?
                      (Nếu có: Ghi rõ ngày/tháng, thông tin người mắc COVID -19: Họ
                      tên; Địa chỉ; hoàn cảnh/thời gian tiếp xúc ). …………………
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Có&nbsp;&nbsp; {value?.healthDeclaration?.humanHasCovid?.toString() == "1" ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Không&nbsp;&nbsp; {value?.healthDeclaration?.humanHasCovid.toString() == "0" ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr style={{
                    border: "1px solid black"
                  }}>
                    <td style={{
                      padding: "1px 5px",
                      border: "1px solid black",
                    }}>
                      Đã làm xét nghiệm sàng lọc COVID trước đó &nbsp;&nbsp; Có kết quả xét
                      nghiệm test nhanh dương tính/ hoặc PCR chưa xác định? (Nếu có: ghi rõ ngày tháng có kết quả:). ………………
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Có&nbsp;&nbsp; {value?.healthDeclaration?.testedCovid.toString() == "1" ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Không&nbsp;&nbsp; {value?.healthDeclaration?.testedCovid.toString() == "0" ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                </tbody>
              </table>
            </Col>
            <Col span={24}>
              <Row style={{
                textAlign: "left"
              }}>
                <p> <strong style={{
                  textAlign: "left",
                  marginBottom: "5px !important"
                }}>Mục C. Khác</strong>
                </p>
                <div style={{
                  textAlign: "left",
                  width: "100%"
                }}>1.	Đã từng nhiễm COVID – 19 trong vòng 6 tháng gần đây không?</div>
                <div style={{
                  display: "flex",
                  width: "100%"
                }}>
                  <div style={{
                    width: "60%",
                    textAlign: "left"
                  }}>
                    A. Đã từng&nbsp;&nbsp; {value?.healthDeclaration?.hasCovid.toString() == "1" ? <CheckSquareOutlined /> : <BorderOutlined />}
                  </div>
                  <div style={{
                    width: "60%",
                    textAlign: "left"
                  }}>
                    B.Chưa từng&nbsp;&nbsp; {value?.healthDeclaration?.hasCovid.toString() == "0" ? <CheckSquareOutlined /> : <BorderOutlined />}
                  </div>
                </div>
                <div style={{
                  textAlign: "left",
                  width: "100%"
                }}>(Ghi rõ ngày/tháng nhiễm:{value?.healthDeclaration?.dateHascovid ? moment(value?.healthDeclaration?.dateHascovid).format("DD/MM/YYYY") : "…….…………….."} )<br /></div>
                <div style={{
                  textAlign: "left",
                  width: "100%"
                }}>2.	Đã tiêm đủ 2 mũi vắc xin chưa?</div>
                <div style={{
                  display: "flex",
                  width: "100%"
                }} >
                  <div style={{
                    width: "60%",
                    textAlign: "left"
                  }}>
                    A.&nbsp;&nbsp; Có&nbsp;&nbsp; {value?.healthDeclaration?.hasVaccine.toString() == "1" ? <CheckSquareOutlined /> : <BorderOutlined />}
                  </div>
                  <div style={{
                    width: "60%",
                    textAlign: "left"
                  }}>
                    B.&nbsp;&nbsp; Không&nbsp;&nbsp; {value?.healthDeclaration?.hasVaccine.toString() == "0" ? <CheckSquareOutlined /> : <BorderOutlined />}
                  </div>
                </div>
              </Row>
            </Col>
          </Row>
          <Row>
            <div style={{
              width: "100%",
              display: "flex"
            }}>
              <div style={{
                width: "60%",
                textAlign: 'center'
              }}>
                <p><strong>Người khai báo</strong><br />
                  <em>(Ký và ghi rõ họ tên)</em></p><br />
              </div>
              <div style={{
                width: "60%",
                textAlign: 'center'
              }}>
                <p> <em>……..giờ……. phút, Ngày….Tháng….Năm ......</em><br />
                  <strong>Cán bộ khai thác thông tin</strong><br />
                  <em>(Ký và ghi rõ họ tên)</em></p><br />
              </div>
            </div>
          </Row>
        </div>
      </div>
      <div className="p-5" style={{
        position: "relative",
        marginTop: 300
      }} >
        <div style={{
          width: "100%",
          padding: "10px 20px",
        }}>
          <div style={{
            width: "100%",
            display: "flex"
          }}>
            <div style={{
              width: "60%",
            }}>
              <div style={{ display: "flex" }}>
                <img style={{
                  width: 80
                }} src={Logo} />
                <div style={{
                  textAlign: "center",
                  fontSize: 12,
                  marginLeft: 30,
                  paddingTop: 26
                }} >
                  <div>
                    HA NOI DEPARTMENT OF HEALTH
                  </div>
                  <div style={{
                    fontWeight: "bold"
                  }}>
                    MEDLATEC  HOSPITAL
                  </div>
                </div>
              </div>
            </div>
            <div style={{
              width: "60%",
            }}>
              <div style={{
                float: "right",
              }}>
                <div style={{
                  border: "solid 1px black",
                  padding: "1px 5px",
                  textAlign: "center",

                }}>
                  <div style={{
                    fontWeight: "bold",
                    textTransform: "uppercase",
                  }}>Customer classification</div>
                  <div>
                    <p style={{ marginBottom: 0 }}>( Green &nbsp;&nbsp; {value?.patientLabel=="Xanh" ? <CheckSquareOutlined /> : <BorderOutlined />  }  &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Yellow &nbsp;&nbsp; {value?.patientLabel=="Vàng" ? <CheckSquareOutlined /> : <BorderOutlined />  } &nbsp;&nbsp; &nbsp;&nbsp;  Red &nbsp;&nbsp; {value?.patientLabel=="Đỏ" ? <CheckSquareOutlined /> : <BorderOutlined />  } )</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Row>
            <Col span={24}>
              <p style={{
                textAlign: "center"
              }}><strong>MEDICAL INFORMATION DECLARATION FORM</strong><br /> <em>(Applied from December 14 th , 2021)</em></p>
              <div style={{
                textAlign: "left",
                fontSize: "16px",
                fontWeight: "bold",
                marginBottom: 10
              }}>
                I hereby declare that the information provided is true and I take full responsibility before  the law for the information in this declaration form.
              </div>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <p style={{
                textAlign: "left",
                marginBottom: "5px !important"
              }}><strong>I. GENERAL INFORMATION</strong></p>
              <table style={{
                width: "100%",
                border: "1px solid black",
                borderCollapse: "separate",
                borderSpacing: 0
              }}>
                <tbody>
                  <tr style={{
                    width: "100%",
                  }}>
                    <td style={{
                      border: "1px solid black",
                      width: "60%",
                      padding: "1px 5px",
                    }}>
                      Full name: {value?.patientName ? value?.patientName : "……………………"}
                    </td>
                    <td style={{
                      border: "1px solid black",
                      width: "40%",
                      padding: "1px 5px",
                    }}>
                      {value?.patientSex == "MALE" ? "Male" : value?.patientSex == "FEMALE" ? "Female" : "Another"}
                    </td>
                  </tr>
                  <tr style={{
                    border: "1px solid black",
                    width: "100%",
                  }}>
                    <td style={{
                      border: "1px solid black",
                      width: "60%",
                      padding: "1px 5px",
                    }}>Year of birth:{value?.patientBirthDate ? moment(value?.patientBirthDate).format("YYYY") : "………"}</td>
                    <td style={{
                      border: "1px solid black",
                      width: "40%",
                      padding: "1px 5px",
                    }}>Mobile phone:{value?.phone ? value?.phone : "…………"}  </td>
                  </tr>
                  <tr style={{
                    width: "100%"
                  }}>
                    <td style={{
                      border: "1px solid black",
                      width: "100%",
                      padding: "1px 5px",
                    }} colSpan={2}>
                      Current address: {value?.address},<br />
                      District: &nbsp;&nbsp; {getDistric(value?.districtId)} Province/ City:&nbsp;&nbsp; {getProvice(value?.provinceId)}
                    </td>
                  </tr>
                  <tr style={{
                    border: "1px solid black",
                    width: "100%",
                  }}>
                    <td style={{
                      border: "1px solid black",
                      width: "100%",
                      padding: "1px 5px",
                    }} colSpan={2}>
                      Object classification :
                      <table style={{
                        border: "1px solid black",
                        width: "100%",
                        paddingLeft: "5px",
                        borderCollapse: "separate",
                        borderSpacing: 0
                      }}>
                        <tr style={{
                          border: "1px solid black",
                          width: "100%",
                          borderBottom: "none"
                        }}>
                          <td style={{
                            width: "60%",
                            padding: "1px 5px",
                          }}> Patient &nbsp;&nbsp; {value?.healthDeclaration?.typeCustomerKBYT == "1" ? <CheckSquareOutlined /> : <BorderOutlined />} </td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            borderBottom: "none",
                            padding: "1px 5px",
                          }}>
                            Family member &nbsp;&nbsp; {value?.healthDeclaration?.typeCustomerKBYT == "2" ? <CheckSquareOutlined /> : <BorderOutlined />}
                          </td>
                        </tr>
                        <tr>
                          <td style={{
                            borderBottom: "none",
                            padding: "1px 5px",
                          }}> Reasons:………</td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            borderBottom: "none",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}> </td>
                        </tr>
                        <tr style={{
                          border: "1px solid black",
                          width: "100%",
                          borderBottom: "none",
                          borderTop: "none"
                        }}>
                          <td style={{
                            width: "60%",
                            borderBottom: "none",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}>  1.	Medical examination &nbsp;&nbsp; {value?.reasonNote == "KB" ? <CheckSquareOutlined /> : <BorderOutlined />}</td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}>

                          </td>
                        </tr>
                        <tr style={{
                          border: "1px solid black",
                          width: "100%",
                          borderBottom: "none",
                          borderTop: "none"
                        }}>
                          <td style={{
                            width: "60%",
                            borderBottom: "none",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}>  2.	Pre-departure test &nbsp;&nbsp; {value?.reasonNote == "XN" ? <CheckSquareOutlined /> : <BorderOutlined />}</td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            borderTop: "1px solid black",
                            padding: "1px 5px",
                          }}>
                            Staff &nbsp;&nbsp; {value?.healthDeclaration?.typeCustomerKBYT == "3" ? <CheckSquareOutlined /> : <BorderOutlined />}
                          </td>
                        </tr>
                        <tr style={{
                          border: "1px solid black",
                          width: "100%",
                          borderBottom: "none",
                          borderTop: "none"
                        }}>
                          <td style={{
                            width: "60%",
                            borderBottom: "none",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}>  3.	Passport  &nbsp;&nbsp; {value?.reasonNote == "GTH" ? <CheckSquareOutlined /> : <BorderOutlined />}<br /></td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            padding: "1px 5px",
                          }}>
                            Guests come to contact for business   &nbsp;&nbsp; {value?.healthDeclaration?.typeCustomerKBYT == "4" ? <CheckSquareOutlined /> : <BorderOutlined />}
                          </td>
                        </tr>
                        <tr style={{
                          border: "1px solid black",
                          width: "100%",
                          borderBottom: "none",
                          borderTop: "none"
                        }}>
                          <td style={{
                            width: "60%",
                            borderBottom: "none",
                            borderTop: "none",
                            padding: "1px 5px",
                          }}>  4.	Other  &nbsp;&nbsp; {value?.reasonNote == "K" ? <CheckSquareOutlined /> : <BorderOutlined />}   Specific: {value?.appointmentNote ? value?.appointmentNote : "....................."}<br /></td>
                          <td style={{
                            border: "1px solid black",
                            width: "40%",
                            padding: "1px 5px",
                          }}>
                            Other &nbsp;&nbsp; <BorderOutlined />   Specific: .....................
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </Col>
            <Col span={24}>
              <p style={{
                textAlign: "left",
                marginBottom: "5px !important"
              }}>
                <strong>II. MEDICAL REPORT: Circle Yes/No</strong>
              </p>
              <table style={{
                // border: "1px solid black",
                width: "100%",
                paddingLeft: "5px",
                borderCollapse: "separate",
                borderSpacing: 0
              }}>
                <tr style={{
                  // border: "1px solid black",
                  width: "100%"
                }}>
                  <th style={{
                    border: "1px solid black",
                    width: "70%",
                    textAlign: "center"
                  }}>
                    Questions
                  </th>
                  <th colSpan={2} style={{
                    border: "1px solid black",
                    width: "30%",
                    textAlign: "center"
                  }}>
                    Answers
                  </th>
                </tr>
                <tbody>
                  <tr style={{
                    border: "1px solid black",
                    width: "100%"
                  }}>
                    <td style={{
                      padding: "1px 5px",
                      border: "1px solid black",
                    }}><strong> Item A. In the 14 days recently, have at least 1 of the following symptoms</strong></td>
                    <td style={{
                      border: "1px solid black",
                    }} colSpan={2}></td>
                  </tr>
                  <tr>
                    <td style={{
                      border: "1px solid black",
                      padding: "1px 5px",
                    }}>Fever, Coughing, sore throat</td>
                    <td style={{
                      border: "1px solid black",
                      width: "15%"
                    }}>
                      &nbsp;&nbsp; Yes&nbsp;&nbsp; {value?.healthDeclaration?.prognostic.indexOf("16") != -1 || value?.healthDeclaration?.prognostic.indexOf("2") != -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; No&nbsp;&nbsp; {value?.healthDeclaration?.prognostic.indexOf("16") == -1 && value?.healthDeclaration?.prognostic.indexOf("2") == -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr>
                    <td style={{
                      padding: "1px 5px",
                      border: "1px solid black",
                    }}>Pain in the chest, stuggle to breath</td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Yes&nbsp;&nbsp; {value?.healthDeclaration?.prognostic.indexOf("4") != -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; No&nbsp;&nbsp;{value?.healthDeclaration?.prognostic.indexOf("4") == -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr style={{
                    // border: "1px solid black"
                  }}>
                    <td style={{
                      border: "1px solid black",
                      padding: "1px 5px",
                    }}>
                      Symptoms of flu syndrome: body aches, headache, chills, decrease or loss of taste or smell, running nose,…
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Yes&nbsp;&nbsp;  {value?.healthDeclaration?.prognostic.indexOf("8") != -1 || value?.healthDeclaration?.prognostic.indexOf("32") != -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; No&nbsp;&nbsp; {value?.healthDeclaration?.prognostic.indexOf("8") == -1 || value?.healthDeclaration?.prognostic.indexOf("32") == -1 ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr style={{
                    // border: "1px solid black",
                  }}>
                    <td style={{
                      border: "1px solid black",
                      padding: "1px 5px",
                    }}>The date of symptom onset (if any)</td>
                    <td style={{
                      border: "1px solid black",
                      padding: "1px 5px",
                    }} colSpan={2}>{value?.healthDeclaration?.dateOnset ? moment(value?.healthDeclaration?.dateOnset).format("DD/MM/YYYY") : "………………"}</td>
                  </tr>
                  <tr>
                    <td style={{
                      border: "1px solid black",
                      padding: "1px 5px",
                    }} colSpan={3}><strong>Item B. Epidemilogy factors in the 14 days recently</strong></td>
                  </tr>
                  <tr style={{
                    // border: "1px solid black"
                  }}>
                    <td style={{
                      padding: "1px 5px",
                      border: "1px solid black",
                    }}>
                      CHave a history of close contact with a distance of ≤ 2m or in the same enclosed space as someone infected with COVID-19? (Subject F1 is in close contact with F0)<br />
                      (If yes: Specify date/month, information of person with COVID-19: Full name; Address; contact situation/time…………………)
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Yes&nbsp;&nbsp;{value?.healthDeclaration?.humanHasCovid?.toString() == "1" ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; No&nbsp;&nbsp; {value?.healthDeclaration?.humanHasCovid.toString() == "0" ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr style={{
                    border: "1px solid black"
                  }}>
                    <td style={{
                      padding: "1px 5px",
                      border: "1px solid black",
                    }}>
                      Have you been tested for Sars – CoV – 2 before?. Result:  positive rapid test or unconfirmed PCR?<br />
                      (If you have, please note the time of result ………………….….)
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; Yes&nbsp;&nbsp; {value?.healthDeclaration?.testedCovid.toString() == "1" ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td style={{
                      border: "1px solid black",
                    }}>
                      &nbsp;&nbsp; No&nbsp;&nbsp; {value?.healthDeclaration?.testedCovid.toString() == "0" ? <CheckSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                </tbody>
              </table>
            </Col>
            <Col span={24}>
              <Row style={{
                textAlign: "left"
              }}>
                <p> <strong style={{
                  textAlign: "left",
                  marginBottom: "5px !important"
                }}>Item C. Others</strong>
                </p>
                <div style={{
                  textAlign: "left",
                  width: "100%"
                }}>1.	1.	Have you had COVID-19 in the last 6 months?</div>
                <div style={{
                  display: "flex",
                  width: "100%"
                }}>
                  <div style={{
                    width: "60%",
                    textAlign: "left"
                  }}>
                    A. Yes&nbsp;&nbsp; {value?.healthDeclaration?.hasCovid.toString() == "1" ? <CheckSquareOutlined /> : <BorderOutlined />}
                  </div>
                  <div style={{
                    width: "60%",
                    textAlign: "left"
                  }}>
                    B.No&nbsp;&nbsp; {value?.healthDeclaration?.hasCovid.toString() == "0" ? <CheckSquareOutlined /> : <BorderOutlined />}
                  </div>
                </div>
                <div style={{
                  textAlign: "left",
                  width: "100%"
                }}>(The date of infection: {value?.healthDeclaration?.dateHascovid ? moment(value?.healthDeclaration?.dateHascovid).format("DD/MM/YYYY") : "…….…………….."} )<br /></div>
                <div style={{
                  textAlign: "left",
                  width: "100%"
                }}>2.	Have you had 2 doses of the vaccine yet?</div>
                <div style={{
                  display: "flex",
                  width: "100%"
                }} >
                  <div style={{
                    width: "60%",
                    textAlign: "left"
                  }}>
                    A.&nbsp;&nbsp; Yes&nbsp;&nbsp; {value?.healthDeclaration?.hasVaccine.toString() == "1" ? <CheckSquareOutlined /> : <BorderOutlined />}
                  </div>
                  <div style={{
                    width: "60%",
                    textAlign: "left"
                  }}>
                    B.&nbsp;&nbsp; No&nbsp;&nbsp; {value?.healthDeclaration?.hasVaccine.toString() == "0" ? <CheckSquareOutlined /> : <BorderOutlined />}
                  </div>
                </div>
              </Row>
            </Col>
          </Row>
          <Row>
            <div style={{
              width: "100%",
              display: "flex"
            }}>
              <div style={{
                width: "60%",
                textAlign: 'center'
              }}>
                <p><strong>Person declares information</strong><br />
                  <em>(Sign)</em></p><br />
              </div>
              <div style={{
                width: "60%",
                textAlign: 'center'
              }}>
                <p> <em>….. ..….:..….AM/PM, Date….……Month……...Year.....</em><br />
                  <strong>Healthcare staff</strong><br />
                  <em></em></p><br />
              </div>
            </div>
          </Row>
        </div>
      </div>
    </div>
  )
}
export default KBYTGenPDF
