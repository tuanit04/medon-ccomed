import { Col, Row } from "antd";
import React from "react";
import { FC } from "react";

interface CovideAssignItemProps {

}

const CovideAssignItem: FC<CovideAssignItemProps> = ({ }) => {
    return (
        <div className="assign-item">
            <Row>
                <Col span={16}>
                    <div className="mt-10px">
                        <span className="assign-item-time">
                            07h00 - 08h00 | 
                        </span>
                        <span className="assign-item-code">
                            151221-12345
                        </span>
                    </div>
                </Col>
                <Col span={8}>
                    <div className="assign-item-label-green">
                        Xanh
                    </div>
                </Col>
            </Row>
            <div className="assign-item-content">
                Phạm Thị Thanh Huyền - Nữ - 0346628475
            </div>
        </div>
    )
}
export default CovideAssignItem;