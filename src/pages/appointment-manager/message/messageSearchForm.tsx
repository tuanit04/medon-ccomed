import React, { FC, useState } from 'react';
import { Role } from 'interface/permission/role.interface';
import useGetSearchForm from './useGetSearchForm';
import { useLocale } from 'locales';
import { SearchOutlined, ClearOutlined, DownOutlined, UpOutlined } from '@ant-design/icons';

export interface Values extends Role {}

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};

const MessageSearchForm: FC = () => {
  const { Form, Date, Unit, Status, Office, Buttons } = useGetSearchForm({ name: 'searchForm', responsive: true });
  const { formatMessage } = useLocale();
  const [expand, setExpand] = useState(false);
  const onSearch = () => {
    //
  };

  return (
    <Form>
      <Date />
      <Status />
      <Unit />
      <Office />
      <Buttons />
    </Form>
  );
};

export default MessageSearchForm;
