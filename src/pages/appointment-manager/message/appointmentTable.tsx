import React, { FC, useState, useEffect, useCallback } from 'react';
import { Button, Table, Tag, Modal, Radio, Divider, Select } from 'antd';
import { useLocale, LocaleFormatter } from 'locales';
import { apiGetRoleList } from 'api/permission/role.api';
import { Role, RoleStatus } from 'interface/permission/role.interface';
import { useAppState } from '../../../helpers';
import { RedoOutlined } from '@ant-design/icons';

enum TagColor {
  enabled = 'success',
  disabled = 'error'
}

interface AppointmentTableProps {
  onCreate: () => void;
  onModify: (row: Role) => void;
  onAuthorize: (row: Role) => void;
}

const AppointmentTable: FC<AppointmentTableProps> = ({ onCreate, onModify, onAuthorize }) => {
  const { formatMessage } = useLocale();
  const [tableData, setTableData] = useState<Role[]>();
  const { locale } = useAppState((state: any) => state.user);

  // callback that get the data from backend(mock)
  // and loads the table with role-member rows
  const initData = useCallback(async () => {
    const { result, status } = await apiGetRoleList();
    if (status) {
      setTableData(result);
    }
  }, []);

  const getLocaleStatus = (status: RoleStatus) => {
    switch (status) {
      case 'enabled':
        return formatMessage({ id: 'app.permission.role.status.enabled' });
    }
  };

  useEffect(() => {
    initData();
    console.log('test');
  }, [initData]);
  return (
    <Table
      rowKey="id"
      dataSource={tableData}
      scroll={{ x: 500 }}
      title={() => (
        <>
          <div style={{ float: 'left' }}>
            <label className="lblTableTxt">Danh sách lịch hẹn </label>
            <label className="lblTableValue">(20)</label>
            <span> </span>
            <label className="lblTableTxt"> - Đã chọn </label>
            <label className="lblTableValue">(20)</label>
            <Divider />
          </div>
          <div style={{ float: 'right' }}>
            <Button type="primary">Gửi tin nhắn BOT</Button>
          </div>
          <div style={{ clear: 'both' }}></div>
        </>
      )}
    >
      <Table.Column<Role> title="STT" width={30} render={(_, { name }) => 1} />
      <Table.Column<Role> title="Mã lịch" width={90} render={(_, { name }) => name[locale]} />
      <Table.Column<Role> title="Cung đường" width={300} render={(_, { name }) => name[locale]} />
      <Table.Column<Role> title="Tên khách hàng" width={300} render={(_, { name }) => name[locale]} />
      <Table.Column<Role> title="Địa chỉ" width={300} render={(_, { name }) => name[locale]} />
      <Table.Column<Role> title="Ghi chú XN" width={300} render={(_, { name }) => name[locale]} />
      <Table.Column<Role> title="Giờ hẹn" width={150} render={(_, { name }) => name[locale]} />
      <Table.Column<Role> title="Cán bộ TN" width={150} render={(_, { name }) => name[locale]} />
      <Table.Column<Role> title="Gửi tin nhắn" width={150} render={(_, { name }) => name[locale]} />
      <Table.Column<Role> title="Thời gian gửi tin nhắn" width={150} render={(_, { name }) => name[locale]} />
      <Table.Column<Role> title="Người gửi" width={150} render={(_, { name }) => name[locale]} />
    </Table>
  );
};

export default AppointmentTable;
