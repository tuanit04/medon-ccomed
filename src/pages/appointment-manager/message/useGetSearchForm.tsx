import React, { FC } from 'react';
import { Form, Input, Col, Row, Select, Radio, DatePicker, Space, Button } from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import { Role } from 'interface/permission/role.interface';

const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 8,
  xl: 8,
  xxl: 8
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 }
};

interface MessageFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  /** Initial form data */
  values?: Role;
}

export default function useGetSearchForm({
  required = false,
  responsive = false,
  name = 'form',
  values
}: MessageFormProps) {
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<FormProps>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={values}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Ngay hen
  const Date: FC = () => {
    function handleChange(value: any) {}
    const date = (
      <Form.Item
        name="date"
        label="Ngày hẹn"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <DatePicker />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{date}</Col> : date;
  };
  //Van phong
  const Unit: FC = () => {
    function handleChange(value: any) {}
    const unit = (
      <Form.Item
        name="office"
        label="Văn Phòng"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select defaultValue="lucy"></Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{unit}</Col> : unit;
  };

  //Gui tin nhan
  const Status: FC = () => {
    function handleChange(value: any) {}
    const status = (
      <Form.Item
        name="status"
        label="Gửi tin nhắn"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select defaultValue="lucy"></Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{status}</Col> : status;
  };

  //Van phong
  const Office: FC = () => {
    function handleChange(value: any) {}
    const office = (
      <Form.Item
        name="road"
        label="Cung đường"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select defaultValue="lucy"></Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{office}</Col> : office;
  };
  //Buttons
  const Buttons: FC = () => {
    const buttons = (
      <Form.Item>
        <Button type="primary">Tìm kiếm</Button>
        &nbsp;&nbsp;
        <Button>Bỏ lọc</Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };

  return {
    form: formInstance,
    Form: WrappedForm,
    Date,
    Unit,
    Status,
    Office,
    Buttons
  };
}
