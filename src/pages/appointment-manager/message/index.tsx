import { Col, Row } from 'antd';
import React, { FC, useState } from 'react';
import './index.less';
import MessageSearchForm from './messageSearchForm';
import AppointmentTable from './appointmentTable';

const MessagePage: FC = () => {
  const [mcreateVisible, setCreateVisible] = useState(false);
  const [modifyVisible, setModifyVisible] = useState(false);
  const [authorizeVisible, setAuthorizeVisible] = useState(false);
  const [values, setValues] = useState({});
  return (
    <div>
      <MessageSearchForm />
      <AppointmentTable
        onCreate={() => setCreateVisible(true)}
        onModify={values => {
          setValues(values);
          setModifyVisible(true);
        }}
        onAuthorize={values => {
          setValues(values);
          setAuthorizeVisible(true);
        }}
      />
    </div>
  );
};
export default MessagePage;
