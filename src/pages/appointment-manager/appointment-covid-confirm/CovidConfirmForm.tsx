import ReactPDF, { PDFDownloadLink, PDFViewer } from '@react-pdf/renderer';
import { Checkbox, Form, Input, Modal, Row, Space, Tabs } from 'antd';
import React, { useCallback, useEffect, useState } from 'react';
import { FC } from 'react';
import MyDocument from './KBYTGenPDF';
import useGetCovidConfirmForm from './useGetConfirmForm';
import { Page, Text, View, Document, StyleSheet, Image, Font } from '@react-pdf/renderer';
import Logo from '../../../assets/logo/fiqrnlaqxepmybpgaqxy-logo_2.jpg';
import KBYTGenPDF from './KBYTGenPDF';
import { jsPDF } from 'jspdf';
import html2canvas from 'html2canvas';
import { renderToString } from 'react-dom/server';
import CommitPDF from './CommitPDF';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetAllCountriesLazy } from 'hooks/appointment/useGetAllCountries';
import { useGetFacilitys } from 'hooks/facility';
import { useGetAllWorktimes } from 'hooks/worktimes/useGetWorkTimes';
import { apiGetAppoimentCovidByCode, approveKbytKH, confirmKbytKH } from 'api/appoiment-covid-home/appoimentCovid';
import { AppoimentCovidConfrim } from 'common/interfaces/appoimentCovidConfrim.interface';
import moment from 'moment';
import { PrinterOutlined } from '@ant-design/icons';
import { openNotificationRight } from 'utils/notification';
import KBYTENGGenPDF from './KBYTENGGenPDF';
import KBYTVNGenPDF from './KBYTVNGenPDF';

const TabPane = Tabs.TabPane;

const CheckboxGroup = Checkbox.Group;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};
interface CovidConfirmFormProps {
  isDetail: boolean;
  itemE: any;
  setIsDetail: any;
}

const CovidConfirmForm: FC<CovidConfirmFormProps> = ({ isDetail, itemE, setIsDetail }) => {
  const [isInvoice, setIsInvoice] = useState(false);
  const [typeInvoice, setTypeInvoice] = useState('0');
  const [isPrint, setIsprint] = useState('0');
  const [txtTime, setTxtTime] = useState('');
  const [typeA, setTypeA]: any = useState('Tab1');
  const [otp, setOtp] = useState('');
  const [isOpenOtp, setIsOpenOtp] = useState(false);
  const [transactionId, setTransactionId] = useState('false');
  const [langauge, setLangauge] = useState('VN');
  const [itemApp, setItemApp]: any = useState();

  const {
    Form,
    form,
    PhoneNumber,
    CustomerFullname,
    Gender,
    Nationality,
    ContactAddress,
    Provinces,
    Districts,
    IdentityCard,
    AppointmentDate,
    AppointmentWorkTime,
    DateOnset,
    Prognostic,
    StreetHasCovid,
    NationPassOther,
    TestedCovid,
    HumanHasCovid,
    Invoice,
    AddressProfile,
    EmailProfile,
    NameCompany,
    TaxCode,
    AddressCompany,
    EmailCompany,
    PrintName,
    IsCovid,
    TowInjections,
    GuardianPhone,
    GuardianName,
    IssuedPlace,
    IssuedDate,
    ReasonCheckup,
    StaffCode,
    PackageType,
    PackageId,
    DateofBirth,
    TypeCustomer,
    Facility,
    GuardianDateOfBirth,
    GuardianRelationship,
    Reson
  } = useGetCovidConfirmForm({
    name: 'add-appointment-form',
    responsive: true
    // appointmentInput: { patient: { nationality: 'VN', relationType: "PATIENT", countryName: "VIỆT NAM" }, channelType: "CCOMED" }
  });

  const exportPDF = async () => {
    var top =
      '<head><style>@page { size: auto; /* auto is the initial value */ display: block; margin: 0 auto;  } @media print { @page { margin-top: 0.8cm; } body { margin-top: 0.8cm; }</style></head><body>';
    var end = '</body>';
    if (langauge == 'VN') {
      var string = renderToString(
        <KBYTVNGenPDF
          districs={districtsPatient}
          provinces={provinces}
          dataEdit={valueEx}
          print={true}
          display={true}
        />
      );
      var doc = window.open('', 'PRINT') as Window;
      doc.document.write(top + string + end);
      doc.print();
      doc.close();
    } else if (langauge == 'EN') {
      var string = renderToString(
        <KBYTENGGenPDF districs={districtsPatient} provinces={provinces} dataEdit={valueEx} print={true} />
      );
      var doc = window.open('', 'PRINT') as Window;
      doc.document.write(top + string + end);
      doc.print();
      doc.close();
    }
  };

  const getOtp = async () => {
    setIsprint('1');
    var values = await form.validateFields();
    var item = {
      ...valueEx
    };
    if (item) {
      //var location = getCodeFactility(item.examFacilityId);
      if (item.healthDeclaration) {
        //item.healthDeclaration.location = location;
        item.healthDeclaration.invoice = item.healthDeclaration?.invoice?.length > 0 ? 1 : '';
        item.healthDeclaration.printName =
          item.healthDeclaration?.typeInvoice == '1' && values.healthDeclaration?.printName?.length > 0 ? '' : 1;
        item.healthDeclaration.prognostic = item.healthDeclaration?.prognostic?.toString();
      }
    }

    item.appointmentDate =
      moment(item?.appointmentDate).format('YYYY-MM-DD') != 'Invalid date'
        ? moment(item?.appointmentDate).format('YYYY-MM-DD')
        : fromatDateYYYYMMDD(item?.appointmentDate);
    if (item?.issuedDate)
      item.issuedDate =
        moment(item?.issuedDate).format('YYYY-MM-DD') != 'Invalid date'
          ? moment(item?.issuedDate).format('YYYY-MM-DD')
          : fromatDateYYYYMMDD(item?.issuedDate);
    item.patientBirthDate =
      moment(item?.patientBirthDate).format('YYYY-MM-DD') != 'Invalid date'
        ? moment(item?.patientBirthDate).format('YYYY-MM-DD')
        : fromatDateYYYYMMDD(item?.patientBirthDate);

    if (item?.guardianDate)
      item.guardianDate =
        moment(item?.guardianDate).format('YYYY-MM-DD') != 'Invalid date'
          ? moment(item?.guardianDate).format('YYYY-MM-DD')
          : fromatDateYYYYMMDD(item?.guardianDate);
    if (item?.healthDeclaration) {
      if (item?.healthDeclaration.dateOnset)
        item.healthDeclaration.dateOnset =
          moment(item?.healthDeclaration.dateOnset).format('YYYY-MM-DD') != 'Invalid date'
            ? moment(item?.healthDeclaration.dateOnset).format('YYYY-MM-DD')
            : fromatDateYYYYMMDD(item?.healthDeclaration.dateOnset);
      if (item?.healthDeclaration.dateHascovid) {
        item.healthDeclaration.dateHascovid =
          moment(item?.healthDeclaration.dateHascovid).format('YYYY-MM-DD') != 'Invalid date'
            ? moment(item?.healthDeclaration.dateHascovid).format('YYYY-MM-DD')
            : fromatDateYYYYMMDD(item?.healthDeclaration.dateHascovid);
      }
    }

    if (txtTime) item.workTimeName = txtTime;
    setValueEx(item);
    var result: any = await confirmKbytKH(item);
    if (result?.data.status == 1) {
      setIsOpenOtp(true);
      setTransactionId(result?.data.data);
    } else {
      openNotificationRight('Lỗi : ' + result?.data?.message, 'error');
    }
  };

  const fromatDateYYYYMMDD = value => {
    console.log(value);
    if (value) {
      var list = value.split('/');
      if (list) {
        return list[2] + '-' + list[1] + '-' + list[0];
      }
    }

    return '';
  };

  useEffect(() => {
    if (isDetail && itemE) {
      setItemApp(itemE);
      const value: AppoimentCovidConfrim = {
        ...itemE.patients[0],
        covidServiceId: itemE?.covidServiceId
        // ...itemE
      };

      value.appointmentDate = moment(value.appointmentDate);
      if (value.issuedDate) value.issuedDate = moment(value.issuedDate);
      value.patientBirthDate = moment(value.patientBirthDate);
      if (value.guardianDate) value.guardianDate = moment(value.guardianDate);
      if (value.healthDeclaration) {
        setIsCovided(
          value?.healthDeclaration?.hasCovid == '1' || value?.healthDeclaration?.hasCovid == '3' ? true : false
        );
        if (value.healthDeclaration.dateOnset)
          value.healthDeclaration.dateOnset = moment(value.healthDeclaration.dateOnset);
        if (value.healthDeclaration.dateHascovid)
          value.healthDeclaration.dateHascovid = moment(value.healthDeclaration.dateHascovid);
        if (value.healthDeclaration.invoice == '1') {
          value.healthDeclaration.invoice = value.healthDeclaration.invoice.split(',');
          setIsInvoice(true);
        }
        if (value.healthDeclaration.prognostic) {
          value.healthDeclaration.prognostic = value.healthDeclaration?.prognostic?.split(',');
        }
        if (value.healthDeclaration.testedCovid || value.healthDeclaration.testedCovid == 0) {
          value.healthDeclaration.testedCovid = value.healthDeclaration.testedCovid?.toString();
        }
        if (value.healthDeclaration.humanHasCovid || value.healthDeclaration.humanHasCovid == 0) {
          value.healthDeclaration.humanHasCovid = value.healthDeclaration.humanHasCovid?.toString();
        }
        if (value.healthDeclaration.nationPassOther || value.healthDeclaration.nationPassOther == 0) {
          value.healthDeclaration.nationPassOther = value.healthDeclaration.nationPassOther?.toString();
        }
        if (value.healthDeclaration.streetHasCovid || value.healthDeclaration.streetHasCovid == 0) {
          value.healthDeclaration.streetHasCovid = value.healthDeclaration.streetHasCovid?.toString();
        }
        if (value.healthDeclaration.typeCustomerKBYT || value.healthDeclaration.typeCustomerKBYT == 0) {
          value.healthDeclaration.typeCustomerKBYT = value.healthDeclaration.typeCustomerKBYT?.toString();
        }
        if (value.healthDeclaration && value.healthDeclaration.typeInvoice) {
          setTypeInvoice(value.healthDeclaration.typeInvoice);
        }
      }
      value.confirm = ['1'];
      if (value.provinceId) {
        loadDistrictsPatient({
          variables: {
            provinceId: value.provinceId
          }
        });
      }

      form.setFieldsValue({
        ...value
      });
      value.appointmentDate = value.appointmentDate.format('DD/MM/YYYY');
      if (value.issuedDate) value.issuedDate = moment(value.issuedDate).format('DD/MM/YYYY');
      value.patientBirthDate = moment(value.patientBirthDate).format('DD/MM/YYYY');
      if (value.guardianDate) value.guardianDate = moment(value.guardianDate).format('DD/MM/YYYY');
      if (value.healthDeclaration) {
        if (value.healthDeclaration.dateOnset)
          value.healthDeclaration.dateOnset = moment(value.healthDeclaration.dateOnset).format('DD/MM/YYYY');
        if (value.healthDeclaration.dateHascovid)
          value.healthDeclaration.dateHascovid = moment(value.healthDeclaration.dateHascovid).format('DD/MM/YYYY');
      }
      setValueEx(value);
    }
  }, [isDetail]);

  const getCodeFactility = examFacilityId => {
    if (facilitiesHome.length > 0 && examFacilityId) {
      for (var i = 0; i < facilitiesHome.length; i++) {
        if (facilitiesHome[i].id == examFacilityId) {
          return facilitiesHome[i].code;
          break;
        }
      }
    }
    return null;
  };

  useEffect(() => {
    loadWorktimes({
      variables: {
        type: 'STAFF'
      }
    });
    loadCountries();
    loadProvinces();
    loadFacilitiesHome({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'isHome',
            value: '1',
            operation: '=='
          },
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
  }, []);

  //lay danh sach khun gio
  const { worktimes, isLoadingWorktimes, loadWorktimes } = useGetAllWorktimes();
  //Hook Lấy danh sách nơi khám tại nhà
  const {
    facilities: facilitiesHome,
    isLoadingFacilities: isLoadingFacilitiesHome,
    loadFacilities: loadFacilitiesHome
  } = useGetFacilitys();

  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //Hook Lấy danh sách Quận/Huyện
  const {
    districts: districtsPatient,
    isLoadingDistricts: isLoadingDistrictsPatient,
    loadDistricts: loadDistrictsPatient
  } = useGetDistricts();
  //Hook Lấy danh sách quốc tịch
  const { pagingCountries, isLoadingCountries, loadCountries, errorCountries } = useGetAllCountriesLazy();
  const [isCovided, setIsCovided] = useState(false);
  const [valueEx, setValueEx]: any = useState();

  const approvalItem = async () => {
    var item = { ...itemApp };
    console.log(item);
    item.otp = otp;
    item.transactionId = transactionId;
    item.appointmentDate = valueEx.appointmentDate;
    item.workTimeId = valueEx.workTimeId;
    item.workTimeName = valueEx.workTimeName;
    console.log(item);
    if (item?.patients?.length > 0) {
      item.patients[0] = { ...valueEx };
    } else {
      var itemE = {
        ...valueEx
      };
      console.log(itemE);
      var list: any = [];
      list.push(itemE);
      console.log(list);
      item.patients = list;
    }
    console.log(item);
    var result: any = await approveKbytKH(item);
    if (result?.data.status == 1) {
      setValueEx({
        ...valueEx,
        patientLabel: 'Đã có'
      });
      setIsOpenOtp(false);
      openNotificationRight('Xác nhận thành công', 'error');
    } else {
      openNotificationRight('Lỗi : ' + result?.data?.message, 'error');
    }
  };
  // console.log(valueEx);
  return (
    <Modal
      className="appointment-dialog"
      maskClosable={false}
      style={{ marginTop: 5 }}
      centered
      width={1000}
      onOk={getOtp}
      visible={isDetail}
      onCancel={() => setIsDetail(false)}
      cancelText="Đóng"
      okText="Xác nhận thông tin khách hàng"
      okButtonProps={{ disabled: valueEx?.patientLabel ? true : false }}
      title={
        <div>
          Xác nhận
          <select
            style={{
              marginLeft: 5
            }}
            value={isPrint}
            onChange={value => {
              var itemV = { ...valueEx };
              if (itemV && value?.target.value == '1') {
                console.log('test');
                itemV.appointmentDate = moment(new Date(fromatDateYYYYMMDD(itemV?.appointmentDate)));
                if (itemV?.issuedDate) itemV.issuedDate = moment(new Date(fromatDateYYYYMMDD(itemV?.issuedDate)));
                itemV.patientBirthDate = moment(new Date(fromatDateYYYYMMDD(itemV?.patientBirthDate)));
                if (itemV?.guardianDate) itemV.guardianDate = moment(new Date(fromatDateYYYYMMDD(itemV?.guardianDate)));
                if (itemV?.healthDeclaration) {
                  if (itemV?.healthDeclaration.dateOnset)
                    itemV.healthDeclaration.dateOnset = moment(
                      new Date(fromatDateYYYYMMDD(itemV?.healthDeclaration.dateOnset))
                    );
                  if (itemV?.healthDeclaration.dateHascovid)
                    itemV.healthDeclaration.dateHascovid = moment(
                      new Date(fromatDateYYYYMMDD(itemV?.healthDeclaration.dateHascovid))
                    );
                }
                form.setFieldsValue({
                  ...itemV
                });
              } else {
                console.log('test1');
                var itemV = { ...valueEx };
                itemV.appointmentDate =
                  moment(itemV?.appointmentDate).format('DD/MM/YYYY') == 'Invalid date'
                    ? itemV?.appointmentDate
                    : moment(itemV?.appointmentDate).format('DD/MM/YYYY');
                if (itemV?.issuedDate)
                  itemV.issuedDate =
                    moment(itemV?.issuedDate).format('DD/MM/YYYY') == 'Invalid date'
                      ? itemV?.issuedDate
                      : moment(itemV?.issuedDate).format('DD/MM/YYYY');
                itemV.patientBirthDate =
                  moment(itemV?.patientBirthDate).format('DD/MM/YYYY') == 'Invalid date'
                    ? itemV?.issuedDate
                    : moment(itemV?.issuedDate).format('DD/MM/YYYY');
                if (itemV?.guardianDate)
                  itemV.guardianDate =
                    moment(itemV?.guardianDate).format('DD/MM/YYYY') == 'Invalid date'
                      ? itemV?.guardianDate
                      : moment(itemV?.guardianDate).format('DD/MM/YYYY');
                if (itemV?.healthDeclaration) {
                  if (itemV?.healthDeclaration.dateOnset)
                    itemV.healthDeclaration.dateOnset =
                      moment(itemV?.healthDeclaration.dateOnset).format('DD/MM/YYYY') == 'Invalid date'
                        ? itemV?.healthDeclaration.dateOnset
                        : moment(itemV?.healthDeclaration.dateOnset).format('DD/MM/YYYY');
                  if (itemV?.healthDeclaration.dateHascovid)
                    itemV.healthDeclaration.dateHascovid =
                      moment(itemV?.healthDeclaration.dateHascovid).format('DD/MM/YYYY') == 'Invalid date'
                        ? itemV?.healthDeclaration.dateHascovid
                        : moment(itemV?.healthDeclaration.dateHascovid).format('DD/MM/YYYY');
                }
                setValueEx(itemV);
              }
              setIsprint(value?.target.value);
            }}
          >
            <option value={'0'}>Xem biên bản</option>
            <option value={'1'}>Chỉnh sửa</option>
          </select>
          <select
            style={{
              marginLeft: 5
            }}
            value={langauge}
            onChange={value => {
              setLangauge(value?.target.value);
            }}
          >
            <option value={'VN'}>Việt Nam</option>
            <option value={'EN'}>English</option>
          </select>
          {valueEx?.patientLabel && (
            <span
              style={{
                marginLeft: 10,
                color: '#206ad2',
                cursor: 'pointer',
                fontSize: 30
              }}
              onClick={exportPDF}
            >
              <PrinterOutlined size={30} />
            </span>
          )}
        </div>
      }
    >
      <Modal
        className="appointment-dialog"
        maskClosable={false}
        style={{ marginTop: 5 }}
        title="Xác nhận thông tin khách hàng"
        visible={isOpenOtp}
        okText="Đồng ý xác thực thông tin"
        onOk={approvalItem}
        onCancel={() => setIsOpenOtp(false)}
        cancelText="Đóng"
      >
        <div
          style={{
            textAlign: 'center',
            fontSize: 18
          }}
        >
          <div>Mã xác thực đã được gửi tới SĐT của khách hàng</div>
          <div
            style={{
              marginTop: 5,
              marginBottom: 5
            }}
          >
            {form.getFieldValue('phone')}
          </div>
          <Space>
            <Input
              onChange={e => {
                setOtp(e.target.value);
              }}
              placeholder="Gõ SĐT tìm kiếm thông tin"
              style={{ width: 210 }}
            />
          </Space>
        </div>
      </Modal>
      {isPrint == '0' && (
        <div style={{ width: '100%' }}>
          {langauge == 'EN' && (
            <KBYTENGGenPDF districs={districtsPatient} provinces={provinces} dataEdit={valueEx} print={false} />
          )}
          {langauge == 'VN' && (
            <KBYTVNGenPDF
              districs={districtsPatient}
              provinces={provinces}
              dataEdit={valueEx}
              print={false}
              display={true}
            />
          )}
        </div>
      )}

      {isPrint == '1' && (
        <Form {...formItemLayout} className="new-add-appointment-form">
          <Row style={{ width: '100%' }}>
            <AppointmentDate valueEx={valueEx} onChangeValue={setValueEx} />
            <PackageId valueEx={valueEx} onChangeValue={setValueEx} />
            <AppointmentWorkTime appointmentWorkTime={worktimes} isLoading={isLoadingWorktimes} setTxt={setTxtTime} />
            <PackageType
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  examType: value
                });
              }}
            />
            <CustomerFullname
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  patientName: value
                });
              }}
            />
            <Nationality
              onChange={(id, name) => {
                setValueEx({
                  ...valueEx,
                  nationality: id,
                  countryName: name
                });
              }}
              countries={pagingCountries?.data}
              isLoading={isLoadingCountries}
            />
            <DateofBirth
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  patientBirthDate: value
                });
              }}
            />
            <Gender
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  patientSex: value
                });
              }}
            />
            <Provinces
              valueEx={valueEx}
              setValueEx={setValueEx}
              isLoading={isLoadingProvince}
              provinceList={provinces}
              handleChangeProvince={value => {
                if (value) {
                  loadDistrictsPatient({
                    variables: {
                      provinceId: value
                    }
                  });
                }
              }}
            />
            <Districts
              valueEx={valueEx}
              setValueEx={setValueEx}
              districtList={districtsPatient}
              isLoading={isLoadingDistrictsPatient}
              handleChangeDistrict={() => {}}
            />
            <ContactAddress
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  address: value
                });
              }}
            />
            <StaffCode
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  preDoctorHisId: value
                });
              }}
            />
            <PhoneNumber
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  phone: value
                });
              }}
              handleChangePatient={value => {}}
              handleChangeProvince={provinceId => {}}
            />
            <IdentityCard
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  patientIdNo: value
                });
              }}
            />
            <IssuedDate
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  issuedDate: value
                });
              }}
            />
            <IssuedPlace
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  issuedPlace: value
                });
              }}
            />
            <GuardianName
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  guardianName: value
                });
              }}
            />
            <GuardianPhone
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  guardianPhone: value
                });
              }}
            />
            <ReasonCheckup
              onChange={value => {
                setValueEx({
                  ...valueEx,
                  reasonNote: value
                });
              }}
            />
            <div className="kbyt-title">Khai báo y tế dành cho khách hàng đăng ký khám</div>
            <div className="kbyt-sub-title">
              <p>* Phiếu khai báo thông tin y tế chỉ có giá trị trong vòng 24 giờ </p>
              <p>* Cảnh báo: Khai báo thông tin sai là vi phạm pháp luật Việt Nam, có thể bị xử lý hình sự!</p>
            </div>
            <TypeCustomer
              onChange={value => {
                var healthDeclaration = valueEx['healthDeclaration'];
                healthDeclaration = {
                  ...healthDeclaration,
                  typeCustomerKBYT: value
                };
                setValueEx({
                  ...valueEx,
                  healthDeclaration: healthDeclaration
                });
              }}
            />
            <DateOnset
              onChange={value => {
                var healthDeclaration = valueEx['healthDeclaration'];
                healthDeclaration = {
                  ...healthDeclaration,
                  dateOnset: value
                };
                setValueEx({
                  ...valueEx,
                  healthDeclaration: healthDeclaration
                });
              }}
            />
            <Prognostic
              onChange={value => {
                var healthDeclaration = valueEx['healthDeclaration'];
                healthDeclaration = {
                  ...healthDeclaration,
                  prognostic: value
                };
                setValueEx({
                  ...valueEx,
                  healthDeclaration: healthDeclaration
                });
              }}
            />
            <div>
              <span className="red">*</span>
              <b> Mục B. Yếu tố dịch tễ trong 14 ngày qua</b>
            </div>
            <HumanHasCovid
              onChange={value => {
                var healthDeclaration = valueEx['healthDeclaration'];
                healthDeclaration = {
                  ...healthDeclaration,
                  humanHasCovid: value
                };
                setValueEx({
                  ...valueEx,
                  healthDeclaration: healthDeclaration
                });
              }}
            />
            <StreetHasCovid
              onChange={value => {
                var healthDeclaration = valueEx['healthDeclaration'];
                healthDeclaration = {
                  ...healthDeclaration,
                  streetHasCovid: value
                };
                setValueEx({
                  ...valueEx,
                  healthDeclaration: healthDeclaration
                });
              }}
            />
            <TestedCovid
              onChange={value => {
                var healthDeclaration = valueEx['healthDeclaration'];
                healthDeclaration = {
                  ...healthDeclaration,
                  streetHasCovid: value
                };
                setValueEx({
                  ...valueEx,
                  healthDeclaration: healthDeclaration
                });
              }}
            />
            <div className="">
              <span className="red">* </span>
              <b>Mục C. CÁC THÔNG TIN KHÁC: Hãy tích dấu “X” vào ô trống trước phần lựa chọn</b>
            </div>
            <IsCovid
              onChange={value => {
                var healthDeclaration = valueEx['healthDeclaration'];
                healthDeclaration = {
                  ...healthDeclaration,
                  hasCovid: value
                };
                setValueEx({
                  ...valueEx,
                  healthDeclaration: healthDeclaration
                });
              }}
              isCovided={isCovided}
              setIsCovided={setIsCovided}
            />
            <TowInjections
              onChange={value => {
                var healthDeclaration = valueEx['healthDeclaration'];
                healthDeclaration = {
                  ...healthDeclaration,
                  hasVaccine: value
                };
                setValueEx({
                  ...valueEx,
                  healthDeclaration: healthDeclaration
                });
              }}
            />
            <div className="kbyt-confirm">
              <Form.Item name={'confirm'} rules={[{ required: true, message: 'Không được bỏ trống!' }]}>
                <CheckboxGroup name="confirm">
                  <Checkbox indeterminate={false} value="1">
                    <span className="red">Tôi cam kết đúng thông tin *</span>
                  </Checkbox>
                </CheckboxGroup>
              </Form.Item>
            </div>
            {/* <Invoice setIsInvoice={setIsInvoice} onChange={(value) => {
                            var healthDeclaration = valueEx["healthDeclaration"];
                            healthDeclaration = {
                                ...healthDeclaration,
                                invoice: value
                            }
                            setValueEx({
                                ...valueEx,
                                healthDeclaration: healthDeclaration
                            })
                        }} />
                        {
                            isInvoice &&
                            <Tabs
                                defaultActiveKey={typeInvoice}
                                activeKey={typeInvoice}
                                type="card"
                                onTabClick={activeKey => {
                                    setTypeInvoice(activeKey);
                                    if (typeInvoice == "0") {
                                        var healthDeclaration = valueEx["healthDeclaration"];
                                        healthDeclaration = {
                                            ...healthDeclaration,
                                            typeInvoice: "0"
                                        }
                                        setValueEx({
                                            ...valueEx,
                                            healthDeclaration: healthDeclaration
                                        })
                                        form.setFieldsValue({
                                            healthDeclaration: {
                                                printName: undefined,
                                                nameCompany: undefined,
                                                addressCompany: undefined,
                                                taxCode: undefined,
                                                emailCompany: undefined
                                            }
                                        })
                                    } else {
                                        var healthDeclaration = valueEx["healthDeclaration"];
                                        healthDeclaration = {
                                            ...healthDeclaration,
                                            typeInvoice: "1"
                                        }
                                        setValueEx({
                                            ...valueEx,
                                            healthDeclaration: healthDeclaration
                                        })
                                        form.setFieldsValue({
                                            healthDeclaration: {
                                                emailProfile: undefined,
                                                addressProfile: undefined
                                            }
                                        })
                                    }

                                }}
                            >
                                <TabPane
                                    tab={
                                        <span>
                                            Cá nhân
                                        </span>
                                    }
                                    key="0"
                                    forceRender={true}
                                >
                                    <Row>
                                        <AddressProfile onChange={(value) => {
                                            var healthDeclaration = valueEx["healthDeclaration"];
                                            healthDeclaration = {
                                                ...healthDeclaration,
                                                addressProfile: value
                                            }
                                            setValueEx({
                                                ...valueEx,
                                                healthDeclaration: healthDeclaration
                                            })
                                        }} required={typeInvoice == "0" ? true : false} />
                                        <EmailProfile onChange={(value) => {
                                            var healthDeclaration = valueEx["healthDeclaration"];
                                            healthDeclaration = {
                                                ...healthDeclaration,
                                                addressProfile: value
                                            }
                                            setValueEx({
                                                ...valueEx,
                                                healthDeclaration: healthDeclaration
                                            })
                                        }} required={typeInvoice == "0" ? true : false} />
                                    </Row>
                                </TabPane>
                                <TabPane
                                    tab={
                                        <span>
                                            Công ty
                                        </span>
                                    }
                                    key="1"
                                    forceRender={true}
                                >
                                    <Row>
                                        <NameCompany onChange={(value) => {
                                            var healthDeclaration = valueEx["healthDeclaration"];
                                            healthDeclaration = {
                                                ...healthDeclaration,
                                                nameCompany: value
                                            }
                                            setValueEx({
                                                ...valueEx,
                                                healthDeclaration: healthDeclaration
                                            })
                                        }} required={typeInvoice == "1" ? true : false} />
                                        <TaxCode onChange={(value) => {
                                            var healthDeclaration = valueEx["healthDeclaration"];
                                            healthDeclaration = {
                                                ...healthDeclaration,
                                                taxCode: value
                                            }
                                            setValueEx({
                                                ...valueEx,
                                                healthDeclaration: healthDeclaration
                                            })
                                        }} required={typeInvoice == "1" ? true : false} />
                                        <AddressCompany onChange={(value) => {
                                            var healthDeclaration = valueEx["healthDeclaration"];
                                            healthDeclaration = {
                                                ...healthDeclaration,
                                                addressCompany: value
                                            }
                                            setValueEx({
                                                ...valueEx,
                                                healthDeclaration: healthDeclaration
                                            })
                                        }} required={typeInvoice == "1" ? true : false} />
                                        <EmailCompany onChange={(value) => {
                                            var healthDeclaration = valueEx["healthDeclaration"];
                                            healthDeclaration = {
                                                ...healthDeclaration,
                                                emailCompany: value
                                            }
                                            setValueEx({
                                                ...valueEx,
                                                healthDeclaration: healthDeclaration
                                            })
                                        }} required={typeInvoice == "1" ? true : false} />
                                        <PrintName />
                                    </Row>
                                </TabPane>
                            </Tabs>
                        } */}
          </Row>
        </Form>
      )}
    </Modal>
  );
};

export default CovidConfirmForm;
