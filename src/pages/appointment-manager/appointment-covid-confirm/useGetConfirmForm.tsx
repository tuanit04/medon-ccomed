import { Checkbox, Col, ColProps, DatePicker, Form, FormProps, Input, Modal, Row, Select, Image, Radio, Button } from 'antd';

import { Country } from 'common/interfaces/country.interface';
import { District } from 'common/interfaces/district.interface';
import { Patient } from 'common/interfaces/patient.interface';
import { Province } from 'common/interfaces/province.interface';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import moment from 'moment';
import React, { FC, useState } from 'react';
import AppointmentPhoneSearch from '../../common/appointmentPhoneSearch';
import { Facility } from 'common/interfaces/facility.interface';
import { AppoimentCovidConfrim } from 'common/interfaces/appoimentCovidConfrim.interface';
import { PhoneOutlined } from '@ant-design/icons';
import { csCallout } from 'utils/vcc/actions';

const { Option } = Select;
const { TextArea } = Input;
const CheckboxGroup = Checkbox.Group;
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
  xxl: 12
};

const wrapperColAllCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const wrapperColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 20,
  xl: 20,
  xxl: 20
};

const wrapperColOneItemNew: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 19,
  xl: 19,
  xxl: 19
};

const labelColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 4,
  xl: 4,
  xxl: 4
};

const labelColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperSDTInput: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 9,
  xl: 9,
  xxl: 9
};

const wrapperSDT: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 16,
  xl: 16,
  xxl: 16
};

const wapperColPhoneInput: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 16,
  xl: 16,
  xxl: 16
}

const wapperColPhoneButton: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
}

interface AddGetConfirmFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  appointmentInput?: Partial<AppoimentCovidConfrim>;
}

export default function useGetCovidConfirmForm({
  required = false,
  responsive = false,
  name = 'form',
  appointmentInput
}: AddGetConfirmFormProps) {
  const [formInstance] = Form.useForm<Partial<AppoimentCovidConfrim>>();
  const [patternIdNo, setPatternIdNo] = useState(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
  //Hook Lấy danh sách Quận/Huyện
  const { districts, loadDistricts, isLoadingDistricts } = useGetDistricts();
  const [districCode, setDistricCode]: any = useState();
  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const onValuesChange = (changedValues: any, values: AppoimentCovidConfrim) => { };

    return (
      <Form
        {...props}
        // {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={{ ...appointmentInput, appointmentDate: moment() }}
        onValuesChange={onValuesChange}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };
  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Ngày lấy mẫu
  interface AppointmentDateProps {
    onChange?: (value: any, dateString: any) => void;
    handleChangAppointmentDate?: (examFacilityId: string, appointmentDate: string) => void;
    onChangeValue?;
    valueEx?: AppoimentCovidConfrim
  }
  const AppointmentDate: FC<AppointmentDateProps> = ({ valueEx, onChangeValue }) => {
    const disablePastDt = current => {
      const yesterday = moment().subtract(1, 'day');
      return current.isBefore(yesterday);
    };
    const appointmentDate = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Ngày lấy mẫu:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required
            name="appointmentDate"
            rules={[{ required: true, message: 'Ngày đặt không được để trống.' }]}
          >
            <DatePicker
              onChange={(date, dateString) => {
                //   handleDatePickerChange(date, dateString,1);
                onChangeValue({
                  ...valueEx,
                  appointmentDate: dateString
                });
              }}
              disabledDate={disablePastDt}
              format="DD/MM/YYYY"
              placeholder="-- Chọn ngày --"
            />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentDate}</Col> : appointmentDate;
  };

  //Dịch vụ xét nghiệm
  interface PackageIdProp {
    valueEx: any;
    onChangeValue;
  }
  const PackageId: FC<PackageIdProp> = ({ valueEx, onChangeValue }) => {
    const packageId = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Dịch vụ xét nghiệm:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={'covidServiceId'}
            rules={[{ required: true, message: 'Dịch vụ xét nghiệm không được để trống.' }]}
          >
            <Select onChange={(value) => {
              onChangeValue({
                ...valueEx,
                covidServiceId: value
              })
            }} placeholder="-- Chọn dịch vụ --">
              <Option value="895">Xét nghiệm PCR</Option>
              {/* <Option value="896">Test nhanh</Option> */}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{packageId}</Col> : packageId;
  };

  //Khung giờ
  interface AppointmentWorkTimeProps {
    appointmentWorkTime: any;
    isLoading: boolean;
    setTxt?: any;
  }
  const AppointmentWorkTime: FC<AppointmentWorkTimeProps> = ({ appointmentWorkTime, isLoading, setTxt }) => {
    const isToday = (someDate: Date) => {
      const today = new Date();
      return (
        someDate.getDate() == today.getDate() &&
        someDate.getMonth() == today.getMonth() &&
        someDate.getFullYear() == today.getFullYear()
      );
    };

    const handleChangeWorkTime = (value: string) => { };
    const handleSelectWorkTime = (value, e) => {
      let appointmentDate = formInstance.getFieldValue('appointmentDate');
      if (!appointmentDate) {
        return;
      } else if (isToday(new Date(String(appointmentDate)))) {
        var today = new Date();
        var timeCurrent = today.getHours() + ':' + today.getMinutes() + ':00';
        const workTimeSelectedArr = e['key']?.split('-');
        if (workTimeSelectedArr) {
          const startTime = workTimeSelectedArr[0];
          const endTime = workTimeSelectedArr[1];
          //The 1st January is an arbitrary date, doesn't mean anything.
          if (Date.parse('01/01/2011 ' + timeCurrent) >= Date.parse('01/01/2011 ' + startTime + ':00')) {
            //openNotificationRight('Khung giờ vừa chọn đã trôi qua hoặc đang nằm trong thời gian hiện tại, vui lòng chọn đúng khung giờ.');
            formInstance.setFieldsValue({
              workTimeId: undefined
            });
            return false;
          } else {
            return true;
          }
        }
      }
    };
    let appointmentDate = formInstance.getFieldValue('appointmentDate');
    let appointmentWorkTimeArr: any = [];
    if (appointmentDate && isToday(new Date(String(appointmentDate)))) {
      for (let i = 0; i < appointmentWorkTime?.length; i++) {
        var today = new Date();
        var timeCurrent = today.getHours() + ':' + today.getMinutes() + ':00';
        const startTime = appointmentWorkTime[i]['startTime'];
        const endTime = appointmentWorkTime[i]['endTime'];
        appointmentWorkTimeArr.push(appointmentWorkTime[i]);
      }
    } else if (appointmentWorkTime) {
      appointmentWorkTimeArr.push(...appointmentWorkTime);
    }
    const appointmentWorkTimeCpn = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Khung giờ:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="workTimeId" rules={[{ required: true, message: 'Khung giờ không hợp lệ.' }]}>
            <Select
              loading={isLoading}
              style={{ width: '100%' }}
              showSearch
              onChange={(value, name) => {
                setTxt(name['children']);
              }}
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn khung giờ --"
            >
              {appointmentWorkTime === undefined
                ? []
                : appointmentWorkTime?.map((option, index) => {
                  var today = new Date();
                  var timeCurrent = today.getHours() + ':' + today.getMinutes() + ':00';
                  const startTime = option['startTime'];
                  if (appointmentDate && isToday(new Date(String(appointmentDate)))) {
                    if (Date.parse('01/01/2011 ' + timeCurrent) >= Date.parse('01/01/2011 ' + startTime + ':00')) {
                      return (<Select.Option disabled={true} key={option['startTime'] + '-' + option['endTime']} value={option['id']}>
                        {option['name']}
                      </Select.Option>)
                    } else {
                      return (<Select.Option key={option['startTime'] + '-' + option['endTime']} value={option['id']}>
                        {option['name']}
                      </Select.Option>)
                    }
                  } else {
                    return (<Select.Option key={option['startTime'] + '-' + option['endTime']} value={option['id']}>
                      {option['name']}
                    </Select.Option>)
                  }
                }
                )}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentWorkTimeCpn}</Col> : appointmentWorkTimeCpn;
  };

  //Loại xét nghiệm
  interface PackageTypeProps {
    onChange;
  }
  const PackageType: FC<PackageTypeProps> = ({ onChange }) => {
    const packageType = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Loại xét nghiệm:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={'examType'} rules={[{ required: true, message: 'Loại xét nghiệm không được để trống.' }]}>
            <Select
              placeholder="-- Chọn loại --"
              onChange={value => {
                onChange(value);
              }}
            >
              <Option value="0">Cá nhân</Option>
              <Option value="2">Nhóm 2 KH</Option>
              <Option value="3">Nhóm 3 KH</Option>
              <Option value="4">Nhóm 4 KH</Option>
              <Option value="5">Nhóm 5 KH</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{packageType}</Col> : packageType;
  };

  //Ho ten KH
  interface CustomerFullnameProps {
    onChange
  }
  const CustomerFullname: FC<CustomerFullnameProps> = ({ onChange }) => {
    const customerFullname = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Họ tên KH:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            normalize={value => value?.toUpperCase()}
            name={"patientName"} rules={[{ required: true, message: 'Họ tên KH không được để trống.' }]}>
            <Input onChange={value => {
              onChange(value.target.value)
            }} placeholder="Nhập họ tên khách hàng" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{customerFullname}</Col> : customerFullname;
  };

  //Quốc tịch
  interface NationalityProps {
    countries: Country[];
    isLoading: boolean;
    onChange: any;
  }
  const Nationality: FC<NationalityProps> = ({ countries, isLoading, onChange }) => {
    const nationality = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Quốc tịch:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={"nationality"}
            rules={[{ required: true, message: 'Quốc tịch không được để trống.' }]}
          >
            <Select
              loading={isLoading}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Quốc tịch --"
              onChange={(value, item) => {
                if (String(value).toUpperCase() === 'VN') {
                  setPatternIdNo(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
                } else {
                  setPatternIdNo(/^[a-zA-Z0-9_.-]{6,13}$/ as RegExp);
                }
                formInstance.setFieldsValue({ countryName: item['children'] });
                onChange(value, item['children'])
              }}
            >
              {countries?.map(country => (
                <Select.Option key={country.id} value={country.countryCode} o>
                  {country.countryName}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item style={{ display: 'none' }} name={['patient', 'countryName']}>
            <Input />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{nationality}</Col> : nationality;
  };

  //Ngay sinh
  const handleCheckInput = () => {
    var date = document.getElementById('date-picker-antd-appointment-create');
    function checkValue(str, max) {
      if (str.charAt(0) !== '0' || str == '00') {
        var num = parseInt(str);
        if (isNaN(num) || num <= 0 || num > max) num = 1;
        str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
      }
      return str;
    }
    date?.addEventListener('input', function (e) {
      if (date) {
        let dateInput: HTMLElement = date;
        dateInput['type'] = 'text';
        var input = dateInput['value'];
        if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
        var values = input.split('/').map(function (v) {
          return v.replace(/\D/g, '');
        });
        if (values[0]) values[0] = checkValue(values[0], 31);
        if (values[1]) values[1] = checkValue(values[1], 12);
        var output = values.map(function (v, i) {
          return v.length == 2 && i < 2 ? v + '/' : v;
        });
        dateInput['value'] = output.join('').substr(0, 14);
        let birthDay = output.join('').substr(0, 14);

        if (dateInput['value'].length === 10) {
          formInstance.setFieldsValue({
            patientBirthDate: moment(birthDay, 'DD/MM/YYYY')
          });
        }
      }
    });
  };
  interface DateofBirthProps {
    onChange
  }
  const DateofBirth: FC<DateofBirthProps> = ({ onChange }) => {
    const dateFormat = 'DD/MM/YYYY';
    const dofBirth = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Ngày sinh:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={"patientBirthDate"}
            rules={[{ required: true, message: 'Ngày sinh không được để trống.' }]}
          >
            <DatePicker
              onFocus={() => handleCheckInput()}
              id="date-picker-antd-appointment-create"
              onChange={(value, dateString) => {
                onChange(dateString);
              }}
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              placeholder="Chọn ngày sinh"
              format={dateFormat}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{dofBirth}</Col> : dofBirth;
  };

  //Gioi tinh
  interface GenderProps {
    onChange
  }
  const Gender: FC<GenderProps> = ({ onChange }) => {
    const gender = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Giới tính:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={"patientSex"} rules={[{ required: true, message: 'Giới tính không được để trống.' }]}>
            <Select onChange={value => {
              onChange(value)
            }} placeholder="-- Chọn Giới tính --">
              <Option value="MALE">Nam</Option>
              <Option value="FEMALE">Nữ</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{gender}</Col> : gender;
  };

  //Tinh/TP
  interface ProvincesProps {
    isLoading: boolean;
    provinceList: Province[];
    handleChangeProvince: (provinceId: string) => void;
    valueEx: any;
    setValueEx: any;
  }
  const Provinces: FC<ProvincesProps> = ({ provinceList, handleChangeProvince, isLoading, valueEx, setValueEx }) => {
    const onChange = (value, itemSelete) => {
      formInstance.setFieldsValue({
        districtId: undefined,
        provinceCode: itemSelete.key
      });
      setValueEx({
        ...valueEx,
        provinceId: value,
        provinceCode: itemSelete.key,
        districtId: undefined,
        districtCode: undefined
      })
      handleChangeProvince(value);
      // setFirst(true);
    };

    const provinces = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Tỉnh/TP:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            name={"provinceId"}
            rules={[
              {
                required: true,
                message: 'Tỉnh/TP không được để trống.'
              }
            ]}
          >
            <Select
              loading={isLoading}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={(value, item) => {
                onChange(value, item);
              }}
              placeholder="-- Chọn Tỉnh/TP --"
            >
              {provinceList?.map(province => (
                <Select.Option key={province.code} value={province.id}>
                  {province.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{provinces}</Col> : provinces;
  };
  //Quan/Huyen
  interface DistrictProps {
    districtList: District[];
    isLoading: boolean;
    handleChangeDistrict: (districtId: string) => void;
    valueEx: any;
    setValueEx: any;
  }

  const Districts: FC<DistrictProps> = ({ districtList, handleChangeDistrict, isLoading, valueEx, setValueEx }) => {
    const onChange = (value, item) => {
      formInstance.setFieldsValue({
        districtCode: item.key
      })
      setValueEx({
        ...valueEx,
        districtCode: item.key,
        districtId: value
      })
    };
    const districts = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Quận/Huyện:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            name={'districtId'}
            rules={[
              {
                required: true,
                message: 'Quận/Huyện không được để trống.'
              }
            ]}
          >
            <Select
              // ref={ref={disRefC}}
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={(value, item) => { onChange(value, item) }}
              placeholder="-- Chọn Quận/Huyện --"
            >
              {districtList?.map(district => (
                <Select.Option key={district.code} value={district.id}>
                  {district.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{districts}</Col> : districts;
  };

  //Dia chi lien he
  interface ContactAddressProps {
    onChange
  }
  const ContactAddress: FC<ContactAddressProps> = ({ onChange }) => {
    const contactAddress = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Địa chỉ liên hệ:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name={"address"}
            rules={[{ required: true, max: 100, message: 'Địa chỉ liên hệ không hợp lệ hoặc quá dài.' }]}
          >
            <Input onChange={value => { onChange(value.target.value) }} placeholder="Nhập Địa chỉ liên hệ" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{contactAddress}</Col> : contactAddress;
  };

  //Mã giới thiệu
  interface StaffCodeProps {
    onChange
  }
  const StaffCode: FC<StaffCodeProps> = ({ onChange }) => {
    const staffCode = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Mã giới thiệu:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="preDoctorHisId"
          // rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input onChange={value => { onChange(value?.target.value) }} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{staffCode}</Col> : staffCode;
  };

  //So dien thoai
  interface PhoneNumberProps {
    handleChangePatient: (values?: Partial<Patient>) => void;
    handleChangeProvince: (provinceId: string) => void;
    provinces?;
    onChange;
  }
  const PhoneNumber: FC<PhoneNumberProps> = ({ handleChangePatient, handleChangeProvince, provinces, onChange }) => {
    const [isCalling, setIsCalling] = useState(false);
    const [isSearching, setIsSearching] = useState(false);
    // const onApplyPID = pid => {
    //   setIsSearching(false);
    //   formInstance.setFieldsValue({
    //     pid: pid
    //   });
    // };

    const onApply = (values?: Partial<Patient>) => {
      setIsSearching(false);
      if (values?.provinceId) {
        handleChangeProvince(values?.provinceId);
      } else if (values?.provinceCode) {
        var id = '';
        provinces?.map(item => {
          if (item.code == values?.provinceCode) {
            id = item.id;
          }
        });
        if (id && id != '') {
          values.provinceId = id;
          setDistricCode(values.districtCode);
          loadDistricts({
            variables: {
              provinceId: id
            }
          });
        }
      }
      // formInstance.setFieldsValue({
      //   ...values,
      //   phone: values?.phone ? 0 + values.phone : undefined
      // });
      handleChangePatient(values);
    };

    const phoneNumber = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Số điện thoại:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Row>
            <Col {...wapperColPhoneInput}>
              <Form.Item
                name={'phone'}
                required
                rules={[
                  {
                    required: true,
                    message: 'SĐT không được để trống hoặc không hợp lệ.',
                    pattern: /(84|0[1|2|3|5|6|7|8|9])+([0-9]{8})\b/g
                  }
                ]}
              >
                <Input
                  onKeyUp={e => {
                    if (e.key == 'Enter') {
                      setIsSearching(true);
                    }
                  }}
                  placeholder="Nhập số điện thoại"
                />
              </Form.Item>
            </Col>
            <Col {...wapperColPhoneButton}>
              <Button
                className="btn-call"
                type="primary"
                // shape="circle"
                icon={<PhoneOutlined />}
                // size={size}
                onClick={() => {
                  console.log(formInstance.getFieldValue('phone'));
                  var phoneNumber = formInstance.getFieldValue('phone');
                  if (phoneNumber) {
                    console.log(phoneNumber)
                    csCallout(phoneNumber.trim());
                  }
                }}
              >
                Gọi
              </Button>
            </Col>
          </Row>
        </Col>
        <Modal
          title="Đang thực hiện cuộc gọi ..."
          visible={isCalling}
          onOk={() => { }}
          onCancel={() => {
            setIsCalling(false);
          }}
        >
          <Image width={200} src={require('../../../assets/icons/calling.gif')} />
        </Modal>
        <Modal
          maskClosable={false}
          centered
          style={{ marginTop: 5, height: 'calc(100vh - 200px)' }}
          bodyStyle={{ display: 'flex', overflowY: 'auto' }}
          title="Danh sách thông tin khách hàng"
          visible={isSearching}
          onCancel={() => {
            setIsSearching(false);
          }}
          footer={null}
          width={1000}
          destroyOnClose={true}
        >
          <AppointmentPhoneSearch
            phoneNumber={formInstance.getFieldValue("phone")}
            onApply={onApply}
          // onApplyPID={onApplyPID}
          />
        </Modal>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{phoneNumber}</Col> : phoneNumber;
  };

  //CMND/CCCD
  interface IdentityCardProps {
    isValidate?: boolean;
    onChange
  }
  const IdentityCard: FC<IdentityCardProps> = ({ isValidate, onChange }) => {
    const identityCard = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
         CMND/CCCD:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={"patientIdNo"}
            /*rules={[
                          ({ getFieldValue }) => ({
                            validator: (_, value) => {

                              var patient = getFieldValue("patient");
                              if (validateidNo(value, patient?.nationality)) {
                                return Promise.resolve();
                              }
                              return Promise.reject(new Error('Không đúng định dạng'))
                            }
                          }),
                        ]}
                        */
            rules={[{ required: false, message: 'Không đúng định dạng', pattern: patternIdNo }]}
          >
            <Input
              onChange={e => {
                // setIdNo(e.target.value);
                onChange(e.target.value);
              }}
              placeholder="Nhập CMND/CCCD"
            />
          </Form.Item>
          {/* {checkIdNo && <span>Không đúng định dạng</span>} */}
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{identityCard}</Col> : identityCard;
  };

  //Ngay cap CCCD
  interface IssuedDateProps {
    onChange
  }
  const IssuedDate: FC<IssuedDateProps> = ({
    onChange
  }) => {
    const dateFormat = 'DD/MM/YYYY';


    const issuedDate = (
      <Row className="">
        <Col {...labelColTowItem} className="fs-12">
          Ngày cấp CMT/CCCD:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={'issuedDate'}>
            <DatePicker
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              placeholder="Chọn ngày cấp CCCD"
              format={dateFormat}
              onChange={(value, dateString) => {
                console.log(dateString);
                onChange(dateString)
              }}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{issuedDate}</Col> : issuedDate;
  };

  //Nơi cấp CCCD
  interface IssuedPlaceProps {
    onChange
  }
  const IssuedPlace: FC<IssuedPlaceProps> = ({ onChange }) => {
    const issuedPlace = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Nơi cấp CMT/CCCD:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item name="issuedPlace">
            <Input onChange={value => {
              onChange(value.target.value);
            }} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{issuedPlace}</Col> : issuedPlace;
  };

  //Họ tên người giám hộ
  interface GuardianNameProps {
    onChange
  }
  const GuardianName: FC<GuardianNameProps> = ({ onChange }) => {
    const guardianName = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Họ tên người thân/giám hộ:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="guardianName"
            rules={[{ required: true, message: "Họ tên người thân/giám hộ là bất buộc" }]}
          >
            <Input onChange={value => {
              onChange(value?.target.value);
            }} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{guardianName}</Col> : guardianName;
  };

  //Số điện thoại người giám hộ
  interface GuardianPhone {
    onChange
  }
  const GuardianPhone: FC<GuardianPhone> = ({ onChange }) => {
    const guardianPhone = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>SĐT người thân/giám hộ:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="guardianPhone"
            rules={[{ required: true, message: "Bắt buộc nhập" }]}
          >
            <Input onChange={value => {
              onChange(value?.target.value);
            }} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{guardianPhone}</Col> : guardianPhone;
  };

  //Mới quan hệ người giám hộ
  const GuardianRelationship: FC = () => {
    const ruardianRelationship = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Mối quan hệ:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="relationType"
            rules={[
              {
                required: true,
                message: "Mối quan hệ là bắt buộc"
              }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{ruardianRelationship}</Col> : ruardianRelationship;
  };

  //Lý do khám
  const Reson: FC = () => {
    const reson = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Lý do khám:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="reson"
            rules={[{ required: true, message: "Bắt buộc nhập" }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{reson}</Col> : reson;
  };

  //ngay sinh người giám hộ
  const GuardianDateOfBirth: FC = () => {
    const dateFormat = 'DD/MM/YYYY';

    const guardianDateOfBirth = (
      <Row className="">
        <Col {...labelColTowItem} className="fs-12">
          Ngày sinh người giám hộ
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={'guardianDate'}>
            <DatePicker
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              placeholder="Chọn Ngày sinh người giám hộ"
              format={dateFormat}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{guardianDateOfBirth}</Col> : guardianDateOfBirth;
  };

  //Lý do khám
  interface ReasonCheckup {
    isCommit?: boolean,
    onChange: any;
  }
  const ReasonCheckup: FC<ReasonCheckup> = ({ isCommit, onChange }) => {
    const reasonCheckup = (
      <Row className="field-css">
        {!isCommit &&
          <>
            <Col {...labelColOneItem} className="fs-12">
              <span className="red">* </span> Lý do khám:
            </Col>
            <Col {...wrapperColOneItem} className="over-hidden">
              <Form.Item
                name={'reasonNote'}
                rules={[{ required: true, message: 'Giới tính không được để trống.' }]}>
                <Select onChange={value => {
                  onChange(value)
                }} placeholder="-- Chọn Lý do khám --">
                  <Option value="Khác">Khác</Option>
                  <Option value="Khám bệnh">Khám bệnh</Option>
                  {/* <Option value="SARS-COV-2-Ag">Có dịch tễ nghi ngờ (Test nhanh)</Option> */}
                  <Option value="SARS-COV-2-PCR">Có triệu chứng nghi ngờ (PCR - Đơn)</Option>
                  <Option value="SARS-COV-2-PCR-FA">Có KQXN nghi ngờ nhiễm COVID-19 (PCR-Đơn)</Option>
                  <Option value="SARS-COV-2-PCR-XC">Nhập cảnh/ Xuất cảnh (PCR-Đơn Xuất cảnh)</Option>
                </Select>
              </Form.Item>
            </Col>
          </>}
        {isCommit &&
          <>
            <Col {...labelColTowItem} className="fs-12">
              <span className="red">* </span> Lý do khám:
            </Col>
            <Col {...wrapperColTowItem} className="over-hidden">
              <Form.Item
                name={'reasonNote'}
                rules={[{ required: true, message: 'Giới tính không được để trống.' }]}>
                <Select onChange={value => {
                  onChange(value)
                }} placeholder="-- Chọn Lý do khám --">
                  <Option value="KB">Khám bệnh</Option>
                  <Option value="XN">Làm xét nghiệm trước xuất cảnh</Option>
                  <Option value="GTH">Giấy thông hành</Option>
                  <Option value="K">Khác</Option>
                </Select>
              </Form.Item>
            </Col>
          </>}
      </Row>
    );

    return responsive ? isCommit ? <Col {...wrapperCol}>{reasonCheckup}</Col> : <Col {...wrapperColAllCol}>{reasonCheckup}</Col> : reasonCheckup;
  };

  //Ngay khoi phat
  interface DateOnsetProps {
    onChange
  }
  const DateOnset: FC<DateOnsetProps> = ({ onChange }) => {
    const dateFormat = 'DD/MM/YYYY';


    const dateOnset = (
      <Row className="">
        <Col {...labelColTowItem} className="fs-12">
          Ngày khởi phát triệu chứng:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={['healthDeclaration', 'dateOnset']}>
            <DatePicker
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              placeholder="Chọn ngày khởi phát"
              format={dateFormat}
              onChange={(value, dateString) => {
                onChange(dateString)
              }}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{dateOnset}</Col> : dateOnset;
  };

  interface InvoiceProps {
    setIsInvoice;
    onChange
  }

  const Invoice: FC<InvoiceProps> = ({ setIsInvoice, onChange }) => {
    const invoice = (
      <div className="flex">
        <div className="text-invoice">Bạn có muốn xuất hóa đơn ?</div>
        <Form.Item name={['healthDeclaration', 'invoice']} className="">
          <CheckboxGroup
            name="healthDeclaration.invoice"
            onChange={value => {
              if (value && value.length > 0) {
                setIsInvoice(true);
                onChange("1")
              } else {
                setIsInvoice(false);
                onChange("")
              }
            }}
          >
            <Checkbox value="1" indeterminate={false}>
              <span>Có</span>
            </Checkbox>
          </CheckboxGroup>
        </Form.Item>
      </div>
    );
    return invoice;
  };

  const PrintName: FC = ({ }) => {
    const printName = (
      <Form.Item name={['healthDeclaration', 'printName']} className="">
        <CheckboxGroup onChange={value => {
          console.log(value);
        }} name="healthDeclaration.printName">
          <Checkbox indeterminate={false} value="1">
            <span className="red">Ẩn tên khách hàng</span>
          </Checkbox>
        </CheckboxGroup>
      </Form.Item>
    );
    return printName;
  };

  //
  interface AddressProfileProps {
    required: boolean;
    onChange
  }
  const AddressProfile: FC<AddressProfileProps> = ({ required, onChange }) => {
    const addressProfile = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Địa chỉ:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'addressProfile']}
            rules={[{ required: required, message: 'Địa chỉ không được để trống.' }]}
          >
            <Input onChange={value => {
              onChange(value?.target?.value)
            }} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{addressProfile}</Col> : addressProfile;
  };

  //
  interface AddressCompanyProps {
    required: boolean;
    onChange
  }
  const AddressCompany: FC<AddressCompanyProps> = ({ required, onChange }) => {
    const addressCompany = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Địa chỉ công ty:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'addressCompany']}
            rules={[{ required: required, message: 'Địa chỉ công ty không được để trống.' }]}
          >
            <Input onChange={value => {
              onChange(value?.target?.value)
            }} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{addressCompany}</Col> : addressCompany;
  };

  //
  interface EmailProfileProps {
    required: boolean;
    onChange
  }
  const EmailProfile: FC<EmailProfileProps> = ({ required, onChange }) => {
    const emailProfile = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Email:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'emailProfile']}
            rules={[
              {
                required: required,
                message: 'Email không được để trống và phải đúng định dạng',
                pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
              }
            ]}
          >
            <Input onChange={value => {
              onChange(value?.target.value);
            }} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{emailProfile}</Col> : emailProfile;
  };

  //
  interface EmailCompanyProps {
    required: boolean;
    onChange
  }
  const EmailCompany: FC<EmailCompanyProps> = ({ required, onChange }) => {
    const emailCompany = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Email:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'emailCompany']}
            rules={[
              {
                required: required,
                message: 'Email không được để trống.',
                pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
              }
            ]}
          >
            <Input onChange={value => {
              onChange(value?.target.value);
            }} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{emailCompany}</Col> : emailCompany;
  };

  //
  interface TaxCodeProps {
    required: boolean;
    onChange
  }
  const TaxCode: FC<TaxCodeProps> = ({ required, onChange }) => {
    const taxCode = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Mã số thuế:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'taxCode']}
            rules={[{ required: required, message: 'Mã số thuế không được để trống.' }]}
          >
            <Input onChange={value => {
              onChange(value?.target.value);
            }} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{taxCode}</Col> : taxCode;
  };

  //
  interface NameCompanyProps {
    required: boolean;
    onChange
  }
  const NameCompany: FC<NameCompanyProps> = ({ required, onChange }) => {
    const nameCompany = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Tên công ty:
        </Col> v
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'nameCompany']}
            rules={[{ required: required, message: 'Tên công ty không được để trống.' }]}
          >
            <Input onChange={value => {
              onChange(value?.target.value);
            }} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{nameCompany}</Col> : nameCompany;
  };

  //NationPassOther
  interface NationPassOtherProps { }
  const NationPassOther: FC<NationPassOtherProps> = ({ }) => {
    const nationPassOther = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 3. Tiền sử đi/đến/ở/về từ nước ngoài/ địa phương có dịch bệnh Covid 19 trong
          thời gian 14 ngày trở lại đây?
        </div>
        <Form.Item
          name={['healthDeclaration', 'nationPassOther']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group name="healthDeclaration.nationPassOther">
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return nationPassOther;
  };

  //HumanHasCovid
  interface HumanHasCovidProps {
    onChange
  }
  const HumanHasCovid: FC<HumanHasCovidProps> = ({ onChange }) => {
    const humanHasCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 1. Có tiền sử tiếp xúc gần với người nhiễm COVID- 19 trong thời gian 2 ngày trước kể từ ngày F0 được lấy mẫu xác định hoặc tính từ ngày F0 khởi phát triệu chứng? (đối tượng F1 tiếp xúc gần F0)?
        </div>
        <Form.Item
          name={['healthDeclaration', 'humanHasCovid']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group onChange={value => {
            onChange(value?.target?.value);
          }} name="healthDeclaration.humanHasCovid">
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return humanHasCovid;
  };

  //TestedCovid
  interface TestedCovidProps {
    onChange: any
  }
  const TestedCovid: FC<TestedCovidProps> = ({ onChange }) => {
    const testedCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 3. Đã làm xét nghiệm SARS CoV2 trước đó có kết quả xét nghiệm test nhanh Ag dương tính/ hoặc PCR dương tính/nghi ngờ dương tính?
        </div>
        <Form.Item
          name={['healthDeclaration', 'testedCovid']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group onChange={value => {
            onChange(value?.target?.value);
          }} name="healthDeclaration.testedCovid">
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return testedCovid;
  };

  //prognostic
  interface PrognosticProps {
    onChange
  }
  const Prognostic: FC<PrognosticProps> = ({ onChange }) => {
    const prognostic = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span><b> Mục A. Trong 14 ngày qua, có ít nhất 1 trong các triệu chứng dưới đây</b>
        </div>
        <Form.Item
          name={['healthDeclaration', 'prognostic']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <CheckboxGroup
            name="healthDeclaration.prognostic"
            onChange={value => {
              if (value && value[value.length - 1] == '1') {
                formInstance.setFieldsValue({ healthDeclaration: { prognostic: ['1'] } });
                onChange(['1'])
              } else if (value && value[value.length - 1] != '1') {
                if (value.indexOf('1') != -1) value.splice(value.indexOf('1'), 1);
                formInstance.setFieldsValue({ healthDeclaration: { prognostic: value } });
                onChange(value)
              } else {
                formInstance.setFieldsValue({ healthDeclaration: { prognostic: value } });
                onChange(value)
              }
            }}
          >
            <div className="checkbox-kbyt">
              <Checkbox value="16">Ho - đau họng</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="2">Sốt</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="4">Khó thở - tức ngực</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="8">Các biểu hiện: Đau mỏi người. gai rét. đau họng</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="32">Giảm hoặc mất vị giác hoặc khứu giác</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="1">Không có triệu chứng</Checkbox>
            </div>
          </CheckboxGroup>
        </Form.Item>
      </div>
    );
    return prognostic;
  };

  //StreetHasCovid
  interface StreetHasCovidProps { onChange }
  const StreetHasCovid: FC<StreetHasCovidProps> = ({ onChange }) => {
    const streetHasCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 2. Di chuyển trên cùng phương tiện giao thông hoặc cùng địa điểm, sự kiện, nơi làm việc, lớp học… với ca bệnh xác định (F0) trong thời gian 2 ngày trước kể từ ngày F0 được lấy mẫu xác định hoặc tính từ ngày F0 khởi phát triệu chứng.?
        </div>
        <Form.Item
          name={['healthDeclaration', 'streetHasCovid']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group onChange={value => {
            onChange(value?.target.value);
          }} name="healthDeclaration.streetHasCovid">
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return streetHasCovid;
  };

  //Đã tiêm đủ 2 mũi vắc xin
  interface TowInjectionsProps {
    onChange
  }
  const TowInjections: FC<TowInjectionsProps> = ({ onChange }) => {
    const towInjections = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> b. Đã tiêm đủ 2 mũi vắc xin chưa?
        </div>
        <Form.Item
          name={['healthDeclaration', 'hasVaccine']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group onChange={value => {
            onChange(value?.target?.value);
          }} name="healthDeclaration.hasVaccine">
            <Row>
              <Col span={6}>
                <Radio value="0">Chưa tiêm</Radio>
              </Col>
              <Col span={6}>
                <Radio value="1">1 mũi</Radio>
              </Col>
              <Col span={6}>
                <Radio value="2">2 mũi</Radio>
              </Col>
              <Col span={6}>
                <Radio value="3"> &gt; 2 mũi</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return towInjections;
  };

  //Đã từng nhiễm covid
  interface IsCovidProps {
    isCovided: boolean;
    setIsCovided: any;
    onChange
  }
  const IsCovid: FC<IsCovidProps> = ({ isCovided, setIsCovided, onChange }) => {
    const isCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> a. Đã từng mắc  COVID – 19 trong vòng 6 tháng gần đây không?
        </div>
        <Form.Item name={["healthDeclaration", "hasCovid"]}
          rules={[
            { required: true, message: 'Không được bỏ trống!' },
          ]}
        >
          <Radio.Group
            onChange={(value) => {
              var itemValue = value.target.value;
              onChange(value.target.value)
              if (itemValue == 1 || itemValue == 3) {
                setIsCovided(true);
              } else {
                setIsCovided(false);
              }
            }}
            name={".healthDeclaration.hasCovid"}>
            <Row>
              <Col span={6}>
                <Radio value="1">Đã từng</Radio>
              </Col>
              <Col span={6}>
                {
                  isCovided && <Form.Item
                    name={["healthDeclaration", "dateHascovid"]}
                    label="Thời gian nhiễm"
                    rules={[{ required: isCovided, message: 'Bắt buộc phải nhập' }]}
                  >
                    <DatePicker
                      format="DD/MM/YYYY"
                      placeholder="-- Chọn ngày --"
                    />
                  </Form.Item>
                }

              </Col>
              <Col span={6}>
                <Radio value="2">Chưa từng</Radio>
              </Col>
              <Col span={6}>
                <Radio value="3">Đang điều trị</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return isCovid;
  };

  //Đối tượng
  interface TypeCustomerProps {
    // appointmentObjects: any;
    // handleChange: (value) => void;
    onChange
  }
  const TypeCustomer: FC<TypeCustomerProps> = ({ onChange }) => {
    const typeCustomer = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Đối tượng :
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'typeCustomerKBYT']}
            rules={[{ required: true, message: 'Đối tượng khám không được để trống.' }]}
          >
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Đối tượng --"
              onChange={value => { onChange(value) }}
              defaultValue="6"
            >
              <Select.Option value="">-- Chọn --</Select.Option>
              <Select.Option value="6">Xét nghiệm COVID-19</Select.Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{typeCustomer}</Col> : typeCustomer;
  };

  //Đơn vị
  interface FacilityProps {
    facilityList: Facility[];
    isLoading: boolean;
  }
  const Facility: FC<FacilityProps> = ({ facilityList, isLoading }) => {
    const facility = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span>Đơn vị lấy mẫu:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="examFacilityId"
            rules={[
              {
                required: true,
                message: 'Nơi khám không được để trống.'
              }
            ]}
          >
            <Select
              loading={isLoading}
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Nơi khám --"
              disabled
            >
              {facilityList?.map(option => (
                <Select.Option key={option.id} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{facility}</Col> : facility;
  };

  return React.useMemo(
    () => ({
      form: formInstance,
      Form: WrappedForm,
      PhoneNumber,
      CustomerFullname,
      Gender,
      Nationality,
      ContactAddress,
      Provinces,
      Districts,
      IdentityCard,
      AppointmentDate,
      AppointmentWorkTime,
      DateOnset,
      Prognostic,
      StreetHasCovid,
      NationPassOther,
      TestedCovid,
      HumanHasCovid,
      Invoice,
      AddressProfile,
      EmailProfile,
      NameCompany,
      TaxCode,
      AddressCompany,
      EmailCompany,
      PrintName,
      IsCovid,
      TowInjections,
      GuardianPhone,
      GuardianName,
      IssuedPlace,
      IssuedDate,
      ReasonCheckup,
      StaffCode,
      PackageType,
      PackageId,
      DateofBirth,
      TypeCustomer,
      Facility,
      GuardianDateOfBirth,
      GuardianRelationship,
      Reson
    }),
    [patternIdNo]
  );
}
