import { BorderOutlined, CloseSquareOutlined } from '@ant-design/icons';
import { AppoimentCovidConfrim } from 'common/interfaces/appoimentCovidConfrim.interface';
import moment from 'moment';

import React, { FC, useEffect, useState } from 'react';
import Logo from '../../../assets/images/logo2.png';

interface KBYTVNGenPDFProps {
  dataEdit: AppoimentCovidConfrim;
  provinces: any[];
  districs: any[];
  print: boolean;
  display;
  // nationaly:any[];
}

const KBYTVNGenPDF: FC<KBYTVNGenPDFProps> = ({ provinces, districs, dataEdit, print, display }) => {
  const getProvice = id => {
    if (id && provinces?.length > 0) {
      for (var i = 0; i < provinces?.length; i++) {
        if (provinces[i]?.id == id) {
          return provinces[i]?.name;
        }
      }
      return '......';
    }
  };

  const getDistric = id => {
    if (id && districs?.length > 0) {
      for (var i = 0; i < districs?.length; i++) {
        if (districs[i]?.id == id) {
          return districs[i]?.name;
        }
      }
      return '......';
    }
  };

  console.log(dataEdit);

  return (
    <div
      id="KBYT"
      style={
        print
          ? {
              width: '794px',
              height: '1123px',
              fontFamily: '"Times New Roman", Times, serif'
            }
          : {
              // width: "794px",
              // height: "1123px",
              fontFamily: '"Times New Roman", Times, serif'
            }
      }
    >
      <div className={display ? '' : 'p-5'}>
        <div
          style={{
            width: '100%',
            padding: '10px 20px'
          }}
        >
          {dataEdit?.appointmentHisId && (
            <div
              style={{
                width: '100%',
                textAlign: 'right'
              }}
            >
              Mã lịch hẹn : {dataEdit?.appointmentHisId}
            </div>
          )}
          <div
            style={{
              width: '100%',
              display: 'flex'
            }}
          >
            <div
              style={{
                width: '60%'
              }}
            >
              <div style={{ display: 'flex' }}>
                <div
                  style={{
                    textAlign: 'center',
                    marginLeft: 30
                  }}
                >
                  <div
                    style={{
                      fontWeight: 'bold'
                    }}
                  >
                    CÔNG TY TNHH CÔNG NGHỆ
                    <br /> VÀ XÉT NGHIỆM Y HỌC
                  </div>
                </div>
              </div>
            </div>
            <div
              style={{
                width: '60%'
              }}
            >
              <div
                style={{
                  float: 'right'
                }}
              >
                <div
                  style={{
                    padding: '1px 5px',
                    textAlign: 'center'
                  }}
                >
                  <div>
                    <strong>
                      CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM
                      <br /> Độc lập - Tự do - Hạnh phúc
                    </strong>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <p
                style={{
                  textAlign: 'center'
                }}
              >
                <strong>PHIẾU KHAI BÁO Y TẾ VÀ YÊU CẦU XÉT NGHIỆM SARS-CoV2</strong>
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <p
                style={{
                  textAlign: 'left',
                  marginBottom: '5px !important'
                }}
              >
                <strong>1. 1. Thông tin dành cho khách hàng</strong>
                <br />
                <div style={{ paddingLeft: '10px', paddingBottom: '10px' }}>
                  1.1. Họ và tên bệnh nhân:&nbsp;
                  {dataEdit?.patientName || '……………………………………………………………………………………… '}
                  <br />
                  1.2. Ngày sinh:&nbsp;
                  {dataEdit?.patientBirthDate ? dataEdit?.patientBirthDate : '……… / ……… / ………… '}
                  &nbsp;&nbsp; {display ? <br /> : ''} 1.3. Giới tính:&nbsp;
                  {dataEdit?.patientSex == 'MALE' ? <CloseSquareOutlined /> : <BorderOutlined />}
                  &nbsp; Nam &nbsp;
                  {dataEdit?.patientSex == 'FEMALE' ? <CloseSquareOutlined /> : <BorderOutlined />}
                  &nbsp; Nữ <br />
                  1.4. Số điện thoại: &nbsp;{dataEdit?.phone || '………………………… '} &nbsp;&nbsp;
                  {display ? <br /> : ''} 1.5. Quốc gia/ Dân tộc: &nbsp;
                  {dataEdit?.countryName || '…………………………… '} <br />
                  1.6. Số CMTND/ Số hộ chiếu:&nbsp;{dataEdit?.patientIdNo || '……………………… '} Ngày cấp: &nbsp;
                  {dataEdit?.issuedDate ? dataEdit?.issuedDate : '……………………… '} &nbsp; Nơi cấp:&nbsp;
                  {dataEdit?.issuedPlace || '……………………… '} <br />
                  1.7. Địa chỉ bệnh nhân: &nbsp;
                  {dataEdit?.address +
                    ', ' +
                    getDistric(dataEdit?.districtId) +
                    ', ' +
                    getProvice(dataEdit?.provinceId) || '……………………… '}
                  <br />
                  1.8. Họ tên người giám hộ1 (bố mẹ/người thân, nếu có): &nbsp;
                  {dataEdit?.guardianName || '……………………… '}&nbsp;&nbsp; Điện thoại: &nbsp;
                  {dataEdit?.guardianPhone || '……………………… '} <br />
                  1.9. Lý do xét nghiệm: <br />
                  <div style={{ float: 'left', padding: '0px 10px' }}>
                    &nbsp;&nbsp;
                    {dataEdit?.reasonNote == 'SARS-COV-2-PCR' ? <CloseSquareOutlined /> : <BorderOutlined />}
                    &nbsp; Có dịch tễ nghi ngờ; <br /> &nbsp;&nbsp;
                    {dataEdit?.reasonNote == 'SARS-COV-2-PCR' ? <CloseSquareOutlined /> : <BorderOutlined />}
                    &nbsp; Có triệu chứng nghi ngờ; <br />
                    &nbsp;&nbsp;
                    {dataEdit?.reasonNote == 'SARS-COV-2-PCR-FA' ? <CloseSquareOutlined /> : <BorderOutlined />}
                    &nbsp; Có KQXN nghi ngờ nhiễm COVID-19;
                  </div>
                  <div style={{ float: 'left', padding: '0px 10px' }}>
                    &nbsp;&nbsp;
                    {dataEdit?.reasonNote == 'SARS-COV-2-XC' ? <CloseSquareOutlined /> : <BorderOutlined />}
                    &nbsp; Nhập cảnh/ Xuất cảnh; <br /> &nbsp;&nbsp;
                    {dataEdit?.reasonNote == 'Khám bệnh' ? <CloseSquareOutlined /> : <BorderOutlined />}
                    &nbsp; Khám chữa bệnh tại bệnh viện chuyên khoa; <br />
                    &nbsp;&nbsp;
                    {dataEdit?.reasonNote == 'Khác' ? <CloseSquareOutlined /> : <BorderOutlined />}&nbsp; Khác:
                    {(dataEdit?.reasonNote == 'Khác' && dataEdit?.appointmentNote) || '……………………… '}
                  </div>
                </div>
              </p>
              <br />
            </div>
          </div>
          <br />
          <div className="row">
            <div className="col-md-12">
              <div
                style={{
                  textAlign: 'left',
                  marginBottom: '5px !important'
                }}
              >
                <strong>1.10. Khai báo Y tế: Hãy tích dấu “x” vào ô trống trước phần lựa chọn</strong>
              </div>
              <table
                style={{
                  // border: "1px solid black",
                  width: '100%',
                  paddingLeft: '5px',
                  borderCollapse: 'separate',
                  borderSpacing: 0
                }}
              >
                <tr
                  style={{
                    // border: "1px solid black",
                    width: '100%'
                  }}
                >
                  <th
                    style={{
                      border: '1px solid black',
                      width: '70%',
                      textAlign: 'center'
                    }}
                  >
                    Câu hỏi
                  </th>
                  <th
                    colSpan={2}
                    style={{
                      border: '1px solid black',
                      width: '30%',
                      textAlign: 'center'
                    }}
                  >
                    Trả lời
                  </th>
                </tr>
                <tbody>
                  <tr
                    style={{
                      border: '1px solid black',
                      width: '100%'
                    }}
                  >
                    <td
                      style={{
                        padding: '1px 5px',
                        border: '1px solid black'
                      }}
                    >
                      <strong></strong>
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                      colSpan={2}
                    ></td>
                  </tr>
                  <tr>
                    <td
                      style={{
                        border: '1px solid black',
                        padding: '1px 5px'
                      }}
                    >
                      Ho, đau họng
                    </td>
                    <td
                      style={{
                        border: '1px solid black',
                        width: '15%'
                      }}
                    >
                      &nbsp;&nbsp; Có&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic &&
                      dataEdit?.healthDeclaration?.prognostic?.includes('16') ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Không&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic &&
                      !dataEdit?.healthDeclaration?.prognostic?.includes('16') ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td
                      style={{
                        border: '1px solid black',
                        padding: '1px 5px'
                      }}
                    >
                      Sốt
                    </td>
                    <td
                      style={{
                        border: '1px solid black',
                        width: '15%'
                      }}
                    >
                      &nbsp;&nbsp; Có&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic &&
                      dataEdit?.healthDeclaration?.prognostic?.toString()?.includes('2') ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Không&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic &&
                      !dataEdit?.healthDeclaration?.prognostic?.toString()?.includes('2') ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td
                      style={{
                        padding: '1px 5px',
                        border: '1px solid black'
                      }}
                    >
                      Tức ngực, khó thở
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Có&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic &&
                      dataEdit?.healthDeclaration?.prognostic?.toString()?.includes('4') ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Không&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic &&
                      !dataEdit?.healthDeclaration?.prognostic?.toString()?.includes('4') ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                  </tr>
                  <tr
                    style={
                      {
                        // border: "1px solid black"
                      }
                    }
                  >
                    <td
                      style={{
                        border: '1px solid black',
                        padding: '1px 5px'
                      }}
                    >
                      Các biểu hiện: Đau mỏi người, đau đầu, gai rét, sổ mũi,...
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Có&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic &&
                      dataEdit?.healthDeclaration?.prognostic?.toString()?.includes('8') ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Không&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic &&
                      !dataEdit?.healthDeclaration?.prognostic?.toString()?.includes('8') ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                  </tr>
                  <tr
                    style={
                      {
                        // border: "1px solid black"
                      }
                    }
                  >
                    <td
                      style={{
                        border: '1px solid black',
                        padding: '1px 5px'
                      }}
                    >
                      Giảm hoặc mất vị giác hoặc khứu giác
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Có&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic &&
                      dataEdit?.healthDeclaration?.prognostic?.toString()?.includes('32') ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Không&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.prognostic &&
                      !dataEdit?.healthDeclaration?.prognostic?.toString()?.includes('32') ? (
                        <CloseSquareOutlined />
                      ) : (
                        <BorderOutlined />
                      )}
                    </td>
                  </tr>
                  <tr
                    style={
                      {
                        // border: "1px solid black",
                      }
                    }
                  >
                    <td
                      style={{
                        border: '1px solid black',
                        padding: '1px 5px'
                      }}
                    >
                      Ngày bắt đầu triệu chứng(nếu có)
                    </td>
                    <td
                      style={{
                        border: '1px solid black',
                        padding: '1px 5px'
                      }}
                      colSpan={2}
                    >
                      {dataEdit?.healthDeclaration?.dateOnset && dataEdit?.healthDeclaration?.dateOnset != null
                        ? dataEdit?.healthDeclaration?.dateOnset
                        : '…………………'}
                    </td>
                  </tr>
                  <tr>
                    <td
                      style={{
                        border: '1px solid black',
                        padding: '1px 5px'
                      }}
                      colSpan={3}
                    >
                      <strong>Mục B. Yếu tố dịch tễ nếu có</strong>
                    </td>
                  </tr>
                  <tr
                    style={
                      {
                        // border: "1px solid black"
                      }
                    }
                  >
                    <td
                      style={{
                        padding: '1px 5px',
                        border: '1px solid black'
                      }}
                    >
                      Có tiền sử tiếp xúc gần với người nhiễm COVID- 19 trong thời gian 2 ngày trước kể từ ngày F0 được
                      lấy mẫu xác định hoặc tính từ ngày F0 khởi phát triệu chứng? (đối tượng F1 tiếp xúc gần F0)
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Có&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.humanHasCovid == 1 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Không&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.humanHasCovid == 0 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr
                    style={
                      {
                        // border: "1px solid black"
                      }
                    }
                  >
                    <td
                      style={{
                        padding: '1px 5px',
                        border: '1px solid black'
                      }}
                    >
                      Di chuyển trên cùng phương tiện giao thông hoặc cùng địa điểm, sự kiện, nơi làm việc, lớp học… với
                      ca bệnh xác định (F0) trong thời gian 2 ngày trước kể từ ngày F0 được lấy mẫu xác định hoặc tính
                      từ ngày F0 khởi phát triệu chứng.
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Có&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.streetHasCovid == 1 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Không&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.streetHasCovid == 0 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                  <tr
                    style={{
                      border: '1px solid black'
                    }}
                  >
                    <td
                      style={{
                        padding: '1px 5px',
                        border: '1px solid black'
                      }}
                    >
                      Đã làm xét nghiệm SARS CoV2 trước đó có kết quả xét nghiệm test nhanh Ag dương tính/ hoặc PCR
                      dương tính/nghi ngờ dương tính? (Ngày xét nghiệm:……………………)
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Có&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.testedCovid == 1 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                    <td
                      style={{
                        border: '1px solid black'
                      }}
                    >
                      &nbsp;&nbsp; Không&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.testedCovid == 0 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="col-md-12">
              <div
                className="row"
                style={{
                  textAlign: 'left'
                }}
              >
                <p
                  style={{
                    marginBottom: 0
                  }}
                >
                  <strong
                    style={{
                      textAlign: 'left'
                      // marginBottom: "5px !important"
                    }}
                  >
                    1.11. CÁC THÔNG TIN KHÁC: Hãy tích dấu “X” vào ô trống trước phần lựa chọn
                  </strong>
                </p>
                <div className="row">
                  <div
                    style={{
                      textAlign: 'left',
                      width: '100%'
                    }}
                  >
                    1. Tiền sử nhiễm COVID – 19 trong vòng 6 tháng gần đây không?
                  </div>
                  {/* <div
                    style={{
                      display: "flex",
                      width: "100%"
                    }}
                  > */}
                  <div
                    style={{
                      display: 'flex'
                    }}
                  >
                    <div
                      style={{
                        textAlign: 'left',
                        paddingLeft: '10px',
                        width: '33.3%'
                      }}
                    >
                      A. Đã từng&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.hasCovid == 1 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </div>
                    <div
                      style={{
                        textAlign: 'left',
                        paddingLeft: '10px',
                        width: '33.3%'
                      }}
                    >
                      B.Chưa từng&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.hasCovid == 2 ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </div>
                    <div
                      style={{
                        textAlign: 'left',
                        paddingLeft: '10px',
                        width: '33.3%'
                      }}
                    >
                      C.Đang điều trị&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.hasCovid == '3' ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </div>
                  </div>
                </div>

                {/* </div> */}
              </div>
              <div
                style={{
                  textAlign: 'left'
                }}
              >
                (Ghi rõ ngày/tháng nhiễm:&nbsp;&nbsp;
                {dataEdit?.healthDeclaration?.dateHascovid ? dataEdit?.healthDeclaration?.dateHascovid : '…….……………..'}
                )<br />
                <div className="row">
                  <div
                    style={{
                      textAlign: 'left',
                      width: '100%',
                      paddingLeft: '10px'
                    }}
                  >
                    2. Đã tiêm vắc xin phòng ngừa COVID-19 chưa?
                  </div>
                  <div
                    style={{
                      display: 'flex',
                      width: '100%'
                    }}
                  >
                    <div
                      className="col-md-3"
                      style={{
                        textAlign: 'left',
                        paddingLeft: '10px',
                        width: '25%'
                      }}
                    >
                      A.&nbsp;&nbsp; Chưa tiêm&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.hasVaccine == '0' ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </div>
                    <div
                      className="col-md-3"
                      style={{
                        textAlign: 'left',
                        paddingLeft: '10px',
                        width: '25%'
                      }}
                    >
                      B.&nbsp;&nbsp; 1 mũi&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.hasVaccine == '1' ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </div>
                    <div
                      className="col-md-3"
                      style={{
                        textAlign: 'left',
                        paddingLeft: '10px',
                        width: '25%'
                      }}
                    >
                      C.&nbsp;&nbsp; 2 mũi&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.hasVaccine == '2' ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </div>
                    <div
                      className="col-md-3"
                      style={{
                        textAlign: 'left',
                        paddingLeft: '10px',
                        width: '25%'
                      }}
                    >
                      D.&nbsp;&nbsp; &gt; 2 mũi&nbsp;&nbsp;
                      {dataEdit?.healthDeclaration?.hasVaccine == '3' ? <CloseSquareOutlined /> : <BorderOutlined />}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <p
              style={{
                marginBottom: 0
              }}
            >
              <strong>2. Đề nghị xét nghiệm</strong>
              <br />
              &nbsp;&nbsp; Nhằm mục đích sàng lọc, kiểm soát dịch COVID-19, tôi đề nghị đơn vị tiếp nhận yêu cầu và lấy
              mẫu, thực hiện xét nghiệm SARS-CoV2 cho tôi.
              <br />
              <strong>
                &nbsp;&nbsp; Tôi xin cam đoan những lời khai trên hoàn toàn đúng sự thật, nếu vi phạm tôi xin chịu trách
                nhiệm trước pháp luật. Đồng thời cam kết thực hiện các biện pháp phòng chống dịch theo quy định.
              </strong>
              <br />
            </p>
          </div>
          <div className="row">
            <div
              style={{
                width: '100%',
                display: 'flex'
              }}
            >
              <div
                style={{
                  width: '60%',
                  textAlign: 'center'
                }}
              >
                <p
                  style={{
                    marginBottom: 0
                  }}
                >
                  <strong>Người khai báo</strong>
                  <br />
                  <em>{(dataEdit?.patientLabel && 'Đã xác nhận') || '(Ký và ghi rõ họ tên)'}</em>
                </p>
                <br />
                <p>{(dataEdit?.patientLabel && dataEdit?.patientName) || ''}</p>
                <br />
              </div>
              <div
                style={{
                  width: '60%',
                  textAlign: 'center'
                }}
              >
                <p
                  style={{
                    marginBottom: 0
                  }}
                >
                  <em>……..giờ……. phút, Ngày….Tháng….Năm ......</em>
                  <br />
                  <strong>Cán bộ khai thác thông tin</strong>
                  <br />
                  <em>(Ký và ghi rõ họ tên)</em>
                </p>
                <br />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default KBYTVNGenPDF;
