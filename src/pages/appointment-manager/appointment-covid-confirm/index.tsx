import { Col, DatePicker, Form, Row } from "antd";
import { useAppState } from "helpers";
import moment from "moment";
import React, { useEffect, useState } from "react"
import { FC } from "react"
import AppointmentCovidConfirmTable from "./AppointmentCovidConfirmTable";
import CovidConfirmForm from "./CovidConfirmForm";
import CovideAssignItem from "./CovideAssignItem";
import './index.less';

const ColLeft = {
    xs: 24,
    sm: 24,
    md: 24,
    lg: 6,
    xl: 6,
    xxl: 6
}

const ColRight = {
    xs: 24,
    sm: 24,
    md: 24,
    lg: 18,
    xl: 18,
    xxl: 18
}

interface AppointmentCovidConfirmPageProps {
}

const AppointmentCovidConfirmPage: FC<AppointmentCovidConfirmPageProps> = () => {
    const functionOb = useAppState(state => state.user.functionObject);
    const [isDetail, setIsDetail] = useState(false);
    const [itemE, setItemE]: any = useState();
    const [appoimentDate, setAppoimentDate] = useState(moment(new Date()));

    useEffect(() => {
        // checkMedia();
    })

    function checkMedia() {
        (navigator as any).getUserMedia = (navigator as any).getUserMedia || (navigator as any).webkitGetUserMedia || (navigator as any).mozGetUserMedia;
        if ((navigator as any).getUserMedia) {
            (navigator as any).getUserMedia({ video: true },
                function (stream) {
                    console.log("Accessed the video");
                },
                function (err) {
                    console.log("Error: Cannot access the video");
                });
        } else {
            console.log("Error: Cannot access the video");
        }

    }

    return (
        <div className="main-content" style={{
            height: "auto !important"
        }}>
            <Row>
                <Col className="covid-form" span={6}>Ngày khám</Col>
                <Col span={12}>
                    <Form className="covid-form">
                        <Form.Item>
                            <DatePicker
                                value={appoimentDate}
                                format={"DD/MM/YYYY"}
                                onChange={(value, dateString) => { setAppoimentDate(moment(value)); }}
                            />
                        </Form.Item>
                    </Form>
                </Col>
                <Col span={6}>
                    <img  style={{ width: "50px" }} src="/qr-image.png" alt="Quét QR" />
                </Col>
            </Row>
            <CovidConfirmForm setIsDetail={setIsDetail} isDetail={isDetail} itemE={itemE} />
            <AppointmentCovidConfirmTable
                valueSearch={null}
                functionOb={functionOb}
                setItemE={setItemE}
                setIsDetail={setIsDetail}
                appoimentDate={appoimentDate}
            />
        </div>
    )
}

export default AppointmentCovidConfirmPage;