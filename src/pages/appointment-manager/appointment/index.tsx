import React, { FC, useEffect, useState } from 'react';
import AppointmentTable from './appointmentTable';
import AppointmentSearch from './appointmentSearch';
import './index.less';
import { AppointmentPatient } from 'common/interfaces/appointmentPatient.interface';
import { useDeleteAppointment } from 'hooks/appointment/useDeleteAppointment';
import {
  useCancleAppointment,
  useCreateAppointment,
  useCreateAppointmentAndForwardConsultant,
  useGetAppointments,
  useUpdateAppointment
} from 'hooks/appointment';
import moment from 'moment';
import { AppointmentInput } from 'common/interfaces/input/appointmentInput.interfce';
import { useGetAppointmentDetail } from 'hooks/appointment/useGetAppointmentDetail';
import AppointmentBookingMultipleDialog from './appointmentBookingMultipleDialog';
import { useSaveMultiAppointment } from 'hooks/appointment/useSaveMultiAppointment';
import { openNotificationRight } from 'utils/notification';
import { FormInstance, FormProps, Spin } from 'antd';
import { useAppState } from 'helpers';
import { useGetChannels } from 'hooks/channel';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { useForwardAppointment } from 'hooks/appointment/useForwardAppointmentToDoctorConsultant';
import { useCancleMultiAppointment } from 'hooks/appointment/useCancleMultiAppointment';
import { useDeleteMultiAppointment } from 'hooks/appointment/useDeleteMultiAppointment';
import AppointmentDialog from './appoimentDialog/AppoimentDialog';
import AppoimentDialogUpdate from './appoimentDialog/AppoimentDialogUpdate';
import { useNavigate } from 'react-router-dom';
import { useSearchParams } from 'react-router-dom';
import { apiForwardAppointmentCovid } from 'hooks/appointment/useForwardAppointmentCovid';
import { CODE_RESPONSE } from 'constants/response';

const AppointmentPage: FC = () => {
  const [searchParams] = useSearchParams();
  const [emptySearch, setEmptySearch] = useState(Number);
  const navigate = useNavigate();
  const [fromDateSearch, setFromDateSearch] = useState(moment().format('DD/MM/YYYY'));
  const [toDateSearch, setToDateSearch] = useState(moment().format('DD/MM/YYYY'));
  const [form, setForm] = useState({} as FormInstance<FormProps<any>>);
  const functionOb = useAppState(state => state.user.functionObject);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(50);
  const [isLoadingCreate, setIsLoadingCreate] = useState(false);
  const [filtered, setFiltered] = useState<FilteredInput[]>(
    searchParams.get('id')
      ? [
          {
            id: 'appointmentId',
            operation: '==',
            value: searchParams.get('id') ?? ''
          }
        ]
      : [
          {
            id: 'appointmentDate',
            operation: 'between',
            value: moment().format('DD/MM/YYYY') + ',' + moment().format('DD/MM/YYYY')
          }
        ]
  );

  const [mcreateVisible, setCreateVisible] = useState(false);
  const [modifyVisible, setModifyVisible] = useState(false);
  const [assignStaffName, setAssignStaffName] = useState('');
  const [bookingMultipleVisible, setBookingMultipleVisible] = useState(false);
  const [appointmentPatientSelected, setAppointmentPatientSelected] = useState({} as AppointmentPatient);
  const [appointmentIdRes, setAppointmentIdRes] = useState('');
  const [values, setValues] = useState({} as AppointmentPatient);

  //lắng nghe Id trả về
  const onSetIdRes = async id => {
    setAppointmentIdRes(id);
  };
  const onSetorwardAppointmentIdRes = async id => {
    setAppointmentIdRes(id);
  };
  const onResetAppointmentIdRes = () => {
    setAppointmentIdRes('');
  };
  const onSetCreateVisible = (isShow: boolean) => {
    setCreateVisible(isShow);
  };
  const onSeModifyVisible = (isShow: boolean) => {
    setModifyVisible(isShow);
    //refetchAppointments();
  };

  //Xử lý thêm mới Lịch hẹn
  const {
    createAppointment,
    resultSaveAppointment,
    isLoadingSaveAppointment,
    errorSaveAppointment
  } = useCreateAppointment(onSetIdRes, onSetCreateVisible);
  //Xử lý thêm mới Lịch hẹn và chuyển tư vấn
  const {
    createAppointmentAndForwardConsultant,
    resultSaveAppointmentAndForwardConsultant,
    isLoadingSaveAppointmentAndForwardConsultant,
    errorSaveAppointmentAndForwardConsultant
  } = useCreateAppointmentAndForwardConsultant(onSetIdRes, onSetCreateVisible);
  //Xoá Lịch hẹn
  const {
    deleteAppointment,
    isLoadingDeleteAppointment,
    resultDeleteAppointment,
    errorDeleteAppointment
  } = useDeleteAppointment();
  const handleDeleteAppointment = async id => {
    await deleteAppointment({ id });
  };
  //Xoá nhiều nhiều Lịch hẹn
  const {
    deleteMultiAppointment,
    isLoadingDeleteMultiAppointment,
    resultDeleteMultiAppointment,
    errorDeleteMultiAppointment
  } = useDeleteMultiAppointment();
  const handleDeleteMultiAppointment = async ids => {
    await deleteMultiAppointment({ ids });
  };
  //Chuyển BS Tư vấn
  const {
    forwardAppointment,
    resultForwardAppointment,
    isLoadingForwardAppointment,
    errorForwardAppointment
  } = useForwardAppointment(onSetorwardAppointmentIdRes, onSeModifyVisible);
  const [resultForwardAppointmentState, setResultForwardAppointmentState] = useState();
  const handleForwardAppointment = async (appointmentId: string) => {
    await forwardAppointment({ id: appointmentId });
    //refetchAppointments?.();
  };
  //Hủy lịch hẹn
  const {
    cancleAppointment,
    isLoadingCancleAppointment,
    resultCancleAppointment,
    errorCancleAppointment
  } = useCancleAppointment(onSetIdRes, onSeModifyVisible);
  const handleCancleAppointment = async id => {
    await cancleAppointment({ id });
    refetchAppointments?.();
  };
  //Hủy nhiều lịch hẹn
  const {
    cancleMultiAppointment,
    isLoadingCancleMultiAppointment,
    resultCancleMultiAppointment,
    errorCancleMultiAppointment
  } = useCancleMultiAppointment(onSetIdRes, onSeModifyVisible);
  //Chuyển lịch hẹn
  const handleForwardAppointmentCovid = async id => {
    let result = await apiForwardAppointmentCovid(id);
    if (result?.code === CODE_RESPONSE.SUCCESS) {
      openNotificationRight(result?.message, 'success');
      navigate('/appointment-management/appointment-covid/update/' + result?.data?.groupId + '/' + result?.data?.id);
    } else {
      openNotificationRight(result?.message, 'error');
    }
  };
  const handleCancleMultiAppointment = async ids => {
    await cancleMultiAppointment({ ids });
    refetchAppointments?.();
  };
  const handleCreateAppointment = async variables => {
    await createAppointment(variables);
  };
  const handleCreateAppointmentAndForwardConsultant = async variables => {
    await createAppointmentAndForwardConsultant(variables);
  };
  //Xử lý Cập nhật Lịch hẹn
  const {
    updateAppointment,
    resultUpdateAppointment,
    isLoadingUpdateAppointment,
    errorUpdateAppointment
  } = useUpdateAppointment(onSetIdRes, onSeModifyVisible);
  //C1 Lấy chi tiết lịch hẹn từ table lưu vào state
  const [appointmentIdSelected, setAppointmentIdSelected] = useState({});
  const [appointmentPatient, setAppointmentPatient]: any = useState();
  //C1. Lấy chi tiết lịch hẹn
  const { loadAppointmentDetail, isLoadingAppointment, appointmentDetail } = useGetAppointmentDetail();
  //Lấy nguồn lịch hẹn
  const { channels, isLoadingChannels, loadChannels } = useGetChannels();
  const {
    saveMultiAppointment,
    resultSaveMultiAppointment,
    isLoadingSaveMultiAppointment,
    errorSaveMultiAppointment
  } = useSaveMultiAppointment(onSetIdRes);
  const { pagingAppointments, isLoadingAppointments, errorAppointments, refetchAppointments } = useGetAppointments({
    page,
    pageSize,
    filtered
  });

  /**Tìm kiếm Lịch hẹn**/
  //Lọc theo ngày

  //Thông tin tìm kiếm

  const [infoType, setInfoType] = useState('patientPhone');
  const [infoSearch, setInfoSearch] = useState('');
  const [dateType, setDateType] = useState('appointmentDate');
  const [appointmentTypeSearch, setAppointmentTypeSearch] = useState({
    id: 'appointmentType',
    value: '',
    operation: '=='
  });
  const [methodTypeSearch, setMethodTypeSearch] = useState({
    id: 'methodType',
    value: '',
    operation: '=='
  });
  const [statusSearch, setStatusSearch] = useState({
    id: 'status',
    value: '',
    operation: '=='
  });
  const [channelSearch, setChannelSearch] = useState({
    id: 'channelType',
    value: '',
    operation: '=='
  });
  const [examFacilitySearch, setExamFacilitySearch] = useState({
    id: 'examFacilityId',
    value: '',
    operation: '=='
  });
  useEffect(() => {
    if (localStorage.getItem('IS_SHOW_CREATE_APPOINTMENT_DIALOG')) {
      setCreateVisible(true);
    }
    loadChannels();
  }, []);
  return (
    <>
      <div className="main-content appointment" style={{ padding: '10px' }}>
        <Spin spinning={isLoadingCreate} tip="Đang tải...">
          <AppointmentSearch
            functionOb={functionOb}
            channels={channels}
            onSetFormInstance={(value: FormInstance<FormProps<any>>) => {
              setForm(value);
            }}
            onSearch={value => {
              if (value['dateType']) {
                setDateType(value['dateType']);
              }
              if (value['fromDate']) {
                setFromDateSearch(moment(value['fromDate']).format('DD/MM/YYYY'));
              }
              if (value['toDate']) {
                setToDateSearch(moment(value['toDate']).format('DD/MM/YYYY'));
              }

              if (value['infoType']) {
                setInfoType(value['infoType']);
              }
              if (infoType) {
                //debugger
                if (value['info']) {
                  if (infoType === 'patientPhone') {
                    if (value['info'].slice(0, 1) === '0') {
                      setInfoSearch(value['info'].slice(1));
                    } else {
                      setInfoSearch(value['info']);
                    }
                  } else {
                    setInfoSearch(value['info'].trim());
                  }
                } else {
                  setInfoSearch('');
                }
              }
              if (value['appointmentType']) {
                setAppointmentTypeSearch({ ...appointmentTypeSearch, value: value['appointmentType'] });
              } else {
                setAppointmentTypeSearch({ ...appointmentTypeSearch, value: '' });
              }
              if (value['methodType']) {
                setMethodTypeSearch({ ...methodTypeSearch, value: value['methodType'] });
              } else {
                setMethodTypeSearch({ ...methodTypeSearch, value: '' });
              }
              if (value['status']) {
                setStatusSearch({ ...statusSearch, value: value['status'] });
              } else {
                setStatusSearch({ ...statusSearch, value: '' });
              }
              if (value['code']) {
                setChannelSearch({ ...channelSearch, value: value['code'] });
              } else {
                setChannelSearch({ ...channelSearch, value: '' });
              }
              if (value['examFacilityId']) {
                setExamFacilitySearch({ ...examFacilitySearch, value: value['examFacilityId'] });
              } else {
                setExamFacilitySearch({ ...examFacilitySearch, value: '' });
              }
            }}
          />
          <AppointmentTable
            onSearch={value => {
              setEmptySearch(Math.random());
              if (value['dateType']) {
                setDateType(value['dateType']);
              }
              if (value['fromDate']) {
                setFromDateSearch(moment(value['fromDate']).format('DD/MM/YYYY'));
              }
              if (value['toDate']) {
                setToDateSearch(moment(value['toDate']).format('DD/MM/YYYY'));
              }

              if (value['infoType']) {
                setInfoType(value['infoType']);
              }
              if (infoType) {
                //debugger
                if (value['info']) {
                  if (infoType === 'patientPhone') {
                    if (value['info'].slice(0, 1) === '0') {
                      setInfoSearch(value['info'].slice(1));
                    } else {
                      setInfoSearch(value['info']);
                    }
                  } else {
                    setInfoSearch(value['info'].trim());
                  }
                } else {
                  setInfoSearch('');
                }
              }
              if (value['appointmentType']) {
                setAppointmentTypeSearch({ ...appointmentTypeSearch, value: value['appointmentType'] });
              } else {
                setAppointmentTypeSearch({ ...appointmentTypeSearch, value: '' });
              }
              if (value['methodType']) {
                setMethodTypeSearch({ ...methodTypeSearch, value: value['methodType'] });
              } else {
                setMethodTypeSearch({ ...methodTypeSearch, value: '' });
              }
              if (value['status']) {
                setStatusSearch({ ...statusSearch, value: value['status'] });
              } else {
                setStatusSearch({ ...statusSearch, value: '' });
              }
              if (value['code']) {
                setChannelSearch({ ...channelSearch, value: value['code'] });
              } else {
                setChannelSearch({ ...channelSearch, value: '' });
              }
              if (value['examFacilityId']) {
                setExamFacilitySearch({ ...examFacilitySearch, value: value['examFacilityId'] });
              } else {
                setExamFacilitySearch({ ...examFacilitySearch, value: '' });
              }
            }}
            onCreate={() => {
              localStorage.setItem('IS_SHOW_CREATE_APPOINTMENT_DIALOG', 'true');
              setCreateVisible(true);
            }}
            onModify={(appointmentPatient: AppointmentPatient, refetchAppointments) => {
              //setRefetchAppointments(refetchAppointments);
              setAppointmentPatient(appointmentPatient);
              setAssignStaffName(appointmentPatient?.assignStaffName);
              loadAppointmentDetail({ variables: { id: appointmentPatient.appointmentId } });
              setAppointmentIdSelected(appointmentPatient.appointmentId);
              setModifyVisible(true);
            }}
            onBookingMultipleAppointments={appointmentPatient => {
              setBookingMultipleVisible(true);
              setAppointmentPatientSelected(appointmentPatient);
            }}
            onDelete={async id => {
              await handleDeleteAppointment(id);
            }}
            onCancle={async id => {
              await handleCancleAppointment(id);
            }}
            onDeleteMulti={async ids => {
              await handleDeleteMultiAppointment(ids);
            }}
            onCancleMulti={async ids => {
              await handleCancleMultiAppointment(ids);
            }}
            onAuthorize={values => {
              setValues(values);
              //setAuthorizeVisible(true);
            }}
            forwardAppointmentState={resultForwardAppointment}
            appointmentIdRes={appointmentIdRes}
            onResetAppointmentIdRes={onResetAppointmentIdRes}
            statusSearch={statusSearch}
            dateType={dateType}
            fromDateSearch={fromDateSearch}
            toDateSearch={toDateSearch}
            infoType={infoType}
            infoSearch={infoSearch}
            appointmentTypeSearch={appointmentTypeSearch}
            methodTypeSearch={methodTypeSearch}
            channelSearch={channelSearch}
            examFacilitySearch={examFacilitySearch}
            emptySearch={emptySearch}
            isSearch={true}
            formInstance={form}
            functionOb={functionOb}
            page={page}
            setPage={setPage}
            pageSize={pageSize}
            setPageSize={setPageSize}
            filtered={filtered}
            setFiltered={setFiltered}
            pagingAppointments={pagingAppointments}
            isLoadingAppointments={isLoadingAppointments}
            refetchAppointments={refetchAppointments}
            isLoadingAppointment={isLoadingAppointment}
          />
          {/* <AppointmentCreateDialog
        isLoadingChannels={isLoadingChannels}
        visible={mcreateVisible}
        onCreate={async values => {
          const appointmentInput: Partial<AppointmentInput> = {
            ...values
          };
          await handleCreateAppointment({ data: appointmentInput });
        }}
        onCreateAndForward={async values => {
          const appointmentInput: Partial<AppointmentInput> = {
            ...values
          };
          await handleCreateAppointmentAndForwardConsultant({ data: appointmentInput });
        }}
        onCancel={() => setCreateVisible(false)}
        functionOb={functionOb}
        channels={channels}
      /> */}
          {/* <AppointmentModifyDialog
        isLoadingChannels={isLoadingChannels}
        rowObj={appointmentPatient}
        functionOb={functionOb}
        onForwardAppointment={(appointmentId: any) => handleForwardAppointment(appointmentId)}
        onCancelAppointment={(appointmentId: any) => handleCancleAppointment(appointmentId)}
        channels={channels}
        visible={modifyVisible}
        assignStaffName={assignStaffName}
        onModify={values => {
          const appointmentInput: Partial<AppointmentInput> = {
            ...values
          };
          updateAppointment({ data: appointmentInput });
          //setModifyVisible(false);
        }}
        onBookingMultipleAppointments={appointmentPatient => {
          setAppointmentPatientSelected(appointmentPatient);
          setBookingMultipleVisible(true);
        }}
        appointment={appointmentDetail}
        onCancel={() => setModifyVisible(false)}
        isLoading={isLoadingAppointment}
      /> */}
          <AppointmentBookingMultipleDialog
            functionOb={functionOb}
            visible={bookingMultipleVisible}
            appointmentPatient={appointmentPatientSelected}
            onBookingMutipleAppointment={(values: AppointmentInput) => {
              let fromAppointmentDate: any = values['fromAppointmentDate'];
              fromAppointmentDate = moment(fromAppointmentDate).format('DD/MM/YYYY');
              let toAppointmentDate: any = values['toAppointmentDate'];
              toAppointmentDate = moment(toAppointmentDate).format('DD/MM/YYYY');
              /*if (moment(toAppointmentDate).isAfter(moment(fromAppointmentDate))) {
              openNotificationRight('Ngày kết thúc phải lớn ngày bắt đầu.');
              return;
            }*/
              /*let appointmentDate:any = values["appointmentDate"];
            appointmentDate = moment(appointmentDate).format('DD/MM/YYYY');
            delete values['__typename'];
            values['id'] = values['appointmentId'];
            delete values['appointmentId'];
            delete values['patientName'];
            delete values['patientAddress'];
            delete values['patientPhone'];
            delete values['workTimeName'];
            delete values['assignStaffName'];
            delete values['assignStaffDate'];
            delete values['channelName'];
            delete values['createDate'];
            delete values['statusName'];*/
              let id = values['appointmentId'];
              let amountDate = values['reExamDate'];
              let fromAppointmentDateSplit = String(fromAppointmentDate).split('/');
              let toAppointmentDateSplit = String(toAppointmentDate).split('/');
              let fromDateCaculate = moment([
                fromAppointmentDateSplit[2],
                fromAppointmentDateSplit[1],
                fromAppointmentDateSplit[0]
              ]);
              let toDateCaculate = moment([
                toAppointmentDateSplit[2],
                toAppointmentDateSplit[1],
                toAppointmentDateSplit[0]
              ]);
              if (toDateCaculate.diff(fromDateCaculate, 'days') < Number(amountDate)) {
                openNotificationRight('Cách ngày không hợp lệ, giá trị phải thoả mãn Từ ngày, Đến ngày');
                return;
              }
              saveMultiAppointment({ data: { id, fromAppointmentDate, toAppointmentDate, amountDate } });
              setBookingMultipleVisible(false);
            }}
            onCancel={() => setBookingMultipleVisible(false)}
          />
          <AppointmentDialog
            // isLoadingChannels={isLoadingChannels}
            visible={mcreateVisible}
            onCreate={async values => {
              const appointmentInput: Partial<AppointmentInput> = {
                ...values
              };
              await handleCreateAppointment({ data: appointmentInput });
              setIsLoadingCreate(false);
            }}
            onCreateAndForward={async values => {
              const appointmentInput: Partial<AppointmentInput> = {
                ...values
              };
              await handleCreateAppointmentAndForwardConsultant({ data: appointmentInput });
              setIsLoadingCreate(false);
            }}
            onCancel={() => {
              setCreateVisible(false);
            }}
            functionOb={functionOb}
            isLoading={isLoadingCreate}
            setIsLoading={setIsLoadingCreate}
          />

          <AppoimentDialogUpdate
            isLoadingChannels={isLoadingChannels}
            rowObj={appointmentPatient}
            functionOb={functionOb}
            onForwardAppointmentCovid={(appointmentId: any) => handleForwardAppointmentCovid(appointmentId)}
            onForwardAppointment={(appointmentId: any) => handleForwardAppointment(appointmentId)}
            onCancelAppointment={(appointmentId: any) => handleCancleAppointment(appointmentId)}
            channels={channels}
            visible={modifyVisible}
            assignStaffName={assignStaffName}
            onModify={values => {
              const appointmentInput: Partial<AppointmentInput> = {
                ...values
              };
              updateAppointment({ data: appointmentInput });
              //setModifyVisible(false);
            }}
            onBookingMultipleAppointments={appointmentPatient => {
              setAppointmentPatientSelected(appointmentPatient);
              setBookingMultipleVisible(true);
            }}
            appointment={appointmentDetail}
            onCancel={() => setModifyVisible(false)}
            isLoading={isLoadingUpdateAppointment || isLoadingAppointment}
          />
        </Spin>
      </div>
    </>
  );
};

export default AppointmentPage;
