import { Button, Table, Modal, Divider, Popover, Image, Tag } from 'antd';
import { ServiceView } from 'common/interfaces/service.interface';
import { formatterMoney } from 'pages/appointment-manager/assign/utils';
import React, { FC } from "react";

interface serviceTableProps {
    listService: any[];
}

const ServiceTable: FC<serviceTableProps> = ({
    listService
}) => {
    return (
        <Table
            rowKey="id"
            dataSource={listService}
            pagination={false}
            className={"display-tbl-modal"}
            scroll={{y:600}}
        >
            <Table.Column
                title="STT"
                width={30}
                align="center"
                render={(value, item, index) => index + 1}
            />
            <Table.Column<ServiceView> align="left" title="Tên xét nghiệm" width={100} render={(_, { serviceName }) => serviceName} />
            <Table.Column<ServiceView> align="right" title="Giá xét nghiệm" width={100} render={(_, { originPrice }) =>originPrice ? formatterMoney.format(Number(originPrice)) : ""} />
        </Table>
    )
}
export default ServiceTable;