import { Modal, Tabs, Row, ColProps, Col } from 'antd';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { useGetFacilitys, useGetFacilitysHos } from 'hooks/facility';
import { useGetServices } from 'hooks/services/useGetServices';
import { useGetWards } from 'hooks/ward/useGetWard';
import React, { FC, useEffect, useState } from 'react';
import { messageError } from 'utils/message';
import { openNotificationRight } from 'utils/notification';
import { ExcelRenderer } from 'react-excel-renderer';

import { formatterMoney } from 'pages/appointment-manager/assign/utils';
import { Image } from 'antd';

import moment from 'moment';
import { useGetPackageTypes } from 'hooks/package';
import ServiceTable from './serviceTable';
const wrapper: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 12,
  xl: 12,
  xxl: 12
};
interface FacilityHos {
  facilityId: any;
  facilityName: any;
}
interface ExaminationPackageCreateDialogProps {
  visible: boolean;
  values: any;
  onCancel: () => void;
  provinces: any[];
  facilitiesList: any[];
}
const ExaminationPackageCreateDialog: FC<ExaminationPackageCreateDialogProps> = ({
  visible,
  onCancel,
  values,
  provinces,
  facilitiesList
}) => {
  //Paging
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [pagination, setPagination] = useState({});
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [serviceSelected, setServiceSelected] = useState([
    {} as {
      key: any;
      serviceCode: any;
      serviceName: any;
      originPrice: number;
      hosSalePrice: number;
    }
  ]);
  const TabPane = Tabs.TabPane;
  if (serviceSelected.length === 1 && JSON.stringify(serviceSelected[0]) === '{}') {
    serviceSelected.shift();
  }
  const [facilityHosList, setFacilityHosList] = useState([{} as FacilityHos]);
  //facilityHosList.shift();
  if (facilityHosList.length === 1 && JSON.stringify(facilityHosList[0]) === '{}') {
    facilityHosList.shift();
  }
  const [discountPercent, setDiscountPercent] = useState(0);
  const [loading, setLoading] = useState(false);
  const [facilityHospitalIds, setFacilityHospitalIds] = useState([]);
  const [listService, setListService] = useState([]);
  //Hook Lấy danh sách Tỉnh/TP
  // const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //Hook Lấy danh sách Quận/Huyện
  const { districts, isLoadingDistricts, loadDistricts } = useGetDistricts();
  //Hook Lấy danh sách Xã/Phường
  const { wards, isLoadingWards, loadWards } = useGetWards();
  //Hook Lấy danh sách don vi de xuat
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  //Hook Lấy danh sách don vi thuc hiên tai vien
  const { facilitiesHos, isLoadingFacilitiesHos, loadFacilitiesHos } = useGetFacilitysHos();
  let filtered: FilteredInput[] = [];
  const variables = { page, pageSize, filtered };
  //Hook Lấy danh sách dịch vụ
  const { pagingServices, isLoadingServices, errorServices, refetchServices } = useGetServices(variables);
  //Lấy danh sách

  const [step, setStep] = React.useState(0);
  // Proceed to next step
  // const nextStep = () => {
  //   //const valuesCreate: any = await form.validateFields();
  //   setStep(step + 1);
  //   //setFacilityHosList([]);
  //   const facilityHospitalIdSelected = form.getFieldValue('facilityHospital')
  //   let facilityHos: FacilityHos;
  //   if (facilityHospitalIdSelected !== undefined) {
  //     for (let i = 0; i < facilityHospitalIdSelected.length; i++) {
  //       let values = facilityHospitalIdSelected[i].split('#');
  //       let facilityId = values[0];
  //       let facilityName = values[1];
  //       facilityHos = { facilityName, facilityId };
  //       facilityHosList.push(facilityHos);
  //     }
  //   }
  //   console.log('Data : ', form.getFieldsValue());
  // };
  // Go back to prev step
  const prevStep = () => {
    setStep(step - 1);
    //setFacilityHosList([]);
  };
  // Handle fields change
  const handleChange = (input, e) => {};
  /*useEffect(() => {
    setPagination({ current: page, pageSize: pageSize, total: pagingServices['records'] });
  }, [pagingServices]);*/
  useEffect(() => {
    // loadProvinces();
    loadFacilities();
    loadFacilitiesHos({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'provinceId',
            value: 'f0e0cba7-aaef-4f0e-a602-ce4eb10b5f38,e303649a-aa6e-4343-b972-851b4da1822e',
            operation: 'IN'
          }
        ]
      }
    });
  }, []);
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    setPage(pagination['current']);
    setPageSize(pagination['pageSize']);
  };
  const handleDelete = key => {
    /* const dataSource = [...this.state.dataSource];
    this.setState({
      dataSource: dataSource.filter(item => item.key !== key)
    });*/
  };
  const handleAdd = () => {
    /*const { count, dataSource } = this.state;
    const newData = {
      key: count,
      name: `Edward King ${count}`,
      age: '32',
      address: `London, Park Lane no. ${count}`
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1
    });*/
  };
  const handleSave = row => {
    /*const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    this.setState({
      dataSource: newData
    });*/
  };

  const fileHandler = fileList => {
    let fileObj = fileList;
    if (!fileObj) {
      //Xu ly thong bao
      //openNotificationRight('Không có file nào được tải lên.');
      messageError('Không có file nào được tải lên.');
      return false;
    }
    if (
      !(
        fileObj.type === 'application/vnd.ms-excel' ||
        fileObj.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      )
    ) {
      openNotificationRight('Định dạng file không hợp lệ, vui lòng kiểm tra lại.');
      messageError('Định dạng file không hợp lệ, vui lòng kiểm tra lại.');
      return false;
    }
    //just pass the fileObj as parameter
    ExcelRenderer(fileObj, (err, resp) => {
      if (err) {
        console.log(err);
      } else {
        let newRows: string[] = [];
        resp.rows.slice(1)?.map((row, index) => {
          if (row && row !== 'undefined') {
            newRows.push(row[1]);
          }
        });
        if (newRows.length === 0) {
          //Xu ly thong bao
          return false;
        } else {
          refetchServices({
            filtered: [
              {
                id: 'serviceCode',
                value: newRows.join(','),
                operation: 'IN'
              }
            ]
          });
          setServiceSelected(pagingServices.data);
        }
      }
    });
    return false;
  };
  const start = () => {
    setLoading(true);
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
      setServiceSelected([]);
      setLoading(false);
    }, 500);
  };
  let servicesDataSelectedArr = [
    {} as {
      key: any;
      serviceCode: any;
      serviceName: any;
      originPrice: any;
      hosSalePrice: any;
    }
  ];
  servicesDataSelectedArr.shift();
  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
    for (let i = 0; i < pagingServices?.data.length; i++) {
      for (let j = 0; j < selectedRowKeys.length; j++) {
        if (pagingServices?.data[i]['serviceId'] === selectedRowKeys[j]) {
          servicesDataSelectedArr.push(pagingServices?.data[i]);
        }
      }
    }
    setServiceSelected(servicesDataSelectedArr);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  };
  const hasSelected = selectedRowKeys.length > 0;
  const onInputChange = (key, index) => (e: React.ChangeEvent<HTMLInputElement>) => {
    /*const newData = [...tableData];
    newData[index][key] = Number(e.target.value);
    setTotal(newData, index);
    setTableData(newData);*/
  };

  const setTotal = (data, index) => {
    // Set total
    data[index]['totalCount'] = Number(data[index]['goals'] + data[index]['assists']);
  };

  const { packageTypes, isLoadingPackageTypes, loadPackageTypes } = useGetPackageTypes();
  useEffect(() => {
    if (visible) {
      loadPackageTypes();
    }
  }, [visible]);
  const dayOfWeek = [
    { name: 'Thứ hai', id: 2 },
    { name: 'Thứ ba', id: 3 },
    { name: 'Thứ tư', id: 4 },
    { name: 'Thứ năm', id: 5 },
    { name: 'Thứ sáu', id: 6 },
    { name: 'Thứ bảy', id: 0 },
    { name: 'Chủ nhật', id: 1 }
  ];

  const mapData = (listId, listItem) => {
    var str = '';
    const listArrId = listId.split(',');
    if (listArrId && listArrId.length > 0 && listItem && listItem.length > 0) {
      for (var i = 0; i < listItem.length; i++) {
        for (var j = 0; j < listArrId.length; j++) {
          if (listArrId[j] == listItem[i]?.id) {
            if (str.length > 0) {
              str += ', ';
            }
            str += listItem[i]?.name;
            break;
          }
        }
      }
    }
    return str;
  };

  useEffect(() => {
    if (values && values.services) {
      let listS = values.services.filter(item => {
        return item.status == 1;
      });
      setListService(listS);
    }
  }, [values]);

  return (
    <Modal
      centered
      style={{ marginTop: 15 }}
      title="Thông tin gói khám"
      visible={visible}
      width={1300}
      // onOk={false}
      onCancel={onCancel}
      okButtonProps={{ style: { display: 'none' } }}
    >
      <Row className="fix-row">
        <Col {...wrapper}>
          <div className="title-form">Thông tin chung</div>
          <div className="">
            <span className="title-col"> Ảnh đại diện : </span> <br />
            <Image width={200} src={values?.image ? values?.image : ''} />
          </div>
          <div className="">
            <span className="title-col"> Tên gói khám : </span> {values?.name ? values?.name : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Giới hạn lượt đặt :</span> {values?.maxNumBook ? values?.maxNumBook : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Độ tuổi từ - đến : </span> {values?.minAge ? values?.minAge : ''} -{' '}
            {values?.maxAge ? values?.maxAge : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Giá gốc : </span>{' '}
            {values?.originPrice ? formatterMoney.format(values?.originPrice) : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Giá giảm : </span>{' '}
            <span style={{ color: 'red' }}>
              {values?.hosSalePrice ? formatterMoney.format(values?.hosSalePrice) : ''}
            </span>
          </div>
          <div className="tle-col">
            <span className="title-col"> Loại gói khám : </span>{' '}
            {values?.packageDetail?.packageTypes ? mapData(values?.packageDetail?.packageTypes, packageTypes) : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Ngày làm việc : </span>
            {values?.packageDetail?.weekDays ? mapData(values?.packageDetail?.weekDays, dayOfWeek) : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Tỉnh/thành : </span>{' '}
            {values?.packageDetail?.provinces ? mapData(values?.packageDetail?.provinces, provinces) : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Đơn vị : </span>{' '}
            {values?.packageDetail?.applyFacilities
              ? mapData(values?.packageDetail?.applyFacilities, facilitiesList)
              : ''}
          </div>
          <div className="">
            <span className="title-col"> Ngày bắt đầu : </span>{' '}
            {values?.startDate ? moment(values?.startDate).format('DD/MM/YYYY') : ''}
          </div>
          <div className="">
            <span className="title-col"> Ngày kết thúc : </span>
            {values?.endDate ? moment(values?.endDate).format('DD/MM/YYYY') : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Trạng thái : </span>{' '}
            {values?.status == 1 ? 'Đã duyệt' : values?.status == '2' ? 'Chưa duyệt' : ' '}
          </div>
          <div className="tle-col">
            <span className="title-col"> Mã gói : </span> {values?.code ? values.code : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> ID gói khám : </span> {values?.packageHisId ? values.packageHisId : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Hình thức gói khám : </span>{' '}
            {values?.methodType == 'HOSPITAL'
              ? 'Tại viện'
              : values?.methodType == 'HOME'
              ? 'Tại nhà'
              : values?.methodType == 'BOTH'
              ? 'Cả hai'
              : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Giới tính : </span>{' '}
            {values?.sex == 'MALE' ? 'Nam' : values?.sex == 'FEMALE' ? 'Nữ' : values?.sex == 'BOTH' ? 'Cả hai' : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Hiển thị : </span>{' '}
            {values?.display == 'WEB'
              ? 'WEB'
              : values?.display == 'APP'
              ? 'APP'
              : values?.display == 'BOTH'
              ? 'Cả hai'
              : ''}
          </div>
          <div className="tle-col">
            <span className="title-col"> Mô tả : </span> <br />
            <div
              dangerouslySetInnerHTML={{
                __html: values?.description ? values?.description : ''
              }}
              style={{
                overflow: 'auto',
                maxHeight: 500,
                whiteSpace: 'pre-line'
              }}
            ></div>
          </div>
          <div className="tle-col">
            <span className="title-col"> Quy trình : </span>
            <br />
            <div
              dangerouslySetInnerHTML={{
                __html: values?.processNote ? values?.processNote : ''
              }}
              style={{
                overflow: 'auto',
                maxHeight: 500,
                whiteSpace: 'pre-line'
              }}
            ></div>
          </div>
          <div className="tle-col">
            <span className="title-col"> Lưu ý : </span> <br />
            <div
              dangerouslySetInnerHTML={{
                __html: values?.warningNote ? values?.warningNote : ''
              }}
              style={{
                overflow: 'auto',
                maxHeight: 500,
                whiteSpace: 'pre-line'
              }}
            ></div>
          </div>
        </Col>
        <Col className="tble-service" {...wrapper}>
          <div className="title-form">Danh sách gói khám</div>
          <ServiceTable listService={listService} />
        </Col>
      </Row>
    </Modal>
  );
};

export default ExaminationPackageCreateDialog;
