import { Button, Checkbox, Col, Modal, PageHeader, Row, Space, Spin, Tabs } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import '../index.less';
import {
  ArrowRightOutlined,
  BankOutlined,
  CloseOutlined,
  ExclamationCircleOutlined,
  HighlightOutlined,
  HistoryOutlined,
  HomeOutlined,
  OrderedListOutlined,
  SaveOutlined,
  StepForwardOutlined
} from '@ant-design/icons';
import { useDeleteAppointmentComments } from 'hooks/appointment/useDeleteAppointmentComments';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { AppointmentPatient } from 'common/interfaces/appointmentPatient.interface';
import { useGetWards } from 'hooks/ward/useGetWard';
import { useGetPatients } from 'hooks/patient/useGetPatient';
import { useGetSpecialist } from 'hooks/specialist/useGetSpecialist';
import { useGetFacilitys } from 'hooks/facility';
import { useGetStreetsLazy } from 'hooks/street';
import { useGetSiteStaffsLazy } from 'hooks/site-staff';
import {
  useGetAppointmentComments,
  useGetAppointmentWorkTimeHome,
  useGetAppointmentWorkTimeHospital,
  useSaveAppointmentComments
} from 'hooks/appointment';
import { AppointmentInput } from 'common/interfaces/input/appointmentInput.interfce';
import { useGetReasons } from 'hooks/reasons/useGetReasons';
import useGetModifyAppointmentForm from '../useGetModifyAppointmentForm';
import { Appointment } from 'common/interfaces/appointment.interface';
import moment from 'moment';
import AppointmentActions from '../appointmentActions';
import { Patient } from 'common/interfaces/patient.interface';
import { useGetRoutesLazy } from 'hooks/route';
import AssignmentPage from '../assignAppoinment';
import { useGetDoctor } from 'hooks/doctor/useGetDoctor';
import { useGetPackage } from 'hooks/package';
import { useGetAllWorkTimeFacility } from 'hooks/worktimes/useGetAllWorkTimeFacility';
import { Channel } from 'common/interfaces/channel.interface';
import { useGetAllCountriesLazy } from 'hooks/appointment/useGetAllCountries';
import { functionCodeConstants } from 'constants/functions';
import { openNotificationRight } from 'utils/notification';
import { getStatus } from 'pages/advisory-manager/advisory-input/utils';
import { useGetAllWorktimes } from 'hooks/worktimes/useGetWorkTimes';

const TabPane = Tabs.TabPane;
const CheckboxGroup = Checkbox.Group;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};
interface AppointmentModifyDialogProps {
  visible: boolean;
  rowObj: any;
  onModify: (values: Partial<AppointmentInput>) => void;
  onBookingMultipleAppointments: (appointmentPatient: AppointmentPatient) => void;
  appointment?: Partial<Appointment>;
  onCancelAppointment: (id: any) => void;
  onForwardAppointment: (id: any) => void;
  onForwardAppointmentCovid: (id: any) => void;
  onCancel: () => void;
  isLoading?: boolean;
  functionOb: any;
  channels: any;
  isLoadingChannels: boolean;
  assignStaffName?: string;
}

const AppoimentDialogUpdate: FC<AppointmentModifyDialogProps> = ({
  visible,
  onModify,
  rowObj,
  appointment,
  onCancelAppointment,
  onForwardAppointment,
  onForwardAppointmentCovid,
  onBookingMultipleAppointments,
  onCancel,
  isLoading,
  functionOb,
  channels,
  isLoadingChannels,
  assignStaffName
}) => {
  const [isBHYT, setIsBHYT] = useState(false);
  const [appointmentDate, setAppointmentDate]: any = useState();
  const [streetId, setStreetId] = useState('');
  const [parentFacilityId, setParentFacilityId] = useState('');
  const [visibleAssigAppointment, setVisibleAssigAppointment] = useState(false);
  const [txtKG, setTxtKG]: any = useState();
  const [isInvoice, setIsInvoice] = useState(false);
  const [typeInvoice, setTypeInvoice] = useState('0');
  const [isCovided, setIsCovided] = useState(false);
  const [locationAll, setLoactionAll] = useState('');
  const [patternIdNo, setPatternIdNo] = useState(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
  //confirm Loading
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  //Hook Lấy danh sách Mã thẻ KH (PID)
  const { patients, loadPatients, isLoadingPatients } = useGetPatients();
  // Hook Lấy danh sách Cán bộ báo lịch
  const { siteStaffs, loadSiteStaffs, isLoadingSiteStaffs } = useGetSiteStaffsLazy();
  //Hook Lấy danh sách CB được phân lịch
  const {
    siteStaffs: siteStaffsAssign,
    isLoadingSiteStaffs: isLoadingSiteStaffsAssign,
    loadSiteStaffs: loadSiteStaffsAssign
  } = useGetSiteStaffsLazy();
  //Hook Lấy danh sách Chuyên khoa khám
  const { specialists, loadSpecialist, isLoadingSpecialists } = useGetSpecialist();
  //Hook Lấy danh sách đường phố
  const { streets, loadStreets, isLoadingStreets } = useGetStreetsLazy();
  //Hook Lấy danh sách cung đường
  const { routesData, isLoadingRoutes, loadRoutes } = useGetRoutesLazy();
  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, loadProvinces, isLoadingProvince } = useGetProvincesLazy();
  //Hook Lấy danh sách đơn vị
  const { facilities, loadFacilities, isLoadingFacilities } = useGetFacilitys();
  const {
    facilities: facilitiesObjectSend,
    isLoadingFacilities: isLoadingFacilitiesObjectSend,
    loadFacilities: loadFacilitiesObjectSend
  } = useGetFacilitys();
  //Lấy danh sách khung giờ
  const { worktimes, isLoadingWorktimes, loadWorktimes } = useGetAllWorktimes();
  useEffect(() => {
    if (facilities?.length > 0) {
      form.setFieldsValue({
        facilityId: facilities[0]?.id
      });
    } else {
      form.setFieldsValue({
        facilityId: ''
      });
    }
  }, [facilities]);

  useEffect(() => {
    if (routesData) {
      form.setFieldsValue({
        // samplingRouteId: routesData[0]?.id,
        // samplingRouteName: routesData[0]?.name,
        facilityId: routesData[0]?.facilityId ? routesData[0]?.facilityId : '',
        // examFacilityName: routesData[0]?.parentFacilityName,
        // facilityName: routesData[0]?.facilityName
        examFacilityId: routesData[0]?.parentFacilityId ? routesData[0]?.parentFacilityId : ''
      });
      if (routesData[0]?.facilityCode) {
        setLoactionAll(routesData[0]?.facilityCode);
      } else {
        setLoactionAll('');
      }
    }
  }, [routesData]);
  useEffect(() => {
    let street = streets?.find(item => item?.id === streetId);
    if (street?.['facilityId']) {
      loadFacilities({
        variables: {
          page: 1,
          filtered: [
            /*{
              id: 'isHome',
              value: '1',
              operation: '=='
            },*/
            {
              id: 'id',
              value: street?.['facilityId'],
              operation: '=='
            },
            {
              id: 'status',
              value: '1',
              operation: '=='
            }
          ]
        }
      });
    } else {
      form.setFieldsValue({
        facilityId: ''
      });
    }
    loadFacilitiesHome({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'isHome',
            value: '1',
            operation: '=='
          },
          {
            id: 'status',
            value: '1',
            operation: '=='
          },
          {
            id: 'id',
            value: street?.['parentFacilityId'],
            operation: '=='
          }
        ]
      }
    });
  }, [streets]);
  //Hook Lấy danh sách Quận/Huyện
  const { districts, loadDistricts, isLoadingDistricts } = useGetDistricts();
  //Hook Lấy danh sách Xã/Phường
  const { wards, loadWards, isLoadingWards } = useGetWards();
  //Hook Lấy danh sách nơi khám tại viện
  const {
    facilities: facilitiesHos,
    isLoadingFacilities: isLoadingFacilitiesHos,
    loadFacilities: loadFacilitiesHos
  } = useGetFacilitys();
  //Hook Lấy danh sách nơi khám tại nhà
  const {
    facilities: facilitiesHome,
    isLoadingFacilities: isLoadingFacilitiesHome,
    loadFacilities: loadFacilitiesHome
  } = useGetFacilitys();
  // useEffect(() => {
  //   if (appointment?.methodType === 'HOME') {
  //     // if (facilitiesHome?.length > 0) {
  //     //   // form.setFieldsValue({
  //     //   //   examFacilityId: facilitiesHome[0]?.id
  //     //   // });
  //     // } else {
  //     //   console.log('aaaaaaaaaaaaaaaaaa');
  //     //   form.setFieldsValue({
  //     //     examFacilityId: ''
  //     //   });
  //     // }
  //   }
  // }, [facilitiesHome]);
  //Hook Lấy danh lý do
  const { reasons, loadReasons, isLoadingReasons } = useGetReasons();
  //Hook Lấy danh sách khung giờ tại nhà
  const { appointmentWorkTimeHome, loadAppointmentWorkTimeHome } = useGetAppointmentWorkTimeHome();
  //Hook Lấy danh sách khung giờ tại viện
  const { appointmentWorkTimeHospital, loadAppointmentWorkTimeHospital } = useGetAppointmentWorkTimeHospital();
  //Hook Lấy khung giờ theo đơn vị
  const { allWorkTimeFacility, loadAllWorkTimeFacility, isLoadingAllWorkTimeFacility } = useGetAllWorkTimeFacility();
  //Hook lấy danh sách Ghi chú
  const { loadAppointmentComments, appointmentComments } = useGetAppointmentComments();
  //Hook Lấy danh sách quốc tịch
  const { pagingCountries, isLoadingCountries, loadCountries, errorCountries } = useGetAllCountriesLazy();
  //Hook xoá ghi chú
  const {
    deleteAppointmentComments,
    errorDeleteAppointmentComments,
    resultDeleteAppointmentComments
  } = useDeleteAppointmentComments();
  //Hook lưu ghi chú
  const {
    saveAppointmentComments,
    isLoadingSaveAppointmentComments,
    errorSaveAppointmentComments
  } = useSaveAppointmentComments();
  //Lấy  BS CTV
  const { doctor, isLoadingDoctor, loadDoctor } = useGetDoctor();
  //Lấy gói khám
  const { package: packageDetail, isLoadingPackage, loadPackage } = useGetPackage();

  const [methodType, setMethodType] = useState('HOSPITAL');
  const [activeKey, setActiveKey] = useState('HOSPITAL');
  const initialValues = (): Partial<Appointment> => ({
    ...appointment,
    examFacilityId: appointment?.examFacilityId,
    streetId: appointment?.streetId ? appointment?.streetId + '***' + appointment?.routeId : undefined,
    appointmentDate: moment(appointment?.appointmentDate, 'DD/MM/YYYY HH:mm:ss'),
    assignStaffName: assignStaffName,
    healthDeclaration: {
      ...appointment?.healthDeclaration,
      prognostic: appointment?.healthDeclaration?.prognostic?.split(','),
      testedCovid: appointment?.healthDeclaration?.testedCovid?.toString(),
      humanHasCovid: appointment?.healthDeclaration?.humanHasCovid?.toString(),
      nationPassOther: appointment?.healthDeclaration?.nationPassOther?.toString(),
      streetHasCovid: appointment?.healthDeclaration?.streetHasCovid?.toString(),
      typeCustomerKBYT: appointment?.healthDeclaration?.typeCustomerKBYT?.toString(),
      invoice: appointment?.healthDeclaration?.invoice?.toString().split(','),
      printName:
        appointment?.healthDeclaration?.printName == 0 || !appointment?.healthDeclaration?.printName ? ['1'] : [],
      dateHascovid: appointment?.healthDeclaration?.dateHascovid
        ? moment(appointment?.healthDeclaration?.dateHascovid)
        : undefined,
      dateOnset: appointment?.healthDeclaration?.dateOnset
        ? moment(appointment?.healthDeclaration?.dateOnset)
        : undefined
    },
    patient: {
      ...appointment?.patient,
      pid: appointment?.pid,
      name: appointment?.patient?.name?.trim(),
      birthDate: appointment?.patient?.birthDate
        ? moment(appointment?.patient?.birthDate, 'DD/MM/YYYY HH:mm:ss')
        : appointment?.patient?.birthYear
        ? appointment?.patient?.birthYear !== '0'
          ? moment('01/01/' + appointment?.patient?.birthYear, 'DD/MM/YYYY HH:mm:ss')
          : undefined
        : undefined,
      birthYear: appointment?.patient?.birthYear
        ? appointment?.patient?.birthYear !== '0'
          ? moment(appointment?.patient?.birthYear, 'YYYY')
          : undefined
        : appointment?.patient?.birthDate
        ? moment(moment(appointment?.patient?.birthDate, 'DD/MM/YYYY HH:mm:ss'), 'YYYY')
        : undefined,
      phone: appointment?.patient?.phone
        ? appointment?.patient?.phone.slice(0, 1) !== '0'
          ? '0' + appointment?.patient?.phone
          : appointment?.patient?.phone
        : undefined,
      relationType: appointment?.patient?.relationType ? appointment?.patient?.relationType : 'PATIENT',
      type: appointment?.patient?.type ? appointment?.patient?.type : 'PERSONAL'
    },
    reasonId: appointment?.reasonId ? appointment?.reasonId : 'be164936-c2ae-4641-8674-801927a1f027'
  });

  useEffect(() => {
    form.resetFields();
  }, [visible]);

  useEffect(() => {
    if (appointment?.healthDeclaration?.invoice == 1) {
      setIsInvoice(true);
    } else {
      setIsInvoice(false);
    }
    if (appointment?.healthDeclaration?.dateHascovid) {
      setIsCovided(true);
      form.setFieldsValue({
        healthDeclaration: {
          dateHascovid: moment(appointment?.healthDeclaration?.dateHascovid)
        }
      });
    } else {
      setIsCovided(false);
    }
    if (appointment?.healthDeclaration?.typeInvoice) {
      setTypeInvoice(appointment?.healthDeclaration?.typeInvoice.toString());
    } else {
      setTypeInvoice('0');
    }
    if (appointment?.streetId) {
      setStreetId(appointment.streetId);
    } else {
      setStreetId('');
    }
    if (appointment?.patient?.nationality == 'VN') {
      setPatternIdNo(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
    } else {
      setPatternIdNo(/^[a-zA-Z0-9_.-]{6,13}$/ as RegExp);
    }
    form.setFieldsValue({
      confirm: ['1']
    });
  }, [appointment]);

  const formateYYYYMMDDFromDDMMYYYY = date => {
    if (date) {
      let listDT = date?.split(' ');
      if (listDT && listDT.length > 0) {
        let listDate = listDT[0]?.split('/');
        if (listDate && listDate.length == 3) {
          return listDate[2] + '-' + listDate[1] + '-' + listDate[0];
        }
      }
    }
    return undefined;
  };

  useEffect(() => {
    loadProvinces();
    if (appointment != null) {
      var dateFormatYYYYMMDD = formateYYYYMMDDFromDDMMYYYY(appointment.appointmentDate);
      // console.log('appointment.appointmentDate', dateFormatYYYYMMDD);
      setTxtKG(appointment.workTime?.name);
      setAppointmentDate(dateFormatYYYYMMDD);
      if (appointment.patient?.provinceId) {
        loadDistricts({
          variables: {
            provinceId: appointment.patient.provinceId
          }
        });
      }
      if (appointment.patient?.districtId) {
        loadWards({
          variables: {
            districtId: appointment.patient.districtId
          }
        });
      }
      if (appointment.methodType) {
        setMethodType(appointment.methodType);
      }
      if (appointment?.examFacilityId && appointment?.appointmentDate && appointment?.methodType === 'HOME') {
        var parts = String(appointment.appointmentDate)
          ?.split(' ')[0]
          ?.split('/');
        loadAppointmentWorkTimeHome({
          variables: {
            data: {
              appointmentDate: parts[2] + '-' + parts[1] + '-' + parts[0],
              examFacilityId: appointment?.examFacilityId
            }
          }
        });
        /*loadAppointmentWorkTimeHospital({
                  variables: {
                    data: {
                      appointmentDate: '1111-11-11'
                    }
                  }
                });*/
      } else if (
        appointment?.examFacilityId &&
        appointment?.appointmentDate &&
        appointment?.methodType === 'HOSPITAL'
      ) {
        var parts = String(appointment.appointmentDate)
          ?.split(' ')[0]
          ?.split('/');
        loadAppointmentWorkTimeHospital({
          variables: {
            data: {
              appointmentDate: parts[2] + '-' + parts[1] + '-' + parts[0],
              examFacilityId: appointment?.examFacilityId
            }
          }
        });
        /*loadAppointmentWorkTimeHome({
                  variables: {
                    data: {
                      appointmentDate: '1111-11-11'
                    }
                  }
                });*/
      }
      loadStreets({
        variables: {
          page: 1,
          filtered: [
            {
              id: 'provinceId',
              value: appointment?.patient?.provinceId,
              operation: '=='
            },
            {
              id: 'status',
              value: '1',
              operation: '=='
            }
          ]
        }
      });
      if (appointment?.routeId) {
        loadRoutes({
          variables: {
            filtered: [
              {
                id: 'id',
                value: appointment?.routeId,
                operation: '=='
              },
              {
                id: 'status',
                value: '1',
                operation: '=='
              }
            ]
          }
        });
      }

      loadAppointmentComments({
        variables: {
          filtered: [
            {
              id: 'appointmentId',
              value: appointment?.id,
              operation: '=='
            }
          ],
          sorted: [
            {
              id: 'createDate',
              desc: true
            }
          ]
        }
      });
      loadDoctor({
        variables: {
          id: appointment.indicatedDoctorId
        }
      });
      loadPackage({
        variables: {
          id: appointment.packages?.id
        }
      });
      if (!appointment.methodType || appointment.methodType === 'HOME') {
        loadFacilitiesHome({
          variables: {
            page: 1,
            filtered: [
              {
                id: 'isHome',
                value: '1',
                operation: '=='
              },
              {
                id: 'provinceIds',
                value: appointment.patient?.provinceId,
                operation: '~'
              },
              {
                id: 'status',
                value: '1',
                operation: '=='
              }
            ]
          }
        });
      } else if (appointment.methodType === 'HOSPITAL') {
        loadFacilitiesHos({
          variables: {
            page: 1,
            filtered: [
              {
                id: 'isHospital',
                value: '1',
                operation: '=='
              },
              {
                id: 'status',
                value: '1',
                operation: '=='
              }
            ]
          }
        });
      }
      loadAllWorkTimeFacility({
        variables: {
          // facilityId: appointment.examFacilityId
        }
      });
      form.setFieldsValue(initialValues());
    }
  }, [appointment]);

  useEffect(() => {
    loadFacilitiesObjectSend({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
    loadWorktimes({
      variables: {
        type: 'STAFF'
      }
    });
    loadPatients({
      variables: {
        page: 1
      }
    });
    loadSpecialist({
      variables: {
        pageSize: -1
      }
    });
    loadSiteStaffs({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
    loadReasons();
    loadCountries();
  }, []);

  const {
    Form,
    form,
    AppointmentSource,
    PhoneNumber,
    CustomerType,
    CustomerFullname,
    ReasonBooking,
    DateofBirth,
    BirthYear,
    CollaboratorDoctor,
    Gender,
    MedicalExaminationPackage,
    ContactAddress,
    Test,
    Provinces,
    Districts,
    Wards,
    Street4Home,
    TestNotes,
    Email,
    TotalPrice,
    IdentityCard,
    TotalPriceDiscount,
    GeneticCode,
    ScheduleReporter,
    PID,
    MedicalPlace,
    ObjectSend,
    Specialists,
    AppointmentWorkTime,
    AppointmentObject,
    AppointmentTimeFrame,
    AppointmentNotes,
    AppointmentNotesTable,
    MgsContent,
    AppointmentDate,
    Route,
    IsCovid,
    TowInjections,
    FacilityParent,
    Facility,
    Nationality,
    BHYT,
    ReasonBookingStr,
    AssignStaff,
    CodeAppoiment,
    Status,
    SetForObject,
    TypeCustomer,
    DateOnset,
    Prognostic,
    StreetHasCovid,
    NationPassOther,
    HumanHasCovid,
    TestedCovid,
    Invoice,
    AddressProfile,
    EmailProfile,
    NameCompany,
    TaxCode,
    AddressCompany,
    EmailCompany,
    PrintName,
    ReasonCheckup
  } = useGetModifyAppointmentForm({
    name: 'modify-appointment-form',
    responsive: true,
    appointmentInput: initialValues(),
    isNewDialog: true,
    isDisable: false,
    patternIdNo: patternIdNo
  });
  const getCodeFactility = (examFacilityId, isParent, isHospital?) => {
    if (isHospital) {
      if (facilitiesHos && facilitiesHos.length > 0 && examFacilityId && methodType == 'HOSPITAL') {
        for (var i = 0; i < facilitiesHos.length; i++) {
          if (facilitiesHos[i].id == examFacilityId) {
            return facilitiesHos[i].code;
          }
        }
      }
    }
    if (isParent) {
      if (facilitiesHome && facilitiesHome.length > 0 && examFacilityId && methodType == 'HOME') {
        for (var i = 0; i < facilitiesHome.length; i++) {
          if (facilitiesHome[i].id == examFacilityId) {
            return facilitiesHome[i].code;
          }
        }
      }
    } else {
      if (facilities && facilities.length > 0 && examFacilityId && methodType == 'HOME') {
        for (var i = 0; i < facilities.length; i++) {
          if (facilities[i].id == examFacilityId) {
            return facilities[i].code;
          }
        }
      }
    }

    return null;
  };
  const onSubmit = async () => {
    if (activeKey === 'NOTE') {
      openNotificationRight(
        'Sau khi nhập ghi chú, vui lòng chọn lại loại Lịch hẹn Tại nhà/Tại viện để lưu dữ liệu.',
        'warning'
      );
      return;
    }
    const values = await form.validateFields();
    let channelType = values['channelType'];
    let channel: Channel = channels.find(function(element: Channel) {
      return element.code === channelType;
    });
    const methodType = form.getFieldValue('methodType') ? form.getFieldValue('methodType') : 'HOME';
    delete values['parentFacilityId'];
    let location = '';
    if (methodType === 'HOME') {
      if (values.facilityId) {
        location = getCodeFactility(values.facilityId, false);
      } else {
        location = getCodeFactility(values.examFacilityId, true);
      }
    }
    delete values['facilityId'];
    delete values['assignStaffName'];
    let streetId = values['streetId']?.split('***')[0];
    let routeId = values['streetId']?.split('***')[1];
    delete values['confirm'];
    delete values['appointmentNotes'];
    if (values.healthDeclaration && values?.healthDeclaration?.invoice?.length > 0) {
      values.healthDeclaration.typeInvoice = typeInvoice;
      values.healthDeclaration.invoice = 1;
      values.healthDeclaration.printName = typeInvoice == '1' && values.healthDeclaration.printName.length > 0 ? '' : 1;
    } else if (values.healthDeclaration && values?.healthDeclaration?.invoice?.length <= 0) {
      values.healthDeclaration.invoice = '';
    }
    onModify({
      id: appointment?.id,
      ...values,
      methodType,
      appointmentDate: (values.appointmentDate as moment.Moment)?.format('DD/MM/YYYY'),
      streetId,
      routeId,
      assignStaffId: appointment?.assignStaffId,
      channelId: channel?.id,
      channelType,
      originalAmount: form.getFieldValue('originalAmount'),
      totalAmount: form.getFieldValue('totalAmount'),
      healthDeclaration:
        methodType === 'HOME'
          ? {
              ...values.healthDeclaration,
              prognostic: values.healthDeclaration?.prognostic.toString(),
              location: location,
              purpose: 'Khám bệnh',
              dateOnset: values.healthDeclaration?.dateOnset
                ? moment(values.healthDeclaration?.dateOnset).format('DD/MM/YYYY')
                : '',
              dateHascovid: values.healthDeclaration?.dateHascovid
                ? moment(values.healthDeclaration?.dateHascovid).format('DD/MM/YYYY')
                : ''
            }
          : {
              ...values.healthDeclaration,
              prognostic: values.healthDeclaration?.prognostic.toString(),
              location: getCodeFactility(values.examFacilityId, true, true),
              purpose: 'Khám bệnh',
              dateOnset: values.healthDeclaration?.dateOnset
                ? moment(values.healthDeclaration?.dateOnset).format('DD/MM/YYYY')
                : '',
              dateHascovid: values.healthDeclaration?.dateHascovid
                ? moment(values.healthDeclaration?.dateHascovid).format('DD/MM/YYYY')
                : ''
            },
      services: values.services
        ? values.services?.map(val => ({
            objectId: val.id,
            originalPrice: val.originalPrice,
            salePrice: val.salePrice
            /*indicatedDoctorId: JSON.parse(String(localStorage.getItem('user')))
              ? JSON.parse(String(localStorage.getItem('user')))['id']
              : ''*/
          }))
        : undefined,
      packages: values.packages?.id
        ? {
            objectId: values.packages?.id,
            originalPrice: form.getFieldValue('originalAmount'),
            salePrice: form.getFieldValue('totalAmount')
          }
        : undefined,
      patient: {
        ...values.patient,
        name: values.patient?.name?.trim(),
        birthDate: (values.patient?.birthDate as moment.Moment)?.format('DD/MM/YYYY'),
        birthYear: values.patient?.birthYear
          ? (values.patient?.birthYear as moment.Moment)?.format('YYYY')
          : (values.patient?.birthDate as moment.Moment)?.format('YYYY')
      },
      pid: values.patient?.pid,
      appointmentType: values.packages?.id ? 'PACKAGE' : 'SCHEDULE'
    });
    setConfirmLoading(true);
    setTimeout(() => {
      setConfirmLoading(false);
    }, 5000);
  };

  const handleChangePatient = (values?: Partial<Patient>) => {
    if (values?.provinceId) {
      loadDistricts({
        variables: {
          provinceId: values.provinceId
        }
      });
    }
    if (values?.districtId) {
      loadWards({
        variables: {
          districtId: values.districtId
        }
      });
    }
  };

  const handleDeleteAppointmentComment = async value => {
    await deleteAppointmentComments(value);
    loadAppointmentComments({
      variables: {
        filtered: [
          {
            id: 'appointmentId',
            value: appointment?.id,
            operation: '=='
          }
        ],
        sorted: [
          {
            id: 'createDate',
            desc: true
          }
        ]
      }
    });
  };

  const handleSaveAppointmentComment = async values => {
    await saveAppointmentComments(values);
    loadAppointmentComments({
      variables: {
        filtered: [
          {
            id: 'appointmentId',
            value: appointment?.id,
            operation: '=='
          }
        ],
        sorted: [
          {
            id: 'createDate',
            desc: true
          }
        ]
      }
    });
  };

  const handleChangeStreet = (streetId?: string, routeId?: string) => {
    setStreetId(streetId ? streetId : '');
    let street = streets?.find(item => item?.id === streetId);
    if (street?.['facilityId']) {
      loadFacilities({
        variables: {
          page: 1,
          filtered: [
            /*{
              id: 'isHome',
              value: '1',
              operation: '=='
            },*/
            {
              id: 'id',
              value: street?.['facilityId'],
              operation: '=='
            },
            {
              id: 'status',
              value: '1',
              operation: '=='
            }
          ]
        }
      });
    } else {
      form.setFieldsValue({
        facilityId: ''
      });
    }
    loadFacilitiesHome({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'isHome',
            value: '1',
            operation: '=='
          },
          {
            id: 'status',
            value: '1',
            operation: '=='
          },
          {
            id: 'id',
            value: street?.['parentFacilityId'],
            operation: '=='
          }
        ]
      }
    });

    if (routeId !== '') {
      loadRoutes({
        variables: {
          filtered: [
            {
              id: 'id',
              value: routeId,
              operation: '=='
            },
            {
              id: 'status',
              value: '1',
              operation: '=='
            }
          ]
        }
      });
    } else {
      //
    }
  };

  const handleForwardAppointment = async appointmentId => {
    await onForwardAppointment(appointmentId);
  };

  const handleCancleAppointment = async appointmentId => {
    await onCancelAppointment(appointmentId);
  };

  const handleForwardAppointmentToCovidAppointment = async appointmentId => {
    await onForwardAppointmentCovid(appointmentId);
  };

  /*useEffect(() => {
    initData();
  }, [initData]);*/
  const handleMenuClick = keyMenu => {
    keyMenu = String(keyMenu);
    if (keyMenu === '1') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn chuyển lịch hẹn đang thao tác sang lịch hẹn covid ?',
        okText: 'Đồng ý',
        cancelText: 'Hủy bỏ',
        onOk() {
          handleForwardAppointmentToCovidAppointment(appointment?.id);
        },
        onCancel() {}
      });
    } else if (keyMenu === '2') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn huỷ lịch hẹn đang thao tác ?',
        okText: 'Đồng ý',
        cancelText: 'Hủy bỏ',
        onOk() {
          handleCancleAppointment(appointment?.id);
        },
        onCancel() {}
      });
    } else if (keyMenu === '3') {
      if (appointment?.status === 13) {
        openNotificationRight('Không thể đặt nhiều lịch đối với bản ghi đã huỷ.', 'warning');
        return;
      }
      onBookingMultipleAppointments(rowObj);
    } else if (keyMenu === '4') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn chuyển lịch hẹn đang thao tác' + '' + ' cho bác sĩ tư vấn ?',
        okText: 'Đồng ý',
        cancelText: 'Hủy bỏ',
        onOk() {
          if (appointment?.status === 13) {
            openNotificationRight('Không thể chuyển Bác sĩ tư vấn đối với bản ghi đã huỷ.', 'warning');
            return;
          }
          handleForwardAppointment(appointment?.id);
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    } else if (keyMenu === '5') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn xóa lịch hẹn đã chọn' + '' + ' ?',
        cancelText: 'Hủy bỏ',
        onOk() {
          //handleDeleteAppointment(appointmentDetail.appointmentId);
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    }
    //setAppointmentDetail({} as AppointmentPatient);
  };

  const check24h = () => {
    if (appointmentDate) {
      console.log('appointmentDate', appointmentDate);
      let appointmentDateTmp: any = moment(appointmentDate).format('YYYY-MM-DD');
      console.log('appointmentDateTmp', appointmentDateTmp);
      if (new Date(appointmentDateTmp) <= new Date()) {
        return true;
      } else if (appointmentDateTmp && txtKG) {
        var listTG = txtKG.split('-');
        console.log('listTG', listTG);
        if (listTG) {
          var time = listTG[0].replace('h', ':');
          var appointmentDateT =
            new Date(appointmentDateTmp).getFullYear() +
            '/' +
            (new Date(appointmentDateTmp).getMonth() + 1) +
            '/' +
            new Date(appointmentDateTmp).getDate();
          var date = new Date(appointmentDateT + ' ' + time);
          console.log(date);
          var hour = Math.abs(date.getTime() - new Date().getTime()) / 36e5;
          console.log('hour', hour);
          if (hour < 24) {
            return true;
          }
        }
      }
    }
    return false;
  };

  useEffect(() => {
    check24h();
  }, [appointmentDate, txtKG]);

  const submitConfirm = async () => {
    if (activeKey === 'NOTE') {
      openNotificationRight(
        'Sau khi nhập ghi chú, vui lòng chọn lại loại Lịch hẹn Tại nhà/Tại viện để lưu dữ liệu.',
        'warning'
      );
      return;
    }
    const values = await form.validateFields();
    let channelType = values['channelType'];
    let channel: Channel = channels.find(function(element: Channel) {
      return element.code === channelType;
    });
    const methodType = form.getFieldValue('methodType') ? form.getFieldValue('methodType') : 'HOME';
    delete values['parentFacilityId'];
    let location = '';
    if (methodType === 'HOME') {
      if (values.facilityId) {
        location = getCodeFactility(values.facilityId, false);
      } else {
        location = getCodeFactility(values.examFacilityId, true);
      }
    }
    delete values['facilityId'];
    delete values['assignStaffName'];
    let streetId = values['streetId']?.split('***')[0];
    let routeId = values['streetId']?.split('***')[1];
    delete values['confirm'];
    delete values['appointmentNotes'];
    if (values.healthDeclaration && values?.healthDeclaration?.invoice?.length > 0) {
      values.healthDeclaration.typeInvoice = typeInvoice;
      values.healthDeclaration.invoice = 1;
      values.healthDeclaration.printName = typeInvoice == '1' && values.healthDeclaration.printName.length > 0 ? '' : 1;
    } else if (values.healthDeclaration && values?.healthDeclaration?.invoice?.length <= 0) {
      values.healthDeclaration.invoice = '';
    }
    onModify({
      id: appointment?.id,
      ...values,
      methodType,
      appointmentDate: (values.appointmentDate as moment.Moment)?.format('DD/MM/YYYY'),
      streetId,
      routeId,
      assignStaffId: appointment?.assignStaffId,
      channelId: channel?.id,
      channelType,
      originalAmount: form.getFieldValue('originalAmount'),
      totalAmount: form.getFieldValue('totalAmount'),
      healthDeclaration:
        methodType === 'HOME'
          ? {
              ...values.healthDeclaration,
              prognostic: values.healthDeclaration?.prognostic.toString(),
              location: location,
              purpose: 'Khám bệnh',
              dateOnset: values.healthDeclaration?.dateOnset
                ? moment(values.healthDeclaration?.dateOnset).format('DD/MM/YYYY')
                : '',
              dateHascovid: values.healthDeclaration?.dateHascovid
                ? moment(values.healthDeclaration?.dateHascovid).format('DD/MM/YYYY')
                : ''
            }
          : {
              ...values.healthDeclaration,
              prognostic: values.healthDeclaration?.prognostic.toString(),
              location: getCodeFactility(values.examFacilityId, true, true),
              purpose: 'Khám bệnh',
              dateOnset: values.healthDeclaration?.dateOnset
                ? moment(values.healthDeclaration?.dateOnset).format('DD/MM/YYYY')
                : '',
              dateHascovid: values.healthDeclaration?.dateHascovid
                ? moment(values.healthDeclaration?.dateHascovid).format('DD/MM/YYYY')
                : ''
            },
      services: values.services
        ? values.services?.map(val => ({
            objectId: val.id,
            originalPrice: val.originalPrice,
            salePrice: val.salePrice
            /*indicatedDoctorId: JSON.parse(String(localStorage.getItem('user')))
              ? JSON.parse(String(localStorage.getItem('user')))['id']
              : ''*/
          }))
        : undefined,
      packages: values.packages?.id
        ? {
            objectId: values.packages?.id,
            originalPrice: form.getFieldValue('originalAmount'),
            salePrice: form.getFieldValue('totalAmount')
          }
        : undefined,
      patient: {
        ...values.patient,
        name: values.patient?.name?.trim(),
        birthDate: (values.patient?.birthDate as moment.Moment)?.format('DD/MM/YYYY'),
        birthYear: values.patient?.birthYear
          ? (values.patient?.birthYear as moment.Moment)?.format('YYYY')
          : (values.patient?.birthDate as moment.Moment)?.format('YYYY')
      },
      pid: values.patient?.pid,
      appointmentType: values.packages?.id ? 'PACKAGE' : 'SCHEDULE'
    });
    setConfirmLoading(true);
    setTimeout(() => {
      setConfirmLoading(false);
    }, 5000);
  };

  return (
    <Modal
      maskClosable={false}
      style={{ marginTop: 5 }}
      centered
      width={1500}
      title="Cập nhật Lịch hẹn"
      visible={visible}
      okText="Lưu thông tin"
      cancelText="Hủy"
      onOk={onSubmit}
      confirmLoading={confirmLoading}
      onCancel={() => {
        onCancel();
      }}
      okButtonProps={{
        disabled: functionOb[functionCodeConstants.TD_LH_SUALH] && !confirmLoading ? false : true,
        color: '#26a661'
      }}
    >
      <Spin tip="Đang xử lý..." spinning={isLoading}>
        <Modal
          maskClosable={false}
          style={{ marginTop: 5 }}
          centered
          width={1000}
          title="Phân lịch hẹn"
          visible={visibleAssigAppointment}
          onCancel={() => {
            setVisibleAssigAppointment(false);
          }}
        >
          <AssignmentPage
            appointmentId={appointment?.id}
            facilityId={''}
            appointmentDate={appointmentDate !== '' ? appointmentDate : appointment?.appointmentDate}
            parentFacilityId={appointment?.examFacilityId ? appointment?.examFacilityId : ''}
          />
        </Modal>
        <div style={{ float: 'left', color: 'red', fontWeight: 'bold' }}></div>
        <div style={{ float: 'right' }}>
          {/*<Button icon={<AppstoreAddOutlined />} style={{ marginLeft: 10 }} type="primary">
                 Đặt nhiều lịch
                </Button>*/}
        </div>
        <PageHeader
          ghost={false}
          // tags=
          //   subTitle={
          //     <span>
          //       <span className="tt">
          //         <span style={{ color: 'black' }}>{'Trạng thái: '}</span>
          //         <span>{getStatus(appointment?.status)}</span>
          //       </span>
          //       <span className="kd">
          //         <span style={{ color: 'black', marginLeft: '7px' }}>{'Mã lịch hẹn: '}</span>
          //         <span style={{ color: 'red', marginRight: 15 }}>
          //           {appointment?.appointmentHisId ? appointment?.appointmentHisId : 'Chưa có mã lịch hẹn'}
          //         </span>
          //       </span>
          //     </span>
          //   }
          extra={[
            // <Button
            //   disabled={functionOb[functionCodeConstants.TD_LH_CHUYEN_LOAI_LICH] && !confirmLoading ? false : true}
            //   onClick={() => {
            //     handleMenuClick(1);
            //   }}
            //   icon={<ArrowRightOutlined />}
            //   type="primary"
            // >
            //   Chuyển loại lịch
            // </Button>,
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_HUY] && !confirmLoading ? false : true}
              onClick={() => {
                handleMenuClick(2);
              }}
              icon={<CloseOutlined />}
              type="primary"
              danger
              key="2"
            >
              Hủy lịch
            </Button>,
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_DATNHIEU] && !confirmLoading ? false : true}
              onClick={() => {
                handleMenuClick(3);
              }}
              icon={<OrderedListOutlined />}
              key="3"
            >
              Đặt nhiều lịch
            </Button>,
            <Button
              onClick={() => {
                handleMenuClick(4);
              }}
              icon={<StepForwardOutlined />}
              key="4"
              className="btn-green"
              type="primary"
              disabled={
                functionOb[functionCodeConstants.TD_LH_CBS] && !confirmLoading && appointment?.status !== 13
                  ? false
                  : true
              }
            >
              Chuyển BS tư vấn
            </Button>,
            <Button
              onClick={() => {
                onSubmit();
              }}
              icon={<SaveOutlined />}
              key="5"
              type="primary"
              disabled={functionOb[functionCodeConstants.TD_LH_SUALH] && !confirmLoading ? false : true}
            >
              Lưu thông tin
            </Button>
          ]}
        ></PageHeader>
        <div style={{ clear: 'both' }}></div>
        <Form {...formItemLayout} className="ant-advanced-add-form" style={{ marginTop: 5 }}>
          <Col className="row hidden-over" span={12}>
            <Tabs
              defaultActiveKey={methodType}
              activeKey={methodType}
              type="card"
              onTabClick={activeKey => {
                setActiveKey(activeKey);
                setMethodType(activeKey);
                if (activeKey === 'HOME') {
                  if (form.getFieldValue(['patient', 'provinceId'])) {
                    loadFacilitiesHome({
                      variables: {
                        page: 1,
                        filtered: [
                          {
                            id: 'isHome',
                            value: '1',
                            operation: '=='
                          },
                          {
                            id: 'provinceIds',
                            value: form.getFieldValue(['patient', 'provinceId']),
                            operation: '~'
                          },
                          {
                            id: 'status',
                            value: '1',
                            operation: '=='
                          }
                        ]
                      }
                    });
                  }
                } else if (activeKey === 'HOSPITAL') {
                  if (form.getFieldValue(['patient', 'provinceId'])) {
                    loadFacilitiesHos({
                      variables: {
                        page: 1,
                        filtered: [
                          {
                            id: 'isHospital',
                            value: '1',
                            operation: '=='
                          },
                          {
                            id: 'status',
                            value: '1',
                            operation: '=='
                          }
                        ]
                      }
                    });
                  }
                }
                form.setFieldsValue({
                  methodType: activeKey
                });
                form.setFieldsValue({
                  examFacilityId: undefined,
                  sendFacilityId: undefined,
                  specialistId: undefined,
                  workTimeId: undefined,
                  appointmentDate: moment(),
                  streetId: undefined,
                  routeId: undefined,
                  reasonNote: undefined,
                  appointmentObject: undefined,
                  routeName: undefined,
                  facilityId: undefined
                });
              }}
              destroyInactiveTabPane={true}
            >
              <TabPane
                tab={
                  <span>
                    <BankOutlined />
                    Đặt lịch tại viện
                  </span>
                }
                key="HOSPITAL"
              >
                <Row>
                  <AppointmentDate
                    isHospital={false}
                    onChange={(value: any, dateString: any) => {}}
                    handleChangAppointmentDate={(examFacilityId, appointmentDate) => {
                      setAppointmentDate(appointmentDate);
                      if (appointmentDate && examFacilityId) {
                        loadAppointmentWorkTimeHome({
                          variables: {
                            data: {
                              appointmentDate,
                              examFacilityId
                            }
                          }
                        });
                      } else {
                        //
                      }
                    }}
                  />
                  <MedicalPlace
                    isLoading={isLoadingFacilitiesHos}
                    facilityList={facilitiesHos}
                    methodType={'HOSPITAL'}
                    handleChangeMedicalPlace={(examFacilityId, appointmentDate) => {
                      if (examFacilityId) {
                        loadAllWorkTimeFacility({
                          variables: {
                            facilityId: examFacilityId
                          }
                        });
                      }
                      if (appointmentDate && examFacilityId) {
                        loadAppointmentWorkTimeHospital({
                          variables: {
                            data: {
                              appointmentDate,
                              examFacilityId
                            }
                          }
                        });
                      } else {
                        loadAppointmentWorkTimeHospital({
                          variables: {
                            data: {
                              appointmentDate: moment().format('YYYY-MM-DD'),
                              examFacilityId
                            }
                          }
                        });
                      }
                    }}
                  />
                  <AppointmentWorkTime
                    isLoading={isLoadingAllWorkTimeFacility}
                    appointmentWorkTime={allWorkTimeFacility}
                    setTxt={setTxtKG}
                  />
                  <AppointmentObject
                    handleChange={(value: boolean) => {
                      setIsBHYT(value);
                    }}
                    appointmentObjects={{}}
                  />
                  <ObjectSend isLoading={isLoadingFacilitiesObjectSend} facilityList={facilitiesObjectSend} />
                  <Specialists isLoading={isLoadingSpecialists} specialists={specialists} />
                  <AppointmentSource
                    isLoading={isLoadingChannels}
                    onChangeAppointmentSource={value => {}}
                    channelList={channels}
                  />
                  <ReasonBooking isLoading={isLoadingReasons} reasonList={reasons} />
                  <Col className="gutter-row" span={24}>
                    <AppointmentTimeFrame
                      appointmentWorkTimeData={
                        form.getFieldValue('examFacilityId') ? appointmentWorkTimeHospital?.['data'] : []
                      }
                    />
                  </Col>
                </Row>
              </TabPane>
              <TabPane
                tab={
                  <span>
                    <HomeOutlined />
                    Đặt lịch tại nhà
                  </span>
                }
                key="HOME"
              >
                <Row>
                  {/*<Provinces
                    isLoading={isLoadingProvince}
                    handleChangeProvince={provinceId => {
                      if (provinceId) {
                        // if (methodType === 'HOME') {
                        loadFacilitiesHome({
                          variables: {
                            page: 1,
                            filtered: [
                              {
                                id: 'isHome',
                                value: '1',
                                operation: '=='
                              },
                              {
                                id: 'provinceIds',
                                value: provinceId,
                                operation: '~'
                              },
                              {
                                id: 'status',
                                value: '1',
                                operation: '=='
                              }
                            ]
                          }
                        });
                        // } else if (methodType === 'HOSPITAL') {
                        //     loadFacilitiesHos({
                        //         variables: {
                        //             page: 1,
                        //             filtered: [
                        //                 {
                        //                     id: 'isHospital',
                        //                     value: '1',
                        //                     operation: '=='
                        //                 },
                        //                 {
                        //                     id: 'provinceIds',
                        //                     value: provinceId,
                        //                     operation: '~'
                        //                 },
                        //                 {
                        //                     id: 'status',
                        //                     value: '1',
                        //                     operation: '=='
                        //                 }
                        //             ]
                        //         }
                        //     });
                        // }
                        loadDistricts({
                          variables: {
                            provinceId: provinceId
                          }
                        });
                        loadStreets({
                          variables: {
                            page: 1,
                            filtered: [
                              {
                                id: 'provinceId',
                                value: provinceId,
                                operation: '=='
                              },
                              {
                                id: 'status',
                                value: '1',
                                operation: '=='
                              }
                            ]
                          }
                        });
                      }
                    }}
                    provinceList={provinces}
                  />*/}
                  <AppointmentDate
                    isHospital={false}
                    onChange={(value: any, dateString: any) => {}}
                    handleChangAppointmentDate={(examFacilityId, appointmentDate) => {
                      console.log('appointmentDate', appointmentDate);
                      setAppointmentDate(appointmentDate);
                      if (appointmentDate && examFacilityId) {
                        loadAppointmentWorkTimeHome({
                          variables: {
                            data: {
                              appointmentDate,
                              examFacilityId
                            }
                          }
                        });
                      } else {
                        //
                      }
                    }}
                  />
                  <AppointmentWorkTime
                    isLoading={isLoadingAllWorkTimeFacility}
                    //appointmentWorkTime={form.getFieldValue('examFacilityId') ? allWorkTimeFacility : []}
                    appointmentWorkTime={worktimes}
                    setTxt={setTxtKG}
                  />
                  <Street4Home
                    isLoading={isLoadingStreets}
                    streetList={form.getFieldValue(['patient', 'provinceId']) ? streets : []}
                    routeId={appointment?.routeId}
                    handleChangeStreet={handleChangeStreet}
                  />
                  <Route street={streetId} routesData={routesData} routeId={appointment?.routeId} />
                  <FacilityParent
                    isLoading={isLoadingFacilitiesHome}
                    facilityParentList={form.getFieldValue(['patient', 'provinceId']) ? facilitiesHome : []}
                    handleChangeFacilityParent={(parentFacilityId, appointmentDate) => {
                      if (parentFacilityId) {
                        loadAllWorkTimeFacility({
                          variables: {
                            facilityId: parentFacilityId
                          }
                        });
                      }
                      if (appointmentDate && parentFacilityId) {
                        //setAppointmentDate(appointmentDate);
                        loadAppointmentWorkTimeHome({
                          variables: {
                            data: {
                              appointmentDate,
                              examFacilityId: parentFacilityId
                            }
                          }
                        });
                      } else {
                        loadAppointmentWorkTimeHome({
                          variables: {
                            data: {
                              appointmentDate: moment().format('YYYY-MM-DD'),
                              examFacilityId: parentFacilityId
                            }
                          }
                        });
                      }
                    }}
                  />
                  <Facility facilityList={facilities} handleChangeFacility={() => {}} isLoading={false} />
                  <AppointmentSource
                    isLoading={isLoadingChannels}
                    onChangeAppointmentSource={value => {}}
                    channelList={channels}
                  />
                  <ReasonBooking isLoading={isLoadingReasons} reasonList={reasons} />
                  {/*<AssignStaff isLoadingSiteStaffsAssign={isLoadingSiteStaffs} siteStaffs={siteStaffs} />*/}
                  <Col className="gutter-row" span={24}>
                    <AppointmentTimeFrame
                      appointmentWorkTimeData={
                        form.getFieldValue('examFacilityId') ? appointmentWorkTimeHome?.['data'] : []
                      }
                    />
                  </Col>
                  {/*<div className="content">
                    <MgsContent
                      provinces={provinces}
                      districts={districts}
                      wards={wards}
                      workTimes={appointmentWorkTimeHospital?.['data']}
                      onAssignAppointment={() => { }}
                      functionOb={functionOb}
                    />
                  </div>*
                    <AssignStaff />*/}
                </Row>
              </TabPane>
            </Tabs>
            <div className="mt-14px-sub">
              <Tabs>
                <TabPane
                  tab={
                    <span>
                      <HighlightOutlined />
                      Ghi chú lịch hẹn
                    </span>
                  }
                  key="NOTE1"
                >
                  <AppointmentNotes
                    onSaveComment={values => {
                      handleSaveAppointmentComment({
                        data: {
                          appointmentId: appointment?.id,
                          ...values
                        }
                      });
                    }}
                  />
                  <Col className="gutter-row" span={24}>
                    <AppointmentNotesTable
                      appointmentComments={appointmentComments}
                      onDelete={id => {
                        handleDeleteAppointmentComment({ id });
                      }}
                    />
                  </Col>
                </TabPane>
                <TabPane
                  tab={
                    <span>
                      <HistoryOutlined />
                      Lịch sử
                    </span>
                  }
                  key="HISTORY"
                >
                  <Col className="gutter-row" span={24}>
                    <AppointmentActions appointmentId={appointment?.id} />
                  </Col>
                </TabPane>
              </Tabs>
              <TestNotes />
            </div>
          </Col>
          <Col className="row" span={12}>
            <Tabs className="tabs-custom">
              <TabPane
                tab={
                  <span
                    className={
                      appointment?.healthDeclaration?.customerLabel == 'Xanh'
                        ? 'green-kbyt'
                        : appointment?.healthDeclaration?.customerLabel == 'Đỏ'
                        ? 'red-kbyt'
                        : appointment?.healthDeclaration?.customerLabel == 'Vàng'
                        ? 'yellow-kbyt'
                        : 'blue-kbyt'
                    }
                  >
                    KH 1
                  </span>
                }
                key="TAB1"
              >
                <Row>
                  <Status statusItem={appointment?.status} />
                  <CodeAppoiment
                    code={appointment?.appointmentHisId ? appointment?.appointmentHisId : 'Chưa có mã lịch hẹn'}
                  />
                  <PhoneNumber
                    handleChangeProvince={provinceId => {
                      if (provinceId) {
                        if (methodType === 'HOME') {
                          loadFacilitiesHome({
                            variables: {
                              page: 1,
                              filtered: [
                                {
                                  id: 'isHome',
                                  value: '1',
                                  operation: '=='
                                },
                                {
                                  id: 'provinceIds',
                                  value: provinceId,
                                  operation: '~'
                                },
                                {
                                  id: 'status',
                                  value: '1',
                                  operation: '=='
                                }
                              ]
                            }
                          });
                        } else if (methodType === 'HOSPITAL') {
                          loadFacilitiesHos({
                            variables: {
                              page: 1,
                              filtered: [
                                {
                                  id: 'isHospital',
                                  value: '1',
                                  operation: '=='
                                },
                                {
                                  id: 'provinceIds',
                                  value: provinceId,
                                  operation: '~'
                                },
                                {
                                  id: 'status',
                                  value: '1',
                                  operation: '=='
                                }
                              ]
                            }
                          });
                        }
                        loadDistricts({
                          variables: {
                            provinceId: provinceId
                          }
                        });
                        loadStreets({
                          variables: {
                            page: 1,
                            filtered: [
                              {
                                id: 'provinceId',
                                value: provinceId,
                                operation: '=='
                              },
                              {
                                id: 'status',
                                value: '1',
                                operation: '=='
                              }
                            ]
                          }
                        });
                      }
                    }}
                    handleChangePatient={handleChangePatient}
                  />
                  <PID patientList={patients} />
                  <CustomerFullname />
                  <DateofBirth />
                  <Gender />
                  <ContactAddress />
                  <Provinces
                    isLoading={isLoadingProvince}
                    handleChangeProvince={provinceId => {
                      if (provinceId) {
                        if (methodType === 'HOME') {
                          loadStreets({
                            variables: {
                              page: 1,
                              filtered: [
                                {
                                  id: 'provinceId',
                                  value: provinceId,
                                  operation: '=='
                                },
                                {
                                  id: 'status',
                                  value: '1',
                                  operation: '=='
                                }
                              ]
                            }
                          });
                          form.setFieldsValue({
                            facilityId: '',
                            examFacilityId: ''
                          });
                        }
                        loadDistricts({
                          variables: {
                            provinceId: provinceId
                          }
                        });
                      }
                    }}
                    provinceList={provinces}
                  />
                  <Districts
                    isLoading={isLoadingDistricts}
                    handleChangeDistrict={districtId => {
                      if (districtId !== '') {
                        loadWards({
                          variables: {
                            districtId: districtId
                          }
                        });
                      }
                    }}
                    districtList={form.getFieldValue(['patient', 'provinceId']) ? districts : []}
                  />
                  <Wards
                    isLoading={isLoadingWards}
                    handleChangeWard={() => {}}
                    wardList={form.getFieldValue(['patient', 'districtId']) ? wards : []}
                  />
                  <IdentityCard />
                  <Email />
                  <Nationality
                    isLoading={isLoadingCountries}
                    countries={pagingCountries?.data}
                    value={appointment?.patient?.nationality}
                    setPatternIdNo={setPatternIdNo}
                  />
                  <ReasonBookingStr />
                  <SetForObject />
                  <ScheduleReporter
                    isLoading={isLoadingSiteStaffs}
                    siteStaffList={siteStaffs}
                    handleChangeSiteStaff={() => {}}
                  />
                  <CustomerType />
                  <CollaboratorDoctor appointmentStatus={appointment?.status} functionOb={functionOb} doctor={doctor} />
                  <MedicalExaminationPackage
                    functionOb={functionOb}
                    appointmentStatus={appointment?.status}
                    status={appointment?.status}
                    methodType={activeKey}
                    provinceId={
                      activeKey === 'HOSPITAL'
                        ? facilitiesHos?.find(facility => facility.id === form.getFieldValue('examFacilityId'))
                            ?.provinceId
                        : form.getFieldValue(['patient', 'provinceId'])
                    }
                    packageObj={packageDetail}
                  />
                  <Test
                    appointmentStatus={appointment?.status}
                    functionOb={functionOb}
                    status={appointment?.status}
                    services={appointment?.services}
                    facilityList={methodType === 'HOSPITAL' ? facilitiesHos : facilitiesHome}
                  />
                  <BHYT isRequired={isBHYT} />
                  <GeneticCode />
                  {check24h() && (
                    <>
                      <div className="kbyt-title">Khai báo y tế dành cho khách hàng đăng ký khám</div>
                      <div className="kbyt-sub-title">
                        <p>* Phiếu khai báo thông tin y tế chỉ có giá trị trong vòng 24 giờ </p>
                        <p>
                          * Cảnh báo: Khai báo thông tin sai là vi phạm pháp luật Việt Nam, có thể bị xử lý hình sự!
                        </p>
                      </div>
                      <ReasonCheckup medthodType={activeKey} isDisabled={false} />
                      <TypeCustomer />
                      <DateOnset />
                      <Prognostic />
                      <div className="">
                        <span className="red">* </span>
                        <b>Mục B. Yếu tố dịch tễ nếu có</b>
                      </div>
                      <HumanHasCovid />
                      <StreetHasCovid />
                      <TestedCovid />
                      <div className="">
                        <span className="red">* </span>
                        <b>Mục C. CÁC THÔNG TIN KHÁC: Hãy tích vào ô trống trước phần lựa chọn</b>
                      </div>
                      <IsCovid isCovided={isCovided} setIsCovided={setIsCovided} field={{}} />
                      <TowInjections field={{}} />
                      <div className="kbyt-confirm">
                        <Form.Item name={'confirm'} rules={[{ required: true, message: 'Không được bỏ trống!' }]}>
                          <CheckboxGroup value={['1']} name="confirm">
                            <Checkbox value="1" indeterminate={false}>
                              <span className="red" style={{ fontWeight: 500 }}>
                                Tôi cam kết đúng thông tin *
                              </span>
                            </Checkbox>
                          </CheckboxGroup>
                        </Form.Item>
                      </div>
                      {/*<Invoice setIsInvoice={setIsInvoice} />*/}
                      {isInvoice && (
                        <Tabs
                          defaultActiveKey={typeInvoice}
                          activeKey={typeInvoice}
                          type="card"
                          // onTabClick={activeKey => {
                          //     setTypeInvoice(activeKey);
                          //     if (typeInvoice == "0") {
                          //         form.setFieldsValue({
                          //             healthDeclaration: {
                          //                 printName: undefined,
                          //                 nameCompany: undefined,
                          //                 addressCompany: undefined,
                          //                 taxCode: undefined,
                          //                 emailCompany: undefined
                          //             }
                          //         })
                          //     } else {
                          //         form.setFieldsValue({
                          //             healthDeclaration: {
                          //                 emailProfile: undefined,
                          //                 addressProfile: undefined
                          //             }
                          //         })
                          //     }

                          // }}
                        >
                          <TabPane disabled tab={<span>Cá nhân</span>} key="0">
                            <Row>
                              <AddressProfile required={typeInvoice == '0' ? true : false} />
                              <EmailProfile required={typeInvoice == '0' ? true : false} />
                            </Row>
                          </TabPane>
                          <TabPane disabled tab={<span>Công ty</span>} key="1">
                            <Row>
                              <NameCompany required={false} />
                              <TaxCode required={false} />
                              <AddressCompany required={false} />
                              <EmailCompany required={false} />
                              <PrintName />
                            </Row>
                          </TabPane>
                        </Tabs>
                      )}
                    </>
                  )}
                </Row>
              </TabPane>
            </Tabs>
          </Col>
        </Form>
      </Spin>
    </Modal>
  );
};

export default AppoimentDialogUpdate;
