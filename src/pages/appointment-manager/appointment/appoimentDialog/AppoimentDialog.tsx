import {
  BankOutlined,
  HighlightOutlined,
  HistoryOutlined,
  HomeOutlined,
  SaveOutlined,
  StepForwardOutlined
} from '@ant-design/icons';
import { Button, Checkbox, Col, Modal, PageHeader, Row, Spin, Tabs } from 'antd';
import { functionCodeConstants } from 'constants/functions';
import React, { useEffect, useState } from 'react';
import { FC } from 'react';
import './../index.less';
import useGetAddAppointmentForm from '../useGetAddAppointmentForm';
import { useGetAllWorkTimeFacility } from 'hooks/worktimes/useGetAllWorkTimeFacility';
import { useGetFacilitys } from 'hooks/facility';
import { useGetAppointmentWorkTimeHome, useGetAppointmentWorkTimeHospital } from 'hooks/appointment';
import moment from 'moment';
import { useGetSpecialist } from 'hooks/specialist/useGetSpecialist';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { useGetStreetsLazy } from 'hooks/street';
import { useGetRoutesLazy } from 'hooks/route';
import { useGetWards } from 'hooks/ward/useGetWard';
import { Patient } from 'common/interfaces/patient.interface';
import { useGetPatients } from 'hooks/patient';
import { useGetAllCountriesLazy } from 'hooks/appointment/useGetAllCountries';
import { useGetSiteStaffsLazy } from 'hooks/site-staff';
import { useGetChannels } from 'hooks/channel';
import { Channel } from 'common/interfaces/channel.interface';
import { useGetReasons } from 'hooks/reasons/useGetReasons';
import { AppointmentInput } from 'common/interfaces/input/appointmentInput.interfce';
import { Appointment } from 'common/interfaces/appointment.interface';
import AppointmentTabs from '../appointmentTabs';
import { useGetAllWorktimes } from 'hooks/worktimes/useGetWorkTimes';
import { FilteredInput } from 'common/interfaces/filtered.interface';
const TabPane = Tabs.TabPane;
const CheckboxGroup = Checkbox.Group;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};

interface AppointmentDialogProps {
  visible: boolean;
  onCreate: (values: Partial<AppointmentInput>) => void;
  onCreateAndForward: (values: Partial<AppointmentInput>) => void;
  onCancel: () => void;
  functionOb: any;
  setIsLoading: any;
  isLoading: boolean;
  defaultValue?: Partial<Appointment>;
  // channels: any;
  // isLoadingChannels: boolean;
}
const AppointmentDialog: FC<AppointmentDialogProps> = ({
  visible,
  onCreate,
  onCreateAndForward,
  onCancel,
  functionOb,
  setIsLoading,
  isLoading,
  defaultValue
  // channels,
  // isLoadingChannels
}) => {
  const [disabledField, setDisabledField] = useState(false);
  const [appointmentCache, setAppointmentCache]: any = useState();
  const [methodType, setMethodType] = useState('HOSPITAL');
  const [activeKey, setActiveKey] = useState('HOSPITAL');
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [isBHYT, setIsBHYT] = useState(false);
  const [appointmentDate, setAppointmentDate]: any = useState(new Date());
  const [txtKG, setTxtKG]: any = useState();
  const [listCheck, setListCheck]: any[] = useState([16]);
  const [isInvoice, setIsInvoice] = useState(false);
  const [typeInvoice, setTypeInvoice] = useState('0');
  const [isCovided, setIsCovided] = useState(false);
  //Hook Lấy khung giờ theo đơn vị
  const { allWorkTimeFacility, loadAllWorkTimeFacility, isLoadingAllWorkTimeFacility } = useGetAllWorkTimeFacility();
  //Hook Lấy danh sách nơi khám tại nhà
  const {
    facilities: facilitiesHome,
    isLoadingFacilities: isLoadingFacilitiesHome,
    loadFacilities: loadFacilitiesHome
  } = useGetFacilitys();
  const {
    facilities: facilitiesObjectSend,
    isLoadingFacilities: isLoadingFacilitiesObjectSend,
    loadFacilities: loadFacilitiesObjectSend
  } = useGetFacilitys();
  useEffect(() => {
    if (facilitiesHome?.length > 0) {
      form.setFieldsValue({
        examFacilityId: facilitiesHome[0]?.id
      });
    } else {
      form.setFieldsValue({
        examFacilityId: ''
      });
    }
  }, [facilitiesHome]);

  //Hook Lấy danh sách khung giờ tại nhà
  const {
    appointmentWorkTimeHome,
    isLoadingAppointmentWorkTimeHome,
    loadAppointmentWorkTimeHome
  } = useGetAppointmentWorkTimeHome();
  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //Hook Lấy danh sách đơn vị
  const { facilities, loadFacilities, isLoadingFacilities } = useGetFacilitys();
  useEffect(() => {
    if (methodType === 'HOME') {
      if (facilities?.length > 0) {
        form.setFieldsValue({
          facilityId: facilities[0]?.id
        });
      } else {
        form.setFieldsValue({
          facilityId: ''
        });
      }
    }
  }, [facilities]);
  //Hook Lấy danh sách Quận/Huyện
  const { districts, isLoadingDistricts, loadDistricts } = useGetDistricts();
  //Hook Lấy danh sách Chuyên khoa khám
  const { specialists, isLoadingSpecialists, loadSpecialist } = useGetSpecialist();
  //Hook Lấy danh sách đường phố
  const { streets, isLoadingStreets, loadStreets } = useGetStreetsLazy();
  //Lấy nguồn lịch hẹn
  const { channels, isLoadingChannels, loadChannels } = useGetChannels();
  //Hook Lấy danh sách Xã/Phường
  const { wards, isLoadingWards, loadWards } = useGetWards();
  //Hook Lấy danh sách Mã thẻ KH (PID)
  const { patients, isLoadingPatients, loadPatients } = useGetPatients();
  // Hook Lấy danh sách Cán bộ báo lịch
  const { siteStaffs, isLoadingSiteStaffs, loadSiteStaffs } = useGetSiteStaffsLazy();
  //Hook Lấy danh sách quốc tịch
  const { pagingCountries, isLoadingCountries, loadCountries, errorCountries } = useGetAllCountriesLazy();
  //Hook Lấy danh lý do
  const { reasons, isLoadingReasons, loadReasons } = useGetReasons();
  //Hook lấy chi tiết cung đường
  const { loadRoutes, isLoadingRoutes, routesData } = useGetRoutesLazy();
  //Lấy danh sách khung giờ
  const { worktimes, isLoadingWorktimes, loadWorktimes } = useGetAllWorktimes();
  //Hook Lấy danh sách CB được phân lịch
  const {
    siteStaffs: siteStaffsAssign,
    isLoadingSiteStaffs: isLoadingSiteStaffsAssign,
    loadSiteStaffs: loadSiteStaffsAssign
  } = useGetSiteStaffsLazy();
  useEffect(() => {
    loadWorktimes({
      variables: {
        type: 'STAFF'
      }
    });
    if (localStorage.getItem('appoiment-create-data')) {
      let appointment: Partial<Appointment> = JSON.parse(localStorage.getItem('appoiment-create-data') || '{}');
      let appointmentDate = moment(appointment?.appointmentDate);
      setAppointmentCache({
        ...appointment,
        appointmentDate: appointmentDate,
        patient: {
          ...appointment?.patient,
          birthDate: moment(appointment?.patient?.birthDate),
          birthYear: moment(appointment?.patient?.birthYear)
        }
      });
      if (appointment?.patient?.provinceId) {
        loadDistricts({
          variables: {
            provinceId: appointment?.patient?.provinceId
          }
        });
      }
      if (appointment?.patient?.districtId) {
        loadWards({
          variables: {
            districtId: appointment?.patient?.districtId
          }
        });
      }
      if (appointment?.examFacilityId) {
        loadAllWorkTimeFacility({
          variables: {
            facilityId: appointment?.examFacilityId
          }
        });
        if (appointment?.methodType === 'HOME') {
          loadAppointmentWorkTimeHome({
            variables: {
              data: {
                appointmentDate,
                examFacilityId: appointment?.examFacilityId
              }
            }
          });
        } else {
          loadAppointmentWorkTimeHospital({
            variables: {
              data: {
                appointmentDate,
                examFacilityId: appointment?.examFacilityId
              }
            }
          });
        }
      }
      form.setFieldsValue({ confirm: appointment?.confirm ? ['1'] : [''] });
    }
  }, []);
  useEffect(() => {
    if (appointmentCache) {
      if (appointmentCache.streetId && streets?.length > 0) {
        let streetId = appointmentCache?.streetId.split('***')[0];
        let street = streets?.find(item => item?.id === streetId);
        if (street?.['facilityId']) {
          loadFacilities({
            variables: {
              page: 1,
              filtered: [
                /*{
                  id: 'isHome',
                  value: '1',
                  operation: '=='
                },*/
                {
                  id: 'id',
                  value: street?.['facilityId'],
                  operation: '=='
                },
                {
                  id: 'status',
                  value: '1',
                  operation: '=='
                }
              ]
            }
          });
        } else {
          form.setFieldsValue({
            facilityId: ''
          });
        }
      }
    }
  }, [streets, appointmentCache]);
  useEffect(() => {
    form.resetFields();
    setComments([]);
    setIsInvoice(false);
    setTypeInvoice('0');
    setTxtKG('');
    setAppointmentDate(new Date());
  }, [visible]);

  useEffect(() => {
    if (defaultValue) {
      form.setFieldsValue(defaultValue);
    }
  }, [defaultValue]);
  const getCodeFactility = (examFacilityId, isParent, isHospital?) => {
    if (isHospital) {
      if (facilitiesHos && facilitiesHos.length > 0 && examFacilityId && methodType == 'HOSPITAL') {
        for (var i = 0; i < facilitiesHos.length; i++) {
          if (facilitiesHos[i].id == examFacilityId) {
            return facilitiesHos[i].code;
          }
        }
      }
    }
    if (isParent) {
      if (facilitiesHome && facilitiesHome.length > 0 && examFacilityId && methodType == 'HOME') {
        for (var i = 0; i < facilitiesHome.length; i++) {
          if (facilitiesHome[i].id == examFacilityId) {
            return facilitiesHome[i].code;
          }
        }
      }
    } else {
      if (facilities && facilities.length > 0 && examFacilityId && methodType == 'HOME') {
        for (var i = 0; i < facilities.length; i++) {
          if (facilities[i].id == examFacilityId) {
            return facilities[i].code;
          }
        }
      }
    }

    return null;
  };
  const onSubmit = async isForward => {
    setAppointmentCache(null);
    const values = await form.validateFields();
    setIsLoading(true);
    let channelType = values['channelType'];
    let channel: Channel = channels.find(function(element: Channel) {
      return element.code === channelType;
    });
    delete values['parentFacilityId'];
    let location = '';
    if (methodType === 'HOME') {
      if (values.facilityId) {
        location = getCodeFactility(values.facilityId, false);
      } else {
        location = getCodeFactility(values.examFacilityId, true);
      }
    }
    delete values['facilityId'];
    let streetId = values['streetId']?.split('***')[0];
    let routeId = values['streetId']?.split('***')[1];
    let examFacilityId = values.examFacilityId;
    let examFacilityCode = values['examFacilityId']?.split('***')[1];
    delete values['confirm'];
    delete values['appointmentNotes'];
    if (values.healthDeclaration && values?.healthDeclaration?.invoice?.length > 0) {
      values.healthDeclaration.typeInvoice = typeInvoice;
      values.healthDeclaration.invoice = 1;
      values.healthDeclaration.printName = typeInvoice == '1' && values.healthDeclaration.printName.length > 0 ? '' : 1;
    } else if (values.healthDeclaration && values?.healthDeclaration?.invoice?.length <= 0) {
      values.healthDeclaration.invoice = '';
    }
    if (isForward) {
      onCreateAndForward({
        ...values,
        methodType,
        comments: comments,
        streetId,
        routeId,
        channelId: channel?.id,
        channelType,
        appointmentDate: (values.appointmentDate as moment.Moment)?.format('DD/MM/YYYY'),
        services: values.services?.map(val => ({
          objectId: val.id,
          originalPrice: val.originalPrice,
          salePrice: val.salePrice
          /*indicatedDoctorId: JSON.parse(String(localStorage.getItem('user')))
                        ? JSON.parse(String(localStorage.getItem('user')))['id']
                        : ''*/
        })),
        packages: values.packages
          ? {
              objectId: values.packages?.id,
              originalPrice: form.getFieldValue('originalAmount'),
              salePrice: form.getFieldValue('totalAmount')
            }
          : undefined,
        patient: {
          ...values.patient,
          name: values.patient?.name?.trim(),
          birthDate: (values.patient?.birthDate as moment.Moment)?.format('DD/MM/YYYY'),
          birthYear: values.patient?.birthYear
            ? (values.patient?.birthYear as moment.Moment)?.format('YYYY')
            : (values.patient?.birthDate as moment.Moment)?.format('YYYY'),
          provinceId: values.patient?.provinceId?.split('***')[0],
          provinceCode: values.patient?.provinceId?.split('***')[1],
          districtId: values.patient?.districtId?.split('***')[0],
          districtCode: values.patient?.districtId?.split('***')[1]
        },
        pid: values.patient?.pid,
        appointmentType: values.packages?.id ? 'PACKAGE' : 'SCHEDULE',
        originalAmount: form.getFieldValue('originalAmount') ? Number(form.getFieldValue('originalAmount')) : undefined,
        totalAmount: form.getFieldValue('totalAmount') ? Number(form.getFieldValue('totalAmount')) : undefined,
        examFacilityId,
        examFacilityCode,
        healthDeclaration:
          methodType === 'HOME'
            ? {
                ...values.healthDeclaration,
                prognostic: values.healthDeclaration?.prognostic.toString(),
                location: location,
                purpose: 'Khám bệnh',
                dateOnset: values.healthDeclaration?.dateOnset
                  ? moment(values.healthDeclaration?.dateOnset).format('DD/MM/YYYY')
                  : '',
                dateHascovid: values.healthDeclaration?.dateHascovid
                  ? moment(values.healthDeclaration?.dateHascovid).format('DD/MM/YYYY')
                  : ''
              }
            : {
                ...values.healthDeclaration,
                location: getCodeFactility(values.examFacilityId, true, true),
                prognostic: values.healthDeclaration?.prognostic.toString(),
                purpose: 'Khám bệnh',
                dateOnset: values.healthDeclaration?.dateOnset
                  ? moment(values.healthDeclaration?.dateOnset).format('DD/MM/YYYY')
                  : '',
                dateHascovid: values.healthDeclaration?.dateHascovid
                  ? moment(values.healthDeclaration?.dateHascovid).format('DD/MM/YYYY')
                  : ''
              }
      });
    } else {
      onCreate({
        ...values,
        methodType,
        comments: comments,
        streetId,
        routeId,
        channelId: channel?.id,
        channelType,
        appointmentDate: (values.appointmentDate as moment.Moment)?.format('DD/MM/YYYY'),
        services: values.services?.map(val => ({
          objectId: val.id,
          originalPrice: val.originalPrice,
          salePrice: val.salePrice
          /*indicatedDoctorId: JSON.parse(String(localStorage.getItem('user')))
                        ? JSON.parse(String(localStorage.getItem('user')))['id']
                        : ''*/
        })),
        packages: values.packages
          ? {
              objectId: values.packages?.id,
              originalPrice: form.getFieldValue('originalAmount'),
              salePrice: form.getFieldValue('totalAmount')
            }
          : undefined,
        patient: {
          ...values.patient,
          name: values.patient?.name?.trim(),
          birthDate: (values.patient?.birthDate as moment.Moment)?.format('DD/MM/YYYY'),
          birthYear: values.patient?.birthYear
            ? (values.patient?.birthYear as moment.Moment)?.format('YYYY')
            : (values.patient?.birthDate as moment.Moment)?.format('YYYY'),
          provinceId: values.patient?.provinceId?.split('***')[0],
          provinceCode: values.patient?.provinceId?.split('***')[1],
          districtId: values.patient?.districtId?.split('***')[0],
          districtCode: values.patient?.districtId?.split('***')[1]
        },
        pid: values.patient?.pid,
        appointmentType: values.packages?.id ? 'PACKAGE' : 'SCHEDULE',
        originalAmount: form.getFieldValue('originalAmount') ? Number(form.getFieldValue('originalAmount')) : undefined,
        totalAmount: form.getFieldValue('totalAmount') ? Number(form.getFieldValue('totalAmount')) : undefined,
        examFacilityId,
        examFacilityCode,
        healthDeclaration:
          methodType === 'HOME'
            ? {
                ...values.healthDeclaration,
                prognostic: values.healthDeclaration?.prognostic.toString(),
                location: location,
                purpose: 'Khám bệnh',
                dateOnset: values.healthDeclaration?.dateOnset
                  ? moment(values.healthDeclaration?.dateOnset).format('DD/MM/YYYY')
                  : '',
                dateHascovid: values.healthDeclaration?.dateHascovid
                  ? moment(values.healthDeclaration?.dateHascovid).format('DD/MM/YYYY')
                  : ''
              }
            : {
                ...values.healthDeclaration,
                prognostic: values.healthDeclaration?.prognostic.toString(),
                location: getCodeFactility(values.examFacilityId, true, true),
                purpose: 'Khám bệnh',
                dateOnset: values.healthDeclaration?.dateOnset
                  ? moment(values.healthDeclaration?.dateOnset).format('DD/MM/YYYY')
                  : '',
                dateHascovid: values.healthDeclaration?.dateHascovid
                  ? moment(values.healthDeclaration?.dateHascovid).format('DD/MM/YYYY')
                  : ''
              }
      });
    }
    if (disabledField) {
      setDisabledField(false);
    }
    if (isCovided) {
      setIsCovided(false);
    }
  };
  const handleSaveData = async () => {
    const values = await form.getFieldsValue(true);
    localStorage.setItem(
      'appoiment-create-data',
      JSON.stringify({ ...values, methodType: form.getFieldValue('methodType') })
    );
  };
  useEffect(() => {
    if (routesData) {
      form.setFieldsValue({
        routeId: routesData[0]?.name
      });
    }
    return () => {
      handleSaveData();
    };
  }, [routesData]);
  //Hook Lấy danh sách khung giờ tại viện
  const {
    appointmentWorkTimeHospital,
    isLoadingAppointmentWorkTimeHospital,
    loadAppointmentWorkTimeHospital
  } = useGetAppointmentWorkTimeHospital();
  //Hook Lấy danh sách nơi khám tại viện
  const {
    facilities: facilitiesHos,
    isLoadingFacilities: isLoadingFacilitiesHos,
    loadFacilities: loadFacilitiesHos
  } = useGetFacilitys();
  // Nội dung Ghi chú
  const [comments, setComments] = useState([{} as { content: string }]);
  useEffect(() => {
    loadChannels();
    loadCountries();
    loadPatients({
      variables: {
        page: 1
      }
    });
    loadFacilitiesHos({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'isHospital',
            value: '1',
            operation: '=='
          },
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
    loadFacilitiesObjectSend({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
    loadSpecialist({
      variables: {
        pageSize: -1
      }
    });
    loadProvinces();
    loadReasons();
    loadSiteStaffs({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
  }, []);
  const {
    Form,
    form,
    AppointmentCode,
    AppointmentStatus,
    AppointmentSource,
    PhoneNumber,
    CustomerType,
    CustomerFullname,
    ReasonBooking,
    DateofBirth,
    BirthYear,
    Nationality,
    CollaboratorDoctor,
    Gender,
    MedicalExaminationPackage,
    ContactAddress,
    Test,
    Provinces,
    Districts,
    Wards,
    Street4Home,
    TestNotes,
    Email,
    IdentityCard,
    GeneticCode,
    ScheduleReporter,
    PID,
    MedicalPlace,
    ObjectSend,
    Specialists,
    AppointmentDate,
    AppointmentWorkTime,
    AppointmentObject,
    AppointmentTimeFrame,
    AppointmentNotes,
    AppointmentNotesTable,
    MgsContent,
    Route,
    AssignStaff,
    FacilityParent,
    Facility,
    BHYT,
    ReasonBookingStr,
    HistoryTableTable,
    Status,
    CodeAppoiment,
    SetForObject,
    TypeCustomer,
    DateOnset,
    Prognostic,
    StreetHasCovid,
    NationPassOther,
    HumanHasCovid,
    TestedCovid,
    Invoice,
    AddressProfile,
    EmailProfile,
    NameCompany,
    IsCovid,
    TowInjections,
    TaxCode,
    AddressCompany,
    EmailCompany,
    PrintName,
    ReasonCheckup
  } = useGetAddAppointmentForm({
    name: 'add-appointment-form',
    responsive: true,
    appointmentInput: appointmentCache
      ? {
          ...appointmentCache,
          healthDeclaration: {
            ...appointmentCache?.healthDeclaration,
            purpose: 'Khám bệnh',
            dateOnset: appointmentCache?.healthDeclaration?.dateOnset
              ? moment(appointmentCache?.healthDeclaration?.dateOnset as moment.Moment)
              : undefined,
            dateHascovid: appointmentCache?.healthDeclaration?.dateHascovid
              ? moment(appointmentCache?.healthDeclaration?.dateHascovid as moment.Moment)
              : undefined
          },
          patient: {
            ...appointmentCache?.patient,
            nationality: appointmentCache?.patient?.nationality ? appointmentCache?.patient?.nationality : 'VN',
            relationType: appointmentCache?.patient?.relationType ? appointmentCache?.patient?.relationType : 'PATIENT',
            countryName: appointmentCache?.patient?.countryName ? appointmentCache?.patient?.countryName : 'VIỆT NAM',
            birthDate: appointmentCache?.patient?.birthDate ? moment(appointmentCache?.patient?.birthDate) : undefined,
            birthYear: appointmentCache?.patient?.birthYear
              ? moment(appointmentCache?.patient?.birthYear)
              : moment(appointmentCache?.patient?.birthDate)
              ? moment(moment(appointmentCache?.patient?.birthDate).format('YYYY'))
              : undefined
          },
          channelType: appointmentCache?.channelType ? appointmentCache.channelType : 'CCOMED'
        }
      : {
          patient: { nationality: 'VN', relationType: 'PATIENT', countryName: 'VIỆT NAM' },
          channelType: 'CCOMED',
          healthDeclaration: {
            purpose: 'Khám bệnh',
            typeCustomerKBYT: '1',
            prognostic: ['1'],
            humanHasCovid: '0',
            streetHasCovid: '0',
            testedCovid: '0',
            hasCovid: '2',
            hasVaccine: '3'
          },
          confirm: ['1']
        },
    isNewDialog: true,
    visible: visible
  });
  useEffect(() => {
    if (appointmentCache?.healthDeclaration?.dateHascovid) {
      setIsCovided(true);
    } else {
      setIsCovided(false);
    }
    if (appointmentCache?.methodType) {
      if (appointmentCache?.methodType === 'HOME') {
        setMethodType('HOME');
        setActiveKey('HOME');
      } else {
        setMethodType('HOSPITAL');
        setActiveKey('HOSPITAL');
      }
    }
    if (appointmentCache?.streetId) {
      if (appointmentCache?.patient?.provinceId) {
        loadStreets({
          variables: {
            page: 1,
            filtered: [
              {
                id: 'provinceId',
                value: appointmentCache?.patient?.provinceId,
                operation: '=='
              },
              {
                id: 'status',
                value: '1',
                operation: '=='
              }
            ]
          }
        });
        if (appointmentCache?.streetId) {
          loadRoutes({
            variables: {
              filtered: [
                {
                  id: 'id',
                  value: appointmentCache?.streetId?.split('***')[1],
                  operation: '=='
                },
                {
                  id: 'status',
                  value: '1',
                  operation: '=='
                }
              ]
            }
          });
        }

        loadFacilitiesHome({
          variables: {
            page: 1,
            filtered: [
              {
                id: 'isHome',
                value: '1',
                operation: '=='
              },
              {
                id: 'provinceIds',
                value: appointmentCache?.patient?.provinceId,
                operation: '~'
              },
              {
                id: 'status',
                value: '1',
                operation: '=='
              }
            ]
          }
        });
      }
    } else {
    }
  }, [appointmentCache]);
  const handleChangePatient = (values?: Partial<Patient>) => {
    if (values?.provinceId) {
      loadDistricts({
        variables: {
          provinceId: values.provinceId
        }
      });
    }
    if (values?.districtId) {
      loadWards({
        variables: {
          districtId: values.districtId
        }
      });
    }
  };

  const check24h = () => {
    if (appointmentDate) {
      if (new Date(appointmentDate) <= new Date()) {
        return true;
      } else if (appointmentDate && txtKG) {
        var listTG = txtKG.split('-');
        if (listTG) {
          var time = listTG[0].replace('h', ':');
          var appointmentDateT =
            new Date(appointmentDate).getFullYear() +
            '/' +
            (new Date(appointmentDate).getMonth() + 1) +
            '/' +
            new Date(appointmentDate).getDate();
          var date = new Date(appointmentDateT + ' ' + time);
          var hour = Math.abs(date.getTime() - new Date().getTime()) / 36e5;
          if (hour < 24) {
            return true;
          }
        }
      }
    }
    return false;
  };

  return (
    <Modal
      className="appointment-dialog"
      maskClosable={false}
      style={{ marginTop: 5 }}
      centered
      width={1500}
      title="Tạo mới lịch hẹn"
      okText="Lưu thông tin"
      cancelText="Hủy"
      visible={visible}
      onOk={() => {
        if (!isLoading) {
          onSubmit(false);
        }
      }}
      okButtonProps={{
        disabled: functionOb[functionCodeConstants.TD_LH_TLH] && !confirmLoading ? false : true,
        color: '#26a661'
      }}
      confirmLoading={confirmLoading}
      onCancel={() => {
        if (isCovided) {
          setIsCovided(false);
        }
        for (var i = 0; i < localStorage?.length; i++) {
          if (localStorage.key(i)?.includes('patient')) {
            localStorage.removeItem(localStorage.key(i) + '');
          }
        }
        localStorage.removeItem('IS_SHOW_CREATE_APPOINTMENT_DIALOG');
        localStorage.removeItem('appoiment-create-data');
        if (disabledField) {
          setDisabledField(false);
        }
        form.resetFields();
        setAppointmentCache(null);
        onCancel();
      }}
    >
      <Spin spinning={isLoading} tip="Đang xử lý...">
        <PageHeader
          ghost={false}
          extra={[
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_CBS] && !confirmLoading ? false : true}
              onClick={() => {
                if (!isLoading) {
                  onSubmit(true);
                }

                //handleMenuClick(4);
              }}
              icon={<StepForwardOutlined />}
              key="3"
              className="btn-green"
              type="primary"
            >
              Lưu &amp; Chuyển BS tư vấn
            </Button>,
            <Button
              onClick={() => {
                if (!isLoading) {
                  onSubmit(false);
                }
              }}
              icon={<SaveOutlined />}
              key="4"
              type="primary"
              disabled={functionOb[functionCodeConstants.TD_LH_TLH] && !confirmLoading ? false : true}
            >
              Lưu thông tin
            </Button>
          ]}
        ></PageHeader>
        <Form {...formItemLayout} className="new-add-appointment-form">
          <Col className="row hidden-over" span={12}>
            <div className="w-95p">
              <Tabs
                defaultActiveKey={methodType}
                activeKey={methodType}
                type="card"
                onTabClick={activeKey => {
                  setActiveKey(activeKey);
                  setMethodType(activeKey);
                  if (activeKey === 'HOME') {
                    if (form.getFieldValue(['patient', 'provinceId'])) {
                      loadStreets({
                        variables: {
                          page: 1,
                          filtered: [
                            {
                              id: 'provinceId',
                              value: form.getFieldValue(['patient', 'provinceId']),
                              operation: '=='
                            },
                            {
                              id: 'status',
                              value: '1',
                              operation: '=='
                            }
                          ]
                        }
                      });
                      loadFacilitiesHome({
                        variables: {
                          page: 1,
                          filtered: [
                            {
                              id: 'isHome',
                              value: '1',
                              operation: '=='
                            },
                            {
                              id: 'provinceIds',
                              value: form.getFieldValue(['patient', 'provinceId']),
                              operation: '~'
                            },
                            {
                              id: 'status',
                              value: '1',
                              operation: '=='
                            }
                          ]
                        }
                      });
                    }
                  } else if (activeKey === 'HOSPITAL') {
                    form.setFieldsValue({
                      healthDeclaration: {
                        purpose: 'Khám bệnh'
                      }
                    });
                    if (form.getFieldValue(['patient', 'provinceId'])) {
                      loadFacilitiesHos({
                        variables: {
                          page: 1,
                          filtered: [
                            {
                              id: 'isHospital',
                              value: '1',
                              operation: '=='
                            },
                            {
                              id: 'status',
                              value: '1',
                              operation: '=='
                            }
                          ]
                        }
                      });
                    }
                  }
                  form.setFieldsValue({
                    methodType: activeKey
                  });
                  form.setFieldsValue({
                    examFacilityId: undefined,
                    facilityId: undefined,
                    sendFacilityId: undefined,
                    specialistId: undefined,
                    workTimeId: undefined,
                    appointmentDate: moment(),
                    streetId: undefined,
                    routeId: undefined,
                    reasonNote: undefined,
                    appointmentObject: undefined,
                    routeName: undefined
                  });
                }}
                destroyInactiveTabPane={true}
              >
                <TabPane
                  tab={
                    <span>
                      <BankOutlined />
                      Đặt lịch tại viện
                    </span>
                  }
                  key="HOSPITAL"
                >
                  <Row>
                    <AppointmentDate
                      isHospital={false}
                      onChange={(value: any, dateString: any) => {}}
                      handleChangAppointmentDate={(examFacilityId, appointmentDate) => {
                        setAppointmentDate(appointmentDate);
                        if (appointmentDate && examFacilityId) {
                          loadAppointmentWorkTimeHome({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId
                              }
                            }
                          });
                        } else {
                          //
                        }
                      }}
                      setAppointmentDateP={setAppointmentDate}
                    />
                    <MedicalPlace
                      isLoading={isLoadingFacilitiesHos}
                      facilityList={facilitiesHos}
                      methodType={'HOSPITAL'}
                      handleChangeMedicalPlace={(examFacilityId, appointmentDate) => {
                        setTxtKG('');
                        if (examFacilityId) {
                          loadAllWorkTimeFacility({
                            variables: {
                              facilityId: examFacilityId
                            }
                          });
                        }
                        if (appointmentDate && examFacilityId) {
                          loadAppointmentWorkTimeHospital({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId
                              }
                            }
                          });
                        } else {
                          loadAppointmentWorkTimeHospital({
                            variables: {
                              data: {
                                appointmentDate: moment().format('YYYY-MM-DD'),
                                examFacilityId
                              }
                            }
                          });
                        }
                      }}
                    />
                    <AppointmentWorkTime
                      isLoading={isLoadingAllWorkTimeFacility}
                      appointmentWorkTime={form.getFieldValue('examFacilityId') ? allWorkTimeFacility : []}
                      setTxt={setTxtKG}
                      txtKG={txtKG}
                    />
                    <AppointmentObject
                      handleChange={(value: boolean) => {
                        setIsBHYT(value);
                      }}
                      appointmentObjects={{}}
                    />
                    <ObjectSend isLoading={isLoadingFacilitiesObjectSend} facilityList={facilitiesObjectSend} />
                    <Specialists isLoading={isLoadingSpecialists} specialists={specialists} />
                    <AppointmentSource
                      isLoading={isLoadingChannels}
                      onChangeAppointmentSource={value => {}}
                      channelList={channels}
                    />
                    <ReasonBooking isLoading={isLoadingReasons} reasonList={reasons} />
                    <Col className="gutter-row" span={24}>
                      <AppointmentTimeFrame
                        appointmentWorkTimeData={
                          form.getFieldValue('examFacilityId') ? appointmentWorkTimeHospital?.['data'] : []
                        }
                      />
                    </Col>
                  </Row>
                </TabPane>
                <TabPane
                  tab={
                    <span>
                      <HomeOutlined />
                      Đặt lịch tại nhà
                    </span>
                  }
                  key="HOME"
                >
                  <Row>
                    <AppointmentDate
                      isHospital={false}
                      onChange={(value: any, dateString: any) => {}}
                      handleChangAppointmentDate={(examFacilityId, appointmentDate) => {
                        setAppointmentDate(appointmentDate);
                        if (appointmentDate) {
                          loadAppointmentWorkTimeHome({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId
                              }
                            }
                          });
                        } else {
                          //
                        }
                      }}
                      setAppointmentDateP={setAppointmentDate}
                    />
                    <AppointmentWorkTime
                      isLoading={isLoadingAllWorkTimeFacility}
                      //appointmentWorkTime={form.getFieldValue('examFacilityId') ? allWorkTimeFacility : []}
                      appointmentWorkTime={worktimes}
                      setTxt={setTxtKG}
                    />
                    <Street4Home
                      isLoading={isLoadingStreets}
                      handleChangeStreet={async (routeId, appointmentDate) => {
                        let streetId = form.getFieldValue('streetId').split('***')[0];
                        let street = streets?.find(item => item?.id === streetId);
                        if (street?.['facilityId']) {
                          if (street?.['parentFacilityId']) {
                            let filtered: FilteredInput[] = [];
                            filtered.push({
                              id: 'parentFacilityId',
                              value: street?.['parentFacilityId'],
                              operation: '=='
                            });
                            filtered.push({
                              id: 'status',
                              value: '1',
                              operation: '=='
                            });
                            // loadSiteStaffsAssign({
                            //   variables: {
                            //     filtered: filtered
                            //   }
                            // });
                          }
                          loadFacilities({
                            variables: {
                              page: 1,
                              filtered: [
                                /*{
                                  id: 'isHome',
                                  value: '1',
                                  operation: '=='
                                },*/
                                {
                                  id: 'id',
                                  value: street?.['facilityId'],
                                  operation: '=='
                                },
                                {
                                  id: 'status',
                                  value: '1',
                                  operation: '=='
                                }
                              ]
                            }
                          });
                        } else {
                          form.setFieldsValue({
                            facilityId: ''
                          });
                        }
                        loadFacilitiesHome({
                          variables: {
                            page: 1,
                            filtered: [
                              {
                                id: 'isHome',
                                value: '1',
                                operation: '=='
                              },
                              {
                                id: 'status',
                                value: '1',
                                operation: '=='
                              },
                              {
                                id: 'id',
                                value: street?.['parentFacilityId'],
                                operation: '=='
                              }
                            ]
                          }
                        });

                        if (routeId !== '') {
                          loadRoutes({
                            variables: {
                              filtered: [
                                {
                                  id: 'id',
                                  value: routeId,
                                  operation: '=='
                                },
                                {
                                  id: 'status',
                                  value: '1',
                                  operation: '=='
                                }
                              ]
                            }
                          });
                        } else {
                          //
                        }
                      }}
                      streetList={form.getFieldValue(['patient', 'provinceId']) ? streets : []}
                    />
                    <Route routeNameDefault={form.getFieldValue('streetId') ? true : false} />
                    <FacilityParent
                      isLoading={isLoadingFacilitiesHome}
                      facilityParentList={facilitiesHome}
                      handleChangeFacilityParent={(parentFacilityId, appointmentDate) => {
                        if (parentFacilityId) {
                          /*loadAllWorkTimeFacility({
                            variables: {
                              facilityId: parentFacilityId
                            }
                          });*/
                          loadFacilities({
                            variables: {
                              page: 1,
                              filtered: [
                                {
                                  id: 'parentId',
                                  value: parentFacilityId,
                                  operation: '=='
                                }
                              ]
                            }
                          });
                        }
                        if (appointmentDate && parentFacilityId) {
                          //setAppointmentDate(appointmentDate);
                          /*loadAppointmentWorkTimeHome({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId: parentFacilityId
                              }
                            }
                          });*/
                        } else {
                          /*loadAppointmentWorkTimeHome({
                            variables: {
                              data: {
                                appointmentDate: moment().format('YYYY-MM-DD'),
                                examFacilityId: parentFacilityId
                              }
                            }
                          });*/
                        }
                      }}
                    />
                    <Facility facilityList={facilities} handleChangeFacility={() => {}} isLoading={false} />
                    <AppointmentSource
                      isLoading={isLoadingChannels}
                      onChangeAppointmentSource={value => {}}
                      channelList={channels}
                    />
                    <ReasonBooking isLoading={isLoadingReasons} reasonList={reasons} />
                    {/*<AssignStaff siteStaffs={siteStaffs} isLoadingSiteStaffsAssign={isLoadingSiteStaffs} />*/}
                    <Col className="gutter-row" span={24}>
                      <AppointmentTimeFrame
                        appointmentWorkTimeData={
                          form.getFieldValue('examFacilityId') ? appointmentWorkTimeHome?.['data'] : []
                        }
                      />
                    </Col>
                    {/*<div className="content">
                      <MgsContent
                        provinces={provinces}
                        districts={districts}
                        wards={wards}
                        workTimes={appointmentWorkTimeHospital?.['data']}
                      />
                    </div>*/}
                  </Row>
                </TabPane>
              </Tabs>
            </div>
            <div className="mt-14px-sub">
              <Tabs>
                <TabPane
                  tab={
                    <span>
                      <HighlightOutlined />
                      Ghi chú lịch hẹn
                    </span>
                  }
                  key="NOTE1"
                >
                  <AppointmentNotes
                    onSaveComment={value => {
                      setComments([...comments, value]);
                    }}
                  />
                  <Col className="gutter-row" span={24}>
                    <AppointmentNotesTable
                      comments={comments}
                      onDelete={index => {
                        comments.splice(index, 1);
                        setComments([...comments]);
                      }}
                    />
                  </Col>
                </TabPane>
                <TabPane
                  tab={
                    <span>
                      <HistoryOutlined />
                      Lịch sử
                    </span>
                  }
                  key="HISTORY"
                >
                  {/* <HistoryTableTable appoiments={[]} /> */}
                </TabPane>
              </Tabs>
            </div>
          </Col>
          <Col className="row" style={{ overflowX: 'scroll' }} span={12}>
            {/*<AppointmentTabs
              isHospital={methodType === 'HOSPITAL'}
              form={form}
              {/*tabContent={

              }
            />*/}
            <Tabs className="tabs-custom">
              <TabPane tab={<span className={'blue-kbyt'}>KH 1</span>} key="TAB1">
                <Row>
                  <Status />
                  <CodeAppoiment />
                  <PhoneNumber
                    loadStreets={loadStreets}
                    methodType={activeKey}
                    disabledField={disabledField}
                    provinces={provinces}
                    setDisabledField={setDisabledField}
                    handleChangeProvince={provinceId => {
                      if (provinceId) {
                        if (methodType === 'HOME') {
                          form.setFieldsValue({
                            facilityId: undefined,
                            examFacilityId: undefined
                          });
                          loadFacilitiesHome({
                            variables: {
                              page: 1,
                              filtered: [
                                {
                                  id: 'isHome',
                                  value: '1',
                                  operation: '=='
                                },
                                {
                                  id: 'provinceIds',
                                  value: provinceId,
                                  operation: '~'
                                },
                                {
                                  id: 'status',
                                  value: '1',
                                  operation: '=='
                                }
                              ]
                            }
                          });
                        } /*else if (methodType === 'HOSPITAL') {
                            loadFacilitiesHos({
                              variables: {
                                page: 1,
                                filtered: [
                                  {
                                    id: 'isHospital',
                                    value: '1',
                                    operation: '=='
                                  },
                                  {
                                    id: 'provinceIds',
                                    value: provinceId,
                                    operation: '~'
                                  },
                                  {
                                    id: 'status',
                                    value: '1',
                                    operation: '=='
                                  }
                                ]
                              }
                            });
                          }*/
                        loadDistricts({
                          variables: {
                            provinceId: provinceId
                          }
                        });
                        loadStreets({
                          variables: {
                            page: 1,
                            filtered: [
                              {
                                id: 'provinceId',
                                value: provinceId,
                                operation: '=='
                              },
                              {
                                id: 'status',
                                value: '1',
                                operation: '=='
                              }
                            ]
                          }
                        });
                      }
                    }}
                    handleChangePatient={handleChangePatient}
                  />
                  <PID patientList={patients} disabledField={disabledField} />
                  <CustomerFullname disabledField={disabledField} />
                  <DateofBirth disabledField={disabledField} />
                  <Gender disabledField={disabledField} />
                  <ContactAddress disabledField={disabledField} />
                  <Provinces
                    disabledField={disabledField}
                    isLoading={isLoadingProvince}
                    handleChangeProvince={provinceId => {
                      if (provinceId) {
                        if (methodType === 'HOME') {
                          loadStreets({
                            variables: {
                              page: 1,
                              filtered: [
                                {
                                  id: 'status',
                                  value: '1',
                                  operation: '=='
                                },
                                {
                                  id: 'provinceId',
                                  value: provinceId,
                                  operation: '=='
                                }
                              ]
                            }
                          });
                          form.setFieldsValue({
                            facilityId: '',
                            examFacilityId: ''
                          });
                        }
                        loadDistricts({
                          variables: {
                            provinceId: provinceId
                          }
                        });
                      }
                    }}
                    provinceList={provinces}
                  />
                  <Districts
                    disabledField={disabledField}
                    isLoading={isLoadingDistricts}
                    handleChangeDistrict={districtId => {
                      if (districtId !== '') {
                        loadWards({
                          variables: {
                            districtId: districtId
                          }
                        });
                      }
                    }}
                    districtList={form.getFieldValue(['patient', 'provinceId']) ? districts : []}
                  />
                  <Wards
                    disabledField={disabledField}
                    isLoading={isLoadingWards}
                    handleChangeWard={() => {}}
                    wardList={form.getFieldValue(['patient', 'districtId']) ? wards : []}
                  />
                  <IdentityCard disabledField={disabledField} />
                  <Email disabledField={disabledField} />
                  <Nationality isLoading={isLoadingCountries} countries={pagingCountries?.data} />
                  <ReasonBookingStr />
                  <SetForObject />
                  <ScheduleReporter
                    isLoading={isLoadingSiteStaffs}
                    siteStaffList={siteStaffs}
                    handleChangeSiteStaff={() => {}}
                  />
                  <CustomerType />
                  {/*<AppointmentSource
                  isLoading={isLoadingChannels}
                  onChangeAppointmentSource={value => { }}
                  channelList={channels}
                />*/}
                  {/*<ReasonBooking isLoading={isLoadingReasons} reasonList={reasons} />*/}
                  <CollaboratorDoctor functionOb={functionOb} />
                  <MedicalExaminationPackage
                    functionOb={functionOb}
                    methodType={activeKey}
                    provinceId={
                      activeKey === 'HOSPITAL'
                        ? facilitiesHos?.find(facility => facility.id === form.getFieldValue('examFacilityId'))
                            ?.provinceId
                        : form.getFieldValue(['patient', 'provinceId'])
                    }
                  />
                  <Test
                    functionOb={functionOb}
                    facilityList={methodType === 'HOSPITAL' ? facilitiesHos : facilitiesHome}
                  />
                  <BHYT isRequired={isBHYT} />
                  <GeneticCode />
                  {check24h() && (
                    <>
                      <div className="kbyt-title">Khai báo y tế dành cho khách hàng đăng ký khám</div>
                      <div className="kbyt-sub-title">
                        <p>* Phiếu khai báo thông tin y tế chỉ có giá trị trong vòng 24 giờ </p>
                        <p>
                          * Cảnh báo: Khai báo thông tin sai là vi phạm pháp luật Việt Nam, có thể bị xử lý hình sự!
                        </p>
                      </div>
                      <ReasonCheckup isDisabled={false} medthodType={activeKey} />
                      <TypeCustomer />
                      <DateOnset />
                      <Prognostic />
                      <div className="">
                        <span className="red">* </span>
                        <b>Mục B. Yếu tố dịch tễ nếu có</b>
                      </div>
                      <HumanHasCovid />
                      <StreetHasCovid />
                      <TestedCovid />
                      <div className="">
                        <span className="red">* </span>
                        <b>Mục C. CÁC THÔNG TIN KHÁC: Hãy tích vào ô trống trước phần lựa chọn</b>
                      </div>
                      <IsCovid isCovided={isCovided} setIsCovided={setIsCovided} field={{}} />
                      <TowInjections field={{}} />
                      <div className="kbyt-confirm">
                        <Form.Item name={'confirm'} rules={[{ required: true, message: 'Không được bỏ trống!' }]}>
                          <CheckboxGroup name="confirm">
                            <Checkbox
                              value={'1'}
                              // onChange={value => {
                              //   form.setFieldsValue({ confirm: value?.target?.checked });
                              //   //console.log('isCheck : ', await form.getFieldValue('confirm'));
                              // }}
                              indeterminate={false}
                            >
                              <span className="red" style={{ fontWeight: 500 }}>
                                Tôi cam kết đúng thông tin *
                              </span>
                            </Checkbox>
                          </CheckboxGroup>
                        </Form.Item>
                      </div>
                      {/*<Invoice setIsInvoice={setIsInvoice} />*/}
                      {isInvoice && (
                        <Tabs
                          defaultActiveKey={typeInvoice}
                          activeKey={typeInvoice}
                          type="card"
                          onTabClick={activeKey => {
                            setTypeInvoice(activeKey);
                            if (typeInvoice == '0') {
                              form.setFieldsValue({
                                healthDeclaration: {
                                  printName: undefined,
                                  nameCompany: undefined,
                                  addressCompany: undefined,
                                  taxCode: undefined,
                                  emailCompany: undefined
                                }
                              });
                            } else {
                              form.setFieldsValue({
                                healthDeclaration: {
                                  emailProfile: undefined,
                                  addressProfile: undefined
                                }
                              });
                            }
                          }}
                        >
                          <TabPane tab={<span>Cá nhân</span>} key="0">
                            <Row>
                              <AddressProfile
                                disabledField={disabledField}
                                required={typeInvoice === '0' ? true : false}
                              />
                              <EmailProfile required={typeInvoice === '0' ? true : false} />
                            </Row>
                          </TabPane>
                          <TabPane tab={<span>Công ty</span>} key="1">
                            <Row>
                              <NameCompany required={typeInvoice === '1' ? true : false} />
                              <TaxCode required={typeInvoice === '1' ? true : false} />
                              <AddressCompany required={typeInvoice === '1' ? true : false} />
                              <EmailCompany required={typeInvoice === '1' ? true : false} />
                              <PrintName />
                            </Row>
                          </TabPane>
                        </Tabs>
                      )}
                    </>
                  )}
                </Row>
              </TabPane>
            </Tabs>
          </Col>
        </Form>
      </Spin>
    </Modal>
  );
};
export default AppointmentDialog;
