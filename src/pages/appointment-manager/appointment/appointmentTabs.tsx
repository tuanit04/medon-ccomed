import { CloseCircleOutlined } from '@ant-design/icons';
import { Popconfirm, Tabs } from 'antd';
import moment from 'moment';
import React, { cloneElement, FC, useEffect, useState } from 'react';
const { TabPane } = Tabs;

interface AppointmentTabsState {
  form: any;
  tabContent: any;
  isHospital: boolean;
}
const AppointmentTabs: FC<AppointmentTabsState> = ({ tabContent, form, isHospital }) => {
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [targetKey, setTargetKey] = useState('');
  const initialPanes = [
    {
      title: 'KH 1',
      content: tabContent,
      key: '1',
      closable: false
    }
  ];
  const [activeKey, setActiveKey] = useState('1');
  const [panes, setPanes] = useState(initialPanes);
  const showPopconfirm = () => {
    setVisible(true);
  };

  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 1000);
  };

  const handleCancel = () => {
    setVisible(false);
  };
  const onChange = async activeKeyNew => {
    const patient = await form.getFieldValue('patient');
    const healthDeclaration = await form.getFieldValue('healthDeclaration');
    localStorage.setItem('patient' + activeKey, JSON.stringify(patient));
    localStorage.setItem('healthDeclaration' + activeKey, JSON.stringify(healthDeclaration));
    form.setFieldsValue({ patient: undefined, healthDeclaration: undefined });
    let patientValue = localStorage.getItem('patient' + activeKeyNew);
    let healthDeclarationValue = localStorage.getItem('healthDeclaration' + activeKeyNew);
    form.setFieldsValue({
      patient:
        patientValue && patientValue !== 'undefined'
          ? {
              ...JSON.parse(patientValue),
              birthDate: moment(JSON.parse(patientValue)?.birthDate),
              birthYear: moment(JSON.parse(patientValue)?.birthYear)
            }
          : undefined,
      healthDeclaration:
        healthDeclarationValue && healthDeclarationValue !== 'undefined'
          ? {
              ...JSON.parse(healthDeclarationValue),
              dateOnset: moment(JSON.parse(healthDeclarationValue)?.dateOnset)
            }
          : undefined,
      confirm: localStorage.getItem('confirm' + activeKeyNew)
    });
    setActiveKey(activeKeyNew);
  };
  const objAction = {
    add: async () => {
      let patient = await form.getFieldValue('patient');
      let healthDeclaration = await form.getFieldValue('healthDeclaration');
      let confirm = await form.getFieldValue('confirm');
      console.log('confirm : ', confirm);
      localStorage.setItem('patient' + activeKey, JSON.stringify(patient));
      localStorage.setItem('healthDeclaration' + activeKey, JSON.stringify(healthDeclaration));
      localStorage.setItem('confirm' + activeKey, confirm);
      form.setFieldsValue({ patient: undefined, healthDeclaration: undefined, confirm: false });
      form.setFieldsValue({ patient: { nationality: 'VN', relationType: 'PATIENT', countryName: 'VIỆT NAM' } });
      let newTab = Number(activeKey) + 1;
      let newPanes = [...panes];
      setActiveKey(String(newTab));
      newPanes.push({
        content: tabContent,
        key: String(newTab),
        title: 'KH ' + newTab,
        closable: true
      });
      setPanes(newPanes);
    },
    remove: targetKey => {
      //showPopconfirm();
      let newActiveKey = activeKey;
      let lastIndex;
      panes.forEach((pane, i) => {
        if (pane.key === targetKey) {
          lastIndex = i - 1;
        }
      });
      const newPanes = panes.filter(pane => pane.key !== targetKey);
      if (newPanes.length && newActiveKey === targetKey) {
        if (lastIndex >= 0) {
          newActiveKey = newPanes[lastIndex].key;
        } else {
          newActiveKey = newPanes[0].key;
        }
      }
      setPanes(newPanes);
      setActiveKey(newActiveKey);
      //setActiveKey(panes[panes.length - 1].key);
    }
  };
  const onEdit = (targetKey, action) => {
    objAction[action](targetKey);
  };

  useEffect(() => {
    if (isHospital) {
      setActiveKey('1');
      form.setFieldsValue({ patient: { nationality: 'VN', relationType: 'PATIENT', countryName: 'VIỆT NAM' } });
    } else {
      setActiveKey(panes[panes.length - 1].key);
      form.setFieldsValue({ patient: { nationality: 'VN', relationType: 'PATIENT', countryName: 'VIỆT NAM' } });
    }
  }, [isHospital]);

  return (
    <>
      <Tabs
        type="editable-card"
        defaultActiveKey="1"
        hideAdd={isHospital}
        onChange={onChange}
        activeKey={activeKey}
        onEdit={onEdit}
        id="appointment-tabs"
        tabBarStyle={{ color: 'red' }}
      >
        {isHospital ? (
          <TabPane
            closeIcon={<CloseCircleOutlined />}
            tab="KH"
            key={initialPanes[0].key}
            closable={initialPanes[0].closable}
          >
            {initialPanes[0].content}
          </TabPane>
        ) : (
          panes.map(pane => (
            <TabPane closeIcon={<CloseCircleOutlined />} tab={pane.title} key={pane.key} closable={pane.closable}>
              {pane.content}
            </TabPane>
          ))
        )}
      </Tabs>
      <Popconfirm
        title="Bạn có chắc chắn muốn đóng tab khách hàng này không ?"
        visible={visible}
        onConfirm={handleOk}
        okButtonProps={{ loading: confirmLoading }}
        onCancel={handleCancel}
      ></Popconfirm>
    </>
  );
};

export default AppointmentTabs;
