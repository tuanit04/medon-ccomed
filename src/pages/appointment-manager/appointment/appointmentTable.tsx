import React, { FC, useState, useEffect } from 'react';
import { Button, Table, Modal, Tag, Dropdown, Menu, Tooltip, FormProps, FormInstance } from 'antd';
import {
  PlusCircleOutlined,
  CloseOutlined,
  ExclamationCircleOutlined,
  DeleteOutlined,
  EditOutlined,
  OrderedListOutlined,
  StepForwardOutlined,
  DownOutlined,
  SearchOutlined,
  ClearOutlined,
  CheckCircleOutlined,
  CopyrightOutlined
} from '@ant-design/icons';
import { AppointmentPatient } from 'common/interfaces/appointmentPatient.interface';
import { useForwardAppointment } from 'hooks/appointment/useForwardAppointmentToDoctorConsultant';
import { openNotificationRight } from 'utils/notification';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import moment from 'moment';
import { useAppointmentStatistics } from 'hooks/appointment/useGetAppointmentStatistics';
import './index.less';
import { functionCodeConstants } from 'constants/functions';
import { getStatus } from 'pages/advisory-manager/advisory-input/utils';
import useUpdateEffect from 'hooks/useUpdateEffect';
import { useSearchParams } from 'react-router-dom';
interface AppointmentTableProps {
  onCreate: () => void;
  forwardAppointmentState: any;
  onModify: (appointmentPatient: AppointmentPatient, refetchAppointments?: any) => void;
  onSearch: ({}) => void;
  onBookingMultipleAppointments: (appointmentPatient: AppointmentPatient) => void;
  onDelete: (id: any) => void;
  onCancle: (id: any) => void;
  onDeleteMulti: (ids: any) => void;
  onCancleMulti: (ids: any) => void;
  onAuthorize: (row: AppointmentPatient) => void;
  appointmentIdRes: string;
  onResetAppointmentIdRes: () => void;
  statusSearch: FilteredInput;
  dateType: string;
  fromDateSearch: string;
  toDateSearch: string;
  infoType: string;
  infoSearch: string;
  appointmentTypeSearch: FilteredInput;
  methodTypeSearch: FilteredInput;
  channelSearch: FilteredInput;
  examFacilitySearch: FilteredInput;
  emptySearch: number;
  isSearch: boolean;
  formInstance: FormInstance<FormProps<any>>;
  functionOb: any;
  page: number;
  pageSize: number;
  setPage: (value: number) => void;
  setPageSize: (value: number) => void;
  filtered: FilteredInput[];
  setFiltered: (value: FilteredInput[]) => void;
  pagingAppointments: any;
  isLoadingAppointments: boolean;
  refetchAppointments?: any;
  isLoadingAppointment: boolean;
}

const AppointmentTable: FC<AppointmentTableProps> = ({
  onCreate,
  onModify,
  onSearch,
  forwardAppointmentState,
  onBookingMultipleAppointments,
  onDelete,
  onCancle,
  onDeleteMulti,
  onCancleMulti,
  onResetAppointmentIdRes,
  appointmentIdRes,
  statusSearch,
  dateType,
  fromDateSearch,
  toDateSearch,
  infoType,
  infoSearch,
  appointmentTypeSearch,
  methodTypeSearch,
  channelSearch,
  examFacilitySearch,
  formInstance,
  functionOb,
  page,
  setPage,
  pageSize,
  filtered,
  setFiltered,
  setPageSize,
  isLoadingAppointment,
  pagingAppointments,
  isLoadingAppointments,
  refetchAppointments
}) => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [baseData, setBaseData] = useState([]);
  const [appointmentDetail, setAppointmentDetail] = useState({} as AppointmentPatient);

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const variables = { page, pageSize, filtered };

  useEffect(() => {
    if (statusSearch['value'] || infoSearch || appointmentTypeSearch['value'] || methodTypeSearch['value']) {
      setPage(1);
    }
  }, [statusSearch['value'] || infoSearch || appointmentTypeSearch['value'] || methodTypeSearch['value']]);

  useUpdateEffect(() => {
    const filtered: FilteredInput[] = [];
    if (infoSearch && infoSearch !== '' && infoType && infoType !== '') {
      filtered.push({
        id: infoType,
        value: infoSearch,
        operation: '~'
      });
    }
    if (fromDateSearch && toDateSearch && dateType) {
      filtered.push({
        id: dateType,
        value: fromDateSearch + ',' + toDateSearch,
        operation: 'between'
      });
    } else if (!fromDateSearch && toDateSearch && dateType) {
      filtered.push({
        id: dateType,
        value: toDateSearch,
        operation: '=='
      });
    } else if (fromDateSearch && !toDateSearch && dateType) {
      filtered.push({
        id: dateType,
        value: fromDateSearch,
        operation: '=='
      });
    }
    if (statusSearch['value'] !== '') {
      filtered.push(statusSearch);
    }
    if (appointmentTypeSearch['value'] !== '') {
      filtered.push(appointmentTypeSearch);
    }
    if (methodTypeSearch['value'] !== '') {
      filtered.push(methodTypeSearch);
    }
    if (channelSearch['value'] !== '') {
      filtered.push(channelSearch);
    }
    if (examFacilitySearch['value'] !== '') {
      filtered.push(examFacilitySearch);
    }
    setFiltered(filtered);
  }, [
    appointmentTypeSearch,
    channelSearch,
    dateType,
    examFacilitySearch,
    fromDateSearch,
    infoSearch,
    infoType,
    methodTypeSearch,
    statusSearch,
    toDateSearch
  ]);

  useEffect(() => {
    refetchAppointments?.(variables);
    refetchAppointmentStatistics(variables);
  }, [filtered]);

  useEffect(() => {
    if (pagingAppointments && forwardAppointmentState) {
      setTimeout(() => {
        refetchAppointments?.(variables);
      }, 3000);
    }
  }, [forwardAppointmentState]);

  const {
    appointmentStatistics,
    isLoadingAppointmentStatistics,
    refetchAppointmentStatistics,
    errorAppointmentStatistics
  } = useAppointmentStatistics({ variables });
  const [pagination, setPagination] = useState({});

  useEffect(() => {
    setBaseData(pagingAppointments?.data);
    setPagination({ current: page, pageSize: pageSize, total: pagingAppointments['records'] });
  }, [pagingAppointments]);

  //Chuyển BS Tư vấn
  const {
    forwardAppointment,
    resultForwardAppointment,
    isLoadingForwardAppointment,
    errorForwardAppointment
  } = useForwardAppointment();
  const handleForwardAppointment = async appointmentId => {
    //setSelectedRowKeys([]);
    await forwardAppointment(appointmentId);
    refetchAppointments?.(variables);
    refetchAppointmentStatistics(variables);
  };

  const handleDeleteAppointment = async appointmentId => {
    //setSelectedRowKeys([]);
    await onDelete(appointmentId);
    refetchAppointments?.(variables);
    refetchAppointmentStatistics(variables);
  };
  const handleDeleteMultiAppointment = async appointmentIds => {
    //setSelectedRowKeys([]);
    await onDeleteMulti(appointmentIds);
    refetchAppointments?.(variables);
    refetchAppointmentStatistics(variables);
  };
  const handleCancleAppointment = async appointmentId => {
    //setSelectedRowKeys([]);
    await onCancle(appointmentId);
    refetchAppointments?.(variables);
    refetchAppointmentStatistics(variables);
  };
  const handleCancleMultiAppointment = async appointmentIds => {
    //setSelectedRowKeys([]);
    await onCancleMulti(appointmentIds);
    refetchAppointments?.(variables);
    refetchAppointmentStatistics(variables);
  };
  // callback that get the data from backend(mock)
  // and loads the table with role-member row

  /*useEffect(() => {
    initData();
  }, [initData]);*/
  useEffect(() => {
    if (appointmentIdRes !== '') {
      refetchAppointments?.(variables);
      refetchAppointmentStatistics(variables);
    }
    onResetAppointmentIdRes();
  }, [appointmentIdRes]);

  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };
  const convertPhoneNumber = (str, index, replacement) => {
    if (str) {
      if (str.substr(0, 1) !== '0') {
        str = '0' + str;
        return str.substr(0, index) + replacement + str.substr(index + replacement.length);
      } else {
        return str.substr(0, index) + replacement + str.substr(index + replacement.length);
      }
    } else {
      return '';
    }
  };

  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    if (pageSize !== pagination['pageSize']) {
      setPage(1);
    } else {
      setPage(pagination['current']);
    }
    setPageSize(pagination['pageSize']);
  };
  const isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        return false;
      }
    }
    return JSON.stringify(obj) === JSON.stringify({});
  };
  const handleMenuClick = e => {
    if (selectedRowKeys && selectedRowKeys.length === 0) {
      openNotificationRight('Vui lòng chọn lịch hẹn cụ thể để thực hiện thao tác này.', 'warning');
      return;
    }
    const keyMenu = e['key'];
    if (keyMenu === '1') {
      if (selectedRowKeys.length > 1) {
        openNotificationRight('Không thể sửa lịch hẹn khi chọn nhiều hơn một bản ghi.', 'warning');
        //setSelectedRowKeys([]);
        return;
      }
      for (let i = 0; i < pagingAppointments?.data?.length; i++) {
        if (pagingAppointments?.data[i]['appointmentId'] === selectedRowKeys[0]) {
          onModify(pagingAppointments?.data[i]);
          break;
        }
      }
      //setSelectedRowKeys([]);
    } else if (keyMenu === '2') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        okText: 'Đồng ý',
        cancelText: 'Hủy bỏ',
        title: 'Bạn có chắc chắn muốn huỷ các lịch hẹn đã chọn' + '' + ' ?',
        onOk() {
          handleCancleMultiAppointment(selectedRowKeys);
          // handleCancleAppointment(selectedRowKeys[0]);
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    } else if (keyMenu === '3') {
      if (selectedRowKeys.length > 1) {
        openNotificationRight('Không thể đặt nhiều lịch hẹn khi chọn nhiều hơn một bản ghi.', 'warning');
        //setSelectedRowKeys([]);
        return;
      }
      if (appointmentDetail.status === 13) {
        openNotificationRight('Không thể đặt nhiều lịch đối với bản ghi đã huỷ.', 'warning');
        return;
      }
      for (let i = 0; i < pagingAppointments?.data?.length; i++) {
        if (pagingAppointments?.data[i]['appointmentId'] === selectedRowKeys[0]) {
          onBookingMultipleAppointments(pagingAppointments?.data[i]);
          break;
        }
      }
    } else if (keyMenu === '4') {
      if (selectedRowKeys.length > 1) {
        openNotificationRight('Không thể chuyển lịch hẹn cho bác sĩ tư vấn khi chọn nhiều hơn một bản ghi.', 'warning');
        //setSelectedRowKeys([]);
        return;
      }
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        okText: 'Đồng ý',
        cancelText: 'Hủy bỏ',
        title: 'Bạn có chắc chắn muốn chuyển lịch hẹn đã chọn' + '' + ' cho bác sĩ tư vấn ?',
        onOk() {
          for (let i = 0; i < pagingAppointments?.data?.length; i++) {
            if (pagingAppointments?.data[i]['appointmentId'] === selectedRowKeys[0]) {
              handleForwardAppointment({ id: pagingAppointments?.data[i]['appointmentId'] });
              break;
            }
          }
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    } else if (keyMenu === '5') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        okText: 'Đồng ý',
        cancelText: 'Hủy bỏ',
        title: 'Bạn có chắc chắn muốn xóa các lịch hẹn đã chọn' + '' + ' ?',
        onOk() {
          handleDeleteMultiAppointment(selectedRowKeys);
          //handleDeleteAppointment(selectedRowKeys[0]);
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    }
    //setAppointmentDetail({} as AppointmentPatient);
  };
  const menu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item
        disabled={functionOb[functionCodeConstants.TD_LH_SUALH] ? false : true}
        key="1"
        icon={<EditOutlined />}
      >
        Sửa
      </Menu.Item>
      <Menu.Item disabled={functionOb[functionCodeConstants.TD_LH_HUY] ? false : true} key="2" icon={<CloseOutlined />}>
        Huỷ
      </Menu.Item>
      <Menu.Item
        disabled={functionOb[functionCodeConstants.TD_LH_DATNHIEU] ? false : true}
        key="3"
        icon={<OrderedListOutlined />}
      >
        Đặt nhiều lịch
      </Menu.Item>
      <Menu.Item
        disabled={functionOb[functionCodeConstants.TD_LH_CBS] ? false : true}
        key="4"
        icon={<StepForwardOutlined />}
      >
        Chuyển BS tư vấn
      </Menu.Item>
      <Menu.Item
        disabled={functionOb[functionCodeConstants.TD_LH_XOA] ? false : true}
        key="5"
        icon={<DeleteOutlined />}
      >
        Xoá
      </Menu.Item>
    </Menu>
  );

  return (
    <Table
      rowClassName={(record: AppointmentPatient, index) => {
        let createDateSplit = record.createDate?.split(' ')[0] + ' 00:00:00';
        let appointmentDateSplit = record.appointmentDate?.split(' ')[0] + ' 00:00:00';
        if (createDateSplit < appointmentDateSplit) {
          return 'pre-book';
        } else {
          return '';
        }
      }}
      className="table-appointment"
      loading={isLoadingAppointments}
      rowKey={row => row['appointmentId']}
      rowSelection={{
        selectedRowKeys,
        onChange: onSelectChange,
        type: 'checkbox'
      }}
      pagination={{
        ...pagination,
        defaultPageSize: 20,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '50']
      }}
      onChange={handleTableChange}
      bordered
      dataSource={baseData}
      scroll={{ x: 450, y: 'calc(100vh - 317px)' }}
      title={() => (
        <>
          <div style={{ float: 'left' }}>
            <label className="lblTableTxt">Danh sách lịch hẹn </label>
            <label className="lblTableValue">
              ({appointmentStatistics['totalRecords'] ? appointmentStatistics['totalRecords'] : 0})
            </label>
            <label style={{ marginLeft: 15 }}></label>
            <label className="lblTableTxt">Lịch đặt trước </label>
            <label className="lblTableValue">
              ({appointmentStatistics['futureRecords'] ? appointmentStatistics['futureRecords'] : 0})
            </label>
            <label style={{ marginLeft: 15 }}></label>
            <label className="lblTableTxt">Lịch phát sinh </label>
            <label className="lblTableValue">
              ({appointmentStatistics['currentRecords'] ? appointmentStatistics['currentRecords'] : 0})
            </label>
          </div>
          <div style={{ float: 'right' }}>
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_TIMKIEM] ? false : true}
              onClick={() => {
                setSearchParams({});
                let status = formInstance.getFieldValue('status');
                let dateType = formInstance.getFieldValue('dateType');
                let fromDate = formInstance.getFieldValue('fromDate');
                let toDate = formInstance.getFieldValue('toDate');
                let infoType = formInstance.getFieldValue('infoType');
                let info = formInstance.getFieldValue('info');
                let appointmentType = formInstance.getFieldValue('appointmentType');
                let methodType = formInstance.getFieldValue('methodType');
                let code = formInstance.getFieldValue('code');
                let examFacilityId = formInstance.getFieldValue('examFacilityId');
                onSearch({
                  status,
                  fromDate,
                  toDate,
                  dateType,
                  infoType,
                  info,
                  appointmentType,
                  methodType,
                  code,
                  examFacilityId
                });
              }}
              className="btn-green"
              icon={<SearchOutlined />}
              type="primary"
            >
              Tìm kiếm
            </Button>
            &nbsp;&nbsp;
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_BOLOC] ? false : true}
              onClick={() => {
                formInstance.resetFields();
                onSearch({
                  status: '',
                  dateType: 'appointmentDate',
                  fromDate: moment(),
                  toDate: moment(),
                  infoType: 'patientPhone',
                  info: '',
                  appointmentType: '',
                  methodType: ''
                });
              }}
              icon={<ClearOutlined />}
            >
              Bỏ lọc
            </Button>
            &nbsp;&nbsp;<span style={{ color: '#206ad2', fontSize: '17px' }}>||</span>&nbsp;&nbsp;
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_TLH] ? false : true}
              icon={<PlusCircleOutlined />}
              type="primary"
              onClick={() => onCreate()}
            >
              Thêm lịch hẹn
            </Button>
            &nbsp;&nbsp;&nbsp;
            {/*<Popover trigger="click" placement="topLeft" title="Chọn hành động" content={selectedRowKeys.length === 0 ? 'Vui lòng chọn 1 lịch hẹn để thực hiện thao tác này.' : contentActionButton(selectedRowKeys)}>
              <Button on disabled={selectedRowKeys.length === 0 ? true : false} icon={<SelectOutlined />} type="primary">
                Hành động
              </Button>
          </Popover>*/}
            <Dropdown trigger={['click']} overlay={menu}>
              <Button className="ant-dropdown-link" onClick={() => {}}>
                Thao tác <DownOutlined />
              </Button>
            </Dropdown>
          </div>
          <div style={{ clear: 'both' }}></div>
        </>
      )}
      onRow={(record, rowIndex) => {
        return {
          onDoubleClick: () => {
            //setAppointmentDetail(record);
            onModify(record);
          },
          onClick: event => {
            if (!isEmpty(pagingAppointments) && record) {
              let appointmentCheckSelected: any = [];
              for (let i = 0; i < pagingAppointments?.data?.length; i++) {
                let appointmentObj = { ...pagingAppointments?.data[i] };
                let flag = false;
                if (appointmentObj['appointmentId'] === record['appointmentId']) {
                  appointmentObj['chosen'] = true;
                  flag = true;
                }
                if (!flag) {
                  appointmentObj['chosen'] = false;
                }
                appointmentCheckSelected.push(appointmentObj);
              }
              setSelectedRowKeys(
                appointmentCheckSelected?.filter(item => item.chosen)?.map(item => item.appointmentId)
              );
              setAppointmentDetail(record);
            }
          }
          // onContextMenu: event => {
          //   var txtCopy = '';
          //   txtCopy += record?.appointmentHisId + ' ';
          //   txtCopy += record?.customerLabel + ' ';
          //   txtCopy +=
          //     record?.methodType === 'HOME'
          //       ? 'Tại nhà'
          //       : record?.methodType === 'HOSPITAL'
          //       ? 'Bệnh viện/PK'
          //       : record?.methodType;
          //   txtCopy +=
          //     record?.status === 1
          //       ? 'Chờ xác nhận'
          //       : record?.status === 2
          //       ? 'Đã xác nhận'
          //       : record?.status === 3
          //       ? 'Chờ tư vấn'
          //       : record?.status === 4
          //       ? 'Đã tư vấn đầu vào'
          //       : record?.status === 5
          //       ? 'Chưa check'
          //       : record?.status === 6
          //       ? 'Đã check'
          //       : record?.status === 7
          //       ? 'Đã lấy mẫu'
          //       : record?.status === 8
          //       ? 'Đã có kết quả'
          //       : record?.status === 9
          //       ? 'Đã đăng ký khám'
          //       : record?.status === 10
          //       ? 'Đã thanh toán'
          //       : record?.status === 11
          //       ? 'Đã khám'
          //       : record?.status === 13
          //       ? 'Đã huỷ'
          //       : '';
          //   txtCopy += ' ';
          //   txtCopy += record?.patientName + ' ';
          //   txtCopy += record?.patientAddress + ' ';
          //   txtCopy += record?.patientPhone + ' ';
          //   txtCopy += record?.workTimeName + ' ';
          //   txtCopy += record?.appointmentDate?.split(' ')[0] + ' ';
          //   txtCopy += record?.reasonNote + ' ';
          //   txtCopy += record?.assignStaffName + ' ';
          //   txtCopy += record?.assignStaffPhone + ' ';
          //   txtCopy += record?.createUser + ' ';
          //   txtCopy += record?.createDate + ' ';
          //   txtCopy += record?.updateUser + ' ';
          //   txtCopy += record?.updateDate ? record?.updateDate : record?.createDate + ' ';
          //   txtCopy += record?.channelName + ' ';
          //   navigator.clipboard.writeText(txtCopy);
          // }
        };
      }}
    >
      <Table.Column<AppointmentPatient>
        align="center"
        title="STT"
        showSorterTooltip={true}
        width={50}
        render={(value, item, index) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>{(page - 1) * pageSize + index + 1}</span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        align="center"
        width={130}
        title="Mã lịch hẹn"
        render={(_, record) => (
          <div
            style={{
              display: 'flex',
              textAlign: 'right'
            }}
          >
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <span style={{ fontWeight: 500, width: '80%' }}>{record?.appointmentHisId}</span>
            </Tooltip>
            <Button
              style={{
                fontSize: 12,
                width: 20,
                height: 22,
                marginLeft: 6
              }}
              title="copy"
              icon={<CopyrightOutlined />}
              onClick={() => {
                var txtCopy = '';
                txtCopy += record?.appointmentHisId ? record?.appointmentHisId + '    ' : '';
                txtCopy += record?.customerLabel ? record?.customerLabel + '     ' : '';
                txtCopy +=
                  record?.methodType === 'HOME'
                    ? 'Tại nhà'
                    : record?.methodType === 'HOSPITAL'
                    ? 'Bệnh viện/PK'
                    : record?.methodType;
                txtCopy += '     ';
                txtCopy +=
                  record?.status === 1
                    ? 'Chờ xác nhận'
                    : record?.status === 2
                    ? 'Đã xác nhận'
                    : record?.status === 3
                    ? 'Chờ tư vấn'
                    : record?.status === 4
                    ? 'Đã tư vấn đầu vào'
                    : record?.status === 5
                    ? 'Chưa check'
                    : record?.status === 6
                    ? 'Đã check'
                    : record?.status === 7
                    ? 'Đã lấy mẫu'
                    : record?.status === 8
                    ? 'Đã có kết quả'
                    : record?.status === 9
                    ? 'Đã đăng ký khám'
                    : record?.status === 10
                    ? 'Đã thanh toán'
                    : record?.status === 11
                    ? 'Đã khám'
                    : record?.status === 13
                    ? 'Đã huỷ'
                    : '';
                txtCopy += '     ';
                txtCopy += record?.patientName ? record?.patientName + '    ' : '';
                txtCopy += record?.patientAddress ? record?.patientAddress + '     ' : '';
                txtCopy += record?.patientPhone ? record?.patientPhone + '     ' : '';
                txtCopy += record?.workTimeName ? record?.workTimeName + '    ' : '';
                txtCopy += record?.appointmentDate?.split(' ')[0]
                  ? record?.appointmentDate?.split(' ')[0] + '     '
                  : '';
                txtCopy += record?.reasonNote ? record?.reasonNote + '    ' : '';
                txtCopy += record?.assignStaffName ? record?.assignStaffName + '    ' : '';
                txtCopy += record?.assignStaffPhone ? record?.assignStaffPhone + '    ' : '';
                txtCopy += record?.createUser ? record?.createUser + '    ' : '';
                txtCopy += record?.createDate ? record?.createDate + '    ' : '';
                txtCopy += record?.updateUser ? record?.updateUser + '     ' : '';
                txtCopy += record?.updateDate ? record?.updateDate : record?.createDate + ' ';
                txtCopy += '    ';
                txtCopy += record?.channelName ? record?.channelName + ' ' : '';
                console.log('txtCopy', txtCopy);
                navigator.clipboard.writeText(txtCopy);
              }}
            />
          </div>
        )}
      />
      <Table.Column<AppointmentPatient>
        width={100}
        title="KBYT"
        align="center"
        render={(_, { customerLabel }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            {customerLabel ? (
              <Tag
                style={{ fontWeight: 500 }}
                color={customerLabel == 'Xanh' ? 'green' : customerLabel == 'Vàng' ? 'yellow' : 'red'}
              >
                {customerLabel}
              </Tag>
            ) : (
              ''
            )}
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        width={100}
        title="Hình thức"
        align="center"
        render={(_, { methodType }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>
              {methodType === 'HOME' ? 'Tại nhà' : methodType === 'HOSPITAL' ? 'Bệnh viện/PK' : methodType}
            </span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        align="center"
        width={150}
        title="Trạng thái"
        render={(_, { status }) => <Tooltip title="Click đúp chuột để xem chi tiết">{getStatus(status)}</Tooltip>}
      />
      <Table.Column<AppointmentPatient>
        title="Tên khách hàng"
        width={180}
        render={(_, { patientName }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>{patientName}</span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        title="Địa chỉ liên hệ"
        width={250}
        render={(_, { patientAddress }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>
              {patientAddress?.length >= 40 ? patientAddress.substr(0, 40) + '...' : patientAddress}
            </span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        align="center"
        title="Số điện thoại"
        width={100}
        render={(_, { patientPhone }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            {/*<span style={{ fontWeight: 500 }}>{convertPhoneNumber(patientPhone, 3, '***')}</span>*/}
            <span style={{ fontWeight: 500 }}>{patientPhone}</span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        width={120}
        align="center"
        title="Giờ đặt"
        render={(_, { workTimeName }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>{workTimeName}</span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        align="center"
        title="Ngày đặt lịch"
        width={90}
        render={(_, { appointmentDate }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>{appointmentDate?.split(' ')[0]}</span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        title="Lý do"
        width={260}
        render={(_, { reasonNote }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>
              {reasonNote?.length >= 40 ? reasonNote.substr(0, 40) + '...' : reasonNote}
            </span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        align="center"
        title="CBTN"
        width={120}
        render={(_, { assignStaffName }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>{assignStaffName}</span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        align="center"
        title="SĐT CBTN"
        width={100}
        render={(_, { assignStaffPhone }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>{assignStaffPhone}</span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        align="center"
        title="Người tạo lịch"
        width={120}
        render={(_, { createUser }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>{createUser}</span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        align="center"
        title="Ngày tạo lịch"
        width={150}
        render={(_, { createDate }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>{createDate}</span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        title="Người chỉnh sửa"
        width={150}
        render={(_, { updateUser }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>{updateUser}</span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        align="center"
        title="Thời gian chỉnh sửa"
        width={150}
        render={(_, { updateDate, createDate }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>{updateDate ? updateDate : createDate}</span>
          </Tooltip>
        )}
      />
      <Table.Column<AppointmentPatient>
        width={100}
        align="center"
        title="Đặt trước"
        render={(_, { createDate, appointmentDate }) => {
          let createDateSplit = createDate?.split(' ')[0] + ' 00:00:00';
          let appointmentDateSplit = appointmentDate?.split(' ')[0] + ' 00:00:00';
          if (createDateSplit < appointmentDateSplit) {
            return (
              <Tooltip title="Click đúp chuột để xem chi tiết">
                <Tag className="pre-book" icon={<CheckCircleOutlined />} color="success">
                  <span style={{ fontWeight: 500 }}>Đặt trước</span>
                </Tag>
              </Tooltip>
            );
          } else {
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <Tag icon={<CheckCircleOutlined />} color="success">
                {''}
              </Tag>
            </Tooltip>;
          }
        }}
      />
      {/* <Table.Column<AppointmentPatient>
        align="center"
        title="Người chỉnh sửa"
        width={120}
        render={(_, { updateUser }) => <Tooltip title="Click đúp chuột để xem chi tiết">{updateUser}</Tooltip>}
      /> */}
      {/* <Table.Column<AppointmentPatient>
        align="center"
        title="Thời gian chỉnh sửa"
        width={170}
        render={(_, { updateDate }) => <Tooltip title="Click đúp chuột để xem chi tiết">{updateDate}</Tooltip>}
      /> */}
      <Table.Column<AppointmentPatient>
        width={150}
        align="center"
        title="Nguồn đặt"
        render={(_, { channelName }) => (
          <Tooltip title="Click đúp chuột để xem chi tiết">
            <span style={{ fontWeight: 500 }}>{channelName}</span>
          </Tooltip>
        )}
      />
      {/*<Table.Column<AppointmentPatient>
        title="Hành động"
        align="center"
        render={(_, row) => (
          <Popover trigger="click" placement="topLeft" title="Chọn hành động" content={contentActionButton(row)}>
            <Button icon={<SelectOutlined />} type="primary">
              Hành động
            </Button>
          </Popover>
        )}
        />*/}
    </Table>
  );
};

export default AppointmentTable;
