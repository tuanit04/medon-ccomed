import React from 'react';
import { Form, Table } from 'antd';
import { useGetAppointmentActions } from 'hooks/appointment/useGetAppointmentAction';
import { ColumnsType } from 'antd/lib/table';
import { AppointmentActionView } from 'common/interfaces/appointmentActionView.interface';

interface Props {
  appointmentId?: string;
}

const AppointmentActions = ({ appointmentId }: Props) => {
  const { data, refetch, loading } = useGetAppointmentActions({ appointmentId });

  const columns: ColumnsType<AppointmentActionView> = [
    {
      title: 'Mã lịch hẹn',
      dataIndex: 'appointmentHisId',
      className: 'appointmentActions'
    },
    {
      title: 'Thời gian',
      dataIndex: 'actionTime',
      className: 'appointmentActions'
    },
    {
      title: 'Nhân viên',
      dataIndex: 'actionUser',
      className: 'appointmentActions'
    },
    {
      title: 'Hành động',
      dataIndex: 'actionName',
      className: 'appointmentActions',
      render: (actionName, actionNote) => {
        return actionName ? actionName : '' + (actionNote ? ', ' + actionNote : '');
      }
    }
  ];

  return (
    <Form.Item>
      <Table loading={loading} columns={columns} dataSource={data} />
    </Form.Item>
  );
};

export default AppointmentActions;
