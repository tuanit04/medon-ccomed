
import React, { FC, useEffect, useState } from 'react';
import { Input, Col, Select, Table, Button, Row } from 'antd';
import { Role } from 'interface/permission/role.interface';
import { useGetServicesLazy } from 'hooks/services/useGetServices';
import { Facility } from 'common/interfaces/facility.interface';
import { TableRowSelection } from 'antd/lib/table/interface';
import { VAppointmentDetailService } from 'common/interfaces/vAppointmentDetailService.interface';
interface MedicalExaminationTestFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  /** Initial form data */
  values?: Role;
}

export default function useGetMedicalExaminationTestForm({}: MedicalExaminationTestFormProps) {
  const getFiltered = ({ searchField, search, facilityId }) => {
    const arr: object[] = [];
    if (search) {
      arr.push({
        id: searchField,
        value: search?.trim(),
        operation: '~'
      });
    }
    if (facilityId) {
      arr.push({
        id: 'facilityId',
        value: facilityId ?? '',
        operation: '=='
      });
    } else {
      arr.push({
        id: 'facilityId',
        value: '',
        operation: 'is_null'
      });
    }
    return arr;
  };

  //Bang gia

  //Bang gia
  const ListServices: FC<{ services: any; filter: any; setFilter: any; loadServices: any }> = ({
    filter,
    setFilter,
    services,
    loadServices
  }) => {
    const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
      setFilter({ ...filter, search: e.target.value });
      // if (!e.target.value) {
      //   setTimeout(() => {
      //     onSearch({ ...filter, search: '' });
      //   }, 1200);
      // }
    };

    const onSelect = (value: string) => {
      setFilter({ ...filter, searchField: value });
    };

    useEffect(() => {
      // if (filter?.['search']) {
      //   const timeoutId: any = setTimeout(() => {
      //     onSearch(filter);
      //   }, 1200);
      //   return () => clearTimeout(timeoutId);
      // }
    }, [filter]);

    const onSearch = (filter?) => {
      loadServices({
        variables: {
          //page: services?.page ?? 1,
          page: 1,
          pageSize: services?.pageSize ?? 20,
          filtered: [
            ...getFiltered(filter),
            {
              id: 'historyStatus',
              value: '1',
              operation: '=='
            },
            {
              id: 'status',
              value: '1',
              operation: '=='
            }
          ]
        }
      });
    };

    const select = (
      <Select value={filter.searchField} onChange={onSelect}>
        <Select.Option value="serviceName">Tên dịch vụ</Select.Option>
        <Select.Option value="serviceCode">Mã dịch vụ</Select.Option>
      </Select>
    );

    return (
      <Row align="middle">
        <Col className="gutter-row" span={5}>
          <span style={{ fontWeight: 400, color: 'red' }}>
            Tìm kiếm ({services['records'] ? services['records'] + ' bản ghi' : '0 bản ghi'})
          </span>
        </Col>
        <Col className="gutter-row" span={19}>
          <Input
            placeholder={`Nhập thông tìm kiếm `}
            style={{ width: '100%' }}
            onChange={onChange}
            onPressEnter={(e: any) => {
              onSearch({ ...filter, search: e.target.value });
            }}
            addonBefore={select}
          />
        </Col>
        {/*<Col className="gutter-row" span={16}>
          <Space>
            <Input style={{ width: '100%' }} value={filter.search} onChange={onChange} addonBefore={select} />
            <Button type="primary" onClick={onSearch}>
              <SearchOutlined />
              Tìm kiếm
            </Button>
          </Space>
        </Col>*/}
      </Row>
    );
  };

  interface ListServicesFormProps {
    status?: number;
    appointmentStatus?: number | undefined;
    setServicesDataSelectedArr: (servicesDataSelectedArr: any) => void;
    methodType: string;
    facilities: Facility[];
    facilityId: string;
    serviceList?: any | undefined;
  }

  const ListServicesForm: FC<ListServicesFormProps> = ({
    status,
    setServicesDataSelectedArr,
    methodType,
    facilityId,
    appointmentStatus,
    serviceList
  }) => {
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(20);
    const [pagination, setPagination] = useState({});
    const [selectedRowKeys, setSelectedRowKeys] = useState<string[]>([]);
    const [totalOriginPriceState, setTotalOriginPriceState] = useState(Number);
    const [totalSalePriceState, setTotalSalePriceState] = useState(Number);
    const [servicesDataSelectedState, setServicesDataSelectedState]: any = useState([]);
    const salePriceField = methodType === 'HOME' ? 'homeSalePrice' : 'hosSalePrice';
    let servicesDataSelectedArr: any = [];
    const [loading, setLoading] = useState(false);
    const [filter, setFilter] = useState({
      facilityId,
      searchField: 'serviceName',
      search: ''
    });
    const variables = { page, pageSize };
    const { services, loadServices, isLoadingServices } = useGetServicesLazy({
      ...variables,
      filtered: [
        ...getFiltered(filter),
        {
          id: 'historyStatus',
          value: '1',
          operation: '=='
        },
        {
          id: 'status',
          value: '1',
          operation: '=='
        }
      ]
    });
    const {
      services: servicesSelected,
      isLoadingServices: isLoadingServicesSelected,
      loadServices: loadServicesSelected
    } = useGetServicesLazy({
      page,
      pageSize,
      filtered: [
        ...getFiltered(filter),
        {
          id: 'historyStatus',
          value: '1',
          operation: '=='
        },
        {
          id: 'status',
          value: '1',
          operation: '=='
        }
      ]
    });
    useEffect(() => {
      loadServices({
        variables: {
          page,
          pageSize,
          filtered: [
            ...getFiltered(filter),
            {
              id: 'historyStatus',
              value: '1',
              operation: '=='
            },
            {
              id: 'status',
              value: '1',
              operation: '=='
            }
          ]
        }
      });
      loadServicesSelected({
        variables: {
          page,
          pageSize,
          filtered: [
            ...getFiltered(filter),
            {
              id: 'historyStatus',
              value: '1',
              operation: '=='
            },
            {
              id: 'status',
              value: '1',
              operation: '=='
            },
            {
              id: 'serviceId',
              value: serviceList?.map(val => val.serviceId).join(','),
              operation: 'IN'
            }
          ]
        }
      });
    }, [serviceList]);

    const onSelect = (record, selected) => {
      servicesDataSelectedArr = [...servicesDataSelectedState];
      const index = servicesDataSelectedArr.indexOf(val => val.serviceId === record.serviceId);
      if (selected) {
        if (index === -1) {
          servicesDataSelectedArr.push(record);
        }
      } else {
        if (record !== -1) {
          servicesDataSelectedArr = servicesDataSelectedArr.filter(val => val.serviceId !== record.serviceId);
        }
      }

      for (const row of servicesDataSelectedArr) {
        totalOriginPrice += Number(row['originPrice']);
        totalSalePrice += Number(row[salePriceField]);
      }

      setSelectedRowKeys(servicesDataSelectedArr?.map(val => val.serviceId));
      setServicesDataSelectedState(servicesDataSelectedArr);
      setServicesDataSelectedArr(servicesDataSelectedArr);
      setTotalOriginPriceState(totalOriginPrice);
      setTotalSalePriceState(totalSalePrice);
    };

    useEffect(() => {
      if (serviceList) {
        for (let j = 0; j < serviceList.length; j++) {
          if (serviceList[j]['serviceId']) {
            let serviceObj = { ...serviceList[j], originPrice: '' };
            serviceObj.originPrice = serviceObj['originalPrice'];
            serviceObj[salePriceField] = serviceList[j]['salePrice'];
            servicesDataSelectedArr.push(serviceObj);
            totalOriginPrice = totalOriginPrice + Number(serviceList[j]['originalPrice']);
            totalSalePrice = totalSalePrice + Number(serviceList[j]['salePrice']);
          }
        }
        setSelectedRowKeys(servicesDataSelectedArr?.map(val => val.serviceId));
        setTotalOriginPriceState(totalOriginPrice);
        setTotalSalePriceState(totalSalePrice);
        setServicesDataSelectedState(servicesDataSelectedArr);
        setServicesDataSelectedArr(servicesDataSelectedArr);
      }
    }, [serviceList]);

    useEffect(() => {
      if (servicesSelected && servicesSelected?.data?.length > 0) {
        let _serviceList: any = [];
        for (let i = 0; i < servicesDataSelectedState.length; i++) {
          for (let j = 0; j < servicesSelected?.data?.length; j++) {
            if (servicesDataSelectedState[i]['serviceId'] === servicesSelected?.data[j]['serviceId']) {
              let serviceObj = {
                ...servicesDataSelectedState[i],
                [salePriceField]: servicesSelected?.data[j][salePriceField]
              };
              _serviceList.push(serviceObj);
              totalOriginPrice = totalOriginPrice + Number(serviceObj['originalPrice']);
              totalSalePrice = totalSalePrice + Number(serviceObj[salePriceField]);
              break;
            }
          }
        }
        setTotalOriginPriceState(totalOriginPrice);
        setTotalSalePriceState(totalSalePrice);
        // setServicesDataSelectedState(_serviceList);
        // setServicesDataSelectedArr(_serviceList);
      }
    }, [servicesSelected]);
    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'VND',
      minimumFractionDigits: 0
    });

    useEffect(() => {
      setFilter({ ...filter, facilityId });
      loadServices({
        variables: {
          page,
          pageSize,
          filtered: [
            ...getFiltered({ ...filter, facilityId }),
            {
              id: 'historyStatus',
              value: '1',
              operation: '=='
            },
            {
              id: 'status',
              value: '1',
              operation: '=='
            }
          ]
        }
      });
    }, [facilityId]);

    const handleTableChange = (pagination, filters, sorter) => {
      setPagination({ current: pagination.current, pageSize: pagination.pageSize });
      reFetchData({
        sortField: sorter.field,
        sortOrder: sorter.order,
        pagination,
        ...filters
      });
    };

    const reFetchData = (params = {}) => {
      let pagination = params['pagination'];
      if (pageSize !== pagination['pageSize']) {
        setPage(1);
      } else {
        setPage(pagination['current']);
      }
      setPageSize(pagination['pageSize']);
      loadServices();
    };

    const start = () => {
      setLoading(true);
      setSelectedRowKeys([]);
      serviceList = null;
      setServicesDataSelectedState([]);
      setServicesDataSelectedArr([]);
      setTotalOriginPriceState(0);
      setTotalSalePriceState(0);
      setTimeout(() => {
        setLoading(false);
      }, 500);
    };
    let totalOriginPrice: number = 0;
    let totalSalePrice: number = 0;

    const isSelectDisabled =
      facilityId !== filter.facilityId || (appointmentStatus !== 1 && appointmentStatus !== undefined);

    const rowSelection: TableRowSelection<VAppointmentDetailService> = {
      selectedRowKeys,
      onSelect,
      getCheckboxProps: () => ({
        disabled: isSelectDisabled
        //indeterminate: check()
      })
    };

    const columns = (selected = false) => {
      const arr = [
        {
          title: 'Mã dịch vụ',
          dataIndex: 'serviceCode',
          width: selected ? '15%' : '20%'
        },
        {
          title: 'Tên dịch vụ',
          dataIndex: 'serviceName',
          width: selected ? '35%' : '40%'
        },
        {
          title: 'Giá gốc (VNĐ)',
          dataIndex: 'originPrice',
          width: selected ? '15%' : '20%',
          render: value => value && formatter.format(value)
        },
        {
          title: 'Giá Khu vực/Đơn vị (VNĐ)',
          dataIndex: salePriceField,
          width: selected ? '15%' : '20%',
          render: value => value && formatter.format(value)
        }
      ];
      if (selected) {
        arr.push({
          title: 'Người chỉ định',
          dataIndex: 'indicatedDoctorName',
          width: '20%',
          render: indicatedDoctorName => {
            return indicatedDoctorName
              ? indicatedDoctorName
              : JSON.parse(String(localStorage.getItem('user')))
              ? JSON.parse(String(localStorage.getItem('user')))['username']
              : '';
          }
        });
      }
      return arr;
    };

    return (
      <>
        <Row>
          <Col span={24}>
            {/*<ListFacilities
              loadServices={loadServices}
              pageSize={pageSize}
              filter={filter}
              setFilter={setFilter}
              facilities={facilities}
            />*/}
          </Col>
          <Col style={{ marginTop: 5 }} span={24}>
            <ListServices filter={filter} setFilter={setFilter} services={services} loadServices={loadServices} />
          </Col>
        </Row>
        <Table
          loading={isLoadingServices}
          pagination={{
            ...pagination,
            defaultPageSize: 20,
            total: services?.records ?? 0,
            pageSizeOptions: ['10', '20', '30', '50']
          }}
          onChange={handleTableChange}
          rowKey={record => record.serviceId}
          rowSelection={rowSelection}
          onRow={(record, rowIndex) => ({
            onClick: () => {
              if (isSelectDisabled) return;
              onSelect(record, !selectedRowKeys.includes(record.serviceId));
            }
          })}
          scroll={{ y: 350 }}
          columns={columns()}
          dataSource={services?.data}
        />
        <div style={{ marginTop: 5 }}>
          <Table
            title={() => (
              <>
                <div style={{ marginBottom: 5, marginTop: 5 }}>
                  <Button
                    type="primary"
                    onClick={start}
                    disabled={appointmentStatus !== 1 && appointmentStatus !== undefined}
                    loading={loading}
                  >
                    Chọn lại/Bỏ chọn
                  </Button>
                  <span style={{ marginLeft: 8, color: 'red', fontWeight: 400 }}>
                    Danh sách Chỉ định xét nghiệm đã chọn{' '}
                  </span>
                </div>
              </>
            )}
            scroll={{ y: 350 }}
            pagination={false}
            rowKey={record => record['serviceId']}
            columns={columns(true)}
            loading={isLoadingServicesSelected}
            dataSource={servicesDataSelectedState}
          />
        </div>
        <div style={{ marginTop: 5 }}>
          <div>
            <span>Tổng tiền giá gốc : </span>{' '}
            <span style={{ color: 'red' }}>{formatter.format(totalOriginPriceState)}</span>
          </div>
          <div>
            <span>Tổng tiền giá khu vực/đơn vị </span>{' '}
            <span style={{ color: 'red' }}>{formatter.format(totalSalePriceState)} </span>
          </div>
        </div>
      </>
    );
  };

  return {
    ListServicesForm
  };
}
