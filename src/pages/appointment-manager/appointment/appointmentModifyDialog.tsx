import { Button, Col, Modal, PageHeader, Row, Space, Spin, Tabs } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import './index.less';
import {
  BankOutlined,
  CloseOutlined,
  ExclamationCircleOutlined,
  HighlightOutlined,
  HistoryOutlined,
  HomeOutlined,
  OrderedListOutlined,
  SaveOutlined,
  StepForwardOutlined
} from '@ant-design/icons';
import { useDeleteAppointmentComments } from 'hooks/appointment/useDeleteAppointmentComments';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { AppointmentPatient } from 'common/interfaces/appointmentPatient.interface';
import { useGetWards } from 'hooks/ward/useGetWard';
import { useGetPatients } from 'hooks/patient/useGetPatient';
import { useGetSpecialist } from 'hooks/specialist/useGetSpecialist';
import { useGetFacilitys } from 'hooks/facility';
import { useGetStreetsLazy } from 'hooks/street';
import { useGetSiteStaffsLazy } from 'hooks/site-staff';
import {
  useGetAppointmentComments,
  useGetAppointmentWorkTimeHome,
  useGetAppointmentWorkTimeHospital,
  useSaveAppointmentComments
} from 'hooks/appointment';
import { AppointmentInput } from 'common/interfaces/input/appointmentInput.interfce';
import { useGetReasons } from 'hooks/reasons/useGetReasons';
import useGetModifyAppointmentForm from './useGetModifyAppointmentForm';
import { Appointment } from 'common/interfaces/appointment.interface';
import moment from 'moment';
import AppointmentActions from './appointmentActions';
import { Patient } from 'common/interfaces/patient.interface';
import { useGetRoutesLazy } from 'hooks/route';
import AssignmentPage from './assignAppoinment';
import { useGetDoctor } from 'hooks/doctor/useGetDoctor';
import { useGetPackage } from 'hooks/package';
import { useGetAllWorkTimeFacility } from 'hooks/worktimes/useGetAllWorkTimeFacility';
import { Channel } from 'common/interfaces/channel.interface';
import { useGetAllCountriesLazy } from 'hooks/appointment/useGetAllCountries';
import { functionCodeConstants } from 'constants/functions';
import { openNotificationRight } from 'utils/notification';
import { getStatus } from 'pages/advisory-manager/advisory-input/utils';

const TabPane = Tabs.TabPane;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};
interface AppointmentModifyDialogProps {
  visible: boolean;
  rowObj: any;
  onModify: (values: Partial<AppointmentInput>) => void;
  onBookingMultipleAppointments: (appointmentPatient: AppointmentPatient) => void;
  appointment?: Partial<Appointment>;
  onCancelAppointment: (id: any) => void;
  onForwardAppointment: (id: any) => void;
  onCancel: () => void;
  isLoading?: boolean;
  functionOb: any;
  channels: any;
  isLoadingChannels: boolean;
  assignStaffName?: string;
}

const AppointmentModifyDialog: FC<AppointmentModifyDialogProps> = ({
  visible,
  onModify,
  rowObj,
  appointment,
  onCancelAppointment,
  onForwardAppointment,
  onBookingMultipleAppointments,
  onCancel,
  isLoading,
  functionOb,
  channels,
  isLoadingChannels,
  assignStaffName
}) => {
  const [isBHYT, setIsBHYT] = useState(false);
  const [appointmentDate, setAppointmentDate] = useState('');
  const [streetId, setStreetId] = useState('');
  const [parentFacilityId, setParentFacilityId] = useState('');
  const [visibleAssigAppointment, setVisibleAssigAppointment] = useState(false);
  //confirm Loading
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  //Hook Lấy danh sách Mã thẻ KH (PID)
  const { patients, loadPatients, isLoadingPatients } = useGetPatients();
  // Hook Lấy danh sách Cán bộ báo lịch
  const { siteStaffs, loadSiteStaffs, isLoadingSiteStaffs } = useGetSiteStaffsLazy();
  //Hook Lấy danh sách Chuyên khoa khám
  const { specialists, loadSpecialist, isLoadingSpecialists } = useGetSpecialist();
  //Hook Lấy danh sách đường phố
  const { streets, loadStreets, isLoadingStreets } = useGetStreetsLazy();
  //Hook Lấy danh sách cung đường
  const { routesData, isLoadingRoutes, loadRoutes } = useGetRoutesLazy();
  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, loadProvinces, isLoadingProvince } = useGetProvincesLazy();
  //Hook Lấy danh sách đơn vị
  const { facilities, loadFacilities, isLoadingFacilities } = useGetFacilitys();
  //Hook Lấy danh sách Quận/Huyện
  const { districts, loadDistricts, isLoadingDistricts } = useGetDistricts();
  //Hook Lấy danh sách Xã/Phường
  const { wards, loadWards, isLoadingWards } = useGetWards();
  //Hook Lấy danh sách nơi khám tại viện
  const {
    facilities: facilitiesHos,
    isLoadingFacilities: isLoadingFacilitiesHos,
    loadFacilities: loadFacilitiesHos
  } = useGetFacilitys();
  //Hook Lấy danh sách nơi khám tại nhà
  const {
    facilities: facilitiesHome,
    isLoadingFacilities: isLoadingFacilitiesHome,
    loadFacilities: loadFacilitiesHome
  } = useGetFacilitys();
  //Hook Lấy danh lý do
  const { reasons, loadReasons, isLoadingReasons } = useGetReasons();
  //Hook Lấy danh sách khung giờ tại nhà
  const { appointmentWorkTimeHome, loadAppointmentWorkTimeHome } = useGetAppointmentWorkTimeHome();
  //Hook Lấy danh sách khung giờ tại viện
  const { appointmentWorkTimeHospital, loadAppointmentWorkTimeHospital } = useGetAppointmentWorkTimeHospital();
  //Hook Lấy khung giờ theo đơn vị
  const { allWorkTimeFacility, loadAllWorkTimeFacility, isLoadingAllWorkTimeFacility } = useGetAllWorkTimeFacility();
  //Hook lấy danh sách Ghi chú
  const { loadAppointmentComments, appointmentComments } = useGetAppointmentComments();
  //Hook Lấy danh sách quốc tịch
  const { pagingCountries, isLoadingCountries, loadCountries, errorCountries } = useGetAllCountriesLazy();
  //Hook xoá ghi chú
  const {
    deleteAppointmentComments,
    errorDeleteAppointmentComments,
    resultDeleteAppointmentComments
  } = useDeleteAppointmentComments();
  //Hook lưu ghi chú
  const {
    saveAppointmentComments,
    isLoadingSaveAppointmentComments,
    errorSaveAppointmentComments
  } = useSaveAppointmentComments();
  //Lấy  BS CTV
  const { doctor, isLoadingDoctor, loadDoctor } = useGetDoctor();
  //Lấy gói khám
  const { package: packageDetail, isLoadingPackage, loadPackage } = useGetPackage();

  const [methodType, setMethodType] = useState('HOME');
  const [activeKey, setActiveKey] = useState('HOME');
  const initialValues: Partial<Appointment> = {
    ...appointment,
    streetId: appointment?.streetId ? appointment?.streetId + '***' + appointment?.routeId : undefined,
    appointmentDate: moment(appointment?.appointmentDate, 'DD/MM/YYYY HH:mm:ss'),
    assignStaffName: assignStaffName,
    patient: {
      ...appointment?.patient,
      pid: appointment?.pid,
      birthDate: appointment?.patient?.birthDate
        ? moment(appointment?.patient?.birthDate, 'DD/MM/YYYY HH:mm:ss')
        : appointment?.patient?.birthYear
        ? appointment?.patient?.birthYear !== '0'
          ? moment('01/01/' + appointment?.patient?.birthYear, 'DD/MM/YYYY HH:mm:ss')
          : undefined
        : undefined,
      birthYear: appointment?.patient?.birthYear
        ? appointment?.patient?.birthYear !== '0'
          ? moment(appointment?.patient?.birthYear, 'YYYY')
          : undefined
        : appointment?.patient?.birthDate
        ? moment(moment(appointment?.patient?.birthDate, 'DD/MM/YYYY HH:mm:ss'), 'YYYY')
        : undefined,
      phone: appointment?.patient?.phone
        ? appointment?.patient?.phone.slice(0, 1) !== '0'
          ? '0' + appointment?.patient?.phone
          : appointment?.patient?.phone
        : undefined
    }
  };

  useEffect(() => {
    form.resetFields();
  }, [visible]);

  useEffect(() => {
    if (appointment?.streetId) {
      setStreetId(appointment.streetId);
    } else {
      setStreetId('');
    }
  }, [appointment]);

  useEffect(() => {
    loadProvinces();
    if (appointment != null) {
      if (appointment.patient?.provinceId) {
        loadDistricts({
          variables: {
            provinceId: appointment.patient.provinceId
          }
        });
      }
      if (appointment.patient?.districtId) {
        loadWards({
          variables: {
            districtId: appointment.patient.districtId
          }
        });
      }
      if (appointment.methodType) {
        setMethodType(appointment.methodType);
      }
      if (appointment?.examFacilityId && appointment?.appointmentDate && appointment?.methodType === 'HOME') {
        var parts = String(appointment.appointmentDate)
          ?.split(' ')[0]
          ?.split('/');
        loadAppointmentWorkTimeHome({
          variables: {
            data: {
              appointmentDate: parts[2] + '-' + parts[1] + '-' + parts[0],
              examFacilityId: appointment?.examFacilityId
            }
          }
        });
        /*loadAppointmentWorkTimeHospital({
          variables: {
            data: {
              appointmentDate: '1111-11-11'
            }
          }
        });*/
      } else if (
        appointment?.examFacilityId &&
        appointment?.appointmentDate &&
        appointment?.methodType === 'HOSPITAL'
      ) {
        var parts = String(appointment.appointmentDate)
          ?.split(' ')[0]
          ?.split('/');
        loadAppointmentWorkTimeHospital({
          variables: {
            data: {
              appointmentDate: parts[2] + '-' + parts[1] + '-' + parts[0],
              examFacilityId: appointment?.examFacilityId
            }
          }
        });
        /*loadAppointmentWorkTimeHome({
          variables: {
            data: {
              appointmentDate: '1111-11-11'
            }
          }
        });*/
      }
      loadStreets({
        variables: {
          page: 1,
          filtered: [
            {
              id: 'provinceId',
              value: appointment?.patient?.provinceId,
              operation: '=='
            },
            {
              id: 'status',
              value: '1',
              operation: '=='
            }
          ]
        }
      });
      loadRoutes({
        variables: {
          filtered: [
            {
              id: 'id',
              value: appointment?.routeId,
              operation: '=='
            },
            {
              id: 'status',
              value: '1',
              operation: '=='
            }
          ]
        }
      });
      loadAppointmentComments({
        variables: {
          filtered: [
            {
              id: 'appointmentId',
              value: appointment?.id,
              operation: '=='
            }
          ],
          sorted: [
            {
              id: 'createDate',
              desc: true
            }
          ]
        }
      });
      loadDoctor({
        variables: {
          id: appointment.indicatedDoctorId
        }
      });
      loadPackage({
        variables: {
          id: appointment.packages?.id
        }
      });
      if (!appointment.methodType || appointment.methodType === 'HOME') {
        loadFacilitiesHome({
          variables: {
            page: 1,
            filtered: [
              {
                id: 'isHome',
                value: '1',
                operation: '=='
              },
              {
                id: 'provinceIds',
                value: appointment.patient?.provinceId,
                operation: '~'
              },
              {
                id: 'status',
                value: '1',
                operation: '=='
              }
            ]
          }
        });
      } else if (appointment.methodType === 'HOSPITAL') {
        loadFacilitiesHos({
          variables: {
            page: 1,
            filtered: [
              {
                id: 'isHospital',
                value: '1',
                operation: '=='
              },
              {
                id: 'provinceIds',
                value: appointment.patient?.provinceId,
                operation: '~'
              },
              {
                id: 'status',
                value: '1',
                operation: '=='
              }
            ]
          }
        });
      }
      loadAllWorkTimeFacility({
        variables: {
          facilityId: appointment.examFacilityId
        }
      });
      form.setFieldsValue(initialValues);
    }
  }, [appointment]);

  useEffect(() => {
    loadPatients({
      variables: {
        page: 1
      }
    });
    loadSpecialist({
      variables: {
        pageSize: -1
      }
    });
    loadSiteStaffs({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
    loadReasons();
    loadCountries();
    loadFacilities({
      variables: {
        filtered: [
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
  }, []);

  const {
    Form,
    form,
    AppointmentSource,
    PhoneNumber,
    CustomerType,
    CustomerFullname,
    ReasonBooking,
    DateofBirth,
    BirthYear,
    CollaboratorDoctor,
    Gender,
    MedicalExaminationPackage,
    ContactAddress,
    Test,
    Provinces,
    Districts,
    Wards,
    Street4Home,
    TestNotes,
    Email,
    TotalPrice,
    IdentityCard,
    TotalPriceDiscount,
    GeneticCode,
    ScheduleReporter,
    PID,
    MedicalPlace,
    ObjectSend,
    Specialists,
    AppointmentWorkTime,
    AppointmentObject,
    AppointmentTimeFrame,
    AppointmentNotes,
    AppointmentNotesTable,
    MgsContent,
    AppointmentDate,
    Route,
    FacilityParent,
    Nationality,
    BHYT,
    ReasonBookingStr,
    AssignStaff
  } = useGetModifyAppointmentForm({
    name: 'modify-appointment-form',
    responsive: true,
    appointmentInput: initialValues,
    isDisable: false,
    patternIdNo: null
  });

  const onSubmit = async () => {
    if (activeKey === 'NOTE') {
      openNotificationRight(
        'Sau khi nhập ghi chú, vui lòng chọn lại loại Lịch hẹn Tại nhà/Tại viện để lưu dữ liệu.',
        'warning'
      );
      return;
    }
    const values = await form.validateFields();
    let channelType = values['channelType'];
    let channel: Channel = channels.find(function(element: Channel) {
      return element.code === channelType;
    });
    const methodType = form.getFieldValue('methodType') ? form.getFieldValue('methodType') : 'HOME';
    delete values['parentFacilityId'];
    delete values['assignStaffName'];
    let streetId = values['streetId']?.split('***')[0];
    let routeId = values['streetId']?.split('***')[1];
    onModify({
      id: appointment?.id,
      ...values,
      methodType,
      appointmentDate: (values.appointmentDate as moment.Moment)?.format('DD/MM/YYYY'),
      streetId,
      routeId,
      assignStaffId: appointment?.assignStaffId,
      channelId: channel?.id,
      channelType,
      originalAmount: form.getFieldValue('originalAmount'),
      totalAmount: form.getFieldValue('totalAmount'),
      services: values.services
        ? values.services?.map(val => ({
            objectId: val.id,
            originalPrice: val.originalPrice,
            salePrice: val.salePrice
            /*indicatedDoctorId: JSON.parse(String(localStorage.getItem('user')))
          ? JSON.parse(String(localStorage.getItem('user')))['id']
          : ''*/
          }))
        : undefined,
      packages: values.packages?.id
        ? {
            objectId: values.packages?.id,
            originalPrice: form.getFieldValue('originalAmount'),
            salePrice: form.getFieldValue('totalAmount')
          }
        : undefined,
      patient: {
        ...values.patient,
        birthDate: (values.patient?.birthDate as moment.Moment)?.format('DD/MM/YYYY'),
        birthYear: values.patient?.birthYear
          ? (values.patient?.birthYear as moment.Moment)?.format('YYYY')
          : (values.patient?.birthDate as moment.Moment)?.format('YYYY')
      },
      pid: values.patient?.pid,
      appointmentType: values.packages?.id ? 'PACKAGE' : 'SCHEDULE'
    });
    setConfirmLoading(true);
    setTimeout(() => {
      setConfirmLoading(false);
    }, 5000);
  };

  const handleChangePatient = (values?: Partial<Patient>) => {
    if (values?.provinceId) {
      loadDistricts({
        variables: {
          provinceId: values.provinceId
        }
      });
    }
    if (values?.districtId) {
      loadWards({
        variables: {
          districtId: values.districtId
        }
      });
    }
  };

  const handleDeleteAppointmentComment = async value => {
    await deleteAppointmentComments(value);
    loadAppointmentComments({
      variables: {
        filtered: [
          {
            id: 'appointmentId',
            value: appointment?.id,
            operation: '=='
          }
        ],
        sorted: [
          {
            id: 'createDate',
            desc: true
          }
        ]
      }
    });
  };

  const handleSaveAppointmentComment = async values => {
    await saveAppointmentComments(values);
    loadAppointmentComments({
      variables: {
        filtered: [
          {
            id: 'appointmentId',
            value: appointment?.id,
            operation: '=='
          }
        ],
        sorted: [
          {
            id: 'createDate',
            desc: true
          }
        ]
      }
    });
  };

  const handleChangeStreet = (streetId?: string, routeId?: string) => {
    setStreetId(streetId ? streetId : '');
    loadRoutes({
      variables: {
        filtered: [
          {
            id: 'id',
            value: routeId,
            operation: '=='
          },
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
  };

  const handleForwardAppointment = async appointmentId => {
    await onForwardAppointment(appointmentId);
  };

  const handleCancleAppointment = async appointmentId => {
    await onCancelAppointment(appointmentId);
  };

  /*useEffect(() => {
  initData();
}, [initData]);*/
  const handleMenuClick = keyMenu => {
    keyMenu = String(keyMenu);
    if (keyMenu === '1') {
    } else if (keyMenu === '2') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn huỷ lịch hẹn đang thao tác ?',
        okText: 'Đồng ý',
        cancelText: 'Hủy bỏ',
        onOk() {
          handleCancleAppointment(appointment?.id);
        },
        onCancel() {}
      });
    } else if (keyMenu === '3') {
      if (appointment?.status === 13) {
        openNotificationRight('Không thể đặt nhiều lịch đối với bản ghi đã huỷ.', 'warning');
        return;
      }
      onBookingMultipleAppointments(rowObj);
    } else if (keyMenu === '4') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn chuyển lịch hẹn đang thao tác' + '' + ' cho bác sĩ tư vấn ?',
        okText: 'Đồng ý',
        cancelText: 'Hủy bỏ',
        onOk() {
          if (appointment?.status === 13) {
            openNotificationRight('Không thể chuyển Bác sĩ tư vấn đối với bản ghi đã huỷ.', 'warning');
            return;
          }
          handleForwardAppointment(appointment?.id);
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    } else if (keyMenu === '5') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn xóa lịch hẹn đã chọn' + '' + ' ?',
        cancelText: 'Hủy bỏ',
        onOk() {
          //handleDeleteAppointment(appointmentDetail.appointmentId);
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    }
    //setAppointmentDetail({} as AppointmentPatient);
  };

  return (
    <Modal
      maskClosable={false}
      style={{ marginTop: 5 }}
      centered
      width={1500}
      title="Cập nhật Lịch hẹn"
      visible={visible}
      okText="Lưu thông tin"
      cancelText="Hủy"
      onOk={onSubmit}
      confirmLoading={confirmLoading}
      onCancel={() => {
        onCancel();
      }}
      okButtonProps={{
        disabled: functionOb[functionCodeConstants.TD_LH_SUALH] && !confirmLoading ? false : true,
        color: '#26a661'
      }}
    >
      <Modal
        maskClosable={false}
        style={{ marginTop: 5 }}
        centered
        width={1000}
        title="Phân lịch hẹn"
        visible={visibleAssigAppointment}
        onCancel={() => {
          setVisibleAssigAppointment(false);
        }}
      >
        <AssignmentPage
          appointmentId={appointment?.id}
          facilityId={''}
          appointmentDate={appointmentDate !== '' ? appointmentDate : appointment?.appointmentDate}
          parentFacilityId={appointment?.examFacilityId ? appointment?.examFacilityId : ''}
        />
      </Modal>
      <Spin tip="Đang tải..." spinning={isLoading}>
        <div style={{ float: 'left', color: 'red', fontWeight: 'bold' }}></div>
        <div style={{ float: 'right' }}>
          {/*<Button icon={<AppstoreAddOutlined />} style={{ marginLeft: 10 }} type="primary">
          Đặt nhiều lịch
        </Button>*/}
        </div>
        <PageHeader
          ghost={false}
          // tags=
          subTitle={
            <span>
              <span className="tt">
                <span style={{ color: 'black' }}>{'Trạng thái: '}</span>
                <span>{getStatus(appointment?.status)}</span>
              </span>
              <span className="kd">
                <span style={{ color: 'black', marginLeft: '7px' }}>{'Mã lịch hẹn: '}</span>
                <span style={{ color: 'red', marginRight: 15 }}>
                  {appointment?.appointmentHisId ? appointment?.appointmentHisId : 'Chưa có mã lịch hẹn'}
                </span>
              </span>
            </span>
          }
          extra={[
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_HUY] && !confirmLoading ? false : true}
              onClick={() => {
                handleMenuClick(2);
              }}
              icon={<CloseOutlined />}
              type="primary"
              danger
              key="2"
            >
              Hủy lịch
            </Button>,
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_DATNHIEU] && !confirmLoading ? false : true}
              onClick={() => {
                handleMenuClick(3);
              }}
              icon={<OrderedListOutlined />}
              key="3"
            >
              Đặt nhiều lịch
            </Button>,
            <Button
              onClick={() => {
                handleMenuClick(4);
              }}
              icon={<StepForwardOutlined />}
              key="4"
              className="btn-green"
              type="primary"
              disabled={
                functionOb[functionCodeConstants.TD_LH_CBS] && !confirmLoading && appointment?.status !== 13
                  ? false
                  : true
              }
            >
              Chuyển BS tư vấn
            </Button>,
            <Button
              onClick={() => {
                onSubmit();
              }}
              icon={<SaveOutlined />}
              key="5"
              type="primary"
              disabled={functionOb[functionCodeConstants.TD_LH_SUALH] && !confirmLoading ? false : true}
            >
              Lưu thông tin
            </Button>
          ]}
        ></PageHeader>
        <div style={{ clear: 'both' }}></div>
        <Form {...formItemLayout} className="ant-advanced-add-form" style={{ marginTop: 5 }}>
          <Col className="gutter-row" span={12}>
            <Row>
              <PID patientList={patients} />
              <AppointmentSource
                isLoading={isLoadingChannels}
                onChangeAppointmentSource={value => {}}
                channelList={channels}
              />
              <PhoneNumber
                handleChangeProvince={provinceId => {
                  if (provinceId) {
                    if (methodType === 'HOME') {
                      loadFacilitiesHome({
                        variables: {
                          page: 1,
                          filtered: [
                            {
                              id: 'isHome',
                              value: '1',
                              operation: '=='
                            },
                            {
                              id: 'provinceIds',
                              value: provinceId,
                              operation: '~'
                            },
                            {
                              id: 'status',
                              value: '1',
                              operation: '=='
                            }
                          ]
                        }
                      });
                    } else if (methodType === 'HOSPITAL') {
                      loadFacilitiesHos({
                        variables: {
                          page: 1,
                          filtered: [
                            {
                              id: 'isHospital',
                              value: '1',
                              operation: '=='
                            },
                            {
                              id: 'provinceIds',
                              value: provinceId,
                              operation: '~'
                            },
                            {
                              id: 'status',
                              value: '1',
                              operation: '=='
                            }
                          ]
                        }
                      });
                    }
                    loadDistricts({
                      variables: {
                        provinceId: provinceId
                      }
                    });
                    loadStreets({
                      variables: {
                        page: 1,
                        filtered: [
                          {
                            id: 'provinceId',
                            value: provinceId,
                            operation: '=='
                          },
                          {
                            id: 'status',
                            value: '1',
                            operation: '=='
                          }
                        ]
                      }
                    });
                  }
                }}
                handleChangePatient={handleChangePatient}
              />
              <CustomerFullname />
              <Gender />
              <DateofBirth />
              <BirthYear />
              <ContactAddress />
              <Provinces
                isLoading={isLoadingProvince}
                handleChangeProvince={provinceId => {
                  if (provinceId !== '') {
                    if (methodType === 'HOME') {
                      loadFacilitiesHome({
                        variables: {
                          page: 1,
                          filtered: [
                            {
                              id: 'isHome',
                              value: '1',
                              operation: '=='
                            },
                            {
                              id: 'provinceIds',
                              value: provinceId,
                              operation: '~'
                            },
                            {
                              id: 'status',
                              value: '1',
                              operation: '=='
                            }
                          ]
                        }
                      });
                    } else if (methodType === 'HOSPITAL') {
                      loadFacilitiesHos({
                        variables: {
                          page: 1,
                          filtered: [
                            {
                              id: 'isHospital',
                              value: '1',
                              operation: '=='
                            },
                            {
                              id: 'provinceIds',
                              value: provinceId,
                              operation: '~'
                            },
                            {
                              id: 'status',
                              value: '1',
                              operation: '=='
                            }
                          ]
                        }
                      });
                    }
                    loadDistricts({
                      variables: {
                        provinceId: provinceId
                      }
                    });
                    loadStreets({
                      variables: {
                        page: 1,
                        filtered: [
                          {
                            id: 'provinceId',
                            value: provinceId,
                            operation: '=='
                          },
                          {
                            id: 'status',
                            value: '1',
                            operation: '=='
                          }
                        ]
                      }
                    });
                  }
                }}
                provinceList={provinces}
              />
              <Districts
                isLoading={isLoadingDistricts}
                handleChangeDistrict={districtId => {
                  if (districtId !== '') {
                    loadWards({
                      variables: {
                        districtId: districtId
                      }
                    });
                  }
                }}
                districtList={form.getFieldValue(['patient', 'provinceId']) ? districts : []}
              />
              <Wards
                isLoading={isLoadingWards}
                handleChangeWard={() => {}}
                wardList={form.getFieldValue(['patient', 'districtId']) ? wards : []}
              />
              <IdentityCard />
              <Email />
              <Nationality isLoading={isLoadingCountries} countries={pagingCountries?.data} setPatternIdNo={null} />
            </Row>
          </Col>
          <Col className="gutter-row" span={12}>
            <Row>
              <CustomerType />
              <ReasonBooking isLoading={isLoadingReasons} reasonList={reasons} />
              <CollaboratorDoctor appointmentStatus={appointment?.status} functionOb={functionOb} doctor={doctor} />
              <MedicalExaminationPackage
                functionOb={functionOb}
                appointmentStatus={appointment?.status}
                status={appointment?.status}
                methodType={activeKey}
                provinceId={
                  (form.getFieldValue('methodType') ?? 'HOME') === 'HOSPITAL'
                    ? facilities?.find(facility => facility.id === form.getFieldValue('examFacilityId'))?.provinceId
                    : form.getFieldValue(['patient', 'provinceId'])
                }
                packageObj={packageDetail}
              />
              <Test
                appointmentStatus={appointment?.status}
                functionOb={functionOb}
                status={appointment?.status}
                services={appointment?.services}
                facilityList={methodType === 'HOSPITAL' ? facilitiesHos : facilitiesHome}
              />
              <TestNotes />
              <GeneticCode />
              <ScheduleReporter
                isLoading={isLoadingSiteStaffs}
                siteStaffList={siteStaffs}
                handleChangeSiteStaff={() => {}}
              />
              {/* <TotalPrice services={appointment?.services} />
              <TotalPriceDiscount /> */}
            </Row>
          </Col>
          <Tabs
            defaultActiveKey={methodType}
            activeKey={methodType}
            type="card"
            onTabClick={activeKey => {
              //handleTabClick(tabIndex);
              //setTabIndexSelected(String(tabIndex));
              //console.log('tabIndexSelected : ', tabIndexSelected);
              setActiveKey(activeKey);
              setMethodType(activeKey);
              if (activeKey === 'HOME') {
                if (form.getFieldValue(['patient', 'provinceId'])) {
                  loadFacilitiesHome({
                    variables: {
                      page: 1,
                      filtered: [
                        {
                          id: 'isHome',
                          value: '1',
                          operation: '=='
                        },
                        {
                          id: 'provinceIds',
                          value: form.getFieldValue(['patient', 'provinceId']),
                          operation: '~'
                        },
                        {
                          id: 'status',
                          value: '1',
                          operation: '=='
                        }
                      ]
                    }
                  });
                } else {
                  loadFacilitiesHome({
                    variables: {
                      page: 1,
                      filtered: [
                        {
                          id: 'isHome',
                          value: '1',
                          operation: '=='
                        },
                        {
                          id: 'provinceIds',
                          value: appointment?.patient?.provinceId,
                          operation: '~'
                        },
                        {
                          id: 'status',
                          value: '1',
                          operation: '=='
                        }
                      ]
                    }
                  });
                }
              } else if (activeKey === 'HOSPITAL') {
                if (form.getFieldValue(['patient', 'provinceId'])) {
                  loadFacilitiesHos({
                    variables: {
                      page: 1,
                      filtered: [
                        {
                          id: 'isHospital',
                          value: '1',
                          operation: '=='
                        },
                        {
                          id: 'provinceIds',
                          value: form.getFieldValue(['patient', 'provinceId']),
                          operation: '~'
                        },
                        {
                          id: 'status',
                          value: '1',
                          operation: '=='
                        }
                      ]
                    }
                  });
                } else {
                  loadFacilitiesHos({
                    variables: {
                      page: 1,
                      filtered: [
                        {
                          id: 'isHospital',
                          value: '1',
                          operation: '=='
                        },
                        {
                          id: 'provinceIds',
                          value: appointment?.patient?.provinceId,
                          operation: '~'
                        },
                        {
                          id: 'status',
                          value: '1',
                          operation: '=='
                        }
                      ]
                    }
                  });
                }
              }
              if (activeKey !== 'NOTE') {
                if (activeKey?.toUpperCase() !== appointment?.methodType?.toUpperCase()) {
                  form.setFieldsValue({
                    examFacilityId: undefined,
                    sendFacilityId: undefined,
                    specialistId: undefined,
                    workTimeId: undefined,
                    appointmentDate: moment(),
                    streetId: undefined,
                    routeId: undefined,
                    healthInsuranceCard: undefined,
                    reasonNote: undefined,
                    appointmentObject: undefined,
                    assignStaffName: undefined
                  });
                } else if (activeKey?.toUpperCase() === appointment?.methodType?.toUpperCase()) {
                  form.setFieldsValue({
                    examFacilityId: appointment?.examFacilityId,
                    sendFacilityId: appointment?.sendFacilityId,
                    specialistId: appointment?.specialistId,
                    workTimeId: appointment?.workTimeId,
                    appointmentDate: moment(appointment?.appointmentDate, 'DD/MM/YYYY HH:mm:ss'),
                    streetId: appointment?.streetId + '***' + appointment?.routeId,
                    routeId: appointment?.routeId,
                    healthInsuranceCard: appointment?.healthInsuranceCard,
                    reasonNote: appointment?.reasonNote,
                    appointmentObject: appointment?.appointmentObject,
                    assignStaffName: appointment?.assignStaffName
                  });
                }
                form.setFieldsValue({
                  methodType: activeKey
                });
              }
            }}
            destroyInactiveTabPane={true}
          >
            <TabPane
              tab={
                <span>
                  <HomeOutlined />
                  Đặt lịch tại nhà
                </span>
              }
              key="HOME"
            >
              <Row className="fix-row">
                <Col className="gutter-row" span={12} style={{ marginTop: 15 }}>
                  <Row>
                    <FacilityParent
                      isLoading={isLoadingFacilitiesHome}
                      facilityParentList={form.getFieldValue(['patient', 'provinceId']) ? facilitiesHome : []}
                      handleChangeFacilityParent={(parentFacilityId, appointmentDate) => {
                        if (parentFacilityId) {
                          loadAllWorkTimeFacility({
                            variables: {
                              facilityId: parentFacilityId
                            }
                          });
                        }
                        if (appointmentDate && parentFacilityId) {
                          setParentFacilityId(parentFacilityId);
                          loadAppointmentWorkTimeHome({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId: parentFacilityId
                              }
                            }
                          });
                        } else {
                          if (appointmentDate) {
                            loadAppointmentWorkTimeHome({
                              variables: {
                                data: {
                                  appointmentDate,
                                  examFacilityId: appointment?.examFacilityId
                                }
                              }
                            });
                          } else {
                            var parts = String(appointment?.appointmentDate)
                              ?.split(' ')[0]
                              ?.split('/');
                            if (parts[0] && parts[1] && parts[2]) {
                              loadAppointmentWorkTimeHome({
                                variables: {
                                  data: {
                                    appointmentDate: parts[2] + '-' + parts[1] + '-' + parts[0],
                                    examFacilityId: parentFacilityId
                                  }
                                }
                              });
                            }
                          }
                        }
                      }}
                    />
                    <Street4Home
                      isLoading={isLoadingStreets}
                      streetList={form.getFieldValue(['patient', 'provinceId']) ? streets : []}
                      routeId={appointment?.routeId}
                      handleChangeStreet={handleChangeStreet}
                    />
                    <Route street={streetId} routesData={routesData} routeId={appointment?.routeId} />
                    <AppointmentDate
                      isHospital={false}
                      onChange={(value: any, dateString: any) => {}}
                      handleChangAppointmentDate={(examFacilityId, appointmentDate) => {
                        setAppointmentDate(appointmentDate);
                        if (appointmentDate && examFacilityId) {
                          loadAppointmentWorkTimeHome({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId
                              }
                            }
                          });
                        } else {
                          if (appointmentDate) {
                            loadAppointmentWorkTimeHome({
                              variables: {
                                data: {
                                  appointmentDate,
                                  examFacilityId: appointment?.examFacilityId
                                }
                              }
                            });
                          } else {
                            loadAppointmentWorkTimeHome({
                              variables: {
                                data: {
                                  appointmentDate: moment(appointment?.appointmentDate).format('YYYY-MM-DD'),
                                  examFacilityId
                                }
                              }
                            });
                          }
                        }
                      }}
                    />
                    <AppointmentWorkTime
                      isLoading={isLoadingAllWorkTimeFacility}
                      appointmentWorkTime={form.getFieldValue('examFacilityId') ? allWorkTimeFacility : []}
                      workTimeId={appointment?.workTimeId}
                    />
                    <ReasonBookingStr />
                    <AssignStaff />
                    <MgsContent
                      provinces={provinces}
                      districts={districts}
                      wards={wards}
                      functionOb={functionOb}
                      appointmentHisId={appointment?.appointmentHisId}
                      doctorCode={doctor?.doctorHisId}
                      workTimes={appointmentWorkTimeHome?.['data']}
                      onAssignAppointment={() => {
                        setVisibleAssigAppointment(true);
                      }}
                    />
                  </Row>
                </Col>
                <Col className="gutter-row" span={12}>
                  <AppointmentTimeFrame appointmentWorkTimeData={appointmentWorkTimeHome?.['data']} />
                </Col>
              </Row>
            </TabPane>
            <TabPane
              tab={
                <span>
                  <BankOutlined />
                  Đặt lịch tại viện
                </span>
              }
              key="HOSPITAL"
            >
              <Row className="fix-row">
                <Col className="gutter-row" span={12} style={{ marginTop: 15 }}>
                  <Row>
                    <MedicalPlace
                      isLoading={isLoadingFacilitiesHos}
                      facilityList={form.getFieldValue(['patient', 'provinceId']) ? facilitiesHos : []}
                      methodType={methodType}
                      handleChangeMedicalPlace={(examFacilityId, appointmentDate) => {
                        if (examFacilityId) {
                          loadAllWorkTimeFacility({
                            variables: {
                              facilityId: examFacilityId
                            }
                          });
                        }
                        if (appointmentDate && examFacilityId) {
                          loadAppointmentWorkTimeHospital({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId
                              }
                            }
                          });
                        } else if (appointmentDate) {
                          loadAppointmentWorkTimeHospital({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId: appointment?.examFacilityId
                              }
                            }
                          });
                        } else {
                          let appointmentDateNoTime = String(appointment?.appointmentDate).split(' ')[0];
                          let appointmentDateNoTimeArr = appointmentDateNoTime.split('/');
                          loadAppointmentWorkTimeHospital({
                            variables: {
                              data: {
                                appointmentDate:
                                  appointmentDateNoTimeArr[2] +
                                  '-' +
                                  appointmentDateNoTimeArr[1] +
                                  '-' +
                                  appointmentDateNoTimeArr[0],
                                examFacilityId
                              }
                            }
                          });
                        }
                      }}
                    />
                    <ObjectSend isLoading={isLoadingFacilities} facilityList={facilities} />
                    <Specialists isLoading={isLoadingSpecialists} specialists={specialists} />
                    <AppointmentDate
                      isHospital={true}
                      onChange={(value: any, dateString: any) => {}}
                      handleChangAppointmentDate={(examFacilityId, appointmentDate) => {
                        //setAppointmentDate(appointmentDate);
                        if (appointmentDate && examFacilityId) {
                          loadAppointmentWorkTimeHospital({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId
                              }
                            }
                          });
                        } else {
                          if (appointmentDate) {
                            loadAppointmentWorkTimeHospital({
                              variables: {
                                data: {
                                  appointmentDate,
                                  examFacilityId: appointment?.examFacilityId
                                }
                              }
                            });
                          } else {
                            loadAppointmentWorkTimeHospital({
                              variables: {
                                data: {
                                  appointmentDate: moment(appointment?.appointmentDate).format('YYYY-MM-DD'),
                                  examFacilityId
                                }
                              }
                            });
                          }
                        }
                      }}
                    />
                    <AppointmentWorkTime
                      isLoading={isLoadingAllWorkTimeFacility}
                      appointmentWorkTime={allWorkTimeFacility}
                      workTimeId={appointment?.workTimeId}
                    />
                    <AppointmentObject
                      handleChange={(value: boolean) => {
                        setIsBHYT(value);
                      }}
                      appointmentObjects={{}}
                    />
                    <BHYT isRequired={isBHYT} />
                    <ReasonBookingStr />
                  </Row>
                </Col>
                <Col className="gutter-row" span={12}>
                  <AppointmentTimeFrame appointmentWorkTimeData={appointmentWorkTimeHospital?.['data']} />
                </Col>
              </Row>
            </TabPane>
            <TabPane
              tab={
                <span>
                  <HighlightOutlined />
                  Ghi chú lịch hẹn
                </span>
              }
              key="NOTE"
            >
              <Row className="fix-row">
                <Col className="gutter-row" span={12} style={{ marginTop: 15 }}>
                  <Row>
                    <AppointmentNotes
                      onSaveComment={values => {
                        handleSaveAppointmentComment({
                          data: {
                            appointmentId: appointment?.id,
                            ...values
                          }
                        });
                      }}
                    />
                  </Row>
                </Col>
                <Col className="gutter-row" span={12}>
                  <AppointmentNotesTable
                    appointmentComments={appointmentComments}
                    onDelete={id => {
                      handleDeleteAppointmentComment({ id });
                    }}
                  />
                </Col>
              </Row>
            </TabPane>
          </Tabs>
        </Form>
        <Form className="ant-advanced-add-form" style={{ marginTop: 5 }}>
          <Col span={24}>
            {appointment?.id && (
              <>
                <Space>
                  <HistoryOutlined /> <span style={{ fontWeight: 'bold' }}>Lịch sử thao tác lịch hẹn</span>
                </Space>
                <AppointmentActions appointmentId={appointment?.id} />
              </>
            )}
          </Col>
        </Form>
      </Spin>
    </Modal>
  );
};

export default AppointmentModifyDialog;
