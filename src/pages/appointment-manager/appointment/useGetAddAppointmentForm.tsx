import React, { FC, useEffect, useRef, useState } from 'react';
import {
  Form,
  Input,
  Col,
  Row,
  Select,
  DatePicker,
  Button,
  Modal,
  Table,
  InputNumber,
  Image,
  List,
  Checkbox,
  Radio
} from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import moment from 'moment';
import useGetMedicalExaminationTestForm from './useGetMedicalExaminationTestForm';
import useGetMedicalPackageForm from './useGetMedicalPackageForm';
import {
  PhoneOutlined,
  UserOutlined,
  ContainerOutlined,
  NodeIndexOutlined,
  SaveOutlined,
  SearchOutlined,
  ExclamationCircleOutlined,
  DeleteOutlined
} from '@ant-design/icons';
import { Ward } from 'common/interfaces/ward.interface';
import { District } from 'common/interfaces/district.interface';
import { Province } from 'common/interfaces/province.interface';
import { Patient } from 'common/interfaces/patient.interface';
import { Specialist } from 'common/interfaces/specialist.interface';
import { Street } from 'common/interfaces/street.interface';
import { Facility } from 'common/interfaces/facility.interface';
import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import { useGetRoutesLazy } from 'hooks/route';
import useGetCollaboratorDoctorForm from './useGetCollaboratorDoctorForm';
import { AppointmentComment } from 'common/interfaces/appointmentComment.interface';
import { Channel } from 'common/interfaces/channel.interface';
import { WorkTime } from 'common/interfaces/WorkTime.interface';
import { Reason } from 'common/interfaces/reason.interface';
import { openNotificationRight } from 'utils/notification';
import { ServiceView } from 'common/interfaces/service.interface';
import { Appointment } from 'common/interfaces/appointment.interface';
import AppointmentPhoneSearch from '../../common/appointmentPhoneSearch';
import { Country } from 'common/interfaces/country.interface';
import { functionCodeConstants } from 'constants/functions';
import { APPOINTMENT_STATUS } from 'constants/appointment.constants';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import FormItem from 'antd/lib/form/FormItem';
import { formatDateTyping } from 'utils/datetime';
const { Option } = Select;
const { TextArea } = Input;
const CheckboxGroup = Checkbox.Group;
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
  xxl: 12
};

const wrapperColAllCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const wrapperColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 20,
  xl: 20,
  xxl: 20
};

const wrapperColOneItemNew: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 19,
  xl: 19,
  xxl: 19
};

const labelColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 4,
  xl: 4,
  xxl: 4
};

const labelColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperSDTInput: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 9,
  xl: 9,
  xxl: 9
};

const wrapperSDT: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 16,
  xl: 16,
  xxl: 16
};

interface AddScheduleFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?: boolean;
  /** Initial form data */
  appointmentInput?: Partial<Appointment>;
  isCreate?: boolean;
  isNewDialog?: boolean;
  visible?: boolean;
}

export default function useGetAddAppointmentForm({
  required = false,
  responsive = false,
  name = 'form',
  appointmentInput,
  isNewDialog,
  visible
}: AddScheduleFormProps) {
  const [first, setFirst] = useState(false);
  const [patternIdNo, setPatternIdNo] = useState(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
  useEffect(() => {
    setPatternIdNo(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
  }, [visible]);
  useEffect(() => {
    formInstance.validateFields([['patient', 'idNo']]);
  }, [patternIdNo]);
  const disRefC: any = useRef(null);
  const quRFC: any = useRef(null);
  let countMedicalExamination = 0;
  let countTest = 0;
  const [routeId, setRouteId] = useState('');
  const [appointmentDate, setAppointmentDate] = useState('');
  let { routesData, isLoadingRoutes, loadRoutes } = useGetRoutesLazy();
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<Partial<Appointment>>();
  const { ListServicesForm } = useGetMedicalExaminationTestForm({ name: 'serviceForm', responsive: true });
  const { ListPackageForm } = useGetMedicalPackageForm({ name: 'medicalPackageForm', responsive: true });
  const { ListCollaboratorDoctorForm } = useGetCollaboratorDoctorForm({
    name: 'collaboratorDoctorForm',
    responsive: true
  });
  //Hook Lấy danh sách Quận/Huyện
  const { districts, loadDistricts, isLoadingDistricts } = useGetDistricts();
  const [districCode, setDistricCode]: any = useState();
  const [checkIdNo, setCheckIdNo] = useState(false);
  // const [idNo, setIdNo] = useState("");
  // const [nationalityT, setNatonalityT]: any = useState("VN");
  const inputRef: any = useRef(null);

  useEffect(() => {
    if (districCode && districts?.length > 0) {
      var disId = '';
      districts?.map(item => {
        if (item.code == districCode) {
          disId = item.id;
        }
      });
      formInstance.setFieldsValue({
        patient: {
          districtId: disId
        }
      });
    }
  }, [districCode, districts]);

  // useEffect(()=>{
  //   setCheckIdNo(validateidNo(idNo,nationalityT));
  // },[idNo,nationalityT])

  const validateidNo = (idNo, nationalityInput) => {
    if (!idNo || !nationalityInput) {
      return true;
    }
    if (idNo && nationalityInput == 'VN' && (idNo.length == 9 || idNo.length == 12 || idNo.length == 8)) {
      return true;
    } else if (idNo && nationalityInput != 'VN' && idNo.length >= 6 && idNo.length <= 13) {
      return true;
    }
    return false;
  };

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const onValuesChange = (changedValues: any, values: Appointment) => {};

    return (
      <Form
        {...props}
        // {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={{
          ...appointmentInput,
          appointmentDate: appointmentInput?.appointmentDate ? appointmentInput?.appointmentDate : moment()
        }}
        onValuesChange={onValuesChange}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Ma lich hen
  const AppointmentCode: FC = () => {
    const appointmentCode = (
      <>
        <Form.Item className="appoiment-cpn" name="appointmentScheduleCode" label="Mã lịch hẹn">
          {' '}
          <>
            <Input disabled={true} placeholder="" />
          </>
        </Form.Item>
      </>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentCode}</Col> : appointmentCode;
  };
  //Trang thai lich
  const AppointmentStatus: FC = () => {
    const appointmentStatus = (
      <Form.Item
        className="appoiment-cpn"
        name="appointmentScheduleCode"
        label="Trạng thái lịch"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select defaultValue={APPOINTMENT_STATUS.DA_XAC_NHAN.key} disabled placeholder="-- Chọn trạng thái --">
          <Option value={APPOINTMENT_STATUS.CHO_XAC_NHAN.key}>{APPOINTMENT_STATUS.CHO_XAC_NHAN.value}</Option>
          <Option value={APPOINTMENT_STATUS.DA_XAC_NHAN.key}>{APPOINTMENT_STATUS.DA_XAC_NHAN.value}</Option>
          <Option value={APPOINTMENT_STATUS.CHO_TU_VAN_DAU_VAO.key}>
            {APPOINTMENT_STATUS.CHO_TU_VAN_DAU_VAO.value}
          </Option>
          <Option value={APPOINTMENT_STATUS.DA_TU_VAN_DAU_VAO.key}>Đã phân lịch</Option>
          <Option value={APPOINTMENT_STATUS.DA_CHECK.key}>{APPOINTMENT_STATUS.DA_CHECK.value}</Option>
          <Option value={APPOINTMENT_STATUS.DA_LAY_MAU.key}>{APPOINTMENT_STATUS.DA_LAY_MAU.value}</Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentStatus}</Col> : appointmentStatus;
  };
  //Nguon lich hen
  interface AppointmentSourceProps {
    channelList: Channel[];
    isLoading: boolean;
    onChangeAppointmentSource: (channelType: string) => void;
  }
  const AppointmentSource: FC<AppointmentSourceProps> = ({ channelList, onChangeAppointmentSource, isLoading }) => {
    const handleChangeAppointmentSource = (value, event) => {};
    const appointmentSource = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Nguồn lịch hẹn:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden ">
          <Form.Item
            name="channelType"
            rules={[
              {
                required: true,
                message: 'Nguồn lịch hẹn không được để trống.'
              }
            ]}
          >
            <Select
              dropdownStyle={{ position: 'absolute', top: '0 !important' }}
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Nguồn lịch hẹn --"
              onSelect={(value, event) => {
                handleChangeAppointmentSource(value, event);
              }}
            >
              {channelList?.map(channel => (
                <Select.Option key={channel.id} value={channel.code}>
                  {channel.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentSource}</Col> : appointmentSource;
  };
  //So dien thoai
  interface PhoneNumberProps {
    handleChangePatient: (values?: Partial<Patient>) => void;
    handleChangeProvince: (provinceId: string) => void;
    provinces?;
    methodType?: any;
    setDisabledField?: (value: boolean) => void;
    disabledField?: boolean;
    loadStreets?: (value: any) => void;
  }
  const PhoneNumber: FC<PhoneNumberProps> = ({
    handleChangePatient,
    setDisabledField,
    handleChangeProvince,
    provinces,
    loadStreets,
    methodType,
    disabledField
  }) => {
    const [isCalling, setIsCalling] = useState(false);
    const [isSearching, setIsSearching] = useState(false);
    const onApplyPID = pid => {
      setIsSearching(false);
      formInstance.setFieldsValue({
        patient: {
          pid: pid
        }
      });
    };

    const onApply = (values?: Partial<Patient>) => {
      setIsSearching(false);
      if (values?.provinceId) {
        handleChangeProvince(values?.provinceId);
      } else if (values?.provinceCode) {
        var id = '';
        provinces?.map(item => {
          if (item.code == values?.provinceCode) {
            id = item.id;
          }
        });
        if (id && id != '') {
          if (methodType === 'HOME' && loadStreets) {
            loadStreets({
              variables: {
                page: 1,
                filtered: [
                  {
                    id: 'status',
                    value: '1',
                    operation: '=='
                  },
                  {
                    id: 'provinceId',
                    value: id,
                    operation: '=='
                  }
                ]
              }
            });
            formInstance.setFieldsValue({
              facilityId: '',
              examFacilityId: ''
            });
          }
          values.provinceId = id;
          setDistricCode(values.districtCode);
          loadDistricts({
            variables: {
              provinceId: id
            }
          });
        }
      }
      formInstance.setFieldsValue({
        patient: {
          ...values,
          //phone: values?.phone ? 0 + values.phone : undefined
          phone: values?.phone ? (values?.phone?.slice(0, 1) === '0' ? values?.phone : '0' + values?.phone) : undefined
        }
      });
      handleChangePatient(values);
    };

    const phoneNumber = (
      <Row className="field-css">
        {!isNewDialog && (
          <>
            <Col {...labelColOneItem} className="fs-12">
              <span className="red">* </span>Số điện thoại:
            </Col>
            <Col {...wrapperColOneItem}>
              <Form.Item>
                {/* <Space direction="horizontal"> */}
                <Row>
                  <Col {...wrapperSDTInput}>
                    <Form.Item
                      id="test"
                      name={['patient', 'phone']}
                      noStyle
                      required
                      rules={[
                        {
                          required: true,
                          pattern: new RegExp(/^\S+$/),
                          message: 'Số điện thoại không được để trống.'
                        },
                        ({ getFieldValue }) => ({
                          validator(_, value) {
                            if (!value) {
                              return Promise.reject(new Error(' '));
                            } else {
                              value = value.toLowerCase();
                              value = value.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
                              value = value.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
                              value = value.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
                              value = value.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
                              value = value.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
                              value = value.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
                              value = value.replace(/đ/g, 'd');
                              let result = value?.replace(
                                /^\s+|\s+|[a-zA-Z ]+|[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]+$/gm,
                                ''
                              );
                              if (!/^\d+$/.test(value)) {
                                //return Promise.reject(new Error('SĐT không được chứa ký tự chữ hoặc ký tự đặc biệt.'));
                                formInstance.setFieldsValue({
                                  patient: {
                                    phone: result
                                  }
                                });
                                formInstance.validateFields([['patient', 'phone']]);
                              } else if (!/(0[1|3|2|4|6|5|7|8|9])+([0-9]{8,10})\b/g.test(value)) {
                                return Promise.reject(
                                  new Error('SĐT phải đúng định dạng theo nhà mạng và bắt đầu là 0.')
                                );
                              } else {
                                formInstance.setFieldsValue({
                                  patient: {
                                    phone: result
                                  }
                                });
                                return Promise.resolve();
                              }
                            }
                          }
                        })
                      ]}
                    >
                      <Input
                        ref={disRefC}
                        onKeyUp={e => {
                          if (e.key == 'Enter') {
                            setIsSearching(true);
                          }
                        }}
                        placeholder="Nhập số điện thoại"
                      />
                    </Form.Item>
                  </Col>

                  <Col {...wrapperSDT} className="ant-space ml5-sdt">
                    <Button
                      type="primary"
                      onClick={() => {
                        setIsSearching(true);
                      }}
                      icon={<SearchOutlined />}
                    >
                      Tìm kiếm
                    </Button>
                    <Button
                      className="ml5-call"
                      type="primary"
                      onClick={() => {
                        setIsCalling(true);
                      }}
                      icon={<PhoneOutlined />}
                    >
                      Gọi
                    </Button>
                  </Col>
                </Row>

                {/* </Space> */}
              </Form.Item>
            </Col>
          </>
        )}
        {isNewDialog && (
          <>
            <Col {...labelColTowItem} className="fs-12">
              <span className="red">* </span>Số điện thoại:
            </Col>
            <Col {...wrapperColTowItem} className="over-hidden">
              <Form.Item
                name={['patient', 'phone']}
                style={{ width: '90%' }}
                required
                rules={[
                  {
                    required: true,
                    pattern: new RegExp(/^\S+$/),
                    message: 'Số điện thoại không được để trống.'
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value) {
                        return Promise.reject(new Error(' '));
                      } else {
                        value = value.toLowerCase();
                        value = value.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
                        value = value.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
                        value = value.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
                        value = value.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
                        value = value.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
                        value = value.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
                        value = value.replace(/đ/g, 'd');
                        let result = value?.replace(
                          /^\s+|\s+|[a-zA-Z ]+|[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]+$/gm,
                          ''
                        );
                        if (!/^\d+$/.test(value)) {
                          //return Promise.reject(new Error('SĐT không được chứa ký tự chữ hoặc ký tự đặc biệt.'));
                          formInstance.setFieldsValue({
                            patient: {
                              phone: result
                            }
                          });
                          formInstance.validateFields([['patient', 'phone']]);
                        } else if (!/(0[1|3|2|4|6|5|7|8|9])+([0-9]{8,10})\b/g.test(value)) {
                          return Promise.reject(new Error('SĐT phải đúng định dạng theo nhà mạng và bắt đầu là 0.'));
                        } else {
                          formInstance.setFieldsValue({
                            patient: {
                              phone: result
                            }
                          });
                          return Promise.resolve();
                        }
                      }
                    }
                  })
                ]}
              >
                <Input
                  maxLength={12}
                  ref={disRefC}
                  readOnly={disabledField}
                  onKeyUp={e => {
                    if (e.key == 'Enter') {
                      setIsSearching(true);
                    }
                  }}
                  placeholder="Nhập số điện thoại"
                />
              </Form.Item>
            </Col>
          </>
        )}
        <Modal
          title="Đang thực hiện cuộc gọi ..."
          visible={isCalling}
          onOk={() => {}}
          onCancel={() => {
            setIsCalling(false);
          }}
        >
          <Image width={200} src={require('../../../assets/icons/calling.gif')} />
        </Modal>
        <Modal
          maskClosable={false}
          centered
          style={{ marginTop: 5, height: 'calc(100vh - 200px)' }}
          bodyStyle={{ display: 'flex', overflowY: 'auto' }}
          title="Danh sách thông tin khách hàng"
          visible={isSearching}
          onCancel={() => {
            setIsSearching(false);
          }}
          footer={null}
          width={1000}
          destroyOnClose={true}
        >
          <AppointmentPhoneSearch
            phoneNumber={formInstance.getFieldValue(['patient', 'phone'])}
            onApply={onApply}
            onApplyPID={onApplyPID}
            setDisabledField={setDisabledField}
          />
        </Modal>
      </Row>
    );

    return responsive ? (
      !isNewDialog ? (
        <Col {...wrapperColAllCol}>{phoneNumber}</Col>
      ) : (
        <Col {...wrapperCol}>{phoneNumber}</Col>
      )
    ) : (
      phoneNumber
    );
  };
  //Loai khach hang
  const CustomerType: FC = () => {
    const customerType = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Loại khách hàng:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'type']}
            rules={[{ required: true, message: 'Loại khách hàng không được để trống.' }]}
          >
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Loại khách hàng --"
            >
              <Option value="PERSONAL">Cá nhân</Option>
              <Option value="CLINIC">Phòng khám/BS</Option>
              <Option value="VIP">VIP</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{customerType}</Col> : customerType;
  };

  //Ho ten KH
  interface CustomerFullnameProps {
    disabledField?: boolean;
  }
  const CustomerFullname: FC<CustomerFullnameProps> = ({ disabledField }) => {
    const customerFullname = (
      <Row className="field-css">
        {!isNewDialog && (
          <>
            <Col {...labelColTowItem} className="fs-12">
              <span className="red">* </span> Họ tên KH:
            </Col>
            <Col {...wrapperColTowItem} className="over-hidden">
              <Form.Item
                name={['patient', 'name']}
                rules={[{ required: true, message: 'Họ tên KH không được để trống.' }]}
                normalize={value => value?.toUpperCase()}
              >
                <Input placeholder="Nhập họ tên khách hàng" />
              </Form.Item>
            </Col>
          </>
        )}
        {isNewDialog && (
          <>
            <Col {...labelColOneItem} className="fs-12">
              <span className="red">* </span> Họ tên KH:
            </Col>
            <Col {...wrapperColOneItem} className="over-hidden">
              <Form.Item
                name={['patient', 'name']}
                rules={[{ required: true, message: 'Họ tên KH không được để trống.' }]}
                normalize={value => value?.toUpperCase()}
              >
                <Input readOnly={disabledField} placeholder="Nhập họ tên khách hàng" />
              </Form.Item>
            </Col>
          </>
        )}
      </Row>
    );

    return responsive ? (
      isNewDialog ? (
        <Col {...wrapperColAllCol}>{customerFullname}</Col>
      ) : (
        <Col {...wrapperCol}>{customerFullname}</Col>
      )
    ) : (
      customerFullname
    );
  };

  //Ly do dat lich
  interface ReasonBookingProps {
    reasonList: Reason[];
    isLoading: boolean;
  }
  const ReasonBooking: FC<ReasonBookingProps> = ({ reasonList, isLoading }) => {
    const reasonBooking = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Loại đặt lịch:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="reasonId" rules={[{ required: true, message: 'Loại đặt lịch không được để trống.' }]}>
            <Select
              onKeyDown={e => {
                if (e.which == 9) {
                  e.preventDefault();
                  console.log('test');
                  disRefC.current.focus();
                }
              }}
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Loại đặt lịch --"
            >
              {reasonList?.map(reason => (
                <Select.Option key={reason.id} value={reason.id}>
                  {reason.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{reasonBooking}</Col> : reasonBooking;
  };
  //Ngay sinh
  const handleCheckInput = (e: React.KeyboardEvent<HTMLInputElement>) => {
    setTimeout(() => {
      let value = (e.target as HTMLInputElement)?.value;
      const formattedDate = formatDateTyping(e.key, value);
      if (e.key === 'Backspace' || (isNaN(Number(e.key)) && e.key !== '/')) {
      } else {
        e.target['value'] = formattedDate;
      }
      const birthDate = moment(formattedDate, 'DD/MM/YYYY');
      if (formattedDate.length === 10) {
        formInstance.setFieldsValue({
          patient: {
            birthDate,
            birthYear: moment(birthDate.year(), 'YYYY')
          }
        });
      }
    });
  };

  interface DateofBirthProps {
    disabledField?: boolean;
  }
  const DateofBirth: FC<DateofBirthProps> = ({ disabledField }) => {
    const dateFormat = 'DD/MM/YYYY';
    const startDate = moment('01/01/1900', dateFormat);
    const startYear = moment('1900', 'YYYY');
    const dofBirth = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Ngày sinh :
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          {!isNewDialog && (
            <Form.Item
              name={['patient', 'birthDate']}
              rules={[
                { required: true, message: 'Ngày sinh không được để trống.' },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue(['patient', 'birthDate']) > startDate) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('Năm sinh phải lớn hơn 1900.'));
                  }
                })
              ]}
            >
              <DatePicker
                onKeyDown={handleCheckInput}
                onChange={(date, dateString) => {
                  if (date === null) {
                    formInstance.setFieldsValue({
                      patient: {
                        birthYear: undefined
                      }
                    });
                  } else {
                    formInstance.setFieldsValue({
                      patient: {
                        birthYear: moment(date, 'YYYY')
                      }
                    });
                  }
                }}
                disabledDate={current => {
                  return current && current > moment().add(0, 'day');
                }}
                placeholder="Chọn ngày sinh"
                format={dateFormat}
              />
            </Form.Item>
          )}
          {isNewDialog && (
            <Row>
              <Col span={14}>
                <Form.Item
                  tooltip={'Ví dụ 04/07/1997'}
                  name={['patient', 'birthDate']}
                  rules={[
                    { required: true, message: 'Ngày sinh không được để trống.' },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue(['patient', 'birthDate']) > startDate) {
                          return Promise.resolve();
                        }
                        return Promise.reject(new Error('Năm sinh phải lớn hơn 1900.'));
                      }
                    })
                  ]}
                >
                  <DatePicker
                    disabled={disabledField}
                    onChange={(date, dateString) => {
                      if (date === null) {
                        formInstance.setFieldsValue({
                          patient: {
                            birthYear: undefined
                          }
                        });
                      } else {
                        formInstance.setFieldsValue({
                          patient: {
                            birthYear: moment(date, 'YYYY')
                          }
                        });
                      }
                    }}
                    disabledDate={current => {
                      return current && current > moment().add(0, 'day');
                    }}
                    placeholder="Chọn ngày sinh"
                    format={dateFormat}
                    onKeyDown={handleCheckInput}
                  />
                </Form.Item>
              </Col>
              <Col span={10}>
                <Form.Item
                  name={['patient', 'birthYear']}
                  rules={[
                    { required: true, message: 'Năm sinh không được để trống.' },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue(['patient', 'birthYear']) > startYear) {
                          return Promise.resolve();
                        }
                        return Promise.reject(new Error('Năm sinh phải lớn hơn 1900.'));
                      }
                    })
                  ]}
                >
                  <DatePicker
                    disabled={disabledField}
                    onChange={(date, dateString) => {
                      if (date) {
                        formInstance.setFieldsValue({
                          patient: {
                            birthDate: moment('01/01/' + dateString, 'DD/MM/YYYY')
                          }
                        });
                      }
                    }}
                    disabledDate={current => {
                      return current && current > moment().add(0, 'day');
                    }}
                    format="YYYY"
                    // placeholder="Chọn năm sinh"
                    picker="year"
                  />
                </Form.Item>
              </Col>
            </Row>
          )}
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{dofBirth}</Col> : dofBirth;
  };

  //Ngay khoi phat
  const DateOnset: FC = () => {
    const dateFormat = 'DD/MM/YYYY';

    const dateOnset = (
      <Row className="">
        <Col {...labelColTowItem} className="fs-12">
          Ngày khởi phát triệu chứng:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={['healthDeclaration', 'dateOnset']}>
            <DatePicker
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              placeholder="Chọn ngày khởi phát"
              format={dateFormat}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{dateOnset}</Col> : dateOnset;
  };

  //Năm sinh
  const BirthYear: FC = () => {
    const dateFormat = 'DD/MM/YYYY';
    const startYear = moment('1900', 'YYYY');
    const birthYear = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Năm sinh:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'birthYear']}
            rules={[
              { required: false, message: 'Năm sinh không được để trống.' },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue(['patient', 'birthYear']) > startYear) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('Năm sinh phải lớn hơn 1900.'));
                }
              })
            ]}
          >
            <DatePicker
              onChange={(date, dateString) => {
                if (date) {
                  formInstance.setFieldsValue({
                    patient: {
                      birthDate: moment('01/01/' + dateString, dateFormat)
                    }
                  });
                }
              }}
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              format="YYYY"
              placeholder="Chọn năm sinh"
              picker="year"
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{birthYear}</Col> : birthYear;
  };

  //prognostic
  interface PrognosticProps {}
  const Prognostic: FC<PrognosticProps> = () => {
    const prognostic = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> <b>Mục A. Trong 03 ngày gần đây anh/chị có các biểu hiện sau đây</b>
        </div>
        <Form.Item
          name={['healthDeclaration', 'prognostic']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <CheckboxGroup
            name="healthDeclaration.prognostic"
            onChange={value => {
              if (value && value[value.length - 1] == '1') {
                formInstance.setFieldsValue({ healthDeclaration: { prognostic: ['1'] } });
              } else if (value && value[value.length - 1] != '1') {
                if (value.indexOf('1') != -1) value.splice(value.indexOf('1'), 1);
                formInstance.setFieldsValue({ healthDeclaration: { prognostic: value } });
              } else {
                formInstance.setFieldsValue({ healthDeclaration: { prognostic: value } });
              }
            }}
          >
            <div className="checkbox-kbyt">
              <Checkbox value="16">Ho - đau họng</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="2">Sốt</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="4">Khó thở - tức ngực</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="8">Các biểu hiện: Đau mỏi người. gai rét. đau họng</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="32">Giảm hoặc mất vị giác hoặc khứu giác</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="1">Không có triệu chứng</Checkbox>
            </div>
          </CheckboxGroup>
        </Form.Item>
      </div>
    );
    return prognostic;
  };

  //StreetHasCovid
  interface StreetHasCovidProps {}
  const StreetHasCovid: FC<StreetHasCovidProps> = ({}) => {
    const streetHasCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span>
          <span style={{ fontWeight: 500 }}>
            2. Di chuyển trên cùng phương tiện giao thông hoặc cùng địa điểm, sự kiện, nơi làm việc, lớp học… với ca
            bệnh xác định (F0) trong thời gian 2 ngày trước kể từ ngày F0 được lấy mẫu xác định hoặc tính từ ngày F0
            khởi phát triệu chứng.
          </span>
        </div>
        <Form.Item
          name={['healthDeclaration', 'streetHasCovid']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group name="healthDeclaration.streetHasCovid">
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return streetHasCovid;
  };

  interface InvoiceProps {
    setIsInvoice;
  }

  const Invoice: FC<InvoiceProps> = ({ setIsInvoice }) => {
    const invoice = (
      <div className="flex">
        <div className="text-invoice">Bạn có muốn xuất hóa đơn ?</div>
        <FormItem name={['healthDeclaration', 'invoice']} className="">
          <CheckboxGroup
            name="healthDeclaration.invoice"
            onChange={value => {
              if (value && value.length > 0) {
                setIsInvoice(true);
              } else {
                setIsInvoice(false);
              }
            }}
          >
            <Checkbox value="1" indeterminate={false}>
              <span>Có</span>
            </Checkbox>
          </CheckboxGroup>
        </FormItem>
      </div>
    );
    return invoice;
  };

  const PrintName: FC = ({}) => {
    const printName = (
      <FormItem name={['healthDeclaration', 'printName']} className="">
        <CheckboxGroup name="healthDeclaration.printName">
          <Checkbox indeterminate={false} value="1">
            <span className="red">Ẩn tên khách hàng</span>
          </Checkbox>
        </CheckboxGroup>
      </FormItem>
    );
    return printName;
  };

  //
  interface AddressProfileProps {
    required: boolean;
    disabledField?: boolean;
  }
  const AddressProfile: FC<AddressProfileProps> = ({ required, disabledField }) => {
    const addressProfile = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Địa chỉ:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'addressProfile']}
            rules={[{ required: required, message: 'Địa chỉ không được để trống.' }]}
          >
            <Input disabled={disabledField} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{addressProfile}</Col> : addressProfile;
  };

  //
  interface AddressCompanyProps {
    required: boolean;
  }
  const AddressCompany: FC<AddressCompanyProps> = ({ required }) => {
    const addressCompany = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Địa chỉ công ty:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'addressCompany']}
            rules={[{ required: required, message: 'Địa chỉ công ty không được để trống.' }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{addressCompany}</Col> : addressCompany;
  };

  //
  interface EmailProfileProps {
    required: boolean;
  }
  const EmailProfile: FC<EmailProfileProps> = ({ required }) => {
    const emailProfile = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Email:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'emailProfile']}
            rules={[
              {
                required: required,
                message: 'Email không được để trống và phải đúng định dạng',
                pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
              }
            ]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{emailProfile}</Col> : emailProfile;
  };

  //
  interface EmailCompanyProps {
    required: boolean;
  }
  const EmailCompany: FC<EmailCompanyProps> = ({ required }) => {
    const emailCompany = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Email:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'emailCompany']}
            rules={[
              {
                required: required,
                message: 'Email không được để trống.',
                pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
              }
            ]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{emailCompany}</Col> : emailCompany;
  };

  //
  interface TaxCodeProps {
    required: boolean;
  }
  const TaxCode: FC<TaxCodeProps> = ({ required }) => {
    const taxCode = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Mã số thuế:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'taxCode']}
            rules={[{ required: required, message: 'Mã số thuế không được để trống.' }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{taxCode}</Col> : taxCode;
  };

  //
  interface NameCompanyProps {
    required: boolean;
  }
  const NameCompany: FC<NameCompanyProps> = ({ required }) => {
    const nameCompany = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Tên công ty:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'nameCompany']}
            rules={[{ required: required, message: 'Tên công ty không được để trống.' }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{nameCompany}</Col> : nameCompany;
  };

  //NationPassOther
  interface NationPassOtherProps {}
  const NationPassOther: FC<NationPassOtherProps> = ({}) => {
    const nationPassOther = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span>
          <span style={{ fontWeight: 500 }}></span>
        </div>
        <Form.Item
          name={['healthDeclaration', 'nationPassOther']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group name="healthDeclaration.nationPassOther">
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return nationPassOther;
  };

  //HumanHasCovid
  interface HumanHasCovidProps {}
  const HumanHasCovid: FC<HumanHasCovidProps> = ({}) => {
    const humanHasCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span>
          <span style={{ fontWeight: 500 }}>
            1. Có tiền sử tiếp xúc gần với người nhiễm COVID- 19 trong thời gian 2 ngày trước kể từ ngày F0 được lấy mẫu
            xác định hoặc tính từ ngày F0 khởi phát triệu chứng? (đối tượng F1 tiếp xúc gần F0)
          </span>
        </div>
        <Form.Item
          name={['healthDeclaration', 'humanHasCovid']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group name="healthDeclaration.humanHasCovid">
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return humanHasCovid;
  };

  //TestedCovid
  interface TestedCovidProps {}
  const TestedCovid: FC<TestedCovidProps> = ({}) => {
    const testedCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span>
          <span style={{ fontWeight: 500 }}>
            3. Đã làm xét nghiệm SARS CoV2 trước đó có kết quả xét nghiệm test nhanh Ag dương tính/ hoặc PCR dương
            tính/nghi ngờ dương tính?
          </span>
        </div>
        <Form.Item
          name={['healthDeclaration', 'testedCovid']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group name="healthDeclaration.testedCovid">
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return testedCovid;
  };
  //Đã tiêm đủ 2 mũi vắc xin
  interface TowInjectionsProps {
    field: any;
    isDisabled?: boolean;
  }
  const TowInjections: FC<TowInjectionsProps> = ({ field, isDisabled }) => {
    const towInjections = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 2. Đã tiêm vắc xin phòng ngừa COVID-19 chưa?
        </div>
        <Form.Item
          name={['healthDeclaration', 'hasVaccine']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group disabled={isDisabled} name={'healthDeclaration.hasVaccine'}>
            <Row>
              <Col span={6}>
                <Radio value="0">Chưa tiêm</Radio>
              </Col>
              <Col span={6}>
                <Radio value="1">1 mũi</Radio>
              </Col>
              <Col span={6}>
                <Radio value="2">2 mũi</Radio>
              </Col>
              <Col span={6}>
                <Radio value="3">&gt; 2 mũi</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return towInjections;
  };

  //Đã từng nhiễm covid
  interface IsCovidProps {
    field: any;
    isCovided: boolean;
    setIsCovided: any;
    isDisabled?: boolean;
  }
  const IsCovid: FC<IsCovidProps> = ({ field, isCovided, setIsCovided, isDisabled }) => {
    const isCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 1. Tiền sử nhiễm COVID – 19 trong vòng 6 tháng gần đây không?
        </div>
        <Form.Item
          {...field}
          name={['healthDeclaration', 'hasCovid']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group
            disabled={isDisabled}
            onChange={value => {
              var itemValue = value.target.value;
              if (itemValue == '1' || itemValue == '3') {
                setIsCovided(true);
              } else {
                setIsCovided(false);
              }
            }}
            name={'healthDeclaration.hasCovid'}
          >
            <Row>
              <Col span={8}>
                <Radio value="1">Đã từng</Radio>
              </Col>
              <Col span={8}>
                <Radio value="2">Chưa từng</Radio>
              </Col>
              <Col span={8}>
                <Radio value="3">Đang điều trị</Radio>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                {isCovided && (
                  <Form.Item
                    name={['healthDeclaration', 'dateHascovid']}
                    label="Thời gian nhiễm"
                    rules={[{ required: isCovided, message: 'Bắt buộc phải nhập' }]}
                  >
                    <DatePicker format="DD/MM/YYYY" placeholder="-- Chọn ngày --" />
                  </Form.Item>
                )}
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return isCovid;
  };
  //Quốc tịch
  interface NationalityProps {
    countries: Country[];
    isLoading: boolean;
  }
  const Nationality: FC<NationalityProps> = ({ countries, isLoading }) => {
    const nationality = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Quốc tịch:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'nationality']}
            rules={[{ required: true, message: 'Quốc tịch không được để trống.' }]}
          >
            <Select
              loading={isLoading}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Quốc tịch --"
              onChange={(value, item) => {
                if (String(value).toUpperCase() === 'VN') {
                  setPatternIdNo(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
                } else {
                  setPatternIdNo(/^[a-zA-Z0-9_.-]{6,13}$/ as RegExp);
                }
                formInstance.setFieldsValue({ patient: { countryName: item['children'] } });
              }}
            >
              {countries?.map(country => (
                <Select.Option key={country.id} value={country.countryCode}>
                  {country.countryName}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item style={{ display: 'none' }} name={['patient', 'countryName']}>
            <Input />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{nationality}</Col> : nationality;
  };

  //Bac si CTV
  interface CollaboratorDoctorProps {
    functionOb: any;
  }
  const CollaboratorDoctor: FC<CollaboratorDoctorProps> = ({ functionOb }) => {
    const [visibleModal, setVisibleModal] = useState(false);
    const indicatedDoctorName = formInstance.getFieldValue('indicatedDoctorName');
    const collaboratorDoctor = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Bác sĩ CTV:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="indicatedDoctorId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_CHON_BSCTV] ? false : true}
              style={{ width: '150px', textAlign: 'left' }}
              icon={<UserOutlined />}
              onClick={() => {
                if (countMedicalExamination !== 0) {
                  openNotificationRight('Không thể chọn Bác sĩ CTV khi đã chọn Gói khám.', 'warning');
                  return;
                }
                setVisibleModal(true);
              }}
            >
              Chọn Bác sĩ CTV
            </Button>
            <span title={indicatedDoctorName} style={{ color: 'red', fontWeight: 'bold', marginLeft: '5px' }}>
              {indicatedDoctorName}
            </span>
            <Modal
              bodyStyle={{ overflowY: 'scroll' }}
              title="Danh sách Bác sĩ cộng tác viên"
              style={{ borderRadius: 10, overflow: 'hidden', marginTop: 15 }}
              centered
              okText="Chọn"
              cancelText="Hủy"
              cancelButtonProps={{ style: { display: 'none' } }}
              maskClosable={false}
              visible={visibleModal}
              onOk={() => setVisibleModal(false)}
              onCancel={() => setVisibleModal(false)}
              className="modalAddSchedule"
              width={900}
            >
              <ListCollaboratorDoctorForm
                setCollaboratorDoctorSelectedArr={values => {
                  formInstance.setFieldsValue({
                    indicatedDoctorId: values?.[0]?.id,
                    indicatedDoctorName: values?.[0]?.name
                  });
                }}
              />
            </Modal>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{collaboratorDoctor}</Col> : collaboratorDoctor;
  };
  //Gioi tinh
  interface GenderProps {
    disabledField?: boolean;
  }
  const Gender: FC<GenderProps> = ({ disabledField }) => {
    const gender = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Giới tính:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={['patient', 'sex']} rules={[{ required: true, message: 'Giới tính không được để trống.' }]}>
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              disabled={disabledField}
              placeholder="-- Chọn Giới tính --"
            >
              <Option value="MALE">Nam</Option>
              <Option value="FEMALE">Nữ</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{gender}</Col> : gender;
  };
  //Goi kham
  interface MedicalExaminationPackageProps {
    provinceId: string | undefined;
    methodType: any;
    functionOb: any;
  }
  const MedicalExaminationPackage: FC<MedicalExaminationPackageProps> = ({ provinceId, functionOb, methodType }) => {
    const [visibleModal, setVisibleModal] = useState(false);
    const packageName = formInstance.getFieldValue('packages')?.name;
    countMedicalExamination = packageName != null ? 1 : 0;
    const salePriceField = methodType === 'HOME' ? 'homeSalePrice' : 'hosSalePrice';
    const medicalExaminationPackage = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Gói khám:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="packages"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_CHON_GOI_KHAM] ? false : true}
              style={{ width: '150px', textAlign: 'left' }}
              icon={<ContainerOutlined />}
              onClick={() => {
                if (formInstance.getFieldValue('indicatedDoctorId')) {
                  openNotificationRight('Không thể chọn Gói khám khi đã chọn Bác sĩ CTV.', 'warning');
                  return;
                }
                // if (countTest && countTest !== 0) {
                //   openNotificationRight('Không thể chọn Gói khám khi đã chọn Xét nghiệm.');
                //   return;
                // }
                if (!provinceId && methodType === 'HOME') {
                  openNotificationRight('Vui lòng chọn Tỉnh/TP để có danh sách gói khám.', 'warning');
                  return;
                } else if (!provinceId && formInstance.getFieldValue('examFacilityId') && methodType === 'HOSPITAL') {
                  openNotificationRight('Nơi khám không có thông tin Tỉnh/TP.');
                  return;
                } else if (!provinceId && !formInstance.getFieldValue('examFacilityId')) {
                  openNotificationRight('Vui lòng chọn Nơi khám để có danh sách gói khám.', 'warning');
                  return;
                }
                setVisibleModal(true);
              }}
            >
              Chọn Gói khám
            </Button>
            <span title={packageName} style={{ color: 'red', fontWeight: 'bold', marginLeft: '5px' }}>
              {packageName}
            </span>
            <Modal
              bodyStyle={{ overflowY: 'scroll' }}
              title="Danh sách gói khám"
              style={{ borderRadius: 10, overflow: 'hidden', marginTop: 15 }}
              centered
              okText="Chọn"
              cancelText="Hủy"
              cancelButtonProps={{ style: { display: 'none' } }}
              maskClosable={false}
              visible={visibleModal}
              onOk={() => setVisibleModal(false)}
              onCancel={() => setVisibleModal(false)}
              className="modalAddSchedule"
              width={1200}
            >
              <ListPackageForm
                appointmentStatus={1}
                methodType={methodType}
                provinceId={provinceId}
                setPackageSelectedArr={values => {
                  formInstance.setFieldsValue({
                    packages: values?.[0],
                    originalAmount: values?.[0]?.originPrice,
                    totalAmount: values?.[0]?.[salePriceField]
                  });
                }}
              />
            </Modal>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{medicalExaminationPackage}</Col> : medicalExaminationPackage;
  };
  //Dia chi lien he
  interface ContactAddressProps {
    disabledField?: boolean;
  }
  const ContactAddress: FC<ContactAddressProps> = ({ disabledField }) => {
    const contactAddress = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Địa chỉ liên hệ:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name={['patient', 'address']}
            rules={[{ required: true, max: 100, message: 'Địa chỉ liên hệ không hợp lệ hoặc quá dài.' }]}
          >
            <Input readOnly={disabledField} placeholder="Nhập Địa chỉ liên hệ" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{contactAddress}</Col> : contactAddress;
  };

  //Xet nghiem
  interface TestProps {
    setServicesDataSelected?: (servicesDataSelected: any) => void;
    facilityList: Facility[];
    functionOb: any;
  }
  const Test: FC<TestProps> = ({ setServicesDataSelected, facilityList, functionOb }) => {
    const [visibleModal, setVisibleModal] = useState(false);
    const [servicesDataSelected, setServicesSelected] = useState<ServiceView[]>([]);
    const methodType = formInstance.getFieldValue('methodType') ?? 'HOME';
    const salePriceField = methodType === 'HOME' ? 'homeSalePrice' : 'hosSalePrice';
    const facilityField = methodType === 'HOME' ? 'examFacilityId' : 'examFacilityId';
    const facilityId = formInstance.getFieldValue(facilityField);

    countTest = formInstance.getFieldValue('services')?.length;
    const test = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Xét nghiệm:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="services"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_CHON_XET_NGHIEM] ? false : true}
              style={{ width: '150px', textAlign: 'left' }}
              icon={<NodeIndexOutlined />}
              onClick={() => {
                let examFacilityId = formInstance.getFieldValue('examFacilityId');
                // if (countMedicalExamination !== 0) {
                //   openNotificationRight('Không thể chọn Xét nghiệm khi đã chọn Gói khám.');
                //   return;
                // }
                if (!examFacilityId) {
                  openNotificationRight('Vui lòng chọn đơn vị trước khi chọn xét nghiệm.', 'warning');
                  return;
                }
                setVisibleModal(true);
              }}
            >
              Chọn Xét nghiệm
            </Button>
            <span style={{ color: 'red', marginLeft: '5px', fontWeight: 'bold' }}>
              {' '}
              {countTest ? '(Đã chọn ' + countTest + ')' : ''}
            </span>
            <Modal
              bodyStyle={{ overflowY: 'scroll' }}
              title="Chỉ định xét nghiệm"
              style={{ borderRadius: 10, overflow: 'hidden', marginTop: 15 }}
              centered
              okText="Chọn"
              cancelText="Hủy"
              cancelButtonProps={{ style: { display: 'none' } }}
              maskClosable={false}
              visible={visibleModal}
              onOk={() => {
                setVisibleModal(false);
              }}
              onCancel={() => setVisibleModal(false)}
              className="modalAddSchedule"
              width={900}
            >
              <ListServicesForm
                facilities={facilityList}
                facilityId={facilityId}
                methodType={salePriceField}
                setServicesDataSelectedArr={(values: ServiceView[]) => {
                  setServicesSelected(values);
                  setServicesDataSelected?.(values);
                  const originalAmount = values.reduce((acc, val) => acc + Number(val.originPrice), 0);
                  const totalAmount = values.reduce((acc, val) => acc + Number(val[salePriceField]), 0);
                  formInstance.setFieldsValue({
                    services: values?.map(val => ({
                      id: val.serviceId,
                      originalPrice: Number(val.originPrice),
                      salePrice: Number(val[salePriceField])
                    })),
                    originalAmount,
                    totalAmount
                  });
                }}
              />
            </Modal>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{test}</Col> : test;
  };
  //Tinh/TP
  interface ProvincesProps {
    isLoading: boolean;
    provinceList: Province[];
    handleChangeProvince: (provinceId: string) => void;
    disabledField?: boolean;
  }
  const Provinces: FC<ProvincesProps> = ({ provinceList, handleChangeProvince, isLoading, disabledField }) => {
    const onChange = (value: string) => {
      formInstance.setFieldsValue({
        patient: {
          districtId: undefined,
          wardId: undefined
        },
        streetId: undefined,
        routeId: undefined
      });
      if (!isNewDialog) {
        formInstance.setFieldsValue({
          examFacilityId: undefined
        });
      }
      handleChangeProvince(value);
      setFirst(true);
    };

    const provinces = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Tỉnh/TP:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            name={['patient', 'provinceId']}
            rules={[
              {
                required: true,
                message: 'Tỉnh/TP không được để trống.'
              }
            ]}
          >
            <Select
              disabled={disabledField}
              loading={isLoading}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={onChange}
              placeholder="-- Chọn Tỉnh/TP --"
            >
              {provinceList?.map(province => (
                <Select.Option key={province.id} value={province.id}>
                  {province.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{provinces}</Col> : provinces;
  };

  //Quan/Huyen
  interface DistrictProps {
    districtList: District[];
    isLoading: boolean;
    handleChangeDistrict: (districtId: string) => void;
    disabledField?: boolean;
  }
  const Districts: FC<DistrictProps> = ({ districtList, handleChangeDistrict, isLoading, disabledField }) => {
    const onChange = (value: string) => {
      formInstance.setFieldsValue({
        patient: {
          wardId: undefined
        }
      });
      handleChangeDistrict(value);
    };
    useEffect(() => {
      if (first) {
        //disRefC.current.focus();
      }
    }, [districtList]);
    const districts = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Quận/Huyện:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            name={['patient', 'districtId']}
            rules={[
              {
                required: true,
                message: 'Quận/Huyện không được để trống.'
              }
            ]}
          >
            <Select
              // ref={ref={disRefC}}
              disabled={disabledField}
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={onChange}
              placeholder="-- Chọn Quận/Huyện --"
            >
              {districtList?.map(district => (
                <Select.Option key={district.id} value={district.id}>
                  {district.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{districts}</Col> : districts;
  };

  //Phuong/Xa
  interface WardProps {
    wardList: Ward[];
    isLoading: boolean;
    handleChangeWard: (wardId: string) => void;
    disabledField?: boolean;
  }
  const Wards: FC<WardProps> = ({ wardList, handleChangeWard, isLoading, disabledField }) => {
    const wards = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Phường/Xã:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'wardId']}
            rules={[
              {
                required: false,
                message: 'Phường/Xã không được để trống.'
              }
            ]}
          >
            <Select
              disabled={disabledField}
              ref={quRFC}
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={handleChangeWard}
              placeholder="-- Chọn Phường/Xã --"
            >
              {wardList?.map(ward => (
                <Select.Option key={ward.id} value={ward.id}>
                  {ward.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{wards}</Col> : wards;
  };

  //Duong pho
  interface StreetProps {
    streetList: Street[];
    handleChangeStreet: (streetId: string) => void;
  }
  const StreetCpn: FC<StreetProps> = ({ streetList, handleChangeStreet }) => {
    const street = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Đường phố:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="streetId" rules={[{ required: false, message: 'Đường phố không được bỏ trống.' }]}>
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={handleChangeStreet}
              placeholder="-- Chọn Đường phố --"
            >
              {streetList?.map(street => (
                <Select.Option key={street.id} value={street.id}>
                  {street.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{street}</Col> : street;
  };

  //Ghi chu XN
  const TestNotes: FC = () => {
    const testNotes = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12">
          Ghi chú:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="appointmentNote"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <TextArea rows={5} placeholder="Nhập Ghi chú" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{testNotes}</Col> : testNotes;
  };

  //Email
  interface EmailProps {
    disabledField?: boolean;
  }
  const Email: FC<EmailProps> = ({ disabledField }) => {
    const email = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Email:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'email']}
            rules={[
              {
                required: false,
                message: 'Email không đúng định dạng',
                pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
              }
            ]}
          >
            <Input readOnly={disabledField} placeholder="Nhập Email" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{email}</Col> : email;
  };

  //Tong tien
  const TotalPrice: FC = () => {
    const onChange = value => {
      // console.log('changed', value);
    };
    const totalPrice = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Tổng tiền:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="originalAmount">
            <InputNumber
              disabled={true}
              bordered={false}
              style={{ color: 'red', background: '#fbfbfb', width: '93%' }}
              defaultValue={0}
              formatter={value => `${value} VNĐ`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={(value: any) => value.replace(/\$\s?|(,*)/g, '')}
              onChange={onChange}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{totalPrice}</Col> : totalPrice;
  };

  interface IdentityCardProps {
    isValidate?: boolean;
    disabledField?: boolean;
  }

  //CMND/CCCD
  const IdentityCard: FC<IdentityCardProps> = ({ isValidate, disabledField }) => {
    const identityCard = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          CMND/CCCD:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            dependencies={['patient']}
            name={['patient', 'idNo']}
            /*rules={[
              ({ getFieldValue }) => ({
                validator: (_, value) => {

                  var patient = getFieldValue("patient");
                  if (validateidNo(value, patient?.nationality)) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('Không đúng định dạng'))
                }
              }),
            ]}
            */
            rules={[{ required, message: 'Không đúng định dạng', pattern: patternIdNo }]}
          >
            <Input
              readOnly={disabledField}
              onChange={e => {
                // setIdNo(e.target.value);
              }}
              placeholder="Nhập CMND/CCCD"
            />
          </Form.Item>
          {/* {checkIdNo && <span>Không đúng định dạng</span>} */}
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{identityCard}</Col> : identityCard;
  };

  //Tong tien giam
  const TotalPriceDiscount: FC = () => {
    const onChange = value => {
      console.log('changed', value);
    };
    const totalPriceDiscount = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Tổng tiền giảm:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="totalAmount">
            <InputNumber
              disabled={true}
              bordered={false}
              style={{ color: 'red', background: '#fbfbfb', width: '93%' }}
              defaultValue={0}
              formatter={value => `${value} VNĐ`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={(value: any) => value.replace(/\$\s?|(,*)/g, '')}
              onChange={onChange}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{totalPriceDiscount}</Col> : totalPriceDiscount;
  };

  //Ma gen goi kham
  const GeneticCode: FC = () => {
    const geneticCode = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Mã gen gói khám:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="geneticCode"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input disabled />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{geneticCode}</Col> : geneticCode;
  };

  //CB bao lich
  interface ScheduleReporterProps {
    siteStaffList: SiteStaff[];
    isLoading: boolean;
    handleChangeSiteStaff: (siteStaffId: string) => void;
  }
  const ScheduleReporter: FC<ScheduleReporterProps> = ({ siteStaffList, isLoading, handleChangeSiteStaff }) => {
    const scheduleReporter = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Cán bộ báo lịch:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="infoStaffId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Select
              loading={isLoading}
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Cán bộ báo lịch --"
            >
              {siteStaffList?.map(option => (
                <Select.Option key={option.id} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{scheduleReporter}</Col> : scheduleReporter;
  };
  //Mã thẻ KH PID
  interface PIDProps {
    patientList: Patient[];
    disabledField?: boolean;
  }
  const PID: FC<PIDProps> = ({ patientList, disabledField }) => {
    const pId = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Mã thẻ KH (PID):
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'pid']}
            rules={[
              {
                required: false,
                message: 'Mã thẻ KH (PID) không được để trống.'
              }
            ]}
          >
            <Input readOnly={true} placeholder="" />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{pId}</Col> : pId;
  };

  //Trạng thái
  interface StatusProps {
    statusItem?: any;
  }
  const Status: FC<StatusProps> = ({ statusItem }) => {
    const status = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Trạng thái :
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          Đã xác nhận
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{status}</Col> : status;
  };

  //Trạng thái
  interface CodeAppoimentProps {
    code?: string;
  }
  const CodeAppoiment: FC<CodeAppoimentProps> = ({ code }) => {
    const codeAppoiment = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Mã lịch hẹn :
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          {code}
          <Input style={{ display: 'none' }} />
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{codeAppoiment}</Col> : codeAppoiment;
  };

  //Don vi
  interface FacilityParentProps {
    facilityParentList: Facility[];
    isLoading: boolean;
    handleChangeFacilityParent: (parentFacilityId: string, appointmentDate: string) => void;
  }

  const FacilityParent: FC<FacilityParentProps> = ({ facilityParentList, handleChangeFacilityParent, isLoading }) => {
    const onChangeFacilityParent = (value, event) => {
      const parentFacilityId = event.value;
      //setExamFacilityId(parentFacilityId);
      handleChangeFacilityParent(parentFacilityId, appointmentDate);
    };
    const facilityParent = (
      <Row className="field-css">
        {!isNewDialog && (
          <Col {...labelColOneItem} className="fs-12">
            <span className="red">* </span> Đơn vị thực hiện:{' '}
          </Col>
        )}
        {isNewDialog && (
          <Col {...labelColTowItem} className="fs-12">
            <span className="red">* </span> Đơn vị thực hiện:{' '}
          </Col>
        )}
        {!isNewDialog && (
          <Col {...wrapperColOneItem}>
            <Form.Item
              name="examFacilityId"
              rules={[
                {
                  required: true,
                  message: 'Đơn vị không được để trống.'
                }
              ]}
            >
              <Select
                showSearch
                loading={isLoading}
                optionFilterProp="children"
                filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                filterSort={(optionA, optionB) =>
                  optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                }
                onSelect={(value, event) => onChangeFacilityParent(value, event)}
                defaultValue=""
              >
                <Select.Option key="" value="">
                  -- Chọn Đơn vị --
                </Select.Option>
                {facilityParentList?.map(option => (
                  <Select.Option key={option.facilityCode} value={option.id}>
                    {option.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        )}
        {isNewDialog && (
          <Col {...wrapperColTowItem}>
            <Form.Item
              name="examFacilityId"
              rules={[
                {
                  required: true,
                  message: 'Đơn vị không được để trống.'
                }
              ]}
            >
              <Select
                disabled={true}
                defaultValue=""
                showSearch
                loading={isLoading}
                optionFilterProp="children"
                filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                filterSort={(optionA, optionB) =>
                  optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                }
                onSelect={(value, event) => onChangeFacilityParent(value, event)}
              >
                <Option key="" value="">
                  -- Không có đơn vị --
                </Option>
                {facilityParentList?.map(option => (
                  <Option key={option.facilityCode} value={option.id}>
                    {option.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        )}
      </Row>
    );

    return responsive ? (
      !isNewDialog ? (
        <Col {...wrapperColAllCol}>{facilityParent}</Col>
      ) : (
        <Col {...wrapperCol}>{facilityParent}</Col>
      )
    ) : (
      facilityParent
    );
  };

  //Van phong
  interface FacilityProps {
    facilityList: Facility[];
    handleChangeFacility: (id: any) => void;
    isLoading: boolean;
  }
  const Facility: FC<FacilityProps> = ({ facilityList, handleChangeFacility, isLoading }) => {
    const facility = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Văn phòng:{' '}
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="facilityId"
            rules={[
              {
                required: false,
                message: 'Văn phòng không được để trống.'
              }
            ]}
          >
            <Select
              disabled={true}
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onSelect={(value, event) => handleChangeFacility(value)}
              defaultValue=""
            >
              <Select.Option key="" value="">
                -- Không có văn phòng --
              </Select.Option>
              {facilityList?.map(option => (
                <Select.Option key={option.facilityCode} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? (
      !isNewDialog ? (
        <Col {...wrapperColAllCol}>{facility}</Col>
      ) : (
        <Col {...wrapperCol}>{facility}</Col>
      )
    ) : (
      facility
    );
  };

  //Nơi khám
  interface MedicalPlaceProps {
    facilityList: Facility[];
    methodType: string;
    isLoading: boolean;
    handleChangeMedicalPlace: (examFacilityId: string, appointmentDate: string) => void;
  }
  const MedicalPlace: FC<MedicalPlaceProps> = ({ facilityList, isLoading, methodType, handleChangeMedicalPlace }) => {
    const onChange = (value: string) => {
      //setExamFacilityId(value);
      handleChangeMedicalPlace(value, appointmentDate);
      formInstance.setFieldsValue({
        workTimeId: ''
      });
    };
    const medicalPlace = (
      <Row className="field-css">
        {!isNewDialog && (
          <Col {...labelColOneItem} className="fs-12">
            <span className="red">* </span> Nơi khám:
          </Col>
        )}
        {isNewDialog && (
          <Col {...labelColTowItem} className="fs-12">
            <span className="red">* </span> Nơi khám:
          </Col>
        )}
        {!isNewDialog && (
          <Col {...wrapperColOneItem}>
            <Form.Item
              name="examFacilityId"
              rules={[
                {
                  required: methodType === 'HOSPITAL',
                  message: 'Nơi khám không được để trống.'
                }
              ]}
            >
              <Select
                onChange={onChange}
                loading={isLoading}
                showSearch
                filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                placeholder="-- Chọn Nơi khám --"
              >
                {facilityList?.map(option => (
                  <Select.Option key={option.id} value={option.id}>
                    {option.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        )}
        {isNewDialog && (
          <Col {...wrapperColTowItem}>
            <Form.Item
              name="examFacilityId"
              rules={[
                {
                  required: methodType === 'HOSPITAL',
                  message: 'Nơi khám không được để trống.'
                }
              ]}
            >
              <Select
                onChange={onChange}
                loading={isLoading}
                showSearch
                filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                placeholder="-- Chọn Nơi khám --"
              >
                {facilityList?.map(option => (
                  <Select.Option key={option.id} value={option.id}>
                    {option.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        )}
      </Row>
    );

    return responsive ? (
      !isNewDialog ? (
        <Col {...wrapperColAllCol}>{medicalPlace}</Col>
      ) : (
        <Col {...wrapperCol}>{medicalPlace}</Col>
      )
    ) : (
      medicalPlace
    );
  };

  //Đối tượng gửi
  interface ObjectSendProps {
    facilityList: Facility[];
    isLoading: boolean;
  }
  const ObjectSend: FC<ObjectSendProps> = ({ facilityList, isLoading }) => {
    const objectSend = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Đối tượng gửi:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="sendFacilityId" rules={[{ required, message: 'Đối tượng gửi không được để trống.' }]}>
            <Select
              showSearch
              loading={isLoading}
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Đối tượng gửi --"
            >
              {facilityList?.map(option => (
                <Select.Option key={option.id} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{objectSend}</Col> : objectSend;
  };
  //Đối tượng
  interface AppointmentObjectProps {
    appointmentObjects: any;
    handleChange: (value) => void;
  }
  const AppointmentObject: FC<AppointmentObjectProps> = ({ appointmentObjects, handleChange }) => {
    const appointmentObject = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Hình thức khám :
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="appointmentObject" rules={[{ required: true, message: 'Hình thức không được để trống.' }]}>
            <Select
              showSearch
              onChange={e => {
                if (e === 'HEALTH_INSURANCE') {
                  handleChange(true);
                } else {
                  handleChange(false);
                }
              }}
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Đối tượng --"
            >
              <Select.Option value="SERVICE">Dịch vụ</Select.Option>
              <Select.Option value="HEALTH_INSURANCE">Bảo hiểm y tế</Select.Option>
              <Select.Option value="GUARANTEE_INSURANCE">Bảo hiểm bảo lãnh</Select.Option>
              <Select.Option value="APPRAISAL_INSURANCE">Thẩm định bảo hiểm</Select.Option>
              <Select.Option value="HEALTH_CERTIFICATE">Giấy chứng nhận sức khỏe</Select.Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentObject}</Col> : appointmentObject;
  };

  //Đối tượng
  interface TypeCustomerProps {
    // appointmentObjects: any;
    // handleChange: (value) => void;
  }
  const TypeCustomer: FC<TypeCustomerProps> = ({}) => {
    const typeCustomer = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Đối tượng :
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'typeCustomerKBYT']}
            rules={[{ required: true, message: 'Đối tượng khám không được để trống.' }]}
          >
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Đối tượng --"
            >
              <Select.Option value="">-- Chọn --</Select.Option>
              <Select.Option value="1">Người bệnh</Select.Option>
              <Select.Option value="2">Người nhà</Select.Option>
              <Select.Option value="3">Nhân viên y tế</Select.Option>
              <Select.Option value="4">Khách đến công tác</Select.Option>
              <Select.Option value="5">Tiêm chủng vắc xin</Select.Option>
              <Select.Option value="7">Khai hộ người khác</Select.Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{typeCustomer}</Col> : typeCustomer;
  };

  //Đặt lịch cho
  interface SetForProps {
    // appointmentObjects: any;
    // handleChange: (value) => void;
  }
  const SetForObject: FC<SetForProps> = ({}) => {
    const setForObject = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Đặt lịch cho :
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'relationType']}
            rules={[{ required: true, message: 'Đối tượng không được để trống.' }]}
          >
            <Select
              showSearch
              // onChange={e => {
              //   if (e === 'HEALTH_INSURANCE') {
              //     handleChange(true);
              //   } else {
              //     handleChange(false);
              //   }
              // }}
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn --"
            >
              <Select.Option value="PATIENT">Tôi</Select.Option>
              <Select.Option value="WIFE">Vợ</Select.Option>
              <Select.Option value="HUSBAND">Chồng</Select.Option>
              <Select.Option value="CHILD">Con</Select.Option>
              <Select.Option value="DAD">Bố đẻ</Select.Option>
              <Select.Option value="MOTHER">Mẹ đẻ</Select.Option>
              {/* <Select.Option value="7">Anh Ruột</Select.Option>
              <Select.Option value="8">Chị Ruột</Select.Option>
              <Select.Option value="9">Em Ruột</Select.Option>
              <Select.Option value="10">Bố chồng</Select.Option>
              <Select.Option value="11">Mẹ chồng</Select.Option>
              <Select.Option value="12">Bố vợ</Select.Option>
              <Select.Option value="13">Mẹ vợ</Select.Option>
              <Select.Option value="14">Người giám hộ</Select.Option>
              <Select.Option value="15">Người dùng chung SDT</Select.Option> */}
              {/* <Select.Option value="16">Người thân, họ hàng</Select.Option> */}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{setForObject}</Col> : setForObject;
  };

  //Chuyên khoa:
  interface SpecialistsProps {
    specialists: Specialist[];
    isLoading: boolean;
  }
  const Specialists: FC<SpecialistsProps> = ({ specialists, isLoading }) => {
    const specialist = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Chuyên khoa:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name="specialistId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Select
              loading={isLoading}
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Chuyên khoa--"
            >
              {specialists?.map(option => (
                <Select.Option key={option.code} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{specialist}</Col> : specialist;
  };

  //Thẻ BHYT
  interface BHYTProps {
    isRequired: boolean;
  }
  const BHYT: FC<BHYTProps> = ({ isRequired }) => {
    const bhyt = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Thẻ BHYT:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name="healthInsuranceCard"
            // rules={[{ required: isRequired, message: 'Nhập số thẻ BHYT' }]}
          >
            <Input placeholder="Nhập số thẻ BHYT" />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{bhyt}</Col> : bhyt;
  };
  interface ReasonBookingStrProps {}
  const ReasonBookingStr: FC<ReasonBookingStrProps> = () => {
    const reasonBookingStr = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span>Lý do đặt lịch:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="reasonNote" rules={[{ required: true, message: 'Lý do đặt lịch không được bỏ trống.' }]}>
            <TextArea rows={3} placeholder="Nhập lý do đặt lịch" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{reasonBookingStr}</Col> : reasonBookingStr;
  };

  //Lý do khám
  interface ReasonCheckupProps {
    isDisabled: boolean;
    medthodType: string;
  }

  const ReasonCheckup: FC<ReasonCheckupProps> = ({ isDisabled, medthodType }) => {
    const reasonCheckup = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Phân loại khám:
        </Col>
        <Col {...wrapperColOneItem} className="over-hidden">
          <Form.Item
            name={['healthDeclaration', 'purpose']}
            rules={[{ required: false, message: 'Phân loại khám không được để trống.' }]}
          >
            {/*{ medthodType  === 'HOSPITAL' ?
          <Select  placeholder="-- Chọn phân loại khám --"
          >
            <Option value="Khám bệnh">Khám bệnh</Option>
          </Select>
          :
          <Select placeholder="-- Chọn phân loại khám --"
            >
              <Option value="SARS-COV-2-Ag">Có dịch tễ nghi ngờ</Option>
              <Option value="SARS-COV-2-PCR">Có triệu chứng nghi ngờ</Option>
              <Option value="SARS-COV-2-PCR-FA">Có KQXN nghi ngờ nhiễm COVID-19</Option>
              <Option value="SARS-COV-2-PCR-XC">Nhập cảnh/ Xuất cảnh</Option>
              <Option value="Khám bệnh">Khám bệnh</Option>
              <Option value="Khác">Khác</Option>
            </Select>
          }*/}
            <Select disabled defaultValue="Khám bệnh" placeholder="-- Chọn phân loại khám --">
              <Option value="Khám bệnh">Khám bệnh</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{reasonCheckup}</Col> : reasonCheckup;
  };

  //Ngày đặt tại nhà
  interface AppointmentDateProps {
    onChange: (value: any, dateString: any) => void;
    isHospital: boolean;
    handleChangAppointmentDate: (examFacilityId: string, appointmentDate: string) => void;
    setAppointmentDateP?;
  }
  const AppointmentDate: FC<AppointmentDateProps> = ({
    onChange,
    isHospital,
    handleChangAppointmentDate,
    setAppointmentDateP
  }) => {
    const disablePastDt = current => {
      const yesterday = moment().subtract(1, 'day');
      return current.isBefore(yesterday);
    };
    const handleDatePickerChange = (date, dateString: string, id) => {
      formInstance.setFieldsValue({
        workTimeId: ''
      });
      let dateStringSplit = dateString?.split('/');
      dateString = dateStringSplit[2] + '-' + dateStringSplit[1] + '-' + dateStringSplit[0];
      setAppointmentDate(dateString);
      setAppointmentDateP(dateString);
      let examFacilityId = formInstance.getFieldValue('examFacilityId');
      // console.log("dateString" ,dateString)
      if (examFacilityId) {
        if (isHospital) {
          handleChangAppointmentDate(examFacilityId, dateString);
        } else {
          handleChangAppointmentDate(examFacilityId, dateString);
        }
      }
    };
    const appointmentDate = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Ngày đặt:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required
            name="appointmentDate"
            rules={[{ required: true, message: 'Ngày đặt không được để trống.' }]}
          >
            <DatePicker
              onChange={(date, dateString) => handleDatePickerChange(date, dateString, 1)}
              disabledDate={disablePastDt}
              format="DD/MM/YYYY"
              placeholder="-- Chọn ngày --"
            />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentDate}</Col> : appointmentDate;
  };
  //Khung giờ
  interface AppointmentWorkTimeProps {
    appointmentWorkTime: any;
    isLoading: boolean;
    setTxt?: any;
    txtKG?: any;
  }
  const AppointmentWorkTime: FC<AppointmentWorkTimeProps> = ({ appointmentWorkTime, isLoading, setTxt, txtKG }) => {
    const isWorkTimeValid = () => {
      // const date = moment(appointmentDate, 'DD/MM/YYYY HH:mm:ss');
      // if (!appointmentDate || date.endOf('day').unix() < moment().unix()) {
      //   return false;
      // }

      // if (txtKG) {
      //   const listTG = txtKG.split('-');
      //   if (listTG) {
      //     const [hour, minute] = listTG[0].split('h');
      //     date.set({
      //       hour,
      //       minute
      //     });
      //     if (date.unix() < moment().unix()) {
      //       return false;
      //     }
      //   }
      // }

      return true;
    };

    const isToday = (someDate: Date) => {
      const today = new Date();
      return (
        someDate.getDate() == today.getDate() &&
        someDate.getMonth() == today.getMonth() &&
        someDate.getFullYear() == today.getFullYear()
      );
    };

    let appointmentDate = formInstance.getFieldValue('appointmentDate');
    let appointmentWorkTimeArr: any = [];
    /*if (appointmentDate && isToday(new Date(String(appointmentDate)))) {
      for (let i = 0; i < appointmentWorkTime?.length; i++) {
        var today = new Date();
        var timeCurrent = today.getHours() + ':' + today.getMinutes() + ':00';
        const startTime = appointmentWorkTime[i]['startTime'];
        const endTime = appointmentWorkTime[i]['endTime'];
        if (Date.parse('01/01/2011 ' + timeCurrent) >= Date.parse('01/01/2011 ' + startTime + ':00')) {
        } else {
          appointmentWorkTimeArr.push(appointmentWorkTime[i]);
        }
      }
    } else if (appointmentWorkTime) {
      appointmentWorkTimeArr.push(...appointmentWorkTime);
    }*/
    for (let i = 0; i < appointmentWorkTime?.length; i++) {
      appointmentWorkTimeArr.push(appointmentWorkTime[i]);
    }
    const appointmentWorkTimeCpn = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Khung giờ:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name="workTimeId"
            rules={[
              { required: true, message: 'Khung giờ không hợp lệ.' },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (isWorkTimeValid()) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('Khung giờ này đã qua, chọn khung giờ khác'));
                }
              })
            ]}
          >
            <Select
              loading={isLoading}
              style={{ width: '100%' }}
              showSearch
              onChange={(value, name) => {
                setTxt(name['children']);
              }}
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn khung giờ --"
            >
              {appointmentWorkTimeArr === undefined
                ? []
                : appointmentWorkTimeArr?.map((option, index) => (
                    <Select.Option key={option['startTime'] + '-' + option['endTime']} value={option['id']}>
                      {option['name']}
                    </Select.Option>
                  ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentWorkTimeCpn}</Col> : appointmentWorkTimeCpn;
  };
  //Đường phố
  interface Street4HomeProps {
    streetList: Street[];
    isLoading: boolean;
    handleChangeStreet: (routeId: string, appointmentDate: string) => void;
  }
  const Street4Home: FC<Street4HomeProps> = ({ streetList, handleChangeStreet, isLoading }) => {
    const onChangeStreet = (value: string) => {
      const arrSplit = value.split('***');
      setRouteId(arrSplit[1]);
      handleChangeStreet(arrSplit[1], appointmentDate);
    };
    const street = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Đường phố:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="streetId" rules={[{ required: true, message: 'Đường phố không được bỏ trống' }]}>
            <Select
              loading={isLoading}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={onChangeStreet}
              placeholder="--Chọn Tỉnh/TP để có đường phố--"
            >
              {streetList?.map(street => (
                <Select.Option key={street.id} value={street.id + '***' + street.routeId}>
                  {street.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{street}</Col> : street;
  };
  //Cung đường
  interface RouteProps {
    routeNameDefault: boolean;
  }
  const Route: FC<RouteProps> = ({ routeNameDefault }) => {
    const route = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Cung đường:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="routeId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input readOnly={true} placeholder="Chọn Đường phố để có Cung đường" />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{route}</Col> : route;
  };

  //Tin nhắn gửi CBTN
  interface MgsContentProps {
    provinces?: Province[];
    districts?: District[];
    wards?: Ward[];
    workTimes?: WorkTime[];
  }
  const MgsContent: FC<MgsContentProps> = ({ provinces, districts, wards, workTimes }) => {
    const message = '';
    const mgsContent = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12">
          Tin nhắn gửi CBTN:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            // name="mgsContent"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <TextArea readOnly rows={5} placeholder="Nhập nội dung tin nhắn gửi cán bộ tại nhà" value={message} />
            {/* <Button style={{ marginTop: 10 }} type="primary" onClick={() => {}} icon={<ScheduleOutlined />}>
              Phân lịch
            </Button> */}
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{mgsContent}</Col> : mgsContent;
  };

  //Ghi chu lich hen
  interface AppointmentNotesProps {
    onSaveComment: (values) => void;
  }
  const AppointmentNotes: FC<AppointmentNotesProps> = ({ onSaveComment }) => {
    const [notes, setNotes]: any = useState('');
    const onChange = event => {
      setNotes(event.target.value);
    };
    //const listData = [{} as { id: any; href: any; title: any; content: any }];
    const [listData, setListData] = useState([{} as AppointmentComment]);
    listData.shift();
    const appointmentNotes = (
      <Row className="field-css">
        {!isNewDialog && (
          <>
            {' '}
            <Col {...labelColOneItem} className="fs-12">
              Ghi chú:
            </Col>
            <Col {...wrapperColOneItem}>
              <Form.Item
                name="appointmentNotes"
                rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
              >
                <TextArea rows={3} placeholder="Nhập ghi chú lich hẹn" allowClear onChange={onChange} />
                <Button
                  disabled={notes && notes.trim().length > 0 ? false : true}
                  style={{ marginTop: 10 }}
                  type="primary"
                  onClick={async () => {
                    onSaveComment({ content: notes });
                    formInstance.setFieldsValue({
                      appointmentNotes: ''
                    });
                  }}
                  icon={<SaveOutlined />}
                >
                  Lưu ghi chú
                </Button>
              </Form.Item>
            </Col>{' '}
          </>
        )}
        {isNewDialog && (
          <>
            <Col {...wrapperColOneItemNew}>
              <Form.Item
                name="appointmentNotes"
                rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
              >
                <TextArea rows={3} value={notes} placeholder="Nhập ghi chú lich hẹn" allowClear onChange={onChange} />
                {/* <Button
                style={{ marginTop: 10 }}
                type="primary"
                onClick={async () => {
                  onSaveComment({ content: notes });
                }}
                icon={<SaveOutlined />}
              >
                Lưu ghi chú
              </Button> */}
              </Form.Item>
            </Col>
            <Col {...labelColOneItem} className="fs-12">
              <Button
                disabled={notes && notes.trim().length > 0 ? false : true}
                style={{ marginTop: 10 }}
                type="primary"
                onClick={async () => {
                  onSaveComment({ content: notes });
                  formInstance.setFieldsValue({
                    appointmentNotes: ''
                  });
                  setNotes('');
                }}
                icon={<SaveOutlined />}
              >
                Lưu ghi chú
              </Button>
            </Col>
          </>
        )}
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{appointmentNotes}</Col> : appointmentNotes;
  };
  //Khung gio chi tiet lich hen
  interface AppointmentTimeFrameProps {
    appointmentWorkTimeData: WorkTime[];
  }
  const AppointmentTimeFrame: FC<AppointmentTimeFrameProps> = ({ appointmentWorkTimeData }) => {
    let workTimeArr: WorkTime[] = [];
    if (appointmentWorkTimeData && appointmentWorkTimeData.length !== 0) {
      for (let i = 0; i < appointmentWorkTimeData.length; i++) {
        let workTime: WorkTime = {} as WorkTime;
        workTime.name = appointmentWorkTimeData[i]['name'];
        workTime.id = appointmentWorkTimeData[i]['id'];
        if (appointmentWorkTimeData[i]['objectCountList'] !== null) {
          workTime.objectCountList = appointmentWorkTimeData[i]['objectCountList'];
          workTime.chuaThucHien = 0;
          workTime.daThucHien = 0;
          for (let j = 0; j < appointmentWorkTimeData[i]['objectCountList'].length; j++) {
            if (
              appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 1 ||
              appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 13
            ) {
              /*workTime.chuaThucHien = 0;
              workTime.daThucHien = 0;
              workTime.total = 0;*/
            } else {
              if (
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 2 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 4 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 5 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 6
              ) {
                workTime.chuaThucHien = appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  ? workTime.chuaThucHien + appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  : workTime.chuaThucHien;
              }
              if (
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 7 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 8 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 9 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 10 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 11
              ) {
                workTime.daThucHien = appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  ? workTime.daThucHien + appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  : workTime.daThucHien;
              }
            }
          }
          workTime.total = workTime.daThucHien + workTime.chuaThucHien;
          workTimeArr.push(workTime);
        } else {
          workTime.daThucHien = 0;
          workTime.chuaThucHien = 0;
          workTime.total = 0;
          workTimeArr.push(workTime);
        }
      }
    }
    const columns = [
      {
        title: 'Khung giờ',
        dataIndex: 'name',
        key: 'name',
        render: name => name,
        className: 'timeFame'
      },
      {
        title: 'Tổng lịch đã hẹn',
        dataIndex: 'total',
        key: 'total',
        className: 'timeFame'
      },
      {
        title: 'Lịch chưa thực hiện',
        dataIndex: 'chuaThucHien',
        key: 'chuaThucHien',
        className: 'timeFame lichChuaThucHien'
      },
      {
        title: 'Lịch đã thực hiện',
        dataIndex: 'daThucHien',
        key: 'daThucHien',
        className: 'timeFame'
      }
    ];
    const appointmentTimeFrame = (
      <Form.Item>
        <Table
          rowClassName={(record, index) => (index % 2 === 0 ? 'table-row-light' : 'table-row-dark')}
          scroll={{ y: 300 }}
          // style={{ height: 300 }}
          pagination={false}
          columns={columns}
          dataSource={workTimeArr}
        />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentTimeFrame}</Col> : appointmentTimeFrame;
  };
  //Bang ghi chu
  interface AppointmentNotesTableProps {
    pagingAppointmentComment?: any;
    appointmentComments?: any;
    comments?: any;
    onDelete: (index) => void;
  }
  const AppointmentNotesTable: FC<AppointmentNotesTableProps> = ({
    pagingAppointmentComment,
    appointmentComments,
    onDelete,
    comments
  }) => {
    const columns = [
      {
        title: 'STT',
        key: 'STT',
        render: (value, item, index) => index + 1
      },
      {
        title: 'Thời gian',
        dataIndex: 'time',
        key: 'time'
      },
      {
        title: 'Người ghi chú',
        dataIndex: 'note',
        key: 'note'
      },
      {
        title: 'Nội dung ghi chú',
        dataIndex: 'content',
        key: 'content'
      },
      {
        title: 'Xóa ghi chú',
        dataIndex: 'content',
        key: 'content',
        className: 'appointment-notes',
        render: (value, item, index) => (
          <Button
            type="primary"
            onClick={() => {
              Modal.confirm({
                icon: <ExclamationCircleOutlined />,
                title: 'Bạn có chắc chắn muốn xoá ghi chú đã chọn' + '' + ' ?',
                onOk() {
                  onDelete(index);
                },
                onCancel() {
                  //console.log('Cancel');
                }
              });
            }}
            danger
            icon={<DeleteOutlined />}
          >
            Xoá
          </Button>
        )
      }
    ];
    const appointmentCommentTable = (
      <Form.Item name="comments">
        <Table scroll={{ y: 400 }} pagination={false} columns={columns} dataSource={comments} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentCommentTable}</Col> : appointmentCommentTable;
  };
  //Bang lich su
  interface HistoryTableProps {
    pagingAppointmentComment?: any;
    appointmentComments?: any;
    appoiments?: any;
  }
  const HistoryTableTable: FC<HistoryTableProps> = ({ pagingAppointmentComment, appointmentComments, appoiments }) => {
    const columns = [
      {
        title: 'Mã lịch hẹn',
        dataIndex: 'code',
        key: 'code'
      },
      {
        title: 'Thời gian',
        dataIndex: 'time',
        key: 'time'
      },
      {
        title: 'Mã nhân viên',
        dataIndex: 'staffCode',
        key: 'staffCode'
      },
      {
        title: 'Hành động',
        dataIndex: 'active',
        key: 'active'
      }
    ];
    const historyTableTable = (
      <Form.Item name="appoiments">
        <Table scroll={{ y: 400 }} pagination={false} columns={columns} dataSource={appoiments} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{historyTableTable}</Col> : historyTableTable;
  };
  //Cán bộ được phân lịch
  interface AssignStaffProps {
    siteStaffs?: SiteStaff[];
    isLoadingSiteStaffsAssign?: boolean;
  }
  const AssignStaff: FC<AssignStaffProps> = ({ siteStaffs, isLoadingSiteStaffsAssign }) => {
    const assignStaff = (
      <Row className="field-css">
        {!isNewDialog && (
          <>
            <Col {...labelColOneItem} className="fs-12">
              CB được phân lịch:
            </Col>
            <Col {...wrapperColOneItem} className="over-hidden">
              <Form.Item
                name="assignStaffId"
                rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
              >
                <Select
                  showSearch
                  loading={isLoadingSiteStaffsAssign}
                  optionFilterProp="children"
                  filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  filterSort={(optionA, optionB) =>
                    optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                  }
                  defaultValue=""
                >
                  <Select.Option key="" value="">
                    -- Chọn cán bộ tại nhà --
                  </Select.Option>
                  {siteStaffs?.map(siteStaff => (
                    <Select.Option key={siteStaff.id} value={siteStaff.id}>
                      {siteStaff.name}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </>
        )}
        {isNewDialog && (
          <>
            <Col {...labelColTowItem} className="fs-12">
              CB được phân lịch:
            </Col>
            <Col {...wrapperColTowItem} className="over-hidden">
              <Form.Item
                name="assignStaffId"
                rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
              >
                <Select
                  showSearch
                  loading={isLoadingSiteStaffsAssign}
                  optionFilterProp="children"
                  filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  filterSort={(optionA, optionB) =>
                    optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                  }
                  defaultValue=""
                >
                  <Select.Option key="" value="">
                    -- Chọn cán bộ tại nhà --
                  </Select.Option>
                  {siteStaffs?.map(siteStaff => (
                    <Select.Option key={siteStaff.id} value={siteStaff.id}>
                      {siteStaff.name}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </>
        )}
      </Row>
    );
    return responsive ? (
      isNewDialog ? (
        <Col {...wrapperCol}>{assignStaff}</Col>
      ) : (
        <Col {...wrapperColAllCol}>{assignStaff}</Col>
      )
    ) : (
      assignStaff
    );
  };
  return React.useMemo(
    () => ({
      form: formInstance,
      Form: WrappedForm,
      AppointmentCode,
      AppointmentStatus,
      AppointmentSource,
      PhoneNumber,
      CustomerType,
      CustomerFullname,
      ReasonBooking,
      DateofBirth,
      BirthYear,
      CollaboratorDoctor,
      Gender,
      Nationality,
      MedicalExaminationPackage,
      ContactAddress,
      Test,
      Provinces,
      Districts,
      Wards,
      Street4Home,
      TestNotes,
      Email,
      ReasonCheckup,
      TotalPrice,
      IdentityCard,
      TotalPriceDiscount,
      GeneticCode,
      ScheduleReporter,
      PID,
      IsCovid,
      TowInjections,
      MedicalPlace,
      ObjectSend,
      AppointmentObject,
      Specialists,
      AppointmentDate,
      AppointmentWorkTime,
      AppointmentTimeFrame,
      AppointmentNotes,
      AppointmentNotesTable,
      Route,
      MgsContent,
      StreetCpn,
      FacilityParent,
      ReasonBookingStr,
      BHYT,
      AssignStaff,
      HistoryTableTable,
      Status,
      CodeAppoiment,
      SetForObject,
      TypeCustomer,
      DateOnset,
      Prognostic,
      StreetHasCovid,
      NationPassOther,
      TestedCovid,
      HumanHasCovid,
      Invoice,
      AddressProfile,
      EmailProfile,
      NameCompany,
      TaxCode,
      Facility,
      AddressCompany,
      EmailCompany,
      PrintName
    }),
    [patternIdNo]
  );
}
