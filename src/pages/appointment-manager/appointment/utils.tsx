import React from 'react';
import { Tag } from 'antd';

export const getStatus = (status?: number) =>
  status === 1 ? (
    <Tag color="#f1b91c">Chờ xác nhận</Tag>
  ) : status === 2 ? (
    <Tag color="#dddedf">Đã xác nhận</Tag>
  ) : status === 3 ? (
    <Tag color="#2db7f5">Chờ tư vấn</Tag>
  ) : status === 4 ? (
    <Tag>'Đã tư vấn đầu vào'</Tag>
  ) : status === 5 ? (
    <Tag>Chưa check</Tag>
  ) : status === 6 ? (
    <Tag>Đã check</Tag>
  ) : status === 7 ? (
    <Tag>Đã lấy mẫu</Tag>
  ) : status === 8 ? (
    <Tag>Đã có kết quả</Tag>
  ) : status === 9 ? (
    <Tag>Đã đăng ký khám</Tag>
  ) : status === 10 ? (
    <Tag>Đã thanh toán</Tag>
  ) : status === 11 ? (
    <Tag>Đã khám</Tag>
  ) : status === 13 ? (
    <Tag color="#636363">
      <span style={{ color: '#fff' }}>Đã huỷ</span>
    </Tag>
  ) : (
    <Tag></Tag>
  );

export const getStatusPackages = (status?: number) => (status === 1 ? <Tag color="#87d068">Đã duyệt</Tag> : '');
