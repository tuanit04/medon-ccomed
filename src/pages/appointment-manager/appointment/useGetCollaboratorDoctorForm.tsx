import React, { FC, useEffect, useState } from 'react';
import { Form, Input, Table, Button, Select, Row, Col } from 'antd';
import { Role } from 'interface/permission/role.interface';
import { useGetDoctorsLazy } from 'hooks/doctor/useGetDoctor';
import { Doctor } from 'common/interfaces/doctor.interface';
import { isEmptyObj } from 'utils/helper';

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};
const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 }
};

interface CollaboratorDoctorFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  /** Initial form data */
  values?: Role;
}
const getFiltered = ({ searchField, search }) => {
  const arr: object[] = [];
  if (search) {
    arr.push({
      id: searchField,
      value: search,
      operation: '~'
    });
    arr.push({
      id: 'ccomedStatus',
      value: '1',
      operation: '=='
    });
  }
  return arr;
};

//check obj empty
const isEmpty = obj => {
  return Object.entries(obj).length === 0;
};

export default function useGetCollaboratorDoctorForm({}: CollaboratorDoctorFormProps) {
  const ListDoctors: FC<{
    pagingDoctors: any;
    filter: any;
    setFilter: any;
    loadDoctors: any;
    isLoading: boolean;
    pageSize;
    setPage;
    setFiltered: any;
  }> = ({ filter, setFilter, pagingDoctors, loadDoctors, isLoading, pageSize, setPage, setFiltered }) => {
    const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
      setFilter({ ...filter, search: e.target.value });
    };

    const onSelect = (value: string) => {
      setFilter({ ...filter, searchField: value });
    };

    const onSearch = filter => {
      if (filter['searchField'] === 'phone') {
        let phone = filter['search'];
        phone = phone.slice(0, 1) === '0' ? phone.slice(1) : phone;
        filter = { ...filter, search: phone };
      }
      setPage(1);
      setFiltered(getFiltered(filter));
      if (getFiltered(filter).length !== 0) {
        const variables = {
          //page: pagingDoctors?.page ?? 1,
          page: 1,
          pageSize: pageSize ?? 50,
          filtered: getFiltered(filter)
        };
        loadDoctors({ variables });
      } else {
        const variables = {
          //page: pagingDoctors?.page ?? 1,
          page: 1,
          pageSize: pageSize ?? 50
        };
        loadDoctors({ variables });
      }
    };

    const select = (
      <Select value={filter.searchField} onChange={onSelect}>
        <Select.Option value="name">Tên Bác sĩ</Select.Option>
        <Select.Option value="doctorHisId">Mã CTV</Select.Option>
        <Select.Option value="phone">Số SĐT</Select.Option>
      </Select>
    );

    return (
      <Row style={{ marginTop: 10 }} align="middle">
        <Col className="gutter-row" span={5}>
          <span style={{ fontWeight: 400, color: 'red' }}>
            Tìm kiếm ({pagingDoctors['records'] ? pagingDoctors['records'] + ' bản ghi' : '0 bản ghi'})
          </span>
        </Col>
        <Col className="gutter-row" span={19}>
          <Input
            placeholder="Nhập thông tin tìm kiếm"
            style={{ width: '100%' }}
            //onChange={onChange}
            onPressEnter={(e: any) => {
              onSearch({ ...filter, search: e.target.value?.trim() });
            }}
            addonBefore={select}
          />
        </Col>
        {/*<Col className="gutter-row" span={6}>
          <Button type="primary" icon={<SearchOutlined />} loading={isLoading} onClick={onSearch}>
            Tìm kiếm
          </Button>
          </Col>*/}
      </Row>
    );
  };
  const columns = [
    {
      title: 'Tên Bác sĩ',
      dataIndex: 'name'
    },
    {
      title: 'Mã CTV',
      dataIndex: 'doctorHisId'
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
      render: phone => {
        if (!phone) {
          return '';
        }
        return phone?.substr(0, 1) !== '0' ? (phone = '0' + phone) : phone;
      }
    },
    {
      title: 'Email',
      dataIndex: 'email'
    }
  ];
  interface ListCollaboratorDoctorFormProps {
    doctor?: Doctor;
    setCollaboratorDoctorSelectedArr: (collaboratorDoctorSelectedArr: any) => void;
    appointmentStatus?: number | undefined;
  }
  const ListCollaboratorDoctorForm: FC<ListCollaboratorDoctorFormProps> = ({
    setCollaboratorDoctorSelectedArr,
    doctor,
    appointmentStatus
  }) => {
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(20);
    const [pagination, setPagination] = useState({});
    const [filtered, setFiltered]: any[] = useState([
      {
        id: 'ccomedStatus',
        value: '1',
        operation: '=='
      }
    ]);
    const [collaboratorDoctorDataSelectedState, setCollaboratorDoctorDataSelectedState]: any = useState([]);
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);
    const [loading, setLoading] = useState(false);
    const [filter, setFilter] = useState({
      searchField: 'name',
      search: ''
    });
    const [doctorsRes, setDoctorsRes] = useState([]);

    const variables = { page, pageSize, filtered };
    const { pagingDoctors, isLoadingDoctors, loadDoctors } = useGetDoctorsLazy();

    const handleTableChange = (pagination, filters, sorter) => {
      reFetchData({
        sortField: sorter.field,
        sortOrder: sorter.order,
        pagination,
        ...filters
      });
    };

    const reFetchData = (params = {}) => {
      let pagination = params['pagination'];
      if (pageSize !== pagination['pageSize']) {
        setPage(1);
      } else {
        setPage(pagination['current']);
      }
      setPageSize(pagination['pageSize']);
      loadDoctors({ variables: { page: pagination['current'], pageSize: pagination['pageSize'], filtered: filtered } });
    };

    //Check BS CTV đã chọn
    useEffect(() => {
      loadDoctors({ variables });
    }, []);

    useEffect(() => {
      setPagination({ current: page, pageSize: pageSize, total: pagingDoctors['records'] });
      if (!isEmpty(pagingDoctors) && doctor) {
        setDoctorsRes([]);
        let doctorsCheckSelected: any = [];
        for (let i = 0; i < pagingDoctors?.data?.length; i++) {
          let doctorObj = { ...pagingDoctors?.data[i] };
          let flag = false;
          if (doctorObj['id'] === doctor?.id) {
            doctorObj['chosen'] = true;
            flag = true;
          }
          if (!flag) {
            doctorObj['chosen'] = false;
          }
          doctorsCheckSelected.push(doctorObj);
        }
        setSelectedRowKeys(doctorsCheckSelected?.filter(item => item.chosen)?.map(item => item.id));
        setDoctorsRes(doctorsCheckSelected);
      }
    }, [pagingDoctors]);

    useEffect(() => {
      if (doctor) {
        setCollaboratorDoctorDataSelectedState([doctor]);
      }
    }, [doctor]);

    const start = () => {
      setLoading(true);
      // ajax request after empty completing
      setTimeout(() => {
        setSelectedRowKeys([]);
        setCollaboratorDoctorDataSelectedState([]);
        setCollaboratorDoctorSelectedArr([]);
        setLoading(false);
      }, 500);
    };

    const onSelectChange = selectedRowKeys => {
      const collaboratorDoctorDataSelectedArr: any[] = [];

      setSelectedRowKeys(selectedRowKeys);
      for (let i = 0; i < pagingDoctors?.data.length; i++) {
        for (let j = 0; j < selectedRowKeys.length; j++) {
          if (pagingDoctors?.data[i]['id'] === selectedRowKeys[j]) {
            collaboratorDoctorDataSelectedArr.push(pagingDoctors?.data[i]);
          }
        }
      }
      setCollaboratorDoctorDataSelectedState(collaboratorDoctorDataSelectedArr);
      setCollaboratorDoctorSelectedArr(collaboratorDoctorDataSelectedArr);
    };

    const hasSelected = selectedRowKeys.length > 0 || !isEmptyObj(doctor);

    const isSelectDisabled = appointmentStatus === 1 || appointmentStatus === undefined ? false : true;

    return (
      <>
        <Form {...formItemLayout}>
          <ListDoctors
            filter={filter}
            setFilter={setFilter}
            pagingDoctors={pagingDoctors}
            isLoading={isLoadingDoctors}
            loadDoctors={loadDoctors}
            setPage={setPage}
            pageSize={pageSize}
            setFiltered={setFiltered}
          />
        </Form>
        <div style={{ marginTop: 5 }}></div>
        <Table
          title={() => (
            <>
              <div style={{ marginBottom: 16, marginTop: 15 }}>
                {appointmentStatus === 1 || appointmentStatus === undefined ? (
                  <Button type="primary" onClick={start} disabled={!hasSelected} loading={loading}>
                    Chọn lại
                  </Button>
                ) : (
                  ''
                )}
                <span style={{ marginLeft: 5 }}>Bác sĩ đã chọn đã chọn :</span>
                <span style={{ marginLeft: 8, color: 'red' }}>
                  {collaboratorDoctorDataSelectedState?.length > 0 ? collaboratorDoctorDataSelectedState[0]?.name : '0'}
                </span>
              </div>
            </>
          )}
          loading={isLoadingDoctors}
          pagination={{
            //current: pagingDoctors?.page ?? 1,
            //pageSize: pagingDoctors?.pageSize ?? 20,
            ...pagination,
            defaultPageSize: 20,
            total: pagingDoctors?.records ?? 0,
            pageSizeOptions: ['10', '20', '30', '50']
          }}
          onChange={handleTableChange}
          rowKey={record => record.id}
          rowSelection={{
            getCheckboxProps: () => ({
              disabled: isSelectDisabled
            }),
            selectedRowKeys,
            onChange: onSelectChange,
            type: 'radio'
          }}
          onRow={(record, rowIndex) => ({
            onClick: () => {
              if (isSelectDisabled) return;
              onSelectChange([record.id]);
            }
          })}
          scroll={{ y: 300 }}
          columns={columns}
          dataSource={doctor ? doctorsRes : pagingDoctors?.data}
        />
      </>
    );
  };
  return {
    ListCollaboratorDoctorForm
  };
}
