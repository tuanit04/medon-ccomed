import React, { useEffect, useState } from 'react';
import { Space, Table, Typography, Button, Input } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { Patient } from 'common/interfaces/patient.interface';
import { Appointment } from 'common/interfaces/appointment.interface';
import { CopyOutlined, SearchOutlined } from '@ant-design/icons';
import { useGetPatients } from 'hooks/patient';
import { useGetProvinces } from 'hooks/province/useGetProvince';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { useGetAppointmentsLazy } from 'hooks/appointment';
import moment from 'moment';
import Modal from 'antd/lib/modal/Modal';

interface Props {
  phoneNumber?: string;
  onApply?: (value?: Partial<Patient>) => void;
}

const AppointmentPhoneSearch = ({ phoneNumber, onApply }: Props) => {
  const [phone, setPhone] = useState(phoneNumber);
  const [value, setValue] = useState<Partial<Patient>>();
  const [isConfirm, setIsConfirm] = useState(false);
  const { pagingPatients, loadPatients } = useGetPatients();
  const { pagingProvinces } = useGetProvinces();
  const { districts, loadDistricts } = useGetDistricts();
  const { loadAppointments, pagingAppointments } = useGetAppointmentsLazy({ page: 1, pageSize: 10 });

  useEffect(() => {
    onQuery(phoneNumber);
    setPhone(phoneNumber);
  }, [phoneNumber]);

  const onCopy = (patient?: Partial<Patient>) => {
    setValue(patient);
    setIsConfirm(true);
  };

  const patientColumn: ColumnsType<Patient> = [
    {
      title: 'Sử dụng',
      render: (value, record) => (
        <Button type="primary" onClick={() => onCopy(record)}>
          <CopyOutlined />
        </Button>
      )
    },
    {
      title: 'PID',
      dataIndex: 'pid'
    },
    {
      title: 'Họ tên',
      dataIndex: 'name'
    },
    {
      title: 'Ngày sinh',
      dataIndex: 'birthDate',
      render: value => moment(value, 'DD/MM/YYYY HH:mm:ss').format('DD/MM')
    },
    {
      title: 'Năm sinh',
      dataIndex: 'birthYear',
      render: value => moment(value, 'DD/MM/YYYY HH:mm:ss').format('YYYY')
    },
    {
      title: 'Giới tính',
      dataIndex: 'sex',
      render: value => (value === 'MALE' ? 'Nam' : 'Nữ')
    },
    {
      title: 'SĐT',
      dataIndex: 'phone'
    },
    {
      title: 'CMT/CCCD',
      dataIndex: 'idNo'
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'address'
    },
    {
      title: 'Thành phố',
      dataIndex: 'provinceCode',
      render: value => pagingProvinces?.data?.find(val => val.id === value)?.name
    },
    {
      title: 'Quận huyện',
      dataIndex: 'districtCode'
    }
  ];

  const appointmentColumn: ColumnsType<Appointment> = [
    {
      title: 'Sử dụng',
      render: (value, record) => (
        <Button
          type="primary"
          onClick={() =>
            onCopy({
              name: record.patientName,
              phone: record.patientPhone,
              address: record.patientAddress
            })
          }
        >
          <CopyOutlined />
        </Button>
      )
    },
    {
      title: 'Ngày khám',
      dataIndex: 'appointmentDate',
      render: value => moment(value, 'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY')
    },
    {
      title: 'Họ tên',
      dataIndex: 'patientName'
    },
    {
      title: 'Ngày sinh',
      dataIndex: ''
    },
    {
      title: 'Năm sinh',
      dataIndex: ''
    },
    {
      title: 'Giới tính',
      dataIndex: ''
    },
    {
      title: 'SĐT',
      dataIndex: ''
    },
    {
      title: 'CMT/CCCD',
      dataIndex: ''
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'patientAddress'
    },
    {
      title: 'Thành phố',
      dataIndex: ''
    },
    {
      title: 'Quận huyện',
      dataIndex: ''
    },
    {
      title: 'Email',
      dataIndex: ''
    },
    {
      title: 'Thẻ KH (PID)',
      dataIndex: ''
    }
  ];

  const onQuery = (phoneNumber?: string) => {
    loadPatients({
      variables: {
        page: 1,
        pageSize: 10,
        filtered: [{ id: 'phone', value: phoneNumber?.slice(1), operation: '==' }]
      }
    });
    loadAppointments({
      variables: {
        page: 1,
        pageSize: 10,
        filtered: [{ id: 'patientPhone', value: phoneNumber?.slice(1), operation: '==' }]
      }
    });
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPhone(e.target.value);
  };

  return (
    <Space direction="vertical">
      <Space>
        <Input
          value={phone}
          onChange={onChange}
          placeholder="Gõ SĐT tìm kiếm thông tin"
          type="number"
          style={{ width: 210 }}
        />
        <Button onClick={() => onQuery(phone)}>
          <SearchOutlined />
          Tìm kiếm
        </Button>
      </Space>
      <div>
        <Typography.Title className="fs-10" level={5}>Danh sách thẻ khách hàng (PID)</Typography.Title>
        <Table
          pagination={{
            current: pagingPatients?.page,
            pageSize: pagingPatients?.pageSize,
            total: pagingPatients?.records
          }}
          columns={patientColumn}
          dataSource={pagingPatients?.data}
        />
      </div>
      <div>
        <Typography.Title className="fs-10" level={5}>Danh sách lịch hẹn KH đã đặt trước đó theo SĐT</Typography.Title>
        <Table columns={appointmentColumn} dataSource={pagingAppointments?.data} />
      </div>
      <Modal
        title="Thông báo"
        visible={isConfirm}
        onOk={() => {
          onApply?.(value);
          setIsConfirm(false);
          setValue(undefined);
        }}
        onCancel={() => {
          setIsConfirm(false);
          setValue(undefined);
        }}
      >
        <p>Chọn sử dụng thông tin có thể mất thông tin KH mà bạn đã khai thác trước đó, bạn có muốn tiếp tục không?</p>
      </Modal>
    </Space>
  );
};

export default AppointmentPhoneSearch;
