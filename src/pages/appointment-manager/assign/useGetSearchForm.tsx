import React, { FC, useEffect } from 'react';
import { Form, Col, Row, Select, DatePicker, Button, Input } from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import { Role } from 'interface/permission/role.interface';
import { Facility } from 'common/interfaces/facility.interface';
import moment from 'moment';
import { ClearOutlined, SearchOutlined } from '@ant-design/icons';
import { AppointmentStaff } from 'common/interfaces/appointmentStaff.interface';
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 5,
  xl: 5,
  xxl: 5
};

const wrapperColButton: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 4,
  xl: 4,
  xxl: 4
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
  wrapperColButton: { span: 18 }
};

interface AssignmentFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  /** Initial form data */
  values?: Role;
}

export default function useGetSearchForm({
  required = false,
  responsive = false,
  name = 'form',
  values
}: AssignmentFormProps) {
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<AppointmentStaff>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={values}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Ngay hen
  const AppointmentDate: FC = () => {
    const dateFormat = 'DD/MM/YYYY';
    function handleChange(value: any) {}
    const appointmentDate = (
      <Form.Item
        className="fs-10"
        name="appointmentDate"
        label="Ngày hẹn"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <DatePicker
          allowClear={false}
          defaultValue={moment()}
          format={dateFormat}
          onChange={handleChange}
          placeholder="Chọn ngày hẹn"
          // style={{ width: '90%' }}
          // className="inputSearch"
        />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentDate}</Col> : appointmentDate;
  };
  //Don vi
  interface FacilityParentProps {
    facilityParentList: Facility[];
    handleChangeFacilityParent: (parentId: any) => void;
    defaultPFacility?: string;
  }

  const FacilityParent: FC<FacilityParentProps> = ({
    facilityParentList,
    handleChangeFacilityParent,
    defaultPFacility
  }) => {
    useEffect(() => {
      formInstance.setFieldsValue({
        parentFacilityId: defaultPFacility
      });
    }, [defaultPFacility]);
    const facilityParent = (
      <Form.Item
        className="fs-10"
        name="parentFacilityId"
        label="Đơn vị"
        rules={[
          {
            required: false,
            message: 'Đơn vị không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          className="slcSearch"
          onChange={handleChangeFacilityParent}
          defaultValue={defaultPFacility ? defaultPFacility : ''}
        >
          <Select.Option key="" value="">
            -- Tất cả --
          </Select.Option>
          {facilityParentList?.map(option => (
            <Select.Option key={option.id} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{facilityParent}</Col> : facilityParent;
  };

  //Van phong
  interface FacilityProps {
    facilityList: Facility[];
    handleChangeFacility: (id: any) => void;
  }
  const Facility: FC<FacilityProps> = ({ facilityList, handleChangeFacility }) => {
    const facility = (
      <Form.Item
        className="fs-10"
        name="facilityId"
        label="Văn phòng"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeFacility}
          className="slcSearch"
          defaultValue=""
        >
          <Select.Option key="-1" value="">
            -- Tất cả --
          </Select.Option>
          {facilityList?.map(option => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{facility}</Col> : facility;
  };

  //ten can bo tai nha
  interface SiteStaffNameProps{
    onSearch:any
  }
  const SiteStaffName: FC<SiteStaffNameProps> = ({onSearch}) => {
    const siteStaffName = (
      <Form.Item
        className="fs-10"
        name={'siteStaffName'}
        // rules={[{ required: true, max: 100, message: 'Địa chỉ liên hệ không hợp lệ hoặc quá dài.' }]}
        label="Cán bộ TN"
      >
        <Input onKeyDown={e=>{
          if(e.key=='Enter'){
            onSearch();
          }
        }}/>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{siteStaffName}</Col> : siteStaffName;
  };

  //sdt can bo tai nha
  interface SiteStaffPhoneProps{
    onSearch:any
  }
  const SiteStaffPhone: FC<SiteStaffPhoneProps> = ({
    onSearch
  }) => {
    const siteStaffPhone = (
      <Form.Item
        className="fs-10"
        name={'siteStaffPhone'}
        // rules={[{ required: true, max: 100, message: 'Địa chỉ liên hệ không hợp lệ hoặc quá dài.' }]}
        label="SĐT CBTN"
      >
        <Input onKeyDown={e=>{
          if(e.key=='Enter'){
            onSearch();
          }
        }}/>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{siteStaffPhone}</Col> : siteStaffPhone;
  };

  interface PatientPhoneProps{
    onSearch
  }
  const PatientPhone: FC<PatientPhoneProps>= ({onSearch}) => {
    const patientPhone = (
      <Form.Item
        className="fs-10"
        name={'patientPhone'}
        // rules={[{ required: true, max: 100, message: 'Địa chỉ liên hệ không hợp lệ hoặc quá dài.' }]}
        label="SĐT KH"
      >
        <Input onKeyDown={e=>{
          if(e.key=='Enter'){
            onSearch();
          }
        }}/>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{patientPhone}</Col> : patientPhone;
  };

  //sdt can bo tai nha
  interface PatientNameProps{
    onSearch
  }
  const PatientName: FC<PatientNameProps> = ({onSearch}) => {
    const patientName = (
      <Form.Item
        className="fs-10"
        name={'patientName'}
        // rules={[{ required: true, max: 100, message: 'Địa chỉ liên hệ không hợp lệ hoặc quá dài.' }]}
        label="Tên KH"
      >
        <Input onKeyDown={e=>{
          if(e.key=='Enter'){
            onSearch();
          }
        }}/>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{patientName}</Col> : patientName;
  };

  //Trang thai
  const Status: FC = () => {
    function handleChange(value: any) {}
    const status = (
      <Form.Item
        className="fs-10"
        name="status"
        label="Trạng thái"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select className="slcSearch" defaultValue="" placeholder="--Tất cả--">
          <Select.Option key="-1" value="">
            -- Tất cả --
          </Select.Option>
          <Select.Option key="1" value="1">
            Chưa phân
          </Select.Option>
          <Select.Option key="2" value="2">
            Đã phân
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{status}</Col> : status;
  };
  //Buttons
  interface ButtonsProps {
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          onClick={() => {
            onSearch();
          }}
          icon={<SearchOutlined />}
          type="primary"
          title="tìm kiếm"
        ></Button>
        &nbsp;&nbsp;
        <Button title="bỏ lọc" onClick={resetFields} icon={<ClearOutlined />}></Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperColButton}>{buttons}</Col> : buttons;
  };
  return {
    form: formInstance,
    Form: WrappedForm,
    AppointmentDate,
    FacilityParent,
    Status,
    Facility,
    Buttons,
    PatientName,
    PatientPhone,
    SiteStaffName,
    SiteStaffPhone
  };
}
