import React from 'react';
import { Tag } from 'antd';

export const getStatus = (status?: number) =>
  status === 1 ? (
    <Tag color="#87d068">Chờ xác nhận</Tag>
  ) : status === 2 ? (
    <Tag color="#2db7f5">Đã xác nhận</Tag>
  ) : status === 3 ? (
    <Tag color="#2db7f5">Chờ tư vấn</Tag>
  ) : status === 4 ? (
    'Đã tư vấn đầu vào'
  ) : status === 5 ? (
    'Chưa check'
  ) : status === 6 ? (
    'Đã check'
  ) : status === 7 ? (
    'Đã lấy mẫu'
  ) : status === 8 ? (
    'Đã có kết quả'
  ) : status === 9 ? (
    'Đã đăng ký khám'
  ) : status === 10 ? (
    'Đã thanh toán'
  ) : status === 11 ? (
    'Đã khám'
  ) : status === 13 ? (
    <Tag color="#f50">Đã huỷ</Tag>
  ) : (
    ''
  );

export const getStatusPackages = (status?: number) => (status === 1 ? <Tag color="#87d068">Đã duyệt</Tag> : '');

export const formatterMoney = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'VND',
  minimumFractionDigits: 0
});
