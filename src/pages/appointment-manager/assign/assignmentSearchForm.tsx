import React, { FC, useEffect, useState } from 'react';
import { Role } from 'interface/permission/role.interface';
import useGetSearchForm from './useGetSearchForm';
import { Button, PageHeader } from 'antd';
import { useLocale } from 'locales';
import { SearchOutlined, ClearOutlined, DownOutlined, UpOutlined } from '@ant-design/icons';
import { Facility } from 'common/interfaces/facility.interface';
import { useGetFacilitys } from 'hooks/facility';
import moment from 'moment';

export interface Values extends Role {}

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};
interface AssignmentFormSearchProps {
  facilityParentList: Facility[];
  onSearch: ({}) => void;
  defaultPFacility:string;
}

const AssignmentFormSearch: FC<AssignmentFormSearchProps> = ({ facilityParentList, onSearch,defaultPFacility }) => {
  const [parentFacilityId, setParentFacilityId] = useState('');
  const [facilityId, setFacilityId] = useState('');
  const [status, setStatus] = useState('');
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  const { Form, form, AppointmentDate, FacilityParent, Status, Facility, Buttons, PatientName,
    PatientPhone,
    SiteStaffName,
    SiteStaffPhone } = useGetSearchForm({ name: 'searchForm', responsive: true });
  useEffect(()=>{
    if(defaultPFacility){
      loadFacilities({
        variables: {
          page: 1,
          filtered: [
            {
              id: 'parentId',
              value: defaultPFacility,
              operation: '=='
            },
            // {
            //   id: 'isHome',
            //   value: "1",
            //   operation: '=='
            // },
            {
              id: 'status',
              value: "1",
              operation: '=='
            }
          ]
        }
      });
    }
  },[defaultPFacility])
  return (
    <Form >
      <AppointmentDate />
      <Status />
      <FacilityParent
        defaultPFacility={parentFacilityId ? parentFacilityId : defaultPFacility }
        facilityParentList={facilityParentList}
        handleChangeFacilityParent={(parentId: any) => {
          setParentFacilityId(parentId);
          loadFacilities({
            variables: {
              page: 1,
              filtered: [
                {
                  id: 'parentId',
                  value: parentId,
                  operation: '=='
                },
                // {
                //   id: 'isHome',
                //   value: "1",
                //   operation: '=='
                // },
                {
                  id: 'status',
                  value: "1",
                  operation: '=='
                }
              ]
            }
          });
        }}
      />
      <Facility
        handleChangeFacility={data => {}}
        facilityList={facilities}
      />
      <SiteStaffName onSearch={()=>{
        let appointmentDate = moment(form.getFieldValue('appointmentDate')).format('DD/MM/YYYY');
        let status = form.getFieldValue('status');
        let parentFacilityId = form.getFieldValue('parentFacilityId');
        let facilityId = form.getFieldValue('facilityId');
        let patientName = form.getFieldValue('patientName');
        let patientPhone = form.getFieldValue('patientPhone');
        let siteStaffName = form.getFieldValue('siteStaffName');
        let siteStaffPhone = form.getFieldValue('siteStaffPhone');
        onSearch({ appointmentDate, status, parentFacilityId, facilityId,patientName,patientPhone,siteStaffName,siteStaffPhone });
      }} />
      <SiteStaffPhone onSearch={()=>{
        let appointmentDate = moment(form.getFieldValue('appointmentDate')).format('DD/MM/YYYY');
        let status = form.getFieldValue('status');
        let parentFacilityId = form.getFieldValue('parentFacilityId');
        let facilityId = form.getFieldValue('facilityId');
        let patientName = form.getFieldValue('patientName');
        let patientPhone = form.getFieldValue('patientPhone');
        let siteStaffName = form.getFieldValue('siteStaffName');
        let siteStaffPhone = form.getFieldValue('siteStaffPhone');
        onSearch({ appointmentDate, status, parentFacilityId, facilityId,patientName,patientPhone,siteStaffName,siteStaffPhone });
      }}/>
      <PatientName onSearch={()=>{
        let appointmentDate = moment(form.getFieldValue('appointmentDate')).format('DD/MM/YYYY');
        let status = form.getFieldValue('status');
        let parentFacilityId = form.getFieldValue('parentFacilityId');
        let facilityId = form.getFieldValue('facilityId');
        let patientName = form.getFieldValue('patientName');
        let patientPhone = form.getFieldValue('patientPhone');
        let siteStaffName = form.getFieldValue('siteStaffName');
        let siteStaffPhone = form.getFieldValue('siteStaffPhone');
        onSearch({ appointmentDate, status, parentFacilityId, facilityId,patientName,patientPhone,siteStaffName,siteStaffPhone });
      }}/>
      <PatientPhone onSearch={()=>{
        let appointmentDate = moment(form.getFieldValue('appointmentDate')).format('DD/MM/YYYY');
        let status = form.getFieldValue('status');
        let parentFacilityId = form.getFieldValue('parentFacilityId');
        let facilityId = form.getFieldValue('facilityId');
        let patientName = form.getFieldValue('patientName');
        let patientPhone = form.getFieldValue('patientPhone');
        let siteStaffName = form.getFieldValue('siteStaffName');
        let siteStaffPhone = form.getFieldValue('siteStaffPhone');
        onSearch({ appointmentDate, status, parentFacilityId, facilityId,patientName,patientPhone,siteStaffName,siteStaffPhone });
      }} />
      <Buttons
        onSearch={() => {
          let appointmentDate = moment(form.getFieldValue('appointmentDate')).format('DD/MM/YYYY');
          let status = form.getFieldValue('status');
          let parentFacilityId = form.getFieldValue('parentFacilityId');
          let facilityId = form.getFieldValue('facilityId');
          let patientName = form.getFieldValue('patientName');
          let patientPhone = form.getFieldValue('patientPhone');
          let siteStaffName = form.getFieldValue('siteStaffName');
          let siteStaffPhone = form.getFieldValue('siteStaffPhone');
          onSearch({ appointmentDate, status, parentFacilityId, facilityId,patientName,patientPhone,siteStaffName,siteStaffPhone });
        }}
        resetFields={() => {
          form.resetFields();
          var defaultF = facilityParentList.filter(item=>{
            return item.code =="KTN"
          })
          var defaultP = "";
          if(defaultF && defaultF.length >0){
            defaultP = (defaultF[0].id);
          }
          setParentFacilityId('');
          onSearch({ name: '', code: '', parentFacilityId: defaultP, facilityId: '', status: '',patientName:'',patientPhone:"",siteStaffName:"",siteStaffPhone:"" });
        }}
      />
    </Form>
  );
};

export default AssignmentFormSearch;
