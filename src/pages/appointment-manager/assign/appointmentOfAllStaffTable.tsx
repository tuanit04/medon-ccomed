import React, { FC, useEffect, useState } from 'react';
import { Button, Divider, Modal, Table, Tooltip, Badge, Alert } from 'antd';
import { PhoneOutlined } from '@ant-design/icons';
import { VAppointmentOfStaff } from 'common/interfaces/vAppointmentOfStaff.interface';
import { useGetAppointmentOfAllStaff } from 'hooks/appointment/useGetAppointmentOfAllStaff';
import Marquee from 'react-fast-marquee';
import { Label } from 'recharts';

interface AppointmentOfAllStaffTableProps {
  pFacilityIdStaff: string;
  facilityIdStaff: string;
  appointmentDateStaff: string;
  randomNumberValue: number;
  rowAppointmentStaffSelected: number;
  onSelectSiteStaff: (siteStaffId: string) => void;
  reload: number;
}

const AppointmentOfAllStaffTable: FC<AppointmentOfAllStaffTableProps> = ({
  pFacilityIdStaff,
  facilityIdStaff,
  appointmentDateStaff,
  randomNumberValue,
  onSelectSiteStaff,
  rowAppointmentStaffSelected,
  reload
}) => {
  const [tableData, setTableData]: any = useState([]);
  const [worktimeDetail, setWorktimeDetail]: any = useState([]);
  const [staffId, setStaffId]: any = useState('');
  //Hook lấy danh sách Cán bộ tại nhà thuộc văn phòng/đơn vị
  const {
    appointmentOfAllStaff,
    isLoadingAppointmentOfAllStaff,
    loadAppointmentOfAllStaff
  } = useGetAppointmentOfAllStaff();
  useEffect(() => {
    if (rowAppointmentStaffSelected === 0) {
      loadAppointmentOfAllStaff({
        variables: {
          appointmentDate: appointmentDateStaff,
          parentFacilityId: pFacilityIdStaff,
          facilityId: facilityIdStaff
        }
      });
    } else {
      if (pFacilityIdStaff !== '' && appointmentDateStaff !== '') {
        if (facilityIdStaff !== '') {
          loadAppointmentOfAllStaff({
            variables: {
              appointmentDate: appointmentDateStaff,
              parentFacilityId: pFacilityIdStaff,
              facilityId: facilityIdStaff
            }
          });
        } else {
          loadAppointmentOfAllStaff({
            variables: {
              appointmentDate: appointmentDateStaff,
              parentFacilityId: pFacilityIdStaff
            }
          });
        }
      }
    }
  }, [pFacilityIdStaff, facilityIdStaff, appointmentDateStaff, reload, rowAppointmentStaffSelected]);
  const getUnExcute = vAppointmentOfStaffList => {
    var totalUsual = 0;
    var totalUnUsual = 0;
    if (vAppointmentOfStaffList) {
      vAppointmentOfStaffList?.map(item => {
        /*if ([2, 4, 5, 6].indexOf(item.status) > -1) {
          total = total + item.amount;
        }*/
        if ([7].indexOf(item.status) === -1) {
          if (item?.type === 'USUAL') {
            totalUsual = totalUsual + item.amount;
          } else {
            totalUnUsual = totalUnUsual + item.amount;
          }
        }
      });
    }
    return (
      <>
        <span style={{ color: '#000000' }}>{totalUsual}</span>
        &nbsp;&nbsp;|&nbsp;&nbsp;
        <span style={{ color: '#003cff' }}>{totalUnUsual}</span>
      </>
    );
  };

  const getExcute = vAppointmentOfStaffList => {
    var totalUsual = 0;
    var totalUnUsual = 0;
    if (vAppointmentOfStaffList) {
      vAppointmentOfStaffList?.map(item => {
        if ([7].indexOf(item.status) > -1) {
          if (item?.type === 'USUAL') {
            totalUsual = totalUsual + item.amount;
          } else {
            totalUnUsual = totalUnUsual + item.amount;
          }
        }
      });
    }
    return (
      <>
        <span style={{ color: '#000000' }}>{totalUsual}</span>
        &nbsp;&nbsp;|&nbsp;&nbsp;
        <span style={{ color: '#003cff' }}>{totalUnUsual}</span>
      </>
    );
  };

  const getTotalSchedule = vAppointmentOfStaffList => {
    var totalUsual = 0;
    var totalUnUsual = 0;
    if (vAppointmentOfStaffList) {
      vAppointmentOfStaffList?.map(item => {
        if (item?.type === 'USUAL') {
          totalUsual = totalUsual + item.amount;
        } else {
          totalUnUsual = totalUnUsual + item.amount;
        }
      });
    }
    return (
      <>
        <span style={{ color: '#000000' }}>{totalUsual}</span>
        &nbsp;&nbsp;|&nbsp;&nbsp;
        <span style={{ color: '#003cff' }}>{totalUnUsual}</span>
      </>
    );
  };
  const getTotalOfUnExcute = staffId => {
    var totalUsual = 0;
    var totalUnUsual = 0;
    let worktimes = appointmentOfAllStaff?.data?.filter(a => a.staffId === staffId);
    worktimes?.map(worktime => {
      worktime?.vAppointmentOfStaffList?.map(item => {
        if ([7].indexOf(item.status) === -1) {
          if (item?.type === 'USUAL') {
            totalUsual = totalUsual + item.amount;
          } else {
            totalUnUsual = totalUnUsual + item.amount;
          }
        }
      });
    });
    return (
      <>
        <span style={{ color: '#000000' }}>{totalUsual}</span>
        &nbsp;&nbsp;|&nbsp;&nbsp;
        <span style={{ color: '#003cff' }}>{totalUnUsual}</span>
      </>
    );
  };

  const getTotalOfExcute = staffId => {
    var totalUsual = 0;
    var totalUnUsual = 0;
    let worktimes = appointmentOfAllStaff?.data?.filter(a => a.staffId === staffId);
    worktimes?.map(worktime => {
      worktime?.vAppointmentOfStaffList?.map(item => {
        if ([7].indexOf(item.status) > -1) {
          if (item?.type === 'USUAL') {
            totalUsual = totalUsual + item.amount;
          } else {
            totalUnUsual = totalUnUsual + item.amount;
          }
        }
      });
    });
    return (
      <>
        <span style={{ color: '#000000' }}>{totalUsual}</span>
        &nbsp;&nbsp;|&nbsp;&nbsp;
        <span style={{ color: '#003cff' }}>{totalUnUsual}</span>
      </>
    );
  };

  const getTotalOfTotalSchedule = staffId => {
    var totalUsual = 0;
    var totalUnUsual = 0;
    let worktimes = appointmentOfAllStaff?.data?.filter(a => a.staffId === staffId);
    worktimes?.map(worktime => {
      worktime?.vAppointmentOfStaffList?.map(item => {
        if (item?.type === 'USUAL') {
          totalUsual = totalUsual + item.amount;
        } else {
          totalUnUsual = totalUnUsual + item.amount;
        }
      });
    });
    return (
      <>
        <span style={{ color: '#000000' }}>{totalUsual}</span>
        &nbsp;&nbsp;|&nbsp;&nbsp;
        <span style={{ color: '#003cff' }}>{totalUnUsual}</span>
      </>
    );
  };
  const genDescription = id => {
    let worktimes = appointmentOfAllStaff?.data?.filter(a => a.staffId === id);
    return (
      <Table
        rowKey="id"
        pagination={false}
        bordered
        loading={isLoadingAppointmentOfAllStaff}
        dataSource={worktimes}
        title={() => (
          <>
            <div style={{ float: 'left', width: '60%' }}>
              <span>
                Chi tiết ca làm việc của cán bộ <span style={{ color: 'red' }}>{worktimes?.[0]?.['staffName']}</span>
              </span>
            </div>
            <div style={{ float: 'right', width: '40%' }}>
              {/*<Alert
                style={{ padding: '4px 15px;' }}
                type="info"
                showIcon
                banner
                message={
                  <Marquee pauseOnHover className="lblTableValue fs-10" gradient={false}>
                    <Badge color="#000000" text="Lịch thường" />
                    &nbsp;&nbsp;
                    <Badge color="#f50" text="Lịch Covid" />
                  </Marquee>
                }
              />*/}
            </div>
            <div style={{ clear: 'both' }}></div>
          </>
        )}
      >
        <Table.Column<VAppointmentOfStaff> title="STT" align="center" render={(value, item, index) => index + 1} />
        <Table.Column<VAppointmentOfStaff> title="Ca làm việc" render={(_, { workSessionName }) => workSessionName} />
        <Table.Column<VAppointmentOfStaff> title="Cung đường" render={(_, { routeName }) => routeName} />
        <Table.Column<VAppointmentOfStaff>
          align="center"
          title="Lịch chưa thực hiện"
          render={(_, { vAppointmentOfStaffList }) => getUnExcute(vAppointmentOfStaffList)}
        />
        <Table.Column<VAppointmentOfStaff>
          align="center"
          title="Lịch đã thực hiện"
          render={(_, { vAppointmentOfStaffList }) => getExcute(vAppointmentOfStaffList)}
        />
        <Table.Column<VAppointmentOfStaff>
          align="center"
          title="Tổng lịch đã phân"
          render={(_, { vAppointmentOfStaffList }) => getTotalSchedule(vAppointmentOfStaffList)}
        />
      </Table>
    );
  };
  useEffect(() => {
    if (appointmentOfAllStaff?.data) {
      let uniqueSiteStaffs = Array.from(new Set(appointmentOfAllStaff?.data?.map(a => a.staffId))).map(id => {
        return appointmentOfAllStaff?.data?.find(a => a.staffId === id);
      });
      for (let i = 0; i < uniqueSiteStaffs?.length; i++) {
        let staffId = uniqueSiteStaffs[i]['staffId'];
        uniqueSiteStaffs[i] = { ...uniqueSiteStaffs[i], description: genDescription(staffId) };
      }
      setTableData(uniqueSiteStaffs);
    }
  }, [appointmentOfAllStaff]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalContent, setModalContent]: any = useState();
  useEffect(() => {
    //console.log('appointmentOfAllStaff : ', appointmentOfAllStaff);
  }, [appointmentOfAllStaff]);
  return (
    <>
      <Table
        rowKey="id"
        pagination={false}
        bordered
        className="fs-10"
        loading={isLoadingAppointmentOfAllStaff}
        dataSource={tableData}
        scroll={{ y: 'calc(70vh - 250px)' }}
        onRow={(record, rowIndex) => {
          return {
            onClick: event => {
              onSelectSiteStaff(record['staffId']);
              return;
            },
            onDoubleClick: () => {
              setModalContent(genDescription(record['staffId']));
              setIsModalVisible(true);
              return;
            }
          };
        }}
        title={() => (
          <>
            <div style={{ float: 'left', width: '99%' }}>
              <div style={{ float: 'left', width: '60%' }}>
                <label className="lblTableTxt fs-10">Cán bộ tại nhà thuộc văn phòng/đơn vị </label>
                <label className="lblTableValue fs-10">
                  ({appointmentOfAllStaff ? appointmentOfAllStaff['records'] : 0})
                </label>
              </div>
              <div style={{ float: 'right', width: '40%' }}>
                {/*<Alert
                  type="info"
                  showIcon
                  banner
                  message={
                    <Marquee pauseOnHover className="lblTableValue fs-10" gradient={false}>
                      <Badge color="#000000" text="Lịch thường" />
                      &nbsp;&nbsp;
                      <Badge color="#f50" text="Lịch Covid" />
                    </Marquee>
                  }
                />*/}
              </div>
              <div style={{ clear: 'both' }}></div>
            </div>
            <div style={{ float: 'right' }}>
              {/* <Button className="fix-icon fs-10" type="primary" shape="circle" icon={<PhoneOutlined />} />*/}
            </div>
            <div style={{ clear: 'both' }}></div>
          </>
        )}
      >
        <Table.Column<VAppointmentOfStaff>
          width={50}
          title="STT"
          align="center"
          render={(value, item, index) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <span style={{ fontWeight: 500 }}>{index + 1}</span>
            </Tooltip>
          )}
        />
        {/* <Table.Column<VAppointmentOfStaff> width={100} align="center" title="Mã NV" render={(_, { staffCode }) => staffCode} /> */}
        <Table.Column<VAppointmentOfStaff>
          width={200}
          title="Tên nhân viên"
          render={(_, { staffName }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <span style={{ fontWeight: 500 }}>{staffName}</span>
            </Tooltip>
          )}
        />
        {/*<Table.Column<VAppointmentOfStaff>
        width={100}
        title="Ca làm việc"
        render={(_, { workSessionName }) => workSessionName}
      />
      <Table.Column<VAppointmentOfStaff> width={100} title="Cung đường" render={(_, { routeName }) => routeName} />*/}
        <Table.Column<VAppointmentOfStaff>
          width={100}
          align="center"
          title="Lịch chưa thực hiện"
          render={(_, { staffId }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <span style={{ fontWeight: 500 }}>{getTotalOfUnExcute(staffId)}</span>
            </Tooltip>
          )}
        />
        <Table.Column<VAppointmentOfStaff>
          width={100}
          align="center"
          title="Lịch đã thực hiện"
          render={(_, { staffId }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <span style={{ fontWeight: 500 }}>{getTotalOfExcute(staffId)}</span>
            </Tooltip>
          )}
        />
        <Table.Column<VAppointmentOfStaff>
          width={100}
          align="center"
          title="Tổng lịch đã phân"
          render={(_, { staffId }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <span style={{ fontWeight: 500 }}>{getTotalOfTotalSchedule(staffId)}</span>
            </Tooltip>
          )}
        />
      </Table>
      <Modal
        title="Chi tiết ca làm việc của cán bộ"
        visible={isModalVisible}
        okText="Đóng"
        width={1000}
        cancelButtonProps={{
          style: { display: 'none' }
        }}
        onOk={() => {
          setIsModalVisible(false);
        }}
        onCancel={() => {
          setIsModalVisible(false);
        }}
      >
        {modalContent}
      </Modal>
    </>
  );
};

export default AppointmentOfAllStaffTable;
