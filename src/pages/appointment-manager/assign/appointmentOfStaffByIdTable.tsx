import React, { FC, useState, useEffect, useCallback } from 'react';
import { Button, Table, Tag, Modal, Radio, Divider, Checkbox } from 'antd';
import { useLocale, LocaleFormatter } from 'locales';
import { apiGetRoleList } from 'api/permission/role.api';
import { Role, RoleStatus } from 'interface/permission/role.interface';
// import { useGetAppointmentStaff } from 'hooks/appointment/useGetAppointmentStaff';
import { AppointmentStaff } from 'common/interfaces/appointmentStaff.interface';
import { useGetAppointmentOfStaffById } from 'hooks/appointment/useGetAppointmentOfStaffById';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { useCheckAppointment } from 'hooks/appointment';

interface AppointmentOfStaffTableProps {
  staffId: string;
  appointmentDate: string;
  reload: number;
  rowAppointmentStaffSelected: number;
}

const AppointmentOfStaffTable: FC<AppointmentOfStaffTableProps> = ({
  staffId,
  appointmentDate,
  reload,
  rowAppointmentStaffSelected
}) => {
  //Hook lấy danh sách Danh sách lịch hẹn được phân cho cán bộ
  const {
    appointmentOfStaffById,
    isLoadingAppointmentOfStaffById,
    loadAppointmentOfStaffById
  } = useGetAppointmentOfStaffById();
  //Hook Check lịch
  const {
    checkAppointment,
    isLoadingCheckAppointment,
    errorCheckAppointment,
    resultCheckAppointment
  } = useCheckAppointment();
  useEffect(() => {
    if (staffId && appointmentDate) {
      loadAppointmentOfStaffById({
        variables: {
          appointmentDate,
          staffId
        }
      });
    }
  }, [staffId, appointmentDate]);

  useEffect(() => {
    if (rowAppointmentStaffSelected === 0) {
      loadAppointmentOfStaffById({
        variables: {}
      });
    } else if (reload != 0 && staffId && appointmentDate) {
      loadAppointmentOfStaffById({
        variables: {
          appointmentDate,
          staffId
        }
      });
    }
  }, [reload, rowAppointmentStaffSelected]);

  return (
    <Table
      rowKey="id"
      pagination={false}
      bordered
      className="fs-10"
      loading={isLoadingAppointmentOfStaffById}
      dataSource={staffId ? appointmentOfStaffById?.data : []}
      scroll={{ x: 800, y: 'calc(70vh - 250px)' }}
      title={() => (
        <div>
          <label className="lblTableTxt fs-10">
            Danh sách lịch hẹn được phân cho cán bộ{' '}
            {staffId ? appointmentOfStaffById?.data?.[0]?.['siteStaffName'] : ''}{' '}
          </label>
          <label className="lblTableValue fs-10">
            ({appointmentOfStaffById && staffId ? appointmentOfStaffById['records'] : 0})
          </label>
        </div>
      )}
    >
      <Table.Column<AppointmentStaff>
        width={100}
        title="Mã lịch"
        align="center"
        render={(_, { appointmentHisId }) => appointmentHisId}
      />
      <Table.Column<AppointmentStaff> width={150} title="Cung đường" render={(_, { routeName }) => routeName} />
      <Table.Column<AppointmentStaff>
        width={150}
        title="Địa chỉ"
        render={(_, { appointmentAddress }) => appointmentAddress}
      />
      <Table.Column<AppointmentStaff>
        width={150}
        title="Ghi chú"
        render={(_, { appointmentNote }) => appointmentNote}
      />
      <Table.Column<AppointmentStaff>
        width={80}
        title="Giờ hẹn"
        align="center"
        render={(_, { workTimeName }) => workTimeName}
      />
      <Table.Column<AppointmentStaff> width={100} title="Cán bộ TN" render={(_, { siteStaffName }) => siteStaffName} />
      <Table.Column<AppointmentStaff>
        width={80}
        align="center"
        title="Check lịch"
        render={(_, { isCheck, appointmentId }) => {
          let check: boolean = isCheck === null || isCheck === undefined ? false : isCheck === 1 ? true : false;
          return (
            <Checkbox
              checked={check}
              onChange={e => {
                console.log('e : ');
                if (e.target.checked) {
                  Modal.confirm({
                    icon: <ExclamationCircleOutlined />,
                    title: 'Bạn có chắc chắn muốn check lịch đã chọn ?',
                    async onOk() {
                      await checkAppointment({
                        appointmentIds: [appointmentId]
                      });
                      if (staffId && appointmentDate) {
                        loadAppointmentOfStaffById({
                          variables: {
                            appointmentDate,
                            staffId
                          }
                        });
                      }
                    },
                    onCancel() {
                      //console.log('Cancel');
                    }
                  });
                } else {
                  Modal.confirm({
                    icon: <ExclamationCircleOutlined />,
                    title: 'Bạn có chắc chắn muốn bỏ check lịch đã chọn ?',
                    async onOk() {
                      await checkAppointment({
                        appointmentIds: [appointmentId]
                      });
                      if (staffId && appointmentDate) {
                        loadAppointmentOfStaffById({
                          variables: {
                            appointmentDate,
                            staffId
                          }
                        });
                      }
                    },
                    onCancel() {
                      //console.log('Cancel');
                    }
                  });
                }
              }}
            ></Checkbox>
          );
        }}
      />
      <Table.Column<AppointmentStaff>
        width={120}
        title="Thời gian lấy mẫu"
        render={(_, { sampleTakenDate }) => sampleTakenDate}
      />
    </Table>
  );
};

export default AppointmentOfStaffTable;
