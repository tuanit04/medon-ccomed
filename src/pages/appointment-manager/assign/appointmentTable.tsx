import React, { FC, useState, useEffect, useCallback } from 'react';
import { Button, Table, Modal, Select, Checkbox } from 'antd';
import { CopyrightOutlined, DeleteOutlined, ExclamationCircleOutlined, RedoOutlined } from '@ant-design/icons';
import { useCheckAppointment } from 'hooks/appointment';
import { useGetSiteStaffsLazy } from 'hooks/site-staff';
import { useGetAppointmentStaffLazy } from 'hooks/appointment/useGetAppointmentStaff';
import { AppointmentStaff } from 'common/interfaces/appointmentStaff.interface';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { useAssignAppointment } from 'hooks/appointment/useAssignAppointment';
import { openNotificationRight } from 'utils/notification';
import moment from 'moment';
import { useAssignAppointmentNews } from 'hooks/appointment/useAssignAppointmentNews';
import { useNavigate } from 'react-router';
import { removeAssignAppointment } from 'api/appoiment-covid-home/appoimentCovid';
import { functionCodeConstants } from 'constants/functions';
import { useAppState } from 'helpers';
import Item from 'antd/lib/list/Item';
interface AppointmentTableProps {
  parentFacilityId: string;
  facilityId: string;
  randomNumberValue: number;
  appointmentDate: string;
  patientName:string;
  patientPhone:string;
  siteStaffName:string;
  siteStaffPhone:string;
  status: string;
  reload: number;
  handleChangeStaff: (staffId: string) => void;
  setRowSelected: (value: number) => void;
  handleReloadAppointmentOfStaffTableById: (reload: number) => void;
  handleChoseAssign: (pFacilityCB: string, facilityName: string, appointmentDateStaff: string) => void;
  onModify: (id: string) => void;
  isRun: boolean;
  // handleChangeSiteStaff:() =>void;
}

const AppointmentTable: FC<AppointmentTableProps> = ({
  parentFacilityId,
  randomNumberValue,
  facilityId,
  appointmentDate,
  handleChangeStaff,
  handleChoseAssign,
  handleReloadAppointmentOfStaffTableById,
  reload,
  setRowSelected,
  onModify,
  status,
  isRun,
  patientName,
  patientPhone,
  siteStaffName,
  siteStaffPhone,
  // handleChangeSiteStaff
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(500);
  const [pagination, setPagination] = useState({});
  const [selectedRowKeys, setSelectedRowKeys]: any = useState([]);
  const [listFCD, setListFCD] = useState([]);
  const [listFDC, setListFDC] = useState([]);
  const [listFGC, setListFGC] = useState([]);
  const [listFGH, setListFGH] = useState([]);
  const [listFCB, setListFCB] = useState([]);
  const [listAppointmentHisId, setListAppointmentHisId] = useState([]);
  const [listFNH, setListFNH] = useState([]);
  const [listSl, setListSl]: any = useState([]);
  const [listAdd, setListAdd]: any = useState([]);
  const [staf, setStaf]: any = useState('');
  const functionOb = useAppState(state => state.user.functionObject);
  const [workTime,setWorkTime] =useState("");
  const [listU ,setListU]:any= useState([]);
  const navigate = useNavigate();
  let filtered: FilteredInput[] = [];
  if (appointmentDate) {
    filtered.push({
      id: 'appointmentDate',
      value: appointmentDate,
      operation: '=='
    });
  } else {
    filtered.push({
      id: 'appointmentDate',
      value: moment().format('DD/MM/YYYY'),
      operation: '=='
    });
  }
  if (status) {
    if (status === '1') {
      filtered.push({
        id: 'siteStaffId',
        value: 'null',
        operation: 'is_null'
      });
    } else if (status === '2') {
      filtered.push({
        id: 'siteStaffId',
        value: 'null',
        operation: 'is_not_null'
      });
    }
  }
  if (parentFacilityId) {
    filtered.push({
      id: 'parentFacilityId',
      value: parentFacilityId,
      operation: '=='
    });
  }
  if (facilityId) {
    filtered.push({
      id: 'facilityId',
      value: facilityId,
      operation: '=='
    });
  }

  if (patientName) {
    filtered.push({
      id: 'patientName',
      value: patientName,
      operation: '~'
    });
  }
  if (patientPhone) {
    filtered.push({
      id: 'patientPhone',
      value: patientPhone,
      operation: '~'
    });
  }
  if (siteStaffName) {
    filtered.push({
      id: 'siteStaffName',
      value: siteStaffName,
      operation: '~'
    });
  }
  if (siteStaffPhone) {
    filtered.push({
      id: 'siteStaffPhone',
      value: siteStaffPhone,
      operation: '~'
    });
  }

  //Hook Lấy danh sách cán bộ TN
  const { siteStaffs, isLoadingSiteStaffs, loadSiteStaffs } = useGetSiteStaffsLazy();
  const variables = {
    page: page,
    pageSize: pageSize,
    filtered: filtered,
    sorted: [
      { id: 'routeName', desc: false },
      { id: 'workTimeName', desc: false }
    ]
  };
  //Hook lấy danh sách Lịch hẹn khám
  const {
    loadAppointments,
    pagingAppointmentStaff,
    isLoadingAppointmentStaff,
    errorAppointmentStaff,
    refetchAppointmentStaff
  } = useGetAppointmentStaffLazy(variables);

  // useEffect(()=>{
  //   if(pagingAppointmentStaff){
  //     setListU(pagingAppointmentStaff?.data)
  //   }else{
  //     setListU([]);
  //   }
  // },[pagingAppointmentStaff])

  useEffect(() => {
    setSelectedRowKeys([]);
    //setRowSelected(0);
    loadAppointments({ variables });
  }, [randomNumberValue]);
  //Hook Check lịch
  const {
    checkAppointment,
    isLoadingCheckAppointment,
    errorCheckAppointment,
    resultCheckAppointment
  } = useCheckAppointment();
  useEffect(() => {
    //Load danh sách CBTN
    if (parentFacilityId) {
      let filtered: FilteredInput[] = [];
      if (parentFacilityId) {
        filtered.push({
          id: 'parentFacilityId',
          value: parentFacilityId,
          operation: '=='
        });
      }
      filtered.push({
        id: 'status',
        value: '1',
        operation: '=='
      });
      loadSiteStaffs({
        variables: {
          filtered: filtered
        }
      });
    }
  }, [parentFacilityId, facilityId]);
  useEffect(() => {
    // setPagination({ current: page, total: pagingAppointmentStaff['records'] });
    setListAdd(pagingAppointmentStaff.data);
  }, [pagingAppointmentStaff]);
  const handleTableChange = (pagination, filters, sorter,extra) => {
    console.log(extra);
    // setListU(extra?.currentDataSource)
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    if (pagination['pageSize'] != pageSize) {
      setPage(1);
    } else {
      setPage(pagination['current']);
    }
    setPageSize(pagination['pageSize']);
  };
  useEffect(() => {
    loadAppointments({ variables });
  }, [page, pageSize]);
  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };
  useEffect(() => {
    setRowSelected(selectedRowKeys?.length);
  }, [selectedRowKeys]);
  const onSelectTick = (record, selected, selectedRows, nativeEvent) => {
    var list: any[] = selectedRowKeys;
    var listSlT: any[] = listSl;
    var listUTMP:any[] = listU;
    var check = false;
    for (var i = 0; i < listSlT.length; i++) {
      if (listSlT[i].parentFacilityId == record.parentFacilityId && record.facilityId == listSlT[i].facilityId) {
        check = true;
        break;
      }
    }
    if (listSlT.length > 1 && check) {
      handleChoseAssign('', '', '');
    } else {
      handleChoseAssign(record.parentFacilityId, record.facilityId, record.appointmentDate);
    }
    var item = {
      appointmentId: record.appointmentId,
      workTimeId: record.workTimeId,
      routeId: record.routeId
    };
    var itemU = {
      ...record
    }
    if (selected) {
      listSlT.push(item);
      listUTMP.push(itemU)
    } else {
      listSlT.splice(listSlT.indexOf(item), 1);
      listUTMP.push(listUTMP.indexOf(itemU), 1)
    }
    setListU(listUTMP)
    setListSl(listSlT);
  };

  const onSelectAllTable = (selected, selectedRows, changeRows) => {
    if (selected) {
      var list: any[] = [];
      listAdd?.map(item => {
        var newItem = {
          appointmentId: item.appointmentId,
          workTimeId: item.workTimeId,
          routeId: item.routeId
        };
        list.push(newItem);
      });
      setListSl([...list]);
      setListU([...listAdd]);
    } else {
      setListSl([]);
    }
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    onSelect: onSelectTick,
    onSelectAll: onSelectAllTable,
    columnWidth: 32
  };

  const removeAssign = async (listAppoimentId: []) => {
    var result: any = await removeAssignAppointment(listAppoimentId);
    if (result?.data.status == 1) {
      openNotificationRight('Huỷ phân lịch thành công!');
      setStaf('');
      handleReloadAppointmentOfStaffTableById(reload + 1);
      loadAppointments({ variables });
      setSelectedRowKeys([]);
    } else {
      openNotificationRight('Lỗi : ' + result?.data?.message, 'error');
    }
  };

  useEffect(() => {
    if (isRun) {
      setPage(1);
      loadAppointments({
        variables: {
          ...variables,
          page: 1
        }
      });
    }
  }, [facilityId, parentFacilityId, appointmentDate, status]);

  //Phân lịch lẻ
  const {
    assignAppointment,
    isLoadingAssignAppointment,
    errorAssignAppointment,
    resultAssignAppointment
  } = useAssignAppointment();
  //Phân lịch nhiều
  const {
    assignAppointmentNew,
    isLoadingAssignAppointments,
    errorAssignAppointments,
    resultAssignAppointments
  } = useAssignAppointmentNews();
  const onChangeStaff = async (staffInfo: string) => {
    // if (staffInfo) {
    setStaf(staffInfo);
    let staffId = staffInfo.split('***')['0'];
    let staffName = staffInfo.split('***')['1'];
    // handleChangeStaff(staffId);
    if (selectedRowKeys.length !== 0) {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn phân các lịch đã chọn cho cán bộ tại nhà ' + staffName + ' ?',
        async onOk() {
          // if (selectedRowKeys.length === 1) {
          //   await assignAppointment({
          //     appointmentId: selectedRowKeys[0],
          //     staffId: staffId
          //   });
          //   refetchAppointmentStaff().then(
          //     data => {
          //       if(data.data.code=="SUCCESS"){
          //         console.log("test");
          //         handleReloadAppointmentOfStaffTableById(reload+1)
          //       }
          //     }
          //   );
          // } else if (selectedRowKeys.length > 1) {
          await assignAppointmentNew({
            appointmentIds: listSl,
            staffId: staffId,
            appointmentDate: appointmentDate
          });
          // loadAppointments({
          //   variables
          // });
          // }
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    }
    // }
  };

  useEffect(() => {
    if (!isLoadingAssignAppointments) {
      setStaf('');
      handleReloadAppointmentOfStaffTableById(reload + 1);
      loadAppointments({ variables });
    }
  }, [isLoadingAssignAppointments]);

  const onClickSelectBox = e => {
    if (selectedRowKeys.length === 0) {
      openNotificationRight('Vui lòng chọn lịch hẹn trước khi chọn cán bộ tại nhà.', 'warning');
      return;
    }
  };



  const columns = [
    // {
    //   title: 'STT',
    //   dataIndex: 'STT',
    //   render:(value, item, index) => { return (page - 1) * pageSize + index + 1  } ,
    //   align:'center' as 'center',
    //   width:50,
    // },

    {
      title: 'Khung giờ',
      dataIndex: 'GH',
      align: 'center' as 'center',
      filters: listFGH,
      onFilter: (value, record) => {
        setWorkTime(value)
        return record?.workTimeName.startsWith(value)
      },
      filterMode: 'tree',
      filterSearch: true,
      render: (_, { workTimeName, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {workTimeName}
          </span>
        ) : (
          workTimeName
        ),
      width: 60
    },
    {
      title: 'Cung đường',
      dataIndex: 'CD',
      filters: listFCD,
      onFilter: (value, record) => {
        // setPage(page);
        // setPageSize(pageSize);
        return record?.routeName == value;
      },
      filterMode: 'tree',
      filterSearch: true,
      render: (_, { routeName, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {routeName}
          </span>
        ) : (
          routeName
        ),
      width: 100
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'DC',
      filters: listFDC,
      onFilter: (value, record) => {
        // setPage(page);
        // setPageSize(pageSize);
        return record?.appointmentAddress == value;
      },
      filterMode: 'tree',
      filterSearch: true,
      render: (_, { appointmentAddress, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {appointmentAddress}
          </span>
        ) : (
          appointmentAddress
        ),
      width: 150
    },
    {
      title: 'Ghi chú',
      dataIndex: 'GC',
      filters: listFGC,
      onFilter: (value, record) => {
        // setPage(page);
        // setPageSize(pageSize);
        return record?.appointmentNote == value;
      },
      filterMode: 'tree',
      filterSearch: true,
      render: (_, { appointmentNote, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {appointmentNote}
          </span>
        ) : (
          appointmentNote
        ),
      width: 150
    },
    {
      title: 'Cán bộ TN',
      dataIndex: 'CB',
      filters: listFCB,
      onFilter: (value, record) => {
        // setPage(page);
        // setPageSize(pageSize);
        return record?.siteStaffName == value;
      },
      filterMode: 'tree',
      filterSearch: true,
      render: (_, { siteStaffName, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {siteStaffName}
          </span>
        ) : (
          siteStaffName
        ),
      width: 100
    },
    {
      title: 'SĐT CBTN',
      dataIndex: 'SDTCB',
      // filters: listFCB,
      // onFilter: (value, record) => {
      //   // setPage(page);
      //   // setPageSize(pageSize);
      //   return record?.siteStaffPhone == value;
      // },
      filterMode: 'tree',
      filterSearch: true,
      render: (_, { siteStaffPhone, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {siteStaffPhone}
          </span>
        ) : (
          siteStaffPhone
        ),
      width: 100
    },
    {
      title: 'Khách hàng',
      dataIndex: 'patientName',
      // filters: listFCB,
      // onFilter: (value, record) => {
      //   // setPage(page);
      //   // setPageSize(pageSize);
      //   return record?.siteStaffName == value;
      // },
      filterMode: 'tree',
      filterSearch: true,
      render: (_, { patientName, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {patientName}
          </span>
        ) : (
          patientName
        ),
      width: 100
    },
    {
      title: 'SĐT Khách hàng',
      dataIndex: 'patientPhone',
      // filters: listFCB,
      filterMode: 'tree',
      filterSearch: true,
      render: (_, { patientPhone, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {patientPhone}
          </span>
        ) : (
          patientPhone
        ),
      width: 100
    },
    {
      title: 'Mã lịch',
      dataIndex: 'Mã lịch',
      align: 'center' as 'center',
      filters: listAppointmentHisId,
      onFilter: (value, record) => {
        // setPage(page);
        // setPageSize(pageSize);
        return record?.appointmentHisId == value;
      },
      filterMode: 'tree',
      filterSearch: true,
      width: 100,
      render: (_, { appointmentHisId, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {appointmentHisId}
          </span>
        ) : (
          appointmentHisId
        )
    },
    {
      title: 'Ngày hẹn',
      dataIndex: 'NH',
      filters: listFNH,
      onFilter: (value, record) => {
        // setPage(page);
        // setPageSize(pageSize);
        return record?.appointmentDate == value;
      },
      filterMode: 'tree',
      filterSearch: true,
      onFilterSeach: (value, record) => record?.appointmentDate.startsWith(value),
      render: (_, { appointmentDate, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {appointmentDate}
          </span>
        ) : (
          appointmentDate
        ),
      width: 80
    },
    {
      title: 'Check lịch',
      dataIndex: 'CL',
      align: 'center' as 'center',
      // filters:listFCD,
      // onFilter: (value, record) => record?.appointmentAddress.startsWith(value) ,
      // filterMode: 'tree',
      // filterSearch: true,
      // onFilterSeach:(value, record) => record?.appointmentAddress.startsWith(value) ,
      render: (_, { isCheck, appointmentId }) => {
        let check: boolean = isCheck === null || isCheck === undefined ? false : isCheck === 1 ? true : false;
        return (
          <Checkbox
            checked={check}
            onChange={e => {
              if (e.target.checked) {
                Modal.confirm({
                  icon: <ExclamationCircleOutlined />,
                  title: 'Bạn có chắc chắn muốn check lịch đã chọn ?',
                  async onOk() {
                    await checkAppointment({
                      appointmentIds: [appointmentId]
                    });
                    loadAppointments({
                      variables
                    });
                  },
                  onCancel() {
                    //console.log('Cancel');
                  }
                });
              } else {
                /*Modal.confirm({
                  icon: <ExclamationCircleOutlined />,
                  title: 'Bạn có chắc chắn muốn bỏ check lịch đã chọn ?',
                  async onOk() {
                    await checkAppointment({
                      appointmentIds: [appointmentId]
                    });
                    loadAppointments({
                      variables
                    });
                  },
                  onCancel() {
                    //console.log('Cancel');
                  }
                });*/
              }
            }}
          ></Checkbox>
        );
      },
      width: 80
    },
    {
      title: 'Thời gian lấy mẫu',
      dataIndex: 'LM',
      render: (_, { sampleTakenDate, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {sampleTakenDate}
          </span>
        ) : (
          sampleTakenDate
        ),
      width: 100
    },
    {
      title: 'Đơn vị',
      dataIndex: 'LM',
      render: (_, { parentFacilityName, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue'
            }}
          >
            {' '}
            {parentFacilityName}
          </span>
        ) : (
          parentFacilityName
        ),
      width: 100
    },
    {
      title: 'Văn phòng',
      dataIndex: 'LM',
      render: (_, { facilityName, classify }: AppointmentStaff) =>
        classify == 'COVID_24' ? (
          <span
            style={{
              color: 'blue '
            }}
          >
            {' '}
            {facilityName}
          </span>
        ) : (
          facilityName
        ),
      width: 100
    }
  ];

  useEffect(() => {
    if (pagingAppointmentStaff.data) {
      var listCD: any = [];
      var listDC: any = [];
      var listGC: any = [];
      var listGH: any = [];
      var listCB: any = [];
      var listNH: any = [];
      var listAppointmentHisId: any = [];
      var list = pagingAppointmentStaff.data;

      list?.map(item => {
        listCD = addItemToList(item, listCD, 'routeName');
        listDC = addItemToList(item, listDC, 'appointmentAddress');
        listGC = addItemToList(item, listGC, 'appointmentNote');
        listGH = addItemToList(item, listGH, 'workTimeName');
        listCB = addItemToList(item, listCB, 'siteStaffName');
        listNH = addItemToList(item, listNH, 'appointmentDate');
        listAppointmentHisId = addItemToList(item, listAppointmentHisId, 'appointmentHisId');
      });
      setListFCD(listCD);
      setListFDC(listDC);
      setListFGC(listGC);
      setListFGH(listGH);
      setListFCB(listCB);
      setListFNH(listNH);
      setListAppointmentHisId(listAppointmentHisId);
    }
  }, [pagingAppointmentStaff]);

  const addItemToList = (item, list, fillName) => {
    var itemCD = {
      text: item[fillName],
      value: item[fillName]
    };
    var checkItem = list.filter(itemT => {
      return itemT.value == item[fillName];
    });
    if (!checkItem || checkItem.length == 0) {
      list.push(itemCD);
    }
    return list;
  };

  return (
    <Table
      rowKey={row => row.appointmentId}
      rowSelection={rowSelection}
      onChange={handleTableChange}
      loading={isLoadingAppointmentStaff}
      pagination={{
        current: variables.page,
        pageSize: variables.pageSize,
        total: pagingAppointmentStaff['records'],
        defaultPageSize: 500,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '50', '100', '500']
      }}
      bordered
      dataSource={pagingAppointmentStaff?.data}
      scroll={{ x: 1300, y: 'calc(115vh - 250px)' }}
      columns={columns}
      className="fs-10"
      title={() => (
        <div>
          <div style={{ float: 'left' }}>
            <label className="lblTableTxt fs-10">Danh sách lịch hẹn </label>
            <label className="lblTableValue">({pagingAppointmentStaff['records']})</label>
          </div>
          <div className="fs-10" style={{ float: 'right' }}>
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_HUY_PL_COVID] ? false : true}
              danger
              size="small"
              style={{
                marginRight: 5
              }}
              title="Hủy phân lịch"
              icon={<DeleteOutlined />}
              onClick={() => {
                if (selectedRowKeys.length === 0) {
                  openNotificationRight('Vui lòng chọn lịch hẹn trước khi chọn cán bộ tại nhà.', 'warning');
                  return;
                } else {
                  if (selectedRowKeys.length !== 0) {
                    Modal.confirm({
                      icon: <ExclamationCircleOutlined />,
                      title: 'Bạn có chắc chắn muốn huỷ phân các lịch đã chọn cho cán bộ tại nhà ?',
                      async onOk() {
                        console.log(selectedRowKeys);
                        removeAssign(selectedRowKeys);
                        // await assignAppointmentNew({
                        //   appointmentIds: listSl,
                        //   staffId: staffId,
                        //   appointmentDate: appointmentDate
                        // });
                      },
                      onCancel() {
                        //console.log('Cancel');
                      }
                    });
                  }
                }
              }}
            >
            </Button>
            &nbsp;&nbsp;<span style={{ color: 'blue', fontSize: '17px' }}>||</span>&nbsp;&nbsp;
            <Button
              // disabled={functionOb[functionCodeConstants.TD_LH_HUY_PL_COVID] ? false : true}
              className="green"
              size="small"
              style={{
                marginRight: 5
              }}
              title="Copy"
              icon={<CopyrightOutlined />}
              onClick={() => {
                let text = "";
                if(listU && listU.length > 0){
                  listU?.map(record=>{
                    var txtCopy = '';
                    txtCopy+=record?.workTimeName ? record?.workTimeName +"      " : "      ";
                    txtCopy+=record?.routeName ? record?.routeName  +"     " : "     ";
                    txtCopy+=record?.appointmentAddress ? record?.appointmentAddress +"    " : "    " ;
                    txtCopy+=record?.appointmentNote ? record?.appointmentNote +"      " : "      ";
                    txtCopy+=record?.siteStaffName ? record?.siteStaffName +"    " : "";
                    txtCopy+=record?.siteStaffPhone ? record?.siteStaffPhone +"    " : "";
                    txtCopy+=record?.patientName ? record?.patientName +"    ":"";
                    txtCopy+=record?.patientPhone ? record?.patientPhone +"    " : "";
                    txtCopy+=record?.appointmentHisId ? record?.appointmentHisId +"     ":"";
                    txtCopy+=record?.appointmentDate ? record?.appointmentDate +"     ":"";
                    txtCopy+=record?.isCheck===1 ? ("Đã check" +"    ") : ("Chưa check" +"      ");
                    txtCopy+=record?.sampleTakenDate ? record?.sampleTakenDate +"     ":"";
                    txtCopy+=record?.parentFacilityName ? record?.parentFacilityName +"     ":"";
                    txtCopy+=record?.facilityName? record?.facilityName +"     ":"";
                    text+=txtCopy+"\n";
                    text+="\n";
                  })
                }
                navigator.clipboard.writeText(text);
              }}
            >
            </Button>
            &nbsp;&nbsp;<span style={{ color: 'blue', fontSize: '17px' }}>||</span>&nbsp;&nbsp;
            <Select
              loading={isLoadingSiteStaffs}
              style={{ width: 200 }}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              // className={'slcSearch'}
              onClick={onClickSelectBox}
              // onChange={onChangeStaff}
              onSelect={onChangeStaff}
              defaultValue=""
              value={staf}
            >
              <Select.Option key="" value="">
                -- Chọn cán bộ tại nhà --
              </Select.Option>
              {siteStaffs?.map(siteStaff => (
                <Select.Option key={siteStaff.id} value={siteStaff.id + '***' + siteStaff.name}>
                  {siteStaff.name}
                </Select.Option>
              ))}
            </Select>
            <span> </span>
            <Button className="fix-icon fs-10" type="primary" shape="circle" icon={<RedoOutlined />} />
          </div>
          <div style={{ clear: 'both' }}></div>
        </div>
      )}
      onRow={(record, rowIndex) => {
        return {
          onDoubleClick: () => {
            //setAppointmentDetail(record);
            if (record.classify != 'COVID_24') {
              onModify(record.appointmentId);
            } else {
              navigate(
                '/appointment-management/appointment-covid/update/' + record.groupId + '/' + record.appointmentId
              );
            }
          },
          onClick: event => {
            handleChangeStaff('');
            var list: any[] = [];
            var listSlT: any[] = [];
            handleChoseAssign(record.parentFacilityId, record.facilityId, record.appointmentDate);
            list.push(record.appointmentId);
            setSelectedRowKeys(list?.map(item => item));
            var item = {
              appointmentId: record.appointmentId,
              workTimeId: record.workTimeId,
              routeId: record.routeId
            };
            listSlT.push(item);
            setListSl([...listSlT]);
            setListU([record]);
          }
        };
      }}
    />
  );
};

export default AppointmentTable;
