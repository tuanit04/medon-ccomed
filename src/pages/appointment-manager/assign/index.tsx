import { Col, Row } from 'antd';
import { useGetFacilityHome, useGetFacilityParents, useGetFacilitys } from 'hooks/facility';
import { useGetSiteStaffs } from 'hooks/site-staff';
import React, { FC, useEffect, useState } from 'react';
import AppointmentOfStaffTableById from './appointmentOfStaffByIdTable';
import AppointmentTable from './appointmentTable';
import AssignmentSearchForm from './assignmentSearchForm';
import AppointmentOfAllStaffTable from './appointmentOfAllStaffTable';
import './index.less';
import AllAppointmentOfAllStaffGroupByTimeTable from './AllAppointmentOfAllStaffGroupByTimeTable';
import moment from 'moment';
import { useCancleAppointment, useCreateAppointment, useUpdateAppointment } from 'hooks/appointment';
import AppoimentDialogUpdate from './appointmentModifyDialog';
import { AppointmentInput } from 'common/interfaces/input/appointmentInput.interfce';
import { useGetAppointmentDetail } from 'hooks/appointment/useGetAppointmentDetail';
import { AppointmentPatient } from 'common/interfaces/appointmentPatient.interface';
import { useAppState } from 'helpers';
import { useGetChannels } from 'hooks/channel';
import { useForwardAppointment } from 'hooks/appointment/useForwardAppointmentToDoctorConsultant';
interface AssignmentPageProps {
  isAssignAppointment?: boolean;
}

const AssignmentPage: FC<AssignmentPageProps> = isAssignAppointment => {
  const functionOb = useAppState(state => state.user.functionObject);
  const [parentFacilityId, setParentFacilityId] = useState('');
  const [facilityId, setFacilityId] = useState('');
  const [status, setStatus] = useState('');
  const [staffId, setStaffId] = useState('');
  const [appointmentDate, setAppointmentDate] = useState(moment(new Date()).format('DD/MM/YYYY'));
  const facilityParentList: any[] = useGetFacilityHome();
  const [pFacilityIdStaff, setPFacilityIdStaff] = useState('');
  const [facilityIdStaff, setFacilityIdStaff] = useState('');
  const [appointmentDateStaff, setAppointmentDateStaff] = useState('');
  const [reloadAppointmentOfStaffTableById, setReloadAppointmentOfStaffTableById] = useState(0);
  const [modifyVisible, setModifyVisible] = useState(false);
  const [appointmentIdRes, setAppointmentIdRes] = useState('');
  const [bookingMultipleVisible, setBookingMultipleVisible] = useState(false);
  const { loadAppointmentDetail, isLoadingAppointment, appointmentDetail } = useGetAppointmentDetail();
  const [appointmentPatientSelected, setAppointmentPatientSelected] = useState({} as AppointmentPatient);
  const [appointmentIdSelected, setAppointmentIdSelected] = useState('');
  const [appointmentPatient, setAppointmentPatient]: any = useState();
  const [isRun, setIRun] = useState(false);
  const [rowAppointmentStaffSelected, setRowAppointmentStaffSelected] = useState(-1);
  //Sinh số ngẫu nhiên để refetch data
  const [randomNumberValue, setRandomNumberValue] = useState(0);
  //Lấy nguồn lịch hẹn
  const { channels, loadChannels } = useGetChannels();
  const [assignStaffName, setAssignStaffName] = useState('');
  const [patientName,setPatientName] = useState('');
  const [patientPhone,setPatientPhone]= useState('');
  const [siteStaffName,setSiteStaffName]= useState('');
  const [siteStaffPhone,setSiteStaffPhone]=useState('');

  useEffect(() => {
    if (facilityParentList) {
      var defaultF = facilityParentList.filter(item => {
        return item.code == 'KTN';
      });
      if (defaultF && defaultF.length > 0) {
        setParentFacilityId(defaultF[0].id);
      }
      setIRun(true);
    }
  }, [facilityParentList]);
  const onSetIdRes = id => {
    setAppointmentIdRes(id);
  };
  const onSeModifyVisible = (isShow: boolean) => {
    setModifyVisible(isShow);
  };
  //Xử lý Cập nhật Lịch hẹn
  const {
    updateAppointment,
    resultUpdateAppointment,
    isLoadingUpdateAppointment,
    errorUpdateAppointment
  } = useUpdateAppointment(onSetIdRes, onSeModifyVisible);
  //lắng nghe Id trả về
  const onSetorwardAppointmentIdRes = async id => {
    setAppointmentIdRes(id);
  };
  //Hủy lịch hẹn
  const {
    cancleAppointment,
    isLoadingCancleAppointment,
    resultCancleAppointment,
    errorCancleAppointment
  } = useCancleAppointment(onSetIdRes, onSeModifyVisible);
  const handleCancleAppointment = async id => {
    await cancleAppointment({ id });
  };
  //Chuyển BS Tư vấn
  const {
    forwardAppointment,
    resultForwardAppointment,
    isLoadingForwardAppointment,
    errorForwardAppointment
  } = useForwardAppointment(onSetorwardAppointmentIdRes, onSeModifyVisible);
  const [resultForwardAppointmentState, setResultForwardAppointmentState] = useState();
  const handleForwardAppointment = async (appointmentId: string) => {
    await forwardAppointment({ id: appointmentId });
    //refetchAppointments?.();
  };
  return (
    <div className="main-content">
      <div>
        <AssignmentSearchForm
          facilityParentList={facilityParentList}
          defaultPFacility={parentFacilityId}
          onSearch={value => {
            setRandomNumberValue(Math.random());
            setParentFacilityId(value['parentFacilityId']);
            setFacilityId(value['facilityId']);
            setAppointmentDate(value['appointmentDate']);
            setStatus(value['status']);
            setPatientName(value['patientName']);
            setPatientPhone(value['patientPhone']);
            setSiteStaffName(value['siteStaffName']);
            setSiteStaffPhone(value['siteStaffPhone']);
          }}
        />
      </div>
      <div>
        <div className="tbl-left">
          <AppointmentTable
            setRowSelected={(value = -1) => {
              setRowAppointmentStaffSelected(value);
            }}
            randomNumberValue={randomNumberValue}
            parentFacilityId={parentFacilityId}
            facilityId={facilityId}
            appointmentDate={appointmentDate}
            status={status}
            patientName={patientName}
            patientPhone={patientPhone}
            siteStaffName={siteStaffName}
            siteStaffPhone={siteStaffPhone}
            reload={reloadAppointmentOfStaffTableById}
            onModify={(id: string) => {
              loadAppointmentDetail({ variables: { id: id } });
              setAppointmentIdSelected(id);
              setModifyVisible(true);
            }}
            handleChangeStaff={staffId => {
              setStaffId(staffId);
            }}
            handleChoseAssign={(pFacilityIdStaff, facilityIdStaff, appointmentDateStaff) => {
              setPFacilityIdStaff(pFacilityIdStaff);
              setFacilityIdStaff(facilityIdStaff);
              setAppointmentDateStaff(appointmentDateStaff);
            }}
            handleReloadAppointmentOfStaffTableById={reload => {
              setReloadAppointmentOfStaffTableById(reload);
            }}
            isRun={isRun}
            // handleChangeSiteStaff={() =>{
            //   setStaffId("");
            // }}
          />
        </div>
        <div className="tbl-right">
          <AppointmentOfAllStaffTable
            rowAppointmentStaffSelected={rowAppointmentStaffSelected}
            randomNumberValue={randomNumberValue}
            pFacilityIdStaff={pFacilityIdStaff}
            appointmentDateStaff={appointmentDateStaff}
            facilityIdStaff={facilityIdStaff}
            reload={reloadAppointmentOfStaffTableById}
            onSelectSiteStaff={siteStaffId => {
              setStaffId(siteStaffId);
            }}
          />
          <AppointmentOfStaffTableById
            rowAppointmentStaffSelected={rowAppointmentStaffSelected}
            appointmentDate={appointmentDate}
            staffId={staffId}
            reload={reloadAppointmentOfStaffTableById}
          />
          {/* <AllAppointmentOfAllStaffGroupByTimeTable
            reload={reloadAppointmentOfStaffTableById}
            parentFacilityId={pFacilityIdStaff}
            appointmentDate={appointmentDate}
            facilityId={facilityIdStaff}
          /> */}
          <AppoimentDialogUpdate
            isLoadingChannels={false}
            rowObj={appointmentPatient}
            functionOb={functionOb}
            onForwardAppointment={(appointmentId: any) => handleForwardAppointment(appointmentId)}
            onCancelAppointment={(appointmentId: any) => handleCancleAppointment(appointmentId)}
            channels={channels}
            visible={modifyVisible}
            assignStaffName={assignStaffName}
            onModify={values => {
              const appointmentInput: Partial<AppointmentInput> = {
                ...values
              };
              updateAppointment({ data: appointmentInput });
              //setModifyVisible(false);
            }}
            onBookingMultipleAppointments={appointmentPatient => {
              setAppointmentPatientSelected(appointmentPatient);
              setBookingMultipleVisible(true);
            }}
            appointment={appointmentDetail}
            onCancel={() => setModifyVisible(false)}
            isLoading={isLoadingUpdateAppointment || isLoadingAppointment}
          />
        </div>
      </div>
      <div>
        <AllAppointmentOfAllStaffGroupByTimeTable
          reload={reloadAppointmentOfStaffTableById}
          rowAppointmentStaffSelected={rowAppointmentStaffSelected}
          parentFacilityId={pFacilityIdStaff}
          appointmentDate={appointmentDate}
          facilityId={facilityIdStaff}
        />
      </div>
    </div>
  );
};
export default AssignmentPage;
