import React, { FC, useState, useEffect } from 'react';
import { Button, Table, Tag, Modal, Radio, Divider, Select, Checkbox } from 'antd';
import { Role, RoleStatus } from 'interface/permission/role.interface';
import { ExclamationCircleOutlined, RedoOutlined } from '@ant-design/icons';
import { useCheckAppointment, useGetAppointments } from 'hooks/appointment';
import { useGetSiteStaffs, useGetSiteStaffsLazy } from 'hooks/site-staff';
import { useGetAppointmentStaff } from 'hooks/appointment/useGetAppointmentStaff';
import { AppointmentStaff } from 'common/interfaces/appointmentStaff.interface';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { useAssignAppointment } from 'hooks/appointment/useAssignAppointment';
import { useAssignAppointments } from 'hooks/appointment/useAssignAppointments';
import { openNotificationRight } from 'utils/notification';

interface AppointmentTableProps {
  parentFacilityId: string;
  facilityId: string;
  appointmentId: any;
  appointmentDate: string;
  handleChangeStaff: (staffId: string) => void;
}

const AppointmentTable: FC<AppointmentTableProps> = ({
  parentFacilityId,
  facilityId,
  appointmentDate,
  appointmentId,
  handleChangeStaff
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(1);
  const [pagination, setPagination] = useState({});
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  let filtered: FilteredInput[] = [];
  filtered.push({
    id: 'appointmentId',
    value: appointmentId,
    operation: '=='
  });
  //Hook Lấy danh sách cán bộ TN
  const { siteStaffs, isLoadingSiteStaffs, loadSiteStaffs } = useGetSiteStaffsLazy();
  const variables = { page, pageSize, filtered };
  //Hook lấy danh sách Lịch hẹn khám
  const {
    pagingAppointmentStaff,
    isLoadingAppointmentStaff,
    errorAppointmentStaff,
    refetchAppointmentStaff
  } = useGetAppointmentStaff(variables);
  //Hook Check lịch
  const {
    checkAppointment,
    isLoadingCheckAppointment,
    errorCheckAppointment,
    resultCheckAppointment
  } = useCheckAppointment();
  useEffect(() => {
    //Load danh sách CBTN
    loadSiteStaffs();
  }, [parentFacilityId, appointmentDate, facilityId]);
  useEffect(() => {
    setPagination({ current: page, pageSize: pageSize, total: pagingAppointmentStaff['records'] });
  }, [pagingAppointmentStaff]);
  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    setPage(pagination['current']);
    setPageSize(pagination['pageSize']);
  };
  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  };
  //Phân lịch lẻ
  const {
    assignAppointment,
    isLoadingAssignAppointment,
    errorAssignAppointment,
    resultAssignAppointment
  } = useAssignAppointment();
  //Phân lịch nhiều
  const {
    assignAppointments,
    isLoadingAssignAppointments,
    errorAssignAppointments,
    resultAssignAppointments
  } = useAssignAppointments();
  const onChangeStaff = async (staffInfo: string) => {
    let staffId = staffInfo.split('***')['0'];
    let staffName = staffInfo.split('***')['1'];
    handleChangeStaff(staffId);
    if (selectedRowKeys.length !== 0) {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn phân các lịch đã chọn cho cán bộ tại nhà ' + staffName + ' ?',
        async onOk() {
          if (selectedRowKeys.length === 1) {
            await assignAppointment({
              appointmentId: selectedRowKeys[0],
              staffId: staffId
            });
            refetchAppointmentStaff();
          } else if (selectedRowKeys.length > 1) {
            await assignAppointments({
              appointmentIds: selectedRowKeys,
              staffId: staffId
            });
            refetchAppointmentStaff();
          }
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    }
  };
  const onClickSelectBox = e => {
    if (selectedRowKeys.length === 0) {
      openNotificationRight('Vui lòng chọn lịch hẹn trước khi chọn cán bộ tại nhà.', 'warning');
      return;
    }
  };

  return (
    <Table
      rowKey={row => row.appointmentId}
      rowSelection={rowSelection}
      loading={isLoadingAppointmentStaff}
      onChange={handleTableChange}
      bordered
      pagination={{
        current: variables.page,
        pageSize: variables.pageSize,
        total: pagingAppointmentStaff['records'],
        defaultPageSize: 20,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '50']
      }}
      dataSource={pagingAppointmentStaff?.data}
      scroll={{ x: 500 }}
      title={() => (
        <>
          <div style={{ float: 'left' }}>
            <label className="lblTableTxt">Danh sách lịch hẹn </label>
            <label className="lblTableValue">({pagingAppointmentStaff['records']})</label>
            <Divider />
          </div>
          <div style={{ float: 'right' }}>
            <Select
              style={{ width: 200 }}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              className={'slcSearch'}
              onClick={onClickSelectBox}
              onChange={onChangeStaff}
              defaultValue=""
            >
              <Select.Option key="" value="">
                -- Chọn cán bộ tại nhà --
              </Select.Option>
              {siteStaffs?.map(siteStaff => (
                <Select.Option key={siteStaff.id} value={siteStaff.id + '***' + siteStaff.name}>
                  {siteStaff.name}
                </Select.Option>
              ))}
            </Select>
            <span> </span>
            <Button type="primary" shape="circle" icon={<RedoOutlined />} />
          </div>
          <div style={{ clear: 'both' }}></div>
        </>
      )}
    >
      <Table.Column<AppointmentStaff> title="Mã lịch" align="center" render={(_, { appointmentId }) => appointmentId} />
      <Table.Column<AppointmentStaff> title="Cung đường" render={(_, { routeName }) => routeName} />
      <Table.Column<AppointmentStaff> title="Địa chỉ" render={(_, { appointmentAddress }) => appointmentAddress} />
      <Table.Column<AppointmentStaff> title="Ghi chú" render={(_, { appointmentNote }) => appointmentNote} />
      <Table.Column<AppointmentStaff> title="Giờ hẹn" align="center" render={(_, { workTimeName }) => workTimeName} />
      <Table.Column<AppointmentStaff> title="Cán bộ TN" render={(_, { siteStaffName }) => siteStaffName} />
      <Table.Column<AppointmentStaff>
        align="center"
        title="Check lịch"
        render={(_, { isCheck, appointmentId }) => {
          let check: boolean = isCheck === null || isCheck === undefined ? false : isCheck === 1 ? true : false;
          return (
            <Checkbox
              checked={check}
              onChange={e => {
                if (e.target.checked) {
                  Modal.confirm({
                    icon: <ExclamationCircleOutlined />,
                    title: 'Bạn có chắc chắn muốn check lịch đã chọn ?',
                    async onOk() {
                      await checkAppointment({
                        appointmentIds: [appointmentId]
                      });
                      refetchAppointmentStaff();
                    },
                    onCancel() {
                      //console.log('Cancel');
                    }
                  });
                } else {
                  Modal.confirm({
                    icon: <ExclamationCircleOutlined />,
                    title: 'Bạn có chắc chắn muốn bỏ check lịch đã chọn ?',
                    async onOk() {
                      await checkAppointment({
                        appointmentIds: [appointmentId]
                      });
                      refetchAppointmentStaff();
                    },
                    onCancel() {
                      //console.log('Cancel');
                    }
                  });
                }
              }}
            ></Checkbox>
          );
        }}
      />
      <Table.Column<AppointmentStaff> title="Thời gian lấy mẫu" render={(_, { sampleTakenDate }) => sampleTakenDate} />
    </Table>
  );
};

export default AppointmentTable;
