import React, { FC, useState } from 'react';
import { Role } from 'interface/permission/role.interface';
import useGetSearchForm from './useGetSearchForm';
import { Button, PageHeader } from 'antd';
import { useLocale } from 'locales';
import { SearchOutlined, ClearOutlined, DownOutlined, UpOutlined } from '@ant-design/icons';
import { Facility } from 'common/interfaces/facility.interface';
import { useGetFacilitys } from 'hooks/facility';
import moment from 'moment';

export interface Values extends Role {}

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};
interface AssignmentFormSearchProps {
  facilityParentList: Facility[];
  onSearch: ({}) => void;
}

const AssignmentFormSearch: FC<AssignmentFormSearchProps> = ({ facilityParentList, onSearch }) => {
  const [parentFacilityId, setParentFacilityId] = useState('');
  const [facilityId, setFacilityId] = useState('');
  const [status, setStatus] = useState('');
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  const { Form, form, AppointmentDate, FacilityParent, Status, Facility, Buttons } = useGetSearchForm({
    name: 'searchForm',
    responsive: true
  });
  return (
    <Form>
      <AppointmentDate />
      <Status />
      <FacilityParent
        facilityParentList={facilityParentList}
        handleChangeFacilityParent={(parentId: any) => {
          //setParentFacilityId(parentId);
          loadFacilities({
            variables: {
              page: 1,
              filtered: [
                {
                  id: 'parentId',
                  value: parentId,
                  operation: '=='
                }
              ]
            }
          });
        }}
      />
      <Facility handleChangeFacility={data => {}} facilityList={parentFacilityId === '' ? [] : facilities} />
      <Buttons
        onSearch={() => {
          let appointmentDate = moment(form.getFieldValue('appointmentDate')).format('DD/MM/YYYY');
          let status = form.getFieldValue('status');
          let parentFacilityId = form.getFieldValue('parentFacilityId');
          let facilityId = form.getFieldValue('facilityId');
          onSearch({ appointmentDate, status, parentFacilityId, facilityId });
        }}
        resetFields={() => {
          form.resetFields();
          onSearch({ name: '', code: '', parentFacilityId: '', facilityId: '', status: '' });
        }}
      />
    </Form>
  );
};

export default AssignmentFormSearch;
