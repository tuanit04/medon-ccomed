import React, { FC } from 'react';
import { Form, Input, Col, Row, Select, Radio, DatePicker, Space, Button } from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { Role } from 'interface/permission/role.interface';
import { Facility } from 'common/interfaces/facility.interface';
import moment from 'moment';
import { ClearOutlined, SearchOutlined } from '@ant-design/icons';
import { useAppState } from 'helpers';
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 8,
  xl: 8,
  xxl: 8
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 }
};

interface AssignmentFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  /** Initial form data */
  values?: Role;
}

export default function useGetSearchForm({
  required = false,
  responsive = false,
  name = 'form',
  values
}: AssignmentFormProps) {
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<FormProps>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={values}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Ngay hen
  const AppointmentDate: FC = () => {
    const dateFormat = 'DD/MM/YYYY';
    function handleChange(value: any) {
      console.log(`selected ${value}`);
    }
    const appointmentDate = (
      <Form.Item
        name="appointmentDate"
        label="Ngày hẹn"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <DatePicker defaultValue={moment()} format={dateFormat} onChange={handleChange} placeholder="Chọn ngày hẹn" style={{width: '90%'}} className="inputSearch" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentDate}</Col> : appointmentDate;
  };
  //Don vi
  interface FacilityParentProps {
    facilityParentList: Facility[];
    handleChangeFacilityParent: (parentId: any) => void;
  }

  const FacilityParent: FC<FacilityParentProps> = ({ facilityParentList, handleChangeFacilityParent }) => {
    const facilityParent = (
      <Form.Item
        name="parentFacilityId"
        label="Đơn vị"
        rules={[
          {
            required: false,
            message: 'Đơn vị không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          className='slcSearch'
          onChange={handleChangeFacilityParent}
          defaultValue=""
        >
          <Select.Option key="" value="">
            -- Tất cả --
          </Select.Option>
          {facilityParentList?.map(option => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? (
      <Col {...wrapperCol}>{facilityParent}</Col>
    ) : (
      facilityParent
    );
  };

  //Van phong
  interface FacilityProps {
    facilityList: Facility[];
    handleChangeFacility: (id: any) => void;
  }
  const Facility: FC<FacilityProps> = ({ facilityList, handleChangeFacility }) => {
    const facility = (
      <Form.Item
        name="facilityId"
        label="Văn phòng"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeFacility}
          className='slcSearch'
          defaultValue=""
        >
          <Select.Option key="-1" value="">
            -- Tất cả --
          </Select.Option>
          {facilityList?.map(option => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{facility}</Col> : facility;
  };

  //Trang thai
  const Status: FC = () => {
    function handleChange(value: any) {
      console.log(`selected ${value}`);
    }
    const status = (
      <Form.Item
        name="status"
        label="Trạng thái"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select className='slcSearch' defaultValue="" placeholder="--Tất cả--">
          <Select.Option key="1" value="1">
              Chưa phân
            </Select.Option>
            <Select.Option key="2" value={1}>
              Đã phân
            </Select.Option>
            <Select.Option key="3" value={2}>
              Lịch CheckTime
            </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{status}</Col> : status;
  };
  //Buttons
  interface ButtonsProps {
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          onClick={() => {
            onSearch();
          }}
          icon={<SearchOutlined />}
          type="primary"
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button onClick={resetFields} icon={<ClearOutlined />}>
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };
  return {
    form: formInstance,
    Form: WrappedForm,
    AppointmentDate,
    FacilityParent,
    Status,
    Facility,
    Buttons
  };
}
