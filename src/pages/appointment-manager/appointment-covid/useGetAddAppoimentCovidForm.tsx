import { PhoneOutlined, SearchOutlined } from '@ant-design/icons';
import {
  Checkbox,
  Col,
  ColProps,
  DatePicker,
  Form,
  FormProps,
  Input,
  Modal,
  Row,
  Select,
  Image,
  Button,
  Radio
} from 'antd';
import { SizeType } from 'antd/es/config-provider/SizeContext';
import { Appointment } from 'common/interfaces/appointment.interface';
import { AppointmentCovid } from 'common/interfaces/appointmentCovide.interface';
import { Country } from 'common/interfaces/country.interface';
import { District } from 'common/interfaces/district.interface';
import { Patient } from 'common/interfaces/patient.interface';
import { Province } from 'common/interfaces/province.interface';
import { Street } from 'common/interfaces/street.interface';
import { APPOINTMENT_STATUS } from 'constants/appointment.constants';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import moment from 'moment';
import AppointmentPhoneSearch from 'pages/common/appointmentPhoneSearch';

import React, { useEffect, useState } from 'react';
import { FC } from 'react';
import { formatDateTyping } from 'utils/datetime';
import { csCallout } from 'utils/vcc/actions';

const { Option } = Select;
const { TextArea } = Input;
const CheckboxGroup = Checkbox.Group;
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
  xxl: 12
};

const wrapperColAllCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const wrapperColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 20,
  xl: 20,
  xxl: 20
};

const wrapperColOneItemNew: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 19,
  xl: 19,
  xxl: 19
};

const labelColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 4,
  xl: 4,
  xxl: 4
};

const labelColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperSDTInput: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 9,
  xl: 9,
  xxl: 9
};

const wrapperSDT: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 16,
  xl: 16,
  xxl: 16
};

const wrapperColFourItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wapperColPhoneInput: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 18,
  xl: 18,
  xxl: 18
};

const wapperColPhoneButton: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 6,
  xl: 6,
  xxl: 6
};

interface AddScheduleFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?: boolean;
  /** Initial form data */
  appointmentInput?: Partial<AppointmentCovid>;
  isCreate?: boolean;
  isNewDialog?: boolean;
  visible?: boolean;
  formInstance;
  patternIdNo;
}

export default function useGetAddAppointmentCovidForm({
  required = false,
  responsive = false,
  name = 'form',
  appointmentInput,
  formInstance,
  patternIdNo
}: AddScheduleFormProps) {
  const [first, setFirst] = useState(false);

  // useEffect(() => {
  //     setPatternIdNo(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
  // }, [visible])
  //Hook Lấy danh sách Quận/Huyện
  const { districts, loadDistricts, isLoadingDistricts } = useGetDistricts();
  const [districCode, setDistricCode]: any = useState();
  const [districId, setDistricId]: any = useState();
  const [routeId, setRouteId] = useState('');
  const [appointmentDate, setAppointmentDate] = useState('');
  const [index, setIndex] = useState('');
  const [size, setSize] = useState<SizeType>('large');

  useEffect(() => {
    if ((index || Number(index) == 0) && districts && districts.length > 0 && districId) {
      var code = '';
      districts.map(item => {
        if (item.id == districId) {
          code = item.code;
        }
      });
      if (code) {
        var listItem = formInstance.getFieldValue('patients');
        if (listItem && listItem.length > 0 && (index || Number(index) == 0)) {
          var item = listItem[index];
          if (item) {
            item.districtCode = code;
            formInstance.setFieldsValue({
              patients: listItem
            });
          }
        }
      }
    }
  }, [districts, districId, index]);

  useEffect(() => {
    if ((index || Number(index) == 0) && districts && districts.length > 0 && districCode) {
      var id = '';
      districts.map(item => {
        if (item.code == districCode) {
          id = item.id;
        }
      });
      console.log(id);
      if (id) {
        var listItem = formInstance.getFieldValue('patients');
        if (listItem && listItem.length > 0 && (index || Number(index) == 0)) {
          var item = listItem[index];
          item.districtId = id;
          formInstance.setFieldsValue({
            patients: listItem
          });
        }
      }
    }
  }, [districts, districCode, index]);

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const onValuesChange = (changedValues: any, values: AppointmentCovid) => {};

    return (
      <Form
        {...props}
        // {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={{ ...appointmentInput, appointmentDate: moment() }}
        onValuesChange={onValuesChange}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Ma lich hen
  interface AppointmentCodeProps {
    field: any;
  }
  const AppointmentCode: FC<AppointmentCodeProps> = ({ field }) => {
    const appointmentCode = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Mã lịch hẹn:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item {...field} name={[field?.name, 'appointmentHisId']}>
            <Input disabled={true} placeholder="" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentCode}</Col> : appointmentCode;
  };

  //Trang thai lich
  interface AppointmentStatusProps {
    field: any;
  }
  const AppointmentStatus: FC<AppointmentStatusProps> = ({ field }) => {
    const appointmentStatus = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Trạng thái lịch:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field.name, 'status']}
            // rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Select defaultValue={APPOINTMENT_STATUS.DA_XAC_NHAN.key} disabled placeholder="-- Chọn trạng thái --">
              <Option value={APPOINTMENT_STATUS.CHO_XAC_NHAN.key}>{APPOINTMENT_STATUS.CHO_XAC_NHAN.value}</Option>
              <Option value={APPOINTMENT_STATUS.DA_XAC_NHAN.key}>{APPOINTMENT_STATUS.DA_XAC_NHAN.value}</Option>
              <Option value={APPOINTMENT_STATUS.CHO_TU_VAN_DAU_VAO.key}>
                {APPOINTMENT_STATUS.CHO_TU_VAN_DAU_VAO.value}
              </Option>
              <Option value={APPOINTMENT_STATUS.DA_TU_VAN_DAU_VAO.key}>Đã phân lịch</Option>
              <Option value={APPOINTMENT_STATUS.DA_CHECK.key}>{APPOINTMENT_STATUS.DA_CHECK.value}</Option>
              <Option value={APPOINTMENT_STATUS.DA_LAY_MAU.key}>{APPOINTMENT_STATUS.DA_LAY_MAU.value}</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentStatus}</Col> : appointmentStatus;
  };

  //Ngày lấy mẫu
  interface AppointmentDateProps {
    onChange?: (value: any, dateString: any) => void;
    handleChangAppointmentDate?: (examFacilityId: string, appointmentDate: string) => void;
    setAppointmentDateP?;
    isDisabled?: boolean;
  }
  const AppointmentDate: FC<AppointmentDateProps> = ({
    onChange,
    handleChangAppointmentDate,
    setAppointmentDateP,
    isDisabled
  }) => {
    const disablePastDt = current => {
      const yesterday = moment().subtract(1, 'day');
      return current.isBefore(yesterday);
    };
    // const handleDatePickerChange = (date, dateString: string, id) => {
    //   formInstance.setFieldsValue({
    //     workTimeId:""
    //   })
    //   let dateStringSplit = dateString?.split('/');
    //   dateString = dateStringSplit[2] + '-' + dateStringSplit[1] + '-' + dateStringSplit[0];
    // //   setAppointmentDate(dateString);
    //   setAppointmentDateP(dateString);
    // //   let examFacilityId = formInstance.getFieldValue('examFacilityId');
    //   // console.log("dateString" ,dateString)
    //   if (examFacilityId) {
    //     if (isHospital) {
    //       handleChangAppointmentDate(examFacilityId, dateString);
    //     } else {
    //       handleChangAppointmentDate(examFacilityId, dateString);
    //     }
    //   }
    // };
    const appointmentDate = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Ngày lấy mẫu:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required
            name="appointmentDate"
            rules={[{ required: true, message: 'Ngày đặt không được để trống.' }]}
          >
            <DatePicker
              disabled={isDisabled}
              onChange={(date, dateString) => {
                //   handleDatePickerChange(date, dateString,1);
                setAppointmentDateP(date);
              }}
              disabledDate={disablePastDt}
              format="DD/MM/YYYY"
              placeholder="-- Chọn ngày --"
            />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentDate}</Col> : appointmentDate;
  };

  //Dịch vụ xét nghiệm
  interface PackageIdProps {
    setTabs: any;
    onChange;
    isDisabled?: boolean;
    setTabActive;
  }
  const PackageId: FC<PackageIdProps> = ({ setTabs, onChange, isDisabled, setTabActive }) => {
    const packageId = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Dịch vụ xét nghiệm:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={'covidServiceId'}
            rules={[{ required: true, message: 'Dịch vụ xét nghiệm không được để trống.' }]}

          >
            <Select
              placeholder="-- Chọn dịch vụ --"
              disabled={isDisabled}
              // defaultValue={'895'}
              onChange={value => {
                var listItem = formInstance.getFieldValue('patients');
                var list: any = [];
                if (listItem && listItem.length > 0) {
                  list.push(listItem[0]);
                }
                formInstance.setFieldsValue({
                  patients: list,
                  examType: undefined
                });
                onChange(value);
                setTabs(1);
                setTabActive('TAB0');
              }}
            >
              <Option value="895">Xét nghiệm PCR</Option>
              {/* <Option value="896">Test nhanh</Option> */}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{packageId}</Col> : packageId;
  };

  //Loại xét nghiệm
  interface PackageTypeProps {
    onChange;
    defaultItem;
    packageIdTmp;
    isDisabled?;
    setTabActive;
  }
  const PackageType: FC<PackageTypeProps> = ({ onChange, defaultItem, packageIdTmp, isDisabled, setTabActive }) => {
    const packageType = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Loại xét nghiệm:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={'examType'} rules={[{ required: true, message: 'Loại xét nghiệm không được để trống.' }]}>
            <Select
              disabled={isDisabled}
              placeholder="-- Chọn loại --"
              onChange={value => {
                onChange(value);
                var listItem = formInstance.getFieldValue('patients');
                const list: any = [];
                if (Number(value) < listItem.length) {
                  setTabActive('TAB0');
                }
                for (var i = 0; i < Number(value); i++) {
                  if (listItem[i]) {
                    list.push(listItem[i]);
                  } else {
                    defaultItem.provinceId = formInstance.getFieldValue('samplingProvinceId');
                    list.push(defaultItem);
                  }
                }
                formInstance.setFieldsValue({
                  patients: list
                });
              }}
            >
              {packageIdTmp && <Option value="1">Cá nhân</Option>}

              {packageIdTmp == '895' && (
                <>
                  <Option value="2">Nhóm 2 KH</Option>
                  <Option value="3">Nhóm 3 KH</Option>
                  <Option value="4">Nhóm 4 KH</Option>
                  <Option value="5">Nhóm 5 KH</Option>
                </>
              )}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{packageType}</Col> : packageType;
  };

  //Khung giờ
  interface AppointmentWorkTimeProps {
    appointmentWorkTime: any;
    isLoading: boolean;
    setTxt?: any;
    isEdit?: boolean;
    isDisabled?: boolean;
  }
  const AppointmentWorkTime: FC<AppointmentWorkTimeProps> = ({
    appointmentWorkTime,
    isLoading,
    setTxt,
    isEdit,
    isDisabled
  }) => {
    const isToday = (someDate: Date) => {
      const today = new Date();
      return (
        someDate.getDate() == today.getDate() &&
        someDate.getMonth() == today.getMonth() &&
        someDate.getFullYear() == today.getFullYear()
      );
    };

    const handleChangeWorkTime = (value: string) => {};
    const handleSelectWorkTime = (value, e) => {
      let appointmentDate = formInstance.getFieldValue('appointmentDate');
      if (!appointmentDate) {
        return;
      } else if (isToday(new Date(String(appointmentDate)))) {
        var today = new Date();
        var timeCurrent = today.getHours() + ':' + today.getMinutes() + ':00';
        const workTimeSelectedArr = e['key']?.split('-');
        if (workTimeSelectedArr) {
          const startTime = workTimeSelectedArr[0];
          const endTime = workTimeSelectedArr[1];
          //The 1st January is an arbitrary date, doesn't mean anything.
          if (Date.parse('01/01/2011 ' + timeCurrent) >= Date.parse('01/01/2011 ' + startTime + ':00')) {
            //openNotificationRight('Khung giờ vừa chọn đã trôi qua hoặc đang nằm trong thời gian hiện tại, vui lòng chọn đúng khung giờ.');
            formInstance.setFieldsValue({
              workTimeId: undefined
            });
            return false;
          } else {
            return true;
          }
        }
      }
    };
    let appointmentDate = formInstance.getFieldValue('appointmentDate');
    let appointmentWorkTimeArr: any = [];
    if (appointmentDate && isToday(new Date(String(appointmentDate)))) {
      for (let i = 0; i < appointmentWorkTime?.length; i++) {
        var today = new Date();
        var timeCurrent = today.getHours() + ':' + today.getMinutes() + ':00';
        const startTime = appointmentWorkTime[i]['startTime'];
        const endTime = appointmentWorkTime[i]['endTime'];
        if (Date.parse('01/01/2011 ' + timeCurrent) >= Date.parse('01/01/2011 ' + startTime + ':00')) {
          if (isEdit) {
            appointmentWorkTimeArr.push(appointmentWorkTime[i]);
          }
        } else {
          appointmentWorkTimeArr.push(appointmentWorkTime[i]);
        }
      }
    } else if (appointmentWorkTime) {
      appointmentWorkTimeArr.push(...appointmentWorkTime);
    }

    const appointmentWorkTimeCpn = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Khung giờ:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="workTimeId" rules={[{ required: true, message: 'Khung giờ không hợp lệ.' }]}>
            <Select
              disabled={isDisabled}
              loading={isLoading}
              style={{ width: '100%' }}
              showSearch
              onChange={(value, name) => {
                setTxt(name['children']);
              }}
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn khung giờ --"
            >
              {appointmentWorkTime === undefined
                ? []
                : appointmentWorkTime?.map((option, index) => {
                    if (isEdit) {
                      var today = new Date();
                      var timeCurrent = today.getHours() + ':' + today.getMinutes() + ':00';
                      const startTime = option['startTime'];
                      if (appointmentDate && isToday(new Date(String(appointmentDate)))) {
                        if (Date.parse('01/01/2011 ' + timeCurrent) >= Date.parse('01/01/2011 ' + startTime + ':00')) {
                          return (
                            <Select.Option
                              // disabled={true}
                              key={option['startTime'] + '-' + option['endTime']}
                              value={option['id']}
                            >
                              {option['name']}
                            </Select.Option>
                          );
                        } else {
                          return (
                            <Select.Option key={option['startTime'] + '-' + option['endTime']} value={option['id']}>
                              {option['name']}
                            </Select.Option>
                          );
                        }
                      } else {
                        return (
                          <Select.Option key={option['startTime'] + '-' + option['endTime']} value={option['id']}>
                            {option['name']}
                          </Select.Option>
                        );
                      }
                    } else {
                      return (
                        <Select.Option key={option['startTime'] + '-' + option['endTime']} value={option['id']}>
                          {option['name']}
                        </Select.Option>
                      );
                    }
                  })}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentWorkTimeCpn}</Col> : appointmentWorkTimeCpn;
  };

  //Địa điểm
  interface ProvincesAllProps {
    isLoading: boolean;
    provinceList: Province[];
    handleChange: (provinceId: string) => void;
    isProvince: boolean;
    isDisabled?: boolean;
  }
  const ProvincesAll: FC<ProvincesAllProps> = ({ provinceList, handleChange, isLoading, isProvince, isDisabled }) => {
    const onChange = (value, itemSelete) => {
      console.log(itemSelete.key);
      console.log(value);
      var listItem = formInstance.getFieldValue('patients');
      if (listItem && listItem.length > 0 && itemSelete) {
        for (var i = 0; i < listItem.length; i++) {
          var item = listItem[i];
          item.provinceCode = itemSelete.key;
          item.provinceId = value;
          item.districtId = undefined;
          item.districtCode = '';
          listItem[i] = item;
        }

        formInstance.setFieldsValue({
          patients: listItem
        });
      }
      formInstance.setFieldsValue({
        samplingStreetId: undefined,
        samplingRouteName: undefined,
        examFacilityName: undefined,
        samplingDistrictId: undefined
      });
      handleChange(value);
      setFirst(true);
    };

    const provincesAll = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> {!isProvince ? 'Địa điểm:' : 'Tỉnh thành'}
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            name={'samplingProvinceId'}
            rules={[
              {
                required: true,
                message: 'Tỉnh/TP không được để trống.'
              }
            ]}
          >
            <Select
              loading={isLoading}
              showSearch
              disabled={isProvince || isDisabled}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={(value, itemSelete) => {
                onChange(value, itemSelete);
              }}
              placeholder="-- Chọn Tỉnh/TP --"
            >
              {provinceList?.map(province => (
                <Select.Option key={province.code} value={province.id}>
                  {province.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{provincesAll}</Col> : provincesAll;
  };

  //Tinh/TP
  interface ProvincesProps {
    isLoading: boolean;
    provinceList: Province[];
    handleChangeProvince: (provinceId: string) => void;
    field: any;
  }
  const Provinces: FC<ProvincesProps> = ({ provinceList, handleChangeProvince, isLoading, field }) => {
    const onChange = (value, itemSelete) => {
      var listItem = formInstance.getFieldValue('patients');
      if (listItem && listItem.length > 0 && field && itemSelete) {
        var item = listItem[field.name];
        item.provinceCode = itemSelete.key;
        item.provinceId = value;
        item.districtId = undefined;
        item.districtCode = '';
        listItem[field.name] = item;
        formInstance.setFieldsValue({
          patients: listItem
        });
      }
      // formInstance.setFieldsValue({
      //   patient: {
      //     districtId: undefined,
      //     wardId: undefined
      //   },
      //   streetId: undefined,
      //   routeId: undefined
      // });
      handleChangeProvince(value);
      setFirst(true);
    };

    const provinces = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Tỉnh/TP:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            {...field}
            name={[field?.name, 'provinceId']}
            rules={[
              {
                required: true,
                message: 'Tỉnh/TP không được để trống.'
              }
            ]}
          >
            <Select
              disabled={true}
              loading={isLoading}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={(value, item) => {
                onChange(value, item);
              }}
              placeholder="-- Chọn Tỉnh/TP --"
            >
              {provinceList?.map(province => (
                <Select.Option key={province.code} value={province.id}>
                  {province.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{provinces}</Col> : provinces;
  };
  //Quan/Huyen
  interface DistrictProps {
    districtList: District[];
    isLoading: boolean;
    handleChangeDistrict: (districtId: string) => void;
    field: any;
    isDisabled?: boolean;
  }

  const Districts: FC<DistrictProps> = ({ districtList, handleChangeDistrict, isLoading, field, isDisabled }) => {
    const onChange = (value, itemSelete) => {
      var listItem = formInstance.getFieldValue('patients');
      if (listItem && listItem.length > 0 && field && itemSelete) {
        var item = listItem[field.name];
        item.districtId = value;
        item.districtCode = itemSelete.key;
        listItem[field.name] = item;
        formInstance.setFieldsValue({
          patients: listItem
        });
        handleChangeDistrict(value);
      }
    };
    useEffect(() => {
      if (first) {
        //disRefC.current.focus();
      }
    }, [districtList]);
    const districts = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Quận/Huyện:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            {...field}
            name={[field?.name, 'districtId']}
            rules={[
              {
                required: true,
                message: 'Quận/Huyện không được để trống.'
              }
            ]}
          >
            <Select
              // ref={ref={disRefC}}
              disabled={isDisabled}
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={(value, item) => {
                onChange(value, item);
              }}
              placeholder="-- Chọn Quận/Huyện --"
            >
              {districtList?.map(district => (
                <Select.Option key={district.code} value={district.id}>
                  {district.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{districts}</Col> : districts;
  };

  //So dien thoai
  interface PhoneNumberProps {
    handleChangePatient: (values?: Partial<Patient>) => void;
    handleChangeProvince: (provinceId: string) => void;
    provinces?;
    field: any;
    isDisabled?: boolean;
    isEdit?: boolean;
  }
  const PhoneNumber: FC<PhoneNumberProps> = ({
    handleChangePatient,
    handleChangeProvince,
    provinces,
    field,
    isDisabled,
    isEdit
  }) => {
    const [isCalling, setIsCalling] = useState(false);
    const [isSearching, setIsSearching] = useState(false);
    const onApplyPID = pid => {
      setIsSearching(false);
      formInstance.setFieldsValue({
        patient: {
          pid: pid
        }
      });
    };

    const onApply = (values?: Partial<Patient>) => {
      setIsSearching(false);
      if (values?.provinceId) {
        handleChangeProvince(values?.provinceId);
        provinces.map(item => {
          if (item.id == values.provinceId) {
            values.provinceCode = item.code;
          }
        });
        setIndex(field.name);
        setDistricId(values?.districtId);
        loadDistricts({
          variables: {
            provinceId: values.provinceId
          }
        });
      } else if (values?.provinceCode) {
        var id = '';
        provinces?.map(item => {
          if (item.code == values?.provinceCode) {
            id = item.id;
          }
        });
        handleChangeProvince(id);
        if (id && id != '') {
          values.provinceId = id;
          setIndex(field.name);
          setDistricCode(values?.districtCode);
          loadDistricts({
            variables: {
              provinceId: id
            }
          });
        }
      }
      var listItem = formInstance.getFieldValue('patients');
      if (listItem && listItem.length > 0 && field && values) {
        var item = listItem[field.name];
        if (values.districtId) {
          item.districtId = values.districtId;
        }
        if (values.districtCode) {
          item.districtCode = values.districtCode;
        }

        item.provinceId = values.provinceId;
        item.provinceCode = values.provinceCode;
        item.patientSex = values.sex;
        item.patientIdNo = values.idNo;
        item.address = values.address;
        item.patientName = values.name;
        item.pid = values?.pid;
        item.patientBirthDate = values?.birthDate;
        listItem[field.name] = item;
        formInstance.setFieldsValue({
          patients: listItem,
          samplingProvinceId: values?.provinceId,
          samplingStreetId: undefined,
          samplingRouteName: undefined,
          examFacilityName: undefined,
          samplingDistrictId: undefined
        });
      }

      handleChangePatient(values);
    };

    const phoneNumber = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Số điện thoại:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          {isEdit && (
            <Row>
              <Col {...wapperColPhoneInput}>
                <Form.Item
                  {...field}
                  name={[field.name, 'phone']}
                  required
                  rules={[
                    {
                      required: true,
                      message: 'SĐT không được để trống hoặc không hợp lệ.',
                      pattern: /(84|0[1|2|3|5|6|7|8|9])+([0-9]{8,10})\b/g
                    }
                  ]}
                >
                  <Input
                    disabled={isDisabled}
                    onKeyUp={e => {
                      if (e.key == 'Enter') {
                        setIsSearching(true);
                      }
                    }}
                    placeholder="Nhập số điện thoại"
                  />
                </Form.Item>
              </Col>
              <Col {...wapperColPhoneButton}>
                <Button
                  className="btn-call"
                  type="primary"
                  // shape="circle"
                  icon={<PhoneOutlined />}
                  // size={size}
                  onClick={() => {
                    console.log(formInstance.getFieldValue(['patients', field.name, 'phone']));
                    var phoneNumber = formInstance.getFieldValue(['patients', field.name, 'phone']);
                    if (phoneNumber) {
                      console.log(phoneNumber);
                      csCallout(phoneNumber.trim());
                    }
                  }}
                >
                  Gọi
                </Button>
              </Col>
            </Row>
          )}
          {!isEdit && (
            <Form.Item
              {...field}
              name={[field.name, 'phone']}
              required
              rules={[
                {
                  required: true,
                  message: 'SĐT không được để trống hoặc không hợp lệ.',
                  pattern: /(84|0[1|2|3|5|6|7|8|9])+([0-9]{8,10})\b/g
                }
              ]}
              ß
            >
              <Input
                disabled={isDisabled}
                onKeyUp={e => {
                  if (e.key == 'Enter') {
                    setIsSearching(true);
                  }
                }}
                placeholder="Nhập số điện thoại"
              />
            </Form.Item>
          )}
        </Col>
        <Modal
          title="Đang thực hiện cuộc gọi ..."
          visible={isCalling}
          onOk={() => {}}
          onCancel={() => {
            setIsCalling(false);
          }}
        >
          <Image width={200} src={require('../../../assets/icons/calling.gif')} />
        </Modal>
        <Modal
          maskClosable={false}
          centered
          style={{ marginTop: 5, height: 'calc(100vh - 200px)' }}
          bodyStyle={{ display: 'flex', overflowY: 'auto' }}
          title="Danh sách thông tin khách hàng"
          visible={isSearching}
          onCancel={() => {
            setIsSearching(false);
          }}
          footer={null}
          width={1000}
          destroyOnClose={true}
        >
          <AppointmentPhoneSearch
            phoneNumber={formInstance.getFieldValue(['patients', field.name, 'phone'])}
            onApply={onApply}
            onApplyPID={onApplyPID}
          />
        </Modal>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{phoneNumber}</Col> : phoneNumber;
  };

  //Đường phố
  interface Street4HomeProps {
    streetList: Street[];
    isLoading: boolean;
    isDisabled?: boolean;
    handleChangeStreet: (routeId: string, appointmentDate: string) => void;
  }
  const Street4Home: FC<Street4HomeProps> = ({ streetList, handleChangeStreet, isLoading, isDisabled }) => {
    const onChangeStreet = (value: string) => {
      const arrSplit = value.split('***');
      setRouteId(arrSplit[1]);
      handleChangeStreet(arrSplit[1], appointmentDate);
    };
    const street = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Đường phố:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="samplingStreetId" rules={[{ required: true, message: 'Đường phố không được bỏ trống' }]}>
            <Select
              disabled={isDisabled}
              loading={isLoading}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={onChangeStreet}
              placeholder="-- chọn Tỉnh/TP để có đường phố --"
            >
              {streetList?.map(street => (
                <Select.Option key={street.id} value={street.id + '***' + street.routeId}>
                  {street.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{street}</Col> : street;
  };
  //Cung đường
  interface RouteProps {
    routeNameDefault: boolean;
  }
  const Route: FC<RouteProps> = ({ routeNameDefault }) => {
    const route = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Cung đường:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item name="samplingRouteName" rules={[{ required, message: 'Cung đường không được để trống' }]}>
            <Input disabled={true} placeholder="Chọn Đường phố để có Cung đường" />
          </Form.Item>
          <Form.Item name="samplingRouteId" style={{ display: 'none' }}>
            <Input disabled={true} placeholder="Chọn Đường phố để có Cung đường" />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{route}</Col> : route;
  };

  //Đơn vị
  interface FacilityProps {}
  const Facility: FC<FacilityProps> = ({}) => {
    const facility = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Đơn vị:
        </Col>
        <Col {...wrapperColFourItem}>
          <Form.Item
            name="examFacilityName"

            // rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input disabled={true} placeholder="Chọn Đường phố để có Đơn vị" />
          </Form.Item>
          <Form.Item
            name="examFacilityId"
            style={{ display: 'none' }}
            // rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input disabled={true} placeholder="Chọn Đường phố để có Đơn vị" />
          </Form.Item>
        </Col>
        <Col {...wrapperColFourItem}>
          <Form.Item
            name="facilityName"

            // rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input disabled={true} placeholder="Chọn Đường phố để có Văn phòng" />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{facility}</Col> : facility;
  };

  //Ho ten KH
  interface CustomerFullnameProps {
    field: any;
    isDisabled?: boolean;
  }

  const CustomerFullname: FC<CustomerFullnameProps> = ({ field, isDisabled }) => {
    const customerFullname = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Họ tên KH:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            normalize={value => value?.toUpperCase()}
            name={[field.name, 'patientName']}
            rules={[{ required: true, message: 'Họ tên KH không được để trống.' }]}
          >
            <Input disabled={isDisabled} placeholder="Nhập họ tên khách hàng" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{customerFullname}</Col> : customerFullname;
  };

  //Quốc tịch
  interface NationalityProps {
    countries: Country[];
    isLoading: boolean;
    field: any;
    isDisabled?: boolean;
    setPatternIdNo;
  }
  const Nationality: FC<NationalityProps> = ({ countries, isLoading, field, isDisabled, setPatternIdNo }) => {
    const nationality = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Quốc tịch:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field?.name, 'nationality']}
            rules={[{ required: true, message: 'Quốc tịch không được để trống.' }]}
          >
            <Select
              disabled={isDisabled}
              loading={isLoading}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Quốc tịch --"
              onChange={(value, item) => {
                if (String(value).toUpperCase() === 'VN') {
                  setPatternIdNo(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
                } else {
                  setPatternIdNo(/^[a-zA-Z0-9_.-]{6,13}$/ as RegExp);
                }
                formInstance.setFieldsValue({
                  patient: { countryName: item['children'] }
                });
              }}
            >
              {countries?.map(country => (
                <Select.Option key={country.id} value={country.countryCode}>
                  {country.countryName}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item style={{ display: 'none' }} name={['patient', 'countryName']}>
            <Input />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{nationality}</Col> : nationality;
  };

  interface GenderProps {
    field: any;
    isDisabled?: boolean;
  }

  //Gioi tinh
  const Gender: FC<GenderProps> = ({ field, isDisabled }) => {
    const gender = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Giới tính:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field?.name, 'patientSex']}
            rules={[{ required: true, message: 'Giới tính không được để trống.' }]}
          >
            <Select disabled={isDisabled} placeholder="-- Chọn Giới tính --">
              <Option value="MALE">Nam</Option>
              <Option value="FEMALE">Nữ</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{gender}</Col> : gender;
  };

  interface ContactAddressProps {
    field: any;
    isDisabled?: boolean;
  }

  //Dia chi lien he
  const ContactAddress: FC<ContactAddressProps> = ({ field, isDisabled }) => {
    const contactAddress = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Địa chỉ hiện tại:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            {...field}
            name={[field?.name, 'address']}
            rules={[{ required: true, max: 100, message: 'Địa chỉ hiện tại không hợp lệ hoặc quá dài.' }]}
          >
            <Input disabled={isDisabled} placeholder="Nhập Địa chỉ hiện tại" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{contactAddress}</Col> : contactAddress;
  };

  //Dia chi lien he
  interface AddressProps {
    isDisabled?: boolean;
  }
  const Address: FC<AddressProps> = ({ isDisabled }) => {
    const address = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Địa chỉ lấy mẫu:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name={'samplingAddress'}
            rules={[{ required: true, max: 100, message: 'Địa chỉ lấy mẫu không hợp lệ hoặc quá dài.' }]}
          >
            <Input disabled={isDisabled} placeholder="Nhập Địa chỉ lấy mẫu" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{address}</Col> : address;
  };

  //Mã giới thiệu

  interface StaffCodeProps {
    field: any;
    isDisabled?: boolean;
  }

  const StaffCode: FC<StaffCodeProps> = ({ field, isDisabled }) => {
    const staffCode = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Mã giới thiệu:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            {...field}
            name={[field?.name, 'preDoctorHisId']}
            // rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input disabled={isDisabled} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{staffCode}</Col> : staffCode;
  };

  //CMND/CCCD
  interface IdentityCardProps {
    isValidate?: boolean;
    field;
    isDisabled?: boolean;
  }
  const IdentityCard: FC<IdentityCardProps> = ({ isValidate, field, isDisabled }) => {
    const identityCard = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          CMND/CCCD:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field?.name, 'patientIdNo']}
            rules={[{ required: false, message: 'Không đúng định dạng', pattern: patternIdNo }]}
          >
            <Input
              disabled={isDisabled}
              onChange={e => {
                // setIdNo(e.target.value);
              }}
              placeholder="Nhập CMND/CCCD"
            />
          </Form.Item>
          {/* {checkIdNo && <span>Không đúng định dạng</span>} */}
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{identityCard}</Col> : identityCard;
  };

  //Lý do khám
  interface ReasonCheckupProps {
    field: any;
    isDisabled?: boolean;
    setIsNote;
  }

  const ReasonCheckup: FC<ReasonCheckupProps> = ({ field, isDisabled, setIsNote }) => {
    const reasonCheckup = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Lý do khám:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field.name, 'reasonNote']}
            rules={[{ required: true, message: 'Giới tính không được để trống.' }]}
          >
            <Select
              disabled={isDisabled}
              placeholder="-- Chọn Lý do khám --"
              onChange={value => {
                if (value && value == 'Khác') {
                  setIsNote(true);
                } else {
                  setIsNote(false);
                }
              }}
            >
              <Option value="Khác">Khác</Option>
              <Option value="Khám bệnh">Khám bệnh</Option>
              {/* <Option value="SARS-COV-2-Ag">Có dịch tễ nghi ngờ (Test nhanh)</Option> */}
              <Option value="SARS-COV-2-PCR">Có triệu chứng nghi ngờ (PCR - Đơn)</Option>
              <Option value="SARS-COV-2-PCR-FA">Có KQXN nghi ngờ nhiễm COVID-19 (PCR-Đơn)</Option>
              <Option value="SARS-COV-2-PCR-XC">Nhập cảnh/ Xuất cảnh (PCR-Đơn Xuất cảnh)</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{reasonCheckup}</Col> : reasonCheckup;
  };

  //Ngay khoi phat
  interface DateOnsetProps {
    field: any;
    isDisabled?: boolean;
  }

  const DateOnset: FC<DateOnsetProps> = ({ field, isDisabled }) => {
    const dateFormat = 'DD/MM/YYYY';

    const dateOnset = (
      <Row className="">
        <Col {...labelColTowItem} className="fs-12">
          Ngày khởi phát triệu chứng:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item {...field} name={[field.name, 'healthDeclaration', 'dateOnset']}>
            <DatePicker
              disabled={isDisabled}
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              placeholder="Chọn ngày khởi phát"
              format={dateFormat}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{dateOnset}</Col> : dateOnset;
  };

  //Ngay cap CCCD
  interface IssuedDateProps {
    field: any;
    isDisabled?: boolean;
  }

  const IssuedDate: FC<IssuedDateProps> = ({ field, isDisabled }) => {
    const dateFormat = 'DD/MM/YYYY';

    const issuedDate = (
      <Row className="">
        <Col {...labelColTowItem} className="fs-12">
          Ngày cấp CMT/CCCD:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item {...field} name={[field.name, 'issuedDate']}>
            <DatePicker
              disabled={isDisabled}
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              placeholder="Chọn ngày cấp CCCD"
              format={dateFormat}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{issuedDate}</Col> : issuedDate;
  };

  //Nơi cấp CCCD
  interface IssuedPlaceProps {
    field: any;
    isDisabled?: boolean;
  }

  const IssuedPlace: FC<IssuedPlaceProps> = ({ field, isDisabled }) => {
    const issuedPlace = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Nơi cấp CMT/CCCD:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item {...field} name={[field.name, 'issuedPlace']}>
            <Input disabled={isDisabled} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{issuedPlace}</Col> : issuedPlace;
  };

  //Họ tên người giám hộ
  interface GuardianNameProps {
    field: any;
    isDisabled?: boolean;
  }

  const GuardianName: FC<GuardianNameProps> = ({ field, isDisabled }) => {
    const guardianName = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Họ tên người thân/giám hộ:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            {...field}
            name={[field.name, 'guardianName']}
            // rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input disabled={isDisabled} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{guardianName}</Col> : guardianName;
  };

  //Số điện thoại người giám hộ
  interface GuardianPhoneProps {
    field: any;
    isDisabled?: boolean;
  }
  const GuardianPhone: FC<GuardianPhoneProps> = ({ field, isDisabled }) => {
    const guardianPhone = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          SĐT người thân/giám hộ:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name={[field.name, 'guardianPhone']}
            // rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input disabled={isDisabled} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{guardianPhone}</Col> : guardianPhone;
  };

  //Ghi chu XN
  interface TestNotesProps {
    field: any;
    isDisabled?: boolean;
    required: boolean;
  }

  const TestNotes: FC<TestNotesProps> = ({ field, isDisabled, required }) => {
    const testNotes = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12">
          Ghi chú lịch hẹn:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            {...field}
            name={[field.name, 'appointmentNote']}
            rules={[{ required: required, message: 'Bắt buộc nhập!' }]}
          >
            <Input disabled={isDisabled} placeholder="Nhập Ghi chú" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{testNotes}</Col> : testNotes;
  };

  interface ConfirmProps {
    field: any;
    isDisplaySubmit: boolean;
  }

  const Confirm: FC<ConfirmProps> = ({ field, isDisplaySubmit }) => {
    const confirm = (
      <div className="kbyt-confirm">
        <Form.Item
          {...field}
          name={[field.name, 'confirm']}
          rules={[
            {
              required: true,
              message: 'Không được bỏ trống!'
              // validateTrigger: 'onSubmit'
            }
          ]}
        >
          <CheckboxGroup disabled={isDisplaySubmit} name={field.name + '.confirm'}>
            <Checkbox indeterminate={false} value={'1'}>
              <span className="red">Tôi cam kết đúng thông tin *</span>
            </Checkbox>
          </CheckboxGroup>
        </Form.Item>
      </div>
    );
    return confirm;
  };

  interface InvoiceProps {
    setIsInvoice;
    field: any;
    isDisabled?: boolean;
  }

  const Invoice: FC<InvoiceProps> = ({ setIsInvoice, field, isDisabled }) => {
    const invoice = (
      <div className="flex">
        <div className="text-invoice">Bạn có muốn xuất hóa đơn ?</div>
        <Form.Item {...field} name={[field?.name, 'healthDeclaration', 'invoice']} className="">
          <CheckboxGroup
            disabled={isDisabled}
            name={field.name + '.healthDeclaration.invoice'}
            onChange={value => {
              if (value && value.length > 0) {
                setIsInvoice(true);
              } else {
                setIsInvoice(false);
              }
            }}
          >
            <Checkbox value="1" indeterminate={false}>
              <span>Có</span>
            </Checkbox>
          </CheckboxGroup>
        </Form.Item>
      </div>
    );
    return invoice;
  };

  interface PrintNameProps {
    field: any;
    isDisabled?: boolean;
  }

  const PrintName: FC<PrintNameProps> = ({ field, isDisabled }) => {
    const printName = (
      <Form.Item {...field} name={[field?.name, 'healthDeclaration', 'printName']} className="">
        <CheckboxGroup disabled={isDisabled} name={field?.name + '.healthDeclaration.printName'}>
          <Checkbox indeterminate={false} value="1">
            <span className="red">Ẩn tên khách hàng</span>
          </Checkbox>
        </CheckboxGroup>
      </Form.Item>
    );
    return printName;
  };

  //
  interface AddressProfileProps {
    required: boolean;
    field: any;
    isDisabled?: boolean;
  }
  const AddressProfile: FC<AddressProfileProps> = ({ required, field, isDisabled }) => {
    const addressProfile = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Địa chỉ:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field.name, 'healthDeclaration', 'addressProfile']}
            rules={[{ required: required, message: 'Địa chỉ không được để trống.' }]}
          >
            <Input disabled={isDisabled} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{addressProfile}</Col> : addressProfile;
  };

  //
  interface AddressCompanyProps {
    required: boolean;
    field: any;
    isDisabled?: boolean;
  }
  const AddressCompany: FC<AddressCompanyProps> = ({ required, field, isDisabled }) => {
    const addressCompany = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Địa chỉ công ty:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field?.name, 'healthDeclaration', 'addressCompany']}
            rules={[{ required: required, message: 'Địa chỉ công ty không được để trống.' }]}
          >
            <Input disabled={isDisabled} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{addressCompany}</Col> : addressCompany;
  };

  //
  interface EmailProfileProps {
    required: boolean;
    field: any;
    isDisabled?: boolean;
  }
  const EmailProfile: FC<EmailProfileProps> = ({ required, field, isDisabled }) => {
    const emailProfile = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Email:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field?.name, 'healthDeclaration', 'emailProfile']}
            rules={[
              {
                required: required,
                message: 'Email không được để trống và phải đúng định dạng',
                pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
              }
            ]}
          >
            <Input disabled={isDisabled} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{emailProfile}</Col> : emailProfile;
  };

  //
  interface EmailCompanyProps {
    required: boolean;
    field: any;
    isDisabled?: boolean;
  }
  const EmailCompany: FC<EmailCompanyProps> = ({ required, field, isDisabled }) => {
    const emailCompany = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Email:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field?.name, 'healthDeclaration', 'emailCompany']}
            rules={[
              {
                required: required,
                message: 'Email không được để trống.',
                pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
              }
            ]}
          >
            <Input disabled={isDisabled} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{emailCompany}</Col> : emailCompany;
  };

  //
  interface TaxCodeProps {
    required: boolean;
    field: any;
    isDisabled?: boolean;
  }
  const TaxCode: FC<TaxCodeProps> = ({ required, field, isDisabled }) => {
    const taxCode = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Mã số thuế:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field.name, 'healthDeclaration', 'taxCode']}
            rules={[{ required: required, message: 'Mã số thuế không được để trống.' }]}
          >
            <Input disabled={isDisabled} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{taxCode}</Col> : taxCode;
  };

  //
  interface NameCompanyProps {
    required: boolean;
    field: any;
    isDisabled?: boolean;
  }
  const NameCompany: FC<NameCompanyProps> = ({ required, field, isDisabled }) => {
    const nameCompany = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Tên công ty:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field.name, 'healthDeclaration', 'nameCompany']}
            rules={[{ required: required, message: 'Tên công ty không được để trống.' }]}
          >
            <Input disabled={isDisabled} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{nameCompany}</Col> : nameCompany;
  };

  //NationPassOther
  interface NationPassOtherProps {
    field: any;
  }
  const NationPassOther: FC<NationPassOtherProps> = ({ field }) => {
    const nationPassOther = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 3. Tiền sử đi/đến/ở/về từ nước ngoài/ địa phương có dịch bệnh Covid 19 trong
          thời gian 14 ngày trở lại đây?
        </div>
        <Form.Item
          {...field}
          name={[field.name, 'healthDeclaration', 'nationPassOther']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group name={field.name + '.healthDeclaration.nationPassOther'}>
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return nationPassOther;
  };

  //HumanHasCovid
  interface HumanHasCovidProps {
    field: any;
    isDisabled?: boolean;
  }
  const HumanHasCovid: FC<HumanHasCovidProps> = ({ field, isDisabled }) => {
    const humanHasCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 1. Có tiền sử tiếp xúc gần với người nhiễm COVID- 19 trong thời gian 2 ngày
          trước kể từ ngày F0 được lấy mẫu xác định hoặc tính từ ngày F0 khởi phát triệu chứng? (đối tượng F1 tiếp xúc
          gần F0)
        </div>
        <Form.Item
          {...field}
          name={[field.name, 'healthDeclaration', 'humanHasCovid']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group disabled={isDisabled} name={field.name + '.healthDeclaration.humanHasCovid'}>
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return humanHasCovid;
  };

  //TestedCovid
  interface TestedCovidProps {
    field: any;
    isDisabled?: boolean;
  }
  const TestedCovid: FC<TestedCovidProps> = ({ field, isDisabled }) => {
    const testedCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 3. Đã làm xét nghiệm SARS CoV2 trước đó có kết quả xét nghiệm test nhanh Ag
          dương tính/ hoặc PCR dương tính/nghi ngờ dương tính?
        </div>
        <Form.Item
          {...field}
          name={[field.name, 'healthDeclaration', 'testedCovid']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group disabled={isDisabled} name={field.name + '.healthDeclaration.testedCovid'}>
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return testedCovid;
  };

  //prognostic
  interface PrognosticProps {
    field: any;
    isDisabled?: boolean;
  }
  const Prognostic: FC<PrognosticProps> = ({ field, isDisabled }) => {
    const prognostic = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> <b>Mục A. Trong 03 ngày gần đây anh/chị có các biểu hiện sau đây</b>
        </div>
        <Form.Item
          {...field}
          name={[field.name, 'healthDeclaration', 'prognostic']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <CheckboxGroup
            disabled={isDisabled}
            name={field.name + '.healthDeclaration.prognostic'}
            onChange={value => {
              var patients = formInstance.getFieldValue('patients');
              if (patients) {
                var item = patients[field.name];
                if (item && item?.healthDeclaration && item?.healthDeclaration?.prognostic) {
                  if (value && value[value.length - 1] == '1') {
                    item.healthDeclaration.prognostic = ['1'];
                    patients[field.name] = item;
                    formInstance.setFieldsValue({ patients: patients });
                  } else if (value && value[value.length - 1] != '1') {
                    if (value.indexOf('1') != -1) value.splice(value.indexOf('1'), 1);
                    item.healthDeclaration.prognostic = value;
                    patients[field.name] = item;
                    formInstance.setFieldsValue({ patients: patients });
                  } else {
                    item.healthDeclaration.prognostic = value;
                    patients[field.name] = item;
                    formInstance.setFieldsValue({ patients: patients });
                  }
                }
              }
              if (value && value[value.length - 1] == '1') {
                item.healthDeclaration.prognostic = ['1'];
                patients[field.name] = item;
                formInstance.setFieldsValue({ patients: patients });
              } else if (value && value[value.length - 1] != '1') {
                if (value.indexOf('1') != -1) value.splice(value.indexOf('1'), 1);
                item.healthDeclaration.prognostic = value;
                patients[field.name] = item;
                formInstance.setFieldsValue({ patients: patients });
              } else {
                item.healthDeclaration.prognostic = value;
                patients[field.name] = item;
                formInstance.setFieldsValue({ patients: patients });
              }
            }}
          >
            <div className="checkbox-kbyt">
              <Checkbox value="16">Ho - đau họng</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="2">Sốt</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="4">Khó thở - tức ngực</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="8">Các biểu hiện: Đau mỏi người. gai rét. đau họng</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="32">Giảm hoặc mất vị giác hoặc khứu giác</Checkbox>
            </div>
            <div className="checkbox-kbyt">
              <Checkbox value="1">Không có triệu chứng</Checkbox>
            </div>
          </CheckboxGroup>
        </Form.Item>
      </div>
    );
    return prognostic;
  };

  //StreetHasCovid
  interface StreetHasCovidProps {
    field: any;
    isDisabled?: boolean;
  }
  const StreetHasCovid: FC<StreetHasCovidProps> = ({ field, isDisabled }) => {
    const streetHasCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 2. Di chuyển trên cùng phương tiện giao thông hoặc cùng địa điểm, sự kiện, nơi
          làm việc, lớp học… với ca bệnh xác định (F0) trong thời gian 2 ngày trước kể từ ngày F0 được lấy mẫu xác định
          hoặc tính từ ngày F0 khởi phát triệu chứng.
        </div>
        <Form.Item
          {...field}
          name={[field.name, 'healthDeclaration', 'streetHasCovid']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group disabled={isDisabled} name={field.name + '.healthDeclaration.streetHasCovid'}>
            <Row>
              <Col span={12}>
                <Radio value="1">Có</Radio>
              </Col>
              <Col span={12}>
                <Radio value="0">Không</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return streetHasCovid;
  };

  //Đã tiêm đủ 2 mũi vắc xin
  interface TowInjectionsProps {
    field: any;
    isDisabled?: boolean;
  }
  const TowInjections: FC<TowInjectionsProps> = ({ field, isDisabled }) => {
    const towInjections = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 2. Đã tiêm vắc xin phòng ngừa COVID-19 chưa?
        </div>
        <Form.Item
          {...field}
          name={[field.name, 'healthDeclaration', 'hasVaccine']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group disabled={isDisabled} name={field.name + '.healthDeclaration.streetHasCovid'}>
            <Row>
              <Col span={6}>
                <Radio value="0">Chưa tiêm</Radio>
              </Col>
              <Col span={6}>
                <Radio value="1">1 mũi</Radio>
              </Col>
              <Col span={6}>
                <Radio value="2">2 mũi</Radio>
              </Col>
              <Col span={6}>
                <Radio value="3">&gt; 2 mũi</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return towInjections;
  };

  //Đã từng nhiễm covid
  interface IsCovidProps {
    field: any;
    isCovided: boolean;
    setIsCovided: any;
    isDisabled?: boolean;
  }
  const IsCovid: FC<IsCovidProps> = ({ field, isCovided, setIsCovided, isDisabled }) => {
    const isCovid = (
      <div className="field-css">
        <div className="">
          <span className="red">* </span> 1. Tiền sử nhiễm COVID – 19 trong vòng 6 tháng gần đây không?
        </div>
        <Form.Item
          {...field}
          name={[field.name, 'healthDeclaration', 'hasCovid']}
          rules={[{ required: true, message: 'Không được bỏ trống!' }]}
        >
          <Radio.Group
            disabled={isDisabled}
            onChange={value => {
              var itemValue = value.target.value;
              if (itemValue == 1 || itemValue == 3) {
                setIsCovided(true);
              } else {
                setIsCovided(false);
              }
            }}
            name={field.name + '.healthDeclaration.hasCovid'}
          >
            <Row>
              <Col span={6}>
                <Radio value="1">Đã từng</Radio>
              </Col>
              <Col span={6}>
                {(formInstance.getFieldValue(['patients', field.name, 'healthDeclaration', 'hasCovid']) == 1 ||
                  formInstance.getFieldValue(['patients', field.name, 'healthDeclaration', 'hasCovid']) == 3) && (
                  <Form.Item
                    {...field}
                    name={[field.name, 'healthDeclaration', 'dateHascovid']}
                    label="Thời gian nhiễm"
                    rules={[{ required: isCovided, message: 'Bắt buộc phải nhập' }]}
                  >
                    <DatePicker format="DD/MM/YYYY" placeholder="-- Chọn ngày --" />
                  </Form.Item>
                )}
              </Col>
              <Col span={6}>
                <Radio value="2">Chưa từng</Radio>
              </Col>
              <Col span={6}>
                <Radio value="3">Đang điều trị</Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
      </div>
    );
    return isCovid;
  };

  //Dia chi lay mau
  const SamplingAddress: FC = () => {
    const samplingAddress = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Địa chỉ lấy mẫu:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name={['patient', 'address']}
            // rules={[{ required: true, max: 100, message: 'Địa chỉ liên hệ không hợp lệ hoặc quá dài.' }]}
          >
            <Input placeholder="Nhập địa chỉ lấy mẫu" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{samplingAddress}</Col> : samplingAddress;
  };

  //Tinh/TP
  interface SamplingProvincesProps {
    isLoading: boolean;
    provinceList: Province[];
    handleChangeProvince: (provinceId: string) => void;
  }
  const SamplingProvinces: FC<SamplingProvincesProps> = ({ provinceList, handleChangeProvince, isLoading }) => {
    const onChange = (value: string) => {
      handleChangeProvince(value);
      //   formInstance.setFieldsValue({
      //     patient: {
      //       districtId: undefined,
      //       wardId: undefined
      //     },
      //     streetId: undefined,
      //     routeId: undefined
      //   });
      //   handleChangeProvince(value);
      //   setFirst(true);
    };

    const samplingProvinces = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Tỉnh thành:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            name={'samplingProvinceId'}
            rules={[
              {
                required: true,
                message: 'Tỉnh thành không được để trống.'
              }
            ]}
          >
            <Select
              loading={isLoading}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={onChange}
              placeholder="-- Chọn Tỉnh/TP --"
            >
              {provinceList?.map(province => (
                <Select.Option key={province.id} value={province.id}>
                  {province.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{samplingProvinces}</Col> : samplingProvinces;
  };

  //Quận huyện
  interface SamplingDistrictProps {
    districtList: District[];
    isLoading: boolean;
    isDisabled?: boolean;
  }

  const SamplingDistricts: FC<SamplingDistrictProps> = ({ districtList, isLoading, isDisabled }) => {
    const samplingDistricts = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Quận/Huyện:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={'samplingDistrictId'}
            rules={[
              {
                required: true,
                message: 'Quận/Huyện không được để trống.'
              }
            ]}
          >
            <Select
              // ref={ref={disRefC}}
              disabled={isDisabled}
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Quận/Huyện --"
            >
              {districtList?.map(district => (
                <Select.Option key={district.id} value={district.id}>
                  {district.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{samplingDistricts}</Col> : samplingDistricts;
  };

  interface DateofBirthProps {
    field: any;
    isDisabled?: boolean;
  }

  //Ngay sinh
  const handleCheckInput = (e: React.KeyboardEvent<HTMLInputElement>, field) => {
    setTimeout(() => {
      let value = (e.target as HTMLInputElement)?.value;
      const formattedDate = formatDateTyping(e.key, value);
      if (e.key === 'Backspace' || (isNaN(Number(e.key)) && e.key !== '/')) {
      } else {
        e.target['value'] = formattedDate;
      }
      const birthDate = moment(formattedDate, 'DD/MM/YYYY');
      if (formattedDate.length === 10) {
        var listItem = formInstance.getFieldValue('patients');
        if (listItem && listItem.length > 0 && field) {
          var item = listItem[field.name];
          if (item) {
            item.patientBirthDate = moment(birthDate, 'DD/MM/YYYY');
            listItem[field.name] = item;
            formInstance.setFieldsValue({
              patients: listItem
            });
          }
        }
      }
    });
  };

  //Ngay sinh
  const DateofBirth: FC<DateofBirthProps> = ({ field, isDisabled }) => {
    const dateFormat = 'DD/MM/YYYY';
    const startYear = moment('1900', 'YYYY');
    const dofBirth = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Ngày sinh:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          {/* <Row> */}
            {/* <Col span={14}> */}
              <Form.Item
                {...field}
                name={[field?.name, 'patientBirthDate']}
                rules={[{ required: true, message: 'Ngày sinh không được để trống.' }]}
              >
                <DatePicker
                  onKeyDown={e => {
                    handleCheckInput(e, field);
                  }}
                  onChange={(date, dateString) => {
                    var listItem = formInstance.getFieldValue('patients');
                    if (listItem && listItem.length > 0 && (field?.name || Number(index) == 0)) {
                      var item = listItem[field?.name];
                      if (date === null) {
                        if (item) {
                          item.patientBirtYear = undefined;
                        }
                      } else {
                        if (item) {
                          item.patientBirtYear = moment(date, 'YYYY');
                        }
                      }
                      formInstance.setFieldsValue({
                        patients: listItem
                      });
                    }
                  }}
                  id={'date-picker-antd-appointment-create' + field.name}
                  disabled={isDisabled}
                  disabledDate={current => {
                    return current && current > moment().add(0, 'day');
                  }}
                  placeholder="DD/MM/YYYY"
                  format={dateFormat}
                />
              </Form.Item>
            {/* </Col>
            <Col span={10}>
              <Form.Item
                {...field}
                name={[field?.name, 'patientBirtYear']}
                rules={[
                  { required: true, message: 'Năm sinh không được để trống.' },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue(['patients', field?.name, 'patientBirtYear']) > startYear) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('Năm sinh phải lớn hơn 1900.'));
                    }
                  })
                ]}
              >
                <DatePicker
                  disabled={isDisabled}
                  onChange={(date, dateString) => {
                    if (date) {
                      var listItem = formInstance.getFieldValue('patients');
                      if (listItem && listItem.length > 0 && (field?.name || Number(index) == 0)) {
                        var item = listItem[field?.name];
                        if (item) {
                          item.patientBirthDate = moment('01/01/' + dateString, dateFormat);
                          formInstance.setFieldsValue({
                            patients: listItem
                          });
                        }
                      }
                    }
                  }}
                  disabledDate={current => {
                    return current && current > moment().add(0, 'day');
                  }}
                  format="YYYY"
                  // placeholder="Chọn năm sinh"
                  picker="year"
                />
              </Form.Item>
            </Col>
          </Row> */}
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{dofBirth}</Col> : dofBirth;
  };

  //Đối tượng
  interface TypeCustomerProps {
    // appointmentObjects: any;
    // handleChange: (value) => void;
    field: any;
    isDisabled?: boolean;
  }
  const TypeCustomer: FC<TypeCustomerProps> = ({ field, isDisabled }) => {
    const typeCustomer = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Đối tượng :
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field.name, 'healthDeclaration', 'typeCustomerKBYT']}
            rules={[{ required: true, message: 'Đối tượng khám không được để trống.' }]}
          >
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Đối tượng --"
              defaultValue="6"
              disabled={isDisabled}
            >
              <Select.Option value="">-- Chọn --</Select.Option>
              <Select.Option value="6">Xét nghiệm COVID-19</Select.Option>
              {/* <Select.Option value="2">Người nhà</Select.Option>
              <Select.Option value="3">Nhân viên y tế</Select.Option>
              <Select.Option value="4">Khách đến công tác</Select.Option>
              <Select.Option value="5">Tiêm chủng vắc xin</Select.Option>
              <Select.Option value="7">Khai hộ người khác</Select.Option> */}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{typeCustomer}</Col> : typeCustomer;
  };

  //Cán bộ được phân
  interface SiteStaffIdProps {
    // appointmentObjects: any;
    // handleChange: (value) => void;
    isDisabled?,
   listStaff:any[]
  }
  const SiteStaffId: FC<SiteStaffIdProps> = ({ listStaff,isDisabled }) => {
    const siteStaffId = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Cán bộ được phân :
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={"assignStaffId"}
            // rules={[{ required: true, message: 'Đối tượng khám không được để trống.' }]}
          >
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn cán bộ --"
              disabled={isDisabled}
            >
              <Select.Option value="">-- Chọn --</Select.Option>
              {
                listStaff?.map(item =>{
                  return <Select.Option value={item.id}>{item.name}</Select.Option>
                })
              }
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{siteStaffId}</Col> : siteStaffId;
  };
  //Mã thẻ KH PID
  interface PIDProps {
    field: any;
  }
  const PID: FC<PIDProps> = ({ field }) => {
    const pId = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Mã thẻ KH (PID):
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            {...field}
            name={[field.name, 'pid']}
            rules={[
              {
                required: false,
                message: 'Mã thẻ KH (PID) không được để trống.'
              }
            ]}
          >
            <Input readOnly={true} placeholder="" />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{pId}</Col> : pId;
  };
  return React.useMemo(
    () => ({
      form: formInstance,
      Form: WrappedForm,
      AppointmentCode,
      AppointmentStatus,
      PhoneNumber,
      CustomerFullname,
      Gender,
      Nationality,
      ContactAddress,
      Provinces,
      Districts,
      TestNotes,
      IdentityCard,
      AppointmentDate,
      AppointmentWorkTime,
      Route,
      Street4Home,
      DateOnset,
      Prognostic,
      StreetHasCovid,
      NationPassOther,
      TestedCovid,
      HumanHasCovid,
      Invoice,
      AddressProfile,
      EmailProfile,
      NameCompany,
      TaxCode,
      AddressCompany,
      EmailCompany,
      PrintName,
      SamplingDistricts,
      SamplingProvinces,
      SamplingAddress,
      IsCovid,
      TowInjections,
      GuardianPhone,
      GuardianName,
      IssuedPlace,
      IssuedDate,
      ReasonCheckup,
      StaffCode,
      PackageType,
      PackageId,
      ProvincesAll,
      Facility,
      Address,
      DateofBirth,
      TypeCustomer,
      PID,
      Confirm,
      SiteStaffId
    }),
    [patternIdNo]
  );
}
