import {
  CloseOutlined,
  CopyrightOutlined,
  DeleteOutlined,
  DownOutlined,
  ExclamationCircleOutlined,
  PlusCircleOutlined
} from '@ant-design/icons';
import { Button, Dropdown, Menu, Modal, Table, Tag, Tooltip } from 'antd';
import {
  apiGetAllAppoimentCovidOfHome,
  cancelCovidAppointment,
  deleteCovidAppointment
} from 'api/appoiment-covid-home/appoimentCovid';
import { AppoimentCovidTable } from 'common/interfaces/appoimentCovidTableData.inteface';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { functionCodeConstants } from 'constants/functions';
import moment from 'moment';
import React, { useCallback, useEffect, useState } from 'react';
import { FC } from 'react';
import { Link, useNavigate, useSearchParams } from 'react-router-dom';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { setUserItem } from 'store/user.store';
import { openNotificationRight } from 'utils/notification';
import { getStatus } from '../appointment/utils';
interface AppointmentCovidTableProps {
  valueSearch: any;
  functionOb: any;
}

const AppointmentCovidTable: FC<AppointmentCovidTableProps> = ({ valueSearch, functionOb }) => {
  const [page, setPage] = useState(1);
  const navigate = useNavigate();
  const [pageSize, setPageSize] = useState(20);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [searchParams] = useSearchParams();
  const [selectedRowKeys, setSelectedRowKeys]: any = useState([]);
  const [totalRecord, setTotalRecord] = useState(0);
  const [isLoadingTable, setIsLoadingTable] = useState(false);
  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };
  let sorted = [{ id: 'createDate', desc: 'true', value: '' }];

  const [pagination, setPagination] = useState({});
  // const [appointmentDetail, setAppointmentDetail] = useState({} as AppoimentCovidTable);
  const [tableData, setTableData] = useState<AppoimentCovidTable[]>();

  const getAllAppoiment = useCallback(async (conditions: any, pageTmp, pageSizeTmp) => {
    setIsLoadingTable(true);
    if (localStorage.getItem('appoiment-covid-create-data')) {
      navigate('/appointment-management/appointment-covid/create');
    }
    const data: any = await apiGetAllAppoimentCovidOfHome(pageTmp, pageSizeTmp, conditions);
    if (data?.data?.status == 1) {
      setTableData(data?.data?.['data']);
      setTotalRecord(data?.data?.['records']);
      setPagination({ current: pageTmp, pageSize: pageSizeTmp, total: data?.data?.['records'] });
      setIsLoadingTable(false);
    } else {
      setIsLoadingTable(false);
    }
  }, []);

  const convertPhoneNumber = (str, index, replacement) => {
    if (str) {
      if (str.substr(0, 1) !== '0') {
        str = '0' + str;
        return str.substr(0, index) + replacement + str.substr(index + replacement.length);
      } else {
        return str.substr(0, index) + replacement + str.substr(index + replacement.length);
      }
    } else {
      return '';
    }
  };

  useEffect(() => {
    if (valueSearch && valueSearch.length > 0) {
      getAllAppoiment(
        {
          sorted,
          filtered: valueSearch
        },
        page,
        pageSize
      );
    } else {
      getAllAppoiment(
        {
          filtered: [
            {
              id: 'appointmentDate',
              operation: 'between',
              value: moment(new Date()).format('DD/MM/YYYY') + ',' + moment(new Date()).format('DD/MM/YYYY')
            }
          ],
          sorted
        },
        page,
        pageSize
      );
    }
  }, [page, pageSize]);

  //xoa lich
  const deleteAppoiment = async appoimentId => {
    var result: any = await deleteCovidAppointment(appoimentId);
    if (result?.data.status) {
      openNotificationRight('Xoá lịch hẹn thành công.');
      if (valueSearch && valueSearch.length > 0) {
        getAllAppoiment(
          {
            sorted,
            filtered: valueSearch
          },
          page,
          pageSize
        );
      } else {
        getAllAppoiment(
          {
            filtered: [
              {
                id: 'appointmentDate',
                operation: 'between',
                value: moment(new Date()).format('DD/MM/YYYY') + ',' + moment(new Date()).format('DD/MM/YYYY')
              }
            ],
            sorted
          },
          page,
          pageSize
        );
      }
      setSelectedRowKeys([]);
    } else {
      openNotificationRight('Lỗi : ' + result?.data?.message, 'error');
    }
  };

  //Huy lich
  const cancleAppoiment = async appoimentId => {
    var result: any = await cancelCovidAppointment(appoimentId);
    if (result?.data.status) {
      openNotificationRight('Huỷ lịch hẹn thành công.');
      if (valueSearch && valueSearch.length > 0) {
        getAllAppoiment(
          {
            sorted,
            filtered: valueSearch
          },
          page,
          pageSize
        );
      } else {
        getAllAppoiment(
          {
            filtered: [
              {
                id: 'appointmentDate',
                operation: 'between',
                value: moment(new Date()).format('DD/MM/YYYY') + ',' + moment(new Date()).format('DD/MM/YYYY')
              }
            ],
            sorted
          },
          page,
          pageSize
        );
      }

      setSelectedRowKeys([]);
    } else {
      openNotificationRight('Lỗi : ' + result?.data?.message, 'error');
    }
  };

  useEffect(() => {
    if (valueSearch && valueSearch.length > 0) {
      if (page != 1) {
        setPage(1);
      } else {
        getAllAppoiment(
          {
            sorted,
            filtered: valueSearch
          },
          1,
          pageSize
        );
      }
    }
  }, [valueSearch]);

  const handleMenuClick = e => {
    if (selectedRowKeys && selectedRowKeys.length == 0) {
      openNotificationRight('Vui lòng chọn lịch hẹn cụ thể để thực hiện thao tác này.', 'warning');
      return;
    }
    const keyMenu = e['key'];
    if (keyMenu === '2') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn huỷ lịch hẹn đã chọn ?',
        onOk() {
          cancleAppoiment(selectedRowKeys[0]);
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    } else if (keyMenu === '5') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn xóa lịch hẹn đã chọn ?',
        onOk() {
          deleteAppoiment(selectedRowKeys[0]);
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    }
    //setAppointmentDetail({} as AppointmentPatient);
  };

  const menu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item
        disabled={functionOb[functionCodeConstants.TD_LH_HUY_PL_COVID] ? false : true}
        key="2"
        icon={<CloseOutlined />}
      >
        Huỷ
      </Menu.Item>
      <Menu.Item
        disabled={functionOb[functionCodeConstants.TD_LH_XOA_COVID] ? false : true}
        key="5"
        icon={<DeleteOutlined />}
      >
        Xoá
      </Menu.Item>
    </Menu>
  );

  const handleTableChange = (pagination, filters, sorter) => {
    // reFetchData({
    //   sortField: sorter.field,
    //   sortOrder: sorter.order,
    //   pagination,
    //   ...filters
    // });
    if (pageSize != pagination['pageSize']) {
      setPage(1);
    } else {
      setPage(pagination['current']);
    }
    setPageSize(pagination['pageSize']);
  };

  return (
    <>
      <Table
        loading={isLoadingTable}
        className="table-appointment"
        rowKey={row => {
          return row['id'] + '#' + row['groupId'];
        }}
        rowSelection={{
          selectedRowKeys,
          onChange: onSelectChange,
          type: 'radio'
        }}
        title={() => (
          <>
            <div style={{ float: 'left', marginTop: '5px' }}>
              <label className="lblTableTxt">
                <Tag color="#87d068">Tổng số bản ghi : {totalRecord}</Tag>
              </label>
            </div>
            <div style={{ float: 'right' }}>
              <Link to="/appointment-management/appointment-covid/create">
                <Button
                  disabled={functionOb[functionCodeConstants.TD_LH_TLH_COVID] ? false : true}
                  icon={<PlusCircleOutlined />}
                  type="primary"
                  onClick={() => {
                    localStorage.setItem('IS_SHOW_CREATE_APPOINTMENT_COVID_DIALOG', 'true');
                  }}
                >
                  Thêm lịch hẹn
                </Button>
              </Link>
              &nbsp;&nbsp;&nbsp;
              {/*<Popover trigger="click" placement="topLeft" title="Chọn hành động" content={selectedRowKeys.length === 0 ? 'Vui lòng chọn 1 lịch hẹn để thực hiện thao tác này.' : contentActionButton(selectedRowKeys)}>
                          <Button on disabled={selectedRowKeys.length === 0 ? true : false} icon={<SelectOutlined />} type="primary">
                            Hành động
                          </Button>
                      </Popover>*/}
              <Dropdown trigger={['click']} overlay={menu}>
                <Button className="ant-dropdown-link" onClick={() => {}}>
                  Thao tác <DownOutlined />
                </Button>
              </Dropdown>
            </div>
            <div style={{ clear: 'both' }}></div>
          </>
        )}
        pagination={{
          ...pagination,
          defaultPageSize: 20,
          showSizeChanger: true,
          pageSizeOptions: ['10', '20', '30', '50']
        }}
        scroll={{ x: 450, y: 'calc(100vh - 317px)' }}
        dataSource={tableData}
        onChange={handleTableChange}
        onRow={record => {
          return {
            onDoubleClick: () => {
              navigate('/appointment-management/appointment-covid/update/' + record.groupId + '/' + record.id);
            },
            onClick: envent => {
              var listChose = [...selectedRowKeys];
              if (listChose.indexOf(record.id + '#' + record.groupId) != -1) {
                listChose = [];
              } else {
                listChose = [];
                listChose.push(record.id + '#' + record.groupId);
              }
              setSelectedRowKeys(listChose);
            },
            onContextMenu: event => {}
          };
        }}
      >
        <Table.Column<AppoimentCovidTable>
          width={50}
          title="STT"
          align="center"
          render={(value, item, index) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">{(page - 1) * pageSize + index + 1}</Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          width={130}
          title="Trạng thái"
          render={(_, record) => {
            return (
              <div
                style={{
                  display: 'flex',
                  textAlign:"right"
                }}

              >
                <Tooltip style={{

                }} title="Click đúp chuột để xem chi tiết"><span style={{
                  width:'80%'
                }}>{getStatus(record?.status)}</span></Tooltip>
                <Button
                  style={{
                    fontSize: 12,
                    width: 20,
                    height: 22
                  }}
                  title="copy"
                  icon={<CopyrightOutlined />}
                  onClick={() => {
                    var txtCopy = '';
                    txtCopy +=
                      record?.status === 1
                        ? 'Chờ xác nhận'
                        : record?.status === 2
                        ? 'Đã xác nhận'
                        : record?.status === 3
                        ? 'Chờ tư vấn'
                        : record?.status === 4
                        ? 'Đã tư vấn đầu vào'
                        : record?.status === 5
                        ? 'Chưa check'
                        : record?.status === 6
                        ? 'Đã check'
                        : record?.status === 7
                        ? 'Đã lấy mẫu'
                        : record?.status === 8
                        ? 'Đã có kết quả'
                        : record?.status === 9
                        ? 'Đã đăng ký khám'
                        : record?.status === 10
                        ? 'Đã thanh toán'
                        : record?.status === 11
                        ? 'Đã khám'
                        : record?.status === 13
                        ? 'Đã huỷ'
                        : '   ';
                    txtCopy += '    ';
                    txtCopy +=record?.appointmentHisId ?  record?.appointmentHisId + '    '  : "";
                    txtCopy +=moment(record?.appointmentDate).format('DD/MM/YYYY') ?  moment(record?.appointmentDate).format('DD/MM/YYYY') + '    ' : "";
                    txtCopy +=record?.workTimeName ?  record?.workTimeName + '    ' : "";
                    txtCopy +=record?.patientName ?  record?.patientName + '     ' : "";
                    txtCopy +=record?.address ?  record?.address + '    ' : "";
                    txtCopy +=record?.phone ?  record?.phone + '       ' : "";
                    txtCopy +=moment(record?.patientBirthDate).format('DD/MM/YYYY') ?  moment(record?.patientBirthDate).format('DD/MM/YYYY') + '    ' : "";
                    txtCopy +=record?.examFacilityName ?  record?.examFacilityName + '    ' : "";
                    txtCopy +=record?.assignStaffName ?  record?.assignStaffName + '     ' : "";
                    txtCopy +=record?.assignStaffPhone ?  record?.assignStaffPhone + '    ' : "";
                    txtCopy +=record?.patientLabel ?  record?.patientLabel + '    ' : "";
                    txtCopy +=moment(record?.createDate).format('DD/MM/YYYY HH:mm') ?  moment(record?.createDate).format('DD/MM/YYYY HH:mm') + '    ' : "";
                    txtCopy +=record?.createUser ?  record?.createUser + '    ' : "";
                    txtCopy +=record?.channelType ?  record?.channelType + '    ' : "";
                    txtCopy +=record?.countryName ?  record?.countryName + '    ': "";
                    txtCopy +=record?.patientIdNo ? record?.patientIdNo + '    ' : "";
                    txtCopy +=record?.appointmentNote ? record?.appointmentNote + '    ' : "";
                    navigator.clipboard.writeText(txtCopy);
                  }}
                />
              </div>
            );
          }}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          width={100}
          title="Mã lịch hẹn"
          render={(_, { appointmentHisId }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">{appointmentHisId}</Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="Ngày đặt lịch"
          width={90}
          render={(_, { appointmentDate }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <span style={{ fontWeight: 500 }}>{moment(appointmentDate).format('DD/MM/YYYY')}</span>
            </Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          width={120}
          align="center"
          title="Khung giờ"
          render={(_, { workTimeName }) => <Tooltip title="Click đúp chuột để xem chi tiết">{workTimeName}</Tooltip>}
        />
        <Table.Column<AppoimentCovidTable>
          title="Tên khách hàng"
          width={180}
          render={(_, { patientName }) => <Tooltip title="Click đúp chuột để xem chi tiết">{patientName}</Tooltip>}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="Địa chỉ"
          width={180}
          render={(_, { address }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              {
                <div
                  style={{
                    textAlign: 'left',
                    width: '100%'
                  }}
                >
                  {address}
                </div>
              }
            </Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="Số điện thoại"
          width={100}
          render={(_, { phone }) => <Tooltip title="Click đúp chuột để xem chi tiết">{phone}</Tooltip>}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="Ngày sinh"
          width={90}
          render={(_, { patientBirthDate }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">{moment(patientBirthDate).format('DD/MM/YYYY')}</Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          title="Nơi khám"
          width={150}
          render={(_, { examFacilityName }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              {examFacilityName?.length >= 40 ? examFacilityName.substr(0, 40) + '...' : examFacilityName}
            </Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          title="CBTN"
          width={150}
          render={(_, { assignStaffName }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              {assignStaffName?.length >= 40 ? assignStaffName.substr(0, 40) + '...' : assignStaffName}
            </Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          title="SĐT CBTN"
          width={100}
          render={(_, { assignStaffPhone }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              {assignStaffPhone?.length >= 40 ? assignStaffPhone.substr(0, 40) + '...' : assignStaffPhone}
            </Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          width={100}
          title="KBYT"
          align="center"
          render={(_, { patientLabel }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              {patientLabel ? (
                <Tag color={patientLabel == 'Xanh' ? 'green' : patientLabel == 'Vàng' ? 'yellow' : 'red'}>
                  {patientLabel}
                </Tag>
              ) : (
                ''
              )}
            </Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="Ngày tạo lịch"
          width={150}
          render={(_, { createDate }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">{moment(createDate).format('DD/MM/YYYY HH:mm')}</Tooltip>
          )}
        />
        <Table.Column<AppoimentCovidTable>
          title="Người tạo"
          width={100}
          render={(_, { createUser }) => <Tooltip title="Click đúp chuột để xem chi tiết">{createUser}</Tooltip>}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="Nguồn"
          width={100}
          render={(_, { channelType }) => <Tooltip title="Click đúp chuột để xem chi tiết">{channelType}</Tooltip>}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="Quốc tịch"
          width={120}
          render={(_, { countryName }) => <Tooltip title="Click đúp chuột để xem chi tiết">{countryName}</Tooltip>}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="CMT/CCCD/HC"
          width={120}
          render={(_, { patientIdNo }) => <Tooltip title="Click đúp chuột để xem chi tiết">{patientIdNo}</Tooltip>}
        />
        <Table.Column<AppoimentCovidTable>
          align="center"
          title="Ghi chú"
          width={260}
          render={(_, { appointmentNote }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">{appointmentNote}</Tooltip>
          )}
        />
      </Table>
    </>
  );
};
export default AppointmentCovidTable;
