import { Button, DatePicker, Form, FormProps, Input, Row, Select } from 'antd';
import { Channel } from 'common/interfaces/channel.interface';
import React, { FC, useEffect } from 'react';
import { ColProps } from 'antd/lib/col';
import { useAppState } from 'helpers';
import Col from 'antd/es/grid/col';
import { Country } from 'common/interfaces/country.interface';
import { APPOINTMENT_STATUS } from 'constants/appointment.constants';
import { ClearOutlined, SearchOutlined } from '@ant-design/icons';
import { functionCodeConstants } from 'constants/functions';
const { Option } = Select;
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 6,
  xl: 6,
  xxl: 6
};
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};
interface AppointmentCovidScheduleFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  /** Initial form data */
  values?: FormData;
  functionOb?: any;
}
interface FormData {
  appointmentType?: string;
  code?: string;
  dateType?: string;
  examType?: string;
  fromDate?: moment.Moment;
  info?: string;
  infoType?: string;
  methodType?: string;
  status?: string;
  toDate?: moment.Moment;
  workTimeId?: any;
  channel?: any;
  nationality?: any;
}
export default function useGetAppointmentCovidForm({
  responsive = false,
  name = 'searchForm',
  functionOb,
  values
}: AppointmentCovidScheduleFormProps) {
  const [formInstance] = Form.useForm<FormData>();
  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={values}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };
  useEffect(() => {
    formInstance.resetFields();
  }, [formInstance]);

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  interface DateTypeProps {
    onChange: (value: any) => void;
  }
  const DateType: FC<DateTypeProps> = ({ onChange }) => {
    function handleChange(value: any) {
      onChange(value);
    }
    const dateType = (
      <Form.Item className="appoiment-cpn" name="dateType" label="Lọc theo ngày">
        <Select onChange={handleChange} defaultValue="appointmentDate">
          <Option value="appointmentDate">Ngày đặt lịch</Option>
          <Option value="createDate">Ngày tạo lịch</Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{dateType}</Col> : dateType;
  };

  const FromDate: FC = () => {
    const fromDate = (
      <Form.Item className="appoiment-cpn" name="fromDate" label="Từ ngày">
        <DatePicker format="DD/MM/YYYY" placeholder="Chọn Từ ngày" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{fromDate}</Col> : fromDate;
  };

  const ToDate: FC = () => {
    const toDate = (
      <Form.Item className="appoiment-cpn" name="toDate" label="Đến ngày">
        <DatePicker format="DD/MM/YYYY" placeholder="Chọn Đến ngày" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{toDate}</Col> : toDate;
  };

  const Info: FC = () => {
    const info = (
      <Form.Item className="appoiment-cpn" name="info" label="Giá trị tìm kiếm">
        <Input placeholder="Nhập thông tin tìm kiếm" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{info}</Col> : info;
  };

  const InfoType: FC = () => {
    const infoType = (
      <Form.Item className="appoiment-cpn" name="infoType" label="Tìm kiếm theo">
        <Select defaultValue="phone">
          <Option value="patientName">Tên khách hàng</Option>
          <Option value="phone">Số điện thoại</Option>
          <Option value="appointmentHisId">Mã lịch hẹn</Option>
          <Option value="patientIdNo">CMT/CCCD</Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{infoType}</Col> : infoType;
  };

  const PackageType: FC = () => {
    const packageType = (
      <Form.Item className="appoiment-cpn" name="examType" label="Dịch vụ">
        <Select defaultValue="">
          <Option key="all" value="">
            -- Tất cả --
          </Option>
          <Option value="896">Test Nhanh</Option>
          <Option value="895">Xét nghiệm PCR</Option>
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{packageType}</Col> : packageType;
  };

  interface WorkTimeIdProps {
    appointmentWorkTime: any;
  }

  const WorkTimeId: FC<WorkTimeIdProps> = ({ appointmentWorkTime }) => {
    const workTimeId = (
      <Form.Item className="appoiment-cpn" name="workTimeId" label="Khung giờ">
        <Select
          style={{ width: '100%' }}
          showSearch
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          placeholder="-- Chọn khung giờ --"
        >
          {appointmentWorkTime === undefined
            ? []
            : appointmentWorkTime?.map((option, index) => (
                <Select.Option key={option['name']} value={option['id']}>
                  {option['name']}
                </Select.Option>
              ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{workTimeId}</Col> : workTimeId;
  };

  interface NationalityProps {
    countries: Country[];
    isLoading: boolean;
  }
  const Nationality: FC<NationalityProps> = ({ countries, isLoading }) => {
    const nationality = (
      <Form.Item className="appoiment-cpn" name="nationality" label="Quốc tịch">
        <Select
          loading={isLoading}
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          placeholder="-- Chọn Quốc tịch --"
        >
          {countries?.map(country => (
            <Select.Option key={country.id} value={country.countryCode}>
              {country.countryName}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{nationality}</Col> : nationality;
  };

  const Status: FC = () => {
    const status = (
      <Form.Item name="status" className="appoiment-cpn" label="Trạng thái">
        <Select defaultValue="" onChange={value => {}}>
          <Option key="all" value="">
            -- Tất cả trạng thái --
          </Option>
          <Option key={APPOINTMENT_STATUS.CHO_XAC_NHAN.key} value={APPOINTMENT_STATUS.CHO_XAC_NHAN.key}>
            {APPOINTMENT_STATUS.CHO_XAC_NHAN.value}
          </Option>
          <Option key={APPOINTMENT_STATUS.DA_XAC_NHAN.key} value={APPOINTMENT_STATUS.DA_XAC_NHAN.key}>
            {APPOINTMENT_STATUS.DA_XAC_NHAN.value}
          </Option>
          <Option key={APPOINTMENT_STATUS.CHO_TU_VAN_DAU_VAO.key} value={APPOINTMENT_STATUS.CHO_TU_VAN_DAU_VAO.key}>
            {APPOINTMENT_STATUS.CHO_TU_VAN_DAU_VAO.value}
          </Option>
          <Option key={APPOINTMENT_STATUS.DA_TU_VAN_DAU_VAO.key} value={APPOINTMENT_STATUS.DA_TU_VAN_DAU_VAO.key}>
            {APPOINTMENT_STATUS.DA_TU_VAN_DAU_VAO.value}
          </Option>
          <Option key={APPOINTMENT_STATUS.CHUA_CHECK.key} value={APPOINTMENT_STATUS.CHUA_CHECK.key}>
            {APPOINTMENT_STATUS.CHUA_CHECK.value}
          </Option>
          <Option key={APPOINTMENT_STATUS.DA_CHECK.key} value={APPOINTMENT_STATUS.DA_CHECK.key}>
            {APPOINTMENT_STATUS.DA_CHECK.value}
          </Option>
          <Option key={APPOINTMENT_STATUS.DA_LAY_MAU.key} value={APPOINTMENT_STATUS.DA_LAY_MAU.key}>
            {APPOINTMENT_STATUS.DA_LAY_MAU.value}
          </Option>
          <Option key={APPOINTMENT_STATUS.DA_CO_KQ.key} value={APPOINTMENT_STATUS.DA_CO_KQ.key}>
            {APPOINTMENT_STATUS.DA_CO_KQ.value}
          </Option>
          <Option key={APPOINTMENT_STATUS.DA_DANG_KY_KHAM.key} value={APPOINTMENT_STATUS.DA_DANG_KY_KHAM.key}>
            {APPOINTMENT_STATUS.DA_DANG_KY_KHAM.value}
          </Option>
          <Option key={APPOINTMENT_STATUS.DA_THANH_TOAN.key} value={APPOINTMENT_STATUS.DA_THANH_TOAN.key}>
            {APPOINTMENT_STATUS.DA_THANH_TOAN.value}
          </Option>
          <Option key={APPOINTMENT_STATUS.DA_KHAM.key} value={APPOINTMENT_STATUS.DA_KHAM.key}>
            {APPOINTMENT_STATUS.DA_KHAM.value}
          </Option>
          <Option key={APPOINTMENT_STATUS.DA_HUY.key} value={APPOINTMENT_STATUS.DA_HUY.key}>
            {APPOINTMENT_STATUS.DA_HUY.value}
          </Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{status}</Col> : status;
  };

  //Nguon lich hen
  interface AppointmentSourceProps {
    channelList: Channel[];
  }
  const AppointmentSource: FC<AppointmentSourceProps> = ({ channelList }) => {
    // console.log('channelList : ', channelList);
    const appointmentSource = (
      <Form.Item className="appoiment-cpn" name="channel" label="Nguồn lịch hẹn">
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          defaultValue=""
        >
          <Option key="all" value="">
            -- Tất cả --
          </Option>
          {channelList?.map(channel => (
            <Select.Option key={channel.id} value={channel.code}>
              {channel.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentSource}</Col> : appointmentSource;
  };

  //Buttons
  interface ButtonsProps {
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          onClick={() => {
            onSearch();
          }}
          icon={<SearchOutlined />}
          type="primary"
          disabled={functionOb[functionCodeConstants.TD_LH_TIMKIEM] ? false : true}
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button
          onClick={resetFields}
          icon={<ClearOutlined />}
          disabled={functionOb[functionCodeConstants.TD_LH_BOLOC] ? false : true}
        >
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };

  return React.useMemo(
    () => ({
      form: formInstance,
      Form: WrappedForm,
      DateType,
      FromDate,
      ToDate,
      Info,
      InfoType,
      PackageType,
      Nationality,
      Status,
      AppointmentSource,
      Buttons,
      WorkTimeId
    }),
    []
  );
}
