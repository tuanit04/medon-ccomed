import { FormInstance, FormProps } from 'antd';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { useAppState } from 'helpers';
import { useGetAppointments } from 'hooks/appointment';
import { useGetAllCountriesLazy } from 'hooks/appointment/useGetAllCountries';
import { useGetChannels } from 'hooks/channel';
import { useGetAllWorktimes } from 'hooks/worktimes/useGetWorkTimes';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import AppointmentCovidPageSearch from './AppointmentCovidPageSearch';
import AppointmentCovidTable from './AppointmentCovidTable';
import './index.less';

interface AppointmentCovidPageProps {}

const AppointmentCovidPage: FC<AppointmentCovidPageProps> = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(50);
  const [form, setForm] = useState({} as FormInstance<FormProps<any>>);
  const functionOb = useAppState(state => state.user.functionObject);
  //Lấy nguồn lịch hẹn
  const { channels, isLoadingChannels, loadChannels } = useGetChannels();
  //Lấy khung giờ
  const { worktimes, isLoadingWorktimes, loadWorktimes } = useGetAllWorktimes();
  //Hook Lấy danh sách quốc tịch
  const { pagingCountries, isLoadingCountries, loadCountries, errorCountries } = useGetAllCountriesLazy();

  useEffect(() => {
    loadChannels();
    loadWorktimes({
      variables: {
        type: 'STAFF'
      }
    });
    loadCountries();
  }, []);

  return (
    <div className="main-content">
      <AppointmentCovidPageSearch
        functionOb={functionOb}
        channels={channels}
        worktimes={worktimes}
        countries={pagingCountries?.data}
        isLoadingCountries={isLoadingCountries}
        onSetFormInstance={(value: FormInstance<FormProps<any>>) => {
          if (value) {
            var filter: any = [];
            if (value['channel']) {
              var item: any = {
                id: 'channelType',
                operation: '==',
                value: value['channel']
              };
              filter.push(item);
            }
            if (value['dateType']) {
              var item: any = {
                id: value['dateType'] + '',
                operation: 'between',
                value:
                  moment(value['fromDate']).format('DD/MM/YYYY') + ',' + moment(value['toDate']).format('DD/MM/YYYY')
              };
              filter.push(item);
            }
            if (value['status']) {
              var item: any = {
                id: 'status',
                operation: '==',
                value: value['status']
              };
              filter.push(item);
            }
            if (value['nationality']) {
              var item: any = {
                id: 'nationality',
                operation: '==',
                value: value['nationality']
              };
              filter.push(item);
            }
            if (value['infoType'] && value['info']) {
              var item: any = {
                id: value['infoType'],
                operation: '~',
                value: value['info']
              };
              filter.push(item);
            }
            if (value['examType']) {
              var item: any = {
                id: 'examType',
                operation: '==',
                value: value['examType']
              };
              filter.push(item);
            }
            console.log(filter);
          }
          setForm(filter);
        }}
        onSearch={value => {
          console.log(value);
        }}
      />
      <AppointmentCovidTable valueSearch={form} functionOb={functionOb} />
    </div>
  );
};
export default AppointmentCovidPage;
