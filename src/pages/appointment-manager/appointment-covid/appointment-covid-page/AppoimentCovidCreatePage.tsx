import { Button, Col, Form, Row, Spin, Tabs } from 'antd';
import { apiSaveBookingCovidEmployee } from 'api/appoiment-covid-home/appoimentCovid';
import { Appointment } from 'common/interfaces/appointment.interface';
import { AppointmentCovid } from 'common/interfaces/appointmentCovide.interface';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { PatienCovid } from 'common/interfaces/patientCovid.interface';
import { useGetAllCountriesLazy } from 'hooks/appointment/useGetAllCountries';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { useGetFacilitys } from 'hooks/facility';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetRoutesLazy } from 'hooks/route';
import { useGetSiteStaffsLazy } from 'hooks/site-staff';
import { useGetStreetsLazy } from 'hooks/street';
import { useGetAllWorktimes } from 'hooks/worktimes/useGetWorkTimes';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import { openNotificationRight } from 'utils/notification';
import useGetAddAppointmentCovidForm from '../useGetAddAppoimentCovidForm';

const TabPane = Tabs.TabPane;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};

interface AppoimentCovidCreatePageProps {}

const AppoimentCovidCreatePage: FC<AppoimentCovidCreatePageProps> = () => {
  const [txtTime, setTxtTime] = useState('');
  const [appointmentCache, setAppointmentCache]: any = useState();
  const [isInvoice, setIsInvoice] = useState(false);
  const [typeInvoice, setTypeInvoice] = useState('0');
  const [appointmentDate, setAppointmentDate]: any = useState(new Date());
  const [tabs, setTabs] = useState(1);
  const [tabActive, setTabActive] = useState('TAB0');
  const [isCovided, setIsCovided] = useState(false);
  const [packageIdTmp, setPackageIdTmp] = useState('895');
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [isNote, setIsNote] = useState(false);
  const [patternIdNo, setPatternIdNo] = useState(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
  const [locationAll, setLoactionAll] = useState('');
  //
  const [listPatient, setListPatient]: any[] = useState([]);

  //
  const defaultItem: PatienCovid = {
    patientName: null,
    preDoctorHisId: null,
    patientBirthDate: '',
    patientSex: null,
    address: null,
    phone: null,
    provinceId: null,
    districtId: null,
    provinceCode: null,
    districtCode: null,
    patientIdNo: null,
    issuedDate: null,
    issuedPlace: null,
    nationality: 'VN',
    countryName: 'VIỆT NAM',
    reasonNote: null,
    appointmentObject: null,
    guardianName: null,
    guardianPhone: null,
    status: 2,
    patientBirtYear: null,
    healthDeclaration: {
      typeCustomerKBYT: '6',
      dateOnset: null,
      nationPassOther: null,
      prognostic: '1',
      location: null,
      streetHasCovid: '0',
      humanHasCovid: '0',
      testedCovid: '0',
      invoice: null,
      addressProfile: null,
      nameCompany: null,
      emailProfile: null,
      taxCode: null,
      addressCompany: null,
      printName: null,
      typeInvoice: '0',
      customerLabel: null,
      emailCompany: null,
      hasCovid: '2',
      dateHascovid: null,
      hasVaccine: '2'
    },
    confirm: ['1']
  };

  //lay danh sach khun gio
  const { worktimes, isLoadingWorktimes, loadWorktimes } = useGetAllWorktimes();
  //Hook Lấy danh sách quốc tịch
  const { pagingCountries, isLoadingCountries, loadCountries, errorCountries } = useGetAllCountriesLazy();
  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //Hook Lấy danh sách đường phố
  const { streets, isLoadingStreets, loadStreets } = useGetStreetsLazy();
  //Hook lấy chi tiết cung đường
  const { loadRoutes, isLoadingRoutes, routesData } = useGetRoutesLazy();
  //Hook Lấy danh sách Quận/Huyện
  const { districts, isLoadingDistricts, loadDistricts } = useGetDistricts();
  //Hook Lấy danh sách cán bộ TN
  const { siteStaffs, isLoadingSiteStaffs, loadSiteStaffs } = useGetSiteStaffsLazy();

  //
  const [formInstance] = Form.useForm<Partial<AppointmentCovid>>();
  useEffect(() => {
    var list: any[] = [];
    var item = defaultItem;
    list.push(item);
    formInstance.setFieldsValue({
      patients: list,
      appointmentDate: moment(new Date())
    });
    // if (localStorage.getItem('appoiment-covid-create-data')) {
    //   let appointment: Partial<Appointment> = JSON.parse(localStorage.getItem('appoiment-covid-create-data') || '{}');
    //   console.log('appointment covid >', appointment);
    //   /*setAppointmentCache({
    //     ...appointment,
    //     appointmentDate: moment(appointment?.appointmentDate),
    //     patient: {
    //       ...appointment?.patient,
    //       birthDate: moment(appointment?.patient?.birthDate),
    //       birthYear: moment(appointment?.patient?.birthYear)
    //     }
    //   });*/
    //   formInstance.setFieldsValue({
    //     ...appointment,
    //     patients: list,
    //     appointmentDate: moment(new Date())
    //   });
    //   if (appointment?.patient?.provinceId) {
    //     loadDistricts({
    //       variables: {
    //         provinceId: appointment?.patient?.provinceId
    //       }
    //     });
    //   }
    //   if (appointment?.patient?.districtId) {
    //     /*loadWards({
    //       variables: {
    //         districtId: appointment?.patient?.districtId
    //       }
    //     });*/
    //   }
    //   if (appointment?.examFacilityId) {
    //     /*loadAllWorkTimeFacility({
    //       variables: {
    //         facilityId: appointment?.examFacilityId
    //       }
    //     });*/
    //   }
    // }
  }, []);

  const handleSaveData = async () => {
    const values = await form.getFieldsValue(true);
    localStorage.setItem('appoiment-covid-create-data', JSON.stringify(values));
  };

  //kiem tra 24h

  const check24h = () => {
    if (appointmentDate) {
      if (new Date(appointmentDate) <= new Date()) {
        return true;
      } else if (appointmentDate && txtTime) {
        var listTG = txtTime.split('-');
        if (listTG) {
          var time = listTG[0].replace('h', ':');
          var appointmentDateT =
            new Date(appointmentDate).getFullYear() +
            '/' +
            (new Date(appointmentDate).getMonth() + 1) +
            '/' +
            new Date(appointmentDate).getDate();
          var date = new Date(appointmentDateT + ' ' + time);
          var hour = Math.abs(date.getTime() - new Date().getTime()) / 36e5;
          if (hour < 24) {
            return true;
          }
        }
      }
    }
    return false;
  };

  useEffect(() => {
    check24h();
  }, [appointmentDate, txtTime]);

  useEffect(() => {
    if (routesData) {
      form.setFieldsValue({
        samplingRouteId: routesData[0]?.id,
        samplingRouteName: routesData[0]?.name,
        examFacilityId: routesData[0]?.facilityId ? routesData[0]?.facilityId : routesData[0]?.parentFacilityId,
        examFacilityName: routesData[0]?.parentFacilityName,
        facilityName: routesData[0]?.facilityName
      });
      if (routesData[0]?.facilityCode) {
        setLoactionAll(routesData[0]?.facilityCode);
      } else {
        setLoactionAll('');
      }
    }
    return () => {
      //handleSaveData();
    };
  }, [routesData]);

  const {
    // Form,
    form,
    AppointmentCode,
    AppointmentStatus,
    PhoneNumber,
    CustomerFullname,
    Gender,
    Nationality,
    ContactAddress,
    Provinces,
    Districts,
    TestNotes,
    IdentityCard,
    AppointmentDate,
    AppointmentWorkTime,
    Route,
    Street4Home,
    DateOnset,
    Prognostic,
    StreetHasCovid,
    NationPassOther,
    TestedCovid,
    HumanHasCovid,
    Invoice,
    AddressProfile,
    EmailProfile,
    NameCompany,
    TaxCode,
    AddressCompany,
    EmailCompany,
    PrintName,
    SamplingDistricts,
    SamplingProvinces,
    SamplingAddress,
    IsCovid,
    TowInjections,
    GuardianPhone,
    GuardianName,
    IssuedPlace,
    IssuedDate,
    ReasonCheckup,
    StaffCode,
    PackageType,
    PackageId,
    ProvincesAll,
    Facility,
    Address,
    DateofBirth,
    TypeCustomer,
    PID,
    Confirm,
    SiteStaffId
  } = useGetAddAppointmentCovidForm({
    name: 'add-appointment-form',
    responsive: true,
    appointmentInput: {
      covidServiceId: '895'
    },
    formInstance: formInstance,
    patternIdNo
  });
  //Hook Lấy danh sách nơi khám tại nhà
  const {
    facilities: facilitiesHome,
    isLoadingFacilities: isLoadingFacilitiesHome,
    loadFacilities: loadFacilitiesHome
  } = useGetFacilitys();

  useEffect(() => {
    loadWorktimes({
      variables: {
        type: 'STAFF'
      }
    });
    loadCountries();
    loadProvinces();
    loadFacilitiesHome({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'isHome',
            value: '1',
            operation: '=='
          },
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
    formInstance.setFieldsValue({
      covidServiceId: '895'
    });
    let filtered: FilteredInput[] = [];
    filtered.push({
      id: 'status',
      value: '1',
      operation: '=='
    });
    loadSiteStaffs({
      variables: {
        filtered: filtered
      }
    });
  }, []);

  const onSubmit = async isForward => {
    var listErr = formInstance.getFieldsError();
    var isErr = false;
    if (listErr && listErr.length > 0) {
      for (var i = 0; i < listErr.length; i++) {
        if (listErr[i]['errors'] && listErr[i]['errors'].length > 0) {
          var itemErr = listErr[i]['name'];
          if (itemErr && itemErr[0] == 'patients' && itemErr.length >= 3) {
            var listItem1 = formInstance.getFieldValue('patients');
            if (listItem1 && listItem1.length > 0 && (itemErr[1] || itemErr[1] == 0)) {
              var item = listItem1[itemErr[1]];
              if (item.nationality) {
                if (String(item.nationality).toUpperCase() === 'VN') {
                  setPatternIdNo(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
                } else {
                  setPatternIdNo(/^[a-zA-Z0-9_.-]{6,13}$/ as RegExp);
                }
              }
              if (item?.reasonNote == 'Khác') {
                setIsNote(true);
              } else {
                setIsNote(false);
              }
              var health = item['healthDeclaration'];
              setIsCovided(health?.hasCovid == '1' || health?.hasCovid == '3' ? true : false);
              setTypeInvoice(item['typeInvoice']);
              if (health['invoice'] && health['invoice'].length > 0) {
                setIsInvoice(true);
              } else {
                setIsInvoice(false);
              }
              if (item && item.provinceId) {
                loadDistricts({
                  variables: {
                    provinceId: item.provinceId
                  }
                });
              } else {
                loadDistricts({
                  variables: {
                    provinceId: 'svsv'
                  }
                });
              }
            }
            isErr = true;
            setTabActive('TAB' + itemErr[1]);
            break;
          }
        }
      }
    }
    if (!isErr) {
      const values = await formInstance.validateFields();
      setIsLoading(true);
      values.channel = 'CCOMED';
      values.workTimeName = txtTime;
      values.appointmentDate = moment(values.appointmentDate).format('YYYY-MM-DD');
      if (values.examType == '1') {
        values.examType = '0';
      }
      const arrSplit = values.samplingStreetId?.split('***');
      if (arrSplit) values.samplingStreetId = arrSplit[0];
      if (values) {
        var listItem = values.patients;
        var location = locationAll;
        if (!location) {
          location = getCodeFactility(values.examFacilityId);
        }

        if (listItem && listItem.length > 0) {
          listItem.map(item => {
            if (check24h()) {
              item.healthDeclaration.location = location;
              item.healthDeclaration.invoice = item.healthDeclaration?.invoice?.length > 0 ? 1 : '';
              item.healthDeclaration.printName =
                item.healthDeclaration?.typeInvoice == '1' && item.healthDeclaration?.printName?.length > 0 ? '' : 1;
              item.healthDeclaration.prognostic = item.healthDeclaration?.prognostic?.toString();
            } else {
              item.healthDeclaration = {
                typeCustomerKBYT: null,
                dateOnset: null,
                nationPassOther: null,
                prognostic: null,
                location: null,
                streetHasCovid: null,
                humanHasCovid: null,
                testedCovid: null,
                invoice: null,
                addressProfile: null,
                nameCompany: null,
                emailProfile: null,
                taxCode: null,
                addressCompany: null,
                printName: null,
                typeInvoice: null,
                customerLabel: null,
                emailCompany: null,
                hasCovid: null,
                dateHascovid: null,
                hasVaccine: null
              };
            }
          });
        }
        values.patients = listItem;
      }

      const result = await apiSaveBookingCovidEmployee(values)
        .then(res => {
          if (res?.data.status == 1) {
            setIsLoading(false);
            openNotificationRight('Lưu lịch hẹn tại nhà thành công!');
            var listReset: any[] = [];
            var itemReset = defaultItem;
            listReset.push(itemReset);
            formInstance.setFieldsValue({
              patients: listReset,
              appointmentDate: moment(new Date())
            });
            localStorage.removeItem('appoiment-covid-create-data');
            navigate('/appointment-management/appointment-covid');
          } else {
            setIsLoading(false);
            openNotificationRight('Lỗi : ' + res?.data?.message, 'error');
          }
        })
        .catch(err => {
          openNotificationRight('Có lỗi xảy ra vui lòng thử lại sau!', 'error');
          setIsLoading(false);
        });

      setIsLoading(false);
    }
  };

  const getCodeFactility = examFacilityId => {
    if (facilitiesHome.length > 0 && examFacilityId) {
      for (var i = 0; i < facilitiesHome.length; i++) {
        if (facilitiesHome[i].id == examFacilityId) {
          return facilitiesHome[i].code;
        }
      }
    }
    return null;
  };

  return (
    <div className="main-content">
      <Spin spinning={isLoading} tip="Đang xử lý...">
        <Form {...formItemLayout} className="new-add-appointment-form" form={formInstance}>
          <Col className="row hidden-over" span={8}>
            <Row>
              <AppointmentDate setAppointmentDateP={setAppointmentDate} />
              <PackageId setTabs={setTabs} onChange={setPackageIdTmp} setTabActive={setTabActive} />
              <AppointmentWorkTime appointmentWorkTime={worktimes} isLoading={isLoadingWorktimes} setTxt={setTxtTime} />
              <PackageType
                packageIdTmp={packageIdTmp}
                onChange={setTabs}
                defaultItem={defaultItem}
                setTabActive={setTabActive}
              />
              <ProvincesAll
                isProvince={false}
                provinceList={provinces}
                isLoading={isLoadingProvince}
                handleChange={value => {
                  loadStreets({
                    variables: {
                      page: 1,
                      filtered: [
                        {
                          id: 'provinceId',
                          value: value,
                          operation: '=='
                        },
                        {
                          id: 'status',
                          value: '1',
                          operation: '=='
                        }
                      ]
                    }
                  });
                  if (value) {
                    loadDistricts({
                      variables: {
                        provinceId: value
                      }
                    });
                  }
                }}
              />
              <Facility />
              <Street4Home
                isLoading={isLoadingStreets}
                handleChangeStreet={(routeId, appointmentDate) => {
                  if (routeId !== '') {
                    loadRoutes({
                      variables: {
                        filtered: [
                          {
                            id: 'id',
                            value: routeId,
                            operation: '=='
                          },
                          {
                            id: 'status',
                            value: '1',
                            operation: '=='
                          }
                        ]
                      }
                    });
                  } else {
                    //
                  }
                }}
                streetList={streets}
              />
              <Route routeNameDefault={form.getFieldValue('streetId') ? true : false} />
              {/* <SiteStaffId listStaff={siteStaffs} /> */}
              {tabs > 1 && (
                <>
                  <div className="title-address">Thông tin địa điểm lấy mẫu tại nhà</div>
                  <Address />
                  <ProvincesAll
                    isProvince={true}
                    provinceList={provinces}
                    isLoading={isLoadingProvince}
                    handleChange={value => {
                      loadStreets({
                        variables: {
                          page: 1,
                          filtered: [
                            {
                              id: 'provinceId',
                              value: value,
                              operation: '=='
                            },
                            {
                              id: 'status',
                              value: '1',
                              operation: '=='
                            }
                          ]
                        }
                      });
                      if (value) {
                        loadDistricts({
                          variables: {
                            provinceId: value
                          }
                        });
                      }
                    }}
                  />
                  <SamplingDistricts districtList={districts} isLoading={isLoadingDistricts} />
                </>
              )}
              <Form.List name="patients">
                {(fields, { add, remove }) => (
                  <Tabs
                    defaultActiveKey={tabActive}
                    activeKey={tabActive}
                    onTabClick={activeKey => {
                      setTabActive(activeKey);
                      var index = activeKey.replace('TAB', '');
                      var listItem = formInstance.getFieldValue('patients');
                      if (listItem && listItem.length > 0 && index) {
                        var item = listItem[index];
                        if (item.nationality) {
                          if (String(item.nationality).toUpperCase() === 'VN') {
                            setPatternIdNo(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
                          } else {
                            setPatternIdNo(/^[a-zA-Z0-9_.-]{6,13}$/ as RegExp);
                          }
                        }
                        if (item?.reasonNote == 'Khác') {
                          setIsNote(true);
                        } else {
                          setIsNote(false);
                        }
                        var health = item['healthDeclaration'];
                        setIsCovided(health?.hasCovid == '1' || health?.hasCovid == '3' ? true : false);
                        setTypeInvoice(item['typeInvoice']);
                        if (health['invoice'] && health['invoice'].length > 0) {
                          setIsInvoice(true);
                        } else {
                          setIsInvoice(false);
                        }
                        if (item && item.provinceId) {
                          loadDistricts({
                            variables: {
                              provinceId: item.provinceId
                            }
                          });
                        } else {
                          loadDistricts({
                            variables: {
                              provinceId: 'svsv'
                            }
                          });
                        }
                      }
                    }}
                  >
                    {fields.map((field, index) => (
                      <TabPane forceRender={true} key={'TAB' + index} tab={<span>{'KH' + (1 + index)}</span>}>
                        <Row>
                          <AppointmentStatus field={field} />
                          <AppointmentCode field={field} />
                          <PhoneNumber
                            field={field}
                            handleChangePatient={() => {}}
                            handleChangeProvince={provinceId => {
                              loadStreets({
                                variables: {
                                  page: 1,
                                  filtered: [
                                    {
                                      id: 'provinceId',
                                      value: provinceId,
                                      operation: '=='
                                    },
                                    {
                                      id: 'status',
                                      value: '1',
                                      operation: '=='
                                    }
                                  ]
                                }
                              });
                              if (provinceId) {
                                loadDistricts({
                                  variables: {
                                    provinceId: provinceId
                                  }
                                });
                              }
                            }}
                            provinces={provinces}
                          />
                          <PID field={field} />
                          <CustomerFullname field={field} />
                          <Nationality
                            field={field}
                            countries={pagingCountries?.data}
                            isLoading={isLoadingCountries}
                            setPatternIdNo={setPatternIdNo}
                          />
                          <DateofBirth field={field} />
                          <Gender field={field} />
                          <Provinces
                            field={field}
                            isLoading={isLoadingProvince}
                            provinceList={provinces}
                            handleChangeProvince={value => {
                              loadStreets({
                                variables: {
                                  page: 1,
                                  filtered: [
                                    {
                                      id: 'provinceId',
                                      value: value,
                                      operation: '=='
                                    },
                                    {
                                      id: 'status',
                                      value: '1',
                                      operation: '=='
                                    }
                                  ]
                                }
                              });
                              if (value) {
                                loadDistricts({
                                  variables: {
                                    provinceId: value
                                  }
                                });
                              }
                            }}
                          />
                          <Districts
                            field={field}
                            districtList={districts}
                            isLoading={isLoadingDistricts}
                            handleChangeDistrict={() => {}}
                          />
                          <ContactAddress field={field} />
                          <StaffCode field={field} />
                          <ReasonCheckup field={field} setIsNote={setIsNote} />
                          <IdentityCard field={field} />
                          <IssuedDate field={field} />
                          <IssuedPlace field={field} />
                          <GuardianName field={field} />
                          <GuardianPhone field={field} />
                          <TestNotes field={field} required={isNote} />
                          {check24h() && (
                            <>
                              <div className="kbyt-title">Khai báo y tế dành cho khách hàng đăng ký khám</div>
                              <div className="kbyt-sub-title">
                                <p>* Phiếu khai báo thông tin y tế chỉ có giá trị trong vòng 24 giờ </p>
                                <p>
                                  * Cảnh báo: Khai báo thông tin sai là vi phạm pháp luật Việt Nam, có thể bị xử lý hình
                                  sự!
                                </p>
                              </div>
                              <TypeCustomer field={field} />
                              <DateOnset field={field} />
                              <Prognostic field={field} />
                              {/* <StreetHasCovid field={field} />
                                                        <NationPassOther field={field} /> */}
                              <div className="">
                                <span className="red">* </span>
                                <b>Mục B. Yếu tố dịch tễ nếu có</b>
                              </div>
                              <HumanHasCovid field={field} />
                              <StreetHasCovid field={field} />
                              <TestedCovid field={field} />
                              <div className="">
                                <span className="red">* </span>
                                <b>Mục C. CÁC THÔNG TIN KHÁC: Hãy tích vào ô trống trước phần lựa chọn</b>
                              </div>
                              <IsCovid isCovided={isCovided} setIsCovided={setIsCovided} field={field} />
                              <TowInjections field={field} />
                              <Confirm field={field} isDisplaySubmit={false} />
                              {/* <Invoice field={field} setIsInvoice={setIsInvoice} />
                                                            {isInvoice && (
                                                                <Tabs
                                                                    defaultActiveKey={typeInvoice}
                                                                    activeKey={typeInvoice}
                                                                    type="card"
                                                                    onTabClick={activeKey => {
                                                                        setTypeInvoice(activeKey);
                                                                        var listItem = formInstance.getFieldValue('patients');
                                                                        var item = listItem[index];
                                                                        var health = item['healthDeclaration'];
                                                                        if (typeInvoice == '0') {
                                                                            health = {
                                                                                ...health,
                                                                                printName: undefined,
                                                                                nameCompany: undefined,
                                                                                addressCompany: undefined,
                                                                                taxCode: undefined,
                                                                                emailCompany: undefined,
                                                                                typeInvoice: '0'
                                                                            };
                                                                        } else {
                                                                            health = {
                                                                                ...health,
                                                                                emailProfile: undefined,
                                                                                addressProfile: undefined,
                                                                                typeInvoice: '1'
                                                                            };
                                                                        }
                                                                        item.healthDeclaration = health;
                                                                        listItem[index] = item;
                                                                        console.log(listItem);
                                                                        formInstance.setFieldsValue({
                                                                            patients: listItem
                                                                        });
                                                                    }}
                                                                >
                                                                    <TabPane tab={<span>Cá nhân</span>} key="0" forceRender={true}>
                                                                        <Row>
                                                                            <AddressProfile
                                                                                field={field}
                                                                                required={
                                                                                    formInstance.getFieldValue([field.name, 'healthDeclaration', 'typeInvoice']) ==
                                                                                        '0'
                                                                                        ? true
                                                                                        : false
                                                                                }
                                                                            />
                                                                            <EmailProfile
                                                                                field={field}
                                                                                required={
                                                                                    formInstance.getFieldValue([field.name, 'healthDeclaration', 'typeInvoice']) ==
                                                                                        '0'
                                                                                        ? true
                                                                                        : false
                                                                                }
                                                                            />
                                                                        </Row>
                                                                    </TabPane>
                                                                    <TabPane tab={<span>Công ty</span>} key="1" forceRender={true}>
                                                                        <Row>
                                                                            <NameCompany
                                                                                field={field}
                                                                                required={
                                                                                    formInstance.getFieldValue([field.name, 'healthDeclaration', 'typeInvoice']) ==
                                                                                        '1'
                                                                                        ? true
                                                                                        : false
                                                                                }
                                                                            />
                                                                            <TaxCode
                                                                                field={field}
                                                                                required={
                                                                                    formInstance.getFieldValue([field.name, 'healthDeclaration', 'typeInvoice']) ==
                                                                                        '1'
                                                                                        ? true
                                                                                        : false
                                                                                }
                                                                            />
                                                                            <AddressCompany
                                                                                field={field}
                                                                                required={
                                                                                    formInstance.getFieldValue([field.name, 'healthDeclaration', 'typeInvoice']) ==
                                                                                        '1'
                                                                                        ? true
                                                                                        : false
                                                                                }
                                                                            />
                                                                            <EmailCompany
                                                                                field={field}
                                                                                required={
                                                                                    formInstance.getFieldValue([field.name, 'healthDeclaration', 'typeInvoice']) ==
                                                                                        '1'
                                                                                        ? true
                                                                                        : false
                                                                                }
                                                                            />
                                                                            <PrintName field={field} />
                                                                        </Row>
                                                                    </TabPane>
                                                                </Tabs>
                                                            )} */}
                            </>
                          )}
                        </Row>
                      </TabPane>
                    ))}
                  </Tabs>
                )}
              </Form.List>
            </Row>
            <Button
              className="btn-green"
              disabled={isLoading}
              onClick={() => {
                onSubmit(false);
              }}
            >
              Lưu thông tin
            </Button>
            <Button
              style={{
                marginLeft: 10
              }}
              onClick={() => {
                var listReset: any[] = [];
                var itemReset = defaultItem;
                listReset.push(itemReset);
                formInstance.setFieldsValue({
                  patients: listReset,
                  appointmentDate: moment(new Date())
                });
                localStorage.removeItem('appoiment-covid-create-data');
                localStorage.removeItem('IS_SHOW_CREATE_APPOINTMENT_COVID_DIALOG');
                navigate('/appointment-management/appointment-covid');
              }}
            >
              Quay lại
            </Button>
          </Col>
          <Col className="row hidden-over" span={16}></Col>
        </Form>
      </Spin>
    </div>
  );
};

export default AppoimentCovidCreatePage;
