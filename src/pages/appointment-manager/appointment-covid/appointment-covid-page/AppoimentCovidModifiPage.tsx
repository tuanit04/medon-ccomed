import { UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Col, Form, Row, Spin, Tabs } from 'antd';
import {
  apiGetAppoimentCovidByGroupId,
  apiSaveBookingCovidEmployee,
  apiUpdateAppoumentCovid
} from 'api/appoiment-covid-home/appoimentCovid';
import Buttons from 'common/components/Dialer/Buttons';
import { Appointment } from 'common/interfaces/appointment.interface';
import { AppointmentCovid } from 'common/interfaces/appointmentCovide.interface';
import { AppointmentInput } from 'common/interfaces/input/appointmentInput.interfce';
import { PatienCovid } from 'common/interfaces/patientCovid.interface';
import { useGetAllCountriesLazy } from 'hooks/appointment/useGetAllCountries';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { useGetFacilitys } from 'hooks/facility';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetRoutesLazy } from 'hooks/route';
import { useGetStreetsLazy } from 'hooks/street';
import { useGetAllWorktimes } from 'hooks/worktimes/useGetWorkTimes';
import moment from 'moment';
import React, { useCallback, useEffect, useState } from 'react';
import { FC } from 'react';
import { openNotificationRight } from 'utils/notification';
import useGetAddAppointmentCovidForm from '../useGetAddAppoimentCovidForm';
import { useNavigate, useParams } from 'react-router-dom';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { useGetSiteStaffsLazy } from 'hooks/site-staff';

const TabPane = Tabs.TabPane;
const CheckboxGroup = Checkbox.Group;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};

interface AppoimentCovidModifiPageProps {}

const AppoimentCovidModifiPage: FC<AppoimentCovidModifiPageProps> = () => {
  const [txtTime, setTxtTime] = useState('');
  const [isInvoice, setIsInvoice] = useState(false);
  const [typeInvoice, setTypeInvoice] = useState('0');
  const [appointmentDate, setAppointmentDate]: any = useState(new Date());
  const [tabs, setTabs] = useState(1);
  const [tabActive, setTabActive] = useState('TAB0');
  const [isCovided, setIsCovided] = useState(false);
  const params: any = useParams();
  const [packageIdTmp, setPackageIdTmp] = useState('895');
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [isDisplaySubmit, setIsDisplaySubmit] = useState(false);
  const [isNote, setIsNote] = useState(false);
  const [patternIdNo, setPatternIdNo] = useState(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
  const [locationAll, setLoactionAll] = useState('');
  //
  const [listPatient, setListPatient]: any[] = useState([]);

  //
  const defaultItem: PatienCovid = {
    patientName: null,
    preDoctorHisId: null,
    patientBirthDate: '',
    patientSex: null,
    address: null,
    phone: null,
    provinceId: null,
    districtId: null,
    provinceCode: null,
    districtCode: null,
    patientIdNo: null,
    issuedDate: null,
    issuedPlace: null,
    nationality: 'VN',
    countryName: 'VIỆT NAM',
    reasonNote: null,
    appointmentObject: null,
    guardianName: null,
    guardianPhone: null,
    status: 0,
    appointmentHisId: null,
    patientLabel: '',
    healthDeclaration: {
      typeCustomerKBYT: '6',
      dateOnset: null,
      nationPassOther: null,
      prognostic: '1',
      location: null,
      streetHasCovid: '0',
      humanHasCovid: '0',
      testedCovid: '0',
      invoice: null,
      addressProfile: null,
      nameCompany: null,
      emailProfile: null,
      taxCode: null,
      addressCompany: null,
      printName: null,
      typeInvoice: '0',
      customerLabel: null,
      emailCompany: null,
      hasCovid: '2',
      dateHascovid: null,
      hasVaccine: '2'
    },
    confirm: ['1']
  };

  //lay danh sach khun gio
  const { worktimes, isLoadingWorktimes, loadWorktimes } = useGetAllWorktimes();
  //Hook Lấy danh sách quốc tịch
  const { pagingCountries, isLoadingCountries, loadCountries, errorCountries } = useGetAllCountriesLazy();
  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //Hook Lấy danh sách đường phố
  const { streets, isLoadingStreets, loadStreets } = useGetStreetsLazy();
  //Hook lấy chi tiết cung đường
  const { loadRoutes, isLoadingRoutes, routesData } = useGetRoutesLazy();
  //Hook Lấy danh sách Quận/Huyện
  const { districts, isLoadingDistricts, loadDistricts } = useGetDistricts();
  //Hook Lấy danh sách cán bộ TN
  const { siteStaffs, isLoadingSiteStaffs, loadSiteStaffs } = useGetSiteStaffsLazy();
  //
  const [formInstance] = Form.useForm<Partial<AppointmentCovid>>();
  useEffect(() => {
    var list: any[] = [];
    var item = defaultItem;
    list.push(item);
    formInstance.setFieldsValue({
      patients: list
    });
    formInstance.setFieldsValue({
      covidServiceId: '895'
    });
  }, []);
  //kiem tra 24h
  const check24h = () => {
    if (appointmentDate) {
      setIsDisplaySubmit(false);
      if (new Date(appointmentDate) <= new Date()) {
        return true;
      } else if (appointmentDate && txtTime) {
        var listTG = txtTime.split('-');
        if (listTG) {
          var time = listTG[0].replace('h', ':');
          var appointmentDateT =
            new Date(appointmentDate).getFullYear() +
            '/' +
            (new Date(appointmentDate).getMonth() + 1) +
            '/' +
            new Date(appointmentDate).getDate();
          var date = new Date(appointmentDateT + ' ' + time);
          var hour = Math.abs(date.getTime() - new Date().getTime()) / 36e5;
          if (hour < 24) {
            return true;
          }
        }
      }
    }
    return false;
  };

  useEffect(() => {
    if (check24h()) {
      var listItem = formInstance.getFieldValue('patients');
      var listTmp = [...listItem];
      listTmp?.map(item => {
        item.healthDeclaration = {
          typeCustomerKBYT: '6',
          dateOnset: null,
          nationPassOther: null,
          prognostic: null,
          location: null,
          streetHasCovid: null,
          humanHasCovid: null,
          testedCovid: null,
          invoice: null,
          addressProfile: null,
          nameCompany: null,
          emailProfile: null,
          taxCode: null,
          addressCompany: null,
          printName: null,
          typeInvoice: '0',
          customerLabel: null,
          emailCompany: null,
          hasCovid: null,
          dateHascovid: '',
          hasVaccine: null
        };
      });
      form.setFieldsValue({
        patients: listTmp
      });
    }
  }, [appointmentDate, txtTime]);

  useEffect(() => {
    if (routesData) {
      form.setFieldsValue({
        samplingRouteId: routesData[0]?.id,
        samplingRouteName: routesData[0]?.name,
        examFacilityId: routesData[0]?.facilityId ? routesData[0]?.facilityId : routesData[0]?.parentFacilityId,
        examFacilityName: routesData[0]?.parentFacilityName,
        facilityName: routesData[0]?.facilityName
      });
      if (routesData[0]?.facilityCode) {
        setLoactionAll(routesData[0]?.facilityCode);
      } else {
        setLoactionAll('');
      }
    }
  }, [routesData]);

  const {
    // Form,
    form,
    AppointmentCode,
    AppointmentStatus,
    PhoneNumber,
    CustomerFullname,
    Gender,
    Nationality,
    ContactAddress,
    Provinces,
    Districts,
    TestNotes,
    IdentityCard,
    AppointmentDate,
    AppointmentWorkTime,
    Route,
    Street4Home,
    DateOnset,
    Prognostic,
    StreetHasCovid,
    NationPassOther,
    TestedCovid,
    HumanHasCovid,
    Invoice,
    AddressProfile,
    EmailProfile,
    NameCompany,
    TaxCode,
    AddressCompany,
    EmailCompany,
    PrintName,
    SamplingDistricts,
    SamplingProvinces,
    SamplingAddress,
    IsCovid,
    TowInjections,
    GuardianPhone,
    GuardianName,
    IssuedPlace,
    IssuedDate,
    ReasonCheckup,
    StaffCode,
    PackageType,
    PackageId,
    ProvincesAll,
    Facility,
    Address,
    DateofBirth,
    TypeCustomer,
    PID,
    Confirm,
    SiteStaffId
  } = useGetAddAppointmentCovidForm({
    name: 'add-appointment-form',
    responsive: true,
    appointmentInput: {
      covidServiceId: '895'
    },
    formInstance: formInstance,
    patternIdNo: patternIdNo
  });
  //Hook Lấy danh sách nơi khám tại nhà
  const {
    facilities: facilitiesHome,
    isLoadingFacilities: isLoadingFacilitiesHome,
    loadFacilities: loadFacilitiesHome
  } = useGetFacilitys();

  useEffect(() => {
    if (params && params.groupId) {
      getDetailByGroupID(params.groupId);
    }
    let filtered: FilteredInput[] = [];
    filtered.push({
      id: 'status',
      value: '1',
      operation: '=='
    });
    loadSiteStaffs({
      variables: {
        filtered: filtered
      }
    });
  }, []);

  const getDetailByGroupID = useCallback(async groupId => {
    var idActive = params?.id;
    const result: any = await apiGetAppoimentCovidByGroupId(groupId, '');
    if (result?.data.status == 1) {
      var item = result?.data.data;
      if (item) {
        item.appointmentDate = moment(item.appointmentDate);
        setAppointmentDate(moment(item.appointmentDate).format('YYYY/MM/DD'));
        setTxtTime(item.workTimeName);
        if (item.samplingStreetId && item.samplingRouteId) {
          item.samplingStreetId = item.samplingStreetId + '***' + item.samplingRouteId;
          loadRoutes({
            variables: {
              filtered: [
                {
                  id: 'id',
                  value: item.samplingRouteId,
                  operation: '=='
                },
                {
                  id: 'status',
                  value: '1',
                  operation: '=='
                }
              ]
            }
          });
        }
        if (item.samplingProvinceId) {
          loadStreets({
            variables: {
              page: 1,
              filtered: [
                {
                  id: 'provinceId',
                  value: item.samplingProvinceId,
                  operation: '=='
                },
                {
                  id: 'status',
                  value: '1',
                  operation: '=='
                }
              ]
            }
          });
          if (item.samplingProvinceId) {
            loadDistricts({
              variables: {
                provinceId: item.samplingProvinceId
              }
            });
          }
        }

        if (item.examType == '0') {
          item.examType = '1';
        }
        setTabs(Number(item.examType));
        setPackageIdTmp(item.covidServiceId);
        var listPatients = item.patients;
        listPatients?.map((itemP, index) => {
          if (itemP.patientLabel && itemP.patientLabel != '') {
            console.log('setIsDisplaySubmit');
            setIsDisplaySubmit(true);
          }
          if (itemP.id == idActive) {
            if (String(itemP.nationality).toUpperCase() === 'VN') {
              setPatternIdNo(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
            } else {
              setPatternIdNo(/^[a-zA-Z0-9_.-]{6,13}$/ as RegExp);
            }
            if (itemP?.reasonNote == 'Khác') {
              setIsNote(true);
            } else {
              setIsNote(false);
            }
            var health = itemP['healthDeclaration'];
            setIsCovided(health?.hasCovid == '1' || health?.hasCovid == '3' ? true : false);
            setTypeInvoice(itemP['typeInvoice']);
            if (itemP.healthDeclaration && itemP.healthDeclaration.invoice == '1') {
              setIsInvoice(true);
            } else {
              setIsInvoice(false);
            }
            setIsCovided(
              itemP?.healthDeclaration?.hasCovid == '1' || itemP?.healthDeclaration?.hasCovid == '3' ? true : false
            );
            if (itemP && itemP.provinceId) {
              loadDistricts({
                variables: {
                  provinceId: itemP.provinceId
                }
              });
            } else {
              loadDistricts({
                variables: {
                  provinceId: 'svsv'
                }
              });
            }
            if (itemP.healthDeclaration && itemP.healthDeclaration.typeInvoice) {
              setTypeInvoice(itemP.healthDeclaration.typeInvoice);
            }
            setTabActive('TAB' + index);
          }
          itemP.patientBirthDate = moment(itemP.patientBirthDate);
          if (itemP.issuedDate) itemP.issuedDate = moment(itemP.issuedDate);
          if (itemP.healthDeclaration) {
            if (itemP.healthDeclaration.invoice == '1') {
              itemP.healthDeclaration.invoice = ['1'];
            }
            if (itemP.healthDeclaration.prognostic) {
              itemP.healthDeclaration.prognostic = itemP.healthDeclaration.prognostic.split(',');
            }
            if (itemP.healthDeclaration.dateHascovid) {
              itemP.healthDeclaration.dateHascovid = moment(itemP.healthDeclaration.dateHascovid);
            }
            if (itemP.healthDeclaration.dateOnset) {
              itemP.healthDeclaration.dateOnset = moment(itemP.healthDeclaration.dateOnset);
            }
            if (itemP.healthDeclaration.testedCovid || itemP.healthDeclaration.testedCovid == 0) {
              itemP.healthDeclaration.testedCovid = itemP.healthDeclaration.testedCovid?.toString();
            }
            if (itemP.healthDeclaration.streetHasCovid || itemP.healthDeclaration.streetHasCovid == 0) {
              itemP.healthDeclaration.streetHasCovid = itemP.healthDeclaration.streetHasCovid?.toString();
            }
            if (itemP.healthDeclaration.humanHasCovid || itemP.healthDeclaration.humanHasCovid == 0) {
              itemP.healthDeclaration.humanHasCovid = itemP.healthDeclaration.humanHasCovid?.toString();
            }
            if (itemP.healthDeclaration.nationPassOther || itemP.healthDeclaration.nationPassOther == 0) {
              itemP.healthDeclaration.nationPassOther = itemP.healthDeclaration.nationPassOther?.toString();
            }
            if (itemP.healthDeclaration.streetHasCovid || itemP.healthDeclaration.streetHasCovid == 0) {
              itemP.healthDeclaration.streetHasCovid = itemP.healthDeclaration.streetHasCovid?.toString();
            }
            if (itemP.healthDeclaration.hasVaccine || itemP.healthDeclaration.hasVaccine == 0) {
              itemP.healthDeclaration.hasVaccine = itemP.healthDeclaration.hasVaccine?.toString();
            }
            if (itemP.healthDeclaration.typeCustomerKBYT || itemP.healthDeclaration.typeCustomerKBYT == 0) {
              itemP.healthDeclaration.typeCustomerKBYT = itemP.healthDeclaration.typeCustomerKBYT?.toString();
            }
            itemP.confirm = ['1'];
          }
        });
        item.patients = listPatients;
        console.log('item', item);
        formInstance.setFieldsValue({
          ...item
        });
      }
    } else {
      openNotificationRight('Lỗi : ' + result?.data?.message);
    }
  }, []);

  useEffect(() => {
    loadWorktimes({
      variables: {
        type: 'STAFF'
      }
    });
    loadCountries();
    loadProvinces();
    loadFacilitiesHome({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'isHome',
            value: '1',
            operation: '=='
          },
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
  }, []);

  const onSubmit = async isForward => {
    var listErr = formInstance.getFieldsError();
    var isErr = false;
    if (listErr && listErr.length > 0) {
      for (var i = 0; i < listErr.length; i++) {
        if (listErr[i]['errors'] && listErr[i]['errors'].length > 0) {
          var itemErr = listErr[i]['name'];

          if (itemErr && itemErr[0] == 'patients' && itemErr.length >= 3) {
            var listItem1 = formInstance.getFieldValue('patients');
            console.log('test', itemErr[1]);
            if (listItem1 && listItem1.length > 0 && (itemErr[1] || itemErr[1] == 0)) {
              var item = listItem1[itemErr[1]];
              if (item.nationality) {
                if (String(item.nationality).toUpperCase() === 'VN') {
                  setPatternIdNo(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
                } else {
                  setPatternIdNo(/^[a-zA-Z0-9_.-]{6,13}$/ as RegExp);
                }
              }
              if (item?.reasonNote == 'Khác') {
                setIsNote(true);
              } else {
                setIsNote(false);
              }
              var health = item['healthDeclaration'];
              console.log('health', health);
              setIsCovided(health?.hasCovid == '1' || health?.hasCovid == '3' ? true : false);
              setTypeInvoice(item['typeInvoice']);
              if (health['invoice'] && health['invoice'].length > 0) {
                setIsInvoice(true);
              } else {
                setIsInvoice(false);
              }
              if (item && item.provinceId) {
                loadDistricts({
                  variables: {
                    provinceId: item.provinceId
                  }
                });
              } else {
                loadDistricts({
                  variables: {
                    provinceId: 'svsv'
                  }
                });
              }
            }
            isErr = true;
            setTabActive('TAB' + itemErr[1]);
            break;
          }
        }
      }
    }
    if (!isErr) {
      const values = await formInstance.validateFields();
      setIsLoading(true);
      values.channel = 'CCOMED';
      values.workTimeName = txtTime;
      values.appointmentDate = moment(values.appointmentDate).format('YYYY-MM-DD');
      if (values.examType == '1') {
        values.examType = '0';
      }
      const arrSplit = values.samplingStreetId?.split('***');
      if (arrSplit) values.samplingStreetId = arrSplit[0];

      if (values) {
        var listItem = values.patients;
        var location = locationAll;
        if (!location) {
          location = getCodeFactility(values.examFacilityId);
        }
        if (listItem && listItem.length > 0 ) {
          listItem.map(item => {
            if (check24h()) {
              item.healthDeclaration.location = location;
              item.healthDeclaration.invoice = item.healthDeclaration?.invoice?.length > 0 ? 1 : '';
              item.healthDeclaration.printName =
                item.healthDeclaration?.typeInvoice == '1' && item.healthDeclaration?.printName?.length > 0 ? '' : 1;
              item.healthDeclaration.prognostic = item.healthDeclaration?.prognostic?.toString();
            } else {
              console.log("check24h",check24h());
              item.healthDeclaration = {
                typeCustomerKBYT: null,
                dateOnset: null,
                nationPassOther: null,
                prognostic: null,
                location: null,
                streetHasCovid: null,
                humanHasCovid: null,
                testedCovid: null,
                invoice: null,
                addressProfile: null,
                nameCompany: null,
                emailProfile: null,
                taxCode: null,
                addressCompany: null,
                printName: null,
                typeInvoice: null,
                customerLabel: null,
                emailCompany: null,
                hasCovid: null,
                dateHascovid: null,
                hasVaccine: null
              };
            }
          });
        }
        values.patients = listItem;
      }
      values.groupId = params.groupId;

      const result = await apiUpdateAppoumentCovid(values)
        .then(res => {
          if (res?.data.status == 1) {
            openNotificationRight('Xác nhận lịch hẹn tại nhà thành công!');
            // window.location.href = "/appointment-management/appointment-covid";
            navigate('/appointment-management/appointment-covid');
            setIsLoading(false);
          } else {
            openNotificationRight('Lỗi : ' + res?.data?.message);
            setIsLoading(false);
          }
        })
        .catch(err => {
          openNotificationRight('Có lỗi xảy ra vui lòng thử lại sau!', 'error');
          setIsLoading(false);
        });
      setIsLoading(false);
    }
  };

  const getCodeFactility = examFacilityId => {
    if (facilitiesHome.length > 0 && examFacilityId) {
      for (var i = 0; i < facilitiesHome.length; i++) {
        if (facilitiesHome[i].id == examFacilityId) {
          return facilitiesHome[i].code;
          break;
        }
      }
    }
    return null;
  };

  return (
    <div className="main-content">
      <Spin spinning={isLoading} tip="Đang xử lý...">
        <Form {...formItemLayout} className="new-add-appointment-form" form={formInstance}>
          <Col className="row hidden-over" span={8}>
            <Row>
              <AppointmentDate setAppointmentDateP={setAppointmentDate} isDisabled={isDisplaySubmit} />
              <PackageId
                setTabs={setTabs}
                onChange={setPackageIdTmp}
                isDisabled={isDisplaySubmit}
                setTabActive={setTabActive}
              />
              <AppointmentWorkTime
                isEdit={true}
                appointmentWorkTime={worktimes}
                isLoading={isLoadingWorktimes}
                setTxt={setTxtTime}
                isDisabled={isDisplaySubmit}
              />
              <PackageType
                packageIdTmp={packageIdTmp}
                onChange={setTabs}
                defaultItem={defaultItem}
                isDisabled={isDisplaySubmit}
                setTabActive={setTabActive}
              />
              <ProvincesAll
                isProvince={false}
                provinceList={provinces}
                isLoading={isLoadingProvince}
                isDisabled={isDisplaySubmit}
                handleChange={value => {
                  loadStreets({
                    variables: {
                      page: 1,
                      filtered: [
                        {
                          id: 'provinceId',
                          value: value,
                          operation: '=='
                        },
                        {
                          id: 'status',
                          value: '1',
                          operation: '=='
                        }
                      ]
                    }
                  });
                  if (value) {
                    loadDistricts({
                      variables: {
                        provinceId: value
                      }
                    });
                  }
                }}
              />
              <Facility />
              <Street4Home
                isDisabled={isDisplaySubmit}
                isLoading={isLoadingStreets}
                handleChangeStreet={(routeId, appointmentDate) => {
                  if (routeId !== '') {
                    loadRoutes({
                      variables: {
                        filtered: [
                          {
                            id: 'id',
                            value: routeId,
                            operation: '=='
                          },
                          {
                            id: 'status',
                            value: '1',
                            operation: '=='
                          }
                        ]
                      }
                    });
                  } else {
                    //
                  }
                }}
                streetList={streets}
              />
              <Route routeNameDefault={form.getFieldValue('streetId') ? true : false} />
              {/* <SiteStaffId listStaff={siteStaffs} /> */}
              {tabs > 1 && (
                <>
                  <div className="title-address">Thông tin địa điểm lấy mẫu tại nhà</div>
                  <Address isDisabled={isDisplaySubmit} />
                  <ProvincesAll
                    isDisabled={isDisplaySubmit}
                    isProvince={true}
                    provinceList={provinces}
                    isLoading={isLoadingProvince}
                    handleChange={value => {
                      loadStreets({
                        variables: {
                          page: 1,
                          filtered: [
                            {
                              id: 'provinceId',
                              value: value,
                              operation: '=='
                            },
                            {
                              id: 'status',
                              value: '1',
                              operation: '=='
                            }
                          ]
                        }
                      });
                      if (value) {
                        loadDistricts({
                          variables: {
                            provinceId: value
                          }
                        });
                      }
                    }}
                  />
                  <SamplingDistricts
                    districtList={districts}
                    isLoading={isLoadingDistricts}
                    isDisabled={isDisplaySubmit}
                  />
                </>
              )}
              <Form.List name="patients">
                {(fields, { add, remove }) => (
                  <Tabs
                    defaultActiveKey={tabActive}
                    activeKey={tabActive}
                    onTabClick={activeKey => {
                      setTabActive(activeKey);
                      var index = activeKey.replace('TAB', '');
                      var listItem = formInstance.getFieldValue('patients');
                      if (listItem && listItem.length > 0 && index) {
                        var item = listItem[index];
                        if (item.nationality) {
                          if (String(item.nationality).toUpperCase() === 'VN') {
                            setPatternIdNo(/^(?:[A-Za-z0-9]{8}|[A-Za-z0-9]{9}|[A-Za-z0-9]{12})$/ as RegExp);
                          } else {
                            setPatternIdNo(/^[a-zA-Z0-9_.-]{6,13}$/ as RegExp);
                          }
                        }

                        if (item?.reasonNote == 'Khác') {
                          setIsNote(true);
                        } else {
                          setIsNote(false);
                        }
                        var health = item['healthDeclaration'];
                        setIsCovided(health?.hasCovid == '1' || health?.hasCovid == '3' ? true : false);
                        setTypeInvoice(item['typeInvoice']);
                        if (health['invoice'] && health['invoice'].length > 0) {
                          setIsInvoice(true);
                        } else {
                          setIsInvoice(false);
                        }
                        if (item && item.provinceId) {
                          loadDistricts({
                            variables: {
                              provinceId: item.provinceId
                            }
                          });
                        } else {
                          loadDistricts({
                            variables: {
                              provinceId: 'svsv'
                            }
                          });
                        }
                      }
                    }}
                  >
                    {fields.map((field, index) => (
                      <TabPane
                        forceRender={true}
                        key={'TAB' + index}
                        tab={
                          // <span>
                          //     {"KH" + (1 + index)}
                          // </span>
                          <span
                            className={
                              formInstance.getFieldValue(['patients', field.name, 'patientLabel']) == 'Xanh'
                                ? 'green-kbyt'
                                : formInstance.getFieldValue(['patients', field.name, 'patientLabel']) == 'Đỏ'
                                ? 'red-kbyt'
                                : formInstance.getFieldValue(['patients', field.name, 'patientLabel']) == 'Vàng'
                                ? 'yellow-kbyt'
                                : 'blue-kbyt'
                            }
                          >
                            {'KH' + (1 + index)}
                          </span>
                        }
                      >
                        {console.log(formInstance.getFieldValue(['patients', field.name, 'patientLabel']))}
                        <Row>
                          <AppointmentStatus field={field} />
                          <AppointmentCode field={field} />
                          <PhoneNumber
                            isDisabled={isDisplaySubmit}
                            field={field}
                            handleChangePatient={() => {}}
                            isEdit={true}
                            handleChangeProvince={provinceId => {
                              loadStreets({
                                variables: {
                                  page: 1,
                                  filtered: [
                                    {
                                      id: 'provinceId',
                                      value: provinceId,
                                      operation: '=='
                                    },
                                    {
                                      id: 'status',
                                      value: '1',
                                      operation: '=='
                                    }
                                  ]
                                }
                              });
                              if (provinceId) {
                                loadDistricts({
                                  variables: {
                                    provinceId: provinceId
                                  }
                                });
                              }
                            }}
                            provinces={provinces}
                          />
                          <PID field={field} />
                          <CustomerFullname field={field} isDisabled={isDisplaySubmit} />
                          <Nationality
                            field={field}
                            countries={pagingCountries?.data}
                            isLoading={isLoadingCountries}
                            isDisabled={isDisplaySubmit}
                            setPatternIdNo={setPatternIdNo}
                          />
                          <DateofBirth field={field} isDisabled={isDisplaySubmit} />
                          <Gender field={field} isDisabled={isDisplaySubmit} />
                          <Provinces
                            field={field}
                            isLoading={isLoadingProvince}
                            provinceList={provinces}
                            handleChangeProvince={value => {
                              if (value) {
                                loadDistricts({
                                  variables: {
                                    provinceId: value
                                  }
                                });
                              }
                            }}
                          />
                          <Districts
                            field={field}
                            districtList={districts}
                            isLoading={isLoadingDistricts}
                            handleChangeDistrict={() => {}}
                            isDisabled={isDisplaySubmit}
                          />
                          <ContactAddress field={field} isDisabled={isDisplaySubmit} />
                          <StaffCode field={field} isDisabled={isDisplaySubmit} />
                          <ReasonCheckup setIsNote={setIsNote} field={field} isDisabled={isDisplaySubmit} />
                          <IdentityCard field={field} isDisabled={isDisplaySubmit} />
                          <IssuedDate field={field} isDisabled={isDisplaySubmit} />
                          <IssuedPlace field={field} isDisabled={isDisplaySubmit} />
                          <GuardianName field={field} isDisabled={isDisplaySubmit} />
                          <GuardianPhone field={field} isDisabled={isDisplaySubmit} />
                          <TestNotes field={field} isDisabled={isDisplaySubmit} required={isNote} />
                          {check24h() && (
                            <>
                              <div className="kbyt-title">Khai báo y tế dành cho khách hàng đăng ký khám</div>
                              <div className="kbyt-sub-title">
                                <p>* Phiếu khai báo thông tin y tế chỉ có giá trị trong vòng 24 giờ </p>
                                <p>
                                  * Cảnh báo: Khai báo thông tin sai là vi phạm pháp luật Việt Nam, có thể bị xử lý hình
                                  sự!
                                </p>
                              </div>
                              <TypeCustomer field={field} isDisabled={isDisplaySubmit} />
                              <DateOnset field={field} isDisabled={isDisplaySubmit} />
                              <Prognostic field={field} isDisabled={isDisplaySubmit} />
                              {/* <StreetHasCovid field={field} />
                                                        <NationPassOther field={field} /> */}
                              <div className="">
                                <span className="red">* </span>
                                <b>Mục B. Yếu tố dịch tễ nếu có</b>
                              </div>
                              <HumanHasCovid field={field} isDisabled={isDisplaySubmit} />
                              <StreetHasCovid field={field} isDisabled={isDisplaySubmit} />
                              <TestedCovid field={field} isDisabled={isDisplaySubmit} />
                              <div className="">
                                <span className="red">* </span>
                                <b>Mục C. CÁC THÔNG TIN KHÁC: Hãy tích vào ô trống trước phần lựa chọn</b>
                              </div>
                              <IsCovid
                                isCovided={isCovided}
                                setIsCovided={setIsCovided}
                                field={field}
                                isDisabled={isDisplaySubmit}
                              />
                              <TowInjections field={field} isDisabled={isDisplaySubmit} />
                              <Confirm field={field} isDisplaySubmit={isDisplaySubmit} />
                              {/* <Invoice field={field} setIsInvoice={setIsInvoice} isDisabled={isDisplaySubmit} />
                                                            {
                                                                isInvoice &&
                                                                <Tabs
                                                                    defaultActiveKey={typeInvoice}
                                                                    activeKey={typeInvoice}
                                                                    type="card"
                                                                    onTabClick={activeKey => {
                                                                        setTypeInvoice(activeKey);
                                                                        var listItem = formInstance.getFieldValue("patients");
                                                                        var item = listItem[index];
                                                                        var health = item["healthDeclaration"];
                                                                        setIsCovided(health?.hasCovid == "1" || health?.hasCovid == "2" ? true : false);
                                                                        if (typeInvoice == "0") {
                                                                            health = {
                                                                                ...health,
                                                                                printName: undefined,
                                                                                nameCompany: undefined,
                                                                                addressCompany: undefined,
                                                                                taxCode: undefined,
                                                                                emailCompany: undefined,
                                                                                typeInvoice: "0"
                                                                            }
                                                                        } else {
                                                                            health = {
                                                                                ...health,
                                                                                emailProfile: undefined,
                                                                                addressProfile: undefined,
                                                                                typeInvoice: "1"
                                                                            }
                                                                        }
                                                                        item.healthDeclaration = health;
                                                                        listItem[index] = item;
                                                                        console.log(listItem);
                                                                        formInstance.setFieldsValue({
                                                                            patients: listItem
                                                                        })
                                                                    }}
                                                                >
                                                                    <TabPane
                                                                        tab={
                                                                            <span>
                                                                                Cá nhân
                                                                            </span>
                                                                        }
                                                                        key="0"
                                                                        forceRender={true}
                                                                    >
                                                                        <Row>
                                                                            <AddressProfile field={field} required={formInstance.getFieldValue([field.name, "healthDeclaration", "typeInvoice"]) == "0" ? true : false} isDisabled={isDisplaySubmit} />
                                                                            <EmailProfile field={field} required={formInstance.getFieldValue([field.name, "healthDeclaration", "typeInvoice"]) == "0" ? true : false} isDisabled={isDisplaySubmit} />
                                                                        </Row>
                                                                    </TabPane>
                                                                    <TabPane
                                                                        tab={
                                                                            <span>
                                                                                Công ty
                                                                            </span>
                                                                        }
                                                                        key="1"
                                                                        forceRender={true}
                                                                    >
                                                                        <Row>
                                                                            <NameCompany field={field} required={formInstance.getFieldValue([field.name, "healthDeclaration", "typeInvoice"]) == "1" ? true : false} isDisabled={isDisplaySubmit} />
                                                                            <TaxCode field={field} required={formInstance.getFieldValue([field.name, "healthDeclaration", "typeInvoice"]) == "1" ? true : false} isDisabled={isDisplaySubmit} />
                                                                            <AddressCompany field={field} required={formInstance.getFieldValue([field.name, "healthDeclaration", "typeInvoice"]) == "1" ? true : false} isDisabled={isDisplaySubmit} />
                                                                            <EmailCompany field={field} required={formInstance.getFieldValue([field.name, "healthDeclaration", "typeInvoice"]) == "1" ? true : false} isDisabled={isDisplaySubmit} />
                                                                            <PrintName field={field} isDisabled={isDisplaySubmit} />
                                                                        </Row>
                                                                    </TabPane>
                                                                </Tabs>
                                                            } */}
                            </>
                          )}
                        </Row>
                      </TabPane>
                    ))}
                  </Tabs>
                )}
              </Form.List>
            </Row>
            {!isDisplaySubmit && (
              <Button
                className="btn-green"
                disabled={isLoading}
                onClick={() => {
                  onSubmit(false);
                  console.log('test');
                }}
              >
                Lưu thông tin
              </Button>
            )}
            <Button
              style={{
                marginLeft: 10
              }}
              onClick={() => {
                navigate('/appointment-management/appointment-covid');
              }}
            >
              Quay lại
            </Button>
          </Col>
          <Col className="row hidden-over" span={16}></Col>
        </Form>
      </Spin>
    </div>
  );
};

export default AppoimentCovidModifiPage;
