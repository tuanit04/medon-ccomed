import { ClearOutlined, SearchOutlined } from "@ant-design/icons";
import { Button, Form } from "antd";
import { functionCodeConstants } from "constants/functions";
import moment from "moment";
import React from "react";
import { FC } from "react";
import useGetAppointmentCovidForm from "./useGetAppointmentCovidForm";

interface AppointmentCovidPageSearchProps {
    functionOb: any;
    channels;
    onSetFormInstance: (value) => void;
    worktimes;
    onSearch: ({ }) => void;
    countries;
    isLoadingCountries: boolean;
}

const AppointmentCovidPageSearch: FC<AppointmentCovidPageSearchProps> = ({
    functionOb,
    channels,
    onSetFormInstance,
    onSearch,
    worktimes,
    countries,
    isLoadingCountries
}) => {

    const {
        Form,
        form,
        DateType,
        FromDate,
        ToDate,
        Info,
        InfoType,
        PackageType,
        Nationality,
        Status,
        AppointmentSource,
        Buttons,
        WorkTimeId,

    } = useGetAppointmentCovidForm({
        name: 'searchForm',
        responsive: true,
        functionOb: functionOb,
        values: {
            fromDate: moment(),
            toDate: moment(),
            dateType: "appointmentDate",
            infoType: "phone",
            examType: "",
            workTimeId: undefined,
            channel: undefined,
            nationality: undefined,
            status: undefined
        }
    })

    const onsubmit = async () => {
        const value = await form.validateFields();
        onSetFormInstance(value);
    }

    return (
        <Form >
            <DateType
                onChange={value => {
                    if (value === '') {
                        form.resetFields();
                    }
                }}
            />
            <FromDate />
            <ToDate />
            <Status />
            <InfoType />
            <Info />
            {/* <PackageType />
            <AppointmentSource channelList={channels} />
            <WorkTimeId appointmentWorkTime={worktimes} />
            <Nationality countries={countries} isLoading={isLoadingCountries} /> */}
            <Button disabled={functionOb[functionCodeConstants.TD_LH_TIMKIEM_COVID] ? false : true} type="primary" icon={<SearchOutlined />} onClick={onsubmit}>Tìm kiếm</Button>
            &nbsp;&nbsp;
            <Button
                disabled={functionOb[functionCodeConstants.TD_LH_BOLOC_COVID] ? false : true}
                onClick={() => {
                    form.setFieldsValue({
                        fromDate: moment(),
                        toDate: moment(),
                        dateType: "appointmentDate",
                        infoType: "phone",
                        examType: "",
                        info: "",
                        workTimeId: undefined,
                        channel: "",
                        nationality: undefined,
                        status: ""
                    })
                    onsubmit();
                }}
                icon={<ClearOutlined />}
            >
                Bỏ lọc
            </Button>
        </Form>
    )
}
export default AppointmentCovidPageSearch;
