import React, { FC, useState } from 'react';
import { Role } from 'interface/permission/role.interface';
import { Button, Col, Form, Row } from 'antd';
import { useLocale } from 'locales';
import { SearchOutlined, ClearOutlined, CheckOutlined, FileDoneOutlined } from '@ant-design/icons';
import useGetForm from './useGetForm';

export interface Values extends Role {}

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};

const EmailForm: FC = () => {
  const { Date, SID, CusName, MailCus, Language, Result, Mail } = useGetForm({ name: 'form', responsive: true });
  const { formatMessage } = useLocale();
  const [expand, setExpand] = useState(false);
  const onSearch = () => {
    //
  };

  return (
    <Form {...formItemLayout} className="ant-advanced-search-form">
      <Row gutter={24}>
        <Col span={10}>
          <Date />
          <SID />
        </Col>
        <Col style={{ textAlign: 'right' }} span={14}>
          <Form.Item>
            <Button icon={<SearchOutlined />} type="primary" onClick={onSearch}>
              Tìm kiếm
            </Button>
            <span> </span>
            <Button icon={<FileDoneOutlined />}>Xem kết quả</Button>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={10}>
          <CusName />
        </Col>
        <Col span={14}></Col>
      </Row>
      <Row gutter={24}>
        <Col span={10}>
          <MailCus />
        </Col>
        <Col style={{ textAlign: 'right' }} span={14}>
          <Form.Item>
            <Button icon={<CheckOutlined />} type="primary" onClick={onSearch}>
              Gửi thành công
            </Button>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={10}>
          <Language />
        </Col>
        <Col span={14}></Col>
      </Row>
      <Row gutter={24}>
        <Col span={10}>
          <Result />
        </Col>
        <Col span={14}></Col>
      </Row>
      <Row gutter={24}>
        <Col span={10}>
          <Mail />
        </Col>
        <Col span={14}></Col>
      </Row>
    </Form>
  );
};

export default EmailForm;
