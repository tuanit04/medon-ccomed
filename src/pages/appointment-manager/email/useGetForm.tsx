import React, { FC, useState } from 'react';
import { Form, Input, Col, Row, Select, Radio, DatePicker, Space, Checkbox } from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from './../../../helpers';
import { Role } from 'interface/permission/role.interface';
import TextArea from 'antd/lib/input/TextArea';

const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 8,
  xl: 8,
  xxl: 6
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 }
};

interface MessageFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  /** Initial form data */
  values?: Role;
}

export default function useGetForm({ required = false, responsive = false, name = 'form', values }: MessageFormProps) {
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<FormProps>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={values}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Ngay kham
  const Date: FC = () => {
    const date = (
      <Form.Item
        name="date"
        label="Ngày khám"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <DatePicker />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{date}</Col> : date;
  };

  //Ngay kham
  const SID: FC = () => {
    function handleChange(value: any) {}
    const sID = (
      <Form.Item
        name="sID"
        label="Mã SID"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Input placeholder="Nhập mã SID" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{sID}</Col> : sID;
  };

  //Ten khach hang
  const CusName: FC = () => {
    const cusName = (
      <Form.Item
        name="cusName"
        label="Tên khách hàng"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Input placeholder="Nguyễn Thị Duyên" disabled />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{cusName}</Col> : cusName;
  };

  //Mail nhan tin nhan
  const MailCus: FC = () => {
    const emailCus = (
      <Form.Item
        name="mailCus"
        label="Email (2)"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <TextArea />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{emailCus}</Col> : emailCus;
  };

  //Ngon ngu
  const Language: FC = () => {
    const [value, setValue] = useState(1);

    const onChange = (e: any) => {
      setValue(e.target.value);
    };

    const language = (
      <Form.Item
        name="language"
        label="Ngôn ngữ"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Radio.Group onChange={onChange} value={value}>
          <Radio value={1}>Tiếng Việt</Radio>
          <Radio value={2}>Tiếng Anh</Radio>
        </Radio.Group>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{language}</Col> : language;
  };

  //KQ dac biet
  const Result: FC = () => {
    const CheckboxGroup = Checkbox.Group;
    const plainOptions = ['Không giá', 'Dương tính'];
    const defaultCheckedList = ['Không giá', 'Dương tính'];
    const [checkedList, setCheckedList] = useState(defaultCheckedList);
    const [indeterminate, setIndeterminate] = useState(true);
    const [checkAll, setCheckAll] = useState(false);
    const onChange = (list: any) => {
      setCheckedList(list);
      setIndeterminate(!!list.length && list.length < plainOptions.length);
      setCheckAll(list.length === plainOptions.length);
    };
    const result = (
      <Form.Item
        name="result"
        label="KQ đặc biệt"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <CheckboxGroup options={plainOptions} value={checkedList} onChange={onChange} />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{result}</Col> : result;
  };

  //Mail gui tin nhan
  const Mail: FC = () => {
    const mail = (
      <Form.Item
        name="email"
        label="Mail gửi"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select defaultValue="lucy"></Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{mail}</Col> : mail;
  };

  return {
    Date,
    SID,
    CusName,
    MailCus,
    Language,
    Result,
    Mail
  };
}
