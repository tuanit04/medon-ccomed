import { Col, Row } from 'antd';
import React, { FC, useState } from 'react';
import EmailForm from './emailForm';
import './index.less';

const EmailPage: FC = () => {
  const [mcreateVisible, setCreateVisible] = useState(false);
  const [modifyVisible, setModifyVisible] = useState(false);
  const [authorizeVisible, setAuthorizeVisible] = useState(false);
  const [values, setValues] = useState({});
  return (
    <div className="main-content">
      <Row className="">
        <Col span={24}>
          <EmailForm />
        </Col>
      </Row>
    </div>
  );
};
export default EmailPage;
