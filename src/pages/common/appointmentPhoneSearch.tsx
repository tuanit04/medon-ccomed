import React, { useEffect, useState } from 'react';
import { Space, Table, Typography, Button, Input } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { Patient } from 'common/interfaces/patient.interface';
import { Appointment } from 'common/interfaces/appointment.interface';
import { CopyOutlined, IdcardOutlined, SearchOutlined } from '@ant-design/icons';
import moment from 'moment';
import Modal from 'antd/lib/modal/Modal';
import { useGetPIDByPhone } from 'hooks/appointment/useGetPIDByPhone';
import { useGetAppointmentByPhone } from 'hooks/appointment/useGetAppointmentByPhone';

interface Props {
  phoneNumber?: string;
  onApply?: (value?: Partial<Patient>) => void;
  onApplyPID?: (pid: any) => void;
  setDisabledField?: (value: boolean) => void;
}

const AppointmentPhoneSearch = ({ phoneNumber, onApply, onApplyPID, setDisabledField }: Props) => {
  const [phone, setPhone] = useState(phoneNumber);
  const [value, setValue] = useState<Partial<any>>();
  const [isConfirm, setIsConfirm] = useState(false);
  const { pIDByPhone, loadPIDByPhone, isLoadingPIDByPhone } = useGetPIDByPhone();
  const { loadAppointmentByPhone, appointmentByPhone, isLoadingAppointmentByPhone } = useGetAppointmentByPhone();
  const [pid, setPid]: any = useState();

  /*useEffect(() => {
    if (isEmptyObj(appointmentByPhone)) {
      let dataClone = appointmentByPhone;

    }
  }, [appointmentByPhone]);*/

  useEffect(() => {
    onQuery(phoneNumber);
    setPhone(phoneNumber);
  }, [phoneNumber]);

  const onCopy = (appointment?: any) => {
    setPid(null);
    setDisabledField?.(false);
    const birthDate = appointment?.patientBirthDate
      ? moment(appointment?.patientBirthDate, 'DD/MM/YYYY HH:mm:ss')
      : undefined;

    setValue({
      address: appointment?.patientAddress,
      name: appointment?.patientName,
      sex: appointment?.patientSex,
      birthDate,
      birthYear: appointment?.patientBirthYear ? moment(appointment?.patientBirthYear, 'YYYY') : undefined ?? birthDate,
      email: appointment?.patientEmail,
      idNo: appointment?.patientIdNo,
      provinceId: appointment?.provinceId,
      districtId: appointment?.districtId,
      wardId: appointment?.wardId,
      phone: phone?.slice(0, 1) === '0' ? phone.slice(1) : phone
    });
    setIsConfirm(true);
  };

  const onCopyPatient = (patient?: any) => {
    setPid(null);
    setDisabledField?.(true);
    setValue({
      ...patient,
      birthDate: patient?.birthDate ? moment(patient?.birthDate, 'DD/MM/YYYY') : undefined,
      birthYear: patient?.birthYear ? moment(patient?.birthYear, 'YYYY') : undefined,
      idNo: patient?.idNo,
      provinceId: patient?.provinceId,
      districtId: patient?.districtId,
      wardId: patient?.wardId,
      phone: phone?.slice(0, 1) === '0' ? phone.slice(1) : phone
    });
    setIsConfirm(true);
  };

  const onCopyPID = pid => {
    setValue(undefined);
    setPid(pid);
    setIsConfirm(true);
  };

  const patientColumn: ColumnsType<Patient> = [
    {
      title: 'Sử dụng',
      width: 100,
      align: 'center',
      render: (value, record) => (
        <>
          <Button
            title="Sử dụng tất cả thông tin"
            className="copy-button"
            type="primary"
            onClick={() => onCopyPatient(record)}
          >
            <IdcardOutlined />
          </Button>
          {/*{
            record.pid && <Button title="Sử dụng PID" className="copy-button ml-5px" type="primary" onClick={() => onCopyPID(record.pid)}>
              <CopyOutlined />
            </Button>
          }*/}
        </>
      )
    },
    {
      title: 'PID',
      width: 100,
      dataIndex: 'pid'
    },
    {
      title: 'Họ tên',
      width: 200,
      dataIndex: 'name'
    },
    {
      title: 'Ngày sinh',
      dataIndex: 'birthDate',
      width: 70,
      render: value => moment(value, 'DD/MM/YYYY HH:mm:ss').format('DD/MM')
    },
    {
      title: 'Năm sinh',
      dataIndex: 'birthDate',
      width: 70,
      render: value => moment(value, 'DD/MM/YYYY HH:mm:ss').format('YYYY')
    },
    {
      title: 'Giới tính',
      dataIndex: 'sex',
      width: 50,
      render: value => (value === 'MALE' ? 'Nam' : 'Nữ')
    },
    {
      title: 'SĐT',
      dataIndex: 'phone',
      width: 100,
      render: phone => (phone ? (phone.slice(0, 1) !== '0' ? '0' + phone : phone) : '')
    },
    {
      title: 'CMT/CCCD',
      dataIndex: 'idNo',
      width: 120
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'address',
      width: 300
    }
  ];

  const appointmentColumn: ColumnsType<Appointment> = [
    {
      title: 'Sử dụng',
      width: 100,
      align: 'center',
      render: (value, record) => (
        <>
          <Button
            type="primary"
            className="copy-button"
            title="Sử dụng tất cả thông tin"
            onClick={() => onCopy(record)}
          >
            <IdcardOutlined />
          </Button>
          {/*{record.pid && (
            <Button
              title="Sử dụng PID"
              className="copy-button ml-5px"
              type="primary"
              onClick={() => onCopyPID(record.pid)}
            >
              <CopyOutlined />
            </Button>
          )}*/}
        </>
      )
    },
    {
      title: 'Ngày khám',
      dataIndex: 'appointmentDate',
      width: 100,
      render: value => moment(value, 'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY')
    },
    {
      title: 'Họ tên',
      dataIndex: 'patientName',
      width: 200
    },
    {
      title: 'Ngày sinh',
      dataIndex: 'patientBirthDate',
      width: 70,
      render: value => value && moment(value, 'DD/MM/YYYY HH:mm:ss').format('DD/MM')
    },
    {
      title: 'Năm sinh',
      dataIndex: 'patientBirthYear',
      width: 70,
      render: (value, record: any) =>
        value ?? (record.patientBirthDate && moment(record.patientBirthDate, 'DD/MM/YYYY HH:mm:ss').format('YYYY'))
    },
    {
      title: 'Giới tính',
      dataIndex: 'patientSex',
      width: 50,
      render: value => (value ? (value === 'FEMALE' ? 'Nữ' : value === 'MALE' ? 'Nam' : 'Khác') : '')
    },
    {
      title: 'SĐT',
      dataIndex: 'patientPhone',
      width: 100,
      render: patientPhone =>
        patientPhone ? (patientPhone.slice(0, 1) !== '0' ? '0' + patientPhone : patientPhone) : ''
    },
    {
      title: 'CMT/CCCD',
      dataIndex: 'patientIdNo',
      width: 120
    },
    {
      title: 'Địa chỉ',
      width: 300,
      dataIndex: 'patientAddress'
    },
    {
      title: 'Thành phố',
      width: 100,
      dataIndex: 'provinceName'
    },
    {
      title: 'Quận huyện',
      width: 100,
      dataIndex: 'districtName'
    },
    {
      title: 'Email',
      width: 200,
      dataIndex: 'patientEmail'
    },
    {
      title: 'Thẻ KH (PID)',
      width: 100,
      dataIndex: 'pid'
    }
  ];

  const onQuery = (phoneNumber?: string) => {
    loadPIDByPhone({
      variables: {
        phone: phoneNumber
      }
    });
    loadAppointmentByPhone({
      variables: {
        page: 1,
        pageSize: 20,
        phone: phoneNumber
      }
    });
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPhone(e.target.value);
  };

  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Space>
        <Input
          value={phone}
          onChange={onChange}
          placeholder="Gõ SĐT tìm kiếm thông tin"
          type="number"
          onPressEnter={(e: any) => {
            onQuery(phone);
          }}
          style={{ width: 210 }}
        />
        <Button onClick={() => onQuery(phone)}>
          <SearchOutlined />
          Tìm kiếm
        </Button>
      </Space>
      <div>
        <Typography.Title level={5}>Danh sách thẻ khách hàng (PID)</Typography.Title>
        <Table
          loading={isLoadingPIDByPhone}
          pagination={{
            current: pIDByPhone?.page,
            pageSize: pIDByPhone?.pageSize,
            total: pIDByPhone?.records
          }}
          columns={patientColumn}
          dataSource={pIDByPhone?.data}
          scroll={{ x: 450, y: 'auto' }}
        />
      </div>
      <div>
        <Typography.Title level={5}>Danh sách lịch hẹn KH đã đặt trước đó theo SĐT</Typography.Title>
        <Table
          loading={isLoadingAppointmentByPhone}
          columns={appointmentColumn}
          dataSource={appointmentByPhone?.data}
          scroll={{ x: 450, y: 'auto' }}
        />
      </div>
      <Modal
        title="Thông báo"
        visible={isConfirm}
        onOk={() => {
          if (!pid) {
            onApply?.(value);
          } else {
            onApplyPID?.(pid);
          }
          setIsConfirm(false);
          setValue(undefined);
          setPid(null);
        }}
        onCancel={() => {
          setIsConfirm(false);
          setValue(undefined);
          setPid(null);
        }}
      >
        {!pid && (
          <p>
            Chọn sử dụng thông tin có thể mất thông tin KH mà bạn đã khai thác trước đó, bạn có muốn tiếp tục không?
          </p>
        )}
        {pid && <p>Chọn sử dụng PID có thể mất PID KH mà bạn đã khai thác trước đó, bạn có muốn tiếp tục không?</p>}
      </Modal>
    </Space>
  );
};

export default AppointmentPhoneSearch;
