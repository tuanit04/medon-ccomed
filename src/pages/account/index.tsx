import React, { FC, useState } from 'react';
import './index.css';
import { Form, Input, Select, Button, Spin, Alert } from 'antd';
import { SaveOutlined, ClearOutlined, LockOutlined } from '@ant-design/icons';
import { useChangePassword } from 'hooks/account/useChangePassword';

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0
    },
    sm: {
      span: 16,
      offset: 8
    }
  }
};

const userLogined = JSON.parse(String(localStorage.getItem('user')));
const ChangePasswordForm: FC = () => {
  const [form] = Form.useForm();
  const [isMatch, setIsMatch] = useState(false);
  const { changePassword, isLoadingChangePassword, errorChangePassword, resultChangePassword } = useChangePassword();
  const onFinish = values => {
    let data = { id: userLogined['id'], password: values['password'], oldPassword: values['oldPassword'] };
    changePassword({ data });
  };

  return (
    <div className="main-content">
      <Spin tip="Đang xử lý..." spinning={isLoadingChangePassword}>
        <div
          style={{
            width: '50%',
            height: 'auto',
            marginLeft: 'auto',
            marginRight: 'auto',
            marginTop: '5%',
            border: '1px solid #ccc'
          }}
        >
          <div style={{ textAlign: 'center', background: '#206ad2' }}>
            <span style={{ fontSize: '16px', fontWeight: 'bold', color: '#ffff', padding: '10px' }}>
              Thay đổi mật khẩu truy cập hệ thống
            </span>
          </div>
          <Form
            {...formItemLayout}
            form={form}
            style={{ padding: '15px' }}
            name="register"
            onFinish={onFinish}
            scrollToFirstError
          >
            <Form.Item
              name="oldPassword"
              label="Mật khẩu cũ"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập mật khẩu hiện tại.'
                }
              ]}
            >
              <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} />
            </Form.Item>

            <Form.Item
              name="password"
              dependencies={['password']}
              label="Mật khẩu mới"
              rules={[
                {
                  pattern: new RegExp(
                    /^(?!.* )(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\\.\,\?*()\_\-\=\+\:\;\"\'\/\<\>\[\]\{\}])(?=.{6,})/
                  ),
                  required: true,
                  message:
                    'Mật khẩu mới tối thiếu 6 ký tự, không chứa dấu cách, chứa ít nhất 1 ký tự viết hoa, 1 ký tự đặc biệt, 1 ký tự số.'
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value) {
                      return Promise.reject(new Error(' '));
                    } else {
                      if (getFieldValue('oldPassword') !== value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('Mật khẩu mới và mật khẩu cũ không được phép giống nhau.'));
                    }
                  }
                })
              ]}
              hasFeedback
            >
              <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} />
            </Form.Item>

            <Form.Item
              name="confirmPassword"
              label="Xác nhận mật khẩu mới"
              dependencies={['password']}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: 'Xác nhận mật khẩu mới không khớp.'
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject(new Error('Xác nhận mật khẩu mới không khớp.'));
                  }
                })
              ]}
            >
              <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} />
            </Form.Item>
            <Alert message="Lưu ý: Thay đổi mật khẩu thành công sẽ tự động đăng xuất tài khoản người dùng đang sử dụng hiện tại." type="info" showIcon style={{ marginTop: '15px', marginBottom: '15px' }} />
            <Form.Item {...tailFormItemLayout}>
              <Button icon={<SaveOutlined />} type="primary" htmlType="submit">
                Lưu
              </Button>
              &nbsp;&nbsp;&nbsp;
              <Button icon={<ClearOutlined />} onClick={() => form.resetFields()} type="primary" htmlType="submit">
                Nhập lại
              </Button>
            </Form.Item>
          </Form>
        </div>
      </Spin>
    </div>
  );
};

export default ChangePasswordForm;
