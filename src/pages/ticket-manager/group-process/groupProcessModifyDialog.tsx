import React, { FC, useCallback, useEffect, useState } from 'react';
import { Modal } from 'antd';
import useGetGroupProcessForm from './useGetGroupProcessForm';
import { apiGetUsersInGroupProcess } from 'api/ticket/ticket.api';
import { GroupProcess } from 'common/interfaces/group-process.interface';
import { useGetUsersLazy, useGetUserTypeLazy } from 'hooks/users/useGetUser';
import { openNotificationRight } from 'utils/notification';

interface GroupProcessModifyDialogProps {
  visible: boolean;
  onModify: (values: any) => void;
  onCancel: () => void;
  usersInGroupProcess: any;
  groupProcess: GroupProcess;
}

const GroupProcessModifyDialog: FC<GroupProcessModifyDialogProps> = ({
  onModify,
  onCancel,
  groupProcess,
  visible,
  usersInGroupProcess
}) => {
  const [tableData, setTableData]: any = useState([]);
  const { users, isLoadingUsers, loadUsers } = useGetUsersLazy();
  const { userType, isLoadingUserType, loadUserType } = useGetUserTypeLazy();
  useEffect(() => {
    loadUserType();
  }, []);
  useEffect(() => {
    if (userType) {
      loadUsers({
        variables: {
          page: 1,
          pageSize: 10000000,
          filtered: [
            {
              id: 'status',
              value: '1',
              operation: '=='
            },
            {
              id: 'userTypeId',
              value: userType[0]['id'],
              operation: '=='
            }
          ],
          sorted: [
            {
              id: 'createDate',
              desc: true
            }
          ]
        }
      });
    }
  }, [userType]);
  const {
    form,
    Form,
    Status,
    GroupName,
    GroupCode,
    UsersInGroupProcess,
    UsersInGroupProcessTable
  } = useGetGroupProcessForm({
    name: 'searchForm',
    responsive: true,
    isModal: true,
    isCreate: false,
    groupProcess
  });

  const onSubmit = async () => {
    const valuesModify: any = await form.validateFields();
    let userIds: any[] = [];
    for (let i = 0; i < tableData.length; i++) {
      userIds.push({ userId: tableData[i]['id'] });
    }
    onModify({
      ...groupProcess,
      code: valuesModify['code'],
      name: valuesModify['name'],
      status: valuesModify['status'],
      detail: userIds
    });
  };
  useEffect(() => {
    form.resetFields();
  }, [visible]);

  useEffect(() => {
    let userList: any = [];
    if (usersInGroupProcess?.length > 0) {
      //let userListSelected = users?.find(user => user.id === form.getFieldValue('examFacilityId'))?.provinceId
      for (let i = 0; i < usersInGroupProcess.length; i++) {
        for (let j = 0; j < users.length; j++) {
          if (users[j]['id'] === usersInGroupProcess[i]['id']) {
            userList.push(users[j]);
          }
        }
      }
      setTableData(userList);
    }
  }, [usersInGroupProcess]);
  const handleDeleteTableRow = record => {
    let tableDataCopy: any = [...tableData];
    for (var i = 0; i < tableDataCopy.length; i++) {
      if (tableDataCopy[i]['id'] === record['id']) {
        tableDataCopy.splice(i, 1);
        break;
      }
    }
    setTableData(tableDataCopy);
  };

  return (
    <Modal
      maskClosable={false}
      width={700}
      title="Sửa nhóm xử lý"
      visible={visible}
      onOk={onSubmit}
      onCancel={() => {
        setTableData([]);
        onCancel();
      }}
      okText="Lưu"
      cancelText="Hủy"
    >
      <Form style={{ marginTop: '10px' }}>
        <GroupCode />
        <GroupName />
        <Status />
        <UsersInGroupProcess
          usersInGroupProcess={users}
          onSelectUser={(str: string) => {
            let value = JSON.parse(str);
            let flag = false;
            if (value && tableData.length > 0) {
              for (let i = 0; i < tableData.length; i++) {
                if (tableData[i]['id'] + '' === value['id'] + '') {
                  flag = true;
                  break;
                }
              }
            }
            if (!flag) {
              setTableData([...tableData, value]);
            } else {
              openNotificationRight('Cán bộ đã được thêm vào bảng.', 'warning');
            }
          }}
        />
        <UsersInGroupProcessTable isUpdate={true} handleDeleteTableRow={handleDeleteTableRow} tableData={tableData} />
      </Form>
    </Modal>
  );
};

export default GroupProcessModifyDialog;
