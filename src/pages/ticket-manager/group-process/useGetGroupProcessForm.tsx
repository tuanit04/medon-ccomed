import React, { FC, useEffect, useState } from 'react';
import { Form, Input, Col, Row, Select, Button, Badge, Table } from 'antd';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import { SearchOutlined, ClearOutlined, DeleteOutlined } from '@ant-design/icons';
import { functionCodeConstants } from 'constants/functions';
import { GROUP_PROCESS_STATUS } from 'constants/group-process.constant';
import { GroupProcess } from 'common/interfaces/group-process.interface';
const { Option } = Select;
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperCol4Modal: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
  wrapperCol4Modal: { span: 18 }
};

interface GroupProcessFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?: boolean;
  isCreate?: boolean;
  /** Initial form data */
  groupProcess?: GroupProcess;
}

interface IStreetFormProps {
  provinceId?: string;
  districtId?: string;
  wardId?: string;
  parentFacilityId?: string;
  routeId?: string;
}

export default function useGetSearchForm({
  required = false,
  responsive = false,
  name = 'form',
  isModal,
  isCreate,
  groupProcess
}: GroupProcessFormProps) {
  const [formInstance] = Form.useForm<IStreetFormProps>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={isCreate ? { ...groupProcess, status: 1 } : groupProcess}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };
  useEffect(() => {
    formInstance.resetFields();
  }, [formInstance, groupProcess]);

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  const select = (
    <Select>
      <Option value="patientName">Mã nhóm</Option>
      <Option value="patientPhone">Tên nhóm</Option>
    </Select>
  );

  const Info: FC = () => {
    const info = (
      <Form.Item name="info" label="Giá trị tìm kiếm">
        <Input placeholder="Nhập thông tin tìm kiếm" addonBefore={select} style={{ width: '100%' }} />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{info}</Col> : info;
  };
  //Trang thai
  interface StatusProps {
    handleChangeStatus?: (value: any) => void;
    isSearch?: boolean;
  }
  const Status: FC<StatusProps> = ({ handleChangeStatus, isSearch }) => {
    const status = (
      <Form.Item
        name="status"
        label="Trạng thái"
        rules={[{ required: true, message: 'Trạng thái không được để trống.' }]}
      >
        <Select
          //disabled={isModal && !isCreate}
          defaultValue={isSearch ? '' : 1}
          onChange={handleChangeStatus}
          className={isModal ? '' : ''}
        >
          {!isModal ? (
            <Select.Option value="">
              <Badge color="blue" status="processing" text={GROUP_PROCESS_STATUS.ALL.value} />
            </Select.Option>
          ) : (
            ''
          )}
          <Select.Option value={GROUP_PROCESS_STATUS.ACTIVE.key}>
            <Badge color="green" status="processing" text={GROUP_PROCESS_STATUS.ACTIVE.value} />
          </Select.Option>
          <Select.Option value={GROUP_PROCESS_STATUS.INACTIVE.key}>
            <Badge color="red" status="processing" text={GROUP_PROCESS_STATUS.INACTIVE.value} />
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{status}</Col> : status;
  };

  const GroupCode: FC = () => {
    const groupCode = (
      <Form.Item rules={[{ required: true, message: 'Mã nhóm không được để trống.' }]} name="code" label="Mã nhóm">
        <Input readOnly={!isCreate} placeholder="Nhập mã nhóm" style={{ width: '100%' }} />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol4Modal}>{groupCode}</Col> : groupCode;
  };

  const GroupName: FC = () => {
    const groupName = (
      <Form.Item rules={[{ required: true, message: 'Tên nhóm không được để trống.' }]} name="name" label="Tên nhóm">
        <Input placeholder="Nhập tên nhóm" style={{ width: '100%' }} />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol4Modal}>{groupName}</Col> : groupName;
  };
  //Cán bộ xử lý
  interface UsersInGroupProcessProps {
    usersInGroupProcess: any;
    onSelectUser: (user: any) => void;
  }
  const UsersInGroupProcess: FC<UsersInGroupProcessProps> = ({ usersInGroupProcess, onSelectUser }) => {
    const usersInGroupProcessProps = (
      <Form.Item label="Cán bộ xử lý" name="assignUserId" rules={[{ message: 'Cán bộ xử lý không được để trống.' }]}>
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onSelect={(value, event) => {
            onSelectUser(value);
          }}
          placeholder="-- Chọn Đơn vị --"
        >
          {usersInGroupProcess?.map(option => (
            <Select.Option key={option.id} value={JSON.stringify(option)}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol4Modal}>{usersInGroupProcessProps}</Col> : usersInGroupProcessProps;
  };
  //Bảng Cán bộ xử lý được chọn
  interface UsersInGroupProcessTableProps {
    handleDeleteTableRow: (record) => void;
    tableData: any[];
    isUpdate?: boolean;
  }
  const UsersInGroupProcessTable: FC<UsersInGroupProcessTableProps> = ({
    tableData,
    handleDeleteTableRow,
    isUpdate
  }) => {
    const columns: any = [
      {
        title: 'STT',
        width: 50,
        align: 'center',
        render: (value, item, index) => index + 1
      },
      {
        title: 'Tên nhân viên',
        width: 200,
        dataIndex: 'name'
      },
      {
        title: 'Tài khoản',
        width: 150,
        dataIndex: 'userName'
      },
      {
        title: 'Hành động',
        width: 100,
        dataIndex: 'email',
        align: 'center',
        render: (value, record) => (
          <Button icon={<DeleteOutlined />} danger type="primary" onClick={() => handleDeleteTableRow(record)}>
            Xóa
          </Button>
        )
      }
    ];

    const usersInGroupProcess = (
      <Form.Item label="Bảng cán bộ xử lý được chọn" name="assignUserId" rules={[{ message: '' }]}>
        <Table scroll={{ x: 635, y: 300 }} columns={columns} dataSource={tableData.length !== 0 ? tableData : []} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{usersInGroupProcess}</Col> : usersInGroupProcess;
  };

  //Buttons
  interface ButtonsProps {
    functionOb: any;
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ functionOb, onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_DP_TIMKIEM_THONGKE] ? false : true}
          onClick={() => {
            onSearch();
          }}
          icon={<SearchOutlined />}
          type="primary"
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_DP_TIMKIEM_THONGKE] ? false : true}
          onClick={resetFields}
          icon={<ClearOutlined />}
        >
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };

  //Blank component
  const BlankComponent: FC = () => {
    const blankComponent = <Form.Item></Form.Item>;
    return responsive ? <Col {...wrapperCol}>{blankComponent}</Col> : blankComponent;
  };

  return {
    form: formInstance,
    Form: WrappedForm,
    Status,
    GroupCode,
    GroupName,
    UsersInGroupProcess,
    UsersInGroupProcessTable,
    Buttons,
    BlankComponent,
    Info
  };
}
