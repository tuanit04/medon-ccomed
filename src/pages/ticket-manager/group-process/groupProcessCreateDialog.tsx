import React, { FC, useCallback, useEffect, useState } from 'react';
import { Modal } from 'antd';
import useGetGroupProcessForm from './useGetGroupProcessForm';
import { apiGetUsersInGroupProcess } from 'api/ticket/ticket.api';
import { useGetUsersLazy, useGetUserTypeLazy } from 'hooks/users/useGetUser';
import { openNotificationRight } from 'utils/notification';

interface GroupProcessCreateDialogProps {
  visible: boolean;
  onCreate: (values: any) => void;
  onCancel: () => void;
}

const GroupProcessCreateDialog: FC<GroupProcessCreateDialogProps> = ({ onCreate, onCancel, visible }) => {
  const { users, isLoadingUsers, loadUsers } = useGetUsersLazy();
  const { userType, isLoadingUserType, loadUserType } = useGetUserTypeLazy();
  useEffect(() => {
    loadUserType();
  }, []);
  useEffect(() => {
    if (userType) {
      loadUsers({
        variables: {
          page: 1,
          pageSize: 10000000,
          filtered: [
            {
              id: 'status',
              value: '1',
              operation: '=='
            },
            {
              id: 'userTypeId',
              value: userType[0]['id'],
              operation: '=='
            }
          ],
          sorted: [
            {
              id: 'createDate',
              desc: true
            }
          ]
        }
      });
    }
  }, [userType]);
  const [tableData, setTableData]: any = useState([]);
  const {
    form,
    Form,
    Status,
    GroupName,
    GroupCode,
    UsersInGroupProcess,
    UsersInGroupProcessTable
  } = useGetGroupProcessForm({
    name: 'searchForm',
    responsive: true,
    isModal: true,
    isCreate: true
  });
  //API Lấy danh sách cán bộ xử lý
  const getUsersInGroupProcess = useCallback(async () => {
    const data: any = await apiGetUsersInGroupProcess('a8c616bf-a11b-4883-a0c6-4f084bc62e75');
  }, []);
  const onSubmit = async () => {
    const valuesCreate: any = await form.validateFields();
    let userIds: any[] = [];
    for (let i = 0; i < tableData.length; i++) {
      userIds.push({ userId: tableData[i]['id'] });
    }
    onCreate({
      code: valuesCreate['code'],
      name: valuesCreate['name'],
      status: valuesCreate['status'],
      detail: userIds
    });
  };
  useEffect(() => {
    form.resetFields();
  }, [visible]);
  useEffect(() => {
    //getUsersInGroupProcess();
  }, []);

  const handleDeleteTableRow = record => {
    let tableDataCopy: any = [...tableData];
    for (var i = 0; i < tableDataCopy.length; i++) {
      if (tableDataCopy[i]['id'] === record['id']) {
        tableDataCopy.splice(i, 1);
        break;
      }
    }
    setTableData(tableDataCopy);
  };

  return (
    <Modal
      maskClosable={false}
      width={700}
      title="Thêm mới nhóm xử lý"
      visible={visible}
      onOk={onSubmit}
      onCancel={() => {
        setTableData([]);
        onCancel();
      }}
      okText="Lưu"
      cancelText="Hủy"
    >
      <Form style={{ marginTop: '10px' }}>
        <GroupCode />
        <GroupName />
        <Status />
        <UsersInGroupProcess
          usersInGroupProcess={users}
          onSelectUser={(str: string) => {
            let value = JSON.parse(str);
            let flag = false;
            if (value && tableData.length > 0) {
              for (let i = 0; i < tableData.length; i++) {
                if (tableData[i]['id'] + '' === value['id'] + '') {
                  flag = true;
                  break;
                }
              }
            }
            if (!flag) {
              setTableData([...tableData, value]);
            } else {
              openNotificationRight('Cán bộ đã được thêm vào bảng.', 'warning');
            }
          }}
        />
        <UsersInGroupProcessTable handleDeleteTableRow={handleDeleteTableRow} tableData={tableData} />
      </Form>
    </Modal>
  );
};

export default GroupProcessCreateDialog;
