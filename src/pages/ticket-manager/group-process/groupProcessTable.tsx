import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import { Button, Table, Modal, Tag, Input, Space } from 'antd';
import { PlusCircleOutlined, DeleteOutlined, ExclamationCircleOutlined, SearchOutlined } from '@ant-design/icons';
import { functionCodeConstants } from 'constants/functions';
import { openNotificationRight } from 'utils/notification';
import Highlighter from 'react-highlight-words';
import { apiGetGroupProcessList } from 'api/group-process/group-process.api';
import { GroupProcess } from 'common/interfaces/group-process.interface';
import { GROUP_PROCESS_STATUS } from 'constants/group-process.constant';
import moment from 'moment';

interface GroupProcessTableProps {
  functionOb: any;
  onCreate: () => void;
  onAuthorize: (row: GroupProcess) => void;
  onModify: (groupProcess: GroupProcess) => void;
  onDelete: (value: string) => void;
  groupProcessRes: GroupProcess;
}

const GroupProcessTable: FC<GroupProcessTableProps> = ({
  functionOb,
  onCreate,
  onModify,
  onDelete,
  groupProcessRes
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(20);
  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');
  const [tableData, setTableData] = useState<GroupProcess[]>();

  const getGroupProcessList = useCallback(async () => {
    const data: any = await apiGetGroupProcessList(page, pageSize);
    setTableData(data?.data);
  }, []);
  useEffect(() => {
    getGroupProcessList();
  }, [page, pageSize, groupProcessRes]);
  const [pagination, setPagination] = useState({});

  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    if (pageSize !== pagination['pageSize']) {
      setPage(1);
    } else {
      setPage(pagination['current']);
    }
    setPageSize(pagination['pageSize']);
  };
  const searchInput = useRef<Input>(null);
  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder="Nhập nội dung tìm kiếm"
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm kiếm
          </Button>
          <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Bỏ lọc
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: text =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      )
  });
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = clearFilters => {
    clearFilters();
    setSearchText('');
  };
  const columns: any = [
    {
      title: 'STT',
      dataIndex: 'STT',
      key: 'STT',
      width: 70,
      align: 'center',
      render: (value, item, index) => index + 1
    },
    {
      title: 'Mã nhóm',
      dataIndex: 'code',
      key: 'code',
      width: 170,
      ...getColumnSearchProps('code')
    },
    {
      title: 'Tên nhóm',
      dataIndex: 'name',
      key: 'name',
      width: 270,
      ...getColumnSearchProps('name')
    },
    {
      title: 'Số lượng cán bộ',
      dataIndex: 'groupSize',
      key: 'groupSize',
      align: 'center'
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      filters: [
        { text: 'Kích hoạt', value: 1 },
        { text: 'Không kích hoạt', value: 2 }
      ],
      onFilter: (value, record) => record.status === value,
      with: 150,
      render: value =>
        value === GROUP_PROCESS_STATUS.ACTIVE.key ? (
          <Tag color="#2db7f5">{GROUP_PROCESS_STATUS.ACTIVE.value}</Tag>
        ) : value === GROUP_PROCESS_STATUS.INACTIVE.key ? (
          <Tag color="red">{GROUP_PROCESS_STATUS.INACTIVE.value}</Tag>
        ) : (
          <Tag color="#f50">Đã xóa</Tag>
        )
    },
    {
      title: 'Người tạo',
      dataIndex: 'createUser',
      align: 'center',
      key: 'createUser',
      with: 250,
      ...getColumnSearchProps('createUser')
    },
    {
      title: 'Thời gian tạo',
      dataIndex: 'createDate',
      align: 'center',
      key: 'createDate',
      with: 200,
      render: value => moment(value).format('DD/MM/yyyy HH:mm:ss')
    },
    {
      title: 'Ngày sửa',
      dataIndex: 'updateDate',
      align: 'center',
      key: 'updateDate',
      with: 200,
      render: value => (value ? moment(value).format('DD/MM/yyyy HH:mm:ss') : '')
    },
    {
      title: 'Người sửa',
      dataIndex: 'updateUser',
      align: 'center',
      key: 'updateUser',
      with: 250
    },
    {
      title: 'Sửa',
      dataIndex: '',
      align: 'center',
      key: '',
      with: 30,
      render: (_, row) => (
        <>
          <Button
            disabled={functionOb[functionCodeConstants.TD_QLTK_NHOMXL_THEMMOI_CAPNHAT] ? false : true}
            icon={<DeleteOutlined />}
            onClick={() => {
              onModify(row);
            }}
            type="primary"
          >
            Sửa
          </Button>
        </>
      )
    },
    {
      title: 'Xóa',
      dataIndex: '',
      align: 'center',
      key: '',
      with: 30,
      render: (_, row) => (
        <>
          <Button
            danger
            disabled={functionOb[functionCodeConstants.TD_QLTK_NHOMXL_XOA] ? false : true}
            icon={<DeleteOutlined />}
            onClick={() => {
              Modal.confirm({
                okText: 'Xóa',
                cancelText: 'Đóng',
                icon: <ExclamationCircleOutlined />,
                title: 'Bạn có chắc chắn muốn xóa bản ghi đang thao tác ?',
                onOk() {
                  onDelete(row['id']);
                  //openNotificationRight('Xóa bản ghi thành công.');
                },
                onCancel() {
                  //console.log('Cancel');
                }
              });
            }}
            type="primary"
          >
            Xóa
          </Button>
        </>
      )
    }
  ];

  return (
    <Table
      pagination={{
        ...pagination,
        defaultPageSize: 20,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '50']
      }}
      title={() => (
        <>
          <div style={{ float: 'left', marginTop: '5px' }}>
            <label className="lblTableTxt">
              <Tag color="#87d068">Tổng số bản ghi : {tableData?.length}</Tag>
            </label>
          </div>
          <div style={{ float: 'right' }}>
            <Button
              disabled={functionOb[functionCodeConstants.TD_QLTK_NHOMXL_THEMMOI_CAPNHAT] ? false : true}
              icon={<PlusCircleOutlined />}
              type="primary"
              onClick={onCreate}
            >
              Thêm mới
            </Button>
          </div>
          <div style={{ clear: 'both' }}></div>
        </>
      )}
      columns={columns}
      dataSource={tableData}
    />
  );
};

export default GroupProcessTable;
