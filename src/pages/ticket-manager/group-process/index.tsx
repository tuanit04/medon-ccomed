import React, { FC, useCallback, useState } from 'react';
import './index.less';
import GroupProcessTable from './groupProcessTable';
import GroupProcessModifyDialog from './groupProcessModifyDialog';
import GroupProcessCreateDialog from './groupProcessCreateDialog';
import { useAppState } from 'helpers';
import { TicketType } from 'common/interfaces/ticketType.interface';
import { openNotificationRight } from 'utils/notification';
import { CODE_RESPONSE } from 'constants/response';
import { apiCreateGroupProcess, apiDeleteGroupProcess } from 'api/group-process/group-process.api';
import { GroupProcess } from 'common/interfaces/group-process.interface';
import { Card } from 'antd';
import { apiGetUsersInGroupProcess } from 'api/ticket/ticket.api';

const GroupProcessPage: FC = () => {
  const functionOb = useAppState(state => state.user.functionObject);
  const [mcreateVisible, setCreateVisible] = useState(false);
  const [modifyVisible, setModifyVisible] = useState(false);
  const [authorizeVisible, setAuthorizeVisible] = useState(false);
  const [groupProcessModify, setGroupProcessModify]: any = useState();
  const [groupProcessRes, setGroupProcessRes]: any = useState();
  const [usersInGroupProcess, setUsersInGroupProcess]: any = useState([]);
  const handleSaveGroupProcess = async (values, isCreate) => {
    let data = await apiCreateGroupProcess(values);
    let statusRes = data?.['code'];
    if (statusRes) {
      if (statusRes === CODE_RESPONSE.SUCCESS) {
        //openNotificationRight(data?.data?.['message']);
        isCreate
          ? openNotificationRight('Thêm mới nhóm xử lý thành công.', 'success')
          : openNotificationRight('Cập nhật nhóm xử lý thành công.', 'success');
        setGroupProcessRes(data?.data);
      } else {
        isCreate
          ? openNotificationRight('Thêm mới không thành công.', 'error')
          : openNotificationRight('Cập nhật không thành công.', 'error');
      }
    } else {
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.', 'error');
    }
  };
  const handleDeleteGroupProcess = async id => {
    let data = await apiDeleteGroupProcess(id);
    let statusRes = data?.['code'];
    if (statusRes) {
      if (statusRes === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Xóa bản ghi thành công.', 'success');
        setGroupProcessRes(data?.data);
      } else {
        openNotificationRight('Xóa bản ghi thất bại.', 'error');
      }
    } else {
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.', 'error');
    }
  };
  //API Lấy danh sách cán bộ xử lý
  const getUsersInGroupProcess = useCallback(async groupProcessId => {
    if (groupProcessId) {
      const data: any = await apiGetUsersInGroupProcess(groupProcessId);
      setUsersInGroupProcess(data?.data);
    }
  }, []);
  return (
    <div className="main-content">
      <Card title="Quản lý nhóm xử lý" bordered={false} style={{ width: '100%' }}>
        {/*<GroupProcessSearchForm functionOb={functionOb} onSearch={value => {}} />*/}
        <GroupProcessTable
          functionOb={functionOb}
          onCreate={() => setCreateVisible(true)}
          onAuthorize={value => {
            setGroupProcessModify(value);
            setAuthorizeVisible(true);
          }}
          onModify={(groupProcess: GroupProcess) => {
            setGroupProcessModify(groupProcess);
            getUsersInGroupProcess(groupProcess['id']);
            setModifyVisible(true);
          }}
          onDelete={async id => {
            handleDeleteGroupProcess(id);
          }}
          groupProcessRes={groupProcessRes}
        />
        <GroupProcessModifyDialog
          visible={modifyVisible}
          usersInGroupProcess={usersInGroupProcess}
          groupProcess={groupProcessModify}
          onCancel={() => setModifyVisible(false)}
          onModify={async values => {
            handleSaveGroupProcess(values, false);
            setModifyVisible(false);
          }}
        />
        <GroupProcessCreateDialog
          visible={mcreateVisible}
          onCancel={() => setCreateVisible(false)}
          onCreate={(values: TicketType) => {
            handleSaveGroupProcess(values, true);
            setCreateVisible(false);
          }}
        />
      </Card>
    </div>
  );
};
export default GroupProcessPage;
