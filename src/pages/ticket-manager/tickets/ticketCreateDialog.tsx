import React, { FC, useCallback, useEffect, useState } from 'react';
import { Button, Collapse, Modal } from 'antd';
import './index.less';
import { Facility } from 'common/interfaces/facility.interface';
import useGetCreateTicketForm from './useGetCreateTicketForm';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { apiGetTicketTypeList } from 'api/ticket-type/ticketType.api';
import { useGetFacilitys } from 'hooks/facility';
import { apiGetUsersInGroupProcess } from 'api/ticket/ticket.api';
import { useGetUsersLazy, useGetUserTypeLazy } from 'hooks/users/useGetUser';
import { Channel } from 'common/interfaces/channel.interface';
import { Province } from 'common/interfaces/province.interface';
import { Patient } from 'common/interfaces/patient.interface';
const { Panel } = Collapse;

interface TicketCreateDialogProps {
  facilityParentList: Facility[];
  visible: boolean;
  onCreate: (values: any) => void;
  onProcessingTicket?: (values: any) => void;
  onCancel: () => void;
  groupProcessList: any;
  provinces: Province[];
  channels: Channel[];
  users: any;
}

const TicketCreateDialog: FC<TicketCreateDialogProps> = ({
  onCreate,
  onCancel,
  visible,
  facilityParentList,
  onProcessingTicket,
  groupProcessList,
  channels,
  provinces,
  users
}) => {
  const [isShowProcessingDialog, setShowProcessingDialog] = useState(false);
  const [ticketTypes, setTicketTypes] = useState([]);
  const [usersInGroupProcess, setUsersInGroupProcess] = useState([]);
  const [ticketTypeSelected, setTicketTypeSelected] = useState([]);
  const [slaValue, setSlaValue] = useState(0);
  const [assignFacilitySelected, setAssignFacilitySelected]: any = useState();
  const [isMedon, setIsMedon]: any = useState(true);
  //Hook Lấy danh sách Quận/Huyện
  const { districts, loadDistricts, isLoadingDistricts } = useGetDistricts();
  //Hook Lấy danh sách đơn vị
  const { facilities, loadFacilities, isLoadingFacilities } = useGetFacilitys();
  //API Lấy danh sách cán bộ xử lý
  const getUsersInGroupProcess = useCallback(async assignGroupId => {
    const data: any = await apiGetUsersInGroupProcess(assignGroupId);
    setUsersInGroupProcess(data?.data);
  }, []);
  //API Lấy danh sách Ticket Type
  const getTicketTypes = useCallback(async () => {
    const data: any = await apiGetTicketTypeList();
    setTicketTypes(data?.data);
  }, []);
  useEffect(() => {
    loadFacilities({
      variables: {
        filtered: [
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
    getTicketTypes();
  }, []);

  const handleChangePatient = (values?: Partial<Patient>) => {
    if (values?.provinceId) {
      loadDistricts({
        variables: {
          provinceId: values.provinceId
        }
      });
    }
    if (values?.districtId) {
      /*loadWards({
        variables: {
          districtId: values.districtId
        }
      });*/
    }
  };

  const {
    form,
    Form,
    PID,
    PatientCode,
    PatientType,
    PhoneNumber,
    PatientFullname,
    DateofBirth,
    Gender,
    Email,
    Address,
    Provinces,
    Districts,
    Note,
    TicketStatus,
    TicketType,
    TicketLevel,
    PatientSID,
    TicketContent,
    FileAttach,
    Evaluate,
    ReflectionUnit,
    ChannelReceive,
    PatientGroup,
    PatientRevenue,
    CreateUser,
    CreateFacility,
    CreateDate,
    LastUpdateUser,
    LastUpdateFacility,
    SLA,
    UsersInGroupProcess,
    GroupProcess,
    HandlerFacility,
    ProcessingTime
  } = useGetCreateTicketForm({
    name: 'searchForm',
    responsive: true,
    isModal: true,
    isCreate: true,
    districts: districts
  });
  useEffect(() => {
    form.resetFields();
  }, [visible]);
  const callback = key => {
    console.log(key);
  };
  useEffect(() => {
    if (facilities) {
      for (let i = 0; i < facilities.length; i++) {
        if (facilities[i]['code'] === 'MON') {
          let defaultValue = facilities[i]['id'];
          form.setFieldsValue({
            assignFacilityId: String(defaultValue)
          });
          break;
        }
      }
    }
  }, [facilities]);
  return (
    <Modal
      maskClosable={false}
      width={1500}
      centered
      title="Thêm mới Ticket"
      visible={visible}
      //onOk={async () => onCreate((await form.validateFields()) as any)}
      onOk={async () => {
        const values = await form.validateFields();
        onCreate({
          pid: '',
          sid: '',
          patientPhone: values['patientPhone'],
          patientName: values['patientName'],
          patient_crm_status: 1,
          patientBirthDate: values['patientBirthDate'],
          patientSex: values['patientSex'],
          customerType: values['customerType'],
          patientEmail: values['patientEmail'],
          address: values['address'],
          provinceId: values['provinceId'],
          districtId: values['districtId'],
          status: values['status'],
          level: values['level'],
          ticketType: values['ticketType'],
          content: values['content'],
          attach: typeof values['attach'] === 'object' ? '' : values['attach'],
          channelType: values['channelType'],
          reflectFacilityId: values['reflectFacilityId'],
          customerRevenue: values['customerRevenue'],
          customerGroup: values['customerGroup'],
          rating: values['rating'],
          processUserId: values['processUserId'],
          assignGroupId: values['assignGroupId'],
          assignFacilityId: values['assignFacilityId']
            ? values['assignFacilityId']
            : '6d5014c2-7cdf-4787-8f3f-68efbd31a5aa'
        });
      }}
      onCancel={onCancel}
      okText="Thêm mới"
      cancelText="Hủy bỏ"
      style={{ marginTop: '5px' }}
    >
      <Form style={{ marginTop: '2px' }}>
        <div className="first">
          <Collapse defaultActiveKey={['1']} onChange={callback}>
            <Panel header="THÔNG TIN KHÁCH HÀNG" key="1">
              <PID />
              <PatientCode />
              <PatientType />
              <PhoneNumber
                disabledField={false}
                provinces={provinces}
                setDisabledField={() => {}}
                handleChangeProvince={provinceId => {
                  if (provinceId) {
                    loadDistricts({
                      variables: {
                        provinceId: provinceId
                      }
                    });
                  }
                }}
                handleChangePatient={handleChangePatient}
                loadDistricts={loadDistricts}
              />
              <PatientFullname />
              {/*<div style={{ float: 'left', width: '50%' }}>
                <DateofBirth />
              </div>
              <div style={{ float: 'right', width: '50%' }}>
                <BirthYear />
              </div>
  <div style={{ clear: 'both' }}></div>*/}
              <DateofBirth />
              <Gender />
              <Email />
              <Address />
              <Provinces
                provinceList={provinces}
                isLoading={false}
                handleChangeProvince={provinceId => {
                  loadDistricts({
                    variables: {
                      provinceId: provinceId
                    }
                  });
                }}
              />
              <Districts districtList={districts} isLoading={false} handleChangeDistrict={() => {}} />
            </Panel>
          </Collapse>
          <div style={{ marginTop: '5px', marginBottom: '5px', width: '95%' }}>
            <div style={{ float: 'left' }}>
              <Button danger>Đặt lịch</Button>
            </div>
            <div style={{ float: 'right' }}>
              {/*<Button
                onClick={() => {
                  setShowProcessingDialog(true);
                }}
                type="primary"
                color="#ccc"
              >
                Xử lý Ticket
              </Button>
              &nbsp;&nbsp;*/}
              <Button
                type="primary"
                onClick={async () => {
                  const values = await form.validateFields();
                  onCreate({
                    pid: '',
                    sid: '',
                    patientPhone: values['patientPhone'],
                    patientName: values['patientName'],
                    patient_crm_status: 1,
                    patientBirthDate: values['patientBirthDate'],
                    patientSex: values['patientSex'],
                    customerType: values['customerType'],
                    patientEmail: values['patientEmail'],
                    address: values['address'],
                    provinceId: values['provinceId'],
                    districtId: values['districtId'],
                    status: values['status'],
                    level: values['level'],
                    ticketType: values['ticketType'],
                    content: values['content'],
                    attach: typeof values['attach'] === 'object' ? '' : values['attach'],
                    channelType: values['channelType'],
                    reflectFacilityId: values['reflectFacilityId'],
                    customerRevenue: values['customerRevenue'],
                    customerGroup: values['customerGroup'],
                    rating: values['rating'],
                    processUserId: values['processUserId'],
                    assignGroupId: values['assignGroupId'],
                    assignFacilityId: values['assignFacilityId']
                  });
                }}
              >
                Lưu Ticket
              </Button>
            </div>
            <div style={{ clear: 'both' }}></div>
          </div>
          <Collapse defaultActiveKey={['1']} onChange={callback}>
            <Panel header="THÔNG TIN XỬ LÝ" key="1">
              <UsersInGroupProcess
                usersInGroupProcess={form.getFieldValue('assignGroupId') ? usersInGroupProcess : users}
              />
              <GroupProcess
                groupProcessList={groupProcessList}
                handleChangeGroupProcess={assignGroupId => {
                  getUsersInGroupProcess(assignGroupId);
                }}
              />
              <HandlerFacility
                handleChangeAssignFacility={id => {
                  let flag = false;
                  for (let i = 0; i < facilities.length; i++) {
                    if (facilities[i]['id'] === id) {
                      if (facilities[i]['code'] === 'MON') {
                        flag = true;
                      }
                    }
                  }
                  if (flag) {
                    setIsMedon(true);
                  } else {
                    setIsMedon(false);
                    form.setFieldsValue({
                      assignFacilityId: id + ''
                    });
                  }
                }}
                facilities={facilities}
              />
              <CreateUser />
              <CreateFacility facilities={facilities} />
              {/*<CreateDate />
              <LastUpdateUser />
              <LastUpdateFacility />*/}
              <SLA slaValue={slaValue} />
              {/*<ProcessingTime />*/}
            </Panel>
          </Collapse>
        </div>
        <div className="second">
          <Collapse defaultActiveKey={['2']} onChange={callback}>
            <Panel header="THÔNG TIN TICKET" key="2">
              <div style={{ float: 'left', width: '50%' }}>
                <TicketStatus />
              </div>
              <div style={{ float: 'right', width: '50%' }}>
                <TicketType
                  handleChangeTicketType={value => {
                    for (let i = 0; i < ticketTypes?.length; i++) {
                      if (ticketTypes[i]['id'] === value) {
                        setTicketTypeSelected(ticketTypes[i]);
                        let slaArr: any = ticketTypes[i]['sla'];
                        console.log('slaArr : ', slaArr);
                        for (let i = 0; i < slaArr?.length; i++) {
                          if (
                            String(slaArr[i]['level']).toUpperCase() ===
                            String(form.getFieldValue('level') ? form.getFieldValue('level') : '').toUpperCase()
                          ) {
                            console.log("slaArr[i]['value'] : ", slaArr[i]['value']);
                            setSlaValue(slaArr[i]['value']);
                          }
                        }
                      }
                    }
                  }}
                  ticketTypeList={ticketTypes}
                />
              </div>
              <div style={{ float: 'left', width: '50%' }}>
                <TicketLevel
                  handleChangeTicketLevel={value => {
                    let slaArr = ticketTypeSelected['sla'];
                    if (slaArr) {
                      for (let i = 0; i < slaArr.length; i++) {
                        if (String(slaArr[i]['level']).toUpperCase() === String(value).toUpperCase()) {
                          setSlaValue(slaArr[i]['value']);
                        }
                      }
                    }
                  }}
                />
              </div>
              <div style={{ float: 'right', width: '50%' }}>
                <PatientSID />
              </div>
              <div style={{ float: 'left', width: '100%' }}>
                <TicketContent />
              </div>
              <div style={{ float: 'right', width: '100%' }}>
                <FileAttach />
              </div>
              <div style={{ float: 'left', width: '50%' }}>
                <ChannelReceive channelList={channels} />
              </div>
              <div style={{ float: 'right', width: '50%' }}>
                <PatientGroup />
              </div>
              {isMedon ? (
                ''
              ) : (
                <div style={{ float: 'left', width: '50%' }}>
                  <ReflectionUnit facilities={facilities} />
                </div>
              )}
              {isMedon ? (
                ''
              ) : (
                <div style={{ float: 'right', width: '50%' }}>
                  <Evaluate />
                </div>
              )}
              {isMedon ? (
                ''
              ) : (
                <div style={{ float: 'left', width: '50%' }}>
                  <PatientRevenue />
                </div>
              )}
              <div style={{ float: 'right', width: '50%' }}></div>
              <div style={{ clear: 'both' }}></div>
              {/*<PatientType2 />*/}
            </Panel>
          </Collapse>
          {/*<Collapse defaultActiveKey={['4']} onChange={callback}>
            <Panel header="GHI CHÚ" key="4">
              <Note />
            </Panel>
              </Collapse>*/}
        </div>
        {/*<TicketProcessingDialog
          visible={isShowProcessingDialog}
          onCancel={() => {
            setShowProcessingDialog(false);
          }}
          onCreate={() => { }}
        />*/}
      </Form>
    </Modal>
  );
};

export default TicketCreateDialog;
