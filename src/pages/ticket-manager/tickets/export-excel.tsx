import { Channel } from 'common/interfaces/channel.interface';
import { Ticket } from 'common/interfaces/ticket.interface';
import React, { FC } from 'react';
import ReactExport from 'react-data-export';
import Moment from 'moment';
import { checkChannelName, checkStatusName, checkTicketType } from './utils';
import { Button } from 'antd';
import { FileExcelOutlined } from '@ant-design/icons';
import { functionCodeConstants } from 'constants/functions';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

interface DownloadTicketsProps {
  functionOb: any;
  tickets: Ticket[] | undefined;
  channels: Channel[] | undefined;
}

const getMultiDataSet = arr => {
  return [
    {
      columns: [
        {
          title: 'Mã Ticket',
          width: { wpx: 100 },
          style: {
            fill: { patternType: 'solid', fgColor: { rgb: 'FFCCEEFF' } },
            font: { sz: '12', bold: true },
            alignment: { vertical: 'center', horizontal: 'center' }
          }
        }, //pixels width
        {
          title: 'Phân loại',
          width: { wpx: 100 },
          style: {
            fill: { patternType: 'solid', fgColor: { rgb: 'FFCCEEFF' } },
            font: { sz: '12', bold: true },
            alignment: { vertical: 'center', horizontal: 'center' }
          }
        },
        {
          title: 'Trạng thái',
          width: { wpx: 100 },
          style: {
            fill: { patternType: 'solid', fgColor: { rgb: 'FFCCEEFF' } },
            font: { sz: '12', bold: true },
            alignment: { vertical: 'center', horizontal: 'center' }
          }
        },
        {
          title: 'Khách hàng',
          width: { wpx: 250 },
          style: {
            fill: { patternType: 'solid', fgColor: { rgb: 'FFCCEEFF' } },
            font: { sz: '12', bold: true },
            alignment: { vertical: 'center', horizontal: 'center' }
          }
        },
        {
          title: 'Người xử lý',
          width: { wpx: 120 },
          style: {
            fill: { patternType: 'solid', fgColor: { rgb: 'FFCCEEFF' } },
            font: { sz: '12', bold: true },
            alignment: { vertical: 'center', horizontal: 'center' }
          }
        },
        {
          title: 'SLA',
          width: { wpx: 120 },
          style: {
            fill: { patternType: 'solid', fgColor: { rgb: 'FFCCEEFF' } },
            font: { sz: '12', bold: true },
            alignment: { vertical: 'center', horizontal: 'center' }
          }
        },
        {
          title: 'Kênh tiếp nhận',
          width: { wpx: 200 },
          style: {
            fill: { patternType: 'solid', fgColor: { rgb: 'FFCCEEFF' } },
            font: { sz: '12', bold: true },
            alignment: { vertical: 'center', horizontal: 'center' }
          }
        },
        {
          title: 'Người tạo',
          width: { wpx: 120 },
          style: {
            fill: { patternType: 'solid', fgColor: { rgb: 'FFCCEEFF' } },
            font: { sz: '12', bold: true },
            alignment: { vertical: 'center', horizontal: 'center' }
          }
        },
        {
          title: 'Ngày tạo',
          width: { wpx: 120 },
          style: {
            fill: { patternType: 'solid', fgColor: { rgb: 'FFCCEEFF' } },
            font: { sz: '12', bold: true },
            alignment: { vertical: 'center', horizontal: 'center' }
          }
        },
        {
          title: 'Người cập nhật',
          width: { wpx: 120 },
          style: {
            fill: { patternType: 'solid', fgColor: { rgb: 'FFCCEEFF' } },
            font: { sz: '12', bold: true },
            alignment: { vertical: 'center', horizontal: 'center' }
          }
        },
        {
          title: 'Ngày cập nhật',
          width: { wpx: 120 },
          style: {
            fill: { patternType: 'solid', fgColor: { rgb: 'FFCCEEFF' } },
            font: { sz: '12', bold: true },
            alignment: { vertical: 'center', horizontal: 'center' }
          }
        }
      ],
      data: arr
    }
  ];
};

const DownloadTickets: FC<DownloadTicketsProps> = ({ tickets, channels, functionOb }) => {
  let arr: any = [];
  if (tickets) {
    for (let i = 0; i < tickets?.length; i++) {
      let ticketCode = { value: tickets[i]['ticketCode'] ? tickets[i]['ticketCode'] : '' };
      let ticketType = {
        value: checkTicketType(tickets, tickets[i]['ticketType'])
          ? checkTicketType(tickets, tickets[i]['ticketType'])
          : ''
      };
      let status = {
        value: checkStatusName(tickets[i]['status']),
        style: {
          alignment: { vertical: 'center', horizontal: 'center' }
        }
      };
      let patientName = { value: tickets[i]['patientName'] ? tickets[i]['patientName'] : '' };
      let processUser = { value: tickets[i]['processUser'] ? tickets[i]['processUser'] : '' };
      let sla = { value: tickets[i]['sla'] ? tickets[i]['sla'] : '' };
      let channelType = {
        value: checkChannelName(tickets[i]['channelType'], channels)
          ? checkChannelName(tickets[i]['channelType'], channels)
          : '',
        style: {
          alignment: { vertical: 'center', horizontal: 'center' }
        }
      };
      let createUser = {
        value: tickets[i]['createUser'],
        style: {
          alignment: { vertical: 'center', horizontal: 'center' }
        }
      };
      let createDate = {
        value: tickets[i]['createDate'] !== null ? Moment(tickets[i]['createDate']).format('DD/MM/yyyy') : '',
        style: {
          alignment: { vertical: 'center', horizontal: 'center' }
        }
      };
      let updateUser = {
        value: tickets[i]['updateUser'] ? tickets[i]['updateUser'] : '',
        style: {
          alignment: { vertical: 'center', horizontal: 'center' }
        }
      };
      let updateDate = {
        value: tickets[i]['updateDate'] !== null ? Moment(tickets[i]['updateDate']).format('DD/MM/yyyy') : '',
        style: {
          alignment: { vertical: 'center', horizontal: 'center' }
        }
      };
      let dataTable = [
        ticketCode,
        ticketType,
        status,
        patientName,
        processUser,
        sla,
        channelType,
        createUser,
        createDate,
        updateUser,
        updateDate
      ];
      arr.push(dataTable);
    }
  }
  return (
    <ExcelFile
      filename="DANH-SACH-TICKET"
      fileExtension="xlsx"
      element={
        <Button
          disabled={functionOb[functionCodeConstants.TD_QLTK_DS_EXPORT_EXCEL] ? false : true}
          icon={<FileExcelOutlined />}
        >
          Xuất Excel
        </Button>
      }
    >
      <ExcelSheet dataSet={getMultiDataSet(arr)} name="Organization" />
    </ExcelFile>
  );
};
export default DownloadTickets;
