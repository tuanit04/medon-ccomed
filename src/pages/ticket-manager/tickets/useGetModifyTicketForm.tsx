import React, { FC, useCallback, useEffect, useState } from 'react';
import {
  Form,
  Input,
  Col,
  Row,
  Select,
  DatePicker,
  Button,
  Modal,
  Image,
  List,
  Comment,
  Avatar,
  Rate,
  Upload,
  Typography,
  InputNumber,
  message
} from 'antd';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import moment from 'moment';
import { DeleteOutlined, PhoneOutlined, UploadOutlined } from '@ant-design/icons';
import { Ward } from 'common/interfaces/ward.interface';
import { District } from 'common/interfaces/district.interface';
import { Province } from 'common/interfaces/province.interface';
import {
  CONTACT_METHOD,
  COST_TYPE,
  CUSTOMER_FEEDBACK,
  IMPORTANCE_LEVEL,
  OPINION_TYPE,
  PATIENT_GROUP,
  PATIENT_TYPE,
  TICKET_LEVEL,
  TICKET_STATUS
} from 'constants/ticket.constants';
import { Channel } from 'common/interfaces/channel.interface';
import { TicketType } from 'common/interfaces/ticketType.interface';
import { Facility } from 'common/interfaces/facility.interface';
import { Ticket } from 'common/interfaces/ticket.interface';
import { uploadFile } from 'api/utils.api';
import { openNotificationRight } from 'utils/notification';
import {
  apiGetAllTicketActionOfTicket,
  apiGetAllTicketProcessOfTicket,
  apiSaveTicketAction,
  apiDeleteTicketAction
} from 'api/ticket/ticket.api';
import { TicketAction } from 'common/interfaces/ticketAction.interface';
import { CODE_RESPONSE } from 'constants/response';
import { useGetUsersLazy } from 'hooks/users/useGetUser';
import { useGetFacilitys } from 'hooks/facility';
import AppointmentPhoneSearch from 'pages/common/appointmentPhoneSearch';
import { Patient } from 'common/interfaces/patient.interface';
import { currencyFormatter, currencyOptions, currencyParser } from './utils';
const { Option } = Select;
const { TextArea } = Input;
const { Text } = Typography;
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperColAllCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const wrapperColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 21,
  xl: 21,
  xxl: 21
};

const labelColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 3,
  xl: 3,
  xxl: 3
};

const labelColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperSDTInput: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 15,
  xl: 15,
  xxl: 15
};

const wrapperSDT: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 16,
  xl: 16,
  xxl: 16
};
const labelColTowItemBirthDate: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 6,
  xl: 6,
  xxl: 6
};
const wrapperColTowItemBirthDate: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 17,
  xl: 17,
  xxl: 17
};
const layout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 17 }
};

interface ModifyTicketFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?: boolean;
  /** Initial form data */
  ticket: Ticket;
  districts: District[];
  isCreate?: boolean;
  visible: boolean;
}

export default function useGetModifyTicketForm({
  required = false,
  responsive = false,
  name = 'form',
  ticket,
  visible,
  districts
}: ModifyTicketFormProps) {
  const [first, setFirst] = useState(false);
  const [districCode, setDistricCode]: any = useState();

  useEffect(() => {
    if (districCode && districts?.length > 0) {
      var disId = '';
      districts?.map(item => {
        if (item.code == districCode) {
          disId = item.id;
        }
      });
      formInstance.setFieldsValue({
        districtId: disId
      });
    }
  }, [districCode, districts]);

  const [isChooseGroupProcess, setChooseGroupProcess] = useState(ticket?.assignGroupId ? true : false);
  const [formInstance] = Form.useForm<Ticket>();
  const _Form: FC<FormProps> = ({ children, ...props }) => {
    return (
      <Form
        {...props}
        {...layout}
        form={formInstance}
        name={name}
        initialValues={{
          ...ticket,
          patientBirthDate: moment(moment(ticket?.patientBirthDate).format('DD/MM/YYYY'), 'DD/MM/YYYY'),
          patientBirthYear: moment(moment(ticket?.patientBirthDate).format('DD/MM/YYYY'), 'DD/MM/YYYY'),
          createDate: moment(ticket?.createDate).format('DD/MM/YYYY'),
          updateDate: ticket?.updateDate ? moment(ticket?.updateDate).format('DD/MM/YYYY') : undefined
        }}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  useEffect(() => {
    //formInstance.resetFields();
  }, [formInstance, ticket]);

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Mã khách hàng
  const PatientCode: FC = () => {
    const patientCode = (
      <Form.Item name="patientCode" label="Mã khách hàng">
        <Input readOnly={true} placeholder="" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{patientCode}</Col> : patientCode;
  };

  //Dia chi lien he
  const Address: FC = () => {
    const address = (
      <Form.Item
        label="Địa chỉ liên hệ"
        name="address"
        rules={[{ required: false, max: 100, message: 'Địa chỉ liên hệ không hợp lệ hoặc quá dài.' }]}
      >
        <Input placeholder="Nhập Địa chỉ liên hệ" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperColAllCol}>{address}</Col> : address;
  };

  //So dien thoai
  interface PhoneNumberProps {
    handleChangePatient: (values?: Partial<Patient>) => void;
    handleChangeProvince: (provinceId: string) => void;
    loadDistricts: any;
    provinces?;
    setDisabledField?: (value: boolean) => void;
    disabledField?: boolean;
  }
  const PhoneNumber: FC<PhoneNumberProps> = ({
    handleChangePatient,
    setDisabledField,
    handleChangeProvince,
    provinces,
    loadDistricts,
    disabledField
  }) => {
    const [isCalling, setIsCalling] = useState(false);
    const [isSearching, setIsSearching] = useState(false);
    const onApplyPID = pid => {
      setIsSearching(false);
      formInstance.setFieldsValue({
        pid: pid
      });
    };

    const onApply = (values?: Partial<Patient>) => {
      console.log('VALUES : ', values);
      setIsSearching(false);
      if (values?.provinceId) {
        handleChangeProvince(values?.provinceId);
      } else if (values?.provinceCode) {
        var id = '';
        provinces?.map(item => {
          if (item.code == values?.provinceCode) {
            id = item.id;
          }
        });
        if (id && id != '') {
          values.provinceId = id;
          setDistricCode(values.districtCode);
          loadDistricts?.({
            variables: {
              provinceId: id
            }
          });
        }
      }
      formInstance.setFieldsValue({
        pid: values?.pid,
        patientPhone: values?.phone ? 0 + values.phone : undefined,
        patientName: values?.name,
        patientBirthDate: values?.birthDate,
        patientBirthYear: values?.birthYear,
        patientSex: values?.sex,
        patientEmail: values?.email,
        address: values?.address,
        provinceId: values?.provinceId,
        districtId: values?.districtId
      });
      handleChangePatient(values);
    };

    const phoneNumber = (
      <>
        <Form.Item
          name="patientPhone"
          label="Số điện thoại"
          required
          rules={[
            {
              required: true,
              pattern: new RegExp(/^\S+$/),
              message: 'Số điện thoại không được để trống.'
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value) {
                  return Promise.reject(new Error(' '));
                } else {
                  value = value.toLowerCase();
                  value = value.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
                  value = value.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
                  value = value.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
                  value = value.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
                  value = value.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
                  value = value.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
                  value = value.replace(/đ/g, 'd');
                  let result = value?.replace(/^\s+|\s+|[a-zA-Z ]+|[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]+$/gm, '');
                  if (!/^\d+$/.test(value)) {
                    //return Promise.reject(new Error('SĐT không được chứa ký tự chữ hoặc ký tự đặc biệt.'));
                    formInstance.setFieldsValue({
                      patientPhone: result
                    });
                    formInstance.validateFields(['patientPhone']);
                  } else if (!/(0[1|3|2|4|6|5|7|8|9])+([0-9]{8})\b/g.test(value)) {
                    return Promise.reject(new Error('SĐT phải đúng định dạng theo nhà mạng và bắt đầu là 0.'));
                  } else {
                    formInstance.setFieldsValue({
                      patientPhone: result
                    });
                    return Promise.resolve();
                  }
                }
              }
            })
          ]}
        >
          <Input
            maxLength={10}
            onKeyUp={e => {
              if (e.key == 'Enter') {
                setIsSearching(true);
              }
            }}
            placeholder="Nhập số điện thoại"
          />
        </Form.Item>
        <Modal
          maskClosable={false}
          centered
          style={{ marginTop: 5, height: 'calc(100vh - 200px)' }}
          bodyStyle={{ display: 'flex', overflowY: 'auto' }}
          title="Danh sách thông tin khách hàng"
          visible={isSearching}
          onCancel={() => {
            setIsSearching(false);
          }}
          footer={null}
          width={1000}
          destroyOnClose={true}
        >
          <AppointmentPhoneSearch
            phoneNumber={formInstance.getFieldValue('patientPhone')}
            onApply={onApply}
            onApplyPID={onApplyPID}
            setDisabledField={setDisabledField}
          />
        </Modal>
      </>
    );
    return responsive ? <Col {...wrapperColAllCol}>{phoneNumber}</Col> : phoneNumber;
  };
  //Ho ten KH
  const PatientFullname: FC = () => {
    const patientFullname = (
      <Form.Item
        label="Họ tên KH:"
        name="patientName"
        rules={[{ required: true, message: 'Họ tên KH không được để trống.' }]}
      >
        <Input placeholder="Nhập họ tên khách hàng" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{patientFullname}</Col> : patientFullname;
  };

  //Ngay sinh
  const DateofBirth: FC = () => {
    const handleCheckInput = () => {
      var date = document.getElementById('date-picker-antd-ticket-update');
      function checkValue(str, max) {
        if (str.charAt(0) !== '0' || str == '00') {
          var num = parseInt(str);
          if (isNaN(num) || num <= 0 || num > max) num = 1;
          str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
        }
        return str;
      }
      date?.addEventListener('input', function(e) {
        if (date) {
          let dateInput: HTMLElement = date;
          dateInput['type'] = 'text';
          var input = dateInput['value'];
          if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
          var values = input.split('/').map(function(v) {
            return v.replace(/\D/g, '');
          });
          if (values[0]) values[0] = checkValue(values[0], 31);
          if (values[1]) values[1] = checkValue(values[1], 12);
          var output = values.map(function(v, i) {
            return v.length == 2 && i < 2 ? v + '/' : v;
          });
          dateInput['value'] = output.join('').substr(0, 14);
          let birthDay = output.join('').substr(0, 14);
          if (dateInput['value'].length === 10) {
            formInstance.setFieldsValue({
              patientBirthDate: moment(birthDay, 'DD/MM/YYYY')
            });
            formInstance.setFieldsValue({
              patientBirthYear: moment(birthDay.substr(6, 4) + '', 'YYYY')
            });
          }
        }
      });
    };
    const dateFormat = 'DD/MM/YYYY';
    const dofBirth = (
      <Row className="field-css">
        <Col {...labelColTowItemBirthDate} className="fs-12">
          <span className="red">* </span> Ngày sinh:
        </Col>
        <Col {...wrapperColTowItemBirthDate} style={{ marginLeft: '13px' }} className="over-hidden">
          {
            <Row>
              <Col span={12}>
                <Form.Item
                  name="patientBirthDate"
                  rules={[{ required: true, message: 'Ngày sinh không được để trống.' }]}
                >
                  <DatePicker
                    onFocus={handleCheckInput}
                    id="date-picker-antd-ticket-update"
                    onChange={(date, dateString) => {
                      if (date === null) {
                        formInstance.setFieldsValue({
                          patientBirthYear: undefined
                        });
                      } else {
                        formInstance.setFieldsValue({
                          patientBirthYear: moment(date, 'YYYY')
                        });
                      }
                    }}
                    disabledDate={current => {
                      return current && current > moment().add(0, 'day');
                    }}
                    placeholder="Chọn ngày sinh"
                    format={dateFormat}
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="patientBirthYear"
                  rules={[{ required: false, message: 'Năm sinh không được để trống.' }]}
                >
                  <DatePicker
                    onChange={(date, dateString) => {
                      if (date) {
                        formInstance.setFieldsValue({
                          patientBirthDate: moment('01/01/' + dateString, 'DD/MM/YYYY')
                        });
                      }
                    }}
                    disabledDate={current => {
                      return current && current > moment().add(0, 'day');
                    }}
                    format="YYYY"
                    placeholder="Chọn năm sinh"
                    picker="year"
                  />
                </Form.Item>
              </Col>
            </Row>
          }
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{dofBirth}</Col> : dofBirth;
  };
  //Năm sinh
  const BirthYear: FC = () => {
    const dateFormat = 'DD/MM/YYYY';
    const birthYear = (
      <Form.Item
        label="Năm sinh"
        name="patientBirthYear"
        rules={[{ required: true, message: 'Năm sinh không được để trống.' }]}
      >
        <DatePicker
          onChange={(date, dateString) => {
            if (date) {
            }
          }}
          disabledDate={current => {
            return current && current > moment().add(0, 'day');
          }}
          format="YYYY"
          placeholder="Chọn năm sinh"
          picker="year"
        />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{birthYear}</Col> : birthYear;
  };

  //Gioi tinh
  const Gender: FC = () => {
    const gender = (
      <Form.Item
        label="Giới tính"
        name="patientSex"
        rules={[{ required: true, message: 'Giới tính không được để trống.' }]}
      >
        <Select
          showSearch
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          placeholder="-- Chọn Giới tính --"
        >
          <Option value="MALE">Nam</Option>
          <Option value="FEMALE">Nữ</Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{gender}</Col> : gender;
  };

  //Tinh/TP
  interface ProvincesProps {
    provinceList: Province[];
    isLoading: boolean;
    handleChangeProvince: (provinceId: string) => void;
  }
  const Provinces: FC<ProvincesProps> = ({ provinceList, handleChangeProvince, isLoading }) => {
    const onChange = (value: string) => {
      handleChangeProvince(value);
      setFirst(true);
    };

    const provinces = (
      <Form.Item
        label="Tỉnh/TP"
        name="provinceId"
        rules={[
          {
            required: false,
            message: 'Tỉnh/TP không được để trống.'
          }
        ]}
      >
        <Select
          loading={isLoading}
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={onChange}
          placeholder="-- Chọn Tỉnh/TP --"
        >
          {provinceList?.map(province => (
            <Select.Option key={province.id} value={province.id}>
              {province.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{provinces}</Col> : provinces;
  };

  //Quan/Huyen
  interface DistrictProps {
    districtList: District[];
    isLoading: boolean;
    handleChangeDistrict: (districtId: string) => void;
  }
  const Districts: FC<DistrictProps> = ({ districtList, handleChangeDistrict, isLoading }) => {
    const onChange = (value: string) => {
      handleChangeDistrict(value);
    };
    useEffect(() => {
      if (first) {
        //disRefC.current.focus();
      }
    }, [districtList]);
    const districts = (
      <Form.Item
        label="Quận/Huyện"
        name="districtId"
        rules={[
          {
            required: false,
            message: 'Quận/Huyện không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          loading={isLoading}
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={onChange}
          placeholder="-- Chọn Quận/Huyện --"
        >
          {districtList?.map(district => (
            <Select.Option key={district.id} value={district.id}>
              {district.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{districts}</Col> : districts;
  };

  //Phuong/Xa
  interface WardProps {
    wardList: Ward[];
    isLoading: boolean;
    handleChangeWard: (wardId: string) => void;
  }
  const Wards: FC<WardProps> = ({ wardList, handleChangeWard, isLoading }) => {
    const wards = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Phường/Xã:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name="wardId"
            rules={[
              {
                required: false,
                message: 'Phường/Xã không được để trống.'
              }
            ]}
          >
            <Select
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={handleChangeWard}
              placeholder="-- Chọn Phường/Xã --"
            >
              {wardList?.map(ward => (
                <Select.Option key={ward.id} value={ward.id}>
                  {ward.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{wards}</Col> : wards;
  };

  //Email
  const Email: FC = () => {
    const email = (
      <Form.Item label="Email" name="patientEmail" rules={[{ required: false, message: 'Email không được để trống.' }]}>
        <Input placeholder="Nhập Email" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{email}</Col> : email;
  };

  //Nội dung xử lý
  interface ProcessContentProps {
    ticketProcessRes: any;
    ticketId: string;
  }
  const ProcessContent: FC<ProcessContentProps> = ({ ticketId, ticketProcessRes }) => {
    const [ticketProcessArr, setTicketProcessArr]: any = useState([]);
    const getAllTicketProcessOfTicket = useCallback(async ticketId => {
      const data: any = await apiGetAllTicketProcessOfTicket(ticketId);
      let ticketProcessArr = data?.data;
      setTicketProcessArr(ticketProcessArr);
    }, []);

    useEffect(() => {
      if (ticketId) {
        getAllTicketProcessOfTicket(ticketId);
      }
    }, [ticketId, ticketProcessRes]);

    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'VND',
      minimumFractionDigits: 0
    });

    const content = (
      <div style={{ maxHeight: '900px', overflow: 'auto', marginTop: '10px' }}>
        {ticketProcessArr.map(item => {
          return (
            <>
              <div style={{ float: 'left' }}>
                <Avatar
                  style={{
                    backgroundColor: '#7265e6',
                    verticalAlign: 'middle'
                  }}
                  size="large"
                  gap={0}
                >
                  {item['createUser']}
                </Avatar>
              </div>
              <div style={{ float: 'left', marginLeft: '12px', color: '#00000073' }}>
                <div>
                  <span style={{ fontWeight: 600 }}>{item['createUser']}</span>
                </div>
                <div>
                  <span>{moment(item['createDate']).format('DD/MM/YYYY HH:MM')}</span>
                </div>
              </div>
              <div style={{ clear: 'both' }}></div>
              <div style={{ margin: '10px 0', border: '1px solid #ccc', padding: '15px' }}>
                <Col {...wrapperCol}>
                  <Form.Item label="Phân loại ý kiến" rules={[{ required: true }]}>
                    <Select
                      bordered={false}
                      disabled={true}
                      suffixIcon={''}
                      onChange={e => {}}
                      showSearch
                      style={{ outline: 'none' }}
                      value={item['opinionType']}
                      filterOption={(input, option: any) =>
                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }
                      placeholder="-- Chọn --"
                    >
                      <Select.Option value={OPINION_TYPE.PRAISE.key}>{OPINION_TYPE.PRAISE.value}</Select.Option>
                      <Select.Option value={OPINION_TYPE.FEEDBACK.key}>{OPINION_TYPE.FEEDBACK.value}</Select.Option>
                      <Select.Option value={OPINION_TYPE.REFLECT.key}>{OPINION_TYPE.REFLECT.value}</Select.Option>
                      <Select.Option value={OPINION_TYPE.TROUBLE.key}>{OPINION_TYPE.TROUBLE.value}</Select.Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col {...wrapperCol}>
                  <Form.Item label="Phương thức liên hệ" rules={[{ required: true }]}>
                    <Select
                      bordered={false}
                      disabled={true}
                      suffixIcon={''}
                      onChange={e => {}}
                      showSearch
                      value={item['contactMethod']}
                      filterOption={(input, option: any) =>
                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }
                      placeholder="-- Chọn --"
                    >
                      <Select.Option value={CONTACT_METHOD.CONTACT_SUCCESS.key}>
                        {CONTACT_METHOD.CONTACT_SUCCESS.value}
                      </Select.Option>
                      <Select.Option value={CONTACT_METHOD.CONTACT_FAILURE.key}>
                        {CONTACT_METHOD.CONTACT_FAILURE.value}
                      </Select.Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col {...wrapperCol}>
                  <Form.Item label="Giải trình của các KP liên quan" rules={[{ required: true }]}>
                    <TextArea bordered={false} value={item['explaination']} readOnly={true} rows={3} />
                  </Form.Item>
                </Col>
                <Col {...wrapperCol}>
                  <Form.Item label="Xử lý" rules={[{ required: true }]}>
                    <TextArea bordered={false} value={item['processContent']} readOnly={true} rows={3} />
                  </Form.Item>
                </Col>
                <Col {...wrapperCol}>
                  <Form.Item label="Phản hồi khách hàng" rules={[{ required: true }]}>
                    <Select
                      bordered={false}
                      disabled={true}
                      suffixIcon={''}
                      onChange={e => {}}
                      showSearch
                      value={item['customerFeedback']}
                      filterOption={(input, option: any) =>
                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }
                      placeholder="-- Chọn --"
                    >
                      <Select.Option value={CUSTOMER_FEEDBACK.NORMAL.key}>
                        {CUSTOMER_FEEDBACK.NORMAL.value}
                      </Select.Option>
                      <Select.Option value={CUSTOMER_FEEDBACK.GOOD.key}>{CUSTOMER_FEEDBACK.GOOD.value}</Select.Option>
                      <Select.Option value={CUSTOMER_FEEDBACK.NOTGOOD.key}>
                        {CUSTOMER_FEEDBACK.NOTGOOD.value}
                      </Select.Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col {...wrapperCol}>
                  <Form.Item label="Đánh giá mức độ quan trọng" rules={[{ required: true }]}>
                    <Select
                      bordered={false}
                      disabled={true}
                      suffixIcon={''}
                      onChange={e => {}}
                      showSearch
                      value={item['importanceLevel']}
                      filterOption={(input, option: any) =>
                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }
                      placeholder="-- Chọn --"
                    >
                      <Select.Option value={IMPORTANCE_LEVEL.IMPOTANT.key}>
                        {IMPORTANCE_LEVEL.IMPOTANT.value}
                      </Select.Option>
                      <Select.Option value={IMPORTANCE_LEVEL.ORDINARY.key}>
                        {IMPORTANCE_LEVEL.ORDINARY.value}
                      </Select.Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col {...wrapperCol}>
                  <Form.Item label="Loại chi phí" rules={[{ required: true }]}>
                    <Select
                      bordered={false}
                      disabled={true}
                      suffixIcon={''}
                      onChange={e => {}}
                      showSearch
                      value={item['costType']}
                      filterOption={(input, option: any) =>
                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      <Select.Option value={COST_TYPE.MOVING_COST.key}>{COST_TYPE.MOVING_COST.value}</Select.Option>
                      <Select.Option value={COST_TYPE.COMPETITIVE_COST.key}>
                        {COST_TYPE.COMPETITIVE_COST.value}
                      </Select.Option>
                      <Select.Option value={COST_TYPE.VOUCHER.key}>{COST_TYPE.VOUCHER.value}</Select.Option>
                      <Select.Option value={COST_TYPE.VISIT_COST.key}>{COST_TYPE.VISIT_COST.value}</Select.Option>
                      <Select.Option value={COST_TYPE.OTHER_COST.key}>{COST_TYPE.OTHER_COST.value}</Select.Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col {...wrapperCol}>
                  <Form.Item label="Chi phí xử lý" rules={[{ required: true }]}>
                    <Input bordered={false} value={formatter.format(item['processCost'])} readOnly={true} />
                  </Form.Item>
                </Col>
              </div>
            </>
          );
        })}
      </div>
    );

    return responsive ? <Col {...wrapperCol}>{content}</Col> : content;
  };

  //Note
  interface CommentListProps {
    comments: any;
    handleDeleteTicketAction: (ticketActionId: string) => void;
    handleSaveTicketNote: (value: string) => void;
  }
  const CommentList: FC<CommentListProps> = ({ comments, handleDeleteTicketAction, handleSaveTicketNote }) => (
    <List
      dataSource={comments}
      itemLayout="horizontal"
      renderItem={(item: any) => (
        <List.Item
          extra={
            <Button
              danger
              id={item.id}
              onClick={event => {
                handleDeleteTicketAction(item.id);
              }}
              size="small"
              icon={<DeleteOutlined />}
            ></Button>
          }
        >
          <Comment {...item} />
        </List.Item>
      )}
    />
  );

  const Editor = ({ onChange, onSubmit, submitting, value }) => (
    <>
      <Form.Item>
        <TextArea
          placeholder="Nhập nội dung ghi chú"
          rows={4}
          onChange={onChange}
          //onPressEnter={onSubmit}
          value={value}
        />
      </Form.Item>
      <Form.Item>
        <Button loading={submitting} onClick={onSubmit} type="primary">
          Lưu ghi chú
        </Button>
      </Form.Item>
    </>
  );

  interface NoteProps {}
  const Note: FC<NoteProps> = ({}) => {
    const [comments, setComments]: any = useState([]);
    const [value, setValue] = useState('');
    const [ticketActions, setTicketActions]: any = useState<TicketAction[]>();
    const [ticketActionRes, setTicketActionRes]: any = useState();
    const handleSaveTicketAction = async values => {
      let ticketAction = {
        ticketId: ticket.id,
        actionType: 'NOTE',
        objectId: JSON.parse(String(localStorage.getItem('user')))['id'],
        note: values
      };
      let data = await apiSaveTicketAction(ticketAction);
      let statusRes = data?.['code'];
      if (statusRes) {
        if (statusRes === CODE_RESPONSE.SUCCESS) {
          setValue('');
          //openNotificationRight(data?.data?.['message']);
          //openNotificationRight('Thêm mới ghi chú thành công');
          setTicketActionRes(data?.data);
        } else {
          openNotificationRight('Thêm mới ghi chú thất bại');
        }
      } else {
        openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.');
      }
    };
    const handleDeleteTicketAction = async ticketActionId => {
      let data = await apiDeleteTicketAction(ticketActionId);
      let statusRes = data?.['code'];
      if (statusRes) {
        if (statusRes === CODE_RESPONSE.SUCCESS) {
          setValue('');
          //openNotificationRight(data?.data?.['message']);
          //openNotificationRight('Thêm mới ghi chú thành công');
          setTicketActionRes(data?.data);
        } else {
          openNotificationRight('Xoá ghi chú thất bại');
        }
      } else {
        openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.');
      }
    };
    const getAllTicketActionOfTicket = useCallback(async ticketId => {
      const data: any = await apiGetAllTicketActionOfTicket(ticketId);
      setTicketActions(data?.data);
    }, []);

    useEffect(() => {
      if (ticket?.id) {
        getAllTicketActionOfTicket(ticket?.id);
      }
    }, [ticket]);
    useEffect(() => {
      getAllTicketActionOfTicket(ticket?.id);
    }, [ticketActionRes]);
    useEffect(() => {
      if (ticketActions && ticketActions.length > 0) {
        let commentList: any = [];
        for (let i = 0; i < ticketActions.length; i++) {
          let comment = {
            id: ticketActions[i]['id'],
            author: <p>{ticketActions[i]['createUser']}</p>,
            avatar: (
              <Avatar
                style={{
                  backgroundColor: '#7265e6',
                  verticalAlign: 'middle'
                }}
                size="large"
                gap={0}
              >
                {ticketActions[i]['createUser']}
              </Avatar>
            ),
            content: <p>{ticketActions[i]['note']}</p>,
            datetime: moment(ticketActions[i]['createDate']).format('DD/MM/YYYY HH:mm:ss')
          };
          commentList.push(comment);
        }
        setComments(commentList);
      } else if (ticketActions && ticketActions.length === 0) {
        setComments([]);
      }
      //setComments(ticketActions);
    }, [ticketActions]);
    const handleSubmit = () => {
      if (!value) {
        openNotificationRight('Ghi chú không được để trống.');
        return;
      }
      handleSaveTicketAction(value);
    };

    const handleChange = e => {
      setValue(e.target.value);
    };
    const note = (
      <>
        {comments?.length > 0 && (
          <CommentList
            handleDeleteTicketAction={ticketActionId => {
              handleDeleteTicketAction(ticketActionId);
            }}
            handleSaveTicketNote={handleSaveTicketAction}
            comments={comments}
          />
        )}
        <Comment
          avatar={
            <Avatar
              style={{
                backgroundColor: '#7265e6',
                verticalAlign: 'middle'
              }}
              size="large"
              gap={0}
            >
              {JSON.parse(String(localStorage.getItem('user')))['username']}
            </Avatar>
          }
          content={<Editor onChange={handleChange} onSubmit={handleSubmit} value={value} submitting={false} />}
        />
      </>
    );

    return responsive ? <Col {...wrapperCol}>{note}</Col> : note;
  };

  //Mã thẻ KH PID
  interface PIDProps {}
  const PID: FC<PIDProps> = ({}) => {
    const pId = (
      <Form.Item
        label="PID"
        name="pid"
        rules={[
          {
            required: false,
            message: 'Mã thẻ KH (PID) không được để trống.'
          }
        ]}
      >
        <Input readOnly={true} placeholder="" />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{pId}</Col> : pId;
  };

  // Phân loại KH
  interface PatientTypeProps {}
  const PatientType: FC<PatientTypeProps> = ({}) => {
    const appointmentObject = (
      <Form.Item
        label="Phân loại KH"
        name="customerType"
        rules={[{ required: true, message: 'Loại Khách hàng không được để trống.' }]}
      >
        <Select
          onChange={e => {
            if (e === 'HEALTH_INSURANCE') {
              //handleChange(true);
            } else {
              //handleChange(false);
            }
          }}
          showSearch
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          placeholder="-- Chọn loại khách hàng --"
        >
          <Select.Option value={PATIENT_TYPE.NEW.key}>{PATIENT_TYPE.NEW.value}</Select.Option>
          <Select.Option value={PATIENT_TYPE.LEAD.key}>{PATIENT_TYPE.LEAD.value}</Select.Option>
          <Select.Option value={PATIENT_TYPE.CONTACT.key}>{PATIENT_TYPE.CONTACT.value}</Select.Option>
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentObject}</Col> : appointmentObject;
  };

  //Trang thai lich
  const TicketStatus: FC = () => {
    const ticketStatus = (
      <Form.Item
        label="Trạng thái"
        name="status"
        rules={[{ required: true, message: 'Trạng thái không được để trống.' }]}
      >
        <Select placeholder="-- Chọn trạng thái --">
          <Option value={TICKET_STATUS.WAITING.key}>{TICKET_STATUS.WAITING.value}</Option>
          <Option value={TICKET_STATUS.PROCESSING.key}>{TICKET_STATUS.PROCESSING.value}</Option>
          <Option value={TICKET_STATUS.RESOLVED.key}>{TICKET_STATUS.RESOLVED.value}</Option>
          <Option value={TICKET_STATUS.COMPLETED.key}>{TICKET_STATUS.COMPLETED.value}</Option>
          <Option value={TICKET_STATUS.CANCEL.key}>{TICKET_STATUS.CANCEL.value}</Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{ticketStatus}</Col> : ticketStatus;
  };
  // Phân loại
  interface TicketTypeProps {
    ticketTypeList: TicketType[];
    handleChangeTicketType: (value) => void;
  }
  const TicketType: FC<TicketTypeProps> = ({ ticketTypeList, handleChangeTicketType }) => {
    const ticketType = (
      <Form.Item
        label="Phân loại"
        name="ticketType"
        rules={[{ required: true, message: 'Phân loại không được để trống.' }]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={value => {
            /*let slc: Selection = formInstance.getFieldInstance('level');
            formInstance.setFieldsValue({
              level: ''
            });*/
            handleChangeTicketType(value);
          }}
          placeholder="-- Chọn Phân loại --"
        >
          {ticketTypeList?.map(ticketType => (
            <Select.Option key={ticketType.id} value={ticketType.id}>
              {ticketType.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{ticketType}</Col> : ticketType;
  };
  // Mức độ
  interface TicketLevelProps {
    handleChangeTicketLevel: (value) => void;
  }
  const TicketLevel: FC<TicketLevelProps> = ({ handleChangeTicketLevel }) => {
    const ticketLevel = (
      <Form.Item label="Mức độ" name="level" rules={[{ required: true, message: 'Mức độ không được để trống.' }]}>
        <Select
          onChange={value => {
            /*if (!formInstance.getFieldValue('ticketType')) {
              formInstance.validateFields(['ticketType']);
            }*/
            handleChangeTicketLevel(value);
          }}
          showSearch
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          placeholder="-- Chọn Mức độ --"
        >
          <Select.Option value={TICKET_LEVEL.LOW.key}>{TICKET_LEVEL.LOW.value}</Select.Option>
          <Select.Option value={TICKET_LEVEL.HIGH.key}>{TICKET_LEVEL.HIGH.value}</Select.Option>
          <Select.Option value={TICKET_LEVEL.MEDIUM.key}>{TICKET_LEVEL.MEDIUM.value}</Select.Option>
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{ticketLevel}</Col> : ticketLevel;
  };

  // SID KH
  interface PatientSIDProps {}
  const PatientSID: FC<PatientSIDProps> = ({}) => {
    const patientSID = (
      <Form.Item
        label="SID Khách hàng"
        name="sid"
        rules={[{ required: false, message: 'SID Khách hàng không được để trống.' }]}
      >
        <Input />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{patientSID}</Col> : patientSID;
  };
  //Nội dung
  interface TicketContentProps {}
  const TicketContent: FC<TicketContentProps> = ({}) => {
    const ticketContent = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12 content">
          <span>
            <span className="red">* </span>Nội dung :
          </span>
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="content" rules={[{ required: true, message: 'Nội dung không được bỏ trống.' }]}>
            <TextArea
              maxLength={500}
              style={{ marginLeft: '12px', width: '98%' }}
              rows={4}
              placeholder="Nhập nội dung"
            />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{ticketContent}</Col> : ticketContent;
  };

  // File đính kèm
  interface FileAttachProps {
    filePath: string;
  }
  const FileAttach: FC<FileAttachProps> = ({ filePath }) => {
    const [fileList, setFileList]: any = useState([]);
    useEffect(() => {
      if (filePath) {
        setFileList([
          {
            uid: '-1',
            name: 'Click để xem chi tiết',
            status: 'done',
            url: filePath
          }
        ]);
      }
    }, [filePath]);
    const [fileUrl, setFileUrl]: any = useState(null);
    const [fileSelected, setFileSelected] = useState<any>();
    const [uploading, setUploading] = useState(false);
    const handleUpload = () => {};
    useEffect(() => {
      if (fileSelected && !fileUrl) {
        uploadFile(fileSelected?.file?.originFileObj)
          .then(async resp => {
            if (resp?.status === 1) {
              openNotificationRight('Tải file thành công.', 'success');
              await setFileUrl(resp?.data?.fileName);
              formInstance.setFieldsValue({
                attach: resp?.data?.fileName
              });
              setFileList([
                {
                  uid: '-1',
                  name: 'Click để xem chi tiết',
                  status: 'done',
                  url: resp?.data?.fileName
                }
              ]);
            } else {
              openNotificationRight('Có lỗi xảy ra.', 'error');
              setFileList([]);
            }
          })
          .catch(error => {
            openNotificationRight('Lỗi : ' + error, 'error');
            setFileList([]);
          });
      } else {
        //formik.setFieldValue("image", fileUrl)
      }
    }, [fileSelected, fileUrl]);
    const handleChangeFile = async info => {
      setFileList(info.fileList);
      if (info.file.status !== 'uploading' && info.file.status !== 'removed') {
        await setFileSelected(info);
      }
    };
    const beforeUpload = file => {
      const isImageOrAudio = file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'audio/mpeg';
      if (!isImageOrAudio) {
        message.error('Bạn chỉ có thể tải lên định dạng JPG/PNG hoặc mp3 !');
        setFileList([]);
      }
      const isLt10M = file.size / 1024 / 1024 < 10;
      if (!isLt10M) {
        message.error('File không được lớn hơn 10 MB!');
        setFileList([]);
      }
      return isImageOrAudio && isLt10M;
    };
    const handleRemove = info => {
      formInstance.setFieldsValue({
        attach: undefined
      });
    };

    const fileAttach = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12">
          <span>
            <span className="red"></span>File đính kèm :
          </span>
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="attach" rules={[{ required: false, message: 'File đính kèm không được bỏ trống.' }]}>
            <Upload
              accept=".jpg, .mp3"
              listType="text"
              maxCount={1}
              beforeUpload={beforeUpload}
              onChange={handleChangeFile}
              onRemove={handleRemove}
              fileList={fileList}
            >
              <Button style={{ marginLeft: '11%' }} icon={<UploadOutlined />}>
                Chọn tệp
              </Button>
            </Upload>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{fileAttach}</Col> : fileAttach;
  };
  // Kênh tiếp nhận
  interface ChannelReceiveProps {
    channelList: Channel[] | undefined;
  }
  const ChannelReceive: FC<ChannelReceiveProps> = ({ channelList }) => {
    const channelReceive = (
      <Form.Item
        className="appoiment-cpn"
        name="channelType"
        label="Kênh tiếp nhận"
        rules={[
          {
            required: true,
            message: 'Kênh tiếp nhận không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          defaultValue=""
        >
          <Option key="all" value="">
            -- Tất cả --
          </Option>
          {channelList?.map(channel => (
            <Select.Option key={channel.id} value={channel.code}>
              {channel.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{channelReceive}</Col> : channelReceive;
  };

  // Phân loại khách hàng
  interface PatientType2Props {}
  const PatientType2: FC<PatientType2Props> = ({}) => {
    const patientType = (
      <Form.Item
        label="Phân loại khách hàng"
        name="patientType"
        rules={[{ required: true, message: 'Loại Khách hàng không được để trống.' }]}
      >
        <Select
          onChange={e => {
            if (e === 'HEALTH_INSURANCE') {
              //handleChange(true);
            } else {
              //handleChange(false);
            }
          }}
          showSearch
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          placeholder="-- Chọn loại KH --"
        >
          <Select.Option value="SERVICE">New - KH mới</Select.Option>
          <Select.Option value="HEALTH_INSURANCE">Lead - Khách hàng tiềm năng</Select.Option>
          <Select.Option value="GUARANTEE_INSURANCE">Contact - KH cũ</Select.Option>
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{patientType}</Col> : patientType;
  };
  // Đơn vị phản ánh
  interface ReflectionUnitProps {
    facilities: Facility[];
  }
  const ReflectionUnit: FC<ReflectionUnitProps> = ({ facilities }) => {
    const reflectionUnit = (
      <Form.Item
        label="Đơn vị phản ánh"
        name="reflectFacilityId"
        rules={[{ required: true, message: 'Đơn vị phản ánh không được để trống.' }]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onSelect={(value, event) => {}}
          placeholder="-- Chọn Đơn vị --"
        >
          {facilities?.map(option => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{reflectionUnit}</Col> : reflectionUnit;
  };

  // Đánh giá
  interface EvaluateProps {}
  const Evaluate: FC<EvaluateProps> = ({}) => {
    const evaluate = (
      <Form.Item label="Đánh giá" name="rating" rules={[{ required: true, message: 'Đánh giá không được để trống.' }]}>
        <Rate />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{evaluate}</Col> : evaluate;
  };

  // Nhóm khách hàng
  interface PatientGroupProps {}
  const PatientGroup: FC<PatientGroupProps> = ({}) => {
    const patientGroup = (
      <Form.Item
        label="Nhóm KH"
        name="customerGroup"
        rules={[{ required: true, message: 'Nhóm khách hàng không được để trống.' }]}
      >
        <Select
          onChange={e => {}}
          showSearch
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          placeholder="-- Chọn Nhóm khách hàng --"
        >
          <Select.Option value={PATIENT_GROUP.CUS_PERSONAL.key}>{PATIENT_GROUP.CUS_PERSONAL.value}</Select.Option>
          <Select.Option value={PATIENT_GROUP.CUS_COLLABORATORS.key}>
            {PATIENT_GROUP.CUS_COLLABORATORS.value}
          </Select.Option>
          <Select.Option value={PATIENT_GROUP.CUS_WARD.key}>{PATIENT_GROUP.CUS_WARD.value}</Select.Option>
          <Select.Option value={PATIENT_GROUP.CUS_HEALTH_EXAM.key}>{PATIENT_GROUP.CUS_HEALTH_EXAM.value}</Select.Option>
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{patientGroup}</Col> : patientGroup;
  };

  // Doanh thu khách hàng
  interface PatientRevenueProps {}
  const PatientRevenue: FC<PatientRevenueProps> = ({}) => {
    const [currency, setCurrency] = React.useState(currencyOptions[261].value);
    const patientRevenue = (
      <Form.Item
        label="Doanh thu KH"
        name="customerRevenue"
        rules={[{ required: true, message: 'Doanh thu KH không được để trống.' }]}
      >
        <InputNumber
          style={{
            width: '100%'
          }}
          formatter={currencyFormatter(currency)}
          parser={currencyParser}
        />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{patientRevenue}</Col> : patientRevenue;
  };
  //Cán bộ xử lý
  interface UsersInGroupProcessProps {
    usersInGroupProcess: any;
  }
  const UsersInGroupProcess: FC<UsersInGroupProcessProps> = ({ usersInGroupProcess }) => {
    const usersInGroupProcessProps = (
      <Form.Item
        label="Cán bộ xử lý"
        name="processUserId"
        rules={[{ required: true, message: 'Cán bộ xử lý không được để trống.' }]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onSelect={(value, event) => {
            if (value) {
              setChooseGroupProcess(false);
            }
          }}
          placeholder="-- Chọn Đơn vị --"
        >
          {usersInGroupProcess?.map(option => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{usersInGroupProcessProps}</Col> : usersInGroupProcessProps;
  };
  //Nhóm xử lý
  interface GroupProcessProps {
    groupProcessList: any;
    handleChangeGroupProcess: (assignGroupId) => void;
  }
  const GroupProcess: FC<GroupProcessProps> = ({ groupProcessList, handleChangeGroupProcess }) => {
    const groupProcess = (
      <Form.Item
        label="Nhóm xử lý"
        name="assignGroupId"
        rules={[{ required: isChooseGroupProcess, message: 'Nhóm xử lý không được để trống.' }]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onSelect={(value, event) => {
            if (!value) {
              if (isChooseGroupProcess) {
                setChooseGroupProcess(false);
              }
              return;
            } else {
              if (!isChooseGroupProcess) {
                setChooseGroupProcess(true);
              }
              handleChangeGroupProcess(value);
              formInstance.setFieldsValue({
                processUserId: undefined
              });
            }
          }}
          placeholder="-- Chọn Đơn vị --"
        >
          <Select.Option key="" value="">
            -- Bỏ chọn --
          </Select.Option>
          {groupProcessList?.map(option => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{groupProcess}</Col> : groupProcess;
  };
  //Đơn vị xử lý
  interface HandlerFacilityProps {
    facilities: Facility[];
    handleChangeAssignFacility: (id) => void;
  }
  const HandlerFacility: FC<HandlerFacilityProps> = ({ facilities, handleChangeAssignFacility }) => {
    const handlerFacility = (
      <Form.Item
        label="Đơn vị xử lý"
        name="assignFacilityId"
        rules={[{ required: true, message: 'Đơn vị xử lý không được để trống.' }]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={id => handleChangeAssignFacility(id)}
          onSelect={(value, event) => {}}
          placeholder="-- Chọn Đơn vị --"
        >
          {facilities?.map(option => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{handlerFacility}</Col> : handlerFacility;
  };
  //Người tạo
  interface CreateUserProps {}
  const CreateUser: FC<CreateUserProps> = ({}) => {
    const createUser = (
      <Form.Item label="Người tạo" name="createUser" rules={[{ message: 'Người tạo không được để trống.' }]}>
        <Input readOnly={true} style={{ border: 'none', color: '#206ad2', fontWeight: 'bold' }} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{createUser}</Col> : createUser;
  };
  //Đơn vị người tạo
  interface CreateFacilityProps {}
  const CreateFacility: FC<CreateFacilityProps> = ({}) => {
    //API Lấy user tạo theo id
    const { users: userCreate, isLoadingUsers: isLoadingUserCreate, loadUsers: loadUserCreate } = useGetUsersLazy();
    const { loadFacilities, isLoadingFacilities, facilities } = useGetFacilitys();
    useEffect(() => {
      loadUserCreate({
        variables: {
          filtered: [
            {
              id: 'status',
              value: '1',
              operation: '=='
            },
            {
              id: 'id',
              value: ticket?.createUserId,
              operation: '=='
            }
          ]
        }
      });
    }, []);
    useEffect(() => {
      if (userCreate) {
        loadFacilities({
          variables: {
            filtered: [
              {
                id: 'status',
                value: '1',
                operation: '=='
              },
              {
                id: 'id',
                value: userCreate?.[0]?.['facilityId'],
                operation: '=='
              }
            ]
          }
        });
      }
    }, [userCreate]);
    useEffect(() => {
      if (facilities) {
        formInstance.setFieldsValue({ createFacilityName: facilities?.[0]?.['name'] });
      }
    }, [facilities]);
    const createFacility = (
      <Form.Item
        label="Đơn vị người tạo"
        name="createFacilityName"
        rules={[{ message: 'Đơn vị người tạo không được để trống.' }]}
      >
        <Input readOnly={true} style={{ border: 'none', color: '#206ad2', fontWeight: 'bold' }} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{createFacility}</Col> : createFacility;
  };
  //Thời gian tạo
  interface CreateDateProps {}
  const CreateDate: FC<CreateDateProps> = ({}) => {
    const createDate = (
      <Form.Item label="Thời gian tạo" name="createDate" rules={[{ message: 'Thời gian tạo không được để trống.' }]}>
        <Input readOnly={true} style={{ border: 'none', color: '#206ad2', fontWeight: 'bold' }} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{createDate}</Col> : createDate;
  };
  //Người sửa cuối
  interface LastUpdateUserProps {}
  const LastUpdateUser: FC<LastUpdateUserProps> = ({}) => {
    const lastUpdateUser = (
      <Form.Item label="Người sửa cuối" name="updateUser" rules={[{ message: 'Người sửa cuối không được để trống.' }]}>
        <Input readOnly={true} style={{ border: 'none', color: '#206ad2', fontWeight: 'bold' }} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{lastUpdateUser}</Col> : lastUpdateUser;
  };
  //Đơn vị người sửa cuối
  interface LastUpdateFacilityProps {}
  const LastUpdateFacility: FC<LastUpdateFacilityProps> = ({}) => {
    //API Lấy user tạo theo id
    const { users: userUpdate, isLoadingUsers: isLoadingUserUpdate, loadUsers: loadUserUpdate } = useGetUsersLazy();
    const { loadFacilities, isLoadingFacilities, facilities } = useGetFacilitys();
    useEffect(() => {
      loadUserUpdate({
        variables: {
          filtered: [
            {
              id: 'status',
              value: '1',
              operation: '=='
            },
            {
              id: 'id',
              value: ticket?.updateUserId,
              operation: '=='
            }
          ]
        }
      });
    }, []);
    useEffect(() => {
      if (userUpdate) {
        loadFacilities({
          variables: {
            filtered: [
              {
                id: 'status',
                value: '1',
                operation: '=='
              },
              {
                id: 'id',
                value: userUpdate?.[0]?.['facilityId'],
                operation: '=='
              }
            ]
          }
        });
      }
    }, [userUpdate]);
    useEffect(() => {
      if (facilities) {
        formInstance.setFieldsValue({ updateFacilityName: facilities?.[0]?.['name'] });
      }
    }, [facilities]);
    const lastUpdateFacility = (
      <Form.Item
        label="ĐV người sửa cuối"
        name="updateFacilityName"
        rules={[{ message: 'Đơn vị người sửa cuối không được để trống.' }]}
      >
        <Input readOnly={true} style={{ border: 'none', color: '#206ad2', fontWeight: 'bold' }} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{lastUpdateFacility}</Col> : lastUpdateFacility;
  };
  //SLA
  interface SLAProps {
    slaValue: any;
  }
  const SLA: FC<SLAProps> = ({ slaValue }) => {
    const sla = (
      <Form.Item label="SLA" name="sla" rules={[{ message: 'SLA không được để trống.' }]}>
        <Text style={{ marginLeft: '10px', fontWeight: 'bold' }} type="danger">
          {formInstance.getFieldValue('ticketType') && formInstance.getFieldValue('level') ? slaValue : '0'} phút
        </Text>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{sla}</Col> : sla;
  };

  //Thời lượng xử lý
  interface ProcessingTimeProps {
    slaValue: any;
  }
  const ProcessingTime: FC<ProcessingTimeProps> = ({ slaValue }) => {
    let outOfDate = false;
    let processTimeConvert = 0;
    let processTime = '00:00:00';
    if (ticket?.status === 5) {
      var timeUpdate = moment(ticket?.updateDate).format('DD/MM/YYYY HH:mm:ss');
      var timeCreate = moment(ticket?.createDate).format('DD/MM/YYYY HH:mm:ss');
      processTime = moment
        .utc(moment(timeUpdate, 'DD/MM/YYYY HH:mm:ss').diff(moment(timeCreate, 'DD/MM/YYYY HH:mm:ss')))
        .format('HH:mm:ss');
    }
    if (processTime && slaValue) {
      let processTimeConvertArr = processTime.split(':');
      processTimeConvert =
        +processTimeConvertArr[0] * 60 * 60 + +processTimeConvertArr[1] * 60 + +processTimeConvertArr[2];
      let slaArr = slaValue?.split(':');
      let slaValueConvert = +slaArr[0] * 60 * 60 + +slaArr[1] * 60 + +slaArr[2];
      if (processTimeConvert > slaValueConvert) {
        outOfDate = true;
      }
    }
    formInstance.setFieldsValue({
      processingTime: processTime
    });
    const processingTimeCpn = (
      <Form.Item
        label="Thời lượng xử lý"
        name="processingTime"
        rules={[{ message: 'Thời lượng xử lý không được để trống.' }]}
      >
        <Input
          style={
            outOfDate
              ? { border: 'none', color: '#ff4d4f', fontWeight: 'bold' }
              : { border: 'none', color: '#025da3', fontWeight: 'bold' }
          }
        />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{processingTimeCpn}</Col> : processingTimeCpn;
  };
  //Người sửa cuối
  //Đơn vị sửa cuối
  //Thời gian sửa cuối
  interface UpdateDateProps {}
  const UpdateDate: FC<UpdateDateProps> = ({}) => {
    const updateDate = (
      <Form.Item label="Thời gian sửa" name="updateDate" rules={[{ message: '' }]}>
        <Input readOnly={true} style={{ border: 'none', color: '#206ad2', fontWeight: 'bold' }} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{updateDate}</Col> : updateDate;
  };

  return React.useMemo(
    () => ({
      form: formInstance,
      Form: WrappedForm,
      PID,
      PatientCode,
      PatientType,
      PhoneNumber,
      PatientFullname,
      DateofBirth,
      BirthYear,
      Gender,
      Email,
      Provinces,
      Districts,
      Address,
      Note,
      ProcessContent,
      TicketStatus,
      TicketType,
      TicketLevel,
      PatientSID,
      TicketContent,
      FileAttach,
      Evaluate,
      ReflectionUnit,
      PatientType2,
      ChannelReceive,
      PatientGroup,
      PatientRevenue,
      CreateUser,
      CreateFacility,
      CreateDate,
      UpdateDate,
      LastUpdateUser,
      LastUpdateFacility,
      SLA,
      ProcessingTime,
      UsersInGroupProcess,
      GroupProcess,
      HandlerFacility
    }),
    [ticket, visible, isChooseGroupProcess]
  );
}
