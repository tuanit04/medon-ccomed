import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import { Button, Table, Modal, Tag, Statistic, Row, Col, Card, Select, Popover, Tooltip, Input, Space } from 'antd';
import {
  CheckSquareOutlined,
  CloseOutlined,
  DeliveredProcedureOutlined,
  EllipsisOutlined,
  ExclamationCircleOutlined,
  FieldTimeOutlined,
  HistoryOutlined,
  IssuesCloseOutlined,
  MinusCircleOutlined,
  SearchOutlined,
  UsergroupAddOutlined,
  UserOutlined
} from '@ant-design/icons';
import { PlusCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import Highlighter from 'react-highlight-words';
import Moment from 'moment';
import { functionCodeConstants } from 'constants/functions';
import { apiCancelTicket, apiDeleteTicket, apiGetStatisticTicket, apiGetTicketList } from 'api/ticket/ticket.api';
import { Ticket } from 'common/interfaces/ticket.interface';
import { openNotificationRight } from 'utils/notification';
import { apiGetTicketTypeList, apiSaveTicketProcess } from 'api/ticket-type/ticketType.api';
import TicketProcessingDialog from './ticketProcessingDialog';
import { CODE_RESPONSE } from 'constants/response';
import DownloadTickets from './export-excel';
import moment from 'moment';
import { useSearchParams } from 'react-router-dom';
import useUpdateEffect from 'hooks/useUpdateEffect';
import { Channel } from 'common/interfaces/channel.interface';
import { checkLevelName } from './utils';
import { TICKET_STATISTIC } from 'constants/ticket.constants';

interface TicketTableProps {
  functionOb: any;
  onCreate: () => void;
  onModify: (ticket: Ticket) => void;
  onDelete?: (ticketId: string) => void;
  ticketRes: Ticket;
  channels: Channel[];
  valueSearch: any;
  groupProcessList: any;
  users: any;
}

const TicketTable: FC<TicketTableProps> = ({
  functionOb,
  onCreate,
  onModify,
  ticketRes,
  valueSearch,
  groupProcessList,
  channels,
  users
}) => {
  const [cardSelected, setCardSelected] = useState(0);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [ticketTypeList, setTicketTypeList]: any = useState([]);
  const [statisticTicket, setStatisticTicket]: any = useState([]);
  const [isShowProcessingDialog, setShowProcessingDialog] = useState(false);
  const [ticketIdSelected, setTicketIdSelected] = useState('');
  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(20);
  const [searchParams] = useSearchParams();

  let sorted = [{ id: 'createDate', desc: 'true', value: '' }];
  useEffect(() => {
    if (
      valueSearch &&
      Object.keys(valueSearch).length === 0 &&
      Object.getPrototypeOf(valueSearch) === Object.prototype
    ) {
      getTicketList({
        filtered: [
          {
            id: 'createDate',
            operation: 'between',
            value: moment().format('DD/MM/YYYY') + ',' + moment().format('DD/MM/YYYY')
          }
        ],
        sorted
      });
      getStatisticTicket('fromDate=' + moment().format('DD/MM/YYYY') + '&' + 'toDate=' + moment().format('DD/MM/YYYY'));
    } else if (valueSearch) {
      let infoType = valueSearch['infoType'];
      let info = valueSearch['info'];
      let fromDate = moment(valueSearch['fromDate']).format('DD/MM/YYYY');
      let toDate = moment(valueSearch['toDate']).format('DD/MM/YYYY');
      //let status = valueSearch['status'];
      let objSearch: any = {};
      if (infoType === 'patientPhone') {
        objSearch = { id: 'patientPhone', operation: '==', value: info };
      } else if (infoType === 'content') {
        objSearch = { id: 'content', operation: '==', value: info };
      } else if (infoType === 'ticketCode') {
        objSearch = { id: 'ticketCode', operation: '==', value: info };
      }
      //filtered.push({ id: 'status', operation: '==', value: status });
      if (objSearch && Object.keys(objSearch).length === 0 && Object.getPrototypeOf(objSearch) === Object.prototype) {
        getTicketList({
          filtered: [{ id: 'createDate', operation: 'between', value: fromDate + ',' + toDate }],
          sorted
        });
        getStatisticTicket('fromDate=' + fromDate + '&' + 'toDate=' + toDate);
      } else {
        getTicketList({
          filtered: [objSearch, { id: 'createDate', operation: 'between', value: fromDate + ',' + toDate }],
          sorted
        });
        getStatisticTicket('fromDate=' + fromDate + '&' + 'toDate=' + toDate);
      }
    }
  }, [valueSearch, ticketRes]);

  const getTicketTypeList = useCallback(async () => {
    const data: any = await apiGetTicketTypeList(page, pageSize);
    setTicketTypeList(data?.data);
  }, []);
  const [tableData, setTableData] = useState<Ticket[]>();
  const getTicketList = useCallback(async (conditions?) => {
    const data: any = await apiGetTicketList(page, pageSize, conditions);
    setTableData(data?.data);
  }, []);
  const getStatisticTicket = useCallback(async (conditions?) => {
    const data: any = await apiGetStatisticTicket(conditions);
    setStatisticTicket(data?.data);
  }, []);
  const handleDeleteTicket = useCallback(async ticketId => {
    const data: any = await apiDeleteTicket(ticketId);
    getTicketList({ sorted });
  }, []);
  const handleCancelTicket = useCallback(async ticketId => {
    const data: any = await apiCancelTicket(ticketId);
    getTicketList({ sorted });
  }, []);

  useUpdateEffect(() => {
    /*getTicketList({
      filtered: [
        {
          id: 'createDate',
          operation: 'between',
          value: moment(valueSearch['fromDate']).format('DD/MM/YYYY') + ',' + moment(valueSearch['toDate']).format('DD/MM/YYYY')
        }
      ],
      sorted
    });*/
  }, [page, pageSize]);

  useEffect(() => {
    getTicketTypeList();
    if (searchParams.get('id')) {
      getTicketList({
        filtered: [
          {
            id: 'id',
            operation: '==',
            value: searchParams.get('id')
          }
        ],
        sorted
      });
    } else {
      getTicketList({
        filtered: [
          {
            id: 'createDate',
            operation: 'between',
            value: moment().format('DD/MM/YYYY') + ',' + moment().format('DD/MM/YYYY')
          }
        ],
        sorted
      });
    }
  }, []);
  const [pagination, setPagination] = useState({});

  const onDeleteTicket = async ticketId => {
    Modal.confirm({
      icon: <ExclamationCircleOutlined />,
      okText: 'Xóa',
      cancelText: 'Đóng',
      title: 'Bạn có chắc chắn muốn xóa bản ghi đang thao tác ?',
      onOk() {
        handleDeleteTicket(ticketId);
      },
      onCancel() {
        //console.log('Cancel');
      }
    });
  };

  const onCancelTicket = async ticketId => {
    Modal.confirm({
      icon: <ExclamationCircleOutlined />,
      okText: 'Hủy',
      cancelText: 'Đóng',
      title: 'Bạn có chắc chắn muốn hủy bản ghi đang thao tác ?',
      onOk() {
        handleCancelTicket(ticketId);
      },
      onCancel() {
        //console.log('Cancel');
      }
    });
  };

  const onProcessTicket = async ticketId => {
    setTicketIdSelected(ticketId);
    setShowProcessingDialog(true);
  };

  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const reFetchData = async (params = {}) => {
    let pagination = params['pagination'];
    if (pageSize !== pagination['pageSize']) {
      setPage(1);
    } else {
      setPage(pagination['current']);
    }
    setPageSize(pagination['pageSize']);
    const data: any = await apiGetTicketList(pagination['current'], pagination['pageSize'], {
      filtered: [
        {
          id: 'createDate',
          operation: 'between',
          value: moment(valueSearch['fromDate']).format('DD/MM/YYYY') + ',' + moment(valueSearch['toDate']).format('DD/MM/YYYY')
        }
      ],
      sorted
    });
    setTableData(data?.data);
  };
  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };
  const handleSaveTicketProcess = async values => {
    let ticketProcess = { ticketId: ticketIdSelected, ...values };
    let data = await apiSaveTicketProcess(ticketProcess);
    let statusRes = data?.['code'];
    if (statusRes) {
      if (statusRes === CODE_RESPONSE.SUCCESS) {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Thêm mới xử lý Ticket thành công', 'success');
        setShowProcessingDialog(false);
      } else {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Thêm mới xử lý Ticket thất bại.', 'error');
      }
    } else {
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.', 'error');
    }
  };
  const searchInput = useRef<Input>(null);
  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder="Nhập nội dung tìm kiếm"
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm kiếm
          </Button>
          <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Bỏ lọc
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
          .toString()
          .toLowerCase()
          .includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: text =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      )
  });
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = clearFilters => {
    clearFilters();
    setSearchText('');
  };
  useEffect(() => {
    if (cardSelected) {
      if (valueSearch['fromDate'] && moment(valueSearch['toDate'])) {
        getTicketList(
          cardSelected !== 1
            ? {
              filtered: [
                { id: 'status', operation: '==', value: cardSelected },
                {
                  id: 'createDate',
                  operation: 'between',
                  value:
                    moment(valueSearch['fromDate']).format('DD/MM/YYYY') +
                    ',' +
                    moment(valueSearch['toDate']).format('DD/MM/YYYY')
                }
              ],
              sorted
            }
            : {
              filtered: [
                {
                  id: 'createDate',
                  operation: 'between',
                  value:
                    moment(valueSearch['fromDate']).format('DD/MM/YYYY') +
                    ',' +
                    moment(valueSearch['toDate']).format('DD/MM/YYYY')
                }
              ],
              sorted
            }
        );
      } else {
        getTicketList({
          filtered: [cardSelected === 1 ? { id: 'status', operation: '==', value: cardSelected } : ''],
          sorted
        });
      }
    }
  }, [cardSelected]);
  return (
    <>
      <div className="statistic-block">
        <Row gutter={2}>
          <Col span={2}>
            <Card
              key={1}
              onClick={() => {
                setCardSelected(1);
              }}
              hoverable
              style={cardSelected === 1 ? { background: '#0d91ef66' } : {}}
            >
              <Statistic
                prefix={<EllipsisOutlined />}
                title="Tất cả"
                value={0}
                precision={0}
                valueStyle={{ color: '#206ad2' }}
              />
            </Card>
          </Col>
          <Col span={3}>
            <Card
              onClick={() => {
                setCardSelected(2);
              }}
              key={2}
              hoverable
              style={cardSelected === 2 ? { background: '#0d91ef66' } : {}}
            >
              <Statistic
                suffix={<IssuesCloseOutlined />}
                title={TICKET_STATISTIC.WAITING.value}
                value={statisticTicket?.find(item => item.label === TICKET_STATISTIC.WAITING.lable)?.totalRecords}
                precision={0}
                valueStyle={{ color: '#206ad2' }}
              />
            </Card>
          </Col>
          <Col span={3}>
            <Card
              onClick={() => {
                setCardSelected(3);
              }}
              key={3}
              hoverable
              style={cardSelected === 3 ? { background: '#0d91ef66' } : {}}
            >
              <Statistic
                suffix={<UserOutlined />}
                title={TICKET_STATISTIC.ME.value}
                value={statisticTicket?.find(item => item.label === TICKET_STATISTIC.ME.lable)?.totalRecords}
                precision={0}
                valueStyle={{ color: '#206ad2' }}
              />
            </Card>
          </Col>
          <Col span={5}>
            <Card
              onClick={() => {
                setCardSelected(4);
              }}
              key={4}
              hoverable
              style={cardSelected === 4 ? { background: '#0d91ef66' } : {}}
            >
              <Statistic
                suffix={<UsergroupAddOutlined />}
                title={TICKET_STATISTIC.MY_GROUP.value}
                value={statisticTicket?.find(item => item.label === TICKET_STATISTIC.MY_GROUP.lable)?.totalRecords}
                precision={0}
                valueStyle={{ color: '#206ad2' }}
              />
            </Card>
          </Col>
          <Col span={3}>
            <Card
              onClick={() => {
                setCardSelected(5);
              }}
              key={5}
              hoverable
              style={cardSelected === 5 ? { background: '#0d91ef66' } : {}}
            >
              <Statistic
                suffix={<FieldTimeOutlined />}
                title={TICKET_STATISTIC.OTHER.value}
                value={statisticTicket?.find(item => item.label === TICKET_STATISTIC.OTHER.lable)?.totalRecords}
                precision={0}
                valueStyle={{ color: '#206ad2' }}
              />
            </Card>
          </Col>
          <Col span={3}>
            <Card
              onClick={() => {
                setCardSelected(6);
              }}
              key={6}
              hoverable
              style={cardSelected === 6 ? { background: '#0d91ef66' } : {}}
            >
              <Statistic
                suffix={<CheckSquareOutlined />}
                title={TICKET_STATISTIC.COMPLETED.value}
                value={statisticTicket?.find(item => item.label === TICKET_STATISTIC.COMPLETED.lable)?.totalRecords}
                precision={0}
                valueStyle={{ color: '#206ad2' }}
              />
            </Card>
          </Col>
          <Col span={2}>
            <Card
              onClick={() => {
                setCardSelected(7);
              }}
              key={7}
              hoverable
              style={cardSelected === 7 ? { background: '#0d91ef66' } : {}}
            >
              <Statistic
                suffix={<MinusCircleOutlined />}
                title={TICKET_STATISTIC.CANCEL.value}
                value={statisticTicket?.find(item => item.label === TICKET_STATISTIC.CANCEL.lable)?.totalRecords}
                precision={0}
                valueStyle={{ color: '#206ad2' }}
              />
            </Card>
          </Col>
          <Col span={3}>
            <Card
              onClick={() => {
                setCardSelected(8);
              }}
              key={8}
              hoverable
              style={cardSelected === 8 ? { background: '#0d91ef66' } : {}}
            >
              <Statistic
                suffix={<HistoryOutlined />}
                title={TICKET_STATISTIC.OVER_SLA.value}
                value={statisticTicket?.find(item => item.label === TICKET_STATISTIC.OVER_SLA.lable)?.totalRecords}
                precision={0}
                valueStyle={{ color: '#206ad2' }}
              />
            </Card>
          </Col>
        </Row>
      </div>
      <Table
        style={{ height: 'calc(100vh - 125px)', overflow: 'hidden' }}
        className="table-ticket"
        pagination={{
          ...pagination,
          defaultPageSize: 20,
          showSizeChanger: true,
          pageSizeOptions: ['5', '10','20', '30', '50']
        }}
        rowKey={row => row['id']}
        rowSelection={{
          selectedRowKeys,
          onChange: onSelectChange,
          type: 'checkbox'
        }}
        onChange={handleTableChange}
        bordered
        id="ticket-table"
        dataSource={tableData}
        scroll={{ x: true, y: 'calc(100vh - 250px)' }}
        title={() => (
          <>
            <div style={{ float: 'left' }}></div>
            <div style={{ float: 'right' }}>
              <Button
                icon={<PlusCircleOutlined />}
                disabled={functionOb[functionCodeConstants.TD_QLTK_DS_THEMMOI_CAPNHAT] ? false : true}
                type="primary"
                onClick={onCreate}
              >
                Thêm mới
              </Button>
              &nbsp;
              <Button
                disabled={functionOb[functionCodeConstants.TD_QLTK_DS_XULY] ? false : true}
                icon={<DeliveredProcedureOutlined />}
                style={{ background: '#ffffff', borderColor: '#5ca8ff' }}
                onClick={() => {
                  if (selectedRowKeys.length === 0) {
                    openNotificationRight('Vui lòng chọn một bản ghi cụ thể.', 'warning');
                    return;
                  }
                  onProcessTicket(selectedRowKeys[0]);
                }}
              >
                Xử lý Ticket
              </Button>
              &nbsp;
              <Button
                icon={<CloseOutlined />}
                disabled={functionOb[functionCodeConstants.TD_QLTK_DS_HUY] ? false : true}
                danger
                onClick={() => {
                  if (selectedRowKeys.length === 0) {
                    openNotificationRight('Vui lòng chọn một bản ghi cụ thể.', 'warning');
                    return;
                  }
                  onCancelTicket(selectedRowKeys[0]);
                }}
              >
                Hủy Ticket
              </Button>
              &nbsp;
              <Button
                icon={<DeleteOutlined />}
                disabled={functionOb[functionCodeConstants.TD_QLTK_DS_XOA] ? false : true}
                type="primary"
                danger
                onClick={() => {
                  if (selectedRowKeys.length === 0) {
                    openNotificationRight('Vui lòng chọn một bản ghi cụ thể.', 'warning');
                    return;
                  }
                  onDeleteTicket(selectedRowKeys[0]);
                }}
              >
                Xóa Ticket
              </Button>
              &nbsp;
              <DownloadTickets functionOb={functionOb} tickets={tableData} channels={channels} />
            </div>
            <div style={{ clear: 'both' }}></div>
          </>
        )}
        onRow={(record: Ticket, rowIndex) => {
          return {
            onDoubleClick: () => {
              //setAppointmentDetail(record);
              onModify(record);
            },
            onClick: event => { }
          };
        }}
      >
        <Table.Column<Ticket>
          width={50}
          title="STT"
          align="center"
          render={(value, item, index) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">{(page - 1) * pageSize + index + 1}</Tooltip>
          )}
        />
        <Table.Column<Ticket> width={120} title="Mã Ticket" align="center" render={(_, { ticketCode }) => ticketCode} />
        <Table.Column<Ticket> width={200} title="Mức độ" render={(_, { level }) => checkLevelName(level)} />
        <Table.Column<Ticket>
          width={150}
          title="Phân loại"
          align="center"
          render={(_, { ticketType }) => (
            <Select
              showSearch
              bordered={false}
              disabled={true}
              suffixIcon={''}
              style={{ outline: 'none' }}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              value={ticketType}
            >
              {ticketTypeList?.map(ticketType => (
                <Select.Option key={ticketType.id} value={ticketType.id}>
                  {ticketType.name}
                </Select.Option>
              ))}
            </Select>
          )}
        />
        <Table.Column<Ticket>
          width={350}
          title="Nội dung"
          render={(_, { content }) => (
            <Popover
              style={{ width: 300 }}
              content={<div style={{ width: 300 }}>{content}</div>}
              title="Chi tiết nội dung"
              trigger="hover"
            >
              <span>{content?.length >= 40 ? content.substr(0, 40) + '...' : content}</span>
            </Popover>
          )}
        />
        <Table.Column<Ticket>
          width={100}
          title="Trạng thái"
          align="center"
          render={(_, { status }) =>
            status === 1 ? (
              <Tag color="red">Huỷ</Tag>
            ) : status === 2 ? (
              <Tag color="blue">Chờ xử lý</Tag>
            ) : status === 3 ? (
              <Tag color="#f50">Đang xử lý</Tag>
            ) : status === 4 ? (
              <Tag color="green">Đã xử lý</Tag>
            ) : status === 5 ? (
              <Tag color="green">Hoàn thành</Tag>
            ) : (
              ''
            )
          }
        />
        <Table.Column<Ticket>
          width={200}
          title="Khách hàng"
          render={(_, { patientName, patientPhone }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <div style={{ fontWeight: 'bold', color: '#144283' }}>{patientName}</div>
              <div>SĐT : {patientPhone}</div>
            </Tooltip>
          )}
        />
        <Table.Column<Ticket> width={120} title="Người tạo" align="center" render={(_, { createUser }) => createUser} />
        <Table.Column<Ticket> width={120} title="SLA" align="center" render={(_, { sla }) => sla} />
        <Table.Column<Ticket>
          width={120}
          title="Người xử lý"
          align="center"
          render={(_, { processUserId }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <Select
                showSearch
                value={processUserId}
                bordered={false}
                disabled={true}
                suffixIcon={''}
                style={{ outline: 'none' }}
                optionFilterProp="children"
                filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                filterSort={(optionA, optionB) =>
                  optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                }
              >
                {users?.map(option => (
                  <Select.Option key={option.facilityCode} value={option.id}>
                    {option.name}
                  </Select.Option>
                ))}
              </Select>
            </Tooltip>
          )}
        />

        <Table.Column<Ticket>
          width={100}
          title="Nhóm xử lý"
          align="center"
          render={(_, { assignGroupId }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <Select
                value={assignGroupId}
                showSearch
                bordered={false}
                disabled={true}
                suffixIcon={''}
                style={{ outline: 'none' }}
                optionFilterProp="children"
                filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                filterSort={(optionA, optionB) =>
                  optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                }
                onSelect={(value, event) => { }}
              >
                {groupProcessList?.map(option => (
                  <Select.Option key={option.code} value={option.id}>
                    {option.name}
                  </Select.Option>
                ))}
              </Select>
            </Tooltip>
          )}
        />
        <Table.Column<Ticket>
          width={200}
          title="Kênh tiếp nhận"
          align="center"
          render={(_, { channelType }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              <Select
                value={channelType}
                showSearch
                bordered={false}
                disabled={true}
                suffixIcon={''}
                style={{ outline: 'none' }}
                optionFilterProp="children"
                filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                filterSort={(optionA, optionB) =>
                  optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                }
              >
                {channels?.map(channel => (
                  <Select.Option key={channel.id} value={channel.code}>
                    {channel.name}
                  </Select.Option>
                ))}
              </Select>
            </Tooltip>
          )}
        />
        <Table.Column<Ticket>
          width={100}
          title="Người tạo"
          align="center"
          render={(_, { createUser }) => <Tooltip title="Click đúp chuột để xem chi tiết">{createUser}</Tooltip>}
        />
        <Table.Column<Ticket>
          width={180}
          title="Thời gian tạo"
          align="center"
          render={(_, { createDate }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              {createDate !== null ? Moment(createDate).format('DD/MM/yyyy HH:mm:ss') : ''}
            </Tooltip>
          )}
        />
        <Table.Column<Ticket> width={100} title="Người sửa" align="center" render={(_, { updateUser }) => updateUser} />
        <Table.Column<Ticket>
          width={180}
          title="Thời gian sửa"
          align="center"
          render={(_, { updateDate }) => (
            <Tooltip title="Click đúp chuột để xem chi tiết">
              {updateDate !== null ? Moment(updateDate).format('DD/MM/yyyy HH:mm:ss') : ''}
            </Tooltip>
          )}
        />
      </Table>
      <TicketProcessingDialog
        visible={isShowProcessingDialog}
        onCancel={() => {
          setShowProcessingDialog(false);
        }}
        onCreate={values => {
          handleSaveTicketProcess(values);
        }}
      />
    </>
  );
};

export default TicketTable;
