import React, { FC, useEffect } from 'react';
import { Form, Input, Col, Row, Select, Button, DatePicker } from 'antd';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import { SearchOutlined, ClearOutlined } from '@ant-design/icons';
import { functionCodeConstants } from 'constants/functions';
import { Ticket } from 'common/interfaces/ticket.interface';
import moment from 'moment';
const { Option } = Select;

const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 4,
  xl: 4,
  xxl: 4
};

const layout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 17 }
};

interface TicketFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?: boolean;
  /** Initial form data */
  values?: FormData;
  isCreate?: boolean;
}

interface FormData {
  fromDate?: moment.Moment;
  toDate?: moment.Moment;
}

export default function useGetTicketForm({
  required = false,
  responsive = false,
  name = 'form',
  isModal,
  values,
  isCreate
}: TicketFormProps) {
  const [formInstance] = Form.useForm<FormProps>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState(state => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={{ ...values, fromDate: moment(), toDate: moment() }}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };
  useEffect(() => {
    formInstance.resetFields();
  }, [formInstance, values]);

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  const FromDate: FC = () => {
    const fromDate = (
      <Form.Item className="appoiment-cpn" name="fromDate" label="Từ ngày">
        <DatePicker defaultValue={moment()} format="DD/MM/YYYY" placeholder="Chọn Từ ngày" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{fromDate}</Col> : fromDate;
  };

  const ToDate: FC = () => {
    const toDate = (
      <Form.Item className="appoiment-cpn" name="toDate" label="Đến ngày">
        <DatePicker defaultValue={moment()} format="DD/MM/YYYY" placeholder="Chọn Đến ngày" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{toDate}</Col> : toDate;
  };
  const InfoType: FC = () => {
    const infoType = (
      <Form.Item className="appoiment-cpn" name="infoType" label="Tiêu chí">
        <Select placeholder="Chọn tiêu chí tìm kiếm">
          <Option value="patientPhone">Số điện thoại</Option>
          <Option value="content">Nội dung</Option>
          <Option value="ticketCode">Mã Ticket</Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{infoType}</Col> : infoType;
  };

  const Info: FC = () => {
    const info = (
      <Form.Item className="appoiment-cpn" name="info" label="">
        <Input placeholder="Nhập thông tin tìm kiếm" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{info}</Col> : info;
  };

  //Buttons
  interface ButtonsProps {
    functionOb: any;
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ functionOb, onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_CD_TIMKIEM_THONGKE] ? false : true}
          onClick={() => {
            onSearch();
          }}
          icon={<SearchOutlined />}
          type="primary"
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_CD_TIMKIEM_THONGKE] ? false : true}
          onClick={resetFields}
          icon={<ClearOutlined />}
        >
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };

  return {
    form: formInstance,
    Form: WrappedForm,
    FromDate,
    ToDate,
    InfoType,
    Info,
    Buttons
  };
}
