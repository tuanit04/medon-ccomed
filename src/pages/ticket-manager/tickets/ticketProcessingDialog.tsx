import React, { FC, useEffect } from 'react';
import { Modal } from 'antd';
import './index.less';
import { Route } from 'common/interfaces/route.interface';
import useGetProcessingTicketForm from './useGetProcessingTicketForm';
import { TicketProcess } from 'common/interfaces/ticket-process.interface';
import { apiSaveTicketProcess } from 'api/ticket-type/ticketType.api';

interface TicketProcessingDialogProps {
  visible: boolean;
  onCreate: (values: TicketProcess) => void;
  onCancel: () => void;
}

const TicketProcessingDialog: FC<TicketProcessingDialogProps> = ({ onCreate, onCancel, visible }) => {
  const {
    form,
    Form,
    CostsType,
    RateImportance,
    Feedback,
    Explanation,
    ContactMethod,
    Process,
    CategorizeOpinions,
    HandlingCost
  } = useGetProcessingTicketForm({
    name: 'searchForm',
    responsive: true,
    isModal: true,
    isCreate: true
  });
  useEffect(() => {
    form.resetFields();
  }, [visible]);
  const callback = key => {
    console.log(key);
  };
  const onSubmit = async () => {
    const values: any = await form.validateFields();
    onCreate(values);
  };
  return (
    <Modal
      maskClosable={false}
      width={1100}
      centered
      title="Xử lý Ticket"
      visible={visible}
      onOk={onSubmit}
      onCancel={onCancel}
      okText="Lưu"
      cancelText="Hủy bỏ"
      style={{ marginTop: '5px' }}
    >
      <Form style={{ marginTop: '2px' }}>
        <div style={{ float: 'left', width: '50%' }}>
          <CategorizeOpinions />
        </div>
        <div style={{ float: 'right', width: '50%' }}>
          <ContactMethod />
        </div>
        <div style={{ clear: 'both' }}></div>
        <div style={{ float: 'left', width: '50%' }}>
          <Explanation />
        </div>
        <div style={{ float: 'right', width: '50%' }}>
          <Process />
        </div>
        <div style={{ float: 'left', width: '50%' }}>
          <Feedback />
        </div>
        <div style={{ float: 'right', width: '50%' }}>
          <RateImportance />
        </div>
        <div style={{ float: 'left', width: '50%' }}>
          <CostsType />
        </div>
        <div style={{ float: 'right', width: '50%' }}>
          <HandlingCost />
        </div>
        <div style={{ clear: 'both' }}></div>
      </Form>
    </Modal>
  );
};

export default TicketProcessingDialog;
