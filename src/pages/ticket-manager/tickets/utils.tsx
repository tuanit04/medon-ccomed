import { Alert } from 'antd';
import { TICKET_LEVEL } from 'constants/ticket.constants';
import React from 'react';
import currencyList from './currencyList.js';
const locale = 'en-us';

export const checkTicketType = (ticketTypeList, id) => {
  let ticketType = ticketTypeList?.find(value => value['id'] === id);
  return ticketType ? ticketType['name'] : '';
};

export const checkStatusName = status => {
  return status === 1
    ? 'Huỷ'
    : status === 2
    ? 'Chờ xử lý'
    : status === 3
    ? 'Đang xử lý'
    : status === 4
    ? 'Đã xử lý'
    : status === 5
    ? 'Hoàn thành'
    : '';
};

export const checkLevelName = level => {
  if (level === TICKET_LEVEL.LOW.key) {
    return <Alert style={{ textAlign: 'center' }} message={TICKET_LEVEL.LOW.value} type="info"></Alert>;
  } else if (level === TICKET_LEVEL.MEDIUM.key) {
    return <Alert style={{ textAlign: 'center' }} message={TICKET_LEVEL.MEDIUM.value} type="warning"></Alert>;
  } else if (level === TICKET_LEVEL.HIGH.key) {
    return <Alert style={{ textAlign: 'center' }} message={TICKET_LEVEL.HIGH.value} type="error"></Alert>;
  } else {
    return <Alert style={{ textAlign: 'center' }} message=""></Alert>;
  }
};

export const checkChannelName = (channelType, channels) => {
  let channelObj = channels?.find(value => value['code'] === channelType);
  return channelObj?.name;
};

export const currencyOptions = currencyList.data.map(c => ({
  label: c.CcyNm,
  value: `${c.CtryNm}::${c.Ccy}`
}));

export const currencyFormatter = selectedCurrOpt => value => {
  return new Intl.NumberFormat(locale, {
    style: 'currency',
    currency: selectedCurrOpt.split('::')[1]
  }).format(value);
};

export const currencyParser = val => {
  try {
    // for when the input gets clears
    if (typeof val === 'string' && !val.length) {
      val = '0.0';
    }

    // detecting and parsing between comma and dot
    var group = new Intl.NumberFormat(locale).format(1111).replace(/1/g, '');
    var decimal = new Intl.NumberFormat(locale).format(1.1).replace(/1/g, '');
    var reversedVal = val.replace(new RegExp('\\' + group, 'g'), '');
    reversedVal = reversedVal.replace(new RegExp('\\' + decimal, 'g'), '.');
    //  => 1232.21 €

    // removing everything except the digits and dot
    reversedVal = reversedVal.replace(/[^0-9.]/g, '');
    //  => 1232.21

    // appending digits properly
    const digitsAfterDecimalCount = (reversedVal.split('.')[1] || []).length;
    const needsDigitsAppended = digitsAfterDecimalCount > 2;

    if (needsDigitsAppended) {
      reversedVal = reversedVal * Math.pow(10, digitsAfterDecimalCount - 2);
    }

    return Number.isNaN(reversedVal) ? 0 : reversedVal;
  } catch (error) {
    console.error(error);
  }
};
