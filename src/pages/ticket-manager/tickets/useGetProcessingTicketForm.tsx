import React, { FC } from 'react';
import { Form, Input, Col, Row, Select, InputNumber } from 'antd';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { Appointment } from 'common/interfaces/appointment.interface';
import {
  CONTACT_METHOD,
  COST_TYPE,
  CUSTOMER_FEEDBACK,
  IMPORTANCE_LEVEL,
  OPINION_TYPE
} from 'constants/ticket.constants';
import { currencyFormatter, currencyOptions, currencyParser } from './utils';

const { Option } = Select;
const { TextArea } = Input;

const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 8,
  xl: 8,
  xxl: 8
};

const layout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 17 }
};

interface ProcessingTicketFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?: boolean;
  /** Initial form data */
  appointmentInput?: Partial<Appointment>;
  isCreate?: boolean;
}

export default function useGetProcessingTicketForm({
  required = false,
  responsive = false,
  name = 'form',
  appointmentInput
}: ProcessingTicketFormProps) {
  const [formInstance] = Form.useForm<Partial<Appointment>>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const onValuesChange = (changedValues: any, values: Appointment) => {};

    return (
      <Form
        {...props}
        {...layout}
        form={formInstance}
        name={name}
        initialValues={appointmentInput}
        onValuesChange={onValuesChange}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Chi phí xử lý
  const Costs: FC = () => {
    const costs = (
      <Form.Item name="id" label="Chi phí xử lý">
        <Input placeholder="" />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{costs}</Col> : costs;
  };

  // Loại chi phí
  interface CostsTypeProps {}
  const CostsType: FC<CostsTypeProps> = ({}) => {
    const costsType = (
      <Form.Item
        label="Loại chi phí"
        name="costType"
        rules={[{ required: true, message: 'Loại chi phí không được để trống.' }]}
      >
        <Select
          onChange={e => {}}
          showSearch
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          placeholder="-- Chọn --"
        >
          <Select.Option value={COST_TYPE.MOVING_COST.key}>{COST_TYPE.MOVING_COST.value}</Select.Option>
          <Select.Option value={COST_TYPE.COMPETITIVE_COST.key}>{COST_TYPE.COMPETITIVE_COST.value}</Select.Option>
          <Select.Option value={COST_TYPE.VOUCHER.key}>{COST_TYPE.VOUCHER.value}</Select.Option>
          <Select.Option value={COST_TYPE.VISIT_COST.key}>{COST_TYPE.VISIT_COST.value}</Select.Option>
          <Select.Option value={COST_TYPE.OTHER_COST.key}>{COST_TYPE.OTHER_COST.value}</Select.Option>
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{costsType}</Col> : costsType;
  };

  // Đánh giá mức độ quan trọng
  interface RateImportanceProps {}
  const RateImportance: FC<RateImportanceProps> = ({}) => {
    const rateImportance = (
      <Form.Item
        label="Mức độ quan trọng"
        name="importanceLevel"
        rules={[{ required: true, message: 'Mức độ quan trọng không được để trống.' }]}
      >
        <Select
          onChange={e => {}}
          showSearch
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          placeholder="-- Chọn --"
        >
          <Select.Option value={IMPORTANCE_LEVEL.IMPOTANT.key}>{IMPORTANCE_LEVEL.IMPOTANT.value}</Select.Option>
          <Select.Option value={IMPORTANCE_LEVEL.ORDINARY.key}>{IMPORTANCE_LEVEL.ORDINARY.value}</Select.Option>
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{rateImportance}</Col> : rateImportance;
  };
  // Phản hồi KH
  interface FeedbackProps {}
  const Feedback: FC<FeedbackProps> = ({}) => {
    const feedback = (
      <Form.Item
        label="Phản hồi Khách hàng"
        name="customerFeedback"
        rules={[{ required: true, message: 'Phản hồi KH không được để trống.' }]}
      >
        <Select
          onChange={e => {}}
          showSearch
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          placeholder="-- Chọn --"
        >
          <Select.Option value={CUSTOMER_FEEDBACK.NORMAL.key}>{CUSTOMER_FEEDBACK.NORMAL.value}</Select.Option>
          <Select.Option value={CUSTOMER_FEEDBACK.GOOD.key}>{CUSTOMER_FEEDBACK.GOOD.value}</Select.Option>
          <Select.Option value={CUSTOMER_FEEDBACK.NOTGOOD.key}>{CUSTOMER_FEEDBACK.NOTGOOD.value}</Select.Option>
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{feedback}</Col> : feedback;
  };
  //Phân loại ý kiến
  interface CategorizeOpinionsProps {}
  const CategorizeOpinions: FC<CategorizeOpinionsProps> = ({}) => {
    const categorizeOpinions = (
      <Form.Item
        label="Phân loại ý kiến"
        name="opinionType"
        rules={[{ required: true, message: 'Phân loại ý kiến không được để trống.' }]}
      >
        <Select
          onChange={e => {}}
          showSearch
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          placeholder="-- Chọn --"
        >
          <Select.Option value={OPINION_TYPE.PRAISE.key}>{OPINION_TYPE.PRAISE.value}</Select.Option>
          <Select.Option value={OPINION_TYPE.FEEDBACK.key}>{OPINION_TYPE.FEEDBACK.value}</Select.Option>
          <Select.Option value={OPINION_TYPE.REFLECT.key}>{OPINION_TYPE.REFLECT.value}</Select.Option>
          <Select.Option value={OPINION_TYPE.TROUBLE.key}>{OPINION_TYPE.TROUBLE.value}</Select.Option>
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{categorizeOpinions}</Col> : categorizeOpinions;
  };
  //Phương thức liên hệ
  interface ContactMethodProps {}
  const ContactMethod: FC<ContactMethodProps> = ({}) => {
    const contactMethod = (
      <Form.Item
        label="Phương thức liên hệ"
        name="contactMethod"
        rules={[{ required: true, message: 'Phương thức liên hệ không được để trống.' }]}
      >
        <Select
          onChange={e => {}}
          showSearch
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          placeholder="-- Chọn --"
        >
          <Select.Option value={CONTACT_METHOD.CONTACT_SUCCESS.key}>
            {CONTACT_METHOD.CONTACT_SUCCESS.value}
          </Select.Option>
          <Select.Option value={CONTACT_METHOD.CONTACT_FAILURE.key}>
            {CONTACT_METHOD.CONTACT_FAILURE.value}
          </Select.Option>
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{contactMethod}</Col> : contactMethod;
  };
  //Giải trình
  interface ExplanationProps {}
  const Explanation: FC<ExplanationProps> = ({}) => {
    const explanation = (
      <Form.Item
        label="Giải trình"
        name="explaination"
        rules={[{ required: true, message: 'Giải trình của các KP liên quan không được để trống.' }]}
      >
        <TextArea rows={4} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{explanation}</Col> : explanation;
  };
  //Xử lý
  interface ProcessProps {}
  const Process: FC<ProcessProps> = ({}) => {
    const process = (
      <Form.Item
        label="Xử lý"
        name="processContent"
        rules={[{ required: true, message: 'Nội dung xử lý không được để trống.' }]}
      >
        <TextArea rows={4} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{process}</Col> : process;
  };
  //Chi phí xử lý
  interface HandlingCostProps {}
  const HandlingCost: FC<HandlingCostProps> = ({}) => {
    const [currency, setCurrency] = React.useState(currencyOptions[261].value);
    const handlingCost = (
      <Form.Item
        label="Chi phí xử lý"
        name="processCost"
        validateTrigger="onBlur"
        rules={[{ required: true, message: 'Chi phí xử lý không hợp lệ.', pattern: /^(?:\d*)$/ }]}
      >
        <InputNumber
          defaultValue={0}
          style={{
            width: '100%'
          }}
          formatter={currencyFormatter(currency)}
          parser={currencyParser}
        />

        {/*<Select showSearch value={currency} style={{ width: 120, marginTop: '1rem' }} onChange={setCurrency}>
          {currencyOptions.map(opt => (
            <Option key={opt.value} value={opt.value}>
              {opt.label}
            </Option>
          ))}
          </Select>*/}
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{handlingCost}</Col> : handlingCost;
  };
  return React.useMemo(
    () => ({
      form: formInstance,
      Form: WrappedForm,
      Costs,
      CostsType,
      RateImportance,
      Feedback,
      Explanation,
      ContactMethod,
      CategorizeOpinions,
      Process,
      HandlingCost
    }),
    []
  );
}
