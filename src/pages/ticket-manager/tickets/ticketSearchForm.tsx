import moment from 'moment';
import React, { FC } from 'react';
import { useSearchParams } from 'react-router-dom';
import useGetTicketForm from './useGetTicketForm';

interface TicketSearchProps {
  functionOb: any;
  onSearch: (value) => void;
}

const TicketSearchForm: FC<TicketSearchProps> = ({ functionOb, onSearch }) => {
  const [searchParams, setSearchParams] = useSearchParams();
  const { Form, form, FromDate, ToDate, InfoType, Info, Buttons } = useGetTicketForm({
    name: 'searchForm',
    responsive: true
  });

  return (
    <Form>
      <FromDate />
      <ToDate />
      <InfoType />
      <Info />
      <Buttons
        functionOb={functionOb}
        onSearch={() => {
          let fromDate = form.getFieldValue('fromDate');
          let toDate = form.getFieldValue('toDate');
          let infoType = form.getFieldValue('infoType');
          let info = form.getFieldValue('info');
          onSearch({ fromDate, toDate, info, infoType });
          setSearchParams({})
        }}
        resetFields={() => {
          form.resetFields();
          onSearch({});
        }}
      />
    </Form>
  );
};

export default TicketSearchForm;
