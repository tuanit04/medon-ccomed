import React, { FC, useCallback, useEffect, useState } from 'react';
import { Button, Collapse, Modal, Tabs } from 'antd';
import './index.less';
import { Facility } from 'common/interfaces/facility.interface';
import useGetModifyTicketForm from './useGetModifyTicketForm';
import TicketProcessingDialog from './ticketProcessingDialog';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { apiGetTicketTypeList, apiSaveTicketProcess } from 'api/ticket-type/ticketType.api';
import { useGetFacilitys } from 'hooks/facility';
import { apiGetUsersInGroupProcess } from 'api/ticket/ticket.api';
import { Ticket } from 'common/interfaces/ticket.interface';
import { CODE_RESPONSE } from 'constants/response';
import { openNotificationRight } from 'utils/notification';
import { DeliveredProcedureOutlined, HistoryOutlined } from '@ant-design/icons';
import { Channel } from 'common/interfaces/channel.interface';
import { Province } from 'common/interfaces/province.interface';
import { Patient } from 'common/interfaces/patient.interface';
const { Panel } = Collapse;

const userLogined = JSON.parse(String(localStorage.getItem('user')));

interface TicketModifyDialogProps {
  facilityParentList: Facility[];
  visible: boolean;
  onModify: (values: any) => void;
  onProcessingTicket?: (values: any) => void;
  ticket: Ticket;
  provinces: Province[];
  users: any;
  groupProcessList?: any;
  channels: Channel[];
  onCancel: () => void;
}

const TicketModifyDialog: FC<TicketModifyDialogProps> = ({
  onModify,
  onCancel,
  visible,
  groupProcessList,
  channels,
  facilityParentList,
  ticket,
  provinces,
  users,
  onProcessingTicket
}) => {
  const [isShowProcessingDialog, setShowProcessingDialog] = useState(false);
  const [ticketTypes, setTicketTypes]: any = useState([]);
  const [ticketTypeSelected, setTicketTypeSelected] = useState([]);
  const [slaValue, setSlaValue] = useState(0);
  const [ticketProcessRes, setTicketProcessRes]: any = useState();
  const [isMedon, setIsMedon]: any = useState(true);
  const [usersInGroupProcess, setUsersInGroupProcess] = useState([]);
  //Hook Lấy danh sách Quận/Huyện
  const { districts, loadDistricts, isLoadingDistricts } = useGetDistricts();
  //Hook Lấy danh sách đơn vị
  const { facilities, loadFacilities, isLoadingFacilities } = useGetFacilitys();
  //API Lấy danh sách cán bộ xử lý
  const getUsersInGroupProcess = useCallback(async assignGroupId => {
    if (assignGroupId) {
      const data: any = await apiGetUsersInGroupProcess(assignGroupId);
      setUsersInGroupProcess(data?.data);
    } else {
      setUsersInGroupProcess(users);
    }
  }, []);
  //API Lấy danh sách Ticket Type
  const getTicketTypes = useCallback(async () => {
    const data: any = await apiGetTicketTypeList();
    setTicketTypes(data?.data);
    if (data?.data?.length > 0) {
      for (let i = 0; i < data.data.length; i++) {
        if (data.data[i]['id'] === ticket?.ticketType) {
          setTicketTypeSelected(data?.data[i]);
          if (data?.data[i]?.['sla']) {
            for (let j = 0; j < data?.data[i]?.['sla']?.length; j++) {
              if (String(data?.data[i]?.['sla'][j]['level']).toUpperCase() === String(ticket?.level).toUpperCase()) {
                setSlaValue(data?.data[i]?.['sla'][j]['value']);
                break;
              }
            }
          }
          break;
        }
      }
    }
  }, []);

  useEffect(() => {
    if (ticketTypes?.length > 0) {
      for (let i = 0; i < ticketTypes?.length; i++) {
        if (ticketTypes[i]['id'] === ticket?.ticketType) {
          setTicketTypeSelected(ticketTypes[i]);
          if (ticketTypes[i]?.['sla']) {
            for (let j = 0; j < ticketTypes[i]?.['sla'].length; j++) {
              if (String(ticketTypes[i]?.['sla'][j]['level']).toUpperCase() === String(ticket?.level).toUpperCase()) {
                setSlaValue(ticketTypes[i]?.['sla'][j]['value']);
                break;
              }
            }
          }
          break;
        }
      }
    }
  }, [ticketTypes, visible]);

  useEffect(() => {
    loadFacilities({
      variables: {
        filtered: [
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
    getTicketTypes();
  }, []);

  useEffect(() => {
    if (ticket?.provinceId) {
      loadDistricts({
        variables: {
          provinceId: ticket.provinceId
        }
      });
    }
    if (ticket?.assignGroupId) {
      getUsersInGroupProcess(ticket?.assignGroupId);
    }
  }, [ticket]);

  useEffect(() => {
    if (facilities && ticket?.assignFacilityId && ticket) {
      let assignFacility: Facility = facilities.find(item => item.id === ticket?.assignFacilityId);
      if (assignFacility?.code === 'MON') {
        if (!isMedon) {
          setIsMedon(true);
        }
      } else {
        if (isMedon) {
          setIsMedon(false);
        }
      }
    }
  }, [facilities, ticket]);

  const {
    form,
    Form,
    PID,
    PatientCode,
    PatientType,
    PhoneNumber,
    PatientFullname,
    DateofBirth,
    BirthYear,
    Gender,
    Email,
    Address,
    Provinces,
    Districts,
    Note,
    ProcessContent,
    TicketStatus,
    TicketType,
    TicketLevel,
    PatientSID,
    TicketContent,
    FileAttach,
    Evaluate,
    ReflectionUnit,
    ChannelReceive,
    PatientGroup,
    PatientRevenue,
    CreateUser,
    CreateFacility,
    CreateDate,
    UpdateDate,
    LastUpdateUser,
    LastUpdateFacility,
    SLA,
    UsersInGroupProcess,
    GroupProcess,
    HandlerFacility,
    ProcessingTime
  } = useGetModifyTicketForm({
    name: 'modifyTicketForm',
    responsive: true,
    isModal: true,
    ticket: ticket,
    districts: districts,
    visible: visible,
    isCreate: true
  });
  useEffect(() => {
    form.resetFields();
  }, [visible]);

  const callback = key => {
    console.log(key);
  };
  const handleSaveTicketProcess = async values => {
    let ticketProcess = { ticketId: ticket.id, ...values };
    let data = await apiSaveTicketProcess(ticketProcess);
    let statusRes = data?.['code'];
    if (statusRes) {
      if (statusRes === CODE_RESPONSE.SUCCESS) {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Thêm mới xử lý Ticket thành công', 'success');
        setTicketProcessRes(data?.data);
        setShowProcessingDialog(false);
      } else {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Thêm mới xử lý Ticket thất bại.', 'error');
      }
    } else {
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.', 'error');
    }
  };
  const handleChangePatient = (values?: Partial<Patient>) => {
    if (values?.provinceId) {
      loadDistricts({
        variables: {
          provinceId: values.provinceId
        }
      });
    }
    if (values?.districtId) {
      /*loadWards({
        variables: {
          districtId: values.districtId
        }
      });*/
    }
  };
  return (
    <Modal
      maskClosable={false}
      width={1500}
      centered
      title="Cập nhật Ticket"
      visible={visible}
      //onOk={async () => onCreate((await form.validateFields()) as any)}
      onOk={async () => {
        const values = await form.validateFields();
        onModify({
          pid: '',
          sid: '',
          id: ticket.id,
          patientPhone: values['patientPhone'],
          patientName: values['patientName'],
          patient_crm_status: 1,
          patientBirthDate: values['patientBirthDate'],
          patientSex: values['patientSex'],
          customerType: values['customerType'],
          patientEmail: values['patientEmail'],
          address: values['address'],
          provinceId: values['provinceId'],
          districtId: values['districtId'],
          status: values['status'],
          level: values['level'],
          ticketType: values['ticketType'],
          content: values['content'],
          attach: typeof values['attach'] === 'object' ? '' : values['attach'],
          channelType: values['channelType'],
          reflectFacilityId: values['reflectFacilityId'],
          customerRevenue: values['customerRevenue'],
          customerGroup: values['customerGroup'],
          rating: values['rating'],
          processUserId: values['processUserId'],
          assignGroupId: values['assignGroupId'],
          assignFacilityId: values['assignFacilityId'],
          updateUserId: userLogined['id']
        });
      }}
      onCancel={onCancel}
      okText="Cập nhật"
      cancelText="Hủy bỏ"
      style={{ marginTop: '5px' }}
    >
      <Form style={{ marginTop: '2px' }}>
        <div className="first">
          <Collapse defaultActiveKey={['1']} onChange={callback}>
            <Panel header="THÔNG TIN KHÁCH HÀNG" key="1">
              <PID />
              <PatientCode />
              <PatientType />
              <PhoneNumber
                disabledField={false}
                provinces={provinces}
                setDisabledField={() => {}}
                handleChangeProvince={provinceId => {
                  if (provinceId) {
                    loadDistricts({
                      variables: {
                        provinceId: provinceId
                      }
                    });
                  }
                }}
                handleChangePatient={handleChangePatient}
                loadDistricts={loadDistricts}
              />
              <PatientFullname />
              <DateofBirth />
              <Gender />
              <Email />
              <Address />
              <Provinces
                provinceList={provinces}
                isLoading={false}
                handleChangeProvince={provinceId => {
                  loadDistricts({
                    variables: {
                      provinceId: provinceId
                    }
                  });
                }}
              />
              <Districts districtList={districts} isLoading={false} handleChangeDistrict={() => {}} />
            </Panel>
          </Collapse>
          <div style={{ marginTop: '5px', marginBottom: '5px', width: '95%' }}>
            <div style={{ float: 'left' }}>
              <Button danger>Đặt lịch</Button>
            </div>
            <div style={{ float: 'right' }}>
              <Button
                onClick={() => {
                  setShowProcessingDialog(true);
                }}
                type="primary"
                color="#ccc"
              >
                Xử lý Ticket
              </Button>
              &nbsp;&nbsp;
              <Button
                type="primary"
                onClick={async () => {
                  const values = await form.validateFields();
                  onModify({
                    pid: '',
                    sid: '',
                    id: ticket.id,
                    patientPhone: values['patientPhone'],
                    patientName: values['patientName'],
                    patient_crm_status: 1,
                    patientBirthDate: values['patientBirthDate'],
                    patientSex: values['patientSex'],
                    customerType: values['customerType'],
                    patientEmail: values['patientEmail'],
                    address: values['address'],
                    provinceId: values['provinceId'],
                    districtId: values['districtId'],
                    status: values['status'],
                    level: values['level'],
                    ticketType: values['ticketType'],
                    content: values['content'],
                    attach: typeof values['attach'] === 'object' ? '' : values['attach'],
                    channelType: values['channelType'],
                    reflectFacilityId: values['reflectFacilityId'],
                    customerRevenue: values['customerRevenue'],
                    customerGroup: values['customerGroup'],
                    rating: values['rating'],
                    processUserId: values['processUserId'],
                    assignGroupId: values['assignGroupId'],
                    assignFacilityId: values['assignFacilityId'],
                    updateUserId: userLogined['id']
                  });
                }}
              >
                Lưu Ticket
              </Button>
            </div>
            <div style={{ clear: 'both' }}></div>
          </div>
          <Collapse defaultActiveKey={['1']} onChange={callback}>
            <Panel header="THÔNG TIN XỬ LÝ" key="1">
              <UsersInGroupProcess
                usersInGroupProcess={form.getFieldInstance('assignGroupId') ? usersInGroupProcess : users}
              />
              <GroupProcess
                groupProcessList={groupProcessList}
                handleChangeGroupProcess={assignGroupId => {
                  getUsersInGroupProcess(assignGroupId);
                }}
              />
              <HandlerFacility
                handleChangeAssignFacility={id => {
                  let flag = false;
                  for (let i = 0; i < facilities.length; i++) {
                    if (facilities[i]['id'] === id) {
                      if (facilities[i]['code'] === 'MON') {
                        flag = true;
                      }
                    }
                  }
                  if (flag) {
                    setIsMedon(true);
                  } else {
                    setIsMedon(false);
                  }
                }}
                facilities={facilities}
              />
              <CreateUser />
              <CreateFacility />
              <CreateDate />
              <LastUpdateUser />
              <LastUpdateFacility />
              <UpdateDate />
              <SLA slaValue={slaValue} />
              <ProcessingTime slaValue={new Date(slaValue * 60 * 1000).toISOString().substr(11, 8)} />
            </Panel>
          </Collapse>
        </div>
        <div className="second">
          <Collapse defaultActiveKey={['2']} onChange={callback}>
            <Panel header="THÔNG TIN TICKET" key="2">
              <div style={{ float: 'left', width: '50%' }}>
                <TicketStatus />
              </div>
              <div style={{ float: 'right', width: '50%' }}>
                <TicketType
                  handleChangeTicketType={value => {
                    for (let i = 0; i < ticketTypes?.length; i++) {
                      if (ticketTypes[i]['id'] === value) {
                        setTicketTypeSelected(ticketTypes[i]);
                        break;
                      }
                    }
                  }}
                  ticketTypeList={ticketTypes}
                />
              </div>
              <div style={{ float: 'left', width: '50%' }}>
                <TicketLevel
                  handleChangeTicketLevel={value => {
                    let slaArr = ticketTypeSelected['sla'];
                    if (slaArr) {
                      for (let i = 0; i < slaArr.length; i++) {
                        if (String(slaArr[i]['level']).toUpperCase() === String(value).toUpperCase()) {
                          setSlaValue(slaArr[i]['value']);
                          break;
                        }
                      }
                    }
                  }}
                />
              </div>
              <div style={{ float: 'right', width: '50%' }}>
                <PatientSID />
              </div>
              <div style={{ float: 'left', width: '100%' }}>
                <TicketContent />
              </div>
              <div style={{ float: 'right', width: '100%' }}>
                <FileAttach filePath={ticket?.attach} />
              </div>
              <div style={{ float: 'left', width: '50%' }}>
                <ChannelReceive channelList={channels} />
              </div>
              <div style={{ float: 'right', width: '50%' }}>
                <PatientGroup />
              </div>
              {isMedon ? (
                ''
              ) : (
                <div style={{ float: 'left', width: '50%' }}>
                  <ReflectionUnit facilities={facilities} />
                </div>
              )}
              {isMedon ? (
                ''
              ) : (
                <div style={{ float: 'right', width: '50%' }}>
                  <Evaluate />
                </div>
              )}
              {isMedon ? (
                ''
              ) : (
                <div style={{ float: 'left', width: '50%' }}>
                  <PatientRevenue />
                </div>
              )}
              <div style={{ float: 'right', width: '50%' }}></div>
              <div style={{ clear: 'both' }}></div>
              {/*<PatientType2 />*/}
            </Panel>
          </Collapse>
          <div className="card-container" style={{ border: '1px solid #ccc', padding: '10px' }}>
            <Tabs type="card">
              <Tabs.TabPane
                tab={
                  <span>
                    <HistoryOutlined />
                    LỊCH SỬ
                  </span>
                }
                key="1"
              >
                <Note />
              </Tabs.TabPane>
              <Tabs.TabPane
                tab={
                  <span>
                    <DeliveredProcedureOutlined />
                    XỬ LÝ
                  </span>
                }
                key="2"
              >
                <ProcessContent ticketProcessRes={ticketProcessRes} ticketId={ticket?.id} />
              </Tabs.TabPane>
            </Tabs>
          </div>
        </div>
        <TicketProcessingDialog
          visible={isShowProcessingDialog}
          onCancel={() => {
            setShowProcessingDialog(false);
          }}
          onCreate={values => {
            handleSaveTicketProcess(values);
          }}
        />
      </Form>
    </Modal>
  );
};

export default TicketModifyDialog;
