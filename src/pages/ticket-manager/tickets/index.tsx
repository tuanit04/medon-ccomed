import {
  apiCreateTicket,
  apiDeleteTicket,
  apiGetAllActiveGroupProcess,
  apiGetTicketByID,
  apiUpdateTicket
} from 'api/ticket/ticket.api';
import { Facility } from 'common/interfaces/facility.interface';
import { Ticket } from 'common/interfaces/ticket.interface';
import { CODE_RESPONSE } from 'constants/response';
import { useAppState } from 'helpers';
import { useGetChannels } from 'hooks/channel';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetUsersLazy, useGetUserTypeLazy } from 'hooks/users/useGetUser';
import React, { FC, useCallback, useEffect, useState } from 'react';
import { openNotificationRight } from 'utils/notification';
import TicketCreateDialog from './ticketCreateDialog';
import TicketModifyDialog from './ticketModifyDialog';
import TicketSearchForm from './ticketSearchForm';
import TicketTable from './ticketTable';

const TicketPage: FC = () => {
  const functionOb = useAppState(state => state.user.functionObject);
  const [groupProcessList, setGroupProcessList] = useState([]);
  const [mcreateVisible, setCreateVisible] = useState(false);
  const [mModifyVisible, setModifyVisible] = useState(false);
  const [ticketModify, setTicketModify]: any = useState();
  const [ticketRes, setTicketRes]: any = useState();
  const [valueSearch, setValueSearch]: any = useState();
  let sorted = [{ id: 'createDate', desc: 'true', value: '' }];
  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //API Lấy All danh sách cán bộ xử lý
  const { users, isLoadingUsers, loadUsers } = useGetUsersLazy();
  //API Lấy UserType CBTĐ
  const { userType, isLoadingUserType, loadUserType } = useGetUserTypeLazy();
  //Hook Lấy danh sách kênh tiếp nhận
  const { channels, isLoadingChannels, loadChannels } = useGetChannels();
  //API Lấy danh sách nhóm xử lý
  const getAllActiveGroupProcess = useCallback(async () => {
    const data: any = await apiGetAllActiveGroupProcess(sorted);
    setGroupProcessList(data?.data);
  }, []);

  const handleGetTicketById = async ticketId => {
    let ticketDetail = await apiGetTicketByID(ticketId);
    setTicketModify(ticketDetail?.data);
  };
  const handleCreateTicket = async values => {
    let data = await apiCreateTicket(values);
    let statusRes = data?.['code'];
    if (statusRes) {
      if (statusRes === CODE_RESPONSE.SUCCESS) {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Thêm mới Ticket thành công', 'success');
        setTicketRes(data?.data);
      } else {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Thêm mới Ticket thất bại', 'error');
      }
    } else {
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.', 'error');
    }
  };
  const handleUpdateTicket = async values => {
    let data = await apiUpdateTicket(values);
    let statusRes = data?.['code'];
    if (statusRes) {
      if (statusRes === CODE_RESPONSE.SUCCESS) {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Cập nhật Ticket thành công', 'success');
        setTicketRes(data?.data);
      } else {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Cập nhật Ticket thất bại', 'error');
      }
    } else {
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.', 'error');
    }
  };

  const handleDeleteTicketType = async id => {
    let data = await apiDeleteTicket(id);
    let statusRes = data?.['code'];
    if (statusRes) {
      if (statusRes === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Xóa bản ghi thành công.', 'success');
        setTicketRes(data?.data);
      } else {
        openNotificationRight('Xóa bản ghi thất bại.', 'error');
      }
    } else {
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.', 'error');
    }
  };
  useEffect(() => {
    loadUserType();
    getAllActiveGroupProcess();
    loadChannels();
    loadProvinces();
  }, []);
  useEffect(() => {
    if (userType) {
      loadUsers({
        variables: {
          page: 1,
          pageSize: 10000000,
          filtered: [
            {
              id: 'status',
              value: '1',
              operation: '=='
            },
            {
              id: 'userTypeId',
              value: userType[0]['id'],
              operation: '=='
            }
          ],
          sorted: [
            {
              id: 'createDate',
              desc: true
            }
          ]
        }
      });
    }
  }, [userType]);
  return (
    <div className="main-content">
      <TicketSearchForm
        functionOb={functionOb}
        onSearch={value => {
          setValueSearch(value);
        }}
      />
      <TicketTable
        users={users}
        channels={channels}
        groupProcessList={groupProcessList}
        onCreate={() => setCreateVisible(true)}
        onModify={async value => {
          //setTicketModify(value);
          await handleGetTicketById(value['id']);
          setModifyVisible(true);
        }}
        functionOb={functionOb}
        ticketRes={ticketRes}
        valueSearch={valueSearch}
      />
      <TicketCreateDialog
        provinces={provinces}
        users={users}
        channels={channels}
        groupProcessList={groupProcessList}
        facilityParentList={[] as Facility[]}
        visible={mcreateVisible}
        onCancel={() => setCreateVisible(false)}
        onCreate={values => {
          handleCreateTicket(values);
          setCreateVisible(false);
        }}
      />
      <TicketModifyDialog
        provinces={provinces}
        users={users}
        channels={channels}
        groupProcessList={groupProcessList}
        facilityParentList={[] as Facility[]}
        visible={mModifyVisible}
        onCancel={() => setModifyVisible(false)}
        ticket={ticketModify}
        onModify={values => {
          handleUpdateTicket(values);
          setModifyVisible(false);
        }}
      />
    </div>
  );
};
export default TicketPage;
