import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import { Button, Table, Modal, Tag, Input, Space } from 'antd';
import { PlusCircleOutlined, DeleteOutlined, ExclamationCircleOutlined, SearchOutlined } from '@ant-design/icons';
import { functionCodeConstants } from 'constants/functions';
import { apiGetTicketTypeList } from 'api/ticket-type/ticketType.api';
import { TicketType } from 'common/interfaces/ticketType.interface';
import { openNotificationRight } from 'utils/notification';
import Moment from 'moment';
import Highlighter from 'react-highlight-words';
import { TICKET_TYPE_STATUS } from 'constants/ticket-type.constants';
import moment from 'moment';

interface TicketTypeTableProps {
  functionOb: any;
  onCreate: () => void;
  onAuthorize: (row: TicketType) => void;
  onModify: (ticketType: TicketType) => void;
  onDelete: (value: string) => void;
  ticketTypeRes: TicketType;
}

const TicketTypeTable: FC<TicketTypeTableProps> = ({ functionOb, onCreate, onModify, onDelete, ticketTypeRes }) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(20);
  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');
  const [tableData, setTableData] = useState<TicketType[]>();
  const [totalRecord, setTotalRecord] = useState(0);

  const getTicketTypeList = useCallback(async () => {
    const data: any = await apiGetTicketTypeList(page, pageSize);
    setTableData(data?.['data']);
    setTotalRecord(data?.['records']);
  }, []);
  useEffect(() => {
    getTicketTypeList();
  }, [page, pageSize, ticketTypeRes]);
  const [pagination, setPagination] = useState({});

  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    if (pageSize !== pagination['pageSize']) {
      setPage(1);
    } else {
      setPage(pagination['current']);
    }
    setPageSize(pagination['pageSize']);
  };
  const searchInput = useRef<Input>(null);
  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder="Nhập nội dung tìm kiếm"
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm kiếm
          </Button>
          <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Bỏ lọc
          </Button>
          {/*<Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Tìm kiếm
          </Button>*/}
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: text =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      )
  });
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = clearFilters => {
    clearFilters();
    setSearchText('');
  };
  const columns: any = [
    {
      title: 'STT',
      dataIndex: 'STT',
      key: 'STT',
      width: 100,
      align: 'center',
      render: (value, item, index) => index + 1
    },
    {
      title: 'Tên loại',
      dataIndex: 'name',
      key: 'name',
      width: 270,
      ...getColumnSearchProps('name')
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      key: 'status',
      width: 200,
      filters: [
        { text: 'Kích hoạt', value: 1 },
        { text: 'Không kích hoạt', value: 2 }
      ],
      onFilter: (value, record) => record.status === value,
      align: 'center',
      render: status =>
        status === TICKET_TYPE_STATUS.ACTIVE.key ? (
          <Tag color="#2db7f5">{TICKET_TYPE_STATUS.ACTIVE.value}</Tag>
        ) : status === TICKET_TYPE_STATUS.INACTIVE.key ? (
          <Tag color="red">{TICKET_TYPE_STATUS.INACTIVE.value}</Tag>
        ) : (
          <Tag color="#f50">Đã xóa</Tag>
        )
    },
    {
      title: 'Thời gian tạo',
      dataIndex: 'createDate',
      align: 'center',
      key: 'createDate',
      with: 250,
      render: value => moment(value).format('DD/MM/yyyy HH:mm:ss')
    },
    {
      title: 'Người tạo',
      dataIndex: 'createUser',
      align: 'center',
      key: 'createUser',
      with: 300,
      ...getColumnSearchProps('createUser')
    },
    {
      title: 'Ngày sửa',
      dataIndex: 'updateDate',
      align: 'center',
      key: 'updateDate',
      with: 250,
      render: value => (value ? moment(value).format('DD/MM/yyyy HH:mm:ss') : '')
    },
    {
      title: 'Người sửa',
      dataIndex: 'updateUser',
      align: 'center',
      key: 'updateUser',
      with: 300
    },
    {
      title: 'Sửa',
      dataIndex: '',
      align: 'center',
      key: '',
      with: 30,
      render: (_, row) => (
        <>
          <Button
            disabled={functionOb[functionCodeConstants.TD_QLTK_PLTK_THEMMOI_CAPNHAT] ? false : true}
            icon={<DeleteOutlined />}
            onClick={() => {
              onModify(row);
            }}
            type="primary"
          >
            Sửa
          </Button>
        </>
      )
    },
    {
      title: 'Xóa',
      dataIndex: '',
      align: 'center',
      key: '',
      with: 30,
      render: (_, row) => (
        <>
          <Button
            danger
            icon={<DeleteOutlined />}
            disabled={functionOb[functionCodeConstants.TD_QLTK_PLTK_XOA] ? false : true}
            onClick={() => {
              Modal.confirm({
                okText: 'Xóa',
                cancelText: 'Đóng',
                icon: <ExclamationCircleOutlined />,
                title: 'Bạn có chắc chắn muốn xóa bản ghi đang thao tác ?',
                onOk() {
                  onDelete(row['id']);
                  openNotificationRight('Xóa bản ghi thành công.', 'success');
                },
                onCancel() {
                  //console.log('Cancel');
                }
              });
            }}
            type="primary"
          >
            Xóa
          </Button>
        </>
      )
    }
  ];
  return (
    <Table
      pagination={{
        ...pagination,
        defaultPageSize: 20,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '50']
      }}
      title={() => (
        <>
          <div style={{ float: 'left', marginTop: '5px' }}>
            <label className="lblTableTxt">
              <Tag color="#87d068">Tổng số bản ghi : {totalRecord}</Tag>
            </label>
          </div>
          <div style={{ float: 'right' }}>
            <Button
              icon={<PlusCircleOutlined />}
              disabled={functionOb[functionCodeConstants.TD_QLTK_PLTK_THEMMOI_CAPNHAT] ? false : true}
              type="primary"
              onClick={onCreate}
            >
              Thêm mới
            </Button>
          </div>
          <div style={{ clear: 'both' }}></div>
        </>
      )}
      columns={columns}
      dataSource={tableData}
    />
  );
};

export default TicketTypeTable;
