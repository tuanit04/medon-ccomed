import React, { FC, useEffect } from 'react';
import { Modal } from 'antd';
import useGetTicketTypeForm from './useGetTicketTypeForm';
import { TicketType } from 'common/interfaces/ticketType.interface';

interface TicketTypeModifiyDialogProps {
  visible: boolean;
  ticketType: TicketType;
  onModify: (values: TicketType) => void;
  onCancel: () => void;
}

const TicketTypeModifiyDialog: FC<TicketTypeModifiyDialogProps> = ({ onModify, onCancel, ticketType, visible }) => {
  const { form, Form, Status, GroupName, GroupCode } = useGetTicketTypeForm({
    name: 'searchForm',
    responsive: true,
    isModal: true,
    isCreate: false,
    ticketType
  });
  const onSubmit = async () => {
    const valuesModify: any = await form.validateFields();
    onModify({ ...valuesModify });
  };
  useEffect(() => {
    form.resetFields();
  }, [visible]);
  return (
    <Modal
      maskClosable={false}
      width={700}
      title="Cập nhật loại Ticket"
      visible={visible}
      onOk={onSubmit}
      onCancel={onCancel}
      okText="Lưu"
      cancelText="Hủy"
    >
      <Form style={{ marginTop: '10px' }}>
        {/*<GroupCode />*/}
        <GroupName />
        <Status />
      </Form>
    </Modal>
  );
};

export default TicketTypeModifiyDialog;
