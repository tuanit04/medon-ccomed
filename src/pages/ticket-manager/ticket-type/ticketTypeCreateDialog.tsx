import React, { FC, useEffect } from 'react';
import { Modal } from 'antd';
import useGetRTicketTypeForm from './useGetTicketTypeForm';
import { TicketType } from 'common/interfaces/ticketType.interface';

interface TicketTypeCreateDialogProps {
  visible: boolean;
  onCreate: (values: TicketType) => void;
  onCancel: () => void;
}

const TicketTypeCreateDialog: FC<TicketTypeCreateDialogProps> = ({ onCreate, onCancel, visible }) => {
  const { form, Form, Status, GroupName, GroupCode } = useGetRTicketTypeForm({
    name: 'searchForm',
    responsive: true,
    isModal: true,
    isCreate: true
  });
  const onSubmit = async () => {
    const valuesCreate: any = await form.validateFields();
    onCreate({ ...valuesCreate });
  };
  useEffect(() => {
    form.resetFields();
  }, [visible]);
  return (
    <Modal
      maskClosable={false}
      width={700}
      title="Thêm mới loại Ticket"
      visible={visible}
      onOk={onSubmit}
      onCancel={onCancel}
      okText="Lưu"
      cancelText="Hủy"
    >
      <Form style={{ marginTop: '10px' }}>
        {/*<GroupCode />*/}
        <GroupName />
        <Status />
      </Form>
    </Modal>
  );
};

export default TicketTypeCreateDialog;
