import React, { FC, useState } from 'react';
import './index.less';
import TicketTypeTable from './ticketTypeTable';
import TicketTypeSearchForm from './ticketTypeSearchForm';
import TicketTypeModifyDialog from './ticketTypeModifyDialog';
import TicketTypeCreateDialog from './ticketTypeCreateDialog';
import { useAppState } from 'helpers';
import { TicketType } from 'common/interfaces/ticketType.interface';
import { apiCreateTicketType, apiDeleteTicketType } from 'api/ticket-type/ticketType.api';
import { openNotificationRight } from 'utils/notification';
import { CODE_RESPONSE } from 'constants/response';
import { Card } from 'antd';

const TicketTypePage: FC = () => {
  const functionOb = useAppState(state => state.user.functionObject);
  const [mcreateVisible, setCreateVisible] = useState(false);
  const [modifyVisible, setModifyVisible] = useState(false);
  const [authorizeVisible, setAuthorizeVisible] = useState(false);
  const [ticketTypeModify, setTicketTypeModify]: any = useState();
  const [ticketTypeRes, setTicketTypeRes]: any = useState();
  const handleSaveTicketType = async values => {
    let data = await apiCreateTicketType(values);
    let statusRes = data?.['code'];
    if (statusRes) {
      if (statusRes === CODE_RESPONSE.SUCCESS) {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Thêm mới loại ticket thành công.', 'success');
        setTicketTypeRes(data?.data);
      } else {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Thêm mới loại ticket thất bại.', 'error');
      }
    } else {
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.', 'error');
    }
  };
  const handleDeleteTicketType = async id => {
    let data = await apiDeleteTicketType(id);
    let statusRes = data?.['code'];
    if (statusRes) {
      if (statusRes === CODE_RESPONSE.SUCCESS) {
        openNotificationRight('Xóa bản ghi thành công.', 'success');
        setTicketTypeRes(data?.data);
      } else {
        openNotificationRight('Xóa bản ghi thất bại.', 'error');
      }
    } else {
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.', 'error');
    }
  };
  return (
    <div className="main-content">
      {/*<TicketTypeSearchForm functionOb={functionOb} onSearch={value => {}} />*/}
      <Card title="Phân loại Ticket" bordered={false} style={{ width: '100%' }}>
        <TicketTypeTable
          functionOb={functionOb}
          onCreate={() => setCreateVisible(true)}
          onAuthorize={value => {
            setTicketTypeModify(value);
            setAuthorizeVisible(true);
          }}
          onModify={(ticketType: TicketType) => {
            setTicketTypeModify(ticketType);
            setModifyVisible(true);
          }}
          onDelete={async id => {
            handleDeleteTicketType(id);
          }}
          ticketTypeRes={ticketTypeRes}
        />
        <TicketTypeModifyDialog
          visible={modifyVisible}
          ticketType={ticketTypeModify}
          onCancel={() => setModifyVisible(false)}
          onModify={async values => {
            handleSaveTicketType({ ...ticketTypeModify, ...values });
            setModifyVisible(false);
          }}
        />
        <TicketTypeCreateDialog
          visible={mcreateVisible}
          onCancel={() => setCreateVisible(false)}
          onCreate={(values: TicketType) => {
            handleSaveTicketType(values);
            setCreateVisible(false);
          }}
        />
      </Card>
    </div>
  );
};
export default TicketTypePage;
