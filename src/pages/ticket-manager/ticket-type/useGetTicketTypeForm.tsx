import React, { FC, useEffect } from 'react';
import { Form, Input, Col, Row, Select, Button, Badge } from 'antd';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import { SearchOutlined, ClearOutlined } from '@ant-design/icons';
import { functionCodeConstants } from 'constants/functions';
import { TICKET_TYPE_STATUS } from 'constants/ticket-type.constants';
import { TicketType } from 'common/interfaces/ticketType.interface';
const { Option } = Select;
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperCol4Modal: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
  wrapperCol4Modal: { span: 18 }
};

interface AddTicketTypeFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?: boolean;
  isCreate?: boolean;
  /** Initial form data */
  ticketType?: TicketType;
}

interface ITicketTypeFormProps {
  provinceId?: string;
  districtId?: string;
  wardId?: string;
  parentFacilityId?: string;
  routeId?: string;
}

export default function useGetSearchForm({
  required = false,
  responsive = false,
  name = 'form',
  isModal,
  isCreate,
  ticketType
}: AddTicketTypeFormProps) {
  const [formInstance] = Form.useForm<ITicketTypeFormProps>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={isCreate ? { ...ticketType, status: 1 } : ticketType}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };
  useEffect(() => {
    formInstance.resetFields();
  }, [formInstance, ticketType]);

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  const select = (
    <Select>
      <Option value="patientName">Mã loại Ticket</Option>
      <Option value="patientPhone">Tên loại Ticket</Option>
    </Select>
  );

  const Info: FC = () => {
    const info = (
      <Form.Item name="info" label="Giá trị tìm kiếm">
        <Input placeholder="Nhập thông tin tìm kiếm" addonBefore={select} style={{ width: '100%' }} />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{info}</Col> : info;
  };
  //Trang thai
  interface StatusProps {
    handleChangeStatus?: (value: any) => void;
    isSearch?: boolean;
  }
  const Status: FC<StatusProps> = ({ handleChangeStatus, isSearch }) => {
    const status = (
      <Form.Item
        name="status"
        label="Trạng thái"
        rules={[{ required: true, message: 'Trạng thái không được để trống.' }]}
      >
        <Select
          //disabled={isModal && !isCreate}
          defaultValue={isSearch ? '' : 1}
          onChange={handleChangeStatus}
          className={isModal ? '' : ''}
        >
          {!isModal ? (
            <Select.Option value="">
              <Badge color="blue" status="processing" text={TICKET_TYPE_STATUS.ALL.value} />
            </Select.Option>
          ) : (
            ''
          )}
          <Select.Option value={TICKET_TYPE_STATUS.ACTIVE.key}>
            <Badge color="green" status="processing" text={TICKET_TYPE_STATUS.ACTIVE.value} />
          </Select.Option>
          <Select.Option value={TICKET_TYPE_STATUS.INACTIVE.key}>
            <Badge color="red" status="processing" text={TICKET_TYPE_STATUS.INACTIVE.value} />
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{status}</Col> : status;
  };

  const GroupCode: FC = () => {
    const groupCode = (
      <Form.Item rules={[{ required: true, message: 'Mã nhóm không được để trống.' }]} name="code" label="Mã nhóm">
        <Input readOnly={!isCreate} placeholder="Nhập mã nhóm" style={{ width: '100%' }} />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol4Modal}>{groupCode}</Col> : groupCode;
  };

  const GroupName: FC = () => {
    const groupName = (
      <Form.Item rules={[{ required: true, message: 'Tên nhóm không được để trống.' }]} name="name" label="Tên nhóm">
        <Input placeholder="Nhập tên nhóm" style={{ width: '100%' }} />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol4Modal}>{groupName}</Col> : groupName;
  };

  //Buttons
  interface ButtonsProps {
    functionOb: any;
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ functionOb, onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_DP_TIMKIEM_THONGKE] ? false : true}
          onClick={() => {
            onSearch();
          }}
          icon={<SearchOutlined />}
          type="primary"
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_DP_TIMKIEM_THONGKE] ? false : true}
          onClick={resetFields}
          icon={<ClearOutlined />}
        >
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };

  //Blank component
  const BlankComponent: FC = () => {
    const blankComponent = <Form.Item></Form.Item>;
    return responsive ? <Col {...wrapperCol}>{blankComponent}</Col> : blankComponent;
  };

  return {
    form: formInstance,
    Form: WrappedForm,
    Status,
    GroupCode,
    GroupName,
    Buttons,
    BlankComponent,
    Info
  };
}
