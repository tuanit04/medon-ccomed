import React, { FC } from 'react';
import useGetStreetForm from './useGetTicketTypeForm';

interface TicketTypeSearchProps {
  functionOb: any;
  onSearch: ({}) => void;
}

const TicketTypeSearchForm: FC<TicketTypeSearchProps> = ({ functionOb, onSearch }) => {
  const { Form, form, Info, Status, Buttons } = useGetStreetForm({
    name: 'searchForm',
    responsive: true
  });

  return (
    <Form>
      <Info />
      <Status
        handleChangeStatus={data => {
          if (data === '0') {
            data = '';
          }
        }}
        isSearch={true}
      />
      <Buttons
        functionOb={functionOb}
        onSearch={() => {
          let name = form.getFieldValue('name');
          let code = form.getFieldValue('code');
          let status = form.getFieldValue('status');
          onSearch({ name, code, status });
        }}
        resetFields={() => {
          form.resetFields();
          onSearch({ name: '', code: '', status: '' });
        }}
      />
    </Form>
  );
};

export default TicketTypeSearchForm;
