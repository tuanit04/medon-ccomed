import { SaveOutlined } from '@ant-design/icons';
import { Button, Card, Form, Input, InputNumber, Popconfirm, Table, Typography } from 'antd';
import { apiGetSlaOfTicketType, apiGetTicketTypeList, apiSaveSlaOfTicketType } from 'api/ticket-type/ticketType.api';
import { functionCodeConstants } from 'constants/functions';
import { CODE_RESPONSE } from 'constants/response';
import { useAppState } from 'helpers';
import React, { FC, useCallback, useEffect, useState } from 'react';
import { openNotificationRight } from 'utils/notification';

const EditableCell = ({ editing, dataIndex, title, inputType, record, index, children, ...restProps }) => {
  const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;
  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0
          }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`
            }
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};
const SLAPage: FC = () => {
  const functionOb = useAppState(state => state.user.functionObject);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(20);
  const [originData, setOriginData]: any = useState([]);
  const [dataRes, setDataRes] = useState<any>();
  const [totalRecord, setTotalRecord] = useState(0);
  const getTicketTypeList = useCallback(async () => {
    const dataRes: any = await apiGetTicketTypeList(page, pageSize);
    setDataRes(dataRes);
    let arrSLA: any = [];
    for (let i = 0; i < dataRes?.data?.length; i++) {
      const sla: any = await apiGetSlaOfTicketType(dataRes?.data[i]['id']);
      let slaArr: [] = sla?.data['sla'];
      let slaObj: any = [];
      if (sla?.data?.['sla']) {
        slaObj = [...sla?.data?.['sla']];
      }
      if (slaObj.length > 0) {
        arrSLA.push({
          ...dataRes?.data[i],
          key: `${dataRes?.data[i]['id']}`,
          name: `${dataRes?.data[i]['name']}`,
          low: slaObj[0]['value'],
          medium: slaObj[1]['value'],
          high: slaObj[2]['value']
        });
      } else {
        arrSLA.push({
          ...dataRes?.data[i],
          key: `${dataRes?.data[i]['id']}`,
          name: `${dataRes?.data[i]['name']}`,
          low: 0,
          medium: 0,
          high: 0
        });
      }
    }
    setOriginData(arrSLA);
  }, []);
  const handleSaveSlaOfTicketType = async values => {
    let data = await apiSaveSlaOfTicketType(values);
    let statusRes = data?.['code'];
    if (statusRes) {
      if (statusRes === CODE_RESPONSE.SUCCESS) {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Cập nhật SLA thành công.', 'success');
        getTicketTypeList();
      } else {
        openNotificationRight('Cập nhật SLA thất bại.', 'error');
        getTicketTypeList();
      }
    } else {
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.', 'error');
    }
  };
  useEffect(() => {
    getTicketTypeList();
  }, [page, pageSize]);
  const [form] = Form.useForm();
  const [editingKey, setEditingKey] = useState('');

  const isEditing = record => record.key === editingKey;

  const edit = record => {
    form.setFieldsValue({
      name: '',
      age: '',
      address: '',
      ...record
    });
    setEditingKey(record.key);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const save = async key => {
    try {
      const row = await form.validateFields();
      const newData = [...originData];
      const index = newData.findIndex(item => key === item.key);

      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, { ...item, ...row });
        setOriginData(newData);
        setEditingKey('');
      } else {
        newData.push(row);
        setOriginData(newData);
        setEditingKey('');
      }
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };
  const columns = [
    {
      title: 'Thời gian xử lý SLA',
      dataIndex: 'name',
      width: '20%'
    },
    {
      title: 'Thấp',
      dataIndex: 'low',
      align: 'center' as 'center',
      width: '20%',
      editable: true
    },
    {
      title: 'Bình thường',
      dataIndex: 'medium',
      align: 'center' as 'center',
      width: '20%',
      editable: true
    },
    {
      title: 'Cao',
      dataIndex: 'high',
      align: 'center' as 'center',
      width: '20%',
      editable: true
    },
    {
      title: 'Hành động',
      align: 'center' as 'center',
      dataIndex: 'operation',
      width: '10%',
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <a
              href="javascript:;"
              onClick={() => save(record.key)}
              style={{
                marginRight: 8,
                color: '#09a2fd',
                fontWeight: 'bold'
              }}
            >
              Cập nhật
            </a>
            <Popconfirm title="Bạn có chắc chắn hủy ?" okText="Có" cancelText="Không" onConfirm={cancel}>
              <a style={{ color: 'red', fontWeight: 'bold' }}>Hủy</a>
            </Popconfirm>
          </span>
        ) : (
          <Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)}>
            Chỉnh sửa
          </Typography.Link>
        );
      }
    }
  ];
  const mergedColumns = columns.map(col => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: record => ({
        record,
        inputType: col.dataIndex === 'age' ? 'number' : 'text',
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record)
      })
    };
  });
  return (
    <div className="main-content">
      <Card title="Quản lý SLA" bordered={false} style={{ width: '100%' }}>
        <Form form={form} component={false}>
          <Table
            components={{
              body: {
                cell: EditableCell
              }
            }}
            bordered
            dataSource={originData}
            columns={mergedColumns}
            rowClassName="editable-row"
            pagination={false}
          />
          <div style={{ padding: 5, float: 'right' }}>
            <Button
              disabled={functionOb[functionCodeConstants.TD_QLTK_QLSLA_CAPNHAT] ? false : true}
              onClick={() => {
                let arrSend: any = [];
                for (let i = 0; i < originData?.length; i++) {
                  let sla: any = [
                    {
                      level: 'LOW',
                      value: originData[i]['low']
                    },
                    {
                      level: 'MEDIUM',
                      value: originData[i]['medium']
                    },
                    {
                      level: 'HIGH',
                      value: originData[i]['high']
                    }
                  ];
                  arrSend.push({
                    ...originData[i],
                    sla
                  });
                }
                handleSaveSlaOfTicketType(arrSend);
              }}
              icon={<SaveOutlined />}
              type="primary"
            >
              Lưu lại
            </Button>
          </div>
          <div style={{ clear: 'both' }}></div>
        </Form>
      </Card>
    </div>
  );
};
export default SLAPage;
