import { Table } from 'antd';
import { Appointment } from 'common/interfaces/appointment.interface';
import { getStatus } from 'pages/advisory-manager/advisory-input/utils';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { QueryLazyOptions } from '@apollo/client';

interface Props {
  responseData?: IResponse<Appointment[]>;
  loading?: boolean;
  getData: (options?: QueryLazyOptions<IGraphQLRequest> | undefined) => void;
  profiles?: IResponse<IPatientProfile[]>;
  selectedProfiles?: IPatientProfile;
}

const AppointmentTable = ({ responseData, loading, getData, profiles, selectedProfiles }: Props) => {
  const [filteredValue, setFilteredValue] = useState('');

  useEffect(() => {
    if (profiles?.data) {
      setFilteredValue(profiles.data.map(val => val.id).join(','));
    }
  }, [profiles]);

  useEffect(() => {
    if (selectedProfiles) {
      setFilteredValue(selectedProfiles.id);
    }
  }, [selectedProfiles]);

  const onChangePage = (page?: number, pageSize?: number) => {
    getData({
      variables: {
        page,
        pageSize,
        filtered: [
          {
            id: 'patientId',
            value: filteredValue,
            operation: 'in'
          }
        ]
      }
    });
  };

  return (
    <Table
      pagination={{
        current: responseData?.page,
        pageSize: 10,
        total: responseData?.records,
        onChange: onChangePage
      }}
      dataSource={responseData?.data}
      loading={loading}
      scroll={{ y: 300 }}
    >
      <Table.Column<Appointment>
        align="center"
        width={50}
        title="STT"
        render={(_, __, index) => {
          return responseData?.page ? (responseData.page - 1) * 10 + index + 1 : index + 1;
        }}
      />
      <Table.Column<Appointment> width={100} title="Mã lịch hẹn" dataIndex="appointmentId" ellipsis />
      <Table.Column<Appointment>
        width={100}
        title="Trạng thái"
        align="center"
        dataIndex="status"
        render={value => getStatus(value)}
      />
      <Table.Column<Appointment>
        width={100}
        title="Ngày đặt lịch"
        align="center"
        dataIndex="appointmentDate"
        render={value => moment(value, 'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY')}
      />
      <Table.Column<Appointment> width={100} title="Khung giờ" align="center" dataIndex="workTimeName" />
      <Table.Column<Appointment> width={150} title="Tên khách hàng" dataIndex="patientName" />
      <Table.Column<Appointment> align="center" title="Giới tính" dataIndex="patientSex" width={100} />
      <Table.Column<Appointment> width={100} align="center" title="Số điện thoại" dataIndex="patientPhone" />
      <Table.Column<Appointment>
        width={100}
        align="center"
        title="Loại lịch"
        dataIndex="methodType"
        render={value => (value === 'HOME' ? 'Tại nhà' : value === 'HOSPITAL' ? 'Tại viện' : '')}
      />
      <Table.Column<ListCall>
        width={100}
        align="center"
        title="Hành động"
        dataIndex="appointmentId"
        render={value => {
          return <Link to={`/appointment-management/appointment?id=${value}`} target="_blank">Xem chi tiết</Link>;
        }}
      />
    </Table>
  );
};
export default AppointmentTable;
