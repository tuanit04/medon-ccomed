import { DownCircleOutlined, UpCircleOutlined } from '@ant-design/icons';
import { Space, Table, Tooltip } from 'antd';
import { CallType } from 'constants/enum';
import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';

interface Props {
  responseData?: IResponse<ListCall[]>;
  loading?: boolean;
  getData: (
    params?: {
      patientIds?: string[] | undefined;
    } & IRequest
  ) => void;
  profiles?: IResponse<IPatientProfile[]>;
  selectedProfiles?: IPatientProfile;
}

const CallTable = ({ responseData, loading, getData, profiles, selectedProfiles }: Props) => {
  const [patientIds, setPatientIds] = useState<string[]>();

  useEffect(() => {
    if (profiles?.data) {
      setPatientIds(profiles.data.map(val => val.id));
    }
  }, [profiles]);

  useEffect(() => {
    if (selectedProfiles) {
      setPatientIds([selectedProfiles.id]);
    }
  }, [selectedProfiles]);

  const onChangePage = (pageNo?: number, pageSize?: number) => {
    getData({
      pageNo,
      pageSize,
      patientIds
    });
  };

  return (
    <Table
      loading={loading}
      dataSource={responseData?.data}
      scroll={{ y: 300 }}
      pagination={{
        current: responseData?.page,
        pageSize: 10,
        total: responseData?.records,
        onChange: onChangePage
      }}
    >
      <Table.Column<ListCall>
        align="center"
        width={50}
        title="STT"
        render={(_, __, index) => {
          return responseData?.page ? (responseData.page - 1) * 10 + index + 1 : index + 1;
        }}
      />
      <Table.Column<ListCall>
        width={150}
        title="Loại cuộc gọi"
        dataIndex="direct"
        render={val => (
          <Space>
            {val === CallType.INCOMING && (
              <>
                <DownCircleOutlined className="incoming-call" /> Cuộc gọi đến
              </>
            )}
            {val === CallType.OUTGOING && (
              <>
                <UpCircleOutlined className="outgoing-call" /> Cuộc gọi đi
              </>
            )}
          </Space>
        )}
      />
      <Table.Column<ListCall>
        width={100}
        title="Trạng thái"
        dataIndex="status"
        className="status-cell"
        render={(_, record) =>
          record.answerTime == null || record.endTime == null ? (
            <div className={'status-cell-failed'}>Không thành công</div>
          ) : (
            <div className={'status-cell-success'}>Thành công</div>
          )
        }
      />
      <Table.Column<ListCall>
        width={100}
        title="Thời gian gọi"
        align="center"
        dataIndex="callTime"
        render={value => value && moment(new Date(value)).format('DD/MM/YYYY HH:mm:ss')}
      />
      <Table.Column<ListCall> width={100} title="Mã khách hàng" align="center" dataIndex="id" ellipsis />
      <Table.Column<ListCall> width={150} title="Tên khách hàng" align="center" dataIndex="patientName" />
      <Table.Column<ListCall> align="center" title="Ext" width={50} dataIndex="ext" />
      <Table.Column<ListCall> width={100} align="center" title="AgentID" dataIndex="agentId" />
      <Table.Column<ListCall> width={100} align="center" title="Phân loại" dataIndex="callTypeName" />
      <Table.Column<ListCall>
        width={150}
        title="Nội dung"
        dataIndex="content"
        ellipsis
        render={value => {
          return <Tooltip title={value}>{value}</Tooltip>;
        }}
      />
      <Table.Column<ListCall>
        width={100}
        align="center"
        title="Hành động"
        dataIndex="id"
        render={value => {
          return (
            <Link to={`/call-management/call?id=${value}`} target="_blank">
              Xem chi tiết
            </Link>
          );
        }}
      />
    </Table>
  );
};
export default CallTable;
