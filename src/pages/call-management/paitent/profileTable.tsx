import { FormInstance, Table } from 'antd';
import React from 'react';
import moment from 'moment';
import { convertPhoneNumber } from 'utils/helper';

interface Props {
  data?: IPatientProfile[];
  form: FormInstance<IPatientDialogForm>;
  selectedRow?: IPatientProfile;
  setSelectedRow: React.Dispatch<React.SetStateAction<IPatientProfile | undefined>>;
  loading?: boolean;
}

const ProfileTable = ({ form, data, selectedRow, setSelectedRow, loading }: Props) => {
  return (
    <Table
      loading={loading}
      dataSource={data}
      rowSelection={{
        selectedRowKeys: selectedRow?.id ? [selectedRow.id] : [],
        onChange: (_, selectedRows) => {
          const data = selectedRows?.[0];
          if (selectedRow?.id !== data.id) {
            setSelectedRow(data);
            form.setFieldsValue({
              patientId: data.id,
              address: data.address,
              patientName: data.name,
              patientSex: data.sex,
              patientPhone: convertPhoneNumber(data.phone),
              patientCrmType: data.patientCrmType,
              patientBirthDate: moment(data.birthDate)
            });
          } else {
            setSelectedRow(undefined);
            form.setFieldsValue({
              patientId: '',
              address: '',
              patientName: '',
              patientSex: '',
              patientCrmType: ''
            });
          }
        },
        type: 'radio'
      }}
      scroll={{ y: 300 }}
      rowKey={row => row.id}
      onRow={data => ({
        onClick: () => {
          if (selectedRow?.id !== data.id) {
            setSelectedRow(data);
            form.setFieldsValue({
              patientId: data.id,
              address: data.address,
              patientName: data.name,
              patientSex: data.sex,
              patientPhone: convertPhoneNumber(data.phone),
              patientCrmType: data.patientCrmType,
              patientBirthDate: moment(data.birthDate)
            });
          } else {
            setSelectedRow(undefined);
            form.setFieldsValue({ patientId: '', address: '', patientName: '', patientSex: '', patientCrmType: '' });
          }
        }
      })}
    >
      <Table.Column<IPatientProfile> align="center" width={100} title="Mã profile KH" dataIndex="id" ellipsis />
      <Table.Column<IPatientProfile> width={100} title="Tên khách hàng" dataIndex="name" />
      <Table.Column<IPatientProfile>
        width={50}
        title="Giới tính"
        align="center"
        dataIndex="sex"
        render={value => {
          return value === 'MALE' ? 'Nam' : value === 'FEMALE' ? 'Nữ' : '';
        }}
      />
      <Table.Column<IPatientProfile>
        width={60}
        title="Số điện thoại"
        align="center"
        dataIndex="phone"
        render={value => convertPhoneNumber(value)}
      />
      <Table.Column<IPatientProfile>
        width={60}
        title="Ngày sinh"
        align="center"
        dataIndex="birthDate"
        render={value => {
          return moment(value).format('DD/MM/YYYY');
        }}
      />
      <Table.Column<IPatientProfile> width={150} title="Địa chỉ" dataIndex="address" />
      <Table.Column<IPatientProfile> align="center" title="Thành phố" width={100} dataIndex="provinceName" />
      <Table.Column<IPatientProfile> width={120} align="center" title="Quận huyện" dataIndex="districtName" />
    </Table>
  );
};
export default ProfileTable;
