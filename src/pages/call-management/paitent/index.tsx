import { Badge, Button, Col, ColProps, DatePicker, Form, Input, Modal, Row, Select, Space, Spin, Tabs } from 'antd';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import AppointmentTable from './appointmentTable';
import CallTable from './callTable';
import NotificationTable from './notificationTable';
import ProfileTable from './profileTable';
import SmsTable from './smsTable';
import TicketTable from './ticketTable';
import './index.less';
import { LoadingOutlined } from '@ant-design/icons';
import { useAppState } from 'helpers';
import { onOpenDetailDialog, saveCallDialogAction } from 'store/dialer.store';
import { useDispatch } from 'react-redux';
import useRestQuery from 'hooks/useRestQuery';
import { baseURL, callEndpoint, getPatientByPhoneUrl } from 'config/api';
import { useGetAppointmentsLazy } from 'hooks/appointment';
import { Ticket } from 'common/interfaces/ticket.interface';
import { CRM_TYPES } from 'constants/profile.constants';
import AppointmentDialog from './appointmentDialog';
import TicketDialog from './ticketDialog';

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};

const labelColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 6,
  xl: 6,
  xxl: 6
};

const formProperties: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 18,
  xl: 18,
  xxl: 18
};

const labelColContent: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 2,
  xl: 2,
  xxl: 2
};

const wrapperColContent: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 22,
  xl: 22,
  xxl: 22
};
const { Option } = Select;
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
interface PatientDialogProps {}

const antIcon = <LoadingOutlined style={{ fontSize: 12 }} spin />;

export default function PatientDialog() {
  const dispatch = useDispatch();
  const [selectedRow, setSelectedRow] = useState<IPatientProfile>();
  const [form] = Form.useForm<IPatientDialogForm>();
  const [ticketPage, setTicketPage] = useState(1);
  const [openTicketModal, setOpenTicketModal] = useState(false);
  const [openAppointmentModal, setOpenAppointmentModal] = useState(false);
  const { phone, openDetailDialog } = useAppState(state => state.dialer);

  const { data: callData, call: getCallData, isLoading: callDataLoading } = useRestQuery<
    IResponse<ListCall[]>,
    { patientIds?: string[] } & IRequest
  >({
    url: callEndpoint + '/findListCallByPatientIds',
    method: 'post',
    initialCall: false
  });
  const { data: profiles, call: getProfiles, isLoading: profilesLoading } = useRestQuery<
    IResponse<IPatientProfile[]>,
    FormData
  >({
    url: getPatientByPhoneUrl,
    method: 'post',
    initialCall: false
  });
  const { loadAppointments: getAppointment, pagingAppointments, isLoadingAppointments } = useGetAppointmentsLazy({
    page: 1,
    pageSize: 10
  });

  const { data: tickets, call: getTickets, isLoading: ticketsLoading } = useRestQuery<IResponse<Ticket[]>, IRequest>({
    url: baseURL + `/ticket/findAllTicket/${ticketPage}/10`,
    method: 'post',
    initialCall: false,
    initialParams: {
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    }
  });

  const { data: callTypes } = useRestQuery<IResponse<ICallType[]>>({
    url: callEndpoint + '/getAllCallType'
  });

  const { data: callGroups } = useRestQuery<IResponse<ICallType[]>>({
    url: callEndpoint + '/getAllCallGroup'
  });

  const onSubmit = async () => {
    const values = await form.validateFields();
    console.log(`values`, values);
    dispatch(saveCallDialogAction({ ...values, patientId: selectedRow?.id }));
  };

  const getData = (_profiles?: IPatientProfile[], _selectedRow?: IPatientProfile) => {
    let patientIds: string[] = [];
    if (_selectedRow) {
      patientIds = [_selectedRow.id];
    } else if (_profiles) {
      patientIds = _profiles.map(val => val.id);
    }

    if (patientIds.length > 0) {
      getCallData({
        pageNo: 1,
        pageSize: 10,
        patientIds
      });
      getAppointment({
        variables: {
          page: 1,
          pageSize: 10,
          filtered: [
            {
              id: 'patientId',
              value: patientIds.join(','),
              operation: 'in'
            }
          ]
        }
      });
      getTickets({
        pageNo: 1,
        pageSize: 10,
        filtered: [
          {
            id: 'patientId',
            value: patientIds.join(','),
            operation: 'in'
          }
        ],
        sorted: [
          {
            id: 'createDate',
            desc: true
          }
        ]
      });
    }
  };

  useEffect(() => {
    if (phone) {
      let formData = new FormData();
      formData.append('phone', phone);
      getProfiles(formData);
      form.setFieldsValue({
        patientPhone: phone
      });
      setSelectedRow(undefined);
    }
  }, [phone]);

  useEffect(() => {
    getData(profiles?.data);
  }, [profiles]);

  useEffect(() => {
    getData(profiles?.data, selectedRow);
  }, [selectedRow]);

  return (
    <>
      <Modal
        className="patient-dialog"
        maskClosable={false}
        width={1500}
        visible={openDetailDialog}
        cancelText="Hủy"
        okButtonProps={{ hidden: true }}
        title="Chi tiết khách hàng"
        onCancel={() => {
          dispatch(onOpenDetailDialog(false));
        }}
        style={{
          top: 16
        }}
      >
        <Form
          {...formItemLayout}
          form={form}
          onFinish={values => {
            console.log(`values`, values);
          }}
        >
          <Row className="field-css">
            <Col {...formProperties} className="over-hidden">
              <Row className="field-css">
                <Col {...labelColTowItem} className="fs-12">
                  Họ tên <span className="red">* </span>:
                </Col>
                <Col {...wrapperColTowItem} className="over-hidden">
                  <Form.Item name="patientName" rules={[{ required: true, message: 'Họ tên không được để trống.' }]}>
                    <Input disabled={selectedRow != null} />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col {...formProperties} className="over-hidden">
              <Row className="field-css">
                <Col {...labelColTowItem} className="fs-12">
                  SĐT <span className="red">* </span>:
                </Col>
                <Col {...wrapperColTowItem} className="over-hidden">
                  <Form.Item
                    name="patientPhone"
                    rules={[{ required: true, message: 'Số điện thoại không được để trống.' }]}
                  >
                    <Input disabled />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col {...formProperties} className="over-hidden">
              <Row className="field-css">
                <Col {...labelColTowItem} className="fs-12">
                  Ngày sinh <span className="red">* </span>:
                </Col>
                <Col {...wrapperColTowItem} className="over-hidden">
                  <Form.Item
                    name="patientBirthDate"
                    rules={[{ required: true, message: 'Ngày sinh không được để trống.' }]}
                  >
                    <DatePicker
                      placeholder="Chọn ngày sinh"
                      disabledDate={current => {
                        return current && current > moment().add(0, 'day');
                      }}
                      format={'DD/MM/YYYY'}
                      disabled={selectedRow != null}
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col {...formProperties} className="over-hidden">
              <Row className="field-css">
                <Col {...labelColTowItem} className="fs-12">
                  Giới tính <span className="red">* </span>:
                </Col>
                <Col {...wrapperColTowItem} className="over-hidden">
                  <Form.Item name="patientSex" rules={[{ required: true, message: 'Giới tính không được để trống.' }]}>
                    <Select placeholder="-- Chọn Giới tính --" disabled={selectedRow != null}>
                      <Option value="MALE">Nam</Option>
                      <Option value="FEMALE">Nữ</Option>
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col {...formProperties} className="over-hidden">
              <Row className="field-css">
                <Col {...labelColTowItem} className="fs-12">
                  Địa chỉ :
                </Col>
                <Col {...wrapperColTowItem} className="over-hidden">
                  <Form.Item name="address">
                    <Input disabled={selectedRow != null} />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col {...formProperties} className="over-hidden">
              <Row className="field-css">
                <Col {...labelColTowItem} className="fs-12">
                  Phân loại KH:
                </Col>
                <Col {...wrapperColTowItem} className="over-hidden">
                  {CRM_TYPES.find(val => val.value === form.getFieldValue('patientCrmType'))?.name}
                  {/* <Form.Item
                    name="patientCrmType"
                    rules={[{ required: true, message: 'Phân loại KH không được để trống.' }]}
                  >
                    <Select placeholder="-- Chọn Loại KH --">
                      {CRM_TYPES.map(val => (
                        <Option key={val.value} value={val.value}>
                          {val.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item> */}
                </Col>
              </Row>
            </Col>
            <Tabs type="card">
              <TabPane
                tab={
                  <Space>
                    Profile KH
                    <Badge
                      size="small"
                      count={
                        profilesLoading ? (
                          <Spin indicator={antIcon} />
                        ) : (
                          profiles?.records || profiles?.data?.length || 0
                        )
                      }
                    />
                  </Space>
                }
                key="1"
              >
                <ProfileTable
                  form={form}
                  data={profiles?.data}
                  selectedRow={selectedRow}
                  setSelectedRow={setSelectedRow}
                  loading={profilesLoading}
                />
              </TabPane>
              <TabPane
                tab={
                  <Space>
                    Lịch hẹn
                    <Badge
                      size="small"
                      count={isLoadingAppointments ? <Spin indicator={antIcon} /> : pagingAppointments?.records || 0}
                    />
                  </Space>
                }
                key="2"
              >
                <AppointmentTable
                  responseData={pagingAppointments}
                  loading={isLoadingAppointments}
                  getData={getAppointment}
                  profiles={profiles}
                  selectedProfiles={selectedRow}
                />
              </TabPane>
              <TabPane
                tab={
                  <Space>
                    Cuộc gọi
                    <Badge
                      size="small"
                      count={
                        callDataLoading ? (
                          <Spin indicator={antIcon} />
                        ) : (
                          callData?.records || callData?.data?.length || 0
                        )
                      }
                    />
                  </Space>
                }
                key="3"
              >
                <CallTable
                  responseData={callData}
                  loading={callDataLoading}
                  getData={getCallData}
                  profiles={profiles}
                  selectedProfiles={selectedRow}
                />
              </TabPane>
              <TabPane
                tab={
                  <Space>
                    Ticket
                    <Badge
                      size="small"
                      count={
                        ticketsLoading ? <Spin indicator={antIcon} /> : tickets?.records || tickets?.data?.length || 0
                      }
                    />
                  </Space>
                }
                key="4"
              >
                <TicketTable
                  profiles={profiles}
                  selectedProfiles={selectedRow}
                  responseData={tickets}
                  loading={ticketsLoading}
                  getData={getTickets}
                  page={ticketPage}
                  setPage={setTicketPage}
                />
              </TabPane>
              <TabPane
                tab={
                  <Space>
                    SMS
                    <Badge size="small" count={0} />
                  </Space>
                }
                key="5"
                disabled
              >
                <SmsTable />
              </TabPane>
              <TabPane
                tab={
                  <Space>
                    Notification
                    <Badge size="small" count={0} />
                  </Space>
                }
                key="6"
                disabled
              >
                <NotificationTable />
              </TabPane>
            </Tabs>
            <Col span={24} className="over-hidden">
              <Row className="mt-10px">
                <Col {...labelColContent} className="fs-12">
                  Nội dung trao đổi :
                </Col>
                <Col {...wrapperColContent} className="over-hidden">
                  <Form.Item name="content">
                    <TextArea rows={5} placeholder="Nhập Ghi chú" />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col {...formProperties} className="over-hidden">
              <Row className="field-css">
                <Col {...labelColTowItem} className="fs-12">
                  Phân loại cuộc gọi :
                </Col>
                <Col {...wrapperColTowItem} className="over-hidden">
                  <Form.Item name="callTypeId">
                    <Select placeholder="-- Chọn --">
                      {callTypes?.data?.map(val => (
                        <Option key={val.id} value={val.id}>
                          {val.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col {...formProperties} className="over-hidden">
              <Row className="field-css">
                <Col {...labelColTowItem} className="fs-12">
                  Nhóm cuộc gọi :
                </Col>
                <Col {...wrapperColTowItem} className="over-hidden">
                  <Form.Item name="callGroupId">
                    <Select placeholder="-- Chọn --">
                      {callGroups?.data?.map(val => (
                        <Option key={val.id} value={val.id}>
                          {val.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col className="txt-rigth" span={24}>
              <Button className="btn-yellow mr-10px" type="primary" onClick={() => setOpenTicketModal(true)}>
                Tạo ticket
              </Button>
              <Button className="mr-10px btn-green" type="primary" onClick={() => setOpenAppointmentModal(true)}>
                Tạo lịch hẹn
              </Button>
              <Button className=" btn-blue" type="default" onClick={onSubmit}>
                Lưu và đóng
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>
      <TicketDialog
        form={form}
        openTicketModal={openTicketModal}
        setOpenTicketModal={setOpenTicketModal}
        onSubmit={onSubmit}
      />
      <AppointmentDialog
        form={form}
        openAppointmentModal={openAppointmentModal}
        setOpenAppointmentModal={setOpenAppointmentModal}
        onSubmit={onSubmit}
      />
    </>
  );
}
