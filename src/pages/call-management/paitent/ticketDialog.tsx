import React, { useEffect } from 'react';
import { FormInstance } from 'antd';
import { apiUpdateTicket } from 'api/ticket/ticket.api';
import { Ticket } from 'common/interfaces/ticket.interface';
import { CODE_RESPONSE } from 'constants/response';
import TicketModifyDialog from 'pages/ticket-manager/tickets/ticketModifyDialog';
import { openNotificationRight } from 'utils/notification';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetUsersLazy, useGetUserTypeLazy } from 'hooks/users/useGetUser';
import { useGetChannels } from 'hooks/channel';

interface Props {
  form: FormInstance<IPatientDialogForm>;
  openTicketModal: boolean;
  setOpenTicketModal: React.Dispatch<React.SetStateAction<boolean>>;
  onSubmit: () => void;
}

const TicketDialog = ({ form, openTicketModal, setOpenTicketModal, onSubmit }: Props) => {
  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //API Lấy All danh sách cán bộ xử lý
  const { users, isLoadingUsers, loadUsers } = useGetUsersLazy();
  //API Lấy UserType CBTĐ
  const { userType, isLoadingUserType, loadUserType } = useGetUserTypeLazy();
  //Hook Lấy danh sách kênh tiếp nhận
  const { channels, isLoadingChannels, loadChannels } = useGetChannels();
  const handleUpdateTicket = async values => {
    let data = await apiUpdateTicket(values);
    let statusRes = data?.data?.['code'];
    if (statusRes) {
      if (statusRes === CODE_RESPONSE.SUCCESS) {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Cập nhật Ticket thành công');
        // setTicketRes(data?.data?.['data']);
        onSubmit();
      } else {
        //openNotificationRight(data?.data?.['message']);
        openNotificationRight('Cập nhật Ticket thất bại');
      }
    } else {
      openNotificationRight('Có lỗi xảy ra trong quá trình xử lý.', 'error');
    }
  };

  useEffect(() => {
    loadUserType();
    loadChannels();
    loadProvinces();
  }, []);
  useEffect(() => {
    if (userType) {
      loadUsers({
        variables: {
          page: 1,
          pageSize: 10000000,
          filtered: [
            {
              id: 'status',
              value: '1',
              operation: '=='
            },
            {
              id: 'userTypeId',
              value: userType[0]['id'],
              operation: '=='
            }
          ],
          sorted: [
            {
              id: 'createDate',
              desc: true
            }
          ]
        }
      });
    }
  }, [userType]);
  return (
    <TicketModifyDialog
      users={users}
      provinces={provinces}
      channels={channels}
      facilityParentList={[]}
      visible={openTicketModal}
      onCancel={() => setOpenTicketModal(false)}
      ticket={
        {
          patientName: form.getFieldValue('patientName'),
          patientSex: form.getFieldValue('patientSex'),
          address: form.getFieldValue('address'),
          patientPhone: form.getFieldValue('patientPhone'),
          patientBirthDate: form.getFieldValue('patientBirthDate')
        } as Ticket
      }
      onModify={values => {
        handleUpdateTicket(values);
        setOpenTicketModal(false);
      }}
    />
  );
};

export default TicketDialog;
