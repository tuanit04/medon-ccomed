import React, { useState } from 'react';
import { FormInstance } from 'antd';
import AppointmentDialog from 'pages/appointment-manager/appointment/appoimentDialog/AppoimentDialog';
import { AppointmentInput } from 'common/interfaces/input/appointmentInput.interfce';
import { useCreateAppointment, useCreateAppointmentAndForwardConsultant } from 'hooks/appointment';
import { useAppState } from 'helpers';

interface Props {
  form: FormInstance<IPatientDialogForm>;
  openAppointmentModal: boolean;
  setOpenAppointmentModal: React.Dispatch<React.SetStateAction<boolean>>;
  onSubmit: () => void;
}

const TicketDialog = ({ form, openAppointmentModal, setOpenAppointmentModal, onSubmit }: Props) => {
  const [isLoadingCreate, setIsLoadingCreate] = useState(false);
  const [appointmentIdRes, setAppointmentIdRes] = useState('');
  const functionOb = useAppState(state => state.user.functionObject);

  const onSetIdRes = async id => {
    setAppointmentIdRes(id);
  };
  const onSetCreateVisible = (isShow: boolean) => {
    setOpenAppointmentModal(isShow);
  };

  const {
    createAppointment,
    resultSaveAppointment,
    isLoadingSaveAppointment,
    errorSaveAppointment
  } = useCreateAppointment(onSetIdRes, onSetCreateVisible);
  //Xử lý thêm mới Lịch hẹn và chuyển tư vấn
  const {
    createAppointmentAndForwardConsultant,
    resultSaveAppointmentAndForwardConsultant,
    isLoadingSaveAppointmentAndForwardConsultant,
    errorSaveAppointmentAndForwardConsultant
  } = useCreateAppointmentAndForwardConsultant(onSetIdRes, onSetCreateVisible);

  const handleCreateAppointment = async variables => {
    await createAppointment(variables);
  };

  const handleCreateAppointmentAndForwardConsultant = async variables => {
    await createAppointmentAndForwardConsultant(variables);
  };

  return (
    <AppointmentDialog
      // isLoadingChannels={isLoadingChannels}
      defaultValue={{
        patient: {
          sex: form.getFieldValue('patientSex'),
          name: form.getFieldValue('patientName'),
          address: form.getFieldValue('address'),
          phone: form.getFieldValue('patientPhone'),
          birthDate: form.getFieldValue('patientBirthDate')
        },
        patientName: form.getFieldValue('patientName'),
        patientPhone: form.getFieldValue('patientPhone')
      }}
      visible={openAppointmentModal}
      onCreate={async values => {
        const appointmentInput: Partial<AppointmentInput> = {
          ...values
        };
        await handleCreateAppointment({ data: appointmentInput });
        await onSubmit();
      }}
      onCreateAndForward={async values => {
        const appointmentInput: Partial<AppointmentInput> = {
          ...values
        };
        await handleCreateAppointmentAndForwardConsultant({ data: appointmentInput });
        await onSubmit();
      }}
      onCancel={() => setOpenAppointmentModal(false)}
      functionOb={functionOb ?? {}}
      isLoading={isLoadingCreate}
      setIsLoading={setIsLoadingCreate}
    />
  );
};

export default TicketDialog;
