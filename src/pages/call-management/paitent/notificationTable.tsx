import { Table } from 'antd';
import React, { useState } from 'react';

const NotificationTable = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };

  return (
    <Table
      rowSelection={{
        selectedRowKeys,
        onChange: onSelectChange,
        type: 'radio'
      }}
      scroll={{ y: 300 }}
      dataSource={[]}
    >
      <Table.Column<any>
        align="center"
        width={100}
        dataIndex="id"
        title="Mã profile KH"
        render={() => {
          return 'KH211015';
        }}
      />
      <Table.Column<any>
        width={150}
        title="Tên khách hàng"
        align="center"
        render={() => {
          return 'Phạm Thị Thanh Huyền';
        }}
      />
      <Table.Column<any>
        width={50}
        title="Giới tính"
        align="center"
        render={() => {
          return 'Nữ';
        }}
      />
      <Table.Column<any>
        width={50}
        title="Số điện thoại"
        align="center"
        render={() => {
          return '0346628475';
        }}
      />
      <Table.Column<any>
        width={50}
        title="Ngày sinh"
        align="center"
        render={() => {
          return '25/06/1995';
        }}
      />
      <Table.Column<any>
        width={50}
        title="Địa chỉ"
        align="center"
        render={() => {
          return '2123 cầu giấy';
        }}
      />
      <Table.Column<any>
        align="center"
        title="Thành phố"
        width={100}
        render={() => {
          return 'Hà Nội';
        }}
      />
      <Table.Column<any>
        width={120}
        align="center"
        title="Quận huyện"
        render={() => {
          return 'Cầu Giấy';
        }}
      />
    </Table>
  );
};
export default NotificationTable;
