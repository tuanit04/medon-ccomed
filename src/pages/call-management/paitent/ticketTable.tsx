import { Table, Tag, Tooltip } from 'antd';
import { Ticket } from 'common/interfaces/ticket.interface';
import { checkTicketType } from 'pages/ticket-manager/tickets/utils';
import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';
import useRestQuery from 'hooks/useRestQuery';
import { baseURL } from 'config/api';

interface Props {
  responseData?: IResponse<Ticket[]>;
  loading?: boolean;
  getData: (params?: IRequest) => void;
  profiles?: IResponse<IPatientProfile[]>;
  selectedProfiles?: IPatientProfile;
  page: number;
  setPage: React.Dispatch<React.SetStateAction<number>>;
}

const TicketTable = ({ page, setPage, getData, loading, profiles, responseData, selectedProfiles }: Props) => {
  const { data: ticketTypes } = useRestQuery<IResponse>({
    url: baseURL + '/ticket/findAllTicketType'
  });
  const [filteredValue, setFilteredValue] = useState('');

  useEffect(() => {
    if (profiles?.data) {
      setFilteredValue(profiles.data.map(val => val.id).join(','));
    }
  }, [profiles]);

  useEffect(() => {
    if (selectedProfiles) {
      setFilteredValue(selectedProfiles.id);
    }
  }, [selectedProfiles]);

  const onChangePage = (page?: number, pageSize?: number) => {
    setPage(page ?? 1);
    getData({
      pageNo: page,
      pageSize,
      filtered: [
        {
          id: 'patientId',
          value: filteredValue,
          operation: 'in'
        }
      ],
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    });
  };

  return (
    <Table
      dataSource={responseData?.data}
      loading={loading}
      scroll={{ y: 300 }}
      pagination={{
        current: page,
        pageSize: 10,
        total: responseData?.records,
        onChange: onChangePage
      }}
    >
      <Table.Column<Ticket>
        align="center"
        width={50}
        title="STT"
        render={(_, __, index) => {
          return page ? (page - 1) * 10 + index + 1 : index + 1;
        }}
      />
      <Table.Column<Ticket>
        width={150}
        title="Phân loại"
        align="center"
        dataIndex="ticketType"
        render={value => checkTicketType(ticketTypes?.data ?? [], value)}
      />
      <Table.Column<Ticket>
        width={100}
        title="Trạng thái"
        align="center"
        dataIndex="status"
        render={status =>
          status === 1 ? (
            <Tag color="red">Huỷ</Tag>
          ) : status === 2 ? (
            <Tag color="blue">Chờ xử lý</Tag>
          ) : status === 3 ? (
            <Tag color="#f50">Đang xử lý</Tag>
          ) : status === 4 ? (
            <Tag color="green">Đã xử lý</Tag>
          ) : status === 5 ? (
            <Tag color="green">Hoàn thành</Tag>
          ) : (
            ''
          )
        }
      />
      <Table.Column<Ticket>
        width={200}
        title="Nội dung"
        dataIndex="content"
        ellipsis
        render={value => {
          return <Tooltip title={value}>{value}</Tooltip>;
        }}
      />
      <Table.Column<Ticket>
        width={100}
        title="Thời gian tạo"
        align="center"
        dataIndex="createDate"
        render={value => moment(value).format('DD/MM/YYYY HH:mm')}
      />
      <Table.Column<Ticket> width={100} title="Mã khách hàng" align="center" dataIndex="patientId" ellipsis />
      <Table.Column<Ticket> width={100} title="Tên khách hàng" dataIndex="patientName" />
      <Table.Column<Ticket>
        width={100}
        align="center"
        title="Hành động"
        dataIndex="id"
        render={(value) => {
          return <Link to={`/ticket-management/tickets?id=${value}`} target="_blank">Xem chi tiết</Link>;
        }}
      />
    </Table>
  );
};
export default TicketTable;
