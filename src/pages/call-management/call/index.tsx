import {
  CaretRightOutlined,
  DownCircleOutlined,
  FileExcelOutlined,
  PhoneOutlined,
  SearchOutlined,
  UpCircleOutlined
} from '@ant-design/icons';
import {
  Button,
  Col,
  ColProps,
  DatePicker,
  Form,
  Input,
  Popover,
  Row,
  Select,
  Space,
  Table,
  Tag,
  TimePicker,
  Typography
} from 'antd';
import { ColumnsType, TablePaginationConfig } from 'antd/lib/table';
import { getListCallUrl } from 'config/api';
import { CallType } from 'constants/enum';
import { useAppState } from 'helpers';
import useRestQuery from 'hooks/useRestQuery';
import React, { useEffect, useState } from 'react';
import moment from 'moment';
import './index.less';
import { useSearchParams } from 'react-router-dom';
import { csCallout } from 'utils/vcc/actions';

const wrapperCol: ColProps = {
  xs: 24,
  md: 12
};

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};

const toDateTime = (date: moment.Moment) => date.format('DD/MM/YYYY HH:mm:ss');
const toTime = (date: moment.Moment) => date.format('HH:mm:ss');

interface FilterForm {
  searchField?: 'patientName' | 'phone' | 'content';
  searchData?: string;
  name?: string;
  agentId?: string;
  ext?: string;
  line?: string;
  startCallTime?: moment.Moment | null;
  endCallTime?: moment.Moment | null;
  startDurationTime?: moment.Moment | null;
  endDurationTime?: moment.Moment | null;
  startWaitingTime?: moment.Moment | null;
  endWaitingTime?: moment.Moment | null;
  direct?: string;
  status?: number;
}

const CallPage = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const { device, userObject } = useAppState(state => state.user);
  const [form] = Form.useForm<FilterForm>();
  const [pageSize, setPageSize] = useState<number | undefined>(50);
  const [selectedRow, setSelectedRow] = useState<ListCall>();
  const [filtered, setFiltered] = useState<IFiltered[]>([
    {
      id: 'status',
      value: '5',
      operation: '=='
    }
  ]);
  const [showRecordPopup, setShowRecordPopup] = useState<string | null>();

  const initialValue: FilterForm = {
    searchField: 'phone',
    startCallTime: moment().startOf('day'),
    endCallTime: moment().endOf('day'),
    agentId: userObject?.agentId
  };

  const { data: callData, call: getCallData, isLoading } = useRestQuery<IResponse<ListCall[]>, IRequest>({
    url: getListCallUrl,
    method: 'post',
    initialCall: false,
    initialParams: {
      pageNo: 1,
      pageSize,
      filtered: filtered.length > 0 ? filtered : undefined,
      sorted: [
        {
          id: 'callTime',
          desc: true
        }
      ]
    }
  });

  useEffect(() => {
    onSearch(
      searchParams.get('id')
        ? [
            {
              id: 'id',
              operation: '==',
              value: searchParams.get('id') ?? ''
            }
          ]
        : []
    );
  }, [searchParams.get('id')]);

  const onTableChange = (pagination: TablePaginationConfig) => {
    getCallData({
      pageNo: pagination.current,
      pageSize: pagination.pageSize,
      filtered: filtered.length > 0 ? filtered : undefined,
      sorted: [
        {
          id: 'callTime',
          desc: true
        }
      ]
    });
    setPageSize(pagination.pageSize);
  };

  const onSearch = async (newFiltered: IFiltered[] = []) => {
    try {
      let _filtered: IFiltered[] = [
        {
          id: 'status',
          value: '5',
          operation: '=='
        },
        ...newFiltered
      ];

      let isSuccess: number | undefined = undefined;

      if (newFiltered.length === 0) {
        const values = await form.validateFields();

        const {
          searchData,
          searchField,
          endDurationTime,
          endCallTime,
          endWaitingTime,
          startDurationTime,
          startCallTime,
          startWaitingTime,
          status,
          agentId,
          ext,
          ...rest
        } = values;

        if (searchData && searchField) {
          _filtered.push({
            id: searchField,
            value: searchData,
            operation: '~'
          });
        }

        if (agentId) {
          _filtered.push({
            id: 'agentId',
            value: agentId,
            operation: '=='
          });
        }

        if (ext) {
          _filtered.push({
            id: 'ext',
            value: ext,
            operation: '=='
          });
        }

        for (const [key, value] of Object.entries(rest as Record<string, string | undefined>)) {
          if (value) {
            _filtered.push({
              id: key,
              value: value,
              operation: '~'
            });
          }
        }

        if (status != null) {
          if (status === 1) {
            _filtered.push({
              id: 'answerTime',
              operation: 'is_not_null'
            });
            _filtered.push({
              id: 'endTime',
              operation: 'is_not_null'
            });
          }

          if (status === 0) {
            isSuccess = 0;
          }
        }

        if (startCallTime && endCallTime) {
          _filtered.push({
            id: 'callTime',
            value: `${toDateTime(startCallTime.startOf('day'))}, ${toDateTime(endCallTime.endOf('day'))}`,
            operation: 'between'
          });
        }

        if (startDurationTime) {
          const startDurationSeconds = startDurationTime.diff(moment().startOf('day'), 'seconds');
          _filtered.push({
            id: 'durationTime',
            value: `${startDurationSeconds}`,
            operation: '>='
          });
        }

        if (endDurationTime) {
          const endDurationSeconds = endDurationTime.diff(moment().startOf('day'), 'seconds');
          _filtered.push({
            id: 'durationTime',
            value: `${endDurationSeconds}`,
            operation: '<='
          });
        }

        if (startWaitingTime) {
          const startWaitingSeconds = startWaitingTime.diff(moment().startOf('day'), 'seconds');
          _filtered.push({
            id: 'waitingTime',
            value: `${startWaitingSeconds}`,
            operation: '>='
          });
        }

        if (endWaitingTime) {
          const endWaitingSeconds = endWaitingTime.diff(moment().startOf('day'), 'seconds');
          _filtered.push({
            id: 'waitingTime',
            value: `${endWaitingSeconds}`,
            operation: '<='
          });
        }

        setSearchParams({});
      }

      getCallData({
        pageNo: 1,
        pageSize,
        isSuccess,
        filtered: _filtered.length > 0 ? _filtered : undefined,
        sorted: [
          {
            id: 'callTime',
            desc: true
          }
        ]
      });
      setFiltered(_filtered);
      setShowRecordPopup(null);
    } catch (err) {}
  };

  const columns: ColumnsType<ListCall> = [
    {
      title: 'STT',
      width: 50,
      render: (value, record, index) =>
        callData?.page && pageSize ? (callData.page - 1) * pageSize + index + 1 : index + 1
    },
    {
      title: 'Cuộc gọi',
      width: 50,
      dataIndex: 'direct',
      align: 'center',
      render: val => (
        <Space>
          {val === CallType.INCOMING && <DownCircleOutlined className="incoming-call" />}
          {val === CallType.OUTGOING && <UpCircleOutlined className="outgoing-call" />}
        </Space>
      )
    },
    {
      title: 'Phân loại',
      align: 'center',
      width: 120,
      dataIndex: 'callTypeName'
    },
    {
      title: 'Nội dung cuộc gọi',
      width: 300,
      dataIndex: 'content'
    },
    {
      title: 'Trạng thái',
      width: 100,
      dataIndex: 'status',
      className: 'status-cell',
      render: (value, record) =>
        record.answerTime == null || record.endTime == null ? (
          <div className={'status-cell-failed'}>Không thành công</div>
        ) : (
          <div className={'status-cell-success'}>Thành công</div>
        )
    },
    {
      title: 'Khách hàng',
      width: 300,
      dataIndex: 'patientId',
      render: (value, record) => (
        <Space direction="vertical" size={1}>
          <Typography.Text strong>{record.patientName ?? 'Chưa xác định khách hàng'}</Typography.Text>
          <Typography.Text style={{ width: 280 }} ellipsis>
            Mã KH: {record.patientId}
          </Typography.Text>
          <Typography.Text>SĐT: {record.phone}</Typography.Text>
        </Space>
      )
    },
    {
      title: 'Đầu số gọi vào/ra',
      width: 100,
      dataIndex: 'line'
    },
    {
      title: 'Thời lượng',
      width: 100,
      dataIndex: 'durationTime',
      render: value => value && moment.utc(value * 1000).format('HH:mm:ss')
    },
    {
      title: 'Tên CB',
      width: 150,
      dataIndex: 'name'
    },
    {
      title: 'Ext',
      width: 100,
      dataIndex: 'ext'
    },
    {
      title: 'AgentID',
      width: 130,
      dataIndex: 'agentId',
      ellipsis: true
    },
    {
      title: 'Thời gian KH chờ',
      width: 100,
      dataIndex: 'waitingTime',
      render: value => value && moment.utc(value * 1000).format('HH:mm:ss')
    },
    {
      title: 'Thời gian bắt đầu',
      width: 100,
      dataIndex: 'callTime',
      render: value => value && moment(new Date(value)).format('DD/MM/YYYY HH:mm:ss')
    },
    {
      title: 'Thời gian kết thúc',
      width: 100,
      dataIndex: 'endTime',
      render: value => value && moment(new Date(value)).format('DD/MM/YYYY HH:mm:ss')
    },
    {
      title: 'Ghi âm',
      width: 50,
      dataIndex: 'phone',
      fixed: 'right',
      render: (value, record) => {
        const isSelected = Boolean(showRecordPopup) && showRecordPopup === record.id;
        return record.answerTime == null || record.endTime == null ? null : (
          <Popover
            trigger="click"
            placement="topLeft"
            destroyTooltipOnHide
            visible={isSelected}
            content={
              <audio controls autoPlay>
                <source src={record.path} type="audio/mpeg" />
                Your browser does not support the audio element.
              </audio>
            }
          >
            <Button
              type="primary"
              icon={isSelected ? <span className="btn-stop"></span> : <CaretRightOutlined />}
              onClick={e => {
                e.stopPropagation();
                setShowRecordPopup(isSelected ? null : record.id);
              }}
              className={isSelected ? '' : 'btn-call'}
              danger={isSelected}
              disabled={!record.path}
            ></Button>
          </Popover>
        );
      }
    }
  ];

  return (
    <div className="main-content call-management">
      <Form
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        name="call-management"
        form={form}
        onFinish={() => onSearch()}
        initialValues={initialValue}
      >
        <Row gutter={[8, 8]}>
          <Col {...wrapperCol}>
            <Form.Item
              label="Tìm kiếm theo"
              labelCol={{
                xs: 24,
                md: 4
              }}
              name="searchData"
            >
              <Input
                placeholder="Nhập mã cuộc gọi, SĐT, thông tin khách hàng..."
                autoFocus
                onKeyDown={e => {
                  if (e.key === 'Enter') {
                    onSearch();
                  }
                }}
                addonBefore={
                  <Form.Item name="searchField" noStyle>
                    <Select>
                      <Select.Option value="patientName">Khách hàng</Select.Option>
                      <Select.Option value="content">Nội dung</Select.Option>
                      <Select.Option value="phone">Số điện thoại</Select.Option>
                    </Select>
                  </Form.Item>
                }
              />
            </Form.Item>
            <Row gutter={[8, 8]}>
              <Col {...wrapperCol}>
                <Form.Item
                  label="Tên cán bộ"
                  labelCol={{
                    xs: 24,
                    md: 8
                  }}
                  name="name"
                >
                  <Input
                    placeholder="Tên cán bộ"
                    onKeyDown={e => {
                      if (e.key === 'Enter') {
                        onSearch();
                      }
                    }}
                    allowClear
                  />
                </Form.Item>
              </Col>
              <Col {...wrapperCol}>
                <Form.Item
                  label="Đầu số gọi"
                  labelCol={{
                    xs: 24,
                    md: 8
                  }}
                  name="line"
                >
                  <Input
                    placeholder="Đầu số gọi vào/ra"
                    onKeyDown={e => {
                      if (e.key === 'Enter') {
                        onSearch();
                      }
                    }}
                    allowClear
                  />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col {...wrapperCol}>
            <Row gutter={[8, 8]}>
              <Col {...wrapperCol}>
                <Form.Item
                  label="Agent Id"
                  labelCol={{
                    xs: 24,
                    md: 8
                  }}
                  name="agentId"
                >
                  <Input
                    placeholder="Agent Id"
                    onKeyDown={e => {
                      if (e.key === 'Enter') {
                        onSearch();
                      }
                    }}
                    allowClear
                  />
                </Form.Item>
                <Form.Item
                  label="Cuộc gọi"
                  labelCol={{
                    xs: 24,
                    md: 8
                  }}
                  name="direct"
                >
                  <Select allowClear placeholder="Chọn loại cuộc gọi">
                    <Select.Option value="IN">Cuộc gọi đến</Select.Option>
                    <Select.Option value="OUT">Cuộc gọi đi</Select.Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col {...wrapperCol}>
                <Form.Item
                  label="Ext"
                  labelCol={{
                    xs: 24,
                    md: 8
                  }}
                  name="ext"
                >
                  <Input
                    placeholder="Ext"
                    onKeyDown={e => {
                      if (e.key === 'Enter') {
                        onSearch();
                      }
                    }}
                    allowClear
                  />
                </Form.Item>
                <Form.Item
                  label="Trạng thái"
                  labelCol={{
                    xs: 24,
                    md: 8
                  }}
                  name="status"
                >
                  <Select allowClear placeholder="Chọn trạng thái">
                    <Select.Option value={1}>Thành công</Select.Option>
                    <Select.Option value={0}>Không thành công</Select.Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col {...wrapperCol}>
            <Row className="filter-group">
              <label>Thời gian gọi</label>
              <Col {...wrapperCol}>
                <Form.Item
                  label="Từ"
                  name="startCallTime"
                  rules={[
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (value || (!value && !getFieldValue('endCallTime'))) {
                          return Promise.resolve();
                        }
                        return Promise.reject(new Error('Bắt buộc'));
                      }
                    })
                  ]}
                >
                  <DatePicker
                    format="DD/MM/YYYY"
                    // disabledDate={date => (formValues.endCallTime ? date.utc() > formValues.endCallTime.utc() : false)}
                    placeholder="Chọn ngày"
                  />
                </Form.Item>
              </Col>
              <Col {...wrapperCol}>
                <Form.Item
                  label="Đến"
                  name="endCallTime"
                  rules={[
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (value || (!value && !getFieldValue('startCallTime'))) {
                          return Promise.resolve();
                        }
                        return Promise.reject(new Error('Bắt buộc'));
                      }
                    })
                  ]}
                >
                  <DatePicker
                    format="DD/MM/YYYY"
                    // disabledDate={date =>
                    //   formValues.startCallTime ? date.utc() < formValues.startCallTime.utc() : false
                    // }
                    placeholder="Chọn ngày"
                  />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col {...wrapperCol}>
            <Row className="filter-group">
              <label>Thời lượng đàm thoại</label>
              <Col {...wrapperCol}>
                <Form.Item label="Từ" name="startDurationTime">
                  <TimePicker placeholder="Chọn thời gian" />
                </Form.Item>
              </Col>
              <Col {...wrapperCol}>
                <Form.Item label="Đến" name="endDurationTime">
                  <TimePicker placeholder="Chọn thời gian" />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col {...wrapperCol}>
            <Row className="filter-group">
              <label>Thời gian KH chờ</label>
              <Col {...wrapperCol}>
                <Form.Item label="Từ" name="startWaitingTime">
                  <TimePicker placeholder="Chọn thời gian" />
                </Form.Item>
              </Col>
              <Col {...wrapperCol}>
                <Form.Item label="Đến" name="endWaitingTime">
                  <TimePicker placeholder="Chọn thời gian" />
                </Form.Item>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
      <Table
        title={() => (
          <Space className="table-title">
            <Tag color="#87d068" style={{ color: 'black' }}>
              Tổng số cuộc gọi: {callData?.records ?? 0}
            </Tag>
            <Space className="buttons">
              <Button icon={<SearchOutlined />} type="primary" onClick={() => onSearch()}>
                Tìm kiếm
              </Button>
              <Button
                icon={<PhoneOutlined />}
                onClick={() => {
                  if (selectedRow?.phone?.trim()) {
                    csCallout(selectedRow.phone);
                  }
                }}
                className="btn-call"
                disabled={!selectedRow?.phone?.trim()}
              >
                Gọi
              </Button>
              <Button icon={<FileExcelOutlined />}>Xuất excel</Button>
            </Space>
          </Space>
        )}
        loading={isLoading}
        dataSource={callData?.data ?? []}
        columns={columns}
        scroll={{ x: 500, y: 'calc(100vh - 370px)' }}
        onChange={onTableChange}
        rowKey={record => record.id ?? ''}
        rowSelection={{
          selectedRowKeys: selectedRow?.id ? [selectedRow.id] : [],
          type: 'radio',
          onChange: (_, selectedRows) => {
            setSelectedRow(selectedRows?.[0] && selectedRows[0].id !== selectedRow?.id ? selectedRows?.[0] : undefined);
          }
        }}
        onRow={data => ({
          onClick: () => {
            setSelectedRow(data.id !== selectedRow?.id ? data : undefined);
          }
        })}
        pagination={{
          current: callData?.page,
          pageSize,
          total: callData?.records
        }}
      />
    </div>
  );
};

export default CallPage;
