import { Col, Row } from 'antd';
import React from 'react';
import './index.less';
import CallUserTable from './CallUserTable';
import CallOngoingTable from './CallOngoingTable';
import StatisticalTable from './StatisticalTable';

const UserPage = () => {
  return (
    <Row className="user-management" gutter={[8, 8]} wrap={false}>
      <Col xs={24} md={12}>
        <CallUserTable />
      </Col>
      <Col xs={24} md={12} style={{ display: 'flex' }}>
        <Row className="user-management-right" style={{ width: '100%' }} gutter={[8, 8]}>
          <Col xs={24}>
            <StatisticalTable />
          </Col>
          <Col className="dialing" flex={1} xs={24}>
            <CallOngoingTable />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default UserPage;
