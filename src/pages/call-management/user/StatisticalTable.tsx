import { ReloadOutlined } from '@ant-design/icons';
import { Button, Checkbox, Input, Space, Table, Typography } from 'antd';
import { ColumnsType } from 'antd/es/table/interface';
import { getCallStatisticalByLineUrl } from 'config/api';
import useRestQuery from 'hooks/useRestQuery';
import React, { useEffect, useState } from 'react';
import './index.less';

const StatisticalTable = () => {
  const [statisticData, setStatisticData] = useState<StatisticCallByLine[]>();
  const [statisticInput, setStatisticInput] = useState('');
  const [pageSize] = useState<number | undefined>(20);
  const [filtered, setFiltered] = useState<IFiltered[]>();

  const {
    data: callStatisticalData,
    call: getCallStatisticalData,
    isLoading: loadingCallStatisticalData,
    isFirstCall: firstCallStatisticalData
  } = useRestQuery<IResponse<StatisticCallByLine[]>, IRequest>({
    url: getCallStatisticalByLineUrl,
    method: 'post',
    initialParams: {
      pageNo: 1,
      pageSize
    }
  });

  const statisticColumns: ColumnsType<any> = [
    {
      title: 'Thống kê các chỉ số',
      width: 150,
      dataIndex: 'index',
      fixed: 'left',
      filterDropdown: ({ filters, selectedKeys, setSelectedKeys, confirm }) => {
        return (
          <Space className="statistic-filter" direction="vertical">
            <Input.Search
              className="statistic-input"
              placeholder="Nhập SĐT"
              enterButton
              loading={loadingCallStatisticalData}
              onChange={e => setStatisticInput(e.target.value)}
              onSearch={onLineSearch}
            />
            <Checkbox.Group
              options={filters?.map(val => ({ ...val, label: val.text }))}
              value={selectedKeys}
              onChange={(v: any) => setSelectedKeys(v)}
              className="checkbox-group"
            />
            <Space style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Button
                type="primary"
                size="small"
                onClick={() => {
                  setStatisticData(callStatisticalData?.data?.filter(val => selectedKeys.includes(val.line ?? '')));
                  confirm({ closeDropdown: true });
                }}
              >
                OK
              </Button>
            </Space>
          </Space>
        );
      },
      filteredValue: statisticData?.map(val => val.line ?? ''),
      filters:
        callStatisticalData?.data?.map(val => ({
          text: val.line,
          value: val.line ?? ''
        })) ?? []
    },
    ...(statisticData?.map(val => ({
      title: val.line,
      width: 120,
      dataIndex: val.line
    })) ?? [])
  ];

  const statisticDataObj = statisticData?.reduce(
    (acc, val) => {
      if (!val.line) {
        return acc;
      }

      const newAcc = { ...acc };
      Object.keys(newAcc).forEach(key => {
        newAcc[key][val.line] = val[key];
      });

      return newAcc;
    },
    {
      cntWaiting: {
        index: 'Cuộc gọi đang chờ'
      },
      cntApproved: {
        index: 'Cuộc gọi đang tiếp nhận'
      },
      cntReady: {
        index: 'Sẵn sàng tiếp nhận cuộc gọi'
      },
      cntInDay: {
        index: 'Cuộc gọi trong ngày'
      },
      cntMiss: {
        index: 'Cuộc gọi nhỡ'
      },
      ratioCall: {
        index: 'Tỷ lệ nhận cuộc gọi (%)'
      },
      avgDamthoai: {
        index: 'Thời gian trung bình đàm thoại'
      },
      avgChocuocgoi: {
        index: 'Thời gian chờ trung bình cuộc gọi'
      }
    }
  );

  const onLineSearch = () => {
    const filtered = statisticInput
      ? [
          {
            id: 'line',
            value: statisticInput,
            operation: '~'
          }
        ]
      : undefined;
    getCallStatisticalData({
      pageNo: 1,
      pageSize,
      filtered
    });
    setFiltered(filtered);
  };

  const onReload = () => {
    getCallStatisticalData({
      pageNo: 1,
      pageSize,
      filtered
    });
  };

  useEffect(() => {
    if (firstCallStatisticalData) {
      setStatisticData(callStatisticalData?.data);
    }
  }, [callStatisticalData]);

  return (
    <>
      <Space className="justify-between">
        <Typography.Title level={5}>Thống kê</Typography.Title>
        <Button type="primary" size="small" onClick={onReload}>
          <ReloadOutlined />
        </Button>
      </Space>
      <Table
        columns={statisticColumns}
        scroll={{ x: 300, y: 'calc(50vh - 80px)' }}
        dataSource={Object.values(statisticDataObj ?? {})}
        loading={loadingCallStatisticalData}
        pagination={false}
      />
    </>
  );
};

export default StatisticalTable;
