import { Button, Space, Table, TablePaginationConfig, Typography } from 'antd';
import { ColumnsType } from 'antd/es/table/interface';
import { getListCallUserUrl } from 'config/api';
import useRestQuery from 'hooks/useRestQuery';
import { VCCUnreadyReasonDetail } from 'interface/user/vcc';
import React, { useEffect, useState } from 'react';
import moment from 'moment';
import './index.less';
import { ColumnFilterItem, FilterValue } from 'antd/lib/table/interface';
import { ReloadOutlined } from '@ant-design/icons';

const CallUserTable = () => {
  const [extFilters, setExtFilters] = useState<ColumnFilterItem[]>([]);
  const [agentIdFilters, setAgentIdFilters] = useState<ColumnFilterItem[]>([]);
  const [pageSize, setPageSize] = useState<number | undefined>(50);
  const [filtered, setFiltered] = useState<IFiltered[]>();

  const { data: callUserData, call: getCallUserData, isLoading: loadingCallUserData } = useRestQuery<
    IResponse<ListCallUser[]>,
    IRequest
  >({
    url: getListCallUserUrl,
    method: 'post',
    initialParams: {
      pageNo: 1,
      pageSize: pageSize
    }
  });

  const columns: ColumnsType<ListCallUser> = [
    {
      title: 'STT',
      width: 50,
      render: (value, record, index) => index + 1
    },
    {
      title: 'Ext',
      width: 70,
      dataIndex: 'ext',
      filters: extFilters,
      onFilter: (value, record) => value === record.ext
    },
    // {
    //   title: 'AgentID',
    //   width: 100,
    //   dataIndex: 'agentId',
    //   ellipsis: true,
    //   filters: agentIdFilters,
    //   onFilter: (value, record) => value === record.agentId
    // },
    {
      title: 'Tên CB',
      width: 150,
      dataIndex: 'name',
      filters: agentIdFilters,
      onFilter: (value, record) => value === record.name
    },
    {
      title: 'Trạng thái thoại',
      width: 135,
      dataIndex: 'type',
      align: 'center',
      render: val => (
        <Button className={val === 'READY' ? 'btn-green' : 'btn-orange'} size="small" type="primary">
          {val === 'READY' ? 'Sẵn sàng' : 'Không sẵn sàng'}
        </Button>
      ),
      filters: [
        { text: 'Sẵn sàng', value: 'READY' },
        { text: 'Không sẵn sàng', value: 'UNREADY' }
      ],
      onFilter: (value, record) => record.type === value
    },
    {
      title: 'Trạng thái nghỉ',
      width: 100,
      dataIndex: 'reasonUnready',
      render: value => value && VCCUnreadyReasonDetail[value],
      filters: Object.entries(VCCUnreadyReasonDetail).map(([key, value]) => ({
        text: value,
        value: key
      })),
      onFilter: (value, record) => record.reasonUnready === value
    },
    {
      title: 'Thời gian hoạt động gần nhất',
      width: 150,
      dataIndex: 'exeTime',
      render: value => value && moment(new Date(value)).format('DD/MM/YYYY HH:mm:ss')
    },
    {
      title: 'Tổng số cuộc gọi vào',
      width: 80,
      dataIndex: 'cntIn'
    },
    {
      title: 'Tổng số cuộc gọi ra',
      width: 80,
      dataIndex: 'cntOut'
    },
    {
      title: 'Tổng số cuộc gọi nhỡ',
      width: 80,
      dataIndex: 'cntMiss'
    }
  ];

  const onTableChange = (pagination: TablePaginationConfig, filters: Record<string, FilterValue | null>) => {
    const filtered: IFiltered[] = [];

    if (filters) {
      if (filters.reasonUnready) {
        filtered.push({
          id: 'reasonUnready',
          value: filters.reasonUnready.join(','),
          operation: 'in'
        });
      }
      if (filters.type) {
        filtered.push({
          id: 'type',
          value: filters.type.join(','),
          operation: 'in'
        });
      }
    }

    getCallUserData({
      pageNo: pagination.current,
      pageSize: pagination.pageSize,
      filtered: filtered.length > 0 ? filtered : undefined
    });
    setFiltered(filtered.length > 0 ? filtered : undefined);
    setPageSize(pagination.pageSize);
  };

  const onReload = () => {
    getCallUserData({
      pageNo: 1,
      pageSize,
      filtered
    });
  };

  useEffect(() => {
    if (callUserData?.data) {
      setExtFilters(
        callUserData.data.reduce<ColumnFilterItem[]>((acc, val) => {
          if (!val.ext || acc.find(item => item.value === val.ext)) {
            return acc;
          }
          return [...acc, { value: val.ext ?? '', text: val.ext }];
        }, [])
      );
      setAgentIdFilters(
        callUserData.data.reduce<ColumnFilterItem[]>((acc, val) => {
          if (!val.name || acc.find(item => item.value === val.name)) {
            return acc;
          }
          return [...acc, { value: val.name ?? '', text: val.name }];
        }, [])
      );
    }
  }, [callUserData]);

  return (
    <>
      <Space className="justify-between">
        <Typography.Title level={5}>Danh sách trạng thái tổng đài viên</Typography.Title>
        <Button type="primary" size="small" onClick={onReload}>
          <ReloadOutlined />
        </Button>
      </Space>
      <Table
        className="status-table"
        columns={columns}
        scroll={{ x: 300, y: 'auto' }}
        dataSource={callUserData?.data}
        onChange={onTableChange}
        loading={loadingCallUserData}
        pagination={{
          current: callUserData?.page,
          pageSize: pageSize,
          total: callUserData?.records
        }}
      />
    </>
  );
};

export default CallUserTable;
