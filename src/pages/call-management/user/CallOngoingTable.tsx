import { DownCircleOutlined, ReloadOutlined, UpCircleOutlined } from '@ant-design/icons';
import { Button, Space, Table, TablePaginationConfig, Typography } from 'antd';
import { ColumnsType } from 'antd/es/table/interface';
import { getListCallOngoingUrl } from 'config/api';
import { CallType } from 'constants/enum';
import useRestQuery from 'hooks/useRestQuery';
import React, { useEffect, useState } from 'react';
import './index.less';
import { ColumnFilterItem, FilterValue } from 'antd/lib/table/interface';

const { Title } = Typography;

const CallOngoingTable = () => {
  const [extFilters, setExtFilters] = useState<ColumnFilterItem[]>([]);
  const [agentIdFilters, setAgentIdFilters] = useState<ColumnFilterItem[]>([]);
  const [lineFilters, setLineFilters] = useState<ColumnFilterItem[]>([]);
  const [pageSize, setPageSize] = useState<number | undefined>(20);
  const [filtered, setFiltered] = useState<IFiltered[]>();

  const { data: callOngoingData, call: getCallOngoingData, isLoading: loadingCallOngoingData } = useRestQuery<
    IResponse<ListCallUserOngoing[]>,
    IRequest
  >({
    url: getListCallOngoingUrl,
    method: 'post',
    initialParams: {
      pageNo: 1,
      pageSize
    }
  });

  const columns: ColumnsType<ListCallUserOngoing> = [
    {
      title: 'STT',
      width: 50,
      render: (val, record, i) => i + 1
    },
    {
      title: 'Ext',
      width: 70,
      dataIndex: 'ext',
      filters: extFilters,
      onFilter: (value, record) => value === record.ext
    },
    // {
    //   title: 'AgentID',
    //   width: 80,
    //   dataIndex: 'agentId',
    //   filters: agentIdOngoingFilters,
    //   onFilter: (value, record) => value === record.agentId
    // },
    {
      title: 'Tên CB',
      width: 150,
      dataIndex: 'uname',
      filters: agentIdFilters,
      onFilter: (value, record) => value === record.uname
    },
    {
      title: 'Cuộc gọi',
      width: 60,
      dataIndex: 'direct',
      className: 'call-type-cell',
      align: 'center',
      render: val => (
        <Space>
          {val === CallType.INCOMING && <DownCircleOutlined className="incoming-call" />}
          {val === CallType.OUTGOING && <UpCircleOutlined className="outgoing-call" />}
        </Space>
      ),
      filters: [
        {
          text: 'Cuộc gọi đi',
          value: CallType.OUTGOING
        },
        {
          text: 'Cuộc gọi đến',
          value: CallType.INCOMING
        }
      ]
    },
    {
      title: 'Đầu số gọi vào/ra',
      width: 120,
      dataIndex: 'line',
      filters: lineFilters,
      align: 'right',
      onFilter: (value, record) => value === record.line
    },
    {
      title: 'Khách hàng liên hệ gần nhất',
      width: 300,
      dataIndex: 'patientId',
      render: (value, record) => (
        <Space direction="vertical" size={1}>
          <Typography.Text strong>{record.patientName ?? 'Chưa xác định khách hàng'}</Typography.Text>
          <Typography.Text style={{ width: 280 }} ellipsis>
            Mã KH: {record.userId}
          </Typography.Text>
          <Typography.Text>SĐT: {record.phone}</Typography.Text>
        </Space>
      ),
      filters: []
    },
    {
      title: 'Trạng thái',
      width: 100,
      dataIndex: 'status',
      align: 'center',
      className: 'status-cell',
      render: value => (
        <div
          className={
            'status-cell-' + (value === 2 ? 'waiting' : value === 3 || value === 4 || value === 6 ? 'calling' : '')
          }
        >
          {value === 2 ? 'Đang chờ' : value === 3 || value === 4 ? 'Đang gọi' : ''}
        </div>
      ),
      filters: [
        {
          text: 'Đang gọi',
          value: '3,4,6'
        },
        {
          text: 'Đang chờ',
          value: '2'
        }
      ]
    }
    // {
    //   title: 'Hành động',
    //   width: 170,
    //   fixed: 'right',
    //   render: () => (
    //     <Space>
    //       <Tooltip title={hold ? 'Tiếp tục' : 'Giữ máy'}>
    //         <Button
    //           className={hold ? 'btn-play' : 'btn-hold'}
    //           shape="circle"
    //           icon={hold ? <CaretRightOutlined /> : <PauseOutlined />}
    //           size="small"
    //           onClick={() => setHold(!hold)}
    //           disabled
    //         />
    //       </Tooltip>
    //       <Tooltip title="Chuyển cuộc gọi">
    //         <Button shape="circle" icon={<ArrowRightOutlined />} size="small" onClick={() => {}} disabled />
    //       </Tooltip>
    //       <Tooltip title="Nghe xen">
    //         <Button shape="circle" icon={<CustomerServiceOutlined />} size="small" onClick={() => {}} disabled />
    //       </Tooltip>
    //       <Tooltip title="Cướp cuộc gọi">
    //         <Button shape="circle" icon={<PhoneOutlined />} size="small" onClick={() => {}} disabled />
    //       </Tooltip>
    //       <Tooltip title="Gọi hội nghị">
    //         <Button shape="circle" icon={<TeamOutlined />} size="small" onClick={() => {}} disabled />
    //       </Tooltip>
    //     </Space>
    //   )
    // }
  ];

  const onTableChange = (pagination: TablePaginationConfig, filters: Record<string, FilterValue | null>) => {
    const filtered: IFiltered[] = [];

    if (filters) {
      if (filters.direct) {
        filtered.push({
          id: 'direct',
          value: filters.direct.join(','),
          operation: 'in'
        });
      }
      if (filters.status) {
        filtered.push({
          id: 'status',
          value: filters.status.join(','),
          operation: 'in'
        });
      }
    }

    getCallOngoingData({
      pageNo: pagination.current,
      pageSize: pagination.pageSize,
      filtered: filtered.length > 0 ? filtered : undefined
    });
    setFiltered(filtered.length > 0 ? filtered : undefined);
    setPageSize(pagination.pageSize);
  };

  const onReload = () => {
    getCallOngoingData({
      pageNo: 1,
      pageSize,
      filtered
    });
  };

  useEffect(() => {
    if (callOngoingData?.data) {
      setExtFilters(
        callOngoingData.data.reduce<ColumnFilterItem[]>((acc, val) => {
          if (!val.ext || acc.find(item => item.value === val.ext)) {
            return acc;
          }
          return [...acc, { value: val.ext ?? '', text: val.ext }];
        }, [])
      );
      setAgentIdFilters(
        callOngoingData.data.reduce<ColumnFilterItem[]>((acc, val) => {
          if (!val.uname || acc.find(item => item.value === val.uname)) {
            return acc;
          }
          return [...acc, { value: val.uname ?? '', text: val.uname }];
        }, [])
      );
      setLineFilters(
        callOngoingData?.data?.reduce<ColumnFilterItem[]>((acc, val) => {
          if (!val.line || acc.find(item => item.value === val.line)) {
            return acc;
          }
          return [...acc, { value: val.line ?? '', text: val.line }];
        }, [])
      );
    }
  }, [callOngoingData]);

  return (
    <>
      <Space className="justify-between">
        <Title level={5}>Danh sách user tổng đài đang nghe gọi</Title>
        <Button type="primary" size="small" onClick={onReload}>
          <ReloadOutlined />
        </Button>
      </Space>
      <Table
        columns={columns}
        scroll={{ x: 300, y: 'calc(50vh - 80px)' }}
        dataSource={callOngoingData?.data}
        onChange={onTableChange}
        loading={loadingCallOngoingData}
        pagination={{
          current: callOngoingData?.page,
          pageSize: pageSize,
          total: callOngoingData?.records
        }}
      />
    </>
  );
};

export default CallOngoingTable;
