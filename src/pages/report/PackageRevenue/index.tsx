import React from 'react';
import { Button, Col, Row, Typography, Table } from 'antd';
import { FileExcelOutlined } from '@ant-design/icons';
import exportToExcel from 'utils/exportExcel';
import './index.less';

interface Props {}

const PackageRevenue = (props: Props) => {
  const dataSource = [
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    }
  ];

  const columns = [
    {
      title: 'Ngày',
      dataIndex: 'date'
    },
    {
      title: 'Đơn vị',
      width: 100
    },
    {
      title: 'Số lượng khách hàng',
      children: [
        {
          title: 'Slg Khách hàng thực hiện',
          width: 100
        },
        {
          title: 'Slg Khách hàng kênh ĐVTV',
          width: 100
        },
        {
          title: 'Slg Khách hàng kênh Tổng đài',
          width: 100
        },
        {
          title: 'Slg Khách hàng kênh Web/ App',
          width: 100
        }
      ]
    },
    {
      title: 'Số lượng gói khám',
      children: [
        {
          title: 'Slg gói khám',
          width: 100
        },
        {
          title: 'Slg gói khám kênh ĐVTV',
          width: 100
        },
        {
          title: 'Slg gói khám kênh Tổng đài',
          width: 100
        },
        {
          title: 'Slg gói khám kênh Web/ App',
          width: 100
        }
      ]
    },
    {
      title: 'Doanh thu ĐVTV',
      children: [
        {
          title: 'Thực thu',
          width: 100
        },
        {
          title: 'Doanh thu gói khám sau giảm',
          width: 100
        },
        {
          title: 'Doanh thu gói khám trước giảm',
          width: 100
        },
        {
          title: 'Doanh thu chỉ định lẻ',
          width: 100
        },
        {
          title: 'Phí đi lại',
          width: 100
        },
        {
          title: 'Giảm giá',
          width: 100
        },
        {
          title: 'Doanh thu tính phí kênh ĐVTV',
          width: 100
        },
        {
          title: 'Doanh thu tính phí kênh Tổng đài',
          width: 100
        },
        {
          title: 'Doanh thu tính phí kênh Web/ App',
          width: 100
        }
      ]
    },
    {
      title: 'Phí môi giới MED-ON',
      children: [
        {
          title: 'Tổng doanh thu',
          width: 100
        },
        {
          title: 'Phí môi giới kênh ĐVTV',
          width: 100
        },
        {
          title: 'Phí môi giới kênh Tổng đài',
          width: 100
        },
        {
          title: 'Phí môi giới kênh Web/ App',
          width: 100
        }
      ]
    }
  ];

  return (
    <Row className="report-package-revenue">
      <Col xs={24} className="title">
        <Typography.Title level={4}>Tổng hợp doanh thu gói khám</Typography.Title>
        <Button type="primary" onClick={() => exportToExcel([], 'booking-revenue')}>
          <FileExcelOutlined />
          Xuất tất cả
        </Button>
      </Col>
      <Col xs={24} className="table-container">
        <div>
          <Table dataSource={dataSource} columns={columns} scroll={{ x: 'auto' }} />
        </div>
      </Col>
    </Row>
  );
};

export default PackageRevenue;
