import React from 'react';
import { Button, Col, Row, Typography, Table, Space, Tooltip } from 'antd';
import { FileExcelOutlined, InfoCircleOutlined } from '@ant-design/icons';
import exportToExcel from 'utils/exportExcel';
import './index.less';
import { ColumnType } from 'antd/lib/table';

interface Props {}

const PackageRevenueDetail = (props: Props) => {
  const dataSource = [
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    }
  ];

  const columns: ColumnType<any>[] = [
    {
      title: 'Ngày sử dụng',
      dataIndex: 'date',
      width: 100
    },
    {
      title: 'SID',
      width: 100
    },
    {
      title: 'Mã KCB',
      width: 100
    },
    {
      title: 'Mã lịch hẹn',
      width: 100
    },
    {
      title: 'Tên KH',
      width: 100
    },
    {
      title: 'IDHD (gói)',
      width: 100
    },
    {
      title: 'Mã gói',
      width: 100
    },
    {
      title: 'Tên gói',
      width: 100
    },
    {
      title: 'Mã dịch vụ',
      width: 100
    },
    {
      title: () => (
        <Space direction="vertical" size={4}>
          <span>{'Đơn vị thực hiện'}</span>
          <Tooltip title="Theo pháp nhân">
            <InfoCircleOutlined />
          </Tooltip>
        </Space>
      ),
      width: 100
    },
    {
      title: 'Văn phòng thực hiện',
      width: 100
    },
    {
      title: 'Nguồn bán',
      width: 100
    },
    {
      title: () => (
        <Space direction="vertical" size={4}>
          <span>{'Tỷ lệ % môi giới'}</span>
          <Tooltip title="Có thể thay đổi">
            <InfoCircleOutlined />
          </Tooltip>
        </Space>
      ),
      width: 100
    },
    {
      title: 'Mã nhân viên bán',
      width: 100
    },
    {
      title: 'Giá gói khám (trước giảm)',
      width: 100
    },
    {
      title: 'Giá gói khám (trước giảm)',
      width: 100
    },
    {
      title: 'Doanh thu dịch vụ lẻ',
      width: 100
    },
    {
      title: 'Phí đi lại',
      width: 100
    },
    {
      title: 'Giảm giá',
      width: 100
    },
    {
      title: 'Thực thu',
      width: 100
    },
    {
      title: () => (
        <Space direction="vertical" size={4}>
          <span>{'Doanh thu tính phí môi giới'}</span>
          <Tooltip title="=15+17+18, có thể thay đổi">
            <InfoCircleOutlined />
          </Tooltip>
        </Space>
      ),
      width: 100
    },
    {
      title: () => (
        <Space direction="vertical" size={4}>
          <span>{'Phí môi giới'}</span>
          <Tooltip title="=13*21">
            <InfoCircleOutlined />
          </Tooltip>
        </Space>
      ),
      width: 100
    }
  ];

  return (
    <Row className="report-package-revenue-detail">
      <Col xs={24} className="title">
        <Typography.Title level={4}>Chi tiết doanh thu gói khám</Typography.Title>
        <Button type="primary" onClick={() => exportToExcel([], 'package-revenue')}>
          <FileExcelOutlined />
          Xuất tất cả
        </Button>
      </Col>
      <Col xs={24} className="table-container">
        <div>
          <Table dataSource={dataSource} columns={columns} scroll={{ x: 'auto' }} />
        </div>
      </Col>
    </Row>
  );
};

export default PackageRevenueDetail;
