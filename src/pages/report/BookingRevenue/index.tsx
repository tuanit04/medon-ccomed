import React from 'react';
import { Button, Col, Row, Typography, Table } from 'antd';
import { FileExcelOutlined } from '@ant-design/icons';
import exportToExcel from 'utils/exportExcel';
import './index.less';

interface Props {}

const BookingRevenue = (props: Props) => {
  const dataSource = [
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    }
  ];

  const columns = [
    {
      title: 'Ngày',
      dataIndex: 'date'
    },
    {
      title: 'Đơn vị',
      width: 100
    },
    {
      title: 'Số lượng',
      children: [
        {
          title: 'Slg Khách hàng thực hiện',
          width: 100
        },
        {
          title: 'Slg Khách hàng mới',
          width: 100
        },
        {
          title: 'Slg Khách hàng cũ',
          width: 100
        },
        {
          title: 'Slg Khách hàng upsale',
          width: 100
        }
      ]
    },
    {
      title: 'Doanh thu ĐVTV',
      children: [
        {
          title: 'Doanh thu thực hiện',
          width: 100
        },
        {
          title: 'Doanh thu không upsale',
          width: 100
        },
        {
          title: 'Doanh thu upsale đầu vào',
          width: 100
        },
        {
          title: 'Doanh thu upsale đầu ra',
          width: 100
        },
        {
          title: 'Doanh thu khách hàng hiện hữu',
          width: 100
        },
        {
          title: 'Doanh thu khách hàng mới',
          width: 100
        },
        {
          title: 'Doanh thu khách hàng có mã CTV',
          width: 100
        },
        {
          title: 'Doanh thu KH lẻ qua tổng đài',
          width: 100
        },
        {
          title: 'Doanh thu KH lẻ qua App/ Web',
          width: 100
        }
      ]
    },
    {
      title: 'Doanh thu MED-ON: Khách hàng hiện hữu',
      children: [
        {
          title: 'Doanh thu Khách hàng có mã CTV qua Web/ App',
          width: 100
        },
        {
          title: 'Doanh thu Khách hàng có mã CTV qua tổng đài',
          width: 100
        },
        {
          title: 'Doanh thu KH lẻ qua tổng đài có tư vấn gia tăng doanh thu',
          width: 100
        },
        {
          title: 'Doanh thu KH lẻ qua tổng đài không tư vấn gia tăng doanh thu',
          width: 100
        },
        {
          title: 'Doanh thu KH lẻ qua Web/ App',
          width: 100
        }
      ]
    },
    {
      title: 'Doanh thu MED-ON: Khách hàng mới',
      children: [
        {
          title: 'Doanh thu Khách hàng có mã CTV qua tổng đài',
          width: 100
        },
        {
          title: 'Doanh thu Khách hàng có mã CTV qua Web/ App',
          width: 100
        },
        {
          title: 'Doanh thu KH lẻ qua tổng đài có tư vấn gia tăng doanh thu',
          width: 100
        },
        {
          title: 'Doanh thu KH lẻ qua tổng đài không tư vấn gia tăng doanh thu',
          width: 100
        },
        {
          title: 'Doanh thu KH lẻ qua Web/ App',
          width: 100
        }
      ]
    }
  ];

  return (
    <Row className="report-booking-revenue">
      <Col xs={24} className="title">
        <Typography.Title level={4}>Tổng hợp doanh thu đặt lịch</Typography.Title>
        <Button type="primary" onClick={() => exportToExcel([], 'booking-revenue')}>
          <FileExcelOutlined />
          Xuất tất cả
        </Button>
      </Col>
      <Col xs={24} className="table-container">
        <div>
          <Table dataSource={dataSource} columns={columns} scroll={{ x: 'auto' }} />
        </div>
      </Col>
    </Row>
  );
};

export default BookingRevenue;
