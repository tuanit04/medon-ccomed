import { Button, DatePicker, Row, Space, Spin, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import './index.less';
import { FileExcelOutlined, SearchOutlined } from '@ant-design/icons';
import useRestQuery from 'hooks/useRestQuery';
import { reportHospital } from 'config/api';
import moment from 'moment';

interface Props {}

const defaultFromDate = moment();
const defaultToDate = moment();

const AppointmentHospital = () => {
  const [fromDate, setFromDate] = useState(defaultFromDate);
  const [toDate, setToDate] = useState(defaultToDate);
  const [exportUrl, setExportUrl] = useState('#');
  const { data, isLoading, call, isError, errorMessage } = useRestQuery({
    url: reportHospital.html,
    method: 'post',
    initialParams: {
      fromAppointmentDate: fromDate.format('DD/MM/YYYY'),
      toAppointmentDate: toDate.format('DD/MM/YYYY')
    }
  });

  const { data: exportData, isLoading: isExporting, isError: isExportError, call: callExport } = useRestQuery({
    url: reportHospital.xls,
    method: 'post',
    initialParams: {
      fromAppointmentDate: fromDate.format('DD/MM/YYYY'),
      toAppointmentDate: toDate.format('DD/MM/YYYY')
    },
    config: {
      responseType: 'blob',
      headers: {
        'Content-Type': 'application/json'
      }
    },
    isBlob: true
  });

  useEffect(() => {
    if (exportData) {
      const blob = new Blob([exportData], {
        type: 'application/vnd.ms-excel'
      });
      const url = URL.createObjectURL(blob);
      setExportUrl(url);
    }
  }, [exportData]);

  const onChangeFromDate = (value: any, formatString: string) => {
    if (value) {
      call({
        fromAppointmentDate: formatString,
        toAppointmentDate: toDate.format('DD/MM/YYYY')
      });
      callExport({
        fromAppointmentDate: formatString,
        toAppointmentDate: toDate.format('DD/MM/YYYY')
      });
      setFromDate(value);
    } else {
      call({
        fromAppointmentDate: defaultFromDate.format('DD/MM/YYYY'),
        toAppointmentDate: toDate.format('DD/MM/YYYY')
      });
      callExport({
        fromAppointmentDate: defaultFromDate.format('DD/MM/YYYY'),
        toAppointmentDate: toDate.format('DD/MM/YYYY')
      });
      setFromDate(defaultFromDate);
    }
  };

  const onChangeToDate = (value: any, formatString: string) => {
    if (value) {
      call({
        fromAppointmentDate: fromDate.format('DD/MM/YYYY'),
        toAppointmentDate: formatString
      });
      callExport({
        fromAppointmentDate: fromDate.format('DD/MM/YYYY'),
        toAppointmentDate: formatString
      });
      setToDate(value);
    } else {
      call({
        fromAppointmentDate: fromDate.format('DD/MM/YYYY'),
        toAppointmentDate: defaultToDate.format('DD/MM/YYYY')
      });
      callExport({
        fromAppointmentDate: fromDate.format('DD/MM/YYYY'),
        toAppointmentDate: defaultToDate.format('DD/MM/YYYY')
      });
      setToDate(defaultToDate);
    }
  };

  const onExport = () => {
    // if (exportData) {
    //   window.open(exportUrl);
    // }
  };

  const onSearch = () => {
    call({
      fromAppointmentDate: fromDate.format('DD/MM/YYYY'),
      toAppointmentDate: toDate.format('DD/MM/YYYY')
    });
    callExport({
      fromAppointmentDate: fromDate.format('DD/MM/YYYY'),
      toAppointmentDate: toDate.format('DD/MM/YYYY')
    });
  };

  return (
    <div className="report-appointment-hospital-container">
      <div className="report-appointment-hospital-title">
        <Typography.Title level={4}>Thống kê lịch hẹn tại bệnh viện/phòng khám</Typography.Title>
        <Space className="report-appointment-hospital-date">
          Từ ngày
          <DatePicker
            value={fromDate}
            disabledDate={date => date.utc() > toDate.utc()}
            format="DD/MM/YYYY"
            onChange={onChangeFromDate}
          />
          Đến ngày
          <DatePicker
            value={toDate}
            disabledDate={date => date.utc() < fromDate.utc()}
            format="DD/MM/YYYY"
            onChange={onChangeToDate}
          />
          <Button type="primary" onClick={onSearch}>
            <SearchOutlined />
            Tìm kiếm
          </Button>
          <Button
            href={exportUrl}
            disabled={isExporting || isExportError}
            type="primary"
            onClick={onExport}
            download="report-appointment.xls"
            title="Xuất Excel"
          >
            <FileExcelOutlined />
          </Button>
        </Space>
      </div>
      {isLoading ? (
        <Spin spinning={isLoading}></Spin>
      ) : (
        <Row className="report-appointment-hospital" gutter={[16, 16]}>
          {isError ? (
            errorMessage
          ) : data ? (
            <iframe title="Report" srcDoc={data} width="100%" frameBorder="0" />
          ) : (
            'Không có dữ liệu'
          )}
        </Row>
      )}
    </div>
  );
};

export default AppointmentHospital;
