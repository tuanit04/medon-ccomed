import React from 'react';
import { Button, Col, Row, Typography, Table, Space, Tooltip } from 'antd';
import { FileExcelOutlined, InfoCircleOutlined } from '@ant-design/icons';
import exportToExcel from 'utils/exportExcel';
import './index.less';
import { ColumnType } from 'antd/lib/table';

interface Props {}

const BookingRevenueDetail = (props: Props) => {
  const dataSource = [
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    },
    {
      date: '04/10/2021'
    }
  ];

  const columns: ColumnType<any>[] = [
    {
      title: 'Ngày sử dụng',
      dataIndex: 'date',
      width: 100
    },
    {
      title: 'SID',
      width: 100
    },
    {
      title: 'Mã KCB',
      width: 100
    },
    {
      title: 'Mã lịch hẹn',
      width: 100
    },
    {
      title: 'Tên KH',
      width: 100
    },
    {
      title: 'Mã dịch vụ',
      width: 100
    },
    {
      title: () => (
        <Space direction="vertical" size={4}>
          <span>{'Đơn vị thực hiện'}</span>
          <Tooltip title="Theo pháp nhân">
            <InfoCircleOutlined />
          </Tooltip>
        </Space>
      ),
      width: 100
    },
    {
      title: 'Văn phòng thực hiện',
      width: 100
    },
    {
      title: 'Nền tảng',
      width: 100
    },
    {
      title: 'Phân loại khách hàng',
      width: 100
    },
    {
      title: () => (
        <Space direction="vertical" size={4}>
          <span>{'Mã Cộng tác viên Medlatec'}</span>
          <Tooltip title="Bác sĩ giới thiệu KH đến sử dụng dịch vụ của Medlatec">
            <InfoCircleOutlined />
          </Tooltip>
        </Space>
      ),
      width: 100
    },
    {
      title: 'Hình thức upsale',
      width: 100
    },
    {
      title: () => (
        <Space direction="vertical" size={4}>
          <span>{'Mã nhân viên bác sĩ tư vấn gia tăng doanh thu'}</span>
          <Tooltip title="Bao gồm: Bác sĩ MED-ON, Bác sĩ Medlatec, Bác sĩ CTV hợp tác với MED-ON">
            <InfoCircleOutlined />
          </Tooltip>
        </Space>
      ),
      width: 100
    },
    {
      title: () => (
        <Space direction="vertical" size={4}>
          <span>{'Tên nhân viên bác sĩ tư vấn gia tăng doanh thu'}</span>
          <Tooltip title="Bao gồm: Bác sĩ MED-ON, Bác sĩ Medlatec, Bác sĩ CTV hợp tác với MED-ON">
            <InfoCircleOutlined />
          </Tooltip>
        </Space>
      ),
      width: 100
    },
    {
      title: () => (
        <Space direction="vertical" size={4}>
          <span>{'Doanh thu thực hiện'}</span>
          <Tooltip title="Thực thu của ĐVTV">
            <InfoCircleOutlined />
          </Tooltip>
        </Space>
      ),
      width: 100
    },
    {
      title: 'Doanh thu không upsale',
      width: 100
    },
    {
      title: 'Doanh thu upsale',
      width: 100
    },
    {
      title: () => (
        <Space direction="vertical" size={4}>
          <span>{'Doanh thu MEDON'}</span>
          <Tooltip title="Công thức có thể thay đổi">
            <InfoCircleOutlined />
          </Tooltip>
        </Space>
      ),
      width: 100
    }
  ];

  return (
    <Row className="report-booking-revenue-detail">
      <Col xs={24} className="title">
        <Typography.Title level={4}>Chi tiết doanh thu đặt lịch</Typography.Title>
        <Button type="primary" onClick={() => exportToExcel([], 'booking-revenue')}>
          <FileExcelOutlined />
          Xuất tất cả
        </Button>
      </Col>
      <Col xs={24} className="table-container">
        <div>
          <Table dataSource={dataSource} columns={columns} scroll={{ x: 'auto' }} />
        </div>
      </Col>
    </Row>
  );
};

export default BookingRevenueDetail;
