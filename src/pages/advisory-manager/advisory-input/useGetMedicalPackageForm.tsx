import React, { FC, useEffect, useState } from 'react';
import { Form, Input, Table, Button, Select, Row, Col } from 'antd';
import { Role } from 'interface/permission/role.interface';
import { useGetAllPackageCCOMEDLazy, useGetPackage } from 'hooks/package';
import { Packages } from 'common/interfaces/packages.interface';
import { isEmptyObj } from 'utils/helper';
import { useGetFacilitys } from 'hooks/facility';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import ExaminationPackageCreateDialog from './assignAppoinment/examinationPackage/examinationPackageCreateDialog';

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};
const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 }
};

interface PackageFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  /** Initial form data */
  values?: Role;
}
export default function useGetPackageForm({}: PackageFormProps) {
  const [salePriceField, setSalePriceField] = useState('');
  const getFiltered = ({ searchField, search }) => {
    let filter = {};
    if (search) {
      filter = {
        [searchField]: search
      };
    }
    return filter;
  };

  //Danh sách Gói khám
  const ListPackages: FC<{
    setPage;
    pageSize;
    pagingPackages: any;
    filter: any;
    setFilter: any;
    loadPackages: any;
  }> = ({ setPage, filter, setFilter, pagingPackages, loadPackages, pageSize }) => {
    const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
      setFilter({ ...filter, search: e.target.value });
    };

    const onSelect = (value: string) => {
      setFilter({ ...filter, searchField: value });
    };

    const onSearch = (filter?) => {
      setPage(1);
      if (!isEmptyObj(filter)) {
        var name = filter['searchField'];
        filter[name] = filter['search'];
        var filterOb: any = {
          provinceId: filter['provinceId'],
          [name]: filter['search']
        };
        const variables = {
          //page: pagingDoctors?.page ?? 1,
          page: 1,
          pageSize: pageSize ?? 50,
          filter: filterOb
        };
        //setPage(1);
        //setPageSize(pagingPackages?.pageSize ?? 50);
        setFilter(filter);
        loadPackages({ variables: variables });
      } else {
        setFilter(filter);
        const filterOb = filter;
        delete filterOb.searchField;
        delete filter.search;
        const variables = {
          //page: pagingDoctors?.page ?? 1,
          page: 1,
          pageSize: pageSize ?? 50,
          filter: filter
        };
        //setPage(1);
        //setPageSize(pagingPackages?.pageSize ?? 50);
        loadPackages({ variables: variables });
      }
    };

    const select = (
      <Select value={filter.searchField} onChange={onSelect}>
        <Select.Option value="name">Tên Gói khám</Select.Option>
        <Select.Option value="code">Mã Gói khám</Select.Option>
      </Select>
    );

    return (
      <Row style={{ marginTop: 10 }} align="middle">
        <Col className="gutter-row" span={4}>
          <span style={{ fontWeight: 400, color: 'red' }}>
            Tìm kiếm ({pagingPackages['records'] ? pagingPackages['records'] + ' bản ghi' : '0 bản ghi'})
          </span>
        </Col>
        <Col className="gutter-row" span={20}>
          <Input
            placeholder="Nhập thông tin tìm kiếm"
            style={{ width: '100%' }}
            value={filter.search}
            onChange={onChange}
            onPressEnter={(e: any) => {
              onSearch({ ...filter, search: e.target.value });
            }}
            addonBefore={select}
          />
        </Col>
        {/*<Col className="gutter-row" span={8}>
          <Button style={{ marginLeft: 5 }} type="primary" onClick={onSearch}>
            <SearchOutlined />
            Tìm kiếm
          </Button>
          </Col>*/}
      </Row>
    );
  };
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'VND',
    minimumFractionDigits: 0
  });
  //const salePriceField = meth === 'HOME' ? 'homeSalePrice' : 'hosSalePrice';
  const columns = [
    {
      title: 'Tên gói khám',
      dataIndex: 'name',
      width: '28%'
    },
    {
      title: 'Mã gói khám',
      dataIndex: 'code',
      width: '10%'
    },
    {
      title: 'Giá gốc theo tỉnh/thành',
      dataIndex: 'originPrice',
      width: '10%',
      render: originPrice => {
        return originPrice && formatter.format(originPrice);
      }
    },
    {
      title: 'Giá giảm theo tỉnh/thành',
      dataIndex: 'methodType',
      width: '10%',
      render: (methodType, record) => {
        return record?.methodType === 'HOME'
          ? formatter.format(record?.homeSalePrice ? record?.homeSalePrice : 0)
          : formatter.format(record?.hosSalePrice ? record?.hosSalePrice : 0);
      }
    },
    {
      title: 'Giới tính',
      dataIndex: 'sex',
      width: '7%',
      render: value => value && (value === 'MALE' ? 'Nam' : 'Nữ')
    },
    {
      title: 'Hình thức',
      dataIndex: 'methodType',
      width: '10%',
      render: methodType =>
        methodType && (methodType === 'HOME' ? 'Tại nhà' : methodType === 'HOSPITAL' ? 'Tại viện' : 'Cả hai')
    },
    {
      title: 'Ngày bắt đầu',
      width: '15%',
      dataIndex: 'startDate',
      render: value => (value ? String(value).split(' ')[0] : '')
    },
    {
      title: 'Ngày kết thúc',
      dataIndex: 'endDate',
      width: '15%',
      render: value => (value ? String(value).split(' ')[0] : '')
    }
  ];
  interface ListPackageFormProps {
    status?: number;
    packageObj?: Packages;
    provinceId?: string;
    methodType: string;
    setPackageSelectedArr: (packageSelectedArr: any) => void;
  }
  const ListPackageForm: FC<ListPackageFormProps> = ({
    status,
    setPackageSelectedArr,
    provinceId,
    packageObj,
    methodType
  }) => {
    if (methodType) {
      setSalePriceField(methodType === 'HOME' ? 'homeSalePrice' : 'hosSalePrice');
    }
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(20);
    const [pagination, setPagination] = useState({});
    let filter = {
      provinceId
    };
    const variables = { filter, page, pageSize };
    //Hook Lấy danh dánh sách gói khám
    const { allPackageCCOMED, isLoadingPackages, loadPackages } = useGetAllPackageCCOMEDLazy();
    useEffect(() => {
      loadPackages({ variables: variables });
    }, []);
    useEffect(() => {
      loadPackages({ variables: variables });
    }, [provinceId]);
    const [packageDataSelectedState, setPackageDataSelectedState]: any = useState([]);
    let packageDataSelectedArr = [{}];
    packageDataSelectedArr.shift();
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);
    const [loading, setLoading] = useState(false);
    const [filterInfor, setFilterInfor] = useState({
      provinceId,
      searchField: 'name',
      search: ''
    });
    const { facilities: listFactility, isLoadingFacilities, loadFacilities } = useGetFacilitys();
    const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
    const { package: itemSelect, isLoadingPackage, loadPackage } = useGetPackage();
    const [visibleEx, setVisibleEx] = useState(false);
    const [values, setValues] = useState();
    useEffect(() => {
      loadFacilities({});
      loadProvinces({});
      // loadChannels({});
    }, []);
    useEffect(() => {
      if (itemSelect && Object.keys(itemSelect).length !== 0) {
        setValues(itemSelect);
      }
    }, [itemSelect]);

    const handleTableChange = (pagination, filters, sorter) => {
      reFetchData({
        sortField: sorter.field,
        sortOrder: sorter.order,
        pagination,
        ...filters
      });
    };
    const reFetchData = (params = {}) => {
      let pagination = params['pagination'];
      if (pageSize !== pagination['pageSize']) {
        setPage(1);
      } else {
        setPage(pagination['current']);
      }
      setPageSize(pagination['pageSize']);
      var name = filterInfor['searchField'];
      filterInfor[name] = filterInfor['search'];
      var filter: any = {
        provinceId: filterInfor['provinceId'],
        [name]: filterInfor['search']
      };
      loadPackages({ variables: { page: pagination['current'], pageSize: pagination['pageSize'], filter } });
    };
    //check obj empty
    const isEmpty = obj => {
      for (var key in obj) {
        if (obj.hasOwnProperty(key)) return false;
      }
      return true;
    };
    //Check Gói khám đã chọn
    const [packagesRes, setPackagesRes] = useState([]);
    useEffect(() => {
      setPagination({ current: page, pageSize: pageSize, total: allPackageCCOMED['records'] });
      if (!isEmpty(allPackageCCOMED) && packageObj) {
        setPackagesRes([]);
        let packagesCheckSelected: any = [];
        for (let i = 0; i < allPackageCCOMED?.data?.length; i++) {
          let packageObjArr = { ...allPackageCCOMED?.data[i] };
          let flag = false;
          if (packageObjArr['id'] === packageObj?.id) {
            packageObjArr['chosen'] = true;
            flag = true;
          }
          if (!flag) {
            packageObjArr['chosen'] = false;
          }
          packagesCheckSelected.push(packageObjArr);
        }
        setSelectedRowKeys(packagesCheckSelected?.filter(item => item.chosen)?.map(item => item.id));
        setPackagesRes(packagesCheckSelected);
      }
    }, [allPackageCCOMED]);

    useEffect(() => {
      if (packageObj) {
        setPackageDataSelectedState([packageObj]);
      }
    }, [packageObj]);

    const start = () => {
      setLoading(true);
      // ajax request after empty completing
      setTimeout(() => {
        setSelectedRowKeys([]);
        setPackageDataSelectedState([]);
        setPackageSelectedArr([]);
        setLoading(false);
      }, 500);
    };
    const onSelectChange = selectedRowKeys => {
      setSelectedRowKeys(selectedRowKeys);
      for (let i = 0; i < allPackageCCOMED?.data.length; i++) {
        for (let j = 0; j < selectedRowKeys.length; j++) {
          if (allPackageCCOMED?.data[i]['id'] === selectedRowKeys[j]) {
            packageDataSelectedArr.push(allPackageCCOMED?.data[i]);
          }
        }
      }
      setPackageDataSelectedState(packageDataSelectedArr);
      setPackageSelectedArr(packageDataSelectedArr);
    };

    const hasSelected = selectedRowKeys.length > 0 || !isEmptyObj(packageObj);
    const listPackageForm = (
      <>
        <Form {...formItemLayout}>
          <ListPackages
            setPage={setPage}
            pageSize={pageSize}
            filter={filterInfor}
            setFilter={setFilterInfor}
            pagingPackages={allPackageCCOMED}
            loadPackages={loadPackages}
          />
        </Form>
        <div style={{ marginTop: 5 }}>
          {/*<span style={{ marginLeft: 8, fontWeight: 'bold', color: '#206ad2' }}>DANH SÁCH GÓI KHÁM</span>*/}
        </div>
        <ExaminationPackageCreateDialog
          visible={visibleEx}
          values={values}
          onCancel={() => {
            setVisibleEx(false);
          }}
          provinces={provinces}
          facilitiesList={listFactility}
        />
        <Table
          title={() => (
            <>
              <div style={{ marginBottom: 16, marginTop: 15 }}>
                <Button type="primary" onClick={start} disabled={!hasSelected || status === 2} loading={loading}>
                  Chọn lại
                </Button>
                <span style={{ marginLeft: 5 }}>Gói khám đã chọn :</span>
                <span style={{ marginLeft: 8, color: 'red' }}>
                  {packageDataSelectedState?.length > 0 ? packageDataSelectedState[0]?.name : '0'}
                </span>
              </div>
            </>
          )}
          loading={isLoadingPackages}
          pagination={{
            //current: pagingDoctors?.page ?? 1,
            //pageSize: pagingDoctors?.pageSize ?? 20,
            ...pagination,
            defaultPageSize: 20,
            total: allPackageCCOMED?.records ?? 0,
            pageSizeOptions: ['10', '20', '30', '50']
          }}
          onChange={handleTableChange}
          rowKey={record => record.id}
          rowSelection={{
            selectedRowKeys,
            onChange: onSelectChange,
            type: 'radio',
            getCheckboxProps: () => ({
              disabled: status === 2
            })
          }}
          scroll={{ y: 300 }}
          columns={columns}
          dataSource={packageObj ? packagesRes : allPackageCCOMED?.data}
          onRow={(record, rowIndex) => {
            return {
              onDoubleClick: () => {
                loadPackage({
                  variables: {
                    id: record.id
                  }
                });
                setVisibleEx(true);
              }
            };
          }}
        />
      </>
    );
    return listPackageForm;
  };
  return {
    ListPackageForm
  };
}
