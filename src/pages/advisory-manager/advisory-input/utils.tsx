import React from 'react';
import { Tag } from 'antd';

export const getStatus = (status?: number) =>
  status === 1 ? (
    <Tag style={{ fontWeight: 500 }} color="#f1b91c">
      Chờ xác nhận
    </Tag>
  ) : status === 2 ? (
    <Tag color="#dddedf">
      <span style={{ color: '#0c0c0c', fontWeight: 500 }}>Đã xác nhận</span>
    </Tag>
  ) : status === 3 ? (
    <Tag style={{ fontWeight: 500 }} color="#2db7f5">
      Chờ tư vấn
    </Tag>
  ) : status === 4 ? (
    <Tag style={{ fontWeight: 500 }}>Đã tư vấn đầu vào</Tag>
  ) : status === 5 ? (
    <Tag style={{ fontWeight: 500 }}>Chưa check</Tag>
  ) : status === 6 ? (
    <Tag style={{ fontWeight: 500 }}>Đã check</Tag>
  ) : status === 7 ? (
    <Tag style={{ fontWeight: 500 }}>Đã lấy mẫu</Tag>
  ) : status === 8 ? (
    <Tag style={{ fontWeight: 500 }}>Đã có kết quả</Tag>
  ) : status === 9 ? (
    <Tag style={{ fontWeight: 500 }}>Đã đăng ký khám</Tag>
  ) : status === 10 ? (
    <Tag style={{ fontWeight: 500 }}>Đã thanh toán</Tag>
  ) : status === 11 ? (
    <Tag style={{ fontWeight: 500 }}>Đã khám</Tag>
  ) : status === 13 ? (
    <Tag style={{ fontWeight: 500 }} color="#636363">
      <span style={{ color: '#fff' }}>Đã huỷ</span>
    </Tag>
  ) : (
    <Tag></Tag>
  );

export const getStatusPackages = (status?: number) => (status === 1 ? <Tag color="#87d068">Đã duyệt</Tag> : '');
