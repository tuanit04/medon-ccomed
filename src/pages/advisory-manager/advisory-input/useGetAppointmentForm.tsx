import React, { FC, useEffect } from 'react';
import { Form, Input, Col, Row, Select, DatePicker, Button } from 'antd';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import { Role } from 'interface/permission/role.interface';
import { ClearOutlined, SearchOutlined } from '@ant-design/icons';
import moment from 'moment';
import { functionCodeConstants } from 'constants/functions';
import { Channel } from 'common/interfaces/channel.interface';
import { Facility } from 'common/interfaces/facility.interface';
const { Option } = Select;
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 6,
  xl: 6,
  xxl: 6
};
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};

interface AppointmentScheduleFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  /** Initial form data */
  values?: Role;
  functionOb?: any;
}

export default function useGetAppointmentForm({
  required = false,
  responsive = false,
  name = 'searchForm',
  functionOb,
  values
}: AppointmentScheduleFormProps) {
  const [formInstance] = Form.useForm<FormProps>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={values}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  useEffect(() => {
    formInstance.resetFields();
  }, [formInstance, values]);

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  interface DateTypeProps {
    onChange: (value: any) => void;
  }
  const DateType: FC<DateTypeProps> = ({ onChange }) => {
    const { RangePicker } = DatePicker;
    function handleChange(value: any) {
      onChange(value);
    }
    const dateType = (
      <Form.Item className="appoiment-cpn" name="dateType" label="Lọc theo ngày">
        <Select onChange={handleChange} defaultValue="createDate">
          <Option value="appointmentDate">Ngày đặt lịch</Option>
          <Option value="createDate">Ngày tạo lịch</Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{dateType}</Col> : dateType;
  };

  const FromDate: FC = () => {
    function handleChange(value: any) {}
    const fromDate = (
      <Form.Item className="appoiment-cpn" name="fromDate" label="Từ ngày">
        <DatePicker defaultValue={moment()} format="DD/MM/YYYY" placeholder="Chọn Từ ngày" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{fromDate}</Col> : fromDate;
  };

  const ToDate: FC = () => {
    function handleChange(value: any) {}
    const toDate = (
      <Form.Item className="appoiment-cpn" name="toDate" label="Đến ngày">
        <DatePicker defaultValue={moment()} format="DD/MM/YYYY" placeholder="Chọn Đến ngày" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{toDate}</Col> : toDate;
  };

  const InfoType: FC = () => {
    const infoType = (
      <Form.Item className="appoiment-cpn" name="infoType" label="Tìm kiếm theo">
        <Select defaultValue="patientPhone">
          <Option value="patientName">Tên khách hàng</Option>
          <Option value="patientPhone">Số điện thoại</Option>
          <Option value="patientAddress">Địa chỉ liên hệ</Option>
          <Option value="assignStaffName">Cán bộ tại nhà</Option>
          <Option value="appointmentHisId">Mã lịch hẹn</Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{infoType}</Col> : infoType;
  };

  const Info: FC = () => {
    const info = (
      <Form.Item className="appoiment-cpn" name="info" label="Giá trị tìm kiếm">
        <Input placeholder="Nhập thông tin tìm kiếm" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{info}</Col> : info;
  };

  const AppointmentType: FC = () => {
    function handleChange(value: any) {}

    const { Option } = Select;
    const appointmentType = (
      <Form.Item className="appoiment-cpn" name="appointmentType" label="Loại lịch">
        <Select defaultValue="" onChange={handleChange}>
          <Option key="all" value="">
            -- Tất cả loại --
          </Option>
          <Option key="LH " value="SCHEDULE">
            Lịch hẹn
          </Option>
          <Option key="GK" value="PACKAGE">
            Gói khám
          </Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentType}</Col> : appointmentType;
  };

  const MethodType: FC = () => {
    function handleChange(value: any) {}

    const { Option } = Select;
    const methodType = (
      <Form.Item className="appoiment-cpn" name="methodType" label="Hình thức">
        <Select defaultValue="">
          <Option key="all" value="">
            -- Tất cả hình thức --
          </Option>
          <Option key="TN" value="HOME">
            Tại nhà
          </Option>
          <Option key="TV" value="HOSPITAL">
            Tại viện
          </Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{methodType}</Col> : methodType;
  };
  const Status: FC = () => {
    const status = (
      <Form.Item name="status" className="appoiment-cpn" label="Trạng thái">
        <Select defaultValue="" onChange={value => {}}>
          <Option key="all" value="">
            -- Tất cả trạng thái --
          </Option>
          <Option key="3" value={3}>
            Chờ tư vấn đầu vào
          </Option>
          <Option key="4" value={4}>
            Đã vấn đầu vào
          </Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{status}</Col> : status;
  };
  //Nguon lich hen
  interface AppointmentSourceProps {
    channelList: Channel[];
  }
  const AppointmentSource: FC<AppointmentSourceProps> = ({ channelList }) => {
    // console.log('channelList : ', channelList);
    const appointmentSource = (
      <Form.Item
        className="appoiment-cpn"
        name="code"
        label="Nguồn lịch hẹn"
        rules={[
          {
            required: false,
            message: 'Nguồn lịch hẹn không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          defaultValue=""
        >
          <Option key="all" value="">
            -- Tất cả --
          </Option>
          {channelList?.map(channel => (
            <Select.Option key={channel.id} value={channel.code}>
              {channel.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentSource}</Col> : appointmentSource;
  };
  //Đơn vị
  interface FacilityProps {
    facilityList: Facility[];
  }
  const Facility: FC<FacilityProps> = ({ facilityList }) => {
    // console.log('channelList : ', channelList);
    const facility = (
      <Form.Item
        className="appoiment-cpn"
        name="examFacilityId"
        label="Đơn vị"
        rules={[
          {
            required: false,
            message: 'Đơn vị không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          defaultValue=""
        >
          <Option key="all" value="">
            -- Tất cả --
          </Option>
          {facilityList?.map(facility => (
            <Select.Option key={facility.id} value={facility.id}>
              {facility.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{facility}</Col> : facility;
  };
  //Buttons
  interface ButtonsProps {
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          onClick={() => {
            onSearch();
          }}
          icon={<SearchOutlined />}
          type="primary"
          disabled={functionOb[functionCodeConstants.TD_TUVAN_DAUVAO_TIMKIEM] ? false : true}
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button
          onClick={resetFields}
          icon={<ClearOutlined />}
          disabled={functionOb[functionCodeConstants.TD_TUVAN_DAUVAO_TIMKIEM] ? false : true}
        >
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };

  return {
    form: formInstance,
    Form: WrappedForm,
    DateType,
    FromDate,
    ToDate,
    Info,
    InfoType,
    AppointmentType,
    MethodType,
    Status,
    AppointmentSource,
    Facility,
    Buttons
  };
}
