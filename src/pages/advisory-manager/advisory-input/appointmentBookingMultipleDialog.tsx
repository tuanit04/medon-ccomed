import { DatePicker, Form, Input, Modal } from 'antd';
import React, { FC, useEffect } from 'react';
import './index.less';
import { AppointmentInput } from 'common/interfaces/input/appointmentInput.interfce';
import { AppointmentPatient } from 'common/interfaces/appointmentPatient.interface';
import moment from 'moment';
import { functionCodeConstants } from 'constants/functions';
const formItemLayout = {
  labelCol: {
    span: 6
  },
  wrapperCol: {
    span: 8
  }
};

interface AppointmentBookingMultipleDialogProps {
  visible: boolean;
  onBookingMutipleAppointment: (values: AppointmentInput) => void;
  appointmentPatient: AppointmentPatient;
  onCancel: () => void;
  functionOb: any;
}

const AppointmentBookingMultipleDialog: FC<AppointmentBookingMultipleDialogProps> = ({
  visible,
  onBookingMutipleAppointment,
  appointmentPatient,
  onCancel,
  functionOb
}) => {
  const [form] = Form.useForm();
  const onSubmit = async () => {
    try {
      const values = await form.validateFields();
      onBookingMutipleAppointment({ ...appointmentPatient, ...values });
    } catch (errorInfo) {}
  };
  useEffect(() => {
    form.resetFields();
  }, [visible]);
  const onChange = (dates, dateStrings) => {};
  const disabledDateToDate = current => {
    return (
      current &&
      current <
        moment(form.getFieldValue('fromAppointmentDate'))
          .add(1, 'days')
          .startOf('day')
    );
  };
  const disabledDateFromDate = current => {
    return (
      current &&
      current <
        moment(appointmentPatient?.appointmentDate, 'DD/MM/YYYY HH:mm:ss')
          .add(1, 'days')
          .startOf('day')
    );
  };
  return (
    <Modal
      maskClosable={false}
      style={{ marginTop: 15 }}
      width={800}
      title="Đặt nhiều lịch hẹn"
      visible={visible}
      onOk={onSubmit}
      onCancel={onCancel}
      okButtonProps={{ disabled: functionOb[functionCodeConstants.TD_LH_DATNHIEU] ? false : true }}
    >
      <Form initialValues={appointmentPatient} form={form} name="dynamic_rule">
        <Form.Item {...formItemLayout} name="patientName" style={{ marginTop: 10 }} label="Tên khách hàng">
          <Input disabled />
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          name="fromAppointmentDate"
          label="Ngày bắt đầu"
          rules={[
            {
              required: true,
              message: 'Ngày bắt đầu không được bỏ trống.'
            }
          ]}
        >
          <DatePicker
            disabledDate={disabledDateFromDate}
            placeholder="Chọn ngày bắt đầu"
            format="DD/MM/YYYY"
            onChange={onChange}
          />
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          name="toAppointmentDate"
          label="Ngày kết thúc"
          rules={[
            {
              required: true,
              message: 'Ngày kết thúc không được bỏ trống.'
            }
          ]}
        >
          <DatePicker
            disabledDate={disabledDateToDate}
            placeholder="Chọn ngày kết thúc"
            format="DD/MM/YYYY"
            onChange={onChange}
          />
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          name="reExamDate"
          label="Cách ngày"
          rules={[
            {
              required: true,
              message: 'Giá trị bắt buộc phải là số, và khác 0.',
              pattern: new RegExp(/^[1-9]+$/)
            }
          ]}
        >
          <Input placeholder="Nhập cách ngày" />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default AppointmentBookingMultipleDialog;
