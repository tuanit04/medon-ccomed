import React, { FC, useEffect, useState } from 'react';
import AppointmentTable from './appointmentTable';
import AppointmentSearch from './appointmentSearch';
import './index.less';
import { AppointmentPatient } from 'common/interfaces/appointmentPatient.interface';
import { useDeleteAppointment } from 'hooks/appointment/useDeleteAppointment';
import AppointmentCreateDialog from './appointmentCreateDialog';
import { useCreateAppointment } from 'hooks/appointment';
import moment from 'moment';
import { AppointmentInput } from 'common/interfaces/input/appointmentInput.interfce';
import AppointmentModifyDialog from './appointmentModifyDialog';
import { useAppointmentDetailToConsultant } from 'hooks/appointment/useGetAppointmentDetail';
import AppointmentBookingMultipleDialog from './appointmentBookingMultipleDialog';
import { useSaveMultiAppointment } from 'hooks/appointment/useSaveMultiAppointment';
import { openNotificationRight } from 'utils/notification';
import { FormInstance, FormProps } from 'antd';
import { useAppState } from 'helpers';
import { useGetChannels } from 'hooks/channel';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { useForwardAppointment } from 'hooks/appointment/useForwardAppointmentToDoctorConsultant';
import { useGetAppointmentConsultants } from 'hooks/appointment/useGetAppointmentConsultants';
import { useCancleConsultantAppointment } from 'hooks/appointment/useCancelConsultantAppointment';
import { useConsultantAppointment } from 'hooks/appointment/useConsultantAppointment';

const AppointmentPage: FC = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(50);
  const [filtered, setFiltered] = useState<FilteredInput[]>([
    {
      id: 'createDate',
      operation: 'between',
      value: moment().format('DD/MM/YYYY') + ',' + moment().format('DD/MM/YYYY')
    }
  ]);

  const [mcreateVisible, setCreateVisible] = useState(false);
  const [isForward, setIsForward] = useState(false);
  const [modifyVisible, setModifyVisible] = useState(false);
  const [assignStaffName, setAssignStaffName] = useState('');
  const [bookingMultipleVisible, setBookingMultipleVisible] = useState(false);
  const [appointmentPatientSelected, setAppointmentPatientSelected] = useState({} as AppointmentPatient);
  const [appointmentIdRes, setAppointmentIdRes] = useState('');
  const [values, setValues] = useState({} as AppointmentPatient);
  //lắng nghe Id trả về
  const onSetIdRes = async id => {
    setAppointmentIdRes(id);
    //console.log('isForward : ', isForward);
    if (isForward) {
      handleForwardAppointment({ id });
    }
  };
  const onResetAppointmentIdRes = () => {
    setAppointmentIdRes('');
  };
  const onSetCreateVisible = (isShow: boolean) => {
    setCreateVisible(isShow && isForward);
  };
  const onSeModifyVisible = (isShow: boolean) => {
    setModifyVisible(isShow);
    //refetchAppointments();
  };
  //Xoá Lịch hẹn
  const {
    deleteAppointment,
    isLoadingDeleteAppointment,
    resultDeleteAppointment,
    errorDeleteAppointment
  } = useDeleteAppointment();
  //Chuyển BS Tư vấn
  const {
    forwardAppointment,
    resultForwardAppointment,
    isLoadingForwardAppointment,
    errorForwardAppointment
  } = useForwardAppointment(onSetIdRes, onSeModifyVisible);
  const handleForwardAppointment = async appointmentId => {
    await forwardAppointment(appointmentId);
    refetchAppointments?.();
  };
  const handleDeleteAppointment = async id => {
    await deleteAppointment({ id });
  };
  //Hủy lịch hẹn
  const {
    cancleConsultantAppointment,
    isLoadingCancleConsultantAppointment,
    resultCancleConsultantAppointment,
    errorCancleConsultantAppointment
  } = useCancleConsultantAppointment(onSetIdRes, onSeModifyVisible);
  const handleCancelConsultantAppointment = async id => {
    await cancleConsultantAppointment({ id });
    refetchAppointments?.();
  };

  //Xử lý thêm mới Lịch hẹn
  const {
    createAppointment,
    resultSaveAppointment,
    isLoadingSaveAppointment,
    errorSaveAppointment
  } = useCreateAppointment(onSetIdRes, onSetCreateVisible, handleForwardAppointment);
  useEffect(() => {
    if (resultSaveAppointment) {
    }
  }, [resultSaveAppointment]);
  const handleCreateAppointment = async variables => {
    await createAppointment(variables);
  };
  //Xử lý Cập nhật Lịch hẹn
  const {
    consultantAppointment,
    resultConsultantAppointment,
    isLoadingConsultantAppointment,
    errorConsultantAppointment
  } = useConsultantAppointment(onSetIdRes, onSeModifyVisible);
  //C1 Lấy chi tiết lịch hẹn từ table lưu vào state
  const [appointmentIdSelected, setAppointmentIdSelected] = useState({});
  const [appointmentPatient, setAppointmentPatient]: any = useState();
  //C1. Lấy chi tiết lịch hẹn
  const {
    loadAppointmentDetailToConsultant,
    isLoadingAppointmentDetailToConsultant,
    appointmentDetailToConsultant
  } = useAppointmentDetailToConsultant();
  /**/
  //Lấy nguồn lịch hẹn
  const { channels, isLoadingChannels, loadChannels } = useGetChannels();
  const {
    saveMultiAppointment,
    resultSaveMultiAppointment,
    isLoadingSaveMultiAppointment,
    errorSaveMultiAppointment
  } = useSaveMultiAppointment(onSetIdRes);
  const {
    pagingAppointments,
    isLoadingAppointments,
    errorAppointments,
    refetchAppointments
  } = useGetAppointmentConsultants({
    page,
    pageSize,
    filtered
  });
  /**Tìm kiếm Lịch hẹn**/
  //Lọc theo ngày

  //Thông tin tìm kiếm

  const [infoType, setInfoType] = useState('patientPhone');
  const [infoSearch, setInfoSearch] = useState('');
  const [dateType, setDateType] = useState('createDate');
  const [appointmentTypeSearch, setAppointmentTypeSearch] = useState({
    id: 'appointmentType',
    value: '',
    operation: '=='
  });
  const [methodTypeSearch, setMethodTypeSearch] = useState({
    id: 'methodType',
    value: '',
    operation: '=='
  });
  const [statusSearch, setStatusSearch] = useState({
    id: 'status',
    value: '',
    operation: '=='
  });
  const [channelSearch, setChannelSearch] = useState({
    id: 'channelType',
    value: '',
    operation: '=='
  });
  const [examFacilitySearch, setExamFacilitySearch] = useState({
    id: 'examFacilityId',
    value: '',
    operation: '=='
  });
  const [emptySearch, setEmptySearch] = useState(Number);
  const [fromDateSearch, setFromDateSearch] = useState(moment().format('DD/MM/YYYY'));
  const [toDateSearch, setToDateSearch] = useState(moment().format('DD/MM/YYYY'));
  const [form, setForm] = useState({} as FormInstance<FormProps<any>>);
  const functionOb = useAppState(state => state.user.functionObject);

  useEffect(() => {
    loadChannels();
  }, []);

  return (
    <div className="main-content">
      <AppointmentSearch
        functionOb={functionOb}
        channels={channels}
        onSetFormInstance={(value: FormInstance<FormProps<any>>) => {
          setForm(value);
        }}
        onSearch={value => {
          /*setEmptySearch(Math.random());
          if (value['dateType']) {
            setDateType(value['dateType']);
          }
          if (value['fromDate']) {
            setFromDateSearch(moment(value['fromDate']).format('DD/MM/YYYY'));
          }
          if (value['toDate']) {
            setToDateSearch(moment(value['toDate']).format('DD/MM/YYYY'));
          }

          if (value['infoType']) {
            setInfoType(value['infoType']);
          }
          if (infoType) {
            if (value['info']) {
              if(infoType === 'patientPhone') {
                if(value['info'].substring(0,1) === '0') {
                  setInfo(value['info'].slice(2));
                }
              } else {
                setInfo(value['info'].trim());
              }
            } else if (value['info'] === '') {
              setInfo('');
            }
          }
          if (value['appointmentType']) {
            setAppointmentTypeSearch({ ...appointmentTypeSearch, value: value['appointmentType'] });
          } else if (value['appointmentType'] === '') {
            setAppointmentTypeSearch({ ...appointmentTypeSearch, value: '' });
          }
          if (value['methodType']) {
            setMethodTypeSearch({ ...methodTypeSearch, value: value['methodType'] });
          } else if (value['methodType'] === '') {
            setMethodTypeSearch({ ...methodTypeSearch, value: '' });
          }
          if (value['status']) {
            setStatusSearch({ ...statusSearch, value: value['status'] });
          } else if (value['status'] === '') {
            setStatusSearch({ ...statusSearch, value: '' });
          }*/
        }}
      />
      <AppointmentTable
        functionOb={functionOb}
        onSearch={value => {
          setEmptySearch(Math.random());
          if (value['dateType']) {
            setDateType(value['dateType']);
          }
          if (value['fromDate']) {
            setFromDateSearch(moment(value['fromDate']).format('DD/MM/YYYY'));
          }
          if (value['toDate']) {
            setToDateSearch(moment(value['toDate']).format('DD/MM/YYYY'));
          }

          if (value['infoType']) {
            setInfoType(value['infoType']);
          }
          if (infoType) {
            //debugger
            if (value['info']) {
              if (infoType === 'patientPhone') {
                if (value['info'].slice(0, 1) === '0') {
                  setInfoSearch(value['info'].slice(1));
                } else {
                  setInfoSearch(value['info']);
                }
              } else {
                setInfoSearch(value['info'].trim());
              }
            } else {
              setInfoSearch('');
            }
          }
          if (value['appointmentType']) {
            setAppointmentTypeSearch({ ...appointmentTypeSearch, value: value['appointmentType'] });
          } else {
            setAppointmentTypeSearch({ ...appointmentTypeSearch, value: '' });
          }
          if (value['methodType']) {
            setMethodTypeSearch({ ...methodTypeSearch, value: value['methodType'] });
          } else {
            setMethodTypeSearch({ ...methodTypeSearch, value: '' });
          }
          if (value['status']) {
            setStatusSearch({ ...statusSearch, value: value['status'] });
          } else {
            setStatusSearch({ ...statusSearch, value: '' });
          }
          if (value['code']) {
            setChannelSearch({ ...channelSearch, value: value['code'] });
          } else {
            setChannelSearch({ ...channelSearch, value: '' });
          }
          if (value['examFacilityId']) {
            setExamFacilitySearch({ ...examFacilitySearch, value: value['examFacilityId'] });
          } else {
            setExamFacilitySearch({ ...examFacilitySearch, value: '' });
          }
        }}
        onCreate={() => setCreateVisible(true)}
        onModify={(appointmentPatient: AppointmentPatient, refetchAppointments) => {
          //setRefetchAppointments(refetchAppointments);
          setAppointmentPatient(appointmentPatient);
          setAssignStaffName(appointmentPatient?.assignStaffName);
          loadAppointmentDetailToConsultant({ variables: { id: appointmentPatient.appointmentId } });
          setAppointmentIdSelected(appointmentPatient.appointmentId);
          setModifyVisible(true);
        }}
        onBookingMultipleAppointments={appointmentPatient => {
          setBookingMultipleVisible(true);
          setAppointmentPatientSelected(appointmentPatient);
        }}
        onDelete={async id => {
          await handleDeleteAppointment(id);
        }}
        onCancle={async id => {
          await handleCancelConsultantAppointment(id);
        }}
        onAuthorize={values => {
          setValues(values);
          //setAuthorizeVisible(true);
        }}
        appointmentIdRes={appointmentIdRes}
        onResetAppointmentIdRes={onResetAppointmentIdRes}
        statusSearch={statusSearch}
        dateType={dateType}
        fromDateSearch={fromDateSearch}
        toDateSearch={toDateSearch}
        infoType={infoType}
        infoSearch={infoSearch}
        appointmentTypeSearch={appointmentTypeSearch}
        methodTypeSearch={methodTypeSearch}
        channelSearch={channelSearch}
        examFacilitySearch={examFacilitySearch}
        emptySearch={emptySearch}
        isSearch={true}
        formInstance={form}
        page={page}
        setPage={setPage}
        pageSize={pageSize}
        setPageSize={setPageSize}
        filtered={filtered}
        setFiltered={setFiltered}
        pagingAppointments={pagingAppointments}
        isLoadingAppointments={isLoadingAppointments}
        refetchAppointments={refetchAppointments}
        isLoadingAppointment={isLoadingAppointmentDetailToConsultant}
      />
      <AppointmentCreateDialog
        visible={mcreateVisible}
        onCreate={async values => {
          let isForwardValue = values?.isForward;
          setIsForward(isForwardValue === undefined ? false : isForwardValue);
          delete values?.isForward;
          const appointmentInput: Partial<AppointmentInput> = {
            ...values,
            indicatedDoctorId: JSON.parse(String(localStorage.getItem('user')))['id']
          };
          await handleCreateAppointment({ data: appointmentInput });
          //handleForwardAppointment()
          //console.log('resultSaveAppointment : ', resultSaveAppointment);
          //setCreateVisible(false);
        }}
        onCancel={() => setCreateVisible(false)}
        functionOb={functionOb}
        channels={channels}
      />
      <AppointmentModifyDialog
        isLoadingChannels={isLoadingChannels}
        appointmentDetailToConsultant={appointmentDetailToConsultant}
        rowObj={appointmentPatient}
        functionOb={functionOb}
        onForwardAppointment={(appointmentId: any) => handleForwardAppointment(appointmentId)}
        onCancelConsultantAppointment={(appointmentId: any) => handleCancelConsultantAppointment(appointmentId)}
        channels={channels}
        visible={modifyVisible}
        assignStaffName={assignStaffName}
        onModify={values => {
          const appointmentInput: Partial<AppointmentInput> = {
            ...values,
            indicatedDoctorId: JSON.parse(String(localStorage.getItem('user')))['id']
          };
          consultantAppointment({ data: appointmentInput });
          //setModifyVisible(false);
        }}
        onBookingMultipleAppointments={appointmentPatient => {
          setBookingMultipleVisible(true);
          setAppointmentPatientSelected(appointmentPatient);
        }}
        appointment={appointmentDetailToConsultant?.data}
        onCancel={() => setModifyVisible(false)}
        isLoading={isLoadingAppointmentDetailToConsultant}
      />
      <AppointmentBookingMultipleDialog
        functionOb={functionOb}
        visible={bookingMultipleVisible}
        appointmentPatient={appointmentPatientSelected}
        onBookingMutipleAppointment={(values: AppointmentInput) => {
          let fromAppointmentDate: any = values['fromAppointmentDate'];
          fromAppointmentDate = moment(fromAppointmentDate).format('DD/MM/YYYY');
          let toAppointmentDate: any = values['toAppointmentDate'];
          toAppointmentDate = moment(toAppointmentDate).format('DD/MM/YYYY');
          if (toAppointmentDate <= fromAppointmentDate) {
            openNotificationRight('Ngày kết thúc phải lớn ngày bắt đầu.');
            return;
          }
          /*let appointmentDate:any = values["appointmentDate"];
          appointmentDate = moment(appointmentDate).format('DD/MM/YYYY');
          delete values['__typename'];
          values['id'] = values['appointmentId'];
          delete values['appointmentId'];
          delete values['patientName'];
          delete values['patientAddress'];
          delete values['patientPhone'];
          delete values['workTimeName'];
          delete values['assignStaffName'];
          delete values['assignStaffDate'];
          delete values['channelName'];
          delete values['createDate'];
          delete values['statusName'];*/
          let id = values['appointmentId'];
          let amountDate = values['reExamDate'];
          let fromAppointmentDateSplit = String(fromAppointmentDate).split('/');
          let toAppointmentDateSplit = String(toAppointmentDate).split('/');
          let fromDateCaculate = moment([
            fromAppointmentDateSplit[2],
            fromAppointmentDateSplit[1],
            fromAppointmentDateSplit[0]
          ]);
          let toDateCaculate = moment([
            toAppointmentDateSplit[2],
            toAppointmentDateSplit[1],
            toAppointmentDateSplit[0]
          ]);
          if (toDateCaculate.diff(fromDateCaculate, 'days') < Number(amountDate)) {
            openNotificationRight('Cách ngày không hợp lệ, giá trị phải thoả mãn Từ ngày, Đến ngày');
            return;
          }
          saveMultiAppointment({ data: { id, fromAppointmentDate, toAppointmentDate, amountDate } });
          setBookingMultipleVisible(false);
        }}
        onCancel={() => setBookingMultipleVisible(false)}
      />
    </div>
  );
};

export default AppointmentPage;
