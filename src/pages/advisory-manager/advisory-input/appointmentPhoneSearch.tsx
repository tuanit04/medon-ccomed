import React, { useEffect, useState } from 'react';
import { Space, Table, Typography, Button, Input } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { Patient } from 'common/interfaces/patient.interface';
import { Appointment } from 'common/interfaces/appointment.interface';
import { CopyOutlined, SearchOutlined } from '@ant-design/icons';
import moment from 'moment';
import Modal from 'antd/lib/modal/Modal';
import { useGetPIDByPhone } from 'hooks/appointment/useGetPIDByPhone';
import { useGetAppointmentByPhone } from 'hooks/appointment/useGetAppointmentByPhone';

interface Props {
  phoneNumber?: string;
  onApply?: (value?: Partial<Patient>) => void;
}

const AppointmentPhoneSearch = ({ phoneNumber, onApply }: Props) => {
  const [phone, setPhone] = useState(phoneNumber);
  const [value, setValue] = useState<Partial<any>>();
  const [isConfirm, setIsConfirm] = useState(false);
  const { pIDByPhone, loadPIDByPhone, isLoadingPIDByPhone } = useGetPIDByPhone();
  const { loadAppointmentByPhone, appointmentByPhone, isLoadingAppointmentByPhone } = useGetAppointmentByPhone();

  useEffect(() => {
    onQuery(phoneNumber);
    setPhone(phoneNumber);
  }, [phoneNumber]);

  const onCopy = (appointment?: any) => {
    const birthDate = appointment?.patientBirthDate
      ? moment(appointment?.patientBirthDate, 'DD/MM/YYYY HH:mm:ss')
      : undefined;

    setValue({
      address: appointment?.patientAddress,
      name: appointment?.patientName,
      sex: appointment?.patientSex,
      birthDate,
      birthYear: appointment?.patientBirthYear ? moment(appointment?.patientBirthYear, 'YYYY') : undefined ?? birthDate,
      email: appointment?.patientEmail
    });
    setIsConfirm(true);
  };

  const onCopyPatient = (patient?: any) => {
    setValue({
      ...patient,
      birthDate: patient?.birthDate ? moment(patient?.birthDate, 'DD/MM/YYYY') : undefined,
      birthYear: patient?.birthYear ? moment(patient?.birthYear, 'YYYY') : undefined
    });
    setIsConfirm(true);
  };

  const patientColumn: ColumnsType<Patient> = [
    {
      title: 'Sử dụng',
      render: (value, record) => (
        <Button type="primary" onClick={() => onCopyPatient(record)}>
          <CopyOutlined />
        </Button>
      )
    },
    {
      title: 'PID',
      dataIndex: 'pid'
    },
    {
      title: 'Họ tên',
      dataIndex: 'name'
    },
    {
      title: 'Ngày sinh',
      dataIndex: 'birthDate',
      render: value => moment(value, 'DD/MM/YYYY HH:mm:ss').format('DD/MM')
    },
    {
      title: 'Năm sinh',
      dataIndex: 'birthDate',
      render: value => moment(value, 'DD/MM/YYYY HH:mm:ss').format('YYYY')
    },
    {
      title: 'Giới tính',
      dataIndex: 'sex',
      render: value => (value === 'MALE' ? 'Nam' : 'Nữ')
    },
    {
      title: 'SĐT',
      dataIndex: 'phone'
    },
    {
      title: 'CMT/CCCD',
      dataIndex: 'idNo'
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'address'
    }
  ];

  const appointmentColumn: ColumnsType<Appointment> = [
    {
      title: 'Sử dụng',
      render: (value, record) => (
        <Button type="primary" onClick={() => onCopy(record)}>
          <CopyOutlined />
        </Button>
      )
    },
    {
      title: 'Ngày khám',
      dataIndex: 'appointmentDate',
      render: value => moment(value, 'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY')
    },
    {
      title: 'Họ tên',
      dataIndex: 'patientName'
    },
    {
      title: 'Ngày sinh',
      dataIndex: 'patientBirthDate',
      render: value => value && moment(value, 'DD/MM/YYYY HH:mm:ss').format('DD/MM')
    },
    {
      title: 'Năm sinh',
      dataIndex: 'patientBirthYear',
      render: (value, record: any) =>
        value ?? (record.patientBirthDate && moment(record.patientBirthDate, 'DD/MM/YYYY HH:mm:ss').format('YYYY'))
    },
    {
      title: 'Giới tính',
      dataIndex: 'patientSex',
      render: value => (value ? (value === 'FEMALE' ? 'Nữ' : value === 'MALE' ? 'Nam' : 'Khác') : '')
    },
    {
      title: 'SĐT',
      dataIndex: 'patientPhone'
    },
    {
      title: 'CMT/CCCD',
      dataIndex: 'patientIdNo'
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'patientAddress'
    },
    {
      title: 'Thành phố',
      dataIndex: 'provinceName'
    },
    {
      title: 'Quận huyện',
      dataIndex: 'districtName'
    },
    {
      title: 'Email',
      dataIndex: 'patientEmail'
    },
    {
      title: 'Thẻ KH (PID)',
      dataIndex: 'pid'
    }
  ];

  const onQuery = (phoneNumber?: string) => {
    loadPIDByPhone({
      variables: {
        phone: phoneNumber
      }
    });
    loadAppointmentByPhone({
      variables: {
        page: 1,
        pageSize: 10,
        phone: phoneNumber?.slice(1)
      }
    });
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPhone(e.target.value);
  };

  return (
    <Space direction="vertical">
      <Space>
        <Input
          value={phone}
          onChange={onChange}
          placeholder="Gõ SĐT tìm kiếm thông tin"
          type="number"
          style={{ width: 210 }}
        />
        <Button onClick={() => onQuery(phone)}>
          <SearchOutlined />
          Tìm kiếm
        </Button>
      </Space>
      <div>
        <Typography.Title level={5}>Danh sách thẻ khách hàng (PID)</Typography.Title>
        <Table
          loading={isLoadingPIDByPhone}
          pagination={{
            current: pIDByPhone?.page,
            pageSize: pIDByPhone?.pageSize,
            total: pIDByPhone?.records
          }}
          columns={patientColumn}
          dataSource={pIDByPhone?.data}
        />
      </div>
      <div>
        <Typography.Title level={5}>Danh sách lịch hẹn KH đã đặt trước đó theo SĐT</Typography.Title>
        <Table
          loading={isLoadingAppointmentByPhone}
          columns={appointmentColumn}
          dataSource={appointmentByPhone?.data}
        />
      </div>
      <Modal
        title="Thông báo"
        visible={isConfirm}
        onOk={() => {
          onApply?.(value);
          setIsConfirm(false);
          setValue(undefined);
        }}
        onCancel={() => {
          setIsConfirm(false);
          setValue(undefined);
        }}
      >
        <p>Chọn sử dụng thông tin có thể mất thông tin KH mà bạn đã khai thác trước đó, bạn có muốn tiếp tục không?</p>
      </Modal>
    </Space>
  );
};

export default AppointmentPhoneSearch;
