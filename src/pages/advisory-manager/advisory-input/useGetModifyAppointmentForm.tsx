import React, { FC, useEffect, useRef, useState } from 'react';
import { Form, Input, Col, Row, Select, DatePicker, Button, Modal, Table, InputNumber, Image } from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import moment from 'moment';
import useGetMedicalExaminationTestForm from './useGetMedicalExaminationTestForm';
import useGetMedicalPackageForm from './useGetMedicalPackageForm';
import {
  PhoneOutlined,
  UserOutlined,
  ContainerOutlined,
  NodeIndexOutlined,
  SaveOutlined,
  SearchOutlined,
  ReloadOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined
} from '@ant-design/icons';
import { Ward } from 'common/interfaces/ward.interface';
import { District } from 'common/interfaces/district.interface';
import { Province } from 'common/interfaces/province.interface';
import { Patient } from 'common/interfaces/patient.interface';
import { Specialist } from 'common/interfaces/specialist.interface';
import { Street } from 'common/interfaces/street.interface';
import { Route } from 'common/interfaces/route.interface';
import { Facility } from 'common/interfaces/facility.interface';
import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import { useGetRoutesLazy } from 'hooks/route';
import useGetCollaboratorDoctorForm from './useGetCollaboratorDoctorForm';
import { AppointmentComment } from 'common/interfaces/appointmentComment.interface';
import { Channel } from 'common/interfaces/channel.interface';
import { WorkTime } from 'common/interfaces/WorkTime.interface';
import { Reason } from 'common/interfaces/reason.interface';
import { openNotificationRight } from 'utils/notification';
import { Appointment } from 'common/interfaces/appointment.interface';
import { ServiceView } from 'common/interfaces/service.interface';
import AppointmentPhoneSearch from './appointmentPhoneSearch';
import { Packages } from 'common/interfaces/packages.interface';
import { Doctor } from 'common/interfaces/doctor.interface';
import { Country } from 'common/interfaces/country.interface';
import { functionCodeConstants } from 'constants/functions';
import { VAppointmentDetailService } from 'common/interfaces/vAppointmentDetailService.interface';
const { Option } = Select;
const { TextArea } = Input;

const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
  xxl: 12
};

const wrapperColAllCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const wrapperColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 20,
  xl: 20,
  xxl: 20
};

const labelColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 4,
  xl: 4,
  xxl: 4
};

const labelColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperSDTInput: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 9,
  xl: 9,
  xxl: 9
};

const wrapperSDT: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 16,
  xl: 16,
  xxl: 16
};

interface AddScheduleFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?: boolean;
  /** Initial form data */
  appointmentInput?: Partial<Appointment>;
  isCreate?: boolean;
}

export default function useGetModifyAppointmentForm({
  required = false,
  responsive = false,
  name = 'form',
  appointmentInput
}: AddScheduleFormProps) {
  const [first, setFirst] = useState(false);
  const disRefC: any = useRef(null);
  let countMedicalExamination = 0;
  let countTest = 0;
  let examFacilityId = '';
  const [appointmentDate, setAppointmentDate] = useState('');
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<Partial<Appointment>>();
  const { ListServicesForm } = useGetMedicalExaminationTestForm({ name: 'serviceForm', responsive: true });
  const { ListPackageForm } = useGetMedicalPackageForm({ name: 'medicalPackageForm', responsive: true });
  const { ListCollaboratorDoctorForm } = useGetCollaboratorDoctorForm({
    name: 'collaboratorDoctorForm',
    responsive: true
  });

  const getMessage = (
    values: Partial<Appointment>,
    provinces?: Province[],
    districts?: District[],
    wards?: Ward[],
    appointmentHisId?: string,
    doctorCode?: string,
    workTimes?: WorkTime[]
  ) => {
    const provinceName = provinces?.find(val => val.id === values.patient?.provinceId)?.name;
    const districtName = districts?.find(val => val.id === values.patient?.districtId)?.name;
    const wardName = wards?.find(val => val.id === values.patient?.wardId)?.name;
    const workTimeName = workTimes?.find(val => val.id === values.workTimeId)?.name;

    return `==== Tin nhắn từ Tổng đài ====
    Bạn có lịch hẹn mới
    - Mã lịch hẹn: ${appointmentHisId ?? ''}
    - Thời gian: ${workTimeName ?? ''} - ${moment(values.appointmentDate).format('DD/MM/YYYY')}
    - Tên khách hàng: ${values.patient?.name ?? ''}
    - SĐT người đặt lịch: ${values.patient?.phone ?? ''}
    - Địa chỉ: ${[values.patient?.address, wardName, districtName, provinceName].filter(Boolean).join(', ')}
    - Chỉ định: ${values.services?.map(val => val.serviceName).join(', ') ?? ''}
    - Gói khám: ${values.packages?.name ?? ''}
    - Mã gen gói khám: ${values.packages?.genCode ?? ''}
    - Mã BS CTV: ${doctorCode ?? ''}
    - Mã BS tổng đài:
    - SĐT CB lấy mẫu:
    - Ghi chú: ${values.appointmentNote ?? ''}`;
  };

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const onValuesChange = (changedValues: any, values: Appointment) => {};

    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        // {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={appointmentInput}
        onValuesChange={onValuesChange}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Ma lich hen
  const AppointmentCode: FC = () => {
    const appointmentCode = (
      <Form.Item className="appoiment-cpn" name="id" label="Mã lịch hẹn">
        <Input disabled={true} placeholder="" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentCode}</Col> : appointmentCode;
  };
  //Trang thai lich
  const AppointmentStatus: FC = () => {
    const appointmentStatus = (
      <Form.Item
        className="appoiment-cpn"
        name="status"
        label="Trạng thái lịch"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select defaultValue={2} disabled placeholder="-- Chọn trạng thái --">
          <Option value={1}>Chờ xác nhận</Option>
          <Option value={2}>Đã xác nhận</Option>
          <Option value={3}>Chờ tư vấn</Option>
          <Option value={4}>Đã phân lịch</Option>
          <Option value={5}>Đã check</Option>
          <Option value={6}>Đã lấy mẫu</Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentStatus}</Col> : appointmentStatus;
  };

  //Don vi
  interface FacilityParentProps {
    facilityParentList: Facility[];
    isLoading: boolean;
    handleChangeFacilityParent: (parentFacilityId: string, appointmentDate: string) => void;
  }
  const FacilityParent: FC<FacilityParentProps> = ({ facilityParentList, isLoading, handleChangeFacilityParent }) => {
    const onChangeFacilityParent = (value, event) => {
      const parentFacilityId = event.value;
      handleChangeFacilityParent(parentFacilityId, appointmentDate);
    };
    const facilityParent = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Đơn vị thực hiện:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="examFacilityId"
            rules={[
              {
                required: true,
                message: 'Đơn vị không được để trống.'
              }
            ]}
          >
            <Select
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onSelect={(value, event) => onChangeFacilityParent(value, event)}
              placeholder="-- Chọn Đơn vị --"
            >
              {facilityParentList?.map(option => (
                <Select.Option key={option.facilityCode} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{facilityParent}</Col> : facilityParent;
  };
  //Nguon lich hen
  interface AppointmentSourceProps {
    channelList: Channel[];
    isLoading: boolean;
    onChangeAppointmentSource: (channelType: string) => void;
  }
  const AppointmentSource: FC<AppointmentSourceProps> = ({ channelList, isLoading, onChangeAppointmentSource }) => {
    const handleChangeAppointmentSource = (value, event) => {};
    const appointmentSource = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Nguồn lịch hẹn:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name="channelType"
            rules={[
              {
                required: true,
                message: 'Nguồn lịch hẹn không được để trống.'
              }
            ]}
          >
            <Select
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Nguồn lịch hẹn --"
              onSelect={(value, event) => {
                handleChangeAppointmentSource(value, event);
              }}
            >
              {channelList?.map(channel => (
                <Select.Option key={channel.id} value={channel.code}>
                  {channel.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentSource}</Col> : appointmentSource;
  };

  interface PhoneNumberProps {
    handleChangePatient: (values?: Partial<Patient>) => void;
  }
  //So dien thoai
  const PhoneNumber: FC<PhoneNumberProps> = ({ handleChangePatient }) => {
    const [isCalling, setIsCalling] = useState(false);
    const [isSearching, setIsSearching] = useState(false);

    const onApply = (values?: any) => {
      setIsSearching(false);
      formInstance.setFieldsValue({
        patient: {
          ...values
        }
      });
      handleChangePatient(values);
    };
    const phoneNumber = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span>Số điện thoại:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item>
            <Row>
              <Col {...wrapperSDTInput}>
                <Form.Item
                  name={['patient', 'phone']}
                  noStyle
                  required
                  rules={[
                    {
                      required: true,
                      pattern: new RegExp(/^\S+$/),
                      message: 'Số điện thoại không được để trống.'
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value) {
                          return Promise.reject(new Error(' '));
                        } else {
                          value = value.toLowerCase();
                          value = value.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
                          value = value.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
                          value = value.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
                          value = value.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
                          value = value.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
                          value = value.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
                          value = value.replace(/đ/g, 'd');
                          let result = value?.replace(
                            /^\s+|\s+|[a-zA-Z ]+|[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]+$/gm,
                            ''
                          );
                          if (!/^\d+$/.test(value)) {
                            //return Promise.reject(new Error('SĐT không được chứa ký tự chữ hoặc ký tự đặc biệt.'));
                            formInstance.setFieldsValue({
                              patient: {
                                phone: result
                              }
                            });
                            formInstance.validateFields([['patient', 'phone']]);
                          } else if (!/(0[1|3|2|4|6|5|7|8|9])+([0-9]{8})\b/g.test(value)) {
                            return Promise.reject(new Error('SĐT phải đúng định dạng theo nhà mạng và bắt đầu là 0.'));
                          } else {
                            formInstance.setFieldsValue({
                              patient: {
                                phone: result
                              }
                            });
                            return Promise.resolve();
                          }
                        }
                      }
                    })
                  ]}
                >
                  <Input
                    onKeyUp={e => {
                      if (e.key == 'Enter') {
                        setIsSearching(true);
                      }
                    }}
                    placeholder="Nhập số điện thoại"
                  />
                </Form.Item>
              </Col>
              <Col {...wrapperSDT} className="ant-space ml5-sdt">
                <Button
                  type="primary"
                  onClick={() => {
                    setIsSearching(true);
                  }}
                  icon={<SearchOutlined />}
                >
                  Tìm kiếm
                </Button>
                <Button
                  className="ml5-call"
                  type="primary"
                  onClick={() => {
                    setIsCalling(true);
                  }}
                  icon={<PhoneOutlined />}
                >
                  Gọi
                </Button>
              </Col>
            </Row>
            <Modal
              title="Đang thực hiện cuộc gọi ..."
              visible={isCalling}
              onOk={() => {}}
              onCancel={() => {
                setIsCalling(false);
              }}
            >
              <Image width={200} src={require('../../../assets/icons/calling.gif')} />
            </Modal>
            <Modal
              maskClosable={false}
              style={{ marginTop: 5 }}
              centered
              title="Danh sách thông tin khách hàng"
              visible={isSearching}
              onCancel={() => {
                setIsSearching(false);
              }}
              footer={null}
              width={1000}
            >
              <AppointmentPhoneSearch
                phoneNumber={formInstance.getFieldValue(['patient', 'phone'])}
                onApply={onApply}
              />
            </Modal>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{phoneNumber}</Col> : phoneNumber;
  };
  //Quốc tịch
  interface NationalityProps {
    countries: Country[];
    isLoading: boolean;
  }
  const Nationality: FC<NationalityProps> = ({ countries, isLoading }) => {
    const nationality = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Quốc tịch:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'nationality']}
            rules={[{ required: true, message: 'Quốc tịch không được để trống.' }]}
          >
            <Select
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Quốc tịch --"
            >
              {countries?.map(country => (
                <Select.Option key={country.id} value={country.countryCode}>
                  {country.countryName}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{nationality}</Col> : nationality;
  };

  //Loai khach hang
  const CustomerType: FC = () => {
    const customerType = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Loại khách hàng:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'type']}
            rules={[{ required: true, message: 'Loại khách hàng không được để trống.' }]}
          >
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Loại khách hàng --"
            >
              <Option value="PERSONAL">Cá nhân</Option>
              <Option value="CLINIC">Phòng khám/BS</Option>
              <Option value="VIP">VIP</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{customerType}</Col> : customerType;
  };

  //Ho ten KH
  const CustomerFullname: FC = () => {
    const customerFullname = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Họ tên KH:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={['patient', 'name']} rules={[{ required: true, message: 'Họ tên KH không được để trống.' }]}>
            <Input placeholder="Nhập họ tên khách hàng" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{customerFullname}</Col> : customerFullname;
  };

  //Ly do dat lich
  interface ReasonBookingProps {
    reasonList: Reason[];
    isLoading: boolean;
  }
  const ReasonBooking: FC<ReasonBookingProps> = ({ reasonList, isLoading }) => {
    const reasonBooking = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Loại đặt lịch:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="reasonId" rules={[{ required: true, message: 'Loại đặt lịch không được để trống.' }]}>
            <Select
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Loại đặt lịch --"
            >
              {reasonList?.map(reason => (
                <Select.Option key={reason.id} value={reason.id}>
                  {reason.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{reasonBooking}</Col> : reasonBooking;
  };

  //Ngay sinh
  const DateofBirth: FC = () => {
    const dateFormat = 'DD/MM/YYYY';

    const dofBirth = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Ngày sinh:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'birthDate']}
            rules={[{ required: false, message: 'Ngày sinh không được để trống.' }]}
          >
            <DatePicker
              placeholder="Chọn ngày sinh"
              onChange={(date, dateString) => {
                if (date === null) {
                  formInstance.setFieldsValue({
                    patient: {
                      birthYear: undefined
                    }
                  });
                } else {
                  formInstance.setFieldsValue({
                    patient: {
                      birthYear: moment(date, 'YYYY')
                    }
                  });
                }
              }}
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              format={dateFormat}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{dofBirth}</Col> : dofBirth;
  };

  //Năm sinh
  const BirthYear: FC = () => {
    const dateFormat = 'DD/MM/YYYY';
    const birthYear = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Năm sinh:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'birthYear']}
            rules={[{ required: false, message: 'Năm sinh không được để trống.' }]}
          >
            <DatePicker
              onChange={(date, dateString) => {
                if (date) {
                  formInstance.setFieldsValue({
                    patient: {
                      birthDate: moment('01/01/' + dateString, dateFormat)
                    }
                  });
                }
              }}
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              format="YYYY"
              placeholder="Chọn năm sinh"
              picker="year"
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{birthYear}</Col> : birthYear;
  };

  //Bac si CTV
  interface CollaboratorDoctorProps {
    doctor: Doctor;
  }
  const CollaboratorDoctor: FC<CollaboratorDoctorProps> = ({ doctor }) => {
    const [visibleModal, setVisibleModal] = useState(false);
    const indicatedDoctorName = formInstance.getFieldValue('indicatedDoctorName');
    const collaboratorDoctor = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Bác sĩ CTV:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="indicatedDoctorId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <div>
              <Button
                disabled
                style={{ width: '150px', textAlign: 'left' }}
                icon={<UserOutlined />}
                onClick={() => {
                  setVisibleModal(true);
                }}
              >
                Chọn Bác sĩ CTV
              </Button>
              {indicatedDoctorName ? (
                <span title={indicatedDoctorName} style={{ color: 'red', fontWeight: 'bold', marginLeft: '5px' }}>
                  {indicatedDoctorName}
                </span>
              ) : (
                <span title={doctor.name} style={{ color: 'red', fontWeight: 'bold', marginLeft: '5px' }}>
                  {doctor.name}
                </span>
              )}
            </div>
            <Modal
              bodyStyle={{ overflowY: 'scroll' }}
              title="Danh sách Bác sĩ cộng tác viên"
              style={{ borderRadius: 10, overflow: 'hidden', marginTop: 15 }}
              centered
              okText="Chọn"
              cancelText="Hủy"
              cancelButtonProps={{ style: { display: 'none' } }}
              maskClosable={false}
              visible={visibleModal}
              onOk={() => setVisibleModal(false)}
              onCancel={() => setVisibleModal(false)}
              className="modalAddSchedule"
              width={900}
            >
              <ListCollaboratorDoctorForm
                doctor={doctor}
                setCollaboratorDoctorSelectedArr={values => {
                  formInstance.setFieldsValue({
                    indicatedDoctorId: values?.[0]?.id,
                    indicatedDoctorName: values?.[0]?.name
                  });
                }}
              />
            </Modal>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{collaboratorDoctor}</Col> : collaboratorDoctor;
  };

  //Gioi tinh
  const Gender: FC = () => {
    const gender = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Giới tính:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={['patient', 'sex']} rules={[{ required: true, message: 'Giới tính không được để trống.' }]}>
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Giới tính --"
            >
              <Option value="MALE">Nam</Option>
              <Option value="FEMALE">Nữ</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{gender}</Col> : gender;
  };

  //Goi kham
  interface MedicalExaminationPackageProps {
    status?: number;
    packageObj: Packages;
    provinceId: string | undefined;
  }
  const MedicalExaminationPackage: FC<MedicalExaminationPackageProps> = ({ status, packageObj, provinceId }) => {
    const [visibleModal, setVisibleModal] = useState(false);
    const packageName = formInstance.getFieldValue('packages')?.name;
    countMedicalExamination = packageName != null ? 1 : 0;
    const methodType = formInstance.getFieldValue('methodType') ?? 'HOME';
    const salePriceField = methodType === 'HOME' ? 'homeSalePrice' : 'hosSalePrice';
    const medicalExaminationPackage = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Gói khám:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="packages"
            // rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Button
              style={{ width: '150px', textAlign: 'left' }}
              icon={<ContainerOutlined />}
              onClick={() => {
                if (provinceId === null) {
                  openNotificationRight('Không thể chọn Gói khám khi chưa chọn Tỉnh/TP.', 'warning');
                  return;
                }
                if (countTest && countTest !== 0) {
                  openNotificationRight('Không thể chọn Gói khám khi đã chọn Xét nghiệm.', 'warning');
                  return;
                }
                setVisibleModal(true);
              }}
            >
              Chọn Gói khám
            </Button>
            <span title={packageName} style={{ color: 'red', fontWeight: 'bold', marginLeft: '5px' }}>
              {packageName}
            </span>
            <Modal
              bodyStyle={{ overflowY: 'scroll' }}
              title="Danh sách gói khám"
              style={{ borderRadius: 10, overflow: 'hidden', marginTop: 15 }}
              centered
              okText="Chọn"
              cancelText="Hủy"
              maskClosable={false}
              visible={visibleModal}
              onOk={() => setVisibleModal(false)}
              onCancel={() => {
                setVisibleModal(false);
              }}
              cancelButtonProps={{ style: { display: 'none' } }}
              className="modalAddSchedule"
              width={1200}
            >
              <ListPackageForm
                status={status}
                methodType={methodType}
                provinceId={provinceId}
                packageObj={packageObj}
                setPackageSelectedArr={values => {
                  formInstance.setFieldsValue({
                    packages: values?.[0],
                    originalAmount: values?.[0]?.originPrice,
                    totalAmount: values?.[0]?.[salePriceField]
                  });
                }}
              />
            </Modal>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{medicalExaminationPackage}</Col> : medicalExaminationPackage;
  };

  //Dia chi lien he
  const ContactAddress: FC = () => {
    const contactAddress = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Địa chỉ liên hệ:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name={['patient', 'address']}
            rules={[{ required: true, max: 100, message: 'Địa chỉ liên hệ không hợp lệ hoặc quá dài.' }]}
          >
            <Input placeholder="Nhập Địa chỉ liên hệ" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{contactAddress}</Col> : contactAddress;
  };

  //Xet nghiem
  interface TestProps {
    status?: number;
    services?: Partial<VAppointmentDetailService>[];
    facilityList: Facility[];
    setServicesDataSelected?: (servicesDataSelected: any) => void;
  }
  const Test: FC<TestProps> = ({ status, services, setServicesDataSelected, facilityList }) => {
    const [visibleModal, setVisibleModal] = useState(false);
    const methodType = formInstance.getFieldValue('methodType') ?? 'HOME';
    const salePriceField = methodType === 'HOME' ? 'homeSalePrice' : 'hosSalePrice';
    const facilityField = methodType === 'HOME' ? 'examFacilityId' : 'examFacilityId';
    const facilityId = formInstance.getFieldValue(facilityField);
    countTest = formInstance.getFieldValue('services')?.length;
    const test = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Xét nghiệm:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="services"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Button
              style={{ width: '150px', textAlign: 'left' }}
              icon={<NodeIndexOutlined />}
              onClick={() => {
                if (countMedicalExamination !== 0) {
                  openNotificationRight('Không thể chọn Xét nghiệm khi đã chọn Gói khám.', 'warning');
                  return;
                }
                if (!formInstance.getFieldValue('examFacilityId')) {
                  console.log(
                    "formInstance.getFieldValue('examFacilityId') : ",
                    formInstance.getFieldValue('examFacilityId')
                  );
                  openNotificationRight('Vui lòng chọn đơn vị trước khi chọn xét nghiệm.', 'warning');
                  return;
                }
                setVisibleModal(true);
              }}
            >
              Chọn Xét nghiệm
            </Button>
            <span style={{ color: 'red', marginLeft: '5px', fontWeight: 'bold' }}>
              {' '}
              {countTest ? '(Đã chọn ' + countTest + ')' : ''}
            </span>
            <Modal
              bodyStyle={{ overflowY: 'scroll' }}
              title="Chỉ định xét nghiệm"
              style={{ borderRadius: 10, overflow: 'hidden', marginTop: 15 }}
              centered
              okText="Chọn"
              cancelText="Hủy"
              cancelButtonProps={{ style: { display: 'none' } }}
              maskClosable={false}
              visible={visibleModal}
              onOk={() => {
                setVisibleModal(false);
              }}
              onCancel={() => setVisibleModal(false)}
              className="modalAddSchedule"
              width={900}
            >
              <ListServicesForm
                status={status}
                facilityId={facilityId}
                methodType={methodType}
                facilities={facilityList}
                setServicesDataSelectedArr={(values: ServiceView[]) => {
                  setServicesDataSelected?.(values);
                  const originalAmount = values.reduce((acc, val) => acc + Number(val.originPrice), 0);
                  const totalAmount = values.reduce((acc, val) => acc + Number(val[salePriceField]), 0);
                  formInstance.setFieldsValue({
                    services: values?.map(val => ({
                      id: val.serviceId,
                      originalPrice: Number(val.originPrice),
                      salePrice: Number(val[salePriceField])
                    })),
                    originalAmount,
                    totalAmount
                  });
                }}
                serviceList={services}
              />
            </Modal>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{test}</Col> : test;
  };

  //Tinh/TP
  interface ProvincesProps {
    provinceList: Province[];
    isLoading: boolean;
    handleChangeProvince: (provinceId: string) => void;
  }
  const Provinces: FC<ProvincesProps> = ({ provinceList, isLoading, handleChangeProvince }) => {
    const onChange = (value: string) => {
      formInstance.setFieldsValue({
        patient: {
          districtId: undefined,
          wardId: undefined
        },
        examFacilityId: undefined,
        streetId: undefined
      });
      handleChangeProvince(value);
      setFirst(true);
    };

    const provinces = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Tỉnh/TP:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            name={['patient', 'provinceId']}
            rules={[
              {
                required: true,
                message: 'Tỉnh/TP không được để trống.'
              }
            ]}
          >
            <Select
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={onChange}
              placeholder="-- Chọn Tỉnh/TP --"
            >
              {provinceList?.map(province => (
                <Select.Option key={province.id} value={province.id}>
                  {province.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{provinces}</Col> : provinces;
  };

  //Quan/Huyen
  interface DistrictProps {
    districtList: District[];
    isLoading: boolean;
    handleChangeDistrict: (districtId: string) => void;
  }
  const Districts: FC<DistrictProps> = ({ districtList, isLoading, handleChangeDistrict }) => {
    const onChange = (value: string) => {
      formInstance.setFieldsValue({
        patient: {
          wardId: undefined
        }
      });
      handleChangeDistrict(value);
    };
    useEffect(() => {
      if (first) {
        //disRefC.current.focus();
      }
    }, [districtList]);
    const districts = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Quận/Huyện:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            name={['patient', 'districtId']}
            rules={[
              {
                required: true,
                message: 'Quận/Huyện không được để trống.'
              }
            ]}
          >
            <Select
              ref={disRefC}
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={onChange}
              placeholder="-- Chọn Quận/Huyện --"
            >
              {districtList?.map(district => (
                <Select.Option key={district.id} value={district.id}>
                  {district.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{districts}</Col> : districts;
  };

  //Phuong/Xa
  interface WardProps {
    wardList: Ward[];
    isLoading: boolean;
    handleChangeWard: (wardId: string) => void;
  }
  const Wards: FC<WardProps> = ({ wardList, isLoading, handleChangeWard }) => {
    const wards = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Phường/Xã:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'wardId']}
            rules={[
              {
                required: false,
                message: 'Phường/Xã không được để trống.'
              }
            ]}
          >
            <Select
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={handleChangeWard}
              placeholder="-- Chọn Phường/Xã --"
            >
              {wardList?.map(ward => (
                <Select.Option key={ward.id} value={ward.id}>
                  {ward.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{wards}</Col> : wards;
  };

  //Duong pho
  interface StreetProps {
    streetList: Street[];
    handleChangeStreet: (streetId: string) => void;
  }
  const StreetCpn: FC<StreetProps> = ({ streetList, handleChangeStreet }) => {
    const street = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Đường phố:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="streetId" rules={[{ required: false, message: 'Đường phố không được bỏ trống.' }]}>
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={handleChangeStreet}
              placeholder="-- Chọn Đường phố --"
            >
              {streetList?.map(street => (
                <Select.Option key={street.id} value={street.id}>
                  {street.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{street}</Col> : street;
  };

  //Ghi chu XN
  const TestNotes: FC = () => {
    const testNotes = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12">
          Ghi chú:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="appointmentNote"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <TextArea rows={5} placeholder="Ghi chú gửi CB tại nhà" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{testNotes}</Col> : testNotes;
  };

  //Email
  const Email: FC = () => {
    const email = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Email:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={['patient', 'email']} rules={[{ required: false, message: 'Email không được để trống.' }]}>
            <Input placeholder="Nhập Email" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{email}</Col> : email;
  };

  // Tong tien
  interface TotalPriceProps {
    services: any;
  }
  const TotalPrice: FC<TotalPriceProps> = ({ services }) => {
    useEffect(() => {
      if (services) {
        const methodType = formInstance.getFieldValue('methodType') ?? 'HOME';
        const salePriceField = methodType === 'HOME' ? 'homeSalePrice' : 'hosSalePrice';
        const originalAmount = services.reduce((acc, val) => acc + Number(val.originalPrice), 0);
        const totalAmount = services.reduce((acc, val) => acc + Number(val.salePrice), 0);
      }
    }, [services]);
    const onChange = value => {};
    const totalPrice = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Tổng tiền:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="originalAmount">
            <InputNumber
              disabled={true}
              bordered={false}
              style={{ color: 'red', background: '#fbfbfb', width: '93%' }}
              defaultValue={0}
              formatter={value => `${value} VNĐ`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={(value: any) => value.replace(/\$\s?|(,*)/g, '')}
              onChange={onChange}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{totalPrice}</Col> : totalPrice;
  };

  //CMND/CCCD
  const IdentityCard: FC = () => {
    const identityCard = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          CMND/CCCD:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'idNo']}
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input placeholder="Nhập CMND/CCCD" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{identityCard}</Col> : identityCard;
  };

  //Tong tien giam
  const TotalPriceDiscount: FC = () => {
    const onChange = value => {};
    const totalPriceDiscount = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Tổng tiền giảm:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="totalAmount">
            <InputNumber
              disabled={true}
              bordered={false}
              style={{ color: 'red', background: '#fbfbfb', width: '93%' }}
              defaultValue={0}
              formatter={value => `${value} VNĐ`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={(value: any) => value.replace(/\$\s?|(,*)/g, '')}
              onChange={onChange}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{totalPriceDiscount}</Col> : totalPriceDiscount;
  };

  //Ma gen goi kham
  const GeneticCode: FC = () => {
    const geneticCode = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Mã gen gói khám:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name={['packages', 'genCode']}
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input readOnly={true} />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{geneticCode}</Col> : geneticCode;
  };

  //CB bao lich
  interface ScheduleReporterProps {
    siteStaffList: SiteStaff[];
    isLoading: boolean;
    handleChangeSiteStaff: (siteStaffId: string) => void;
  }
  const ScheduleReporter: FC<ScheduleReporterProps> = ({ siteStaffList, isLoading, handleChangeSiteStaff }) => {
    const scheduleReporter = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Cán bộ báo lịch:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="infoStaffId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Select
              loading={isLoading}
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Cán bộ báo lịch --"
            >
              {siteStaffList?.map(option => (
                <Select.Option key={option.id} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{scheduleReporter}</Col> : scheduleReporter;
  };

  //Mã thẻ KH PID
  interface PIDProps {
    patientList: Patient[];
  }
  const PID: FC<PIDProps> = ({ patientList }) => {
    const pId = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Mã thẻ KH (PID):
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'pid']}
            rules={[
              {
                required: false,
                message: 'Mã thẻ KH (PID) không được để trống.'
              }
            ]}
          >
            <Input readOnly={true} placeholder="" />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{pId}</Col> : pId;
  };

  //Nơi khám
  interface MedicalPlaceProps {
    facilityList: Facility[];
    methodType: string;
    isLoading: boolean;
    handleChangeMedicalPlace: (examFacilityId: string, appointmentDate: string) => void;
  }
  const MedicalPlace: FC<MedicalPlaceProps> = ({ facilityList, isLoading, methodType, handleChangeMedicalPlace }) => {
    const onChange = (value: string) => {
      //let examFacilityIdValue = facilityList.find(val => val.id === value)?.id;
      //examFacilityId = examFacilityIdValue ? examFacilityIdValue : '';
      handleChangeMedicalPlace(value, appointmentDate);
    };
    const medicalPlace = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Nơi khám:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="examFacilityId"
            rules={[
              {
                required: methodType === 'HOSPITAL',
                message: 'Nơi khám không được để trống.'
              }
            ]}
          >
            <Select
              loading={isLoading}
              onChange={onChange}
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Nơi khám --"
            >
              {facilityList?.map(option => (
                <Select.Option key={option.id} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{medicalPlace}</Col> : medicalPlace;
  };

  //Đối tượng gửi
  interface ObjectSendProps {
    facilityList: Facility[];
    isLoading: boolean;
  }
  const ObjectSend: FC<ObjectSendProps> = ({ facilityList, isLoading }) => {
    const objectSend = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Đối tượng gửi:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name="sendFacilityId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Select
              showSearch
              loading={isLoading}
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Đối tượng gửi --"
            >
              {facilityList?.map(option => (
                <Select.Option key={option.id} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{objectSend}</Col> : objectSend;
  };

  //Chuyên khoa
  interface SpecialistsProps {
    specialists: Specialist[];
    isLoading: boolean;
  }
  const Specialists: FC<SpecialistsProps> = ({ specialists, isLoading }) => {
    const specialist = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Chuyên khoa:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name="specialistId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Select
              showSearch
              loading={isLoading}
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Chuyên khoa --"
            >
              {specialists?.map(option => (
                <Select.Option key={option.code} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{specialist}</Col> : specialist;
  };

  //Đối tượng
  interface AppointmentObjectProps {
    appointmentObjects: any;
    handleChange: (value) => void;
  }
  const AppointmentObject: FC<AppointmentObjectProps> = ({ appointmentObjects, handleChange }) => {
    const appointmentObject = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Đối tượng :
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="appointmentObject" rules={[{ required: true, message: 'Đối tượng không được để trống.' }]}>
            <Select
              onChange={e => {
                if (e === 'HEALTH_INSURANCE') {
                  handleChange(true);
                } else {
                  handleChange(false);
                }
              }}
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Đối tượng --"
            >
              <Select.Option value="SERVICE">Dịch vụ</Select.Option>
              <Select.Option value="HEALTH_INSURANCE">Bảo hiểm y tế</Select.Option>
              <Select.Option value="GUARANTEE_INSURANCE">Bảo hiểm bảo lãnh</Select.Option>
              <Select.Option value="APPRAISAL_INSURANCE">Thẩm định bảo hiểm</Select.Option>
              <Select.Option value="HEALTH_CERTIFICATE">Giấy chứng nhận sức khỏe</Select.Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentObject}</Col> : appointmentObject;
  };

  //Thẻ BHYT
  interface BHYTProps {
    isRequired: boolean;
  }
  const BHYT: FC<BHYTProps> = ({ isRequired }) => {
    const bhyt = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Thẻ BHYT:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="healthInsuranceCard" rules={[{ required: isRequired, message: 'Nhập số thẻ BHYT' }]}>
            <Input placeholder="Nhập số thẻ BHYT" />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{bhyt}</Col> : bhyt;
  };
  interface ReasonBookingStrProps {}
  const ReasonBookingStr: FC<ReasonBookingStrProps> = () => {
    const reasonBookingStr = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span>Lý do đặt lịch:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="reasonNote" rules={[{ required: true, message: 'Lý do đặt lịch không được bỏ trống.' }]}>
            <TextArea rows={3} placeholder="Nhập lý do đặt lịch" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{reasonBookingStr}</Col> : reasonBookingStr;
  };

  //Ngày đặt tại nhà
  interface AppointmentDateProps {
    onChange: (value: any, dateString: any) => void;
    isHospital: boolean;
    handleChangAppointmentDate: (routeId: string, appointmentDate: string) => void;
  }
  const AppointmentDate: FC<AppointmentDateProps> = ({ onChange, isHospital, handleChangAppointmentDate }) => {
    const disablePastDt = current => {
      const yesterday = moment().subtract(1, 'day');
      return current.isBefore(yesterday);
    };
    const handleDatePickerChange = (date, dateString, id) => {
      let dateStringSplit = dateString?.split('/');
      dateString = dateStringSplit[2] + '-' + dateStringSplit[1] + '-' + dateStringSplit[0];
      setAppointmentDate(dateString);
      if (isHospital) {
        handleChangAppointmentDate(examFacilityId, dateString);
      } else {
        handleChangAppointmentDate(formInstance.getFieldValue('routeId'), dateString);
      }
    };
    const appointmentDate = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Ngày đặt:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="appointmentDate" rules={[{ required: true, message: 'Ngày đặt không được để trống.' }]}>
            <DatePicker
              onChange={(date, dateString) => handleDatePickerChange(date, dateString, 1)}
              disabledDate={disablePastDt}
              format="DD/MM/YYYY"
              placeholder="-- Chọn ngày --"
            />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentDate}</Col> : appointmentDate;
  };

  //Khung giờ
  interface AppointmentWorkTimeProps {
    appointmentWorkTime: any;
    workTimeId?: string;
    isLoading: boolean;
  }
  const AppointmentWorkTime: FC<AppointmentWorkTimeProps> = ({ appointmentWorkTime, isLoading, workTimeId }) => {
    let appointmentWorkTimeArr: any = [];
    const isToday = (someDate: Date) => {
      const today = new Date();
      return (
        someDate.getDate() == today.getDate() &&
        someDate.getMonth() == today.getMonth() &&
        someDate.getFullYear() == today.getFullYear()
      );
    };

    const handleChangeWorkTime = (value: string) => {};
    const handleSelectWorkTime = (value, e) => {
      let appointmentDate = formInstance.getFieldValue('appointmentDate');
      if (!appointmentDate) {
        return;
      } else if (isToday(new Date(String(appointmentDate)))) {
        var today = new Date();
        var timeCurrent = today.getHours() + ':' + today.getMinutes() + ':00';
        const workTimeSelectedArr = e['key']?.split('-');
        const startTime = workTimeSelectedArr[0];
        const endTime = workTimeSelectedArr[1];
        //The 1st January is an arbitrary date, doesn't mean anything.
        if (Date.parse('01/01/2011 ' + timeCurrent) >= Date.parse('01/01/2011 ' + startTime + ':00')) {
          openNotificationRight(
            'Khung giờ vừa chọn là khung giờ đã chọn trước đó và đã trôi qua trong ngày. Vui lòng chọn khung giờ và ngày khác.',
            'warning'
          );
          formInstance.setFieldsValue({
            workTimeId: undefined
          });
          return;
        } else {
          //console.log('Thoả mãn')
        }
      }
    };
    let appointmentDate = formInstance.getFieldValue('appointmentDate');
    if (appointmentDate && isToday(new Date(String(appointmentDate)))) {
      for (let i = 0; i < appointmentWorkTime?.length; i++) {
        var today = new Date();
        var timeCurrent = today.getHours() + ':' + today.getMinutes() + ':00';
        const startTime = appointmentWorkTime[i]['startTime'];
        if (workTimeId && workTimeId === appointmentWorkTime[i]['id']) {
          appointmentWorkTimeArr.push(appointmentWorkTime[i]);
        } else {
          if (Date.parse('01/01/2011 ' + timeCurrent) >= Date.parse('01/01/2011 ' + startTime + ':00')) {
            //console.log("appointmentWorkTime[i] : ", appointmentWorkTime[i])
          } else {
            appointmentWorkTimeArr.push(appointmentWorkTime[i]);
          }
        }
      }
      if (appointmentWorkTimeArr.length === 0) {
        //console.log('Faile')
        /*formInstance.setFieldsValue({
          workTimeId: undefined,
        });*/
      }
    } else if (appointmentWorkTime) {
      appointmentWorkTimeArr.push(...appointmentWorkTime);
    }
    const appointmentWorkTimeCpn = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Khung giờ:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="workTimeId" rules={[{ required: true, message: 'Khung giờ không hợp lệ.' }]}>
            <Select
              loading={isLoading}
              style={{ width: '100%' }}
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn khung giờ --"
            >
              {appointmentWorkTimeArr === undefined
                ? []
                : appointmentWorkTimeArr?.map((option, index) => (
                    <Select.Option key={option['startTime'] + '-' + option['endTime']} value={option['id']}>
                      {option['name']}
                    </Select.Option>
                  ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentWorkTimeCpn}</Col> : appointmentWorkTimeCpn;
  };

  //Đường phố
  interface Street4HomeProps {
    streetList: Street[];
    isLoading: boolean;
    routeId: string | undefined;
    handleChangeStreet: (streetId?: string, routeId?: string) => void;
  }
  const Street4Home: FC<Street4HomeProps> = ({ streetList, isLoading, routeId, handleChangeStreet }) => {
    const onChangeStreet = (value, event) => {
      const arrSplit = value?.split('***');
      const routeId = arrSplit[1];
      handleChangeStreet(arrSplit[0], routeId);
      formInstance.setFieldsValue({
        routeId
      });
    };

    const street = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Đường phố:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="streetId" rules={[{ required: true, message: 'Đường phố không được bỏ trống' }]}>
            <Select
              showSearch
              loading={isLoading}
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onSelect={(value, event) => onChangeStreet(value, event)}
              placeholder="-- Chọn Đường phố --"
            >
              {streetList?.map(street => (
                <Select.Option key={street.id} value={street?.id + '***' + street?.routeId}>
                  {street.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{street}</Col> : street;
  };

  //Cung đường
  interface RouteProps {
    routesData?: Route[];
    routeId?: string;
    street: string;
  }
  const Route: FC<RouteProps> = ({ routesData, routeId, street }) => {
    const route = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Cung đường:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}>
            <Input placeholder="---Chọn đường phố---" readOnly={true} value={street ? routesData?.[0]?.name : ''} />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{route}</Col> : route;
  };

  //Cán bộ được phân lịch
  interface AssignStaffProps {}
  const AssignStaff: FC<AssignStaffProps> = () => {
    const assignStaff = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          CB được phân lịch:
        </Col>
        <Col {...wrapperColOneItem} className="over-hidden">
          <Form.Item
            name="assignStaffName"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input readOnly={true} />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{assignStaff}</Col> : assignStaff;
  };

  //Tin nhắn gửi CBTN
  interface MgsContentProps {
    provinces?: Province[];
    districts?: District[];
    wards?: Ward[];
    doctorCode?: string;
    workTimes?: WorkTime[];
    appointmentHisId?: string;
    onAssignAppointment: () => void;
    functionOb: any;
  }
  const MgsContent: FC<MgsContentProps> = ({
    provinces,
    districts,
    wards,
    workTimes,
    doctorCode,
    appointmentHisId,
    onAssignAppointment,
    functionOb
  }) => {
    const [message, setMessage] = useState(
      getMessage(formInstance.getFieldsValue(), provinces, districts, wards, appointmentHisId, doctorCode, workTimes)
    );

    const mgsContent = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12">
          Tin nhắn gửi CBTN:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            // name="mgsContent"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <TextArea readOnly rows={7} placeholder="Nhập nội dung tin nhắn gửi cán bộ tại nhà" value={message} />
            <Button
              style={{ marginTop: 10, marginRight: 10 }}
              type="primary"
              disabled={functionOb[functionCodeConstants.TD_LH_TNCN] ? false : true}
              onClick={() => {
                setMessage(
                  getMessage(
                    formInstance.getFieldsValue(),
                    provinces,
                    districts,
                    wards,
                    appointmentHisId,
                    doctorCode,
                    workTimes
                  )
                );
              }}
              icon={<ReloadOutlined />}
            >
              Cập nhật tin nhắn
            </Button>
            {/* <Button style={{ marginTop: 10 }} type="primary" onClick={onAssignAppointment} icon={<ScheduleOutlined />}>
              Phân lịch
            </Button> */}
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{mgsContent}</Col> : mgsContent;
  };

  //Ghi chu lich hen
  interface AppointmentNotesProps {
    onSaveComment: (appointmentComment: any) => void;
  }

  const AppointmentNotes: FC<AppointmentNotesProps> = ({ onSaveComment }) => {
    let notes = '';
    const onChange = event => {
      notes = event.target.value;
    };
    //const listData = [{} as { id: any; href: any; title: any; content: any }];
    const [listData, setListData] = useState([{} as AppointmentComment]);
    listData.shift();
    const appointmentNotes = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Ghi chú:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="appointmentNotes"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <TextArea rows={10} placeholder="Nhập ghi chú lich hẹn" allowClear onChange={onChange} />
            <Button
              style={{ marginTop: 10 }}
              type="primary"
              onClick={async () => {
                onSaveComment({ content: notes });
              }}
              icon={<SaveOutlined />}
            >
              Lưu ghi chú
            </Button>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{appointmentNotes}</Col> : appointmentNotes;
  };

  //Khung gio chi tiet lich hen
  interface AppointmentTimeFrameProps {
    appointmentWorkTimeData: WorkTime[];
  }
  const AppointmentTimeFrame: FC<AppointmentTimeFrameProps> = ({ appointmentWorkTimeData }) => {
    let workTimeArr: WorkTime[] = [];
    if (appointmentWorkTimeData && appointmentWorkTimeData.length !== 0) {
      for (let i = 0; i < appointmentWorkTimeData.length; i++) {
        let workTime: WorkTime = {} as WorkTime;
        workTime.name = appointmentWorkTimeData[i]['name'];
        workTime.id = appointmentWorkTimeData[i]['id'];
        if (appointmentWorkTimeData[i]['objectCountList'] !== null) {
          workTime.objectCountList = appointmentWorkTimeData[i]['objectCountList'];
          workTime.chuaThucHien = 0;
          workTime.daThucHien = 0;
          for (let j = 0; j < appointmentWorkTimeData[i]['objectCountList'].length; j++) {
            if (
              appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 1 ||
              appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 13
            ) {
              /*workTime.chuaThucHien = 0;
              workTime.daThucHien = 0;
              workTime.total = 0;*/
            } else {
              if (
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 2 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 4 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 5 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 6
              ) {
                workTime.chuaThucHien = appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  ? workTime.chuaThucHien + appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  : workTime.chuaThucHien;
              }
              if (
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 7 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 8 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 9 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 10 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 11
              ) {
                workTime.daThucHien = appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  ? workTime.daThucHien + appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  : workTime.daThucHien;
              }
            }
          }
          workTime.total = workTime.daThucHien + workTime.chuaThucHien;
          workTimeArr.push(workTime);
        } else {
          workTime.daThucHien = 0;
          workTime.chuaThucHien = 0;
          workTime.total = 0;
          workTimeArr.push(workTime);
        }
      }
    }
    const columns = [
      {
        title: 'Khung giờ',
        dataIndex: 'name',
        key: 'name',
        render: name => name,
        className: 'timeFame'
      },
      {
        title: 'Tổng lịch đã hẹn',
        dataIndex: 'total',
        key: 'total',
        className: 'timeFame'
      },
      {
        title: 'Lịch chưa thực hiện',
        dataIndex: 'chuaThucHien',
        key: 'chuaThucHien',
        className: 'timeFame lichChuaThucHien'
      },
      {
        title: 'Lịch đã thực hiện',
        dataIndex: 'daThucHien',
        key: 'daThucHien',
        className: 'timeFame'
      }
    ];
    const appointmentTimeFrame = (
      <Form.Item>
        <Table
          rowClassName={(record, index) => (index % 2 === 0 ? 'table-row-light' : 'table-row-dark')}
          scroll={{ y: 300 }}
          style={{ height: 300, overflowX: 'hidden' }}
          pagination={false}
          columns={columns}
          dataSource={workTimeArr}
        />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentTimeFrame}</Col> : appointmentTimeFrame;
  };

  //Bang ghi chu
  interface AppointmentNotesTableProps {
    pagingAppointmentComment?: any;
    appointmentComments?: any;
    onDelete: (id: string) => void;
  }
  const AppointmentNotesTable: FC<AppointmentNotesTableProps> = ({
    pagingAppointmentComment,
    appointmentComments,
    onDelete
  }) => {
    const handleDeleteAppointmentComment = value => {
      onDelete(value);
    };
    const columns = [
      {
        title: 'Thời gian',
        dataIndex: 'createDate',
        key: 'createDate',
        className: 'appointment-notes'
      },
      {
        title: 'Người ghi chú',
        dataIndex: 'createUser',
        key: 'createUser',
        className: 'appointment-notes'
      },
      {
        title: 'Nội dung ghi chú',
        dataIndex: 'content',
        key: 'content'
      },
      {
        title: 'Xóa ghi chú',
        dataIndex: 'id',
        key: 'id',
        className: 'appointment-notes',
        render: id => (
          <Button
            type="primary"
            onClick={() => {
              Modal.confirm({
                icon: <ExclamationCircleOutlined />,
                title: 'Bạn có chắc chắn muốn xoá ghi chú đã chọn' + '' + ' ?',
                onOk() {
                  handleDeleteAppointmentComment(id);
                },
                onCancel() {
                  //console.log('Cancel');
                }
              });
            }}
            danger
            icon={<DeleteOutlined />}
          >
            Xoá
          </Button>
        )
      }
    ];
    const appointmentCommentTable = (
      <Form.Item>
        <Table scroll={{ y: 400 }} pagination={false} columns={columns} dataSource={appointmentComments?.data} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentCommentTable}</Col> : appointmentCommentTable;
  };

  return React.useMemo(
    () => ({
      form: formInstance,
      Form: WrappedForm,
      AppointmentCode,
      AppointmentStatus,
      AppointmentSource,
      PhoneNumber,
      CustomerType,
      CustomerFullname,
      ReasonBooking,
      DateofBirth,
      BirthYear,
      CollaboratorDoctor,
      Gender,
      Nationality,
      MedicalExaminationPackage,
      ContactAddress,
      Test,
      Provinces,
      Districts,
      Wards,
      Street4Home,
      TestNotes,
      Email,
      TotalPrice,
      IdentityCard,
      TotalPriceDiscount,
      GeneticCode,
      ScheduleReporter,
      PID,
      MedicalPlace,
      ObjectSend,
      Specialists,
      AppointmentDate,
      AppointmentWorkTime,
      AppointmentTimeFrame,
      AppointmentNotes,
      AppointmentNotesTable,
      Route,
      MgsContent,
      StreetCpn,
      FacilityParent,
      AppointmentObject,
      BHYT,
      ReasonBookingStr,
      AssignStaff
    }),
    []
  );
}
