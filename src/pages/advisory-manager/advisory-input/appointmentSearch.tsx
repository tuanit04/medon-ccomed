import React, { FC, useEffect } from 'react';
import useGetAppointmentForm from './useGetAppointmentForm';
import { Channel } from 'common/interfaces/channel.interface';
import { useGetFacilitys } from 'hooks/facility';

interface AppointmentSearchProps {
  onSearch: ({}) => void;
  onSetFormInstance: (value) => void;
  functionOb: any;
  channels: Channel[];
}
const AppointmentSearch: FC<AppointmentSearchProps> = ({ onSearch, onSetFormInstance, functionOb, channels }) => {
  //Hook Lấy danh sách nơi khám tại nhà
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  const {
    Form,
    form,
    DateType,
    FromDate,
    ToDate,
    Info,
    InfoType,
    AppointmentType,
    MethodType,
    Status,
    AppointmentSource,
    Facility,
    Buttons
  } = useGetAppointmentForm({
    name: 'searchForm',
    responsive: true,
    functionOb: functionOb
  });
  useEffect(() => {
    loadFacilities({ variables: { id: 'status', value: '1', operation: '==' } });
    onSetFormInstance(form);
  }, []);
  return (
    <Form>
      <DateType
        onChange={value => {
          if (value === '') {
            form.resetFields();
          }
        }}
      />
      <FromDate />
      <ToDate />
      <Status />
      <InfoType />
      <Info />
      <AppointmentType />
      <MethodType />
      <AppointmentSource channelList={channels} />
      <Facility facilityList={facilities} />
      {/*<Buttons
        onSearch={() => {
          let status = form.getFieldValue('status');
          let dateType = form.getFieldValue('dateType');
          let fromDate = form.getFieldValue('fromDate');
          let toDate = form.getFieldValue('toDate');
          let infoType = form.getFieldValue('infoType');
          let info = form.getFieldValue('info');
          let appointmentType = form.getFieldValue('appointmentType');
          let methodType = form.getFieldValue('methodType');
          onSearch({ status, fromDate, toDate, dateType, infoType, info, appointmentType, methodType });
        }}
        resetFields={() => {
          form.resetFields();
          onSearch({
            status: '',
            dateType: '',
            fromDate: '',
            toDate: '',
            infoType: '',
            info: '',
            appointmentType: '',
            methodType: ''
          });
        }}
      />*/}
    </Form>
  );
};

export default AppointmentSearch;
