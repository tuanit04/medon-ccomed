import { Button, Col, Modal, PageHeader, Row, Spin, Tabs, Tag } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import './index.less';
import useGetAddAppointmentForm from './useGetAddAppointmentForm';
import {
  BankOutlined,
  ExclamationCircleOutlined,
  HighlightOutlined,
  HomeOutlined,
  SaveOutlined,
  StepForwardOutlined
} from '@ant-design/icons';
import { useDeleteAppointmentComments } from 'hooks/appointment/useDeleteAppointmentComments';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { useGetWards } from 'hooks/ward/useGetWard';
import { useGetPatients } from 'hooks/patient/useGetPatient';
import { useGetSpecialist } from 'hooks/specialist/useGetSpecialist';
import { useGetFacilitys } from 'hooks/facility';
import { useGetStreetsLazy } from 'hooks/street';
import { useGetSiteStaffsLazy } from 'hooks/site-staff';
import {
  useGetAppointmentComments,
  useGetAppointmentWorkTimeHome,
  useGetAppointmentWorkTimeHospital,
  useSaveAppointmentComments
} from 'hooks/appointment';
import { Patient } from 'common/interfaces/patient.interface';
import moment from 'moment';
import { AppointmentInput } from 'common/interfaces/input/appointmentInput.interfce';
import { useGetReasons } from 'hooks/reasons/useGetReasons';
import { useGetAllWorkTimeFacility } from 'hooks/worktimes/useGetAllWorkTimeFacility';
import { Channel } from 'common/interfaces/channel.interface';
import { useGetAllCountriesLazy } from 'hooks/appointment/useGetAllCountries';
import { functionCodeConstants } from 'constants/functions';
import { openNotificationRight } from 'utils/notification';
import { useGetRoutesLazy } from 'hooks/route';
const TabPane = Tabs.TabPane;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};
interface AppointmentCreateDialogProps {
  visible: boolean;
  onCreate: (values: Partial<AppointmentInput>) => void;
  onCancel: () => void;
  functionOb: any;
  channels: any;
}

const AppointmentCreateDialog: FC<AppointmentCreateDialogProps> = ({
  visible,
  onCreate,
  onCancel,
  functionOb,
  channels
}) => {
  const [isBHYT, setIsBHYT] = useState(false);
  //confirm Loading
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  //const [tabIndex, setTabIndex] = useState("1");
  const [methodType, setMethodType] = useState('HOME');
  // Nội dung Ghi chú
  const [comments, setComments] = useState([{} as { content: string }]);
  let tabIndex = '1';
  //Hook Lấy danh sách Mã thẻ KH (PID)
  const { patients, isLoadingPatients, loadPatients } = useGetPatients();
  // Hook Lấy danh sách Cán bộ báo lịch
  const { siteStaffs, isLoadingSiteStaffs, loadSiteStaffs } = useGetSiteStaffsLazy();
  //Hook Lấy danh sách Chuyên khoa khám
  const { specialists, isLoadingSpecialists, loadSpecialist } = useGetSpecialist();
  //Hook Lấy danh sách đường phố
  const { streets, isLoadingStreets, loadStreets } = useGetStreetsLazy();
  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //Hook Lấy danh sách Quận/Huyện
  const { districts, isLoadingDistricts, loadDistricts } = useGetDistricts();
  //Hook Lấy danh sách Xã/Phường
  const { wards, isLoadingWards, loadWards } = useGetWards();
  //Hook Lấy danh sách nơi khám tại viện
  const {
    facilities: facilitiesHos,
    isLoadingFacilities: isLoadingFacilitiesHos,
    loadFacilities: loadFacilitiesHos
  } = useGetFacilitys();
  //Hook Lấy danh sách đơn vị
  const { facilities, loadFacilities, isLoadingFacilities } = useGetFacilitys();
  //Hook Lấy danh sách nơi khám tại nhà
  const {
    facilities: facilitiesHome,
    isLoadingFacilities: isLoadingFacilitiesHome,
    loadFacilities: loadFacilitiesHome
  } = useGetFacilitys();
  //Hook Lấy khung giờ theo đơn vị
  const { allWorkTimeFacility, loadAllWorkTimeFacility, isLoadingAllWorkTimeFacility } = useGetAllWorkTimeFacility();
  //Hook Lấy danh lý do
  const { reasons, isLoadingReasons, loadReasons } = useGetReasons();
  //Hook Lấy danh sách quốc tịch
  const { pagingCountries, isLoadingCountries, loadCountries, errorCountries } = useGetAllCountriesLazy();
  //Hook Lấy danh sách khung giờ tại nhà
  const {
    appointmentWorkTimeHome,
    isLoadingAppointmentWorkTimeHome,
    loadAppointmentWorkTimeHome
  } = useGetAppointmentWorkTimeHome();
  //Hook Lấy danh sách khung giờ tại viện
  const {
    appointmentWorkTimeHospital,
    isLoadingAppointmentWorkTimeHospital,
    loadAppointmentWorkTimeHospital
  } = useGetAppointmentWorkTimeHospital();
  //Hook lấy danh sách Ghi chú
  const { appointmentComments, isLoadingAppointmentComments, loadAppointmentComments } = useGetAppointmentComments();
  //Hook xoá ghi chú
  const {
    deleteAppointmentComments,
    resultDeleteAppointmentComments,
    errorDeleteAppointmentComments,
    isLoadingDeleteAppointmentComments
  } = useDeleteAppointmentComments();
  //Hook lưu ghi chú
  const {
    saveAppointmentComments,
    isLoadingSaveAppointmentComments,
    resultSaveAppointmentComments,
    errorSaveAppointmentComments
  } = useSaveAppointmentComments();
  //Hook lấy chi tiết cung đường
  const { loadRoutes, isLoadingRoutes, routesData } = useGetRoutesLazy();
  useEffect(() => {
    if (routesData) {
      form.setFieldsValue({
        routeId: routesData[0]?.name
      });
    }
  }, [routesData]);

  useEffect(() => {
    form.resetFields();
    setComments([]);
  }, [visible]);
  useEffect(() => {
    loadProvinces();
    loadPatients({
      variables: {
        page: 1
      }
    });
    loadSpecialist({
      variables: {
        pageSize: -1
      }
    });
    loadSiteStaffs({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
    loadAppointmentComments({
      variables: {
        page: 1
      }
    });
    loadReasons();
    loadCountries();
    loadFacilities({
      variables: {
        data: {
          page: 1
        }
      }
    });
  }, []);
  const {
    Form,
    form,
    AppointmentCode,
    AppointmentStatus,
    AppointmentSource,
    PhoneNumber,
    CustomerType,
    CustomerFullname,
    ReasonBooking,
    DateofBirth,
    BirthYear,
    Nationality,
    CollaboratorDoctor,
    Gender,
    MedicalExaminationPackage,
    ContactAddress,
    Test,
    Provinces,
    Districts,
    Wards,
    Street4Home,
    TestNotes,
    Email,
    TotalPrice,
    IdentityCard,
    TotalPriceDiscount,
    GeneticCode,
    ScheduleReporter,
    PID,
    MedicalPlace,
    ObjectSend,
    Specialists,
    AppointmentDate,
    AppointmentWorkTime,
    AppointmentObject,
    AppointmentTimeFrame,
    AppointmentNotes,
    AppointmentNotesTable,
    MgsContent,
    Route,
    AssignStaff,
    StreetCpn,
    FacilityParent,
    BHYT,
    ReasonBookingStr
  } = useGetAddAppointmentForm({
    name: 'add-appointment-form',
    responsive: true,
    appointmentInput: { patient: { nationality: 'VN' } }
  });
  //const [servicesDataSelected, setServicesDataSelected] = useState([]);
  const onSubmit = async (isForward?) => {
    const values = await form.validateFields();
    let channelType = values['channelType'];
    let channel: Channel = channels.find(function(element: Channel) {
      return element.code === channelType;
    });
    delete values['pId'];
    delete values['parentFacilityId'];
    let streetId = values['streetId']?.split('***')[0];
    let routeId = values['streetId']?.split('***')[1];
    let examFacilityId = values['examFacilityId']?.split('***')[0];
    let examFacilityCode = values['examFacilityId']?.split('***')[1];
    onCreate({
      ...values,
      isForward,
      methodType,
      comments: comments,
      streetId,
      routeId,
      channelId: channel?.id,
      channelType,
      appointmentDate: (values.appointmentDate as moment.Moment)?.format('DD/MM/YYYY'),
      services: values.services?.map(val => ({
        objectId: val.id,
        originalPrice: val.originalPrice,
        salePrice: val.salePrice
        /*indicatedDoctorId: JSON.parse(String(localStorage.getItem('user')))
            ? JSON.parse(String(localStorage.getItem('user')))['id']
            : ''*/
      })),
      packages: values.packages
        ? {
            objectId: values.packages?.id,
            originalPrice: form.getFieldValue('originalAmount'),
            salePrice: form.getFieldValue('totalAmount')
          }
        : undefined,
      patient: {
        ...values.patient,
        birthDate: (values.patient?.birthDate as moment.Moment)?.format('DD/MM/YYYY'),
        birthYear: values.patient?.birthYear
          ? (values.patient?.birthYear as moment.Moment)?.format('YYYY')
          : (values.patient?.birthDate as moment.Moment)?.format('YYYY'),
        provinceId: values.patient?.provinceId?.split('***')[0],
        provinceCode: values.patient?.provinceId?.split('***')[1],
        districtId: values.patient?.districtId?.split('***')[0],
        districtCode: values.patient?.districtId?.split('***')[1]
      },
      appointmentType: values.packages?.id ? 'PACKAGE' : 'SCHEDULE',
      originalAmount: form.getFieldValue('originalAmount') ? Number(form.getFieldValue('originalAmount')) : undefined,
      totalAmount: form.getFieldValue('totalAmount') ? Number(form.getFieldValue('totalAmount')) : undefined,
      examFacilityId,
      examFacilityCode
    });
    setConfirmLoading(true);
    setTimeout(() => {
      setConfirmLoading(false);
    }, 2000);
  };

  const handleChangePatient = (values?: Partial<Patient>) => {
    if (values?.provinceId) {
      loadDistricts({
        variables: {
          provinceId: values.provinceId
        }
      });
    }
    if (values?.districtId) {
      loadWards({
        variables: {
          districtId: values.districtId
        }
      });
    }
  };

  const handleMenuClick = keyMenu => {
    keyMenu = String(keyMenu);
    if (keyMenu === '4') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn chuyển lịch hẹn đang thao tác' + '' + ' cho bác sĩ tư vấn ?',
        onOk() {
          if ('') {
            openNotificationRight('Không thể đặt nhiều lịch đối với bản ghi đã huỷ.', 'warning');
            return;
          }
          //handleForwardAppointment({ id: '' });
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    } else if (keyMenu === '5') {
      Modal.confirm({
        icon: <ExclamationCircleOutlined />,
        title: 'Bạn có chắc chắn muốn xóa lịch hẹn đã chọn' + '' + ' ?',
        onOk() {
          //handleDeleteAppointment(appointmentDetail.appointmentId);
        },
        onCancel() {
          //console.log('Cancel');
        }
      });
    }
    //setAppointmentDetail({} as AppointmentPatient);
  };

  return (
    <Modal
      maskClosable={false}
      style={{ marginTop: 5 }}
      centered
      width={1500}
      title="Tạo mới lịch hẹn"
      okText="Thêm mới"
      cancelText="Hủy"
      visible={visible}
      onOk={() => onSubmit(false)}
      confirmLoading={confirmLoading}
      onCancel={onCancel}
      okButtonProps={{ disabled: functionOb[functionCodeConstants.TD_LH_TLH] ? false : true }}
    >
      <Spin spinning={false} tip="Đang tải...">
        <PageHeader
          ghost={false}
          subTitle="Trạng thái"
          tags={<Tag color="#2db7f5">Đã xác nhận</Tag>}
          extra={[
            <Button
              disabled={functionOb[functionCodeConstants.TD_LH_CBS] ? false : true}
              onClick={() => {
                onSubmit(true);
                //handleMenuClick(4);
              }}
              icon={<StepForwardOutlined />}
              key="4"
              className="btn-green"
              type="primary"
            >
              Lưu &amp; Chuyển BS tư vấn
            </Button>,
            <Button
              onClick={() => {
                onSubmit(false);
                //handleMenuClick(4);
              }}
              icon={<SaveOutlined />}
              key="4"
              type="primary"
              disabled={functionOb[functionCodeConstants.TD_LH_TLH] ? false : true}
            >
              Thêm mới
            </Button>
          ]}
        ></PageHeader>
        <Form {...formItemLayout} className="ant-advanced-add-form">
          <Col className="row" span={12}>
            <Row>
              <PID patientList={patients} />
              <AppointmentSource onChangeAppointmentSource={value => {}} channelList={channels} />
              <PhoneNumber handleChangePatient={handleChangePatient} />
              <CustomerFullname />
              <Gender />
              <DateofBirth />
              <BirthYear />
              <ContactAddress />
              <Provinces
                handleChangeProvince={provinceId => {
                  if (provinceId) {
                    if (methodType === 'HOME') {
                      loadFacilitiesHome({
                        variables: {
                          page: 1,
                          filtered: [
                            {
                              id: 'isHome',
                              value: '1',
                              operation: '=='
                            },
                            {
                              id: 'provinceIds',
                              value: provinceId,
                              operation: '~'
                            },
                            {
                              id: 'status',
                              value: '1',
                              operation: '=='
                            }
                          ]
                        }
                      });
                    } else if (methodType === 'HOSPITAL') {
                      loadFacilitiesHos({
                        variables: {
                          page: 1,
                          filtered: [
                            {
                              id: 'isHospital',
                              value: '1',
                              operation: '=='
                            },
                            {
                              id: 'provinceIds',
                              value: provinceId,
                              operation: '~'
                            },
                            {
                              id: 'status',
                              value: '1',
                              operation: '=='
                            }
                          ]
                        }
                      });
                    }
                    loadDistricts({
                      variables: {
                        provinceId: provinceId
                      }
                    });
                    loadStreets({
                      variables: {
                        page: 1,
                        filtered: [
                          {
                            id: 'provinceId',
                            value: provinceId,
                            operation: '=='
                          },
                          {
                            id: 'status',
                            value: '1',
                            operation: '=='
                          }
                        ]
                      }
                    });
                  }
                }}
                provinceList={provinces}
              />
              <Districts
                handleChangeDistrict={districtId => {
                  if (districtId !== '') {
                    loadWards({
                      variables: {
                        districtId: districtId
                      }
                    });
                  }
                }}
                districtList={districts}
              />
              <Wards handleChangeWard={() => {}} wardList={wards} />
              <IdentityCard />
              <Email />
              <Nationality countries={pagingCountries?.data} />
            </Row>
          </Col>
          <Col className="gutter-row" span={12}>
            <Row>
              <CustomerType />
              <ReasonBooking reasonList={reasons} />
              <CollaboratorDoctor />
              <MedicalExaminationPackage provinceId={form.getFieldValue(['patient', 'provinceId'])} />
              <Test facilityList={methodType === 'HOSPITAL' ? facilitiesHos : facilitiesHome} />
              <TestNotes />
              <GeneticCode />
              <ScheduleReporter siteStaffList={siteStaffs} handleChangeSiteStaff={() => {}} />
              {/* <TotalPrice />
              <TotalPriceDiscount /> */}
            </Row>
          </Col>
          <Tabs
            defaultActiveKey={methodType}
            activeKey={methodType}
            type="card"
            onTabClick={activeKey => {
              //handleTabClick(tabIndex);
              //setTabIndexSelected(String(tabIndex));
              //console.log('tabIndexSelected : ', tabIndexSelected);
              setMethodType(activeKey);
              if (activeKey === 'HOME') {
                if (form.getFieldValue(['patient', 'provinceId'])) {
                  loadFacilitiesHome({
                    variables: {
                      page: 1,
                      filtered: [
                        {
                          id: 'isHome',
                          value: '1',
                          operation: '=='
                        },
                        {
                          id: 'provinceIds',
                          value: form.getFieldValue(['patient', 'provinceId']),
                          operation: '~'
                        },
                        {
                          id: 'status',
                          value: '1',
                          operation: '=='
                        }
                      ]
                    }
                  });
                }
              } else if (activeKey === 'HOSPITAL') {
                if (form.getFieldValue(['patient', 'provinceId'])) {
                  loadFacilitiesHos({
                    variables: {
                      page: 1,
                      filtered: [
                        {
                          id: 'isHospital',
                          value: '1',
                          operation: '=='
                        },
                        {
                          id: 'provinceIds',
                          value: form.getFieldValue(['patient', 'provinceId']),
                          operation: '~'
                        },
                        {
                          id: 'status',
                          value: '1',
                          operation: '=='
                        }
                      ]
                    }
                  });
                }
              }
              if (activeKey !== 'NOTE') {
                if (methodType !== 'NOTE') {
                  form.setFieldsValue({
                    examFacilityId: undefined,
                    sendFacilityId: undefined,
                    specialistId: undefined,
                    workTimeId: undefined,
                    appointmentDate: moment(),
                    streetId: undefined,
                    routeId: undefined,
                    healthInsuranceCard: undefined,
                    reasonNote: undefined,
                    appointmentObject: undefined
                  });
                }
                form.setFieldsValue({
                  methodType: activeKey
                });
              }
            }}
            destroyInactiveTabPane={true}
          >
            <TabPane
              tab={
                <span>
                  <HomeOutlined />
                  Đặt lịch tại nhà
                </span>
              }
              key="HOME"
            >
              <Row className="fix-row">
                <Col className="gutter-row" span={12} style={{ marginTop: 15 }}>
                  <Row>
                    <FacilityParent
                      facilityParentList={form.getFieldValue(['patient', 'provinceId']) ? facilitiesHome : []}
                      handleChangeFacilityParent={(parentFacilityId, appointmentDate) => {
                        if (parentFacilityId) {
                          loadAllWorkTimeFacility({
                            variables: {
                              facilityId: parentFacilityId
                            }
                          });
                        }
                        if (appointmentDate && parentFacilityId) {
                          //setAppointmentDate(appointmentDate);
                          loadAppointmentWorkTimeHome({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId: parentFacilityId
                              }
                            }
                          });
                        } else {
                          loadAppointmentWorkTimeHome({
                            variables: {
                              data: {
                                appointmentDate: moment().format('YYYY-MM-DD'),
                                examFacilityId: parentFacilityId
                              }
                            }
                          });
                        }
                      }}
                    />
                    <Street4Home
                      handleChangeStreet={(routeId, appointmentDate) => {
                        if (routeId !== '') {
                          loadRoutes({
                            variables: {
                              filtered: [
                                {
                                  id: 'id',
                                  value: routeId,
                                  operation: '=='
                                },
                                {
                                  id: 'status',
                                  value: '1',
                                  operation: '=='
                                }
                              ]
                            }
                          });
                        } else {
                          //
                        }
                      }}
                      streetList={form.getFieldValue(['patient', 'provinceId']) ? streets : []}
                    />
                    <Route routeNameDefault={form.getFieldValue('streetId') ? true : false} />
                    <AppointmentDate
                      isHospital={false}
                      onChange={(value: any, dateString: any) => {}}
                      handleChangAppointmentDate={(examFacilityId, appointmentDate) => {
                        if (appointmentDate && examFacilityId) {
                          loadAppointmentWorkTimeHome({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId
                              }
                            }
                          });
                        } else {
                          //
                        }
                      }}
                    />
                    <AppointmentWorkTime
                      appointmentWorkTime={form.getFieldValue('examFacilityId') ? allWorkTimeFacility : []}
                    />
                    <ReasonBookingStr />
                    <AssignStaff />
                    <MgsContent
                      provinces={provinces}
                      districts={districts}
                      wards={wards}
                      workTimes={appointmentWorkTimeHospital?.['data']}
                    />
                  </Row>
                </Col>
                <Col className="gutter-row" span={12}>
                  <AppointmentTimeFrame
                    appointmentWorkTimeData={
                      form.getFieldValue('examFacilityId') ? appointmentWorkTimeHome?.['data'] : []
                    }
                  />
                </Col>
              </Row>
            </TabPane>
            <TabPane
              tab={
                <span>
                  <BankOutlined />
                  Đặt lịch tại viện
                </span>
              }
              key="HOSPITAL"
            >
              <Row className="fix-row">
                <Col className="gutter-row" span={12} style={{ marginTop: 15 }}>
                  <Row>
                    <MedicalPlace
                      facilityList={form.getFieldValue(['patient', 'provinceId']) ? facilitiesHos : []}
                      methodType={methodType}
                      handleChangeMedicalPlace={(examFacilityId, appointmentDate) => {
                        if (examFacilityId) {
                          loadAllWorkTimeFacility({
                            variables: {
                              facilityId: examFacilityId
                            }
                          });
                        }
                        if (appointmentDate && examFacilityId) {
                          loadAppointmentWorkTimeHospital({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId
                              }
                            }
                          });
                        } else {
                          loadAppointmentWorkTimeHospital({
                            variables: {
                              data: {
                                appointmentDate: moment().format('YYYY-MM-DD'),
                                examFacilityId
                              }
                            }
                          });
                        }
                      }}
                    />
                    <ObjectSend facilityList={facilities} />
                    <Specialists specialists={specialists} />
                    <AppointmentDate
                      isHospital={true}
                      onChange={(value: any, dateString: any) => {}}
                      handleChangAppointmentDate={(examFacilityId, appointmentDate) => {
                        if (appointmentDate && examFacilityId) {
                          loadAppointmentWorkTimeHospital({
                            variables: {
                              data: {
                                appointmentDate,
                                examFacilityId
                              }
                            }
                          });
                        } else {
                          //openNotificationRight('')
                        }
                      }}
                    />
                    <AppointmentWorkTime appointmentWorkTime={allWorkTimeFacility} />
                    <AppointmentObject
                      handleChange={(value: boolean) => {
                        setIsBHYT(value);
                      }}
                      appointmentObjects={{}}
                    />
                    <BHYT isRequired={isBHYT} />
                    <ReasonBookingStr />
                  </Row>
                </Col>
                <Col className="gutter-row" span={12}>
                  <AppointmentTimeFrame
                    appointmentWorkTimeData={
                      form.getFieldValue('examFacilityId') ? appointmentWorkTimeHospital?.['data'] : []
                    }
                  />
                </Col>
              </Row>
            </TabPane>
            <TabPane
              tab={
                <span>
                  <HighlightOutlined />
                  Ghi chú lịch hẹn
                </span>
              }
              key="NOTE"
            >
              <Row className="fix-row">
                <Col className="gutter-row" span={12} style={{ marginTop: 15 }}>
                  <Row>
                    <AppointmentNotes
                      onSaveComment={value => {
                        setComments([...comments, value]);
                      }}
                    />
                  </Row>
                </Col>
                <Col className="gutter-row" span={12}>
                  <AppointmentNotesTable
                    comments={comments}
                    onDelete={index => {
                      comments.splice(index, 1);
                      setComments([...comments]);
                    }}
                  />
                </Col>
              </Row>
            </TabPane>
          </Tabs>
        </Form>
      </Spin>
    </Modal>
  );
};

export default AppointmentCreateDialog;
