import React, { FC, useEffect, useRef, useState } from 'react';
import {
  Form,
  Input,
  Col,
  Row,
  Select,
  DatePicker,
  Space,
  Button,
  Tabs,
  Modal,
  Table,
  InputNumber,
  Image,
  List
} from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import moment from 'moment';
import useGetMedicalExaminationTestForm from './useGetMedicalExaminationTestForm';
import useGetMedicalPackageForm from './useGetMedicalPackageForm';
import {
  PhoneOutlined,
  UserOutlined,
  ContainerOutlined,
  NodeIndexOutlined,
  SaveOutlined,
  ScheduleOutlined,
  SearchOutlined,
  ExclamationCircleOutlined,
  DeleteOutlined
} from '@ant-design/icons';
import { Ward } from 'common/interfaces/ward.interface';
import { District } from 'common/interfaces/district.interface';
import { Province } from 'common/interfaces/province.interface';
import { Patient } from 'common/interfaces/patient.interface';
import { Specialist } from 'common/interfaces/specialist.interface';
import { Street } from 'common/interfaces/street.interface';
import { Facility } from 'common/interfaces/facility.interface';
import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import { useGetRoutesLazy } from 'hooks/route';
import useGetCollaboratorDoctorForm from './useGetCollaboratorDoctorForm';
import { AppointmentComment } from 'common/interfaces/appointmentComment.interface';
import { Channel } from 'common/interfaces/channel.interface';
import { WorkTime } from 'common/interfaces/WorkTime.interface';
import { Reason } from 'common/interfaces/reason.interface';
import { openNotificationRight } from 'utils/notification';
import { ServiceView } from 'common/interfaces/service.interface';
import { Appointment } from 'common/interfaces/appointment.interface';
import AppointmentPhoneSearch from './appointmentPhoneSearch';
import { Country } from 'common/interfaces/country.interface';
const { Option } = Select;
const { TextArea } = Input;
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
  xxl: 12
};

const wrapperColAllCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const wrapperColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 20,
  xl: 20,
  xxl: 20
};

const labelColOneItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 4,
  xl: 4,
  xxl: 4
};

const labelColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperSDTInput: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 9,
  xl: 9,
  xxl: 9
};

const wrapperSDT: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperColTowItem: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 16,
  xl: 16,
  xxl: 16
};

const layout = {
  // labelCol: { span: 6 },
  // wrapperCol: { span: 18 }
};

interface AddScheduleFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?: boolean;
  /** Initial form data */
  appointmentInput?: Partial<Appointment>;
  isCreate?: boolean;
}

export default function useGetAddAppointmentForm({
  required = false,
  responsive = false,
  name = 'form',
  appointmentInput
}: AddScheduleFormProps) {
  const [first, setFirst] = useState(false);
  const disRefC: any = useRef(null);
  const quRFC: any = useRef(null);
  let countMedicalExamination = 0;
  let countTest = 0;
  const [routeId, setRouteId] = useState('');
  const [appointmentDate, setAppointmentDate] = useState('');
  let { routesData, isLoadingRoutes, loadRoutes } = useGetRoutesLazy();
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<Partial<Appointment>>();
  const { ListServicesForm } = useGetMedicalExaminationTestForm({ name: 'serviceForm', responsive: true });
  const { ListPackageForm } = useGetMedicalPackageForm({ name: 'medicalPackageForm', responsive: true });
  const { ListCollaboratorDoctorForm } = useGetCollaboratorDoctorForm({
    name: 'collaboratorDoctorForm',
    responsive: true
  });
  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const onValuesChange = (changedValues: any, values: Appointment) => {};

    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        // {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={{ ...appointmentInput, appointmentDate: moment() }}
        onValuesChange={onValuesChange}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Ma lich hen
  const AppointmentCode: FC = () => {
    const appointmentCode = (
      <>
        <Form.Item className="appoiment-cpn" name="appointmentScheduleCode" label="Mã lịch hẹn">
          {' '}
          <>
            <Input disabled={true} placeholder="" />
          </>
        </Form.Item>
      </>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentCode}</Col> : appointmentCode;
  };
  //Trang thai lich
  const AppointmentStatus: FC = () => {
    const appointmentStatus = (
      <Form.Item
        className="appoiment-cpn"
        name="appointmentScheduleCode"
        label="Trạng thái lịch"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select defaultValue="2" disabled placeholder="-- Chọn trạng thái --">
          <Option value="1">Chờ xác nhận</Option>
          <Option value="2">Đã xác nhận</Option>
          <Option value="3">Chờ tư vấn</Option>
          <Option value="4">Đã phân lịch</Option>
          <Option value="5">Đã check</Option>
          <Option value="6">Đã lấy mẫu</Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentStatus}</Col> : appointmentStatus;
  };
  //Nguon lich hen
  interface AppointmentSourceProps {
    channelList: Channel[];
    onChangeAppointmentSource: (channelType: string) => void;
  }
  const AppointmentSource: FC<AppointmentSourceProps> = ({ channelList, onChangeAppointmentSource }) => {
    const handleChangeAppointmentSource = (value, event) => {};
    const appointmentSource = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Nguồn lịch hẹn:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name="channelType"
            rules={[
              {
                required: true,
                message: 'Nguồn lịch hẹn không được để trống.'
              }
            ]}
          >
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Nguồn lịch hẹn --"
              onSelect={(value, event) => {
                handleChangeAppointmentSource(value, event);
              }}
            >
              {channelList?.map(channel => (
                <Select.Option key={channel.id} value={channel.code}>
                  {channel.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{appointmentSource}</Col> : appointmentSource;
  };
  //So dien thoai
  interface PhoneNumberProps {
    handleChangePatient: (values?: Partial<Patient>) => void;
  }
  const PhoneNumber: FC<PhoneNumberProps> = ({ handleChangePatient }) => {
    const [isCalling, setIsCalling] = useState(false);
    const [isSearching, setIsSearching] = useState(false);

    const onApply = (values?: Partial<Patient>) => {
      setIsSearching(false);
      formInstance.setFieldsValue({
        patient: {
          ...values,
          phone: values?.phone ? 0 + values.phone : undefined
        }
      });
      handleChangePatient(values);
    };

    const phoneNumber = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span>Số điện thoại:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item>
            {/* <Space direction="horizontal"> */}
            <Row>
              <Col {...wrapperSDTInput}>
                <Form.Item
                  name={['patient', 'phone']}
                  noStyle
                  required
                  rules={[
                    {
                      required: true,
                      pattern: new RegExp(/^\S+$/),
                      message: 'Số điện thoại không được để trống.'
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value) {
                          return Promise.reject(new Error(' '));
                        } else {
                          value = value.toLowerCase();
                          value = value.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
                          value = value.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
                          value = value.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
                          value = value.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
                          value = value.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
                          value = value.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
                          value = value.replace(/đ/g, 'd');
                          let result = value?.replace(
                            /^\s+|\s+|[a-zA-Z ]+|[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]+$/gm,
                            ''
                          );
                          if (!/^\d+$/.test(value)) {
                            //return Promise.reject(new Error('SĐT không được chứa ký tự chữ hoặc ký tự đặc biệt.'));
                            formInstance.setFieldsValue({
                              patient: {
                                phone: result
                              }
                            });
                            formInstance.validateFields([['patient', 'phone']]);
                          } else if (!/(0[1|3|2|4|6|5|7|8|9])+([0-9]{8})\b/g.test(value)) {
                            return Promise.reject(new Error('SĐT phải đúng định dạng theo nhà mạng và bắt đầu là 0.'));
                          } else {
                            formInstance.setFieldsValue({
                              patient: {
                                phone: result
                              }
                            });
                            return Promise.resolve();
                          }
                        }
                      }
                    })
                  ]}
                >
                  <Input
                    onKeyUp={e => {
                      if (e.key == 'Enter') {
                        setIsSearching(true);
                      }
                    }}
                    placeholder="Nhập số điện thoại"
                  />
                </Form.Item>
              </Col>
              <Col {...wrapperSDT} className="ant-space ml5-sdt">
                <Button
                  type="primary"
                  onClick={() => {
                    setIsSearching(true);
                  }}
                  icon={<SearchOutlined />}
                >
                  Tìm kiếm
                </Button>
                <Button
                  className="ml5-call"
                  type="primary"
                  onClick={() => {
                    setIsCalling(true);
                  }}
                  icon={<PhoneOutlined />}
                >
                  Gọi
                </Button>
              </Col>
            </Row>
            <Modal
              title="Đang thực hiện cuộc gọi ..."
              visible={isCalling}
              onOk={() => {}}
              onCancel={() => {
                setIsCalling(false);
              }}
            >
              <Image width={200} src={require('../../../assets/icons/calling.gif')} />
            </Modal>
            <Modal
              maskClosable={false}
              style={{ marginTop: 5 }}
              centered
              title="Danh sách thông tin khách hàng"
              visible={isSearching}
              onCancel={() => {
                setIsSearching(false);
              }}
              footer={null}
              width={1000}
            >
              <AppointmentPhoneSearch
                phoneNumber={formInstance.getFieldValue(['patient', 'phone'])}
                onApply={onApply}
              />
            </Modal>
            {/* </Space> */}
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{phoneNumber}</Col> : phoneNumber;
  };
  //Loai khach hang
  const CustomerType: FC = () => {
    const customerType = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Loại khách hàng:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'type']}
            rules={[{ required: true, message: 'Loại khách hàng không được để trống.' }]}
          >
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Loại khách hàng --"
            >
              <Option value="PERSONAL">Cá nhân</Option>
              <Option value="CLINIC">Phòng khám/BS</Option>
              <Option value="VIP">VIP</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{customerType}</Col> : customerType;
  };

  //Ho ten KH
  const CustomerFullname: FC = () => {
    const customerFullname = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Họ tên KH:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={['patient', 'name']} rules={[{ required: true, message: 'Họ tên KH không được để trống.' }]}>
            <Input placeholder="Nhập họ tên khách hàng" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{customerFullname}</Col> : customerFullname;
  };

  //Ly do dat lich
  interface ReasonBookingProps {
    reasonList: Reason[];
  }
  const ReasonBooking: FC<ReasonBookingProps> = ({ reasonList }) => {
    const reasonBooking = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Loại đặt lịch:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="reasonId" rules={[{ required: true, message: 'Loại đặt lịch không được để trống.' }]}>
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Loại đặt lịch --"
            >
              {reasonList?.map(reason => (
                <Select.Option key={reason.id} value={reason.id}>
                  {reason.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{reasonBooking}</Col> : reasonBooking;
  };
  //Ngay sinh
  const DateofBirth: FC = () => {
    const dateFormat = 'DD/MM/YYYY';

    const dofBirth = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Ngày sinh:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'birthDate']}
            rules={[{ required: true, message: 'Ngày sinh không được để trống.' }]}
          >
            <DatePicker
              onChange={(date, dateString) => {
                if (date === null) {
                  formInstance.setFieldsValue({
                    patient: {
                      birthYear: undefined
                    }
                  });
                } else {
                  formInstance.setFieldsValue({
                    patient: {
                      birthYear: moment(date, 'YYYY')
                    }
                  });
                }
              }}
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              placeholder="Chọn ngày sinh"
              format={dateFormat}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{dofBirth}</Col> : dofBirth;
  };

  //Năm sinh
  const BirthYear: FC = () => {
    const dateFormat = 'DD/MM/YYYY';
    const birthYear = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Năm sinh:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'birthYear']}
            rules={[{ required: false, message: 'Năm sinh không được để trống.' }]}
          >
            <DatePicker
              onChange={(date, dateString) => {
                if (date) {
                  formInstance.setFieldsValue({
                    patient: {
                      birthDate: moment('01/01/' + dateString, dateFormat)
                    }
                  });
                }
              }}
              disabledDate={current => {
                return current && current > moment().add(0, 'day');
              }}
              format="YYYY"
              placeholder="Chọn năm sinh"
              picker="year"
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{birthYear}</Col> : birthYear;
  };

  //Quốc tịch
  interface NationalityProps {
    countries: Country[];
  }
  const Nationality: FC<NationalityProps> = ({ countries }) => {
    const nationality = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Quốc tịch:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'nationality']}
            rules={[{ required: true, message: 'Quốc tịch không được để trống.' }]}
          >
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              placeholder="-- Chọn Quốc tịch --"
            >
              {countries?.map(country => (
                <Select.Option key={country.id} value={country.countryCode}>
                  {country.countryName}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{nationality}</Col> : nationality;
  };

  //Bac si CTV
  const CollaboratorDoctor: FC = () => {
    const [visibleModal, setVisibleModal] = useState(false);
    const indicatedDoctorName = formInstance.getFieldValue('indicatedDoctorName');
    const collaboratorDoctor = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Bác sĩ CTV:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="indicatedDoctorId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Button
              style={{ width: '150px' }}
              icon={<UserOutlined />}
              onClick={() => {
                setVisibleModal(true);
              }}
            >
              Chọn Bác sĩ CTV
            </Button>
            <span title={indicatedDoctorName} style={{ color: 'red', fontWeight: 'bold', marginLeft: '5px' }}>
              {indicatedDoctorName}
            </span>
            <Modal
              bodyStyle={{ overflowY: 'scroll' }}
              title="Danh sách Bác sĩ cộng tác viên"
              style={{ borderRadius: 10, overflow: 'hidden', marginTop: 15 }}
              centered
              maskClosable={false}
              visible={visibleModal}
              okText="Chọn"
              cancelText="Hủy"
              cancelButtonProps={{ style: { display: 'none' } }}
              onOk={() => setVisibleModal(false)}
              onCancel={() => setVisibleModal(false)}
              className="modalAddSchedule"
              width={900}
            >
              <ListCollaboratorDoctorForm
                setCollaboratorDoctorSelectedArr={values => {
                  formInstance.setFieldsValue({
                    indicatedDoctorId: values?.[0]?.id,
                    indicatedDoctorName: values?.[0]?.name
                  });
                }}
              />
            </Modal>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{collaboratorDoctor}</Col> : collaboratorDoctor;
  };
  //Gioi tinh
  const Gender: FC = () => {
    const gender = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Giới tính:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={['patient', 'sex']} rules={[{ required: true, message: 'Giới tính không được để trống.' }]}>
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Giới tính --"
            >
              <Option value="MALE">Nam</Option>
              <Option value="FEMALE">Nữ</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{gender}</Col> : gender;
  };
  //Goi kham
  interface MedicalExaminationPackageProps {
    provinceId: string | undefined;
  }
  const MedicalExaminationPackage: FC<MedicalExaminationPackageProps> = ({ provinceId }) => {
    const [visibleModal, setVisibleModal] = useState(false);
    const packageName = formInstance.getFieldValue('packages')?.name;
    countMedicalExamination = packageName != null ? 1 : 0;
    const methodType = formInstance.getFieldValue('methodType') ?? 'HOME';
    const salePriceField = methodType === 'HOME' ? 'homeSalePrice' : 'hosSalePrice';
    const medicalExaminationPackage = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Gói khám:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="packages"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Button
              style={{ width: '160px' }}
              icon={<ContainerOutlined />}
              onClick={() => {
                if (countTest && countTest !== 0) {
                  openNotificationRight('Không thể chọn Gói khám khi đã chọn Xét nghiệm.', 'warning');
                  return;
                }
                if (!provinceId) {
                  if (provinceId === undefined) {
                    openNotificationRight('Vui lòng chọn Tỉnh/TP để có danh sách gói khám.', 'warning');
                    return;
                  }
                }
                setVisibleModal(true);
              }}
            >
              Chọn Gói khám
            </Button>
            <span title={packageName} style={{ color: 'red', fontWeight: 'bold', marginLeft: '5px' }}>
              {packageName}
            </span>
            <Modal
              bodyStyle={{ overflowY: 'scroll' }}
              title="Danh sách gói khám"
              style={{ borderRadius: 10, overflow: 'hidden', marginTop: 15 }}
              centered
              okText="Chọn"
              cancelText="Hủy"
              cancelButtonProps={{ style: { display: 'none' } }}
              maskClosable={false}
              visible={visibleModal}
              onOk={() => setVisibleModal(false)}
              onCancel={() => setVisibleModal(false)}
              className="modalAddSchedule"
              width={1200}
            >
              <ListPackageForm
                methodType={methodType}
                provinceId={provinceId}
                setPackageSelectedArr={values => {
                  formInstance.setFieldsValue({
                    packages: values?.[0],
                    originalAmount: values?.[0]?.originPrice,
                    totalAmount: values?.[0]?.[salePriceField]
                  });
                }}
              />
            </Modal>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{medicalExaminationPackage}</Col> : medicalExaminationPackage;
  };
  //Dia chi lien he
  const ContactAddress: FC = () => {
    const contactAddress = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Địa chỉ liên hệ:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name={['patient', 'address']}
            rules={[{ required: true, max: 100, message: 'Địa chỉ liên hệ không hợp lệ hoặc quá dài.' }]}
          >
            <Input placeholder="Nhập Địa chỉ liên hệ" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{contactAddress}</Col> : contactAddress;
  };

  //Xet nghiem
  interface TestProps {
    setServicesDataSelected?: (servicesDataSelected: any) => void;
    facilityList: Facility[];
  }
  const Test: FC<TestProps> = ({ setServicesDataSelected, facilityList }) => {
    const [visibleModal, setVisibleModal] = useState(false);
    const [servicesDataSelected, setServicesSelected] = useState<ServiceView[]>([]);
    const methodType = formInstance.getFieldValue('methodType') ?? 'HOME';
    const salePriceField = methodType === 'HOME' ? 'homeSalePrice' : 'hosSalePrice';
    const facilityField = methodType === 'HOME' ? 'examFacilityId' : 'examFacilityId';
    const facilityId = formInstance.getFieldValue(facilityField);

    countTest = formInstance.getFieldValue('services')?.length;
    const test = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Xét nghiệm:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="services"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Button
              style={{ width: '150px', textAlign: 'left' }}
              icon={<NodeIndexOutlined />}
              onClick={() => {
                let examFacilityId = formInstance.getFieldValue('examFacilityId');
                if (countMedicalExamination !== 0) {
                  openNotificationRight('Không thể chọn Xét nghiệm khi đã chọn Gói khám.', 'warning');
                  return;
                }
                if (!examFacilityId) {
                  openNotificationRight('Vui lòng chọn đơn vị trước khi chọn xét nghiệm.', 'warning');
                  return;
                }
                setVisibleModal(true);
              }}
            >
              Chọn Xét nghiệm
            </Button>
            <span style={{ color: 'red', marginLeft: '5px', fontWeight: 'bold' }}>
              {' '}
              {countTest ? '(Đã chọn ' + countTest + ')' : ''}
            </span>
            <Modal
              bodyStyle={{ overflowY: 'scroll' }}
              title="Chỉ định xét nghiệm"
              style={{ borderRadius: 10, overflow: 'hidden', marginTop: 15 }}
              centered
              okText="Chọn"
              cancelText="Hủy"
              cancelButtonProps={{ style: { display: 'none' } }}
              maskClosable={false}
              visible={visibleModal}
              onOk={() => {
                setVisibleModal(false);
              }}
              onCancel={() => setVisibleModal(false)}
              className="modalAddSchedule"
              width={900}
            >
              <ListServicesForm
                facilities={facilityList}
                facilityId={facilityId}
                methodType={salePriceField}
                setServicesDataSelectedArr={(values: ServiceView[]) => {
                  setServicesSelected(values);
                  setServicesDataSelected?.(values);
                  const originalAmount = values.reduce((acc, val) => acc + Number(val.originPrice), 0);
                  const totalAmount = values.reduce((acc, val) => acc + Number(val[salePriceField]), 0);
                  formInstance.setFieldsValue({
                    services: values?.map(val => ({
                      id: val.serviceId,
                      originalPrice: Number(val.originPrice),
                      salePrice: Number(val[salePriceField])
                    })),
                    originalAmount,
                    totalAmount
                  });
                }}
              />
            </Modal>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{test}</Col> : test;
  };
  //Tinh/TP
  interface ProvincesProps {
    provinceList: Province[];
    handleChangeProvince: (provinceId: string) => void;
  }
  const Provinces: FC<ProvincesProps> = ({ provinceList, handleChangeProvince }) => {
    const onChange = (value: string) => {
      formInstance.setFieldsValue({
        patient: {
          districtId: undefined,
          wardId: undefined
        },
        examFacilityId: undefined,
        streetId: undefined,
        routeId: undefined
      });
      handleChangeProvince(value);
      setFirst(true);
    };

    const provinces = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Tỉnh/TP:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            name={['patient', 'provinceId']}
            rules={[
              {
                required: true,
                message: 'Tỉnh/TP không được để trống.'
              }
            ]}
          >
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={onChange}
              placeholder="-- Chọn Tỉnh/TP --"
            >
              {provinceList?.map(province => (
                <Select.Option key={province.id} value={province.id}>
                  {province.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{provinces}</Col> : provinces;
  };

  //Quan/Huyen
  interface DistrictProps {
    districtList: District[];
    handleChangeDistrict: (districtId: string) => void;
  }

  const Districts: FC<DistrictProps> = ({ districtList, handleChangeDistrict }) => {
    const onChange = (value: string) => {
      formInstance.setFieldsValue({
        patient: {
          wardId: undefined
        }
      });
      handleChangeDistrict(value);
    };
    useEffect(() => {
      if (first) {
        //disRefC.current.focus();
      }
    }, [districtList]);
    const districts = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Quận/Huyện:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required={true}
            name={['patient', 'districtId']}
            rules={[
              {
                required: true,
                message: 'Quận/Huyện không được để trống.'
              }
            ]}
          >
            <Select
              ref={disRefC}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={onChange}
              placeholder="-- Chọn Quận/Huyện --"
            >
              {districtList?.map(district => (
                <Select.Option key={district.id} value={district.id}>
                  {district.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{districts}</Col> : districts;
  };

  //Phuong/Xa
  interface WardProps {
    wardList: Ward[];
    handleChangeWard: (wardId: string) => void;
  }
  const Wards: FC<WardProps> = ({ wardList, handleChangeWard }) => {
    const wards = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Phường/Xã:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'wardId']}
            rules={[
              {
                required: false,
                message: 'Phường/Xã không được để trống.'
              }
            ]}
          >
            <Select
              ref={quRFC}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={handleChangeWard}
              placeholder="-- Chọn Phường/Xã --"
            >
              {wardList?.map(ward => (
                <Select.Option key={ward.id} value={ward.id}>
                  {ward.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{wards}</Col> : wards;
  };

  //Duong pho
  interface StreetProps {
    streetList: Street[];
    handleChangeStreet: (streetId: string) => void;
  }
  const StreetCpn: FC<StreetProps> = ({ streetList, handleChangeStreet }) => {
    const street = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Đường phố:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="streetId" rules={[{ required: false, message: 'Đường phố không được bỏ trống.' }]}>
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={handleChangeStreet}
              placeholder="-- Chọn Đường phố --"
            >
              {streetList?.map(street => (
                <Select.Option key={street.id} value={street.id}>
                  {street.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{street}</Col> : street;
  };

  //Ghi chu XN
  const TestNotes: FC = () => {
    const testNotes = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12">
          Ghi chú:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="appointmentNote"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <TextArea rows={5} placeholder="Nhập Ghi chú" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{testNotes}</Col> : testNotes;
  };

  //Email
  const Email: FC = () => {
    const email = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Email:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name={['patient', 'email']} rules={[{ required: false, message: 'Email không được để trống.' }]}>
            <Input placeholder="Nhập Email" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{email}</Col> : email;
  };

  //Tong tien
  const TotalPrice: FC = () => {
    const onChange = value => {
      console.log('changed', value);
    };
    const totalPrice = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Tổng tiền:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="originalAmount">
            <InputNumber
              disabled={true}
              bordered={false}
              style={{ color: 'red', background: '#fbfbfb', width: '93%' }}
              defaultValue={0}
              formatter={value => `${value} VNĐ`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={(value: any) => value.replace(/\$\s?|(,*)/g, '')}
              onChange={onChange}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{totalPrice}</Col> : totalPrice;
  };

  //CMND/CCCD
  const IdentityCard: FC = () => {
    const identityCard = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          CMND/CCCD:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'idNo']}
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input placeholder="Nhập CMND/CCCD" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{identityCard}</Col> : identityCard;
  };

  //Tong tien giam
  const TotalPriceDiscount: FC = () => {
    const onChange = value => {};
    const totalPriceDiscount = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Tổng tiền giảm:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="totalAmount">
            <InputNumber
              disabled={true}
              bordered={false}
              style={{ color: 'red', background: '#fbfbfb', width: '93%' }}
              defaultValue={0}
              formatter={value => `${value} VNĐ`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={(value: any) => value.replace(/\$\s?|(,*)/g, '')}
              onChange={onChange}
            />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{totalPriceDiscount}</Col> : totalPriceDiscount;
  };

  //Ma gen goi kham
  const GeneticCode: FC = () => {
    const geneticCode = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Mã gen gói khám:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="geneticCode"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input disabled />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{geneticCode}</Col> : geneticCode;
  };

  //CB bao lich
  interface ScheduleReporterProps {
    siteStaffList: SiteStaff[];
    handleChangeSiteStaff: (siteStaffId: string) => void;
  }
  const ScheduleReporter: FC<ScheduleReporterProps> = ({ siteStaffList, handleChangeSiteStaff }) => {
    const scheduleReporter = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Cán bộ báo lịch:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="infoStaffId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Cán bộ báo lịch --"
            >
              {siteStaffList?.map(option => (
                <Select.Option key={option.id} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperCol}>{scheduleReporter}</Col> : scheduleReporter;
  };
  //Mã thẻ KH PID
  interface PIDProps {
    patientList: Patient[];
  }
  const PID: FC<PIDProps> = ({ patientList }) => {
    const pId = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Mã thẻ KH (PID):
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name={['patient', 'pid']}
            rules={[
              {
                required: false,
                message: 'Mã thẻ KH (PID) không được để trống.'
              }
            ]}
          >
            <Input readOnly={true} placeholder="" />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{pId}</Col> : pId;
  };

  //Don vi
  interface FacilityParentProps {
    facilityParentList: Facility[];
    handleChangeFacilityParent: (parentFacilityId: string, appointmentDate: string) => void;
  }

  const FacilityParent: FC<FacilityParentProps> = ({ facilityParentList, handleChangeFacilityParent }) => {
    const onChangeFacilityParent = (value, event) => {
      const parentFacilityId = event.value;
      //setExamFacilityId(parentFacilityId);
      handleChangeFacilityParent(parentFacilityId, appointmentDate);
    };
    const facilityParent = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Đơn vị thực hiện:{' '}
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="examFacilityId"
            rules={[
              {
                required: true,
                message: 'Đơn vị không được để trống.'
              }
            ]}
          >
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onSelect={(value, event) => onChangeFacilityParent(value, event)}
              defaultValue=""
            >
              <Select.Option key="" value="">
                -- Chọn Đơn vị --
              </Select.Option>
              {facilityParentList?.map(option => (
                <Select.Option key={option.facilityCode} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{facilityParent}</Col> : facilityParent;
  };

  //Nơi khám
  interface MedicalPlaceProps {
    facilityList: Facility[];
    methodType: string;
    handleChangeMedicalPlace: (examFacilityId: string, appointmentDate: string) => void;
  }
  const MedicalPlace: FC<MedicalPlaceProps> = ({ facilityList, methodType, handleChangeMedicalPlace }) => {
    const onChange = (value: string) => {
      //setExamFacilityId(value);
      handleChangeMedicalPlace(value, appointmentDate);
    };
    const medicalPlace = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span> Nơi khám:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="examFacilityId"
            rules={[
              {
                required: methodType === 'HOSPITAL',
                message: 'Nơi khám không được để trống.'
              }
            ]}
          >
            <Select
              onChange={onChange}
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Nơi khám --"
            >
              {facilityList?.map(option => (
                <Select.Option key={option.id} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{medicalPlace}</Col> : medicalPlace;
  };

  //Đối tượng gửi
  interface ObjectSendProps {
    facilityList: Facility[];
  }
  const ObjectSend: FC<ObjectSendProps> = ({ facilityList }) => {
    const objectSend = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Đối tượng gửi:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="sendFacilityId" rules={[{ required, message: 'Đối tượng gửi không được để trống.' }]}>
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Đối tượng gửi --"
            >
              {facilityList?.map(option => (
                <Select.Option key={option.id} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{objectSend}</Col> : objectSend;
  };
  //Đối tượng
  interface AppointmentObjectProps {
    appointmentObjects: any;
    handleChange: (value) => void;
  }
  const AppointmentObject: FC<AppointmentObjectProps> = ({ appointmentObjects, handleChange }) => {
    const appointmentObject = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span>Đối tượng :
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="appointmentObject" rules={[{ required: true, message: 'Đối tượng không được để trống.' }]}>
            <Select
              showSearch
              onChange={e => {
                if (e === 'HEALTH_INSURANCE') {
                  handleChange(true);
                } else {
                  handleChange(false);
                }
              }}
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Đối tượng --"
            >
              <Select.Option value="SERVICE">Dịch vụ</Select.Option>
              <Select.Option value="HEALTH_INSURANCE">Bảo hiểm y tế</Select.Option>
              <Select.Option value="GUARANTEE_INSURANCE">Bảo hiểm bảo lãnh</Select.Option>
              <Select.Option value="APPRAISAL_INSURANCE">Thẩm định bảo hiểm</Select.Option>
              <Select.Option value="HEALTH_CERTIFICATE">Giấy chứng nhận sức khỏe</Select.Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentObject}</Col> : appointmentObject;
  };
  //Chuyên khoa:
  interface SpecialistsProps {
    specialists: Specialist[];
  }
  const Specialists: FC<SpecialistsProps> = ({ specialists }) => {
    const specialist = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Chuyên khoa:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            name="specialistId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Select
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn Chuyên khoa--"
            >
              {specialists?.map(option => (
                <Select.Option key={option.code} value={option.id}>
                  {option.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{specialist}</Col> : specialist;
  };

  //Thẻ BHYT
  interface BHYTProps {
    isRequired: boolean;
  }
  const BHYT: FC<BHYTProps> = ({ isRequired }) => {
    const bhyt = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Thẻ BHYT:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="healthInsuranceCard" rules={[{ required: isRequired, message: 'Nhập số thẻ BHYT' }]}>
            <Input placeholder="Nhập số thẻ BHYT" />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{bhyt}</Col> : bhyt;
  };
  interface ReasonBookingStrProps {}
  const ReasonBookingStr: FC<ReasonBookingStrProps> = () => {
    const reasonBookingStr = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12">
          <span className="red">* </span>Lý do đặt lịch:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item name="reasonNote" rules={[{ required: true, message: 'Lý do đặt lịch không được bỏ trống.' }]}>
            <TextArea rows={3} placeholder="Nhập lý do đặt lịch" />
          </Form.Item>
        </Col>
      </Row>
    );

    return responsive ? <Col {...wrapperColAllCol}>{reasonBookingStr}</Col> : reasonBookingStr;
  };
  //Ngày đặt tại nhà
  interface AppointmentDateProps {
    onChange: (value: any, dateString: any) => void;
    isHospital: boolean;
    handleChangAppointmentDate: (examFacilityId: string, appointmentDate: string) => void;
  }
  const AppointmentDate: FC<AppointmentDateProps> = ({ onChange, isHospital, handleChangAppointmentDate }) => {
    const disablePastDt = current => {
      const yesterday = moment().subtract(1, 'day');
      return current.isBefore(yesterday);
    };
    const handleDatePickerChange = (date, dateString: string, id) => {
      let dateStringSplit = dateString?.split('/');
      dateString = dateStringSplit[2] + '-' + dateStringSplit[1] + '-' + dateStringSplit[0];
      setAppointmentDate(dateString);
      let examFacilityId = formInstance.getFieldValue('examFacilityId');
      if (examFacilityId) {
        if (isHospital) {
          handleChangAppointmentDate(examFacilityId, dateString);
        } else {
          handleChangAppointmentDate(examFacilityId, dateString);
        }
      }
    };
    const appointmentDate = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Ngày đặt:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item
            required
            name="appointmentDate"
            rules={[{ required: true, message: 'Ngày đặt không được để trống.' }]}
          >
            <DatePicker
              onChange={(date, dateString) => handleDatePickerChange(date, dateString, 1)}
              disabledDate={disablePastDt}
              format="DD/MM/YYYY"
              placeholder="-- Chọn ngày --"
            />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentDate}</Col> : appointmentDate;
  };
  //Khung giờ
  interface AppointmentWorkTimeProps {
    appointmentWorkTime: any;
  }
  const AppointmentWorkTime: FC<AppointmentWorkTimeProps> = ({ appointmentWorkTime }) => {
    const isToday = (someDate: Date) => {
      const today = new Date();
      return (
        someDate.getDate() == today.getDate() &&
        someDate.getMonth() == today.getMonth() &&
        someDate.getFullYear() == today.getFullYear()
      );
    };

    const handleChangeWorkTime = (value: string) => {};
    const handleSelectWorkTime = (value, e) => {
      let appointmentDate = formInstance.getFieldValue('appointmentDate');
      if (!appointmentDate) {
        return;
      } else if (isToday(new Date(String(appointmentDate)))) {
        var today = new Date();
        var timeCurrent = today.getHours() + ':' + today.getMinutes() + ':00';
        const workTimeSelectedArr = e['key']?.split('-');
        if (workTimeSelectedArr) {
          const startTime = workTimeSelectedArr[0];
          const endTime = workTimeSelectedArr[1];
          //The 1st January is an arbitrary date, doesn't mean anything.
          if (Date.parse('01/01/2011 ' + timeCurrent) >= Date.parse('01/01/2011 ' + startTime + ':00')) {
            //openNotificationRight('Khung giờ vừa chọn đã trôi qua hoặc đang nằm trong thời gian hiện tại, vui lòng chọn đúng khung giờ.');
            formInstance.setFieldsValue({
              workTimeId: undefined
            });
            return false;
          } else {
            return true;
          }
        }
      }
    };
    let appointmentDate = formInstance.getFieldValue('appointmentDate');
    let appointmentWorkTimeArr: any = [];
    if (appointmentDate && isToday(new Date(String(appointmentDate)))) {
      for (let i = 0; i < appointmentWorkTime?.length; i++) {
        var today = new Date();
        var timeCurrent = today.getHours() + ':' + today.getMinutes() + ':00';
        const startTime = appointmentWorkTime[i]['startTime'];
        const endTime = appointmentWorkTime[i]['endTime'];
        if (Date.parse('01/01/2011 ' + timeCurrent) >= Date.parse('01/01/2011 ' + startTime + ':00')) {
        } else {
          appointmentWorkTimeArr.push(appointmentWorkTime[i]);
        }
      }
    } else if (appointmentWorkTime) {
      appointmentWorkTimeArr.push(...appointmentWorkTime);
    }
    const appointmentWorkTimeCpn = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Khung giờ:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="workTimeId" rules={[{ required: true, message: 'Khung giờ không hợp lệ.' }]}>
            <Select
              style={{ width: '100%' }}
              showSearch
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              placeholder="-- Chọn khung giờ --"
            >
              {appointmentWorkTimeArr === undefined
                ? []
                : appointmentWorkTimeArr?.map((option, index) => (
                    <Select.Option key={option['startTime'] + '-' + option['endTime']} value={option['id']}>
                      {option['name']}
                    </Select.Option>
                  ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentWorkTimeCpn}</Col> : appointmentWorkTimeCpn;
  };
  //Đường phố
  interface Street4HomeProps {
    streetList: Street[];
    handleChangeStreet: (routeId: string, appointmentDate: string) => void;
  }
  const Street4Home: FC<Street4HomeProps> = ({ streetList, handleChangeStreet }) => {
    const onChangeStreet = (value: string) => {
      const arrSplit = value.split('***');
      setRouteId(arrSplit[1]);
      handleChangeStreet(arrSplit[1], appointmentDate);
    };
    const street = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          <span className="red">* </span> Đường phố:
        </Col>
        <Col {...wrapperColTowItem} className="over-hidden">
          <Form.Item name="streetId" rules={[{ required: true, message: 'Đường phố không được bỏ trống' }]}>
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
              onChange={onChangeStreet}
              placeholder="-- Chọn Đường phố --"
            >
              {streetList?.map(street => (
                <Select.Option key={street.id} value={street.id + '***' + street.routeId}>
                  {street.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{street}</Col> : street;
  };
  //Cung đường
  interface RouteProps {
    routeNameDefault: boolean;
  }
  const Route: FC<RouteProps> = ({ routeNameDefault }) => {
    const route = (
      <Row className="field-css">
        <Col {...labelColTowItem} className="fs-12">
          Cung đường:
        </Col>
        <Col {...wrapperColTowItem}>
          <Form.Item
            name="routeId"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input placeholder="Chọn Đường phố để có Cung đường" />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperCol}>{route}</Col> : route;
  };

  //Tin nhắn gửi CBTN
  interface MgsContentProps {
    provinces?: Province[];
    districts?: District[];
    wards?: Ward[];
    workTimes?: WorkTime[];
  }
  const MgsContent: FC<MgsContentProps> = ({ provinces, districts, wards, workTimes }) => {
    const message = '';
    const mgsContent = (
      <Row className="">
        <Col {...labelColOneItem} className="fs-12">
          Tin nhắn gửi CBTN:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            // name="mgsContent"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <TextArea readOnly rows={10} placeholder="Nhập nội dung tin nhắn gửi cán bộ tại nhà" value={message} />
            {/* <Button style={{ marginTop: 10 }} type="primary" onClick={() => {}} icon={<ScheduleOutlined />}>
              Phân lịch
            </Button> */}
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{mgsContent}</Col> : mgsContent;
  };

  //Ghi chu lich hen
  interface AppointmentNotesProps {
    onSaveComment: (values) => void;
  }
  const AppointmentNotes: FC<AppointmentNotesProps> = ({ onSaveComment }) => {
    let notes = '';
    const onChange = event => {
      notes = event.target.value;
    };
    //const listData = [{} as { id: any; href: any; title: any; content: any }];
    const [listData, setListData] = useState([{} as AppointmentComment]);
    listData.shift();
    const appointmentNotes = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          Ghi chú:
        </Col>
        <Col {...wrapperColOneItem}>
          <Form.Item
            name="appointmentNotes"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <TextArea rows={7} placeholder="Nhập ghi chú lich hẹn" allowClear onChange={onChange} />
            <Button
              style={{ marginTop: 10 }}
              type="primary"
              onClick={async () => {
                onSaveComment({ content: notes });
              }}
              icon={<SaveOutlined />}
            >
              Lưu ghi chú
            </Button>
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{appointmentNotes}</Col> : appointmentNotes;
  };
  //Khung gio chi tiet lich hen
  interface AppointmentTimeFrameProps {
    appointmentWorkTimeData: WorkTime[];
  }
  const AppointmentTimeFrame: FC<AppointmentTimeFrameProps> = ({ appointmentWorkTimeData }) => {
    let workTimeArr: WorkTime[] = [];
    if (appointmentWorkTimeData && appointmentWorkTimeData.length !== 0) {
      for (let i = 0; i < appointmentWorkTimeData.length; i++) {
        let workTime: WorkTime = {} as WorkTime;
        workTime.name = appointmentWorkTimeData[i]['name'];
        workTime.id = appointmentWorkTimeData[i]['id'];
        if (appointmentWorkTimeData[i]['objectCountList'] !== null) {
          workTime.objectCountList = appointmentWorkTimeData[i]['objectCountList'];
          workTime.chuaThucHien = 0;
          workTime.daThucHien = 0;
          for (let j = 0; j < appointmentWorkTimeData[i]['objectCountList'].length; j++) {
            if (
              appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 1 ||
              appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 13
            ) {
              /*workTime.chuaThucHien = 0;
              workTime.daThucHien = 0;
              workTime.total = 0;*/
            } else {
              if (
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 2 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 4 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 5 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 6
              ) {
                workTime.chuaThucHien = appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  ? workTime.chuaThucHien + appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  : workTime.chuaThucHien;
              }
              if (
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 7 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 8 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 9 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 10 ||
                appointmentWorkTimeData[i]['objectCountList'][j]['status'] === 11
              ) {
                workTime.daThucHien = appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  ? workTime.daThucHien + appointmentWorkTimeData[i]['objectCountList'][j]['count']
                  : workTime.daThucHien;
              }
            }
          }
          workTime.total = workTime.daThucHien + workTime.chuaThucHien;
          workTimeArr.push(workTime);
        } else {
          workTime.daThucHien = 0;
          workTime.chuaThucHien = 0;
          workTime.total = 0;
          workTimeArr.push(workTime);
        }
      }
    }
    const columns = [
      {
        title: 'Khung giờ',
        dataIndex: 'name',
        key: 'name',
        render: name => name,
        className: 'timeFame'
      },
      {
        title: 'Tổng lịch đã hẹn',
        dataIndex: 'total',
        key: 'total',
        className: 'timeFame'
      },
      {
        title: 'Lịch chưa thực hiện',
        dataIndex: 'chuaThucHien',
        key: 'chuaThucHien',
        className: 'timeFame lichChuaThucHien'
      },
      {
        title: 'Lịch đã thực hiện',
        dataIndex: 'daThucHien',
        key: 'daThucHien',
        className: 'timeFame'
      }
    ];
    const appointmentTimeFrame = (
      <Form.Item>
        <Table
          rowClassName={(record, index) => (index % 2 === 0 ? 'table-row-light' : 'table-row-dark')}
          scroll={{ y: 300 }}
          style={{ height: 300, overflowX: 'hidden' }}
          pagination={false}
          columns={columns}
          dataSource={workTimeArr}
        />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentTimeFrame}</Col> : appointmentTimeFrame;
  };
  //Bang ghi chu
  interface AppointmentNotesTableProps {
    pagingAppointmentComment?: any;
    appointmentComments?: any;
    comments?: any;
    onDelete: (index) => void;
  }
  const AppointmentNotesTable: FC<AppointmentNotesTableProps> = ({
    pagingAppointmentComment,
    appointmentComments,
    onDelete,
    comments
  }) => {
    const columns = [
      {
        title: 'Nội dung ghi chú',
        dataIndex: 'content',
        key: 'content'
      },
      {
        title: 'Xóa ghi chú',
        dataIndex: 'content',
        key: 'content',
        className: 'appointment-notes',
        render: (value, item, index) => (
          <Button
            type="primary"
            onClick={() => {
              Modal.confirm({
                icon: <ExclamationCircleOutlined />,
                title: 'Bạn có chắc chắn muốn xoá ghi chú đã chọn' + '' + ' ?',
                onOk() {
                  onDelete(index);
                },
                onCancel() {
                  //console.log('Cancel');
                }
              });
            }}
            danger
            icon={<DeleteOutlined />}
          >
            Xoá
          </Button>
        )
      }
    ];
    const appointmentCommentTable = (
      <Form.Item name="comments">
        <Table scroll={{ y: 400 }} pagination={false} columns={columns} dataSource={comments} />
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{appointmentCommentTable}</Col> : appointmentCommentTable;
  };
  //Cán bộ được phân lịch
  interface AssignStaffProps {}
  const AssignStaff: FC<AssignStaffProps> = () => {
    const assignStaff = (
      <Row className="field-css">
        <Col {...labelColOneItem} className="fs-12">
          CB được phân lịch:
        </Col>
        <Col {...wrapperColOneItem} className="over-hidden">
          <Form.Item
            name="assignStaffName"
            rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
          >
            <Input readOnly={true} />
          </Form.Item>
        </Col>
      </Row>
    );
    return responsive ? <Col {...wrapperColAllCol}>{assignStaff}</Col> : assignStaff;
  };
  return React.useMemo(
    () => ({
      form: formInstance,
      Form: WrappedForm,
      AppointmentCode,
      AppointmentStatus,
      AppointmentSource,
      PhoneNumber,
      CustomerType,
      CustomerFullname,
      ReasonBooking,
      DateofBirth,
      BirthYear,
      CollaboratorDoctor,
      Gender,
      Nationality,
      MedicalExaminationPackage,
      ContactAddress,
      Test,
      Provinces,
      Districts,
      Wards,
      Street4Home,
      TestNotes,
      Email,
      TotalPrice,
      IdentityCard,
      TotalPriceDiscount,
      GeneticCode,
      ScheduleReporter,
      PID,
      MedicalPlace,
      ObjectSend,
      AppointmentObject,
      Specialists,
      AppointmentDate,
      AppointmentWorkTime,
      AppointmentTimeFrame,
      AppointmentNotes,
      AppointmentNotesTable,
      Route,
      MgsContent,
      StreetCpn,
      FacilityParent,
      ReasonBookingStr,
      BHYT,
      AssignStaff
    }),
    []
  );
}
