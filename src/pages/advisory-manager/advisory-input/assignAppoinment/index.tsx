import React, { FC, useState } from 'react';
import AppointmentOfStaffTableById from './appointmentOfStaffByIdTable';
import AppointmentTable from './appointmentTable';
import AppointmentOfAllStaffTable from './appointmentOfAllStaffTable';
import './index.less';
import AllAppointmentOfAllStaffGroupByTimeTable from './AllAppointmentOfAllStaffGroupByTimeTable';
import moment from 'moment';

interface AssignmentPageProps {
  parentFacilityId: any;
  facilityId: any;
  appointmentDate: any;
  appointmentId: any;
}

const AssignmentPage: FC<AssignmentPageProps> = ({ parentFacilityId, facilityId, appointmentDate, appointmentId }) => {
  const [staffId, setStaffId] = useState('');
  const [reload, setReload] = useState(1);
  return (
    <div className="main-content">
      <AppointmentTable
        parentFacilityId={parentFacilityId}
        facilityId={facilityId}
        appointmentId={appointmentId}
        appointmentDate={appointmentDate}
        reload={reload}
        handleReload={value => {
          setReload(value);
        }}
        handleChangeStaff={staffId => {
          setStaffId(staffId);
        }}
      />
      <AppointmentOfAllStaffTable
        parentFacilityId={parentFacilityId}
        reload={reload}
        appointmentDate={moment(appointmentDate).format('DD/MM/YYYY')}
        facilityId={facilityId}
        onSelectSiteStaff={siteStaffId => {
          setStaffId(siteStaffId);
        }}
      />
      <AppointmentOfStaffTableById
        reload={reload}
        appointmentDate={moment(appointmentDate).format('DD/MM/YYYY')}
        staffId={staffId}
      />
      <AllAppointmentOfAllStaffGroupByTimeTable
        reload={reload}
        parentFacilityId={parentFacilityId}
        appointmentDate={moment(appointmentDate).format('DD/MM/YYYY')}
        facilityId={facilityId}
      />
    </div>
  );
};
export default AssignmentPage;
