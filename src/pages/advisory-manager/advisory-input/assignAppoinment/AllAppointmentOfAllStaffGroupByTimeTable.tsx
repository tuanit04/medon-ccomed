import React, { FC, useState, useEffect, useCallback } from 'react';
import { Button, Table, Tag, Modal, Radio, Divider } from 'antd';
import { useLocale, LocaleFormatter } from 'locales';
import { apiGetRoleList } from 'api/permission/role.api';
import { Role, RoleStatus } from 'interface/permission/role.interface';
import { CloseOutlined } from '@ant-design/icons';
import { useGetAllAppointmentOfAllStaffGroupByTime } from 'hooks/appointment/useGetAllAppointmentOfAllStaffGroupByTime';
import { VAppointmentWorkTimeStaff } from 'common/interfaces/vAppointmentWorkTimeStaff.interface';

interface AllAppointmentOfAllStaffGroupByTimeTableProps {
  parentFacilityId: string;
  facilityId: string;
  appointmentDate: string;
  reload: number;
}

const AllAppointmentOfAllStaffGroupByTimeTable: FC<AllAppointmentOfAllStaffGroupByTimeTableProps> = ({
  parentFacilityId,
  facilityId,
  appointmentDate,
  reload
}) => {
  //Hook lấy danh sách Cán bộ tại nhà thuộc văn phòng/đơn vị
  const {
    allAppointmentOfAllStaffGroupByTime,
    isLoadingAllAppointmentOfAllStaffGroupByTime,
    loadAllAppointmentOfAllStaffGroupByTime
  } = useGetAllAppointmentOfAllStaffGroupByTime();
  useEffect(() => {
    if (!parentFacilityId) {
      loadAllAppointmentOfAllStaffGroupByTime({
        variables: {
          appointmentDate: appointmentDate,
          facilityId: facilityId
        }
      });
    } else if (!facilityId) {
      loadAllAppointmentOfAllStaffGroupByTime({
        variables: {
          appointmentDate: appointmentDate,
          parentFacilityId: parentFacilityId
        }
      });
    } else if (!appointmentDate) {
      loadAllAppointmentOfAllStaffGroupByTime({
        variables: {
          parentFacilityId: parentFacilityId,
          facilityId: facilityId
        }
      });
    } else {
      loadAllAppointmentOfAllStaffGroupByTime({
        variables: {
          appointmentDate: appointmentDate,
          parentFacilityId: parentFacilityId,
          facilityId: facilityId
        }
      });
    }
  }, [parentFacilityId, facilityId, appointmentDate, reload]);
  return (
    <Table rowKey="id" bordered dataSource={allAppointmentOfAllStaffGroupByTime?.data} scroll={{ x: 500 }}>
      <Table.Column<VAppointmentWorkTimeStaff>
        align="center"
        title="Khung giờ"
        render={(_, { workTimeName }) => workTimeName}
      />
      <Table.Column<VAppointmentWorkTimeStaff>
        title="Lịch đã phân, chưa thực hiện"
        render={(_, { workTimeStaffList }) =>
          workTimeStaffList?.map(tag => {
            return (
              <Tag color={'blue'} key={tag.staffCode}>
                {tag.staffCode}
              </Tag>
            );
          })
        }
      />
    </Table>
  );
};

export default AllAppointmentOfAllStaffGroupByTimeTable;
