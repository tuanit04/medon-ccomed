import React, { FC, useState, useEffect, useCallback } from 'react';
import { Button, Table, Tag, Modal, Radio, Divider, Checkbox } from 'antd';
import { useLocale, LocaleFormatter } from 'locales';
import { apiGetRoleList } from 'api/permission/role.api';
import { Role, RoleStatus } from 'interface/permission/role.interface';
import { useGetAppointmentStaff } from 'hooks/appointment/useGetAppointmentStaff';
import { AppointmentStaff } from 'common/interfaces/appointmentStaff.interface';
import { useGetAppointmentOfStaffById } from 'hooks/appointment/useGetAppointmentOfStaffById';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { useCheckAppointment } from 'hooks/appointment';

interface AppointmentOfStaffTableProps {
  staffId: string;
  appointmentDate: string;
  reload: number;
}

const AppointmentOfStaffTable: FC<AppointmentOfStaffTableProps> = ({ staffId, appointmentDate, reload }) => {
  //Hook lấy danh sách Danh sách lịch hẹn được phân cho cán bộ
  const {
    appointmentOfStaffById,
    isLoadingAppointmentOfStaffById,
    loadAppointmentOfStaffById
  } = useGetAppointmentOfStaffById();
  //Hook Check lịch
  const {
    checkAppointment,
    isLoadingCheckAppointment,
    errorCheckAppointment,
    resultCheckAppointment
  } = useCheckAppointment();
  useEffect(() => {
    if (staffId && appointmentDate && reload) {
      loadAppointmentOfStaffById({
        variables: {
          appointmentDate,
          staffId
        }
      });
    }
  }, [staffId, appointmentDate, reload]);
  return (
    <Table
      rowKey="id"
      pagination={{ pageSize: 5 }}
      bordered
      loading={isLoadingAppointmentOfStaffById}
      dataSource={appointmentOfStaffById?.data}
      scroll={{ x: 500 }}
      title={() => (
        <>
          <div>
            <label className="lblTableTxt">
              Danh sách lịch hẹn được phân cho cán bộ {appointmentOfStaffById?.data?.[0]?.['siteStaffName']}{' '}
            </label>
            <label className="lblTableValue">({appointmentOfStaffById ? appointmentOfStaffById['records'] : 0})</label>
            <Divider />
          </div>
        </>
      )}
    >
      <Table.Column<AppointmentStaff> title="Mã lịch" align="center" render={(_, { appointmentId }) => appointmentId} />
      <Table.Column<AppointmentStaff> title="Cung đường" render={(_, { routeName }) => routeName} />
      <Table.Column<AppointmentStaff> title="Địa chỉ" render={(_, { appointmentAddress }) => appointmentAddress} />
      <Table.Column<AppointmentStaff> title="Ghi chú" render={(_, { appointmentNote }) => appointmentNote} />
      <Table.Column<AppointmentStaff> title="Giờ hẹn" align="center" render={(_, { workTimeName }) => workTimeName} />
      <Table.Column<AppointmentStaff> title="Cán bộ TN" render={(_, { siteStaffName }) => siteStaffName} />
      <Table.Column<AppointmentStaff>
        align="center"
        title="Check lịch"
        render={(_, { isCheck, appointmentId }) => {
          let check: boolean = isCheck === null || isCheck === undefined ? false : isCheck === 1 ? true : false;
          return (
            <Checkbox
              checked={check}
              onChange={e => {
                if (e.target.checked) {
                  Modal.confirm({
                    icon: <ExclamationCircleOutlined />,
                    title: 'Bạn có chắc chắn muốn check lịch đã chọn ?',
                    async onOk() {
                      await checkAppointment({
                        appointmentIds: [appointmentId]
                      });
                      if (staffId && appointmentDate) {
                        loadAppointmentOfStaffById({
                          variables: {
                            appointmentDate,
                            staffId
                          }
                        });
                      }
                    },
                    onCancel() {
                      //console.log('Cancel');
                    }
                  });
                } else {
                  Modal.confirm({
                    icon: <ExclamationCircleOutlined />,
                    title: 'Bạn có chắc chắn muốn bỏ check lịch đã chọn ?',
                    async onOk() {
                      await checkAppointment({
                        appointmentIds: [appointmentId]
                      });
                      if (staffId && appointmentDate) {
                        loadAppointmentOfStaffById({
                          variables: {
                            appointmentDate,
                            staffId
                          }
                        });
                      }
                    },
                    onCancel() {
                      //console.log('Cancel');
                    }
                  });
                }
              }}
            ></Checkbox>
          );
        }}
      />
      <Table.Column<AppointmentStaff> title="Thời gian lấy mẫu" render={(_, { sampleTakenDate }) => sampleTakenDate} />
    </Table>
  );
};

export default AppointmentOfStaffTable;
