import React, { FC, useEffect } from 'react';
import { Button, Divider, Table } from 'antd';
import { PhoneOutlined } from '@ant-design/icons';
import { VAppointmentOfStaff } from 'common/interfaces/vAppointmentOfStaff.interface';
import { useGetAppointmentOfAllStaff } from 'hooks/appointment/useGetAppointmentOfAllStaff';

interface AppointmentOfAllStaffTableProps {
  parentFacilityId: string;
  facilityId: string;
  appointmentDate: string;
  onSelectSiteStaff: (siteStaffId: string) => void;
  reload: number;
}

const AppointmentOfAllStaffTable: FC<AppointmentOfAllStaffTableProps> = ({
  parentFacilityId,
  facilityId,
  appointmentDate,
  onSelectSiteStaff,
  reload
}) => {
  //Hook lấy danh sách Cán bộ tại nhà thuộc văn phòng/đơn vị
  const {
    appointmentOfAllStaff,
    isLoadingAppointmentOfAllStaff,
    loadAppointmentOfAllStaff
  } = useGetAppointmentOfAllStaff();
  useEffect(() => {
    if (parentFacilityId && appointmentDate && reload) {
      if (facilityId) {
        loadAppointmentOfAllStaff({
          variables: {
            appointmentDate,
            parentFacilityId,
            facilityId
          }
        });
      } else {
        loadAppointmentOfAllStaff({
          variables: {
            appointmentDate,
            parentFacilityId
          }
        });
      }
    }
  }, [parentFacilityId, facilityId, appointmentDate, reload]);
  return (
    <Table
      rowKey="id"
      pagination={{ pageSize: 5 }}
      bordered
      loading={isLoadingAppointmentOfAllStaff}
      dataSource={appointmentOfAllStaff?.data}
      scroll={{ x: 500 }}
      onRow={(record, rowIndex) => {
        return {
          onClick: event => {
            onSelectSiteStaff(record['staffId']);
          }
        };
      }}
      title={() => (
        <>
          <div style={{ float: 'left' }}>
            <label className="lblTableTxt">Cán bộ tại nhà thuộc văn phòng/đơn vị </label>
            <label className="lblTableValue">({appointmentOfAllStaff ? appointmentOfAllStaff['records'] : 0})</label>
            <Divider />
          </div>
          <div style={{ float: 'right' }}>
            <Button type="primary" shape="circle" icon={<PhoneOutlined />} />
          </div>
          <div style={{ clear: 'both' }}></div>
        </>
      )}
    >
      <Table.Column<VAppointmentOfStaff> title="STT" align="center" render={(value, item, index) => index + 1} />
      <Table.Column<VAppointmentOfStaff> align="center" title="Mã NV" render={(_, { staffCode }) => staffCode} />
      <Table.Column<VAppointmentOfStaff> title="Tên nhân viên" render={(_, { staffName }) => staffName} />
      <Table.Column<VAppointmentOfStaff> title="Ca làm việc" render={(_, { workSessionName }) => workSessionName} />
      <Table.Column<VAppointmentOfStaff> title="Cung đường" render={(_, { routeName }) => routeName} />
      <Table.Column<VAppointmentOfStaff> align="center" title="Lịch chưa thực hiện" render={(_, { routeName }) => 0} />
      <Table.Column<VAppointmentOfStaff> align="center" title="Lịch đã thực hiện" render={(_, { routeName }) => 0} />
      <Table.Column<VAppointmentOfStaff> align="center" title="Tổng lịch đã phân" render={(_, { routeName }) => 0} />
    </Table>
  );
};

export default AppointmentOfAllStaffTable;
