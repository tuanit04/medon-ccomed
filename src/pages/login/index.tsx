import { Button, Form, Input, Spin } from 'antd';
import React, { useEffect } from 'react';
import Logo from '../../assets/logo/medon-logo.png';
import './style.less';
import { LoadingOutlined, LockOutlined, UserOutlined } from '@ant-design/icons';
import Background from '../../assets/images/background.jpg';
import { useLocale } from 'locales';
import { useNavigate } from 'react-router-dom';
import { loginAsync } from 'store/user.store';
import { useAppDispatch, useAppState } from 'helpers';

const LoginPage = () => {
  const { formatMessage } = useLocale();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const user = useAppState(state => state.user);
  const onSubmit = ({ username, password }) => {
    dispatch(
      loginAsync({
        username,
        password,
        loginType: 4
      })
    );
  };

  useEffect(() => {
    if (user.logged) {
      navigate('/dashboard');
    }
  });
  useEffect(() => {
    if (user.logged && localStorage.getItem('IS_SHOW_CREATE_APPOINTMENT_DIALOG')) {
      //localStorage.removeItem('TOKEN_EXPIRED');
      navigate('/appointment-management/appointment');
    } else if (user.logged && localStorage.getItem('IS_SHOW_CREATE_APPOINTMENT_COVID_DIALOG')) {
      navigate('/appointment-management/appointment-covid');
    }
  });
  return (
    <div id="loginPage">
      <img src={Background} width="100%" className="background-login" alt="background" />
      <div className="login">
        <div className="logo">
          <img src={Logo} alt="logo" />
        </div>
        <p className="title">{formatMessage({ id: 'app.login.buttonLogin' })}</p>
        {user.error?.message && <p className="error">{user.error.message}</p>}
        <Spin spinning={user.loading} tip="Đang đăng nhập...">
          <Form
            onFinish={onSubmit}
            initialValues={{
              username: '',
              password: ''
            }}
          >
            <Form.Item
              labelAlign="right"
              labelCol={{ span: 6 }}
              rules={[{ required: true, message: formatMessage({ id: 'app.login.requiredUsername' }) }]}
              name="username"
            >
              <Input placeholder={formatMessage({ id: 'app.login.username' })} prefix={<UserOutlined />} />
            </Form.Item>
            <Form.Item
              labelAlign="right"
              labelCol={{ span: 6 }}
              rules={[{ required: true, message: formatMessage({ id: 'app.login.requiredPassword' }) }]}
              name="password"
            >
              <Input.Password
                placeholder={formatMessage({ id: 'app.login.password' })}
                prefix={<LockOutlined className="iconLogin" />}
              />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" block className="btnLogin">
                {formatMessage({ id: 'app.login.buttonLogin' })}
              </Button>
            </Form.Item>
          </Form>
        </Spin>
      </div>
    </div>
  );
};

export default LoginPage;
