import { FC, useEffect, Suspense, useState } from 'react';
import { Layout, Spin } from 'antd';
import './index.less';
import MenuComponent from './menu';
import { getGlobalState } from 'utils/getGloabal';
import SuspendFallbackLoading from './suspendFallbackLoading';
import { MenuList, MenuChild } from 'interface/layout/menu.interface';
import { Outlet, useLocation, useNavigate } from 'react-router';
import { permissionAsync, setUserItem } from 'store/user.store';
import React from 'react';
import { useAppDispatch, useAppState } from 'helpers';
import Dialer from 'common/components/Dialer';
import PatientDialog from 'pages/call-management/paitent';
import { initSocket } from 'utils/vcc/socket';
import { functionCodeConstants } from 'constants/functions';
import mockMenuList from 'config/menu';

const { Sider, Content } = Layout;
const WIDTH = 992;

const whitelist = ['dashboard', 'account', 'TD_BAOCAO_LHTV'];

const LayoutPage: FC = () => {
  const [menuList, setMenuList] = useState<MenuList>([]);
  const { loadingFunction, functions, functionObject } = useAppState(state => state.user);
  const { openDetailDialog } = useAppState(state => state.dialer);
  const { domain, isVCCLoggedIn, tokenVcc } = useAppState(state => state.auth);
  const dispatch = useAppDispatch();
  const location = useLocation();
  const navigate = useNavigate();
  const userLogined = JSON.parse(String(localStorage.getItem('user')));

  useEffect(() => {
    if (location.pathname === '/') {
      navigate('/login');
    }
  }, [navigate, location]);

  useEffect(() => {
    if (isVCCLoggedIn && domain && tokenVcc && functionObject?.[functionCodeConstants.TD_CUOCGOI_THCG] != null) {
      initSocket(tokenVcc, domain);
    }
  }, [isVCCLoggedIn, functionObject]);

  const initMenuListAll = (menu: MenuList) => {
    const MenuListAll: MenuChild[] = [];
    menu.forEach(m => {
      if (!m?.children?.length) {
        MenuListAll.push(m);
      } else {
        m?.children.forEach(mu => {
          MenuListAll.push(mu);
        });
      }
    });
    return MenuListAll;
  };

  useEffect(() => {
    dispatch(permissionAsync());
  }, []);

  useEffect(() => {
    var list: any[] = [];
    if (functions) {
      var listFunC = functions;
      mockMenuList.forEach(item => {
        if (whitelist.includes(item.name)) {
          list.push(item);
        } else if (checkParentMenu(item.name, listFunC)) {
          var itemT: any = item;
          var listC: any[] = [];
          item?.children?.forEach(itemC => {
            if (checkChildMenu(itemC.name, listFunC)) {
              listC.push(itemC);
            }
          });
          itemT = {
            ...itemT,
            children: listC
          };
          // itemT['children'] = listC;
          list.push(itemT);
        }
      });
    }
    list.unshift({
      name: 'account',
      label: {
        zh_CN: userLogined ? 'Hi, ' + userLogined['name'] : '',
        en_US: userLogined ? 'Hi, ' + userLogined['name'] : ''
      },
      icon: 'account',
      key: 'userlogined',
      path: '/account',
      children: [
        {
          name: 'account-change-pass',
          label: {
            zh_CN: 'Đổi mật khẩu',
            en_US: 'Đổi mật khẩu'
          },
          key: 'CHANGE-PASS',
          path: '/user/change-password'
        },
        {
          name: 'account-logout',
          label: {
            zh_CN: 'Đăng xuất',
            en_US: 'Đăng xuất'
          },
          key: 'LOGOUT',
          path: '/user/logout'
        }
      ]
    });
    setMenuList(list);
    dispatch(
      setUserItem({
        menuList: initMenuListAll(list)
      })
    );
  }, [functions]);

  const checkParentMenu = (key, list) => {
    for (var i = 0; i < list.length; i++) {
      if (list[i].functionCode.includes(key)) {
        return true;
      }
    }
    return false;
  };

  const checkChildMenu = (key, list) => {
    for (var i = 0; i < list.length; i++) {
      if (list[i].functionCode.trim() === key.trim()) {
        return true;
      }
    }
    return false;
  };

  // const fetchMenuList = useCallback(async () => {
  //   const { status, result } = await getMenuList();
  //   if (status) {
  //     console.log(result);
  //     setMenuList(result);
  //     dispatch(
  //       setUserItem({
  //         menuList: initMenuListAll(result)
  //       })
  //     );
  //   }
  // }, [dispatch]);

  // useEffect(() => {
  //   fetchMenuList();
  // }, [fetchMenuList]);

  useEffect(() => {
    window.onresize = () => {
      const { device } = getGlobalState();
      const rect = document.body.getBoundingClientRect();
      const needCollapse = rect.width < WIDTH;
      dispatch(
        setUserItem({
          device,
          collapsed: needCollapse
        })
      );
    };
  }, [dispatch]);

  const [isCollapse, setCollapsed] = useState(false);
  const onCollapse = collapsed => {
    setCollapsed(collapsed);
  };

  return (
    <Layout>
      <Sider
        className="layout-page-sider"
        collapsible
        collapsed={isCollapse}
        onCollapse={onCollapse}
        breakpoint="md"
        trigger={null}
      >
        <a href="/">
          <div className="logo-header">
            <img src="/logo-header.svg" alt="Hệ thống CCOMED" />
          </div>
        </a>
        <MenuComponent menuList={menuList} isCollapse={isCollapse} />
      </Sider>
      {/* {!isMobile ? (
        <Sider
          className="layout-page-sider"
          collapsible
          collapsed={isCollapse}
          onCollapse={onCollapse}
          breakpoint="md"
          trigger={null}
        >
          <a href="/">
            <div className="logo-header">
              <img src="/logo-header.svg" alt="Hệ thống CCOMED" />
            </div>
          </a>
          <MenuComponent menuList={menuList} isCollapse={isCollapse} />
        </Sider>
      ) : (
        <Drawer
          width="200"
          placement="left"
          bodyStyle={{ padding: 0, height: '100%' }}
          closable={false}
          onClose={toggle}
          visible={!collapsed}
        >
          <div className="logo" />
          <MenuComponent menuList={menuList} isCollapse={isCollapse} />
        </Drawer>
      )} */}
      <Layout className="site-layout">
        <Content className="layout-page-content">
          {/*<Breadcrumb />*/}
          <Suspense
            fallback={
              <SuspendFallbackLoading message="Đang tải nội dung" description="Vui lòng chờ trong giây lát ..." />
            }
          >
            {loadingFunction && (
              <Spin
                className="login-spinner"
                spinning={false}
                size="large"
                tip="Vui lòng chờ trong giây lát ..."
              ></Spin>
            )}
            <Outlet />
          </Suspense>
        </Content>
        <Dialer />
        {openDetailDialog != null && <PatientDialog />}
      </Layout>
    </Layout>
  );
};

export default LayoutPage;
