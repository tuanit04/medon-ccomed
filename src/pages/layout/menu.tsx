import { FC, useState, useEffect, useRef } from 'react';
import { Menu, Tooltip } from 'antd';
import { MenuList } from '../../interface/layout/menu.interface';
import { useNavigate, useLocation, Link } from 'react-router-dom';
import { CustomIcon } from './customIcon';
import { useAppDispatch, useAppState } from 'helpers';
import { logoutAsync, setUserItem } from 'store/user.store';
import { addTag } from 'store/tags-view.store';
import React from 'react';
import { AudioMutedOutlined, AudioOutlined, LoadingOutlined, PhoneOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import { onOpenDialer } from 'store/dialer.store';
import StatusButton, { StatusButtonRef } from 'common/components/Dialer/StatusButton';
import { csEnableCall } from 'utils/vcc/actions';
import { functionCodeConstants } from 'constants/functions';
import { initSocket } from 'utils/vcc/socket';

const { SubMenu, Item } = Menu;

const MenuItemDialer = styled(Item)`
  margin: 0.5rem auto !important;
  width: fit-content !important;
  border-radius: 0.25rem;
  background-color: ${props => (props.enabled ? 'green' : props.error ? 'red' : '#206ad2')} !important;
  color: white !important;
`;

const MenuItemAudio = styled(Item)`
  margin: 0.5rem auto !important;
  width: fit-content !important;
  border-radius: 0.25rem;
  background-color: ${props => (props.enabled ? 'green' : 'red')} !important;
  color: white !important;
  padding: 0 1rem !important;
`;

interface MenuProps {
  menuList: MenuList;
  isCollapse?: boolean;
}

const MenuComponent: FC<MenuProps> = ({ menuList, isCollapse }) => {
  const [openKeys, setOpenkeys] = useState<string[]>([]);
  const [selectedKeys, setSelectedKeys] = useState<string[]>([]);
  const { collapsed, device, locale, functionObject } = useAppState(state => state.user);
  const [microphoneEnabled, setMicrophoneEnabled] = useState(false);
  const { initStatus, callData } = useAppState(state => state.dialer);
  const { domain, tokenVcc } = useAppState(state => state.auth);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const statusButtonRef = useRef<StatusButtonRef>(null);

  //Xử lý đăng xuất
  const onLogout = () => {
    dispatch(logoutAsync());
  };

  useEffect(() => {
    // Firefox 1.0+
    if (navigator.userAgent.indexOf('Chrome') !== -1) {
      navigator.permissions.query({ name: 'microphone' }).then(permissionStatus => {
        setMicrophoneEnabled(permissionStatus.state === 'granted');

        permissionStatus.onchange = function() {
          setMicrophoneEnabled(this.state === 'granted');
        };
      });
    }

    checkAudio();
  }, []);

  const checkAudio = async () => {
    let stream: MediaStream | null = null;

    try {
      stream = await navigator.mediaDevices.getUserMedia({ audio: true });
      setMicrophoneEnabled(true);
    } catch (err) {
      setMicrophoneEnabled(false);
    }
  };

  const getTitie = (menu: MenuList[0]) => {
    return (
      <span style={{ display: 'flex', alignItems: 'center' }}>
        <CustomIcon type={menu.icon!} />
        <span title={menu.label[locale]}>{menu.label[locale]}</span>
      </span>
    );
  };

  const onMenuClick = (menu: MenuList[0]) => {
    if (menu.key === 'LOGOUT') {
      onLogout();
      return;
    }
    if (menu.path === pathname) return;
    const { key, label, path } = menu;
    setSelectedKeys([key]);
    if (device !== 'DESKTOP') {
      dispatch(setUserItem({ collapsed: true }));
    }
    dispatch(
      addTag({
        id: key,
        label,
        path,
        closable: true
      })
    );
    //Xóa cache khi xảy ra sự kiện chuyển trang
    localStorage.removeItem('appoiment-covid-create-data');
    localStorage.removeItem('appoiment-create-data');
    navigate(path);
  };

  useEffect(() => {
    setSelectedKeys([pathname]);
    setOpenkeys(collapsed ? [] : ['/' + pathname.split('/')[1]]);
  }, [collapsed, pathname]);

  const isCallAllowed = domain && tokenVcc && functionObject?.[functionCodeConstants.TD_CUOCGOI_THCG] != null;

  const onOpenChange = (keys: string[]) => {
    const key = keys.pop();

    setOpenkeys(key ? [key] : []);
  };

  const onClickDialer = () => {
    if (initStatus === 'success') {
      if (callData?.enableVoice) {
        dispatch(onOpenDialer());
      } else {
        csEnableCall();
      }
    } else if (initStatus === 'failed') {
      if (isCallAllowed) {
        initSocket(tokenVcc ?? '', domain ?? '');
      }
    }
  };

  const dialerButtonTitle =
    initStatus === 'success'
      ? callData?.enableVoice
        ? 'Quay số'
        : 'Kích hoạt'
      : initStatus === 'failed'
      ? 'Thử lại'
      : 'Đang tải';

  return (
    <Menu
      mode="inline"
      theme="dark"
      selectedKeys={selectedKeys}
      openKeys={openKeys}
      onOpenChange={onOpenChange as any}
      className="layout-page-sider-menu"
    >
      {menuList?.map(menu =>
        menu.children ? (
          <SubMenu className={menu.key} key={menu.path} title={getTitie(menu)}>
            {menu.children?.map(child => (
              <Item key={child.path}
              // onClick={() => onMenuClick(child)}
              title={child.label[locale]}>
                <Link key={child.path} style={{
                  textDecoration:'none'
                }} to={child.path}>
                  {child.label[locale]}
                </Link>
              </Item>
            ))}
          </SubMenu>
        ) : (
          <Item key={menu.path} onClick={() => onMenuClick(menu)} title={getTitie(menu)}>
            {getTitie(menu)}
          </Item>
        )
      )}
      {isCallAllowed && (
        <>
          {callData?.enableVoice && (
            <Item
              disabled={initStatus !== 'success'}
              key="status"
              onClick={() => {
                if (callData?.callStatus === 'Online') {
                  statusButtonRef.current?.setVisible(!statusButtonRef.current.visible);
                }
              }}
            >
              <StatusButton ref={statusButtonRef} showStatus={!isCollapse} />
            </Item>
          )}
          <MenuItemDialer
            key="dialer"
            enabled={callData?.enableVoice}
            disabled={initStatus === 'loading'}
            loading={initStatus === 'loading'}
            error={initStatus === 'failed'}
            icon={initStatus === 'loading' ? <LoadingOutlined /> : <PhoneOutlined />}
            onClick={onClickDialer}
          >
            {dialerButtonTitle}
          </MenuItemDialer>
          {!microphoneEnabled && (
            <MenuItemAudio key="audio" enabled={microphoneEnabled} onClick={() => checkAudio()}>
              <Tooltip
                title={microphoneEnabled ? 'Đã cấp quyền cho microphone' : 'Vui lòng cấp quyền cho microphone'}
                placement="bottom"
              >
                {microphoneEnabled ? <AudioOutlined /> : <AudioMutedOutlined />}
              </Tooltip>
            </MenuItemAudio>
          )}
        </>
      )}
    </Menu>
  );
};

export default MenuComponent;
