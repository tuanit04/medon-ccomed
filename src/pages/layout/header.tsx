import { FC } from 'react';
import { LogoutOutlined, UserOutlined, MenuUnfoldOutlined, MenuFoldOutlined } from '@ant-design/icons';
import { Layout, Dropdown, Menu, Avatar } from 'antd';
import { useNavigate } from 'react-router-dom';
import { LocaleFormatter, useLocale } from 'locales';
import { logoutAsync } from 'store/user.store';
import { useAppDispatch, useAppState } from '../../helpers';
import React from 'react';

const { Header } = Layout;

interface HeaderProps {
  collapsed: boolean;
  toggle: () => void;
}

const HeaderComponent: FC<HeaderProps> = ({ collapsed, toggle }) => {
  const { device } = useAppState(state => state.user);
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const userLogined = JSON.parse(String(localStorage.getItem('user')));
  
  const onLogout = () => {
    dispatch(logoutAsync());
  };

  const menu = (
    <Menu>
      <Menu.Item key="1">
        <span>
          <UserOutlined />
          <span onClick={() => navigate('/dashboard')}>
            <LocaleFormatter id="header.avator.account" />
          </span>
        </span>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="2">
        <span>
          <LogoutOutlined />
          <span onClick={onLogout}>
            <LocaleFormatter id="header.avator.logout" />
          </span>
        </span>
      </Menu.Item>
    </Menu>
  );
  return (
    <Header className="layout-page-header">
      {device !== 'MOBILE' && (
        <div className="logo" style={{ width: collapsed ? 80 : 200 }}>
          <img
            src="/logo.svg"
            alt=""
            style={{
              marginRight: collapsed ? '2px' : '20px',
              height: collapsed ? '25px' : '45px',
              marginTop: collapsed ? '0px' : '0px'
            }}
          />
        </div>
      )}
      <div className="layout-page-header-main">
        <div onClick={toggle}>
          <span id="sidebar-trigger">{collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}</span>
        </div>
        <div className="actions">
          {/*<HeaderNoticeComponent />
          <Dropdown
            trigger={['click']}
            overlay={
              <Menu onClick={selectLocale}>
                <Menu.Item style={{ textAlign: 'left' }} disabled={locale === 'zh_CN'} key="zh_CN">
                  <ZhCnSvg /> Vietnamese
                </Menu.Item>
                <Menu.Item style={{ textAlign: 'left' }} disabled={locale === 'en_US'} key="en_US">
                  <EnUsSvg /> English
                </Menu.Item>
              </Menu>
            }
          >
            <span>
              <LanguageSvg id="language-change" />
            </span>
          </Dropdown>*/}
          {/* {logged ? (
            <Dropdown overlay={menu} trigger={['click']}>
              <span className="user-action">
                <img src={Avator} className="user-avator" alt="avator" />
              </span>
            </Dropdown>
          ) : (
            <span style={{ cursor: 'pointer' }} onClick={toLogin}>
              {formatMessage({ id: 'gloabal.tips.login' })}
            </span>
          )} */}
          <span>{userLogined ? userLogined['name'] : ''}</span>
          <Dropdown overlay={menu} trigger={['click']}>
            <span className="user-action">
              <Avatar size={32} icon={<UserOutlined />} />
            </span>
          </Dropdown>
        </div>
      </div>
    </Header>
  );
};

export default HeaderComponent;
