import React, { FC, useEffect, useState } from 'react';
import { Modal } from 'antd';
import useGetStreetForm from './useGetStreetForm';
import { useGetFacilityHome, useGetFacilitys } from 'hooks/facility/useGetFacility';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { useGetWards } from 'hooks/ward/useGetWard';
import { useGetRoutesLazy } from 'hooks/route';
import { Street } from 'common/interfaces/street.interface';

interface StreetCreateDialogProps {
  visible: boolean;
  onCreate: (values: Street) => void;
  onCancel: () => void;
}

const StreetCreateDialog: FC<StreetCreateDialogProps> = ({ onCreate, onCancel, visible }) => {
  const [parentFacilityId, setParentFacilityId] = useState('');
  const facilityParentList = useGetFacilityHome();
  //Hook Lấy danh sách Đơn vị
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  //Hook Lấy danh sách Cung đường
  const { routesData, isLoadingRoutes, loadRoutes } = useGetRoutesLazy();
  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //Hook Lấy danh sách Quận/Huyện
  const { districts, isLoadingDistricts, loadDistricts } = useGetDistricts();
  //Hook Lấy danh sách Xã/Phường
  const { wards, isLoadingWards, loadWards } = useGetWards();
  const {
    form,
    Form,
    StreetCode,
    StreetName,
    Provinces,
    Districts,
    Wards,
    FacilityParent,
    Facility,
    Route,
    Status
  } = useGetStreetForm({
    name: 'searchForm',
    responsive: true,
    isModal: true,
    isCreate: true
  });
  const onSubmit = async () => {
    const valuesCreate: any = await form.validateFields();
    onCreate({ ...valuesCreate });
  };
  useEffect(() => {
    loadProvinces();
  }, []);
  useEffect(() => {
    form.resetFields();
  }, [visible]);
  return (
    <Modal
      maskClosable={false}
      width={900}
      title="Thêm mới Đường phố"
      visible={visible}
      onOk={onSubmit}
      onCancel={onCancel}
    >
      <Form style={{ marginTop: '10px' }}>
        <StreetCode />
        <StreetName />
        <Provinces
          handleChangeProvince={provinceId => {
            if (provinceId !== '') {
              loadDistricts({
                variables: {
                  provinceId: provinceId
                }
              });
            }
          }}
          provinceList={provinces}
        />
        <Districts
          handleChangeDistrict={districtId => {
            if (districtId !== '') {
              loadWards({
                variables: {
                  districtId: districtId
                }
              });
            }
          }}
          districtList={districts}
        />
        {/* <Wards handleChangeWard={wardId => {}} wardList={wards} /> */}
        <FacilityParent
          facilityParentList={facilityParentList}
          handleChangeFacilityParent={(parentFacilityId: any) => {
            setParentFacilityId(parentFacilityId);
            if (parentFacilityId !== '0') {
              form.setFieldsValue({
                facilityId: ''
              })
              loadFacilities({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentId',
                      value: parentFacilityId,
                      operation: '=='
                    },
                    {
                      id: 'status',
                      value: '1',
                      operation: '=='
                    }
                  ]
                }
              });
              loadRoutes({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentFacilityId',
                      value: parentFacilityId,
                      operation: '=='
                    }
                  ]
                }
              });
            }
          }}
        />
        <Facility
          facilityList={facilities}
          handleChangeFacility={(facilityId: any) => {
            if (facilityId !== '') {
              loadRoutes({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentFacilityId',
                      value: parentFacilityId,
                      operation: '=='
                    },
                    {
                      id: 'facilityId',
                      value: facilityId,
                      operation: '=='
                    }
                  ]
                }
              });
            } else {
              loadRoutes({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentFacilityId',
                      value: parentFacilityId,
                      operation: '=='
                    }
                  ]
                }
              });
            }
          }}
        />
        <Route routes={routesData} handleChange={() => {}} />
        <Status />
      </Form>
    </Modal>
  );
};

export default StreetCreateDialog;
