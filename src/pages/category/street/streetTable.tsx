import React, { FC, useEffect, useState } from 'react';
import { Button, Table, Modal, Popover, Tag } from 'antd';
import { CheckCircleOutlined, ExclamationCircleOutlined, MinusCircleOutlined } from '@ant-design/icons';
import { PlusCircleOutlined, DeleteOutlined, EditOutlined, SelectOutlined } from '@ant-design/icons';
import { Street } from 'common/interfaces/street.interface';
import { useFindStreetStatistic, useGetStreets } from 'hooks/street';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { functionCodeConstants } from 'constants/functions';

interface StreetTableProps {
  functionOb: any;
  onCreate: () => void;
  onAuthorize: (row: Street) => void;
  onModify: (street: Street) => void;
  onDelete: (value: string) => void;
  onActiveStreet: (value: string) => void;
  onInactiveStreet: (value: string) => void;
  nameSearch: FilteredInput;
  codeSearch: FilteredInput;
  routeSearch: FilteredInput;
  parentFacilityIdSearch: FilteredInput;
  facilityIdSearch: FilteredInput;
  statusSearch: FilteredInput;
  streetIdRes: string;
  onResetStreetIdRes: () => void;
}

const StreetTable: FC<StreetTableProps> = ({
  functionOb,
  onCreate,
  onAuthorize,
  onModify,
  onDelete,
  onActiveStreet,
  onInactiveStreet,
  nameSearch,
  codeSearch,
  routeSearch,
  parentFacilityIdSearch,
  facilityIdSearch,
  statusSearch,
  streetIdRes,
  onResetStreetIdRes
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(20);
  let filtered: FilteredInput[] = [];
  if (nameSearch['value'] !== '') {
    filtered.push(nameSearch);
  }
  if (codeSearch['value'] !== '') {
    filtered.push(codeSearch);
  }
  if (routeSearch['value'] !== '') {
    filtered.push(routeSearch);
  }
  if (parentFacilityIdSearch['value'] !== '') {
    filtered.push(parentFacilityIdSearch);
  }
  if (facilityIdSearch['value'] !== '') {
    filtered.push(facilityIdSearch);
  }
  if (statusSearch['value'] !== '') {
    filtered.push(statusSearch);
  }
  let isCheck = false;
  if (filtered.length === 0) {
    isCheck = true;
  }
  const sorted = [{ id: 'createDate', desc: false }];
  const variable = !isCheck ? { page, pageSize, filtered } : { page, pageSize, filtered, sorted };
  const { pagingStreets, isLoading, error, refetchStreets } = useGetStreets(variable);
  const {
    streetStatistic,
    isLoadingStreetStatistic,
    errorStreetStatistic,
    refetchStreetStatistic
  } = useFindStreetStatistic();
  const [pagination, setPagination] = useState({});
  useEffect(() => {
    if (!isCheck) {
      setPage(1);
    }
    setPagination({ current: page, pageSize: pageSize, total: pagingStreets['records'] });
  }, [pagingStreets]);
  useEffect(() => {
    if (streetIdRes !== '') {
      refetchStreets();
      refetchStreetStatistic();
    }
    onResetStreetIdRes();
  }, [streetIdRes]);
  const handleActiveStreet = async streetId => {
    await onActiveStreet(streetId);
    refetchStreets();
    refetchStreetStatistic();
  };

  const handleInactiveStreet = async streetId => {
    await onInactiveStreet(streetId);
    refetchStreets();
    refetchStreetStatistic();
  };
  const contentActionButton = (street: Street) => (
    <div>
      <Button
        disabled={functionOb[functionCodeConstants.TD_DM_DP_UPDATE] ? false : true}
        type="primary"
        icon={<EditOutlined />}
        style={{ marginLeft: 10 }}
        onClick={() => {
          onModify(street);
          /*if (street.status === 2) {
            openNotificationRight('Không thể cập nhật bản ghi có trạng thái là KHÔNG KÍCH HOẠT.');
          } else {
            onModify(street);
          }*/
        }}
      >
        Sửa
      </Button>
      <Button
        disabled={functionOb[functionCodeConstants.TD_DM_DP_ACTIVE_INACTIVE] ? false : true}
        type="primary"
        icon={street.status === 1 ? <MinusCircleOutlined /> : <CheckCircleOutlined />}
        style={{ marginLeft: 10 }}
        onClick={() => {
          Modal.confirm({
            icon: <ExclamationCircleOutlined />,
            title: 'Bạn có chắc chắn muốn cập nhật trạng thái bản ghi có mã đường phố : ' + street.code + ' ?',
            onOk() {
              if (street.status === 2) {
                handleActiveStreet(street.id);
              } else if (street.status === 1) {
                handleInactiveStreet(street.id);
              }
            },
            onCancel() {
              //console.log('Cancel');
            }
          });
        }}
      >
        {street.status === 1 ? 'Hủy kích hoạt' : 'Kích hoạt'}
      </Button>
      <Button
        disabled={functionOb[functionCodeConstants.TD_DM_DP_DELETE] ? false : true}
        danger
        icon={<DeleteOutlined />}
        style={{ marginLeft: 10 }}
        type="primary"
        onClick={() => {
          Modal.confirm({
            icon: <ExclamationCircleOutlined />,
            title: 'Bạn có chắc chắn muốn xóa bản ghi có mã đường phố là : ' + street.code + ' ?',
            onOk() {
              handleDelete(street.id);
            },
            onCancel() {
              //console.log('Cancel');
            }
          });
        }}
      >
        Xóa
      </Button>
    </div>
  );

  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    if (pageSize !== pagination['pageSize']) {
      setPage(1);
    } else {
      setPage(pagination['current']);
    }
    setPageSize(pagination['pageSize']);
  };
  const handleDelete = async id => {
    await onDelete(id);
    refetchStreets();
    refetchStreetStatistic();
  };

  return (
    <Table
      rowKey="id"
      style={{ height: 'calc(100vh - 125px)', overflow: 'hidden' }}
      className="table-street"
      pagination={{
        ...pagination,
        defaultPageSize: 20,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '50']
      }}
      loading={isLoading}
      onChange={handleTableChange}
      bordered
      dataSource={pagingStreets.data}
      scroll={{ x: true, y: 'calc(100vh - 250px)' }}
      title={() => (
        <>
          <div style={{ float: 'left', marginTop: '5px' }}>
            <label className="lblTableTxt">
              <Tag color="#87d068">Tổng số bản ghi : {streetStatistic['totalRecords']}</Tag>
            </label>
            &nbsp;
            <label className="lblTableTxt">
              <Tag color="#2db7f5">Kích hoạt : {streetStatistic['activeRecords']}</Tag>{' '}
            </label>
            &nbsp;
            <label className="lblTableTxt">
              <Tag color="#f50">Không kích hoạt : {streetStatistic['inactiveRecords']}</Tag>
            </label>
            &nbsp;
          </div>
          <div style={{ float: 'right' }}>
            <Button
              icon={<PlusCircleOutlined />}
              disabled={functionOb[functionCodeConstants.TD_DM_DP_CREATE] ? false : true}
              type="primary"
              onClick={onCreate}
            >
              Thêm mới
            </Button>
          </div>
          <div style={{ clear: 'both' }}></div>
        </>
      )}
    >
      <Table.Column<Street>
        width={50}
        title="STT"
        align="center"
        render={(value, item, index) => (page - 1) * pageSize + index + 1}
      />
      <Table.Column<Street> width={120} title="Mã đường phố" align="center" render={(_, { code }) => code} />
      <Table.Column<Street> width={200} title="Tên đường phố" render={(_, { name }) => name} />
      <Table.Column<Street>
        width={150}
        align="center"
        title="Quận/Huyện"
        render={(_, { districtName }) => districtName}
      />
      <Table.Column<Street> width={150} align="center" title="Tỉnh/TP" render={(_, { provinceName }) => provinceName} />
      <Table.Column<Street> width={200} align="center" title="Cung đường" render={(_, { routeName }) => routeName} />
      <Table.Column<Street>
        width={100}
        title="Trạng thái"
        align="center"
        render={(_, { status }) =>
          status === 1 ? (
            <Tag color="#2db7f5">Kích hoạt</Tag>
          ) : status === 0 ? (
            <Tag color="red">Đã xóa</Tag>
          ) : (
            <Tag color="#f50">Không kích hoạt</Tag>
          )
        }
      />
      <Table.Column<Street> width={120} title="Ngày tạo" align="center" render={(_, { createDate }) => createDate} />
      <Table.Column<Street> width={150} title="Người tạo" align="center" render={(_, { createUser }) => createUser} />
      <Table.Column<Street> width={120} title="Ngày sửa" align="center" render={(_, { updateDate }) => updateDate} />
      <Table.Column<Street>
        width={150}
        title="Người chỉnh sửa"
        align="center"
        render={(_, { updateUser }) => updateUser}
      />
      <Table.Column<Street>
        width={100}
        title="Hành động"
        align="center"
        fixed="right"
        render={(_, row) => (
          <>
            <Popover trigger="click" placement="topLeft" title="Chọn hành động" content={contentActionButton(row)}>
              <Button icon={<SelectOutlined />} type="primary">
                Hành động
              </Button>
            </Popover>
          </>
        )}
      />
    </Table>
  );
};

export default StreetTable;
