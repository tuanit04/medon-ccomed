import React, { FC, useEffect } from 'react';
import { Form, Input, Col, Row, Select, Button, Badge } from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import { Facility } from 'common/interfaces/facility.interface';
import { Route } from 'common/interfaces/route.interface';
import { SearchOutlined, ClearOutlined } from '@ant-design/icons';
import { Ward } from 'common/interfaces/ward.interface';
import { District } from 'common/interfaces/district.interface';
import { Province } from 'common/interfaces/province.interface';
import { Street } from 'common/interfaces/street.interface';
import { functionCodeConstants } from 'constants/functions';

const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperCol4Modal: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
  wrapperCol4Modal: { span: 18 }
};

interface AddSiteStaffFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?: boolean;
  isCreate?: boolean;
  /** Initial form data */
  street?: Street;
}

export default function useGetSearchForm({
  required = false,
  responsive = false,
  name = 'form',
  isModal,
  isCreate,
  street
}: AddSiteStaffFormProps) {
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<Partial<Street>>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={street}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };
  useEffect(() => {
    formInstance.resetFields();
  }, [formInstance, street]);

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Don vi
  interface FacilityParentProps {
    facilityParentList: Facility[];
    handleChangeFacilityParent: (parentId: any) => void;
    isSearch?: boolean;
  }

  const FacilityParent: FC<FacilityParentProps> = ({ facilityParentList, handleChangeFacilityParent, isSearch }) => {
    const onChange = (value: string) => {
      formInstance.setFieldsValue({
        routeId: undefined
      });
      handleChangeFacilityParent(value);
    };

    const facilityParent = (
      <Form.Item
        name="parentFacilityId"
        label="Đơn vị"
        className={isModal ? '' : ''}
        rules={[
          {
            required: isModal,
            message: 'Đơn vị không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={onChange}
          defaultValue=""
          allowClear
        >
          {isSearch ? (
            <Select.Option key="" value="">
              -- Tất cả --
            </Select.Option>
          ) : (
            <Select.Option key="" value="">
              -- Chọn Đơn vị --
            </Select.Option>
          )}
          {facilityParentList?.map(option => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? (
      <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{facilityParent}</Col>
    ) : (
      facilityParent
    );
  };

  //Van phong
  interface FacilityProps {
    facilityList: Facility[];
    handleChangeFacility: (id: any) => void;
    isSearch?: boolean;
  }
  const Facility: FC<FacilityProps> = ({ facilityList, handleChangeFacility, isSearch }) => {
    const facility = (
      <Form.Item
        name="facilityId"
        label="Văn phòng"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select
          showSearch
          className={isModal ? '' : ''}
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeFacility}
          defaultValue=""
          allowClear
        >
          {isSearch ? (
            <Select.Option value="">-- Tất cả --</Select.Option>
          ) : (
            <Select.Option value="">-- Chọn Văn phòng --</Select.Option>
          )}
          {facilityList?.map(option => (
            <Select.Option key={option.id} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{facility}</Col> : facility;
  };

  //Trang thai
  interface StatusProps {
    handleChangeStatus?: (value: any) => void;
    isSearch?: boolean;
  }
  const Status: FC<StatusProps> = ({ handleChangeStatus, isSearch }) => {
    const status = (
      <Form.Item
        name="status"
        label="Trạng thái"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select
          disabled={isModal && !isCreate}
          defaultValue={isSearch ? '' : 1}
          onChange={handleChangeStatus}
          className={isModal ? '' : ''}
        >
          <Select.Option value="">
            <Badge color="blue" status="processing" text="-- Tất cả --" />
          </Select.Option>
          <Select.Option value={1}>
            <Badge color="green" status="processing" text="Kích hoạt" />
          </Select.Option>
          <Select.Option value={2}>
            <Badge color="red" status="processing" text="Không kích hoạt" />
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{status}</Col> : status;
  };
  //Ma duong pho
  interface StreetCodeProps {
    //streetCodeValue: string;
  }
  const StreetCode: FC<StreetCodeProps> = () => {
    const streetCode = (
      <Form.Item
        rules={[
          {
            required: isModal,
            pattern: new RegExp(/^[a-zA-Z0-9@~`!@#$%^&*()_=+\\\\';:\"\\/?>.<,-]+$/i),
            message: 'Mã đường phố không hợp lệ (In hoa, không dấu cách, không dấu, tối đa 20 ký tự)'
          }
        ]}
        name="code"
        label="Mã đường phố"
      >
        <Input
          style={{ textTransform: 'uppercase' }}
          disabled={isModal && !isCreate}
          maxLength={50}
          className=""
          placeholder="Nhập mã đường phố"
        />
      </Form.Item>
    );

    return responsive ? (
      <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{streetCode}</Col>
    ) : (
      streetCode
    );
  };

  //Ten duong pho
  interface StreetNameProps {
    //streetName: string;
  }
  const StreetName: FC<StreetNameProps> = () => {
    const streetName = (
      <Form.Item
        rules={[
          {
            required: isModal,
            //pattern: new RegExp(/^([^0-9]*)$/),
            message: 'Tên đường phố không được để trống, không chứa ký tự số.'
          }
        ]}
        name="name"
        label="Tên đường phố"
      >
        <Input required={true} className="" placeholder="Nhập tên đường phố" />
      </Form.Item>
    );

    return responsive ? (
      <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{streetName}</Col>
    ) : (
      streetName
    );
  };

  //Tinh/TP
  interface ProvincesProps {
    provinceList: Province[];
    handleChangeProvince: (provinceId: string) => void;
  }
  const Provinces: FC<ProvincesProps> = ({ provinceList, handleChangeProvince }) => {
    const onChange = (value: string) => {
      formInstance.setFieldsValue({
        districtId: undefined,
        wardId: undefined
      });
      handleChangeProvince(value);
    };

    const provinces = (
      <Form.Item
        required={true}
        name="provinceId"
        label="Tỉnh/TP"
        rules={[
          {
            required: true,
            message: 'Tỉnh/TP không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={onChange}
          placeholder="-- Chọn Tỉnh/TP --"
          allowClear
        >
          {provinceList?.map(province => (
            <Select.Option key={province.id} value={province.id}>
              {province.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{provinces}</Col> : provinces;
  };

  //Quan/Huyen
  interface DistrictProps {
    districtList: District[];
    handleChangeDistrict: (districtId?: string) => void;
  }
  const Districts: FC<DistrictProps> = ({ districtList, handleChangeDistrict }) => {
    const onChange = (value: string) => {
      formInstance.setFieldsValue({
        wardId: undefined
      });
      handleChangeDistrict(value);
    };

    const districts = (
      <Form.Item
        required={true}
        name="districtId"
        label="Quận/Huyện"
        rules={[
          {
            required: true,
            message: 'Quận/Huyện không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={onChange}
          placeholder="-- Chọn Quận/Huyện --"
          allowClear
        >
          {districtList?.map(district => (
            <Select.Option key={district.id} value={district.id}>
              {district.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{districts}</Col> : districts;
  };
  //Phuong/Xa
  interface WardProps {
    wardList: Ward[];
    handleChangeWard: (wardId?: string) => void;
  }
  const Wards: FC<WardProps> = ({ wardList, handleChangeWard }) => {
    const wards = (
      <Form.Item
        required={true}
        name="wardId"
        label="Phường/Xã"
        rules={[
          {
            required: true,
            message: 'Phường/Xã không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeWard}
          placeholder="-- Chọn Phường/Xã --"
          allowClear
        >
          <Select.Option key="" value="">
            -- Chọn Phường/Xã --
          </Select.Option>
          {wardList?.map(ward => (
            <Select.Option key={ward.id} value={ward.id}>
              {ward.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{wards}</Col> : wards;
  };

  //Ma cung duong
  interface RouteProps {
    routes: Route[];
    handleChange: (routeId: string) => void;
  }
  const Route: FC<RouteProps> = ({ routes, handleChange }) => {
    const route = (
      <Form.Item
        required={true}
        name="routeId"
        label="Cung đường"
        rules={[
          {
            required: true,
            message: 'Cung đường không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          defaultValue=""
          allowClear
        >
          <Select.Option key="" value="">
            -- Chọn Cung đường --
          </Select.Option>
          {routes?.map(option => (
            <Select.Option key={option.code} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{route}</Col> : route;
  };
  //Buttons
  interface ButtonsProps {
    functionOb: any;
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ functionOb, onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_DP_TIMKIEM_THONGKE] ? false : true}
          onClick={() => {
            onSearch();
          }}
          icon={<SearchOutlined />}
          type="primary"
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_DP_TIMKIEM_THONGKE] ? false : true}
          onClick={resetFields}
          icon={<ClearOutlined />}
        >
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };

  //Blank component
  const BlankComponent: FC = () => {
    const blankComponent = <Form.Item></Form.Item>;
    return responsive ? <Col {...wrapperCol}>{blankComponent}</Col> : blankComponent;
  };

  return {
    form: formInstance,
    Form: WrappedForm,
    Route,
    FacilityParent,
    Facility,
    Status,
    StreetCode,
    Wards,
    Districts,
    Provinces,
    StreetName,
    Buttons,
    BlankComponent
  };
}
