import React, { FC, useState } from 'react';
import './index.less';
import StreetTable from './streetTable';
import StreetSearchForm from './streetSearchForm';
import {
  useCreateStreet,
  useDeleteStreet,
  useActiveStreet,
  useGetStreetDetail,
  useInactiveStreet,
  useUpdateStreet
} from 'hooks/street';
import { useGetFacilityHome } from 'hooks/facility';
import { useGetRoutes } from 'hooks/route/useGetRoute';
import { Street } from 'common/interfaces/street.interface';
import StreetModifyDialog from './streetModifyDialog';
import StreetCreateDialog from './streetCreateDialog';
import { useAppState } from 'helpers';

const StreetPage: FC = () => {
  const functionOb = useAppState(state => state.user.functionObject);
  const [mcreateVisible, setCreateVisible] = useState(false);
  const [modifyVisible, setModifyVisible] = useState(false);
  const [authorizeVisible, setAuthorizeVisible] = useState(false);
  const [streetIdRes, setStreetIdRes] = useState('');
  const [nameSearch, setNameSearch] = useState({
    id: 'name',
    value: '',
    operation: '~'
  });
  const [codeSearch, setCodeSearch] = useState({
    id: 'code',
    value: '',
    operation: '~'
  });
  const [routeSearch, setRouteSearch] = useState({
    id: 'routeId',
    value: '',
    operation: '=='
  });
  const [parentFacilityIdSearch, setParentFacilityIdSearch] = useState({
    id: 'parentFacilityId',
    value: '',
    operation: '=='
  });
  const [facilityIdSearch, setFacilityIdSearch] = useState({
    id: 'facilityId',
    value: '',
    operation: '=='
  });
  const [statusSearch, setStatusSearch] = useState({
    id: 'status',
    value: '',
    operation: '=='
  });
  const facilityParentList = useGetFacilityHome();
  const { pagingRoutes, isLoading, error, refetchRoutes } = useGetRoutes({ page: 1 });
  //lắng nghe Id trả về
  const onSetIdRes = id => {
    setStreetIdRes(id);
  };
  //Xử lý thêm mới Street
  const { createStreet, resultCreateStreet, isLoadingCreateStreet, errorCreateStreet } = useCreateStreet(onSetIdRes);
  //Xử lý cập nhật Street
  const { updateStreet, resultUpdateStreet, isLoadingUpdateStreet, errorUpdateStreet } = useUpdateStreet(onSetIdRes);
  const handleUpdateStreet = async values => {
    await updateStreet(values);
  };
  //Xử lý xoá Street
  const { deleteStreet, isLoading: isLoadingDelete, result: resultDelete, error: errorDelete } = useDeleteStreet();
  const handleDeleteStreet = async id => {
    await deleteStreet({ id });
  };
  //Lấy chi tiết Street từ API
  const { loadStreetDetail, isLoadingStreetDetail, streetDetail } = useGetStreetDetail();
  //Lấy chi tiết Street từ table, lưu vào state
  const [street, setStreet] = useState({} as Street);
  //Xử lý Active Street
  const { activeStreet, resultActiveStreet, isLoadingActiveStreet, errorActiveStreet } = useActiveStreet();
  const handleActiveStreet = async id => {
    await activeStreet({ id });
  };
  //Xử lý InActive Street
  const { inactiveStreet, resultInactiveStreet, isLoadingInactiveStreet, errorInactiveStreet } = useInactiveStreet();
  const handleInactiveStreet = async id => {
    await inactiveStreet({ id });
  };
  const onResetStreetIdRes = () => {
    setStreetIdRes('');
  };
  return (
    <div className="main-content">
      <StreetSearchForm
        functionOb={functionOb}
        facilityParentList={facilityParentList}
        routeList={pagingRoutes.data}
        onSearch={value => {
          if (value['name']) {
            setNameSearch({ ...nameSearch, value: value['name'] });
          } else if (value['name'] === '') {
            setNameSearch({ ...nameSearch, value: '' });
          }
          if (value['code']) {
            setCodeSearch({ ...codeSearch, value: value['code'] });
          } else if (value['code'] === '') {
            setCodeSearch({ ...codeSearch, value: '' });
          }
          if (value['routeId']) {
            setRouteSearch({ ...routeSearch, value: value['routeId'] });
          } else if (value['routeId'] === '') {
            setRouteSearch({ ...routeSearch, value: '' });
          }
          if (value['parentFacilityId']) {
            setParentFacilityIdSearch({ ...parentFacilityIdSearch, value: value['parentFacilityId'] });
          } else if (value['parentFacilityId'] === '') {
            setParentFacilityIdSearch({ ...parentFacilityIdSearch, value: '' });
          }
          if (value['facilityId']) {
            setFacilityIdSearch({ ...facilityIdSearch, value: value['facilityId'] });
          } else if (value['facilityId'] === '') {
            setFacilityIdSearch({ ...facilityIdSearch, value: '' });
          }
          if (value['status']) {
            setStatusSearch({ ...statusSearch, value: value['status'] });
          } else if (value['status'] === '') {
            setStatusSearch({ ...statusSearch, value: '' });
          }
        }}
      />
      <StreetTable
        functionOb={functionOb}
        onCreate={() => setCreateVisible(true)}
        onAuthorize={value => {
          setStreet(value);
          setAuthorizeVisible(true);
        }}
        onModify={(street: Street) => {
          setStreet(street);
          setModifyVisible(true);
        }}
        onDelete={async id => {
          await handleDeleteStreet(id);
        }}
        onActiveStreet={async id => {
          await handleActiveStreet(id);
        }}
        onInactiveStreet={async id => {
          await handleInactiveStreet(id);
        }}
        nameSearch={nameSearch}
        codeSearch={codeSearch}
        routeSearch={routeSearch}
        parentFacilityIdSearch={parentFacilityIdSearch}
        facilityIdSearch={facilityIdSearch}
        statusSearch={statusSearch}
        streetIdRes={streetIdRes}
        onResetStreetIdRes={onResetStreetIdRes}
      />
      <StreetModifyDialog
        visible={modifyVisible}
        street={street}
        onCancel={() => setModifyVisible(false)}
        onModify={async value => {
          const { id, code, name, provinceId, districtId, wardId, routeId } = value;
          await handleUpdateStreet({ data: { id, code, name, provinceId, districtId, wardId, routeId } });
          setModifyVisible(false);
        }}
      />
      <StreetCreateDialog
        visible={mcreateVisible}
        onCancel={() => setCreateVisible(false)}
        onCreate={(value: Street) => {
          if (value.status === undefined) {
            value.status = 1;
          }
          const { code, name, provinceId, districtId, wardId, routeId, status } = value;
          code.toUpperCase();
          createStreet({ data: { code, name, provinceId, districtId, wardId, routeId, status } });
          setCreateVisible(false);
        }}
      />
    </div>
  );
};
export default StreetPage;
