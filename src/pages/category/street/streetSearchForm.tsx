import React, { FC, useState } from 'react';
import { Role } from 'interface/permission/role.interface';
import useGetStreetForm from './useGetStreetForm';
import { Facility } from 'common/interfaces/facility.interface';
import { Route } from 'common/interfaces/route.interface';
import { useGetFacilitys } from 'hooks/facility';

export interface Values extends Role {}

interface StreetSearchProps {
  functionOb: any;
  facilityParentList: Facility[];
  routeList: Route[];
  onSearch: ({}) => void;
}

const StreetSearchForm: FC<StreetSearchProps> = ({ functionOb, facilityParentList, routeList, onSearch }) => {
  const [routeId, setRouteId] = useState('');
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  const { Form, form, StreetName, StreetCode, Route, Facility, FacilityParent, Status, Buttons } = useGetStreetForm({
    name: 'searchForm',
    responsive: true
  });

  return (
    <Form>
      <StreetCode />
      <StreetName />
      <FacilityParent
        facilityParentList={facilityParentList}
        handleChangeFacilityParent={(parentId: any) => {
          loadFacilities({
            variables: {
              page: 1,
              filtered: [
                {
                  id: 'parentId',
                  value: parentId,
                  operation: '=='
                },
                {
                  id: 'status',
                  value: '1',
                  operation: '=='
                }
              ]
            }
          });
        }}
        isSearch={true}
      />
      <Facility
        handleChangeFacility={data => {
          if (data === '0') {
            data = '';
          }
        }}
        facilityList={facilities}
        isSearch={true}
      />
      <Status
        handleChangeStatus={data => {
          if (data === '0') {
            data = '';
          }
        }}
        isSearch={true}
      />
      <Buttons
        functionOb={functionOb}
        onSearch={() => {
          let name = form.getFieldValue('name');
          let code = form.getFieldValue('code');
          let parentFacilityId = form.getFieldValue('parentFacilityId');
          let facilityId = form.getFieldValue('facilityId');
          let status = form.getFieldValue('status');
          onSearch({ name, code, routeId, parentFacilityId, facilityId, status });
        }}
        resetFields={() => {
          form.resetFields();
          onSearch({ name: '', code: '', routeId: '', parentFacilityId: '', facilityId: '', status: '' });
        }}
      />
    </Form>
  );
};

export default StreetSearchForm;
