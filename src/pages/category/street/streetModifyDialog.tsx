import React, { FC, useEffect, useState } from 'react';
import { Modal } from 'antd';
import useGetStreetForm from './useGetStreetForm';
import { useGetFacilityHome, useGetFacilitys } from 'hooks/facility/useGetFacility';
import { Street } from 'common/interfaces/street.interface';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { useGetWards } from 'hooks/ward/useGetWard';
import { useGetRoutesLazy } from 'hooks/route';

interface StreetModifyDialogProps {
  visible: boolean;
  street: Street;
  onModify: (values: Street) => void;
  onCancel: () => void;
}

const StreetModifyDialog: FC<StreetModifyDialogProps> = ({ onModify, onCancel, visible, street }) => {
  const facilityParentList = useGetFacilityHome();
  const [parentFacilityId, setParentFacilityId] = useState('');
  const [provinceId, setProvinceId] = useState(street?.provinceId);
  const [districtId, setDistrictId] = useState(street?.districtId);
  const [wardId, setWardId] = useState(street?.wardId);
  //Hook Lấy danh sách Đơn vị
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  //Lấy danh sách cung đường
  //Hook Lấy danh sách Cung đường
  const { routesData, isLoadingRoutes, loadRoutes } = useGetRoutesLazy();
  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //Hook Lấy danh sách Quận/Huyện
  const { districts, isLoadingDistricts, loadDistricts } = useGetDistricts();
  //Hook Lấy danh sách Xã/Phường
  const { wards, isLoadingWards, loadWards } = useGetWards();

  useEffect(() => {
    loadProvinces();
    loadFacilities({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'parentId',
            value: street?.parentFacilityId,
            operation: '=='
          }
        ]
      }
    });
    loadDistricts({
      variables: {
        provinceId: street?.provinceId
      }
    });
    loadWards({
      variables: {
        districtId: street?.districtId
      }
    });
    if (street?.parentFacilityId !== null) {
      if (street?.facilityId !== null) {
        loadRoutes({
          variables: {
            page: 1,
            filtered: [
              {
                id: 'parentFacilityId',
                value: street?.parentFacilityId,
                operation: '=='
              },
              {
                id: 'facilityId',
                value: street?.facilityId,
                operation: '=='
              }
            ]
          }
        });
      } else if (street?.facilityId === null) {
        loadRoutes({
          variables: {
            page: 1,
            filtered: [
              {
                id: 'parentFacilityId',
                value: street?.parentFacilityId,
                operation: '=='
              }
            ]
          }
        });
      }
    } else if (street?.parentFacilityId === null) {
      if (street?.facilityId !== null) {
        loadRoutes({
          variables: {
            page: 1,
            filtered: [
              {
                id: 'facilityId',
                value: street?.facilityId,
                operation: '=='
              }
            ]
          }
        });
      }
    }
  }, [street, street?.provinceId, street?.districtId]);

  const {
    form,
    Form,
    StreetCode,
    StreetName,
    Provinces,
    Districts,
    FacilityParent,
    Facility,
    Route,
    Status
  } = useGetStreetForm({
    name: 'searchForm',
    responsive: true,
    isModal: true,
    street
  });
  const onSubmit = async () => {
    const valuesUpdate: any = await form.validateFields();
    delete valuesUpdate['status'];
    valuesUpdate['id'] = street.id;
    onModify(valuesUpdate);
  };

  return (
    <Modal
      maskClosable={false}
      width={900}
      title="Cập nhật Đường phố"
      visible={visible}
      onOk={onSubmit}
      onCancel={onCancel}
    >
      <Form style={{ marginTop: '5px' }}>
        <StreetCode />
        <StreetName />
        <Provinces
          handleChangeProvince={provinceId => {
            loadDistricts({
              variables: {
                provinceId: provinceId
              }
            });
          }}
          provinceList={provinces}
        />
        <Districts
          handleChangeDistrict={districtId => {
            loadWards({
              variables: {
                districtId: districtId
              }
            });
          }}
          districtList={districts}
        />
        {/* <Wards handleChangeWard={wardId => {}} wardList={wards} /> */}
        <FacilityParent
          facilityParentList={facilityParentList}
          handleChangeFacilityParent={(parentFacilityId: any) => {
            setParentFacilityId(parentFacilityId);
            if (parentFacilityId !== '0') {
              form.setFieldsValue({
                facilityId: ''
              })
              loadFacilities({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentId',
                      value: parentFacilityId,
                      operation: '=='
                    },
                    {
                      id: 'status',
                      value: '1',
                      operation: '=='
                    }
                  ]
                }
              });
              loadRoutes({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentFacilityId',
                      value: parentFacilityId,
                      operation: '=='
                    }
                  ]
                }
              });
            }
          }}
        />
        <Facility
          facilityList={facilities}
          handleChangeFacility={(facilityId: any) => {
            if (facilityId !== '') {
              loadRoutes({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentFacilityId',
                      value: parentFacilityId,
                      operation: '=='
                    },
                    {
                      id: 'facilityId',
                      value: facilityId,
                      operation: '=='
                    }
                  ]
                }
              });
            } else {
              loadRoutes({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentFacilityId',
                      value: parentFacilityId,
                      operation: '=='
                    }
                  ]
                }
              });
            }
          }}
        />
        <Route routes={routesData} handleChange={() => {}} />
        <Status />
      </Form>
    </Modal>
  );
};

export default StreetModifyDialog;
