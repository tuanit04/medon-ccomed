import React, { FC, useEffect } from 'react';
import { Modal, Spin } from 'antd';
import useGetRouteForm from './useGetRouteForm';
import { Facility } from 'common/interfaces/facility.interface';
import { Route } from 'common/interfaces/route.interface';
import { useGetFacilitys } from 'hooks/facility/useGetFacility';
import MyLoader from 'utils/LoadingOverlay';

interface RouteCreateDialogProps {
  facilityParentList: Facility[];
  visible: boolean;
  onCreate: (values: Route) => void;
  onCancel: () => void;
}

const RouteCreateDialog: FC<RouteCreateDialogProps> = ({ onCreate, onCancel, visible, facilityParentList }) => {
  const { form, Form, RouteName, RouteCode, FacilityParent, Facility, Status } = useGetRouteForm({
    name: 'searchForm',
    responsive: true,
    isModal: true,
    isCreate: true
  });
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  useEffect(() => {
    form.resetFields();
  }, [visible]);
  return (
    <Modal
      maskClosable={false}
      width={900}
      title="Thêm mới Cung đường"
      visible={visible}
      onOk={async () => onCreate((await form.validateFields()) as any)}
      onCancel={onCancel}
    >
      <Form>
        <RouteCode />
        <RouteName />
        <FacilityParent
          facilityParentList={facilityParentList}
          handleChangeFacilityParent={(parentId: any) => {
            if (parentId !== '0') {
              form.setFieldsValue({
                facilityId: ''
              })
              loadFacilities({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentId',
                      value: parentId,
                      operation: '=='
                    },
                    // {
                    //   id: 'isHome',
                    //   value: "1",
                    //   operation: '=='
                    // },
                    {
                      id: 'status',
                      value: "1",
                      operation: '=='
                    }
                  ]
                }
              });
            }
          }}
        />
        <Facility facilityList={facilities} handleChangeFacility={() => {}} />
        <Status />
      </Form>
    </Modal>
  );
};

export default RouteCreateDialog;
