import React, { FC, useEffect } from 'react';
import { Form, Input, Col, Row, Select, Button, Badge } from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import { Facility } from 'common/interfaces/facility.interface';
import { SearchOutlined, ClearOutlined } from '@ant-design/icons';
import { Route } from 'common/interfaces/route.interface';
import { functionCodeConstants } from 'constants/functions';

const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 8,
  xl: 8,
  xxl: 8
};

const wrapperCol4Modal: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const layout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 17 }
};

interface RouteFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?: boolean;
  /** Initial form data */
  route?: Route;
  isCreate?: boolean;
}

export default function useGetRouteForm({
  required = false,
  responsive = false,
  name = 'form',
  isModal,
  route,
  isCreate
}: RouteFormProps) {
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<Partial<Route>>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState(state => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={route}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };
  useEffect(() => {
    formInstance.resetFields();
  }, [formInstance, route]);

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;
  //Ten cung duong
  const RouteName: FC = () => {
    const routeName = (
      <Form.Item
        rules={[
          {
            required: isModal,
            message: 'Tên cung đường không được để trống.'
          }
        ]}
        name="name"
        label="Tên cung đường"
      >
        <Input placeholder="Nhập tên cung đường" />
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{routeName}</Col> : routeName;
  };

  //Ma cung duong
  const RouteCode: FC = () => {
    const routeCode = (
      <Form.Item
        rules={[
          {
            required: isModal,
            pattern: new RegExp(/^[a-zA-Z0-9@~`!@#$%^&*()_=+\\\\';:\"\\/?>.<,-]+$/i),
            message: 'Mã cung đường không hợp lệ (In hoa, không dấu cách, không dấu, tối đa 20 ký tự)'
          }
        ]}
        name="code"
        label="Mã cung đường"
      >
        <Input
          style={{ textTransform: 'uppercase' }}
          maxLength={20}
          disabled={isModal && !isCreate}
          placeholder="Nhập mã cung đường"
        />
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{routeCode}</Col> : routeCode;
  };

  //Don vi
  interface FacilityParentProps {
    facilityParentList: Facility[];
    handleChangeFacilityParent: (parentId: any) => void;
    isSearch?: boolean;
  }

  const FacilityParent: FC<FacilityParentProps> = ({ facilityParentList, handleChangeFacilityParent, isSearch }) => {
    const facilityParent = (
      <Form.Item
        name="parentFacilityId"
        rules={[
          {
            required: true,
            message: 'Đơn vị không được để trống.'
          }
        ]}
        label="Đơn vị"
        // className={isModal ? '' : 'slcSearch'}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeFacilityParent}
          defaultValue=""
        >
          {isSearch ? (
            <Select.Option key="" value="">
              -- Tất cả --
            </Select.Option>
          ) : (
            <Select.Option key="" value="">
              -- Chọn Đơn vị --
            </Select.Option>
          )}
          {facilityParentList?.map(option => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? (
      <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{facilityParent}</Col>
    ) : (
      facilityParent
    );
  };

  //Van phong
  interface FacilityProps {
    facilityList: Facility[];
    handleChangeFacility: (id: any) => void;
    isSearch?: boolean;
  }
  const Facility: FC<FacilityProps> = ({ facilityList, handleChangeFacility, isSearch }) => {
    const facility = (
      <Form.Item
        name="facilityId"
        label="Văn phòng"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select
          showSearch
          // className={isModal ? '' : 'slcSearch'}
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeFacility}
          defaultValue=""
        >
          {isSearch ? (
            <Select.Option value="">-- Tất cả --</Select.Option>
          ) : (
            <Select.Option value="">-- Chọn Văn phòng --</Select.Option>
          )}
          {facilityList?.map(option => (
            <Select.Option key={option.id} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{facility}</Col> : facility;
  };

  //Trang thai
  interface StatusProps {
    handleChangeStatus?: (value: any) => void;
    isSearch?: boolean;
  }
  const Status: FC<StatusProps> = ({ handleChangeStatus, isSearch }) => {
    const status = (
      <Form.Item
        name="status"
        label="Trạng thái"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select disabled={isModal && !isCreate} defaultValue={isSearch ? '' : 1} onChange={handleChangeStatus}>
          {isSearch ? (
            <Select.Option value="">
              <Badge color="blue" status="processing" text=" -- Tất cả --" />
            </Select.Option>
          ) : (
            ''
          )}
          <Select.Option value={1}>
            <Badge color="green" status="processing" text="Kích hoạt" />
          </Select.Option>
          <Select.Option value={2}>
            <Badge color="red" status="processing" text="Không kích hoạt" />
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{status}</Col> : status;
  };

  //Buttons
  interface ButtonsProps {
    functionOb: any;
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ functionOb, onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_CD_TIMKIEM_THONGKE] ? false : true}
          onClick={() => {
            onSearch();
          }}
          icon={<SearchOutlined />}
          type="primary"
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_CD_TIMKIEM_THONGKE] ? false : true}
          onClick={resetFields}
          icon={<ClearOutlined />}
        >
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };

  return {
    form: formInstance,
    Form: WrappedForm,
    RouteName,
    RouteCode,
    FacilityParent,
    Facility,
    Status,
    Buttons
  };
}
