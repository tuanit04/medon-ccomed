import React, { FC, useState } from 'react';
import { Role } from 'interface/permission/role.interface';
import useGetRouteForm from './useGetRouteForm';
import { Facility } from 'common/interfaces/facility.interface';
import { useGetFacilitys } from 'hooks/facility';

export interface Values extends Role {}

interface RouteSearchProps {
  functionOb: any;
  facilityParentList: Facility[];
  onSearch: ({}) => void;
}

const RouteSearchForm: FC<RouteSearchProps> = ({ functionOb, facilityParentList, onSearch }) => {
  const [parentFacilityId, setParentFacilityId] = useState('');
  const [facilityId, setFacilityId] = useState('');
  const [status, setStatus] = useState('');
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  const { Form, form, RouteName, RouteCode, Facility, FacilityParent, Status, Buttons } = useGetRouteForm({
    name: 'searchForm',
    responsive: true
  });

  return (
    <Form>
      <RouteCode />
      <RouteName />
      <FacilityParent
        facilityParentList={facilityParentList}
        handleChangeFacilityParent={(parentId: any) => {
          //setParentFacilityId(parentId);
          loadFacilities({
            variables: {
              page: 1,
              filtered: [
                {
                  id: 'parentId',
                  value: parentId,
                  operation: '=='
                },
                // {
                //   id: 'isHome',
                //   value: "1",
                //   operation: '=='
                // }
                // ,
                {
                  id: 'status',
                  value: '1',
                  operation: '=='
                }
              ]
            }
          });
        }}
        isSearch={true}
      />
      <Facility
        handleChangeFacility={data => {
          if (data === '0') {
            data = '';
          }
          //setFacilityId(data);
        }}
        facilityList={facilities}
        isSearch={true}
      />
      <Status
        handleChangeStatus={data => {
          setStatus(data);
        }}
        isSearch={true}
      />
      <Buttons
        functionOb={functionOb}
        onSearch={() => {
          let name = form.getFieldValue('name');
          let code = form.getFieldValue('code');
          let parentFacilityId = form.getFieldValue('parentFacilityId');
          let facilityId = form.getFieldValue('facilityId');
          let status = form.getFieldValue('status');
          onSearch({ name, code, parentFacilityId, facilityId, status });
        }}
        resetFields={() => {
          form.resetFields();
          onSearch({ name: '', code: '', parentFacilityId: '', facilityId: '', status: '' });
        }}
      />
    </Form>
  );
};

export default RouteSearchForm;
