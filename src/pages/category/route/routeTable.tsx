import React, { FC, useEffect, useState } from 'react';
import { Button, Table, Modal, Divider, Popover, Tag } from 'antd';
import { CheckCircleOutlined, ExclamationCircleOutlined, MinusCircleOutlined } from '@ant-design/icons';
import Moment from 'moment';
import { Role } from 'interface/permission/role.interface';
import { PlusCircleOutlined, DeleteOutlined, EditOutlined, SelectOutlined } from '@ant-design/icons';
import { Route } from 'common/interfaces/route.interface';
import { useGetRoutes } from 'hooks/route/useGetRoute';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { useFindRouteStatistic } from 'hooks/route';
import { openNotificationRight } from 'utils/notification';
import { functionCodeConstants } from 'constants/functions';

interface RouteTableProps {
  functionOb: any;
  onCreate: () => void;
  onModify: (route: Route) => void;
  onDelete: (routeId: string) => void;
  onActiveRoute: (value: string) => void;
  onInactiveRoute: (value: string) => void;
  onAuthorize: (row: Role) => void;
  nameSearch: FilteredInput;
  codeSearch: FilteredInput;
  parentFacilityIdSearch: FilteredInput;
  facilityIdSearch: FilteredInput;
  statusSearch: FilteredInput;
  routeIdRes: string;
  onResetRouteIdRes: () => void;
}

const RouteTable: FC<RouteTableProps> = ({
  functionOb,
  onCreate,
  onModify,
  onDelete,
  onActiveRoute,
  onInactiveRoute,
  onAuthorize,
  nameSearch,
  codeSearch,
  parentFacilityIdSearch,
  facilityIdSearch,
  statusSearch,
  routeIdRes,
  onResetRouteIdRes
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(20);
  let filtered: FilteredInput[] = [];
  if (nameSearch['value'] !== '') {
    filtered.push(nameSearch);
  }
  if (codeSearch['value'] !== '') {
    filtered.push(codeSearch);
  }
  if (parentFacilityIdSearch['value'] !== '') {
    filtered.push(parentFacilityIdSearch);
  }
  if (facilityIdSearch['value'] !== '') {
    filtered.push(facilityIdSearch);
  }
  if (statusSearch['value'] !== '') {
    filtered.push(statusSearch);
  }
  let isCheck = false;
  if (filtered.length === 0) {
    isCheck = true;
  }
  const variable = !isCheck ? { page, pageSize, filtered } : { page, pageSize, filtered };
  const { pagingRoutes, isLoading, error, refetchRoutes } = useGetRoutes(variable);
  const {
    routeStatistic,
    isLoadingRouteStatistic,
    errorRouteStatistic,
    refetchRouteStatistic
  } = useFindRouteStatistic();
  const [pagination, setPagination] = useState({});
  const [visibleModalUpdate, setVisibleModalUpdate] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  useEffect(() => {
    if (!isCheck) {
      setPage(1);
    }
    setPagination({ current: page, pageSize: pageSize, total: pagingRoutes['records'] });
  }, [pagingRoutes]);
  useEffect(() => {
    if (routeIdRes !== '') {
      refetchRoutes();
      refetchRouteStatistic();
    }
    onResetRouteIdRes();
  }, [routeIdRes]);
  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setVisibleModalUpdate(false);
      setConfirmLoading(false);
    }, 2000);
  };

  const handleCancel = () => {
    setVisibleModalUpdate(false);
  };

  const handleActiveRoute = async routeId => {
    await onActiveRoute(routeId);
    refetchRoutes();
    refetchRouteStatistic();
  };

  const handleInactiveRoute = async routeId => {
    await onInactiveRoute(routeId);
    refetchRoutes();
    refetchRouteStatistic();
  };
  const handleDeleteRoute = async routeId => {
    await onDelete(routeId);
    refetchRoutes();
    refetchRouteStatistic();
  };

  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    if (pageSize !== pagination['pageSize']) {
      setPage(1);
    } else {
      setPage(pagination['current']);
    }
    setPageSize(pagination['pageSize']);
  };
  const contentActionButton = route => (
    <div>
      <Button
        type="primary"
        icon={<EditOutlined />}
        style={{ marginLeft: 10 }}
        disabled={functionOb[functionCodeConstants.TD_DM_CD_UPDATE] ? false : true}
        onClick={() => {
          onModify(route);
          /*if (route.status === 2) {
            openNotificationRight('Không thể cập nhật bản ghi có trạng thái là KHÔNG KÍCH HOẠT.');
          } else {
            onModify(route);
          }*/
        }}
      >
        Sửa
      </Button>
      <Button
        disabled={functionOb[functionCodeConstants.TD_DM_CD_ACTIVE_INACTIVE] ? false : true}
        type="primary"
        icon={route.status === 1 ? <MinusCircleOutlined /> : <CheckCircleOutlined />}
        style={{ marginLeft: 10 }}
        onClick={() => {
          Modal.confirm({
            icon: <ExclamationCircleOutlined />,
            title: 'Bạn có chắc chắn muốn cập nhật trạng thái bản ghi có mã cung đường : ' + route.code + ' ?',
            onOk() {
              if (route.status === 2) {
                handleActiveRoute(route.id);
              } else if (route.status === 1) {
                handleInactiveRoute(route.id);
              }
            },
            onCancel() {
              //console.log('Cancel');
            }
          });
        }}
      >
        {route.status === 1 ? 'Hủy kích hoạt' : 'Kích hoạt'}
      </Button>
      <Button
        disabled={functionOb[functionCodeConstants.TD_DM_CD_DELETE] ? false : true}
        danger
        icon={<DeleteOutlined />}
        style={{ marginLeft: 10 }}
        type="primary"
        onClick={() => {
          Modal.confirm({
            icon: <ExclamationCircleOutlined />,
            title: 'Bạn có chắc chắn muốn xóa bản ghi có mã cung đường là : ' + route.code + ' ?',
            onOk() {
              handleDeleteRoute(route.id);
            },
            onCancel() {
              //console.log('Cancel');
            }
          });
        }}
      >
        Xóa
      </Button>
    </div>
  );
  return (
    <Table
      rowKey="id"
      style={{ height: 'calc(100vh - 125px)', overflow: 'hidden' }}
      className="table-route"
      pagination={{
        ...pagination,
        defaultPageSize: 20,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '50']
      }}
      loading={isLoading}
      onChange={handleTableChange}
      bordered
      dataSource={pagingRoutes.data}
      scroll={{ x: true, y: 'calc(100vh - 250px)' }}
      title={() => (
        <>
          <div style={{ float: 'left' }}>
            <label className="lblTableTxt">
              <Tag color="#87d068">Tổng số bản ghi : {routeStatistic['totalRecords']}</Tag>
            </label>
            &nbsp;
            <label className="lblTableTxt">
              <Tag color="#2db7f5">Kích hoạt : {routeStatistic['activeRecords']}</Tag>{' '}
            </label>
            &nbsp;
            <label className="lblTableTxt">
              <Tag color="#f50">Không kích hoạt : {routeStatistic['inactiveRecords']}</Tag>
            </label>
            &nbsp;
          </div>
          <div style={{ float: 'right' }}>
            <Button
              icon={<PlusCircleOutlined />}
              disabled={functionOb[functionCodeConstants.TD_DM_CD_CREATE] ? false : true}
              type="primary"
              onClick={onCreate}
            >
              Thêm mới
            </Button>
          </div>
          <div style={{ clear: 'both' }}></div>
        </>
      )}
    >
      <Table.Column<Route>
        width={50}
        title="STT"
        align="center"
        render={(value, item, index) => (page - 1) * pageSize + index + 1}
      />
      <Table.Column<Route> width={120} title="Mã cung đường" align="center" render={(_, { code }) => code} />
      <Table.Column<Route> width={200} title="Tên cung đường" render={(_, { name }) => name} />
      <Table.Column<Route> width={200} title="Đơn vị" render={(_, { parentFacilityName }) => parentFacilityName} />
      <Table.Column<Route> width={200} title="Văn phòng" render={(_, { facilityName }) => facilityName} />
      <Table.Column<Route>
        width={100}
        title="Trạng thái"
        align="center"
        render={(_, { status }) =>
          status === 1 ? (
            <Tag color="#2db7f5">Kích hoạt</Tag>
          ) : status === 0 ? (
            <Tag color="red">Đã xóa</Tag>
          ) : (
            <Tag color="#f50">Không kích hoạt</Tag>
          )
        }
      />
      <Table.Column<Route>
        width={120}
        title="Ngày tạo"
        align="center"
        render={(_, { createDate }) => (createDate !== null ? Moment(createDate).format('DD/MM/yyyy HH:MM:SS') : '')}
      />
      <Table.Column<Route> width={120} title="Người tạo" align="center" render={(_, { createUser }) => createUser} />
      <Table.Column<Route>
        width={120}
        title="Ngày sửa"
        align="center"
        render={(_, { updateDate }) => (updateDate !== null ? Moment(updateDate).format('DD/MM/yyyy HH:MM:SS') : '')}
      />
      <Table.Column<Route>
        width={120}
        title="Người chỉnh sửa"
        align="center"
        render={(_, { createUser }) => createUser}
      />

      <Table.Column<Route>
        width={100}
        title="Hành động"
        align="center"
        fixed="right"
        render={(_, row) => (
          <Popover trigger="click" placement="topLeft" title="Chọn hành động" content={contentActionButton(row)}>
            <Button icon={<SelectOutlined />} type="primary">
              Hành động
            </Button>
          </Popover>
        )}
      />
    </Table>
  );
};

export default RouteTable;
