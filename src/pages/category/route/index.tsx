import React, { FC, useState } from 'react';
import './index.less';
import RouteTable from './routeTable';
import RouteSearchForm from './routeSearchForm';
import { useGetFacilityHome, useGetFacilityParents } from 'hooks/facility';
import {
  useActiveRoute,
  useCreateRoute,
  useDeleteRoute,
  useGetRouteDetail,
  useInactiveRoute,
  useUpdateRoute
} from 'hooks/route';
import RouteCreateDialog from './routeCreateDialog';
import RouteModifyDialog from './routeModifyDialog';
import { Route } from 'common/interfaces/route.interface';
import { useAppState } from 'helpers';

const RoutePage: FC = () => {
  const functionOb = useAppState(state => state.user.functionObject);
  const facilityParentList = useGetFacilityHome();
  const [mcreateVisible, setCreateVisible] = useState(false);
  const [modifyVisible, setModifyVisible] = useState(false);
  const [authorizeVisible, setAuthorizeVisible] = useState(false);
  const [routeIdRes, setRouteIdRes] = useState('');
  const [values, setValues] = useState({});
  const [nameSearch, setNameSearch] = useState({
    id: 'name',
    value: '',
    operation: '~'
  });
  const [codeSearch, setCodeSearch] = useState({
    id: 'code',
    value: '',
    operation: '~'
  });
  const [parentFacilityIdSearch, setParentFacilityIdSearch] = useState({
    id: 'parentFacilityId',
    value: '',
    operation: '=='
  });
  const [facilityIdSearch, setFacilityIdSearch] = useState({
    id: 'facilityId',
    value: '',
    operation: '=='
  });
  const [statusSearch, setStatusSearch] = useState({
    id: 'status',
    value: '',
    operation: '=='
  });
  //Lắng nghe Id trả về
  const onSetIdRes = id => {
    setRouteIdRes(id);
  };
  //Xoá Cung đường
  const { deleteRoute, isLoading, result, error } = useDeleteRoute();
  const handleDeleteRoute = async id => {
    await deleteRoute({ id });
  };
  //Thêm mới cung đường
  const { createRoute, errorCreateRoute, isLoadingCreateRoute, resultCreateRoute } = useCreateRoute(onSetIdRes);
  //Cập nhật cung đường
  const { updateRoute, errorUpdateRoute, isLoadingUpdateRoute, resultUpdateRoute } = useUpdateRoute(onSetIdRes);
  const handleUpdateRoute = async route => {
    await updateRoute(route);
  };
  //C1. Lấy chi tiết cung đường
  const { loadRouteDetail, isLoadingRouteDetail, routeDetail } = useGetRouteDetail();
  //C2. Lấy chi tiết cung đường từ table lưu vào state
  const [route, setRoute] = useState({} as Route);
  //Xử lý Active Route
  const { activeRoute, resultActiveRoute, isLoadingActiveRoute, errorActiveRoute } = useActiveRoute();
  const handleActiveRoute = async id => {
    await activeRoute({ id });
  };
  //Xử lý Inactive Route
  const { inactiveRoute, resultInactiveRoute, isLoadingInactiveRoute, errorInactiveRoute } = useInactiveRoute();
  const handleInactiveRoute = async id => {
    await inactiveRoute({ id });
  };
  const onResetRouteIdRes = () => {
    setRouteIdRes('');
  };
  return (
    <div className="main-content">
      <RouteSearchForm
        functionOb={functionOb}
        facilityParentList={facilityParentList}
        onSearch={value => {
          if (value['name']) {
            setNameSearch({ ...nameSearch, value: value['name'] });
          } else if (value['name'] === '') {
            setNameSearch({ ...nameSearch, value: '' });
          }
          if (value['code']) {
            setCodeSearch({ ...codeSearch, value: value['code'] });
          } else if (value['code'] === '') {
            setCodeSearch({ ...codeSearch, value: '' });
          }
          if (value['parentFacilityId']) {
            setParentFacilityIdSearch({ ...parentFacilityIdSearch, value: value['parentFacilityId'] });
          } else if (value['parentFacilityId'] === '') {
            setParentFacilityIdSearch({ ...parentFacilityIdSearch, value: '' });
          }
          if (value['facilityId']) {
            setFacilityIdSearch({ ...facilityIdSearch, value: value['facilityId'] });
          } else if (value['facilityId'] === '') {
            setFacilityIdSearch({ ...facilityIdSearch, value: '' });
          }
          if (value['status']) {
            setStatusSearch({ ...statusSearch, value: value['status'] });
          } else if (value['status'] === '') {
            setStatusSearch({ ...statusSearch, value: '' });
          }
        }}
      />
      <RouteTable
        functionOb={functionOb}
        onCreate={() => setCreateVisible(true)}
        onModify={route => {
          //loadRouteDetail({ variables: { routeId } });
          setRoute(route);
          setModifyVisible(true);
        }}
        onDelete={async id => {
          await handleDeleteRoute(id);
        }}
        onActiveRoute={async id => {
          await handleActiveRoute(id);
        }}
        onInactiveRoute={async id => {
          await handleInactiveRoute(id);
        }}
        onAuthorize={values => {
          setValues(values);
          setAuthorizeVisible(true);
        }}
        nameSearch={nameSearch}
        codeSearch={codeSearch}
        parentFacilityIdSearch={parentFacilityIdSearch}
        facilityIdSearch={facilityIdSearch}
        statusSearch={statusSearch}
        routeIdRes={routeIdRes}
        onResetRouteIdRes={onResetRouteIdRes}
      />
      <RouteCreateDialog
        facilityParentList={facilityParentList}
        visible={mcreateVisible}
        onCancel={() => setCreateVisible(false)}
        onCreate={values => {
          values['code'].toUpperCase();
          if (values['status'] === undefined) {
            values['status'] = 1;
          }
          createRoute({ data: { ...values } });
          setCreateVisible(false);
        }}
      />
      <RouteModifyDialog
        route={route}
        facilityParentList={facilityParentList}
        visible={modifyVisible}
        onCancel={() => setModifyVisible(false)}
        onModify={async value => {
          //delete value['__typename'];
          await handleUpdateRoute({ data: { ...value } });
          setModifyVisible(false);
        }}
      />
    </div>
  );
};
export default RoutePage;
