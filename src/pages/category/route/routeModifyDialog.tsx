import React, { FC, useEffect } from 'react';
import { Modal } from 'antd';
import useGetRouteForm from './useGetRouteForm';
import { Facility } from 'common/interfaces/facility.interface';
import { Route } from 'common/interfaces/route.interface';
import { useGetFacilitys } from 'hooks/facility/useGetFacility';

interface RouteModifyDialogProps {
  facilityParentList: Facility[];
  route: Route;
  visible: boolean;
  onModify: (values: Route) => void;
  onCancel: () => void;
}

const RouteModifyDialog: FC<RouteModifyDialogProps> = ({ onModify, onCancel, visible, facilityParentList, route }) => {
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  useEffect(() => {
    loadFacilities({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'parentId',
            value: route?.parentFacilityId,
            operation: '=='
          },
          // {
          //   id: 'isHome',
          //   value: "1",
          //   operation: '=='
          // },
          {
            id: 'status',
            value: '1',
            operation: '=='
          }
        ]
      }
    });
  }, [route]);

  const { form, Form, RouteName, RouteCode, FacilityParent, Facility, Status } = useGetRouteForm({
    name: 'searchForm',
    responsive: true,
    isModal: true,
    route
  });

  const onSubmit = async () => {
    const valuesUpdate: any = await form.validateFields();
    delete valuesUpdate['status'];
    valuesUpdate['id'] = route.id;
    onModify(valuesUpdate);
  };
  return (
    <Modal
      maskClosable={false}
      width={900}
      title="Cập nhật Cung đường"
      visible={visible}
      onOk={onSubmit}
      onCancel={onCancel}
    >
      <Form>
        <RouteCode />
        <RouteName />
        <FacilityParent
          facilityParentList={facilityParentList}
          handleChangeFacilityParent={(parentId: any) => {
            if (parentId !== '0') {
              form.setFieldsValue({
                facilityId: ''
              })
              loadFacilities({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentId',
                      value: parentId,
                      operation: '=='
                    },
                    // {
                    //   id: 'isHome',
                    //   value: "1",
                    //   operation: '=='
                    // },
                    {
                      id: 'status',
                      value: '1',
                      operation: '=='
                    }
                  ]
                }
              });
            }
          }}
        />
        <Facility facilityList={facilities} handleChangeFacility={() => {}} />
        <Status />
      </Form>
    </Modal>
  );
};

export default RouteModifyDialog;
