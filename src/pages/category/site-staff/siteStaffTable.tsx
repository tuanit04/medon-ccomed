import React, { FC, useState, useEffect } from 'react';
import { Button, Table, Tag, Modal, Popover, Dropdown, Menu } from 'antd';
import Moment from 'moment';
import { Role } from 'interface/permission/role.interface';
import {
  CheckCircleOutlined,
  DeleteOutlined,
  SelectOutlined,
  EditOutlined,
  ExclamationCircleOutlined,
  MinusCircleOutlined,
  DownOutlined,
  CloseOutlined,
  PlusCircleOutlined
} from '@ant-design/icons';
import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import { useFindSiteStaffStatistic, useGetSiteStaffs } from 'hooks/site-staff';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { functionCodeConstants } from 'constants/functions';

interface StaffTableProps {
  onModify: (siteStaff: SiteStaff) => void;
  onAuthorize: (row: Role) => void;
  onDelete: (value: string) => void;
  onCreate: () => void;
  onActiveSiteStaff: (value: string) => void;
  onInactiveSiteStaff: (value: string) => void;
  nameSearch: FilteredInput;
  codeSearch: FilteredInput;
  parentFacilityIdSearch: FilteredInput;
  facilityIdSearch: FilteredInput;
  statusSearch: FilteredInput;
  siteStaffIdRes: string;
  functionOb: any;
  phoneSearch: FilteredInput;
  onResetSiteStaffIdRes: () => void;
  fullNameSearch: FilteredInput;
}

const SiteStaffTable: FC<StaffTableProps> = ({
  onModify,
  onAuthorize,
  onDelete,
  onCreate,
  onActiveSiteStaff,
  onInactiveSiteStaff,
  nameSearch,
  codeSearch,
  parentFacilityIdSearch,
  facilityIdSearch,
  statusSearch,
  functionOb,
  siteStaffIdRes,
  phoneSearch,
  fullNameSearch,
  onResetSiteStaffIdRes
}) => {
  const [variable, setVariable] = useState<IGraphQLRequest>({
    page: 1,
    pageSize: 20,
    filtered: []
  });
  const [isVisiablePopover, setVisiablePopover] = useState(true);
  useEffect(() => {
    let filtered: FilteredInput[] = [];
    if (nameSearch['value'] !== '') {
      filtered.push(nameSearch);
    }
    if (codeSearch['value'] !== '') {
      filtered.push(codeSearch);
    }
    if (parentFacilityIdSearch['value'] !== '') {
      filtered.push(parentFacilityIdSearch);
    }
    if (facilityIdSearch['value'] !== '') {
      filtered.push(facilityIdSearch);
    }
    if (statusSearch['value'] !== '') {
      filtered.push(statusSearch);
    }
    if (statusSearch['phone'] !== '') {
      filtered.push(phoneSearch);
    }
    if (fullNameSearch['value'] !== '') {
      filtered.push(fullNameSearch);
    }
    setVariable({
      page: 1,
      pageSize: 20,
      filtered
    });
  }, [nameSearch, codeSearch, parentFacilityIdSearch, facilityIdSearch, statusSearch, statusSearch, fullNameSearch]);

  // const variable = { page, pageSize, filtered };
  const { pagingSiteStaffs, isLoadingSiteStaffs, errorSiteStaffs, refetchSiteStaffs } = useGetSiteStaffs(variable);
  const {
    siteStaffStatistic,
    isLoadingSiteStaffStatistic,
    errorSiteStaffStatistic,
    refetchSiteStaffStatistic
  } = useFindSiteStaffStatistic();

  useEffect(() => {
    if (siteStaffIdRes !== '') {
      refetchSiteStaffs();
    }
    onResetSiteStaffIdRes();
  }, [siteStaffIdRes]);
  const handleDelete = async siteStaff => {
    await onDelete(siteStaff.id);
    refetchSiteStaffs();
    refetchSiteStaffStatistic();
  };
  const handleActiveSiteStaff = async siteStaffId => {
    await onActiveSiteStaff(siteStaffId);
    refetchSiteStaffs();
    refetchSiteStaffStatistic();
  };
  const handleInactiveSiteStaff = async siteStaffId => {
    await onInactiveSiteStaff(siteStaffId);
    refetchSiteStaffs();
    refetchSiteStaffStatistic();
  };

  const contentActionButton = (siteStaff: SiteStaff) => (
    <div>
      <Button
        icon={<EditOutlined />}
        disabled={functionOb[functionCodeConstants.TD_DM_CBTAINHA_UPDATE] ? false : true}
        style={{ marginLeft: 10 }}
        onClick={() => {
          onModify(siteStaff);
          /*if (siteStaff.status === 2) {
            openNotificationRight('Không thể cập nhật bản ghi có trạng thái là KHÔNG KÍCH HOẠT.');
          } else {
            onModify(siteStaff);
          }*/
        }}
      >
        Sửa
      </Button>
      <Button
        disabled={functionOb[functionCodeConstants.TD_DM_CBTAINHA_ACTIVE_INACTIVE] ? false : true}
        type={siteStaff.status === 1 ? 'default' : 'primary'}
        icon={siteStaff.status === 1 ? <MinusCircleOutlined /> : <CheckCircleOutlined />}
        style={{ marginLeft: 10 }}
        onClick={() => {
          Modal.confirm({
            icon: <ExclamationCircleOutlined />,
            title: 'Bạn có chắc chắn muốn cập nhật trạng thái bản ghi có mã nhân viên : ' + siteStaff.code + ' ?',
            onOk() {
              if (siteStaff.status === 2) {
                handleActiveSiteStaff(siteStaff.id);
              } else if (siteStaff.status === 1) {
                handleInactiveSiteStaff(siteStaff.id);
              }
            },
            onCancel() {
              //console.log('Cancel');
            }
          });
        }}
      >
        {siteStaff.status === 1 ? 'Hủy kích hoạt' : 'Kích hoạt'}
      </Button>
      <Button
        danger
        icon={<DeleteOutlined />}
        disabled={functionOb[functionCodeConstants.TD_DM_CBTAINHA_DELETE] ? false : true}
        style={{ marginLeft: 10 }}
        type="primary"
        onClick={() => {
          Modal.confirm({
            icon: <ExclamationCircleOutlined />,
            title: 'Bạn có chắc chắn muốn xóa bản ghi có mã nhân viên : ' + siteStaff.code + ' ?',
            onOk() {
              handleDelete(siteStaff);
            },
            onCancel() {
              //console.log('Cancel');
            }
          });
        }}
      >
        Xóa
      </Button>
    </div>
  );

  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    setVariable(state => ({
      ...state,
      page: pagination['current'],
      pageSize: pagination['pageSize']
    }));
  };

  return (
    <Table
      rowKey="id"
      style={{ height: 'calc(100vh - 124px)', overflow: 'hidden' }}
      className="table-site-staff"
      loading={isLoadingSiteStaffs}
      onChange={handleTableChange}
      pagination={{
        current: variable.page,
        pageSize: variable.pageSize,
        total: pagingSiteStaffs['records'],
        defaultPageSize: 20,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '50']
      }}
      dataSource={pagingSiteStaffs.data}
      scroll={{ x: 450, y: 'calc(100vh - 250px)' }}
      title={() => (
        <>
          <div style={{ float: 'left', marginBottom: '10px', marginTop: '5px' }}>
            <label className="lblTableTxt">
              <Tag color="#87d068">Tổng số bản ghi : {siteStaffStatistic['totalRecords']}</Tag>
            </label>
            &nbsp;
            <label className="lblTableTxt">
              <Tag color="#2db7f5">Kích hoạt : {siteStaffStatistic['activeRecords']}</Tag>{' '}
            </label>
            &nbsp;
            <label className="lblTableTxt">
              <Tag color="#f50">Không kích hoạt : {siteStaffStatistic['inactiveRecords']}</Tag>
            </label>
            &nbsp;
          </div>
          <div style={{ float: 'right' }}>
            <Button
              icon={<PlusCircleOutlined />}
              disabled={functionOb[functionCodeConstants.TD_DM_CBTAINHA_CREATE] ? false : true}
              type="primary"
              onClick={onCreate}
            >
              Thêm mới
            </Button>
          </div>
          <div style={{ clear: 'both' }}></div>
        </>
      )}
    >
      <Table.Column<SiteStaff>
        title="STT"
        width={50}
        align="center"
        render={(value, item, index) => ((variable.page ?? 1) - 1) * (variable.pageSize ?? 20) + index + 1}
      />
      <Table.Column<SiteStaff>
        width={100}
        title="Mã vạch"
        filtered={true}
        align="center"
        render={(_, { code }) => code}
      />
      <Table.Column<SiteStaff> width={250} title="Tên nhân viên" render={(_, { name }) => name} />
      <Table.Column<SiteStaff> width={150} title="User tại nhà" render={(_, { hisUserName }) => hisUserName} />
      <Table.Column<SiteStaff> width={150} title="SĐT" align="center" render={(_, { phone }) => phone} />
      <Table.Column<SiteStaff> width={250} title="Đơn vị" render={(_, { parentFacilityName }) => parentFacilityName} />
      <Table.Column<SiteStaff> width={250} title="Văn phòng" render={(_, { facilityName }) => facilityName} />
      <Table.Column<SiteStaff>
        title="Trạng thái"
        align="center"
        width={150}
        render={(_, { status }) =>
          status === 1 ? (
            <Tag color="#2db7f5">Kích hoạt</Tag>
          ) : status === 0 ? (
            <Tag color="red">Đã xóa</Tag>
          ) : (
            <Tag color="#f50">Không kích hoạt</Tag>
          )
        }
      />
      <Table.Column<SiteStaff> width={200} title="Người chỉnh sửa" render={(_, { updateUser }) => updateUser} />
      <Table.Column<SiteStaff>
        width={150}
        title="Thời gian chỉnh sửa"
        align="center"
        render={(_, { updateDate }) => (updateDate !== null ? Moment(updateDate).format('DD/MM/yyyy') : '')}
      />
      <Table.Column<SiteStaff>
        width={150}
        title="Hành động"
        align="center"
        fixed="right"
        render={(_, row) => (
          <>
            <Popover trigger="hover" placement="topLeft" title="Chọn hành động" content={contentActionButton(row)}>
              <Button icon={<SelectOutlined />} type="primary">
                Hành động
              </Button>
            </Popover>
          </>
        )}
      />
    </Table>
  );
};

export default SiteStaffTable;
