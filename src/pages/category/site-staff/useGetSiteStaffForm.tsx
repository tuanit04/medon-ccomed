import React, { FC, useEffect } from 'react';
import { Form, Col, Row, Select, Button, Checkbox, Input, Badge } from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import { Facility } from 'common/interfaces/facility.interface';
import { SearchOutlined, ClearOutlined } from '@ant-design/icons';
import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import { functionCodeConstants } from 'constants/functions';
const CheckboxGroup = Checkbox.Group;
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 6,
  xl: 6,
  xxl: 6
};

const wrapperCol4Modal: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
  wrapperCol4Modal: { span: 18 }
};

interface MessageFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;

  isModal?: boolean;

  isEdit?: boolean;

  isCreate?: boolean;

  siteStaff?: SiteStaff;
}

export default function useGetForm({
  required = false,
  responsive = false,
  name = 'form',
  isModal,
  isEdit,
  isCreate,
  siteStaff
}: MessageFormProps) {
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<Partial<SiteStaff>>();
  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={{ ...siteStaff, status: 1, isBotMessage: ['' + siteStaff?.isBotMessage + ''] }}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };
  useEffect(() => {
    formInstance.resetFields();
  }, [formInstance]);

  useEffect(() => {
    formInstance.setFieldsValue({ ...siteStaff });
    if (siteStaff) {
      formInstance.setFieldsValue({ isBotMessage: ['' + siteStaff.isBotMessage + ''] });
    }
  }, [siteStaff]);

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Don vi
  interface FacilityParentProps {
    facilityParentList: Facility[];
    handleChangeFacilityParent: (parentId: any) => void;
  }

  const FacilityParent: FC<FacilityParentProps> = ({ facilityParentList, handleChangeFacilityParent }) => {
    const facilityParent = (
      <Form.Item
        name="parentFacilityId"
        label="Đơn vị"
        rules={[
          {
            required: isModal,
            message: 'Đơn vị không được để trống.'
          }
        ]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          className={isModal ? '' : 'slcSearch'}
          onChange={handleChangeFacilityParent}
          defaultValue=""
        >
          <Select.Option key="" value="">
            -- Tất cả --
          </Select.Option>
          {facilityParentList?.map(option => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? (
      <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{facilityParent}</Col>
    ) : (
      facilityParent
    );
  };

  //Van phong
  interface FacilityProps {
    facilityList: Facility[];
    handleChangeFacility: (id: any) => void;
  }
  const Facility: FC<FacilityProps> = ({ facilityList, handleChangeFacility }) => {
    const facility = (
      <Form.Item
        name="facilityId"
        label="Văn phòng"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeFacility}
          className={isModal ? '' : 'slcSearch'}
          defaultValue=""
        >
          <Select.Option key="-1" value="">
            -- Tất cả --
          </Select.Option>
          {facilityList?.map(option => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{facility}</Col> : facility;
  };

  //Trang thai
  interface StatusProps {
    handleChangeStatus?: (value: any) => void;
  }
  const Status: FC<StatusProps> = ({ handleChangeStatus }) => {
    const status = (
      <Form.Item
        name="status"
        label="Trạng thái"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select disabled={isModal} defaultValue="" onChange={handleChangeStatus} className={isModal ? '' : 'slcSearch'}>
          <Select.Option key="1" value="" disabled>
            <Badge color="blue" status="processing" text="-- Tất cả --" />
          </Select.Option>
          <Select.Option key="2" value={1}>
            <Badge color="green" status="processing" text="Kích hoạt" />
          </Select.Option>
          <Select.Option key="3" value={2}>
            <Badge color="red" status="processing" text="Không kích hoạt" />
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{status}</Col> : status;
  };

  //Trang thai
  interface StatusCreateProps {
    handleChangeStatus?: (value: any) => void;
  }
  const StatusCreate: FC<StatusCreateProps> = ({ handleChangeStatus }) => {
    const status = (
      <Form.Item
        name="status"
        label="Trạng thái"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select disabled={isModal} defaultValue="" onChange={handleChangeStatus} className={isModal ? '' : 'slcSearch'}>
          <Select.Option key="2" value={1}>
            <Badge color="green" status="processing" text="Kích hoạt" />
          </Select.Option>
          <Select.Option key="3" value={2}>
            <Badge color="red" status="processing" text="Không kích hoạt" />
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{status}</Col> : status;
  };

  //Buttons
  interface ButtonsProps {
    functionOb: any;
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ functionOb, onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_CBTAINHA_TIMKIEM] ? false : true}
          onClick={() => {
            onSearch();
          }}
          icon={<SearchOutlined />}
          type="primary"
          htmlType="submit"
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_CBTAINHA_TIMKIEM] ? false : true}
          onClick={resetFields}
          icon={<ClearOutlined />}
        >
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };

  //Ten nhan vien
  const SiteStaffName: FC = () => {
    const siteStaffName = (
      <Form.Item
        rules={[
          {
            required: isModal,
            message: 'Tên nhân viên không được để trống.'
          }
        ]}
        name="name"
        label="Tên nhân viên"
      >
        <Input readOnly={!isEdit} className={isModal ? '' : 'inputSearch'} placeholder="Nhập tên nhân viên" />
      </Form.Item>
    );

    return responsive ? (
      <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{siteStaffName}</Col>
    ) : (
      siteStaffName
    );
  };

  //Mã nhân viên
  const SiteStaffCode: FC = () => {
    const siteStaffCode = (
      <Form.Item
        rules={[
          {
            required: isModal,
            message: 'Mã nhân viên không được để trống.'
          }
        ]}
        name="code"
        label="Mã nhân viên"
      >
        <Input
          readOnly={!isEdit}
          className={isModal ? '' : 'inputSearch'}
          required={true}
          placeholder="Nhập mã nhân viên"
        />
      </Form.Item>
    );
    return responsive ? (
      <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{siteStaffCode}</Col>
    ) : (
      siteStaffCode
    );
  };

  //User tai nha
  const User: FC = () => {
    const user = (
      <Form.Item
        required={isModal}
        name="hisUserName"
        label="User tại nhà"
        rules={[{ message: 'User tại nhà không được để trống.' }]}
      >
        <Input readOnly={!isEdit} />
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{user}</Col> : user;
  };

  //Phone number
  const PhoneNumber: FC = () => {
    function onChangeInput(e) {}

    const phoneNumber = (
      <Form.Item
        rules={[
          {
            required: isModal,
            message: 'Số điện thoại không được để trống hoặc không hợp lệ.',
            pattern: /(0[3|5|7|8|9])+([0-9]{8})\b/g
          }
        ]}
        name="phone"
        label="SĐT liên hệ"
        required={isModal}
      >
        <Input readOnly={!isEdit} onChange={onChangeInput} value="" placeholder="Nhập số điện thoại" />
      </Form.Item>
    );

    return responsive ? (
      <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{phoneNumber}</Col>
    ) : (
      phoneNumber
    );
  };

  //Bot Message
  const BotMessage: FC = () => {
    const botMessage = (
      <Form.Item
        name={'isBotMessage'}
        label="Gửi tin nhắn BOT"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <CheckboxGroup name="isBotMessage">
          <Checkbox indeterminate={false} value={'1'}>
            <span className="red">Gửi tin nhắn BOT ?</span>
          </Checkbox>
        </CheckboxGroup>
      </Form.Item>
    );

    return responsive ? (
      <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{botMessage}</Col>
    ) : (
      botMessage
    );
  };

  return React.useMemo(
    () => ({
      form: formInstance,
      Form: WrappedForm,
      FacilityParent,
      Status,
      Facility,
      Buttons,
      SiteStaffName,
      SiteStaffCode,
      User,
      PhoneNumber,
      BotMessage,
      StatusCreate
    }),
    []
  );
}
