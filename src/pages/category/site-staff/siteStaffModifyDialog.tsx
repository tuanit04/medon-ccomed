import React, { FC, useEffect, useState } from 'react';
import { Modal } from 'antd';
import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import useGetForm from './useGetSiteStaffForm';
import { Facility } from 'common/interfaces/facility.interface';
import { useGetFacilitys } from 'hooks/facility';

interface SiteStaffModifyDialogProps {
  siteStaff: SiteStaff;
  facilityParentList: Facility[];
  visible: boolean;
  onModify: (values: SiteStaff) => void;
  onCancel: () => void;
}

const SiteStaffModifyDialog: FC<SiteStaffModifyDialogProps> = ({
  onModify,
  onCancel,
  visible,
  siteStaff,
  facilityParentList
}) => {
  const {
    form,
    Form,
    SiteStaffName,
    SiteStaffCode,
    User,
    PhoneNumber,
    BotMessage,
    FacilityParent,
    Facility,
    Status
  } = useGetForm({
    name: 'searchForm',
    responsive: true,
    isEdit: true,
    isModal: true,
    siteStaff: siteStaff
  });
  const [parentFacilityId, setParentFacilityId] = useState('');
  const [facilityId, setFacilityId] = useState('');
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  useEffect(() => {
    if (siteStaff) {
      loadFacilities({
        variables: {
          page: 1,
          filtered: [
            {
              id: 'parentId',
              value: siteStaff?.parentFacilityId,
              operation: '=='
            },
            // {
            //   id: 'isHome',
            //   value: "1",
            //   operation: '=='
            // },
            {
              id: 'status',
              value: '1',
              operation: '=='
            }
          ]
        }
      });
    }
  }, [siteStaff, siteStaff?.parentFacilityId]);
  const handleCancle = () => {
    //siteStaff.name
  };
  const onSubmit = async () => {
    const valuesUpdate: any = await form.validateFields();
    const siteStaffUpdate: SiteStaff = {
      ...siteStaff,
      ...valuesUpdate,
      isBotMessage: valuesUpdate['isBotMessage'] ? Number(valuesUpdate['isBotMessage']) : '0'
    };
    const {
      code,
      name,
      staffHisId,
      birthDate,
      sex,
      avatar,
      address,
      email,
      phone,
      hisUserName,
      userId,
      facilityId,
      parentFacilityId,
      isBotMessage,
      id
    } = siteStaffUpdate;
    onModify({
      code,
      name,
      staffHisId,
      birthDate,
      sex,
      avatar,
      address,
      email,
      phone,
      hisUserName,
      userId,
      facilityId,
      parentFacilityId,
      isBotMessage,
      id
    } as SiteStaff);
  };

  return (
    <Modal
      maskClosable={false}
      width={900}
      title="Cập nhật thông tin Cán bộ tại nhà"
      visible={visible}
      onOk={onSubmit}
      onCancel={onCancel}
    >
      <Form>
        <SiteStaffName />
        <SiteStaffCode />
        <User />
        <BotMessage />
        <PhoneNumber />
        <FacilityParent
          facilityParentList={facilityParentList}
          handleChangeFacilityParent={(parentId: any) => {
            setParentFacilityId(parentId);
            if (parentId !== '') {
              form.setFieldsValue({
                facilityId: ''
              });
              loadFacilities({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentId',
                      value: parentId,
                      operation: '=='
                    },
                    // {
                    //   id: 'isHome',
                    //   value: "1",
                    //   operation: '=='
                    // },
                    {
                      id: 'status',
                      value: '1',
                      operation: '=='
                    }
                  ]
                }
              });
            }
          }}
        />
        <Facility
          facilityList={facilities}
          handleChangeFacility={data => {
            if (data === '0') {
              data = '';
            }
            setFacilityId(data);
          }}
        />
        <Status />
      </Form>
    </Modal>
  );
};

export default SiteStaffModifyDialog;
