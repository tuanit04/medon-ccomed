import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import { CODE_RESPONSE } from 'constants/response';
import { useGetFacilityHome } from 'hooks/facility';
import {
  useActiveSiteStaff,
  useInactiveSiteStaff,
  useDeleteSiteStaff,
  useGetSiteStaffDetail,
  useUpdateSiteStaff,
  useCreateSiteStaff
} from 'hooks/site-staff';
import React, { FC, useState } from 'react';
import { useAppState } from 'helpers';
import MyLoader from 'utils/LoadingOverlay';
import './index.less';
import SiteStaffModifyDialog from './siteStaffModifyDialog';
import SiteStaffSearchForm from './siteStaffSearchForm';
import StaffTable from './siteStaffTable';
import SiteStaffCreateDialog from './siteStaffCreateDialog';

const StaffAtHomePage: FC = () => {
  const functionOb = useAppState(state => state.user.functionObject);
  const [modifyVisible, setModifyVisible] = useState(false);
  const [createVisible, setCreateVisible] = useState(false);
  const [authorizeVisible, setAuthorizeVisible] = useState(false);
  const [siteStaffIdRes, setSiteStaffIdRes] = useState('');
  const facilityParentList = useGetFacilityHome();
  const [nameSearch, setNameSearch] = useState({
    id: 'hisUserName',
    value: '',
    operation: '~'
  });
  const [codeSearch, setCodeSearch] = useState({
    id: 'code',
    value: '',
    operation: '~'
  });
  const [parentFacilityIdSearch, setParentFacilityIdSearch] = useState({
    id: 'parentFacilityId',
    value: '',
    operation: '=='
  });
  const [facilityIdSearch, setFacilityIdSearch] = useState({
    id: 'facilityId',
    value: '',
    operation: '=='
  });
  const [statusSearch, setStatusSearch] = useState({
    id: 'status',
    value: '',
    operation: '=='
  });
  const [phoneSearch, setPhoneSearch] = useState({
    id: 'phone',
    value: '',
    operation: '~'
  });
  const [fullNameSearch, setFullNameSearch] = useState({
    id: 'name',
    value: '',
    operation: '~'
  });
  //Xử lý xoá SiteStaff
  const {
    deleteSiteStaff,
    isLoading: isLoadingDelete,
    result: resultDelete,
    error: errorDelete
  } = useDeleteSiteStaff();
  //Xử lý Active Site Staff
  const {
    activeSiteStaff,
    resultActiveSiteStaff,
    isLoadingActiveSiteStaff,
    errorActiveSiteStaff
  } = useActiveSiteStaff();
  const handleActiveSiteStaff = async id => {
    await activeSiteStaff({ id });
  };
  //Xử lý InActive Site Staff
  const {
    inactiveSiteStaff,
    resultInactiveSiteStaff,
    isLoadingInactiveSiteStaff,
    errorInactiveSiteStaff
  } = useInactiveSiteStaff();
  const handleInactiveSiteStaff = async id => {
    await inactiveSiteStaff({ id });
  };
  const handleDeleteSiteStaff = async id => {
    await deleteSiteStaff({ id });
  };
  const onSetIdRes = id => {
    setSiteStaffIdRes(id);
  };
  //Xử lý cập nhật SiteStaff
  const { updateSiteStaff, resultUpdate, errorUpdate, isLoadingUpdate } = useUpdateSiteStaff(onSetIdRes);
  const handleModifySiteStaff = async siteStaff => {
    await updateSiteStaff(siteStaff);
  };
  //Xử lý thêm mới SiteStaff
  const { createSiteStaff, resultCreate, errorCreate, isLoadingCreate } = useCreateSiteStaff(onSetIdRes);
  const handleCreateSiteStaff = async siteStaff => {
    await createSiteStaff(siteStaff);
  };
  if (resultDelete !== undefined && resultDelete.deleteSiteStaff.code === CODE_RESPONSE.SUCCESS) {
  }
  //Lấy SiteStaff Detail (lAZY QUERY)
  const { loadSiteStaffDetail, isLoadingSiteStaff, siteStaff } = useGetSiteStaffDetail();
  // SiteStaff Detail via Table
  const [siteStaffDetail, setSiteStaffDetail] = useState({} as SiteStaff);
  const onResetSiteStaffIdRes = () => {
    setSiteStaffIdRes('');
  };
  return (
    <div className="main-content">
      <MyLoader active={isLoadingDelete} />
      <SiteStaffSearchForm
        functionOb={functionOb}
        facilityParentList={facilityParentList}
        onSearch={value => {
          if (value['hisUserName']) {
            setNameSearch({ ...nameSearch, value: value['hisUserName'] });
          } else if (value['hisUserName'] === '') {
            setNameSearch({ ...nameSearch, value: '' });
          }
          if (value['code']) {
            setCodeSearch({ ...codeSearch, value: value['code'] });
          } else if (value['code'] === '') {
            setCodeSearch({ ...codeSearch, value: '' });
          }
          if (value['parentFacilityId']) {
            setParentFacilityIdSearch({ ...parentFacilityIdSearch, value: value['parentFacilityId'] });
          } else if (value['parentFacilityId'] === '') {
            setParentFacilityIdSearch({ ...parentFacilityIdSearch, value: '' });
          }
          if (value['facilityId']) {
            setFacilityIdSearch({ ...facilityIdSearch, value: value['facilityId'] });
          } else if (value['facilityId'] === '') {
            setFacilityIdSearch({ ...facilityIdSearch, value: '' });
          }
          if (value['status']) {
            setStatusSearch({ ...statusSearch, value: value['status'] });
          } else if (value['status'] === '') {
            setStatusSearch({ ...statusSearch, value: '' });
          }
          if (value['phone']) {
            setPhoneSearch({ ...phoneSearch, value: value['phone'] });
          } else if (value['phone'] === '') {
            setPhoneSearch({ ...phoneSearch, value: '' });
          }
          if (value['name']) {
            setFullNameSearch({ ...fullNameSearch, value: value['name'] });
          } else {
            setFullNameSearch({ ...fullNameSearch, value: '' });
          }
        }}
      />
      <StaffTable
        onCreate={() => {
          setCreateVisible(true);
        }}
        onModify={siteStaff => {
          setSiteStaffDetail(siteStaff);
          setModifyVisible(true);
        }}
        onAuthorize={siteStaff => {
          setAuthorizeVisible(true);
        }}
        onDelete={async id => {
          await handleDeleteSiteStaff(id);
        }}
        onActiveSiteStaff={async id => {
          await handleActiveSiteStaff(id);
        }}
        onInactiveSiteStaff={async id => {
          await handleInactiveSiteStaff(id);
        }}
        functionOb={functionOb}
        nameSearch={nameSearch}
        codeSearch={codeSearch}
        parentFacilityIdSearch={parentFacilityIdSearch}
        facilityIdSearch={facilityIdSearch}
        statusSearch={statusSearch}
        siteStaffIdRes={siteStaffIdRes}
        phoneSearch={phoneSearch}
        fullNameSearch={fullNameSearch}
        onResetSiteStaffIdRes={onResetSiteStaffIdRes}
      />
      <SiteStaffModifyDialog
        siteStaff={siteStaffDetail}
        facilityParentList={facilityParentList}
        visible={modifyVisible}
        onCancel={() => setModifyVisible(false)}
        onModify={async (values: any) => {
          //delete values['__typename'];
          await handleModifySiteStaff({ data: { ...values } });
          setModifyVisible(false);
        }}
      />
      <SiteStaffCreateDialog
        facilityParentList={facilityParentList}
        visible={createVisible}
        onCancel={() => setCreateVisible(false)}
        onCreate={async (values: any) => {
          await handleCreateSiteStaff({ data: { ...values } });
          setCreateVisible(false);
        }}
      />
    </div>
  );
};
export default StaffAtHomePage;
