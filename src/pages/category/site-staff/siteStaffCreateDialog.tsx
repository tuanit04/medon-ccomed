import React, { FC, useEffect, useState } from 'react';
import { Modal } from 'antd';
import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import useGetForm from './useGetSiteStaffForm';
import { Facility } from 'common/interfaces/facility.interface';
import { useGetFacilitys } from 'hooks/facility';

interface SiteStaffCreateDialogProps {
  facilityParentList: Facility[];
  visible: boolean;
  onCreate: (values: SiteStaff) => void;
  onCancel: () => void;
}

const SiteStaffCreateDialog: FC<SiteStaffCreateDialogProps> = ({ onCreate, onCancel, visible, facilityParentList }) => {
  const {
    form,
    Form,
    SiteStaffName,
    SiteStaffCode,
    User,
    PhoneNumber,
    BotMessage,
    FacilityParent,
    Facility,
    Status,
    StatusCreate
  } = useGetForm({
    name: 'searchForm',
    responsive: true,
    isEdit: true,
    isModal: true,
    isCreate: true
  });
  useEffect(() => {
    form.resetFields();
  }, [visible]);
  const [parentFacilityId, setParentFacilityId] = useState('');
  const [facilityId, setFacilityId] = useState('');
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  const handleCancle = () => {
    //siteStaff.name
  };
  const onSubmit = async () => {
    const valuesUpdate: any = await form.validateFields();
    let isBotMessageValue = valuesUpdate['isBotMessage'][0] === '1' ? '1' : '0';
    const siteStaffUpdate: SiteStaff = {
      ...valuesUpdate,
      status: valuesUpdate['statusCreate'],
      isBotMessage: isBotMessageValue
    };
    const {
      code,
      name,
      staffHisId,
      birthDate,
      sex,
      avatar,
      address,
      email,
      phone,
      hisUserName,
      userId,
      facilityId,
      parentFacilityId,
      isBotMessage,
      id
    } = siteStaffUpdate;
    onCreate({
      code,
      name,
      staffHisId,
      birthDate,
      sex,
      avatar,
      address,
      email,
      phone,
      hisUserName,
      userId,
      facilityId,
      parentFacilityId,
      isBotMessage,
      id
    } as SiteStaff);
  };

  return (
    <Modal
      maskClosable={false}
      width={900}
      title="Thêm mới thông tin Cán bộ tại nhà"
      visible={visible}
      onOk={onSubmit}
      onCancel={onCancel}
    >
      <Form>
        <SiteStaffName />
        <SiteStaffCode />
        <User />
        <BotMessage />
        <PhoneNumber />
        <FacilityParent
          facilityParentList={facilityParentList}
          handleChangeFacilityParent={(parentId: any) => {
            setParentFacilityId(parentId);
            if (parentId !== '') {
              form.setFieldsValue({
                facilityId: ''
              });
              loadFacilities({
                variables: {
                  page: 1,
                  filtered: [
                    {
                      id: 'parentId',
                      value: parentId,
                      operation: '=='
                    },
                    // {
                    //   id: 'isHome',
                    //   value: "1",
                    //   operation: '=='
                    // },
                    {
                      id: 'status',
                      value: '1',
                      operation: '=='
                    }
                  ]
                }
              });
            }
          }}
        />
        <Facility
          facilityList={facilities}
          handleChangeFacility={data => {
            if (data === '0') {
              data = '';
            }
            setFacilityId(data);
          }}
        />
        <StatusCreate />
      </Form>
    </Modal>
  );
};

export default SiteStaffCreateDialog;
