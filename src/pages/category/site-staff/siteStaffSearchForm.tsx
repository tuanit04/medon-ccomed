import React, { FC, useState } from 'react';
import { Role } from 'interface/permission/role.interface';
import useGetForm from './useGetSiteStaffForm';
import { Facility } from 'common/interfaces/facility.interface';
import { useGetFacilitys } from 'hooks/facility';

export interface Values extends Role {}

interface SiteStaffSearchProps {
  functionOb: any;
  facilityParentList: Facility[];
  onSearch: ({}) => void;
}

const SiteStaffSearchForm: FC<SiteStaffSearchProps> = ({ functionOb, facilityParentList, onSearch }) => {
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  const { Form, form, FacilityParent, Status, Facility, Buttons, SiteStaffName, User, PhoneNumber } = useGetForm({
    name: 'searchForm',
    responsive: true,
    isEdit: true
  });
  return (
    <Form>
      <User />
      <PhoneNumber />
      <SiteStaffName />
      <FacilityParent
        facilityParentList={facilityParentList}
        handleChangeFacilityParent={(parentId: any) => {
          loadFacilities({
            variables: {
              page: 1,
              filtered: [
                {
                  id: 'parentId',
                  value: parentId,
                  operation: '=='
                },
                // {
                //   id: 'isHome',
                //   value: "1",
                //   operation: '=='
                // }
                // ,
                {
                  id: 'status',
                  value: '1',
                  operation: '=='
                }
              ]
            }
          });
        }}
      />
      <Facility
        handleChangeFacility={data => {
          if (data === '0') {
            data = '';
          }
        }}
        facilityList={facilities}
      />
      <Status
        handleChangeStatus={data => {
          //setStatus(data);
        }}
      />
      <Buttons
        functionOb={functionOb}
        onSearch={() => {
          let hisUserName = form.getFieldValue('hisUserName');
          let code = form.getFieldValue('code');
          let parentFacilityId = form.getFieldValue('parentFacilityId');
          let facilityId = form.getFieldValue('facilityId');
          let status = form.getFieldValue('status');
          let phone = form.getFieldValue('phone');
          let name = form.getFieldValue('name');
          onSearch({ hisUserName, code, parentFacilityId, facilityId, status, phone, name });
        }}
        resetFields={() => {
          form.resetFields();
          onSearch({
            hisUserName: '',
            code: '',
            parentFacilityId: '',
            facilityId: '',
            status: '',
            phone: '',
            name: ''
          });
        }}
      />
    </Form>
  );
};

export default SiteStaffSearchForm;
