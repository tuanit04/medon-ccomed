import React, { FC, useEffect, useState } from 'react';
import { Button, Table, Modal } from 'antd';
import EditableTagGroup from './components/EditableTagGroup';
import { CopyOutlined, DeleteOutlined, ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Schedule } from 'common/interfaces/schedule.interface';
import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import { useDeleteAllSiteStaffSchedule, useDeleteSiteStaffSchedule, useGetSchedules } from 'hooks/schedule';
import { RouteSchedule } from 'common/interfaces/routeSchedule.interface';
import { WorkSession } from 'common/interfaces/workSession.interface';
import './index.less';
import CopyWorkScheduleDialog from './copyWorkScheduleDialog';
import { functionCodeConstants } from 'constants/functions';

interface ScheduleTableProps {
  onCreate: (routeId: string, refetchSchedules) => void;
  onCreateSchedule: () => void;
  onCopy: (values: any) => void;
  onAuthorize: (row: Schedule) => void;
  onModify: (row: Schedule) => void;
  functionOb: any;
  setVisiableCopyWorkScheduleDialog: (value: boolean) => void;
  visiableCopyWorkScheduleDialog: boolean;
  onSetStaffSelected: (values: { workSessionIdValue; siteStaffIdValue; siteStaffNameValue; scheduleIdValue }[]) => void;
  onDelete: (value: string) => void;
  siteStaffList: SiteStaff[];
  workSessionList: WorkSession[];
  onSetRowSelected: (row: RouteSchedule) => void;
  siteStaffWs: { workSessionIdValue; siteStaffIdValue; siteStaffNameValue; scheduleIdValue };
  parentFacilityId: string;
  facilityId: string;
  scheduleDate: string;
  routeId: string;
  dateOfRegistration: string;
  siteStaffIdRes: string;
}

const ScheduleTable: FC<ScheduleTableProps> = ({
  onCreate,
  onCopy,
  onCreateSchedule,
  onModify,
  onSetStaffSelected,
  onAuthorize,
  siteStaffList,
  workSessionList,
  onSetRowSelected,
  siteStaffWs,
  setVisiableCopyWorkScheduleDialog,
  visiableCopyWorkScheduleDialog,
  parentFacilityId,
  facilityId,
  scheduleDate,
  routeId,
  functionOb,
  dateOfRegistration,
  siteStaffIdRes
}) => {
  const [workSessionIdSelected, setWorkSessionIdSelected] = useState('');
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(20);
  const [isDeleteAll, setIsDeleteAll] = useState(false);
  const variables = { page, pageSize, parentFacilityId, facilityId, scheduleDate };
  let variablesV2 = {
    page: page,
    pageSize: pageSize,
    parentFacilityId: '59b095cc-c731-4aba-9b45-a6bffb980c94',
    facilityId: 'fea34cb1-4e03-4e3c-91d0-5332be9d90b2',
    scheduleDate: dateOfRegistration
  };
  const { pagingSchedules, isLoadingSchedules, errorSchedules, refetchSchedules } = useGetSchedules(variables);
  //Xoa tat ca lich da phan
  const {
    deleteAllSiteStaffSchedule,
    resultDeleteAllSiteStaffSchedule,
    isLoadingDeleteAllSiteStaffSchedule,
    errorDeleteAllSiteStaffSchedule
  } = useDeleteAllSiteStaffSchedule();
  const handleDeleteAllSiteStaffSchedule = async variables => {
    await deleteAllSiteStaffSchedule(variables);
    refetchSchedules();
    //setIsDeleteAll(true);
  };
  const {
    deleteSiteStaffSchedule,
    resultDeleteSiteStaffSchedule,
    isLoadingDeleteSiteStaffSchedule,
    errorDeleteSiteStaffSchedule
  } = useDeleteSiteStaffSchedule();
  const handleDeleteSiteStaffSchedule = async variables => {
    await deleteSiteStaffSchedule(variables);
    refetchSchedules();
  };
  const [pagination, setPagination] = useState({});

  const handleTableChange = (pagination, filters, sorter) => {
    console.log('pagination, filters, sorter ', pagination, filters, sorter);
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  useEffect(() => {
    refetchSchedules();
  }, [siteStaffIdRes]);

  useEffect(() => {
    setPagination({ current: page, pageSize: pageSize, total: pagingSchedules['records'] });
  }, [pagingSchedules]);
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    setPage(pagination['current']);
    setPageSize(pagination['pageSize']);
  };
  const handleDelete = async id => {
    //await onDelete(id);
    //refetchStreets();
  };
  const handleClickCell = (data: string) => {
    setWorkSessionIdSelected(data);
    workSessionList?.map(option => {
      if (option.id === data) {
        const div = document.getElementById(data);
        if (div) div.style.backgroundColor = '#ffce60';
      } else {
        const div = document.getElementById(option.id);
        if (div) div.style.backgroundColor = '#fff';
      }
    });
  };
  return (
    <>
      <Table
        rowKey="id"
        className="table-street"
        style={{ height: 'calc(100vh - 75px)', overflow: 'hidden' }}
        pagination={{
          ...pagination,
          defaultPageSize: 10,
          showSizeChanger: true,
          pageSizeOptions: ['10', '20', '30', '50']
        }}
        loading={isLoadingSchedules}
        onChange={handleTableChange}
        dataSource={pagingSchedules?.data}
        bordered
        scroll={{ x: true, y: 'calc(100vh - 220px)' }}
        title={() => (
          <div>
            <div style={{ float: 'left', maxWidth: '250' }}></div>
            <div style={{ float: 'right' }}>
              <Button
                disabled={functionOb[functionCodeConstants.TD_DM_LLV_COPY_WORK_SCHEDULE] ? false : true}
                icon={<PlusOutlined />}
                style={{ marginLeft: 10 }}
                type="primary"
                onClick={() => {
                  onCreateSchedule();
                }}
              >
                Thêm mới lịch
              </Button>
              &nbsp;&nbsp;
              <Button
                disabled={functionOb[functionCodeConstants.TD_DM_LLV_COPY_WORK_SCHEDULE] ? false : true}
                icon={<CopyOutlined />}
                style={{ marginLeft: 10 }}
                type="primary"
                onClick={() => {
                  setVisiableCopyWorkScheduleDialog(true);
                }}
              >
                Copy lịch làm việc
              </Button>
              &nbsp;&nbsp;
              <Button
                disabled={functionOb[functionCodeConstants.TD_DM_LLV_DELETE_ALL_WORK_SCHEDULE_OF_DAY] ? false : true}
                danger
                icon={<DeleteOutlined />}
                style={{ marginLeft: 10 }}
                type="primary"
                onClick={() => {
                  Modal.confirm({
                    icon: <ExclamationCircleOutlined />,
                    title: 'Bạn có chắc chắn muốn xóa hết lịch đã phân ngày ' + dateOfRegistration + ' ?',
                    onOk() {
                      handleDeleteAllSiteStaffSchedule(
                        facilityId === ''
                          ? {
                              parentFacilityId: parentFacilityId,
                              scheduleDate: dateOfRegistration
                            }
                          : {
                              parentFacilityId: parentFacilityId,
                              facilityId: facilityId,
                              scheduleDate: dateOfRegistration
                            }
                      );
                      /*notification.open({
                        message: 'Thông báo',
                        style: { background: '#edffff' },
                        description: 'Danh sách lịch đã xóa thành công.',
                        icon: <DeleteOutlined style={{ color: '#206ad2' }} />
                      });*/
                    },
                    onCancel() {
                      //console.log('Cancel');
                    }
                  });
                }}
              >
                Xóa hết lịch
              </Button>
            </div>
            <div style={{ clear: 'both' }}></div>
          </div>
        )}
      >
        <Table.Column<RouteSchedule>
          title="STT"
          width={50}
          align="center"
          render={(value, item, index) => (page - 1) * pageSize + index + 1}
        />
        <Table.Column<RouteSchedule> title="Đơn vị" width={200} render={(_, { facilityName }) => facilityName} />
        <Table.Column<RouteSchedule> title="Cung đường" width={200} render={(_, { routeName }) => routeName} />
        <Table.Column<RouteSchedule>
          title={
            <label>
              Ngày đăng ký : {dateOfRegistration}
              <label style={{ color: '#f8feff' }}> (Click loại ca để xem cán bộ theo ca)</label>
            </label>
          }
          width={380}
          render={(_, row: RouteSchedule) => {
            let arr = [{} as { workSessionIdValue; siteStaffIdValue; siteStaffNameValue; scheduleIdValue }];
            for (let i = 0; i < row?.siteStaffs?.length; ++i) {
              let workSessionIdValue = row.siteStaffs[i].workSessionId;
              let siteStaffIdValue = row.siteStaffs[i].objectId;
              let siteStaffNameValue = row.siteStaffs[i].objectName;
              let scheduleIdValue = row.siteStaffs[i].scheduleId;
              let obj = { workSessionIdValue, siteStaffIdValue, siteStaffNameValue, scheduleIdValue };
              arr.push(obj);
            }
            onSetStaffSelected(arr);
            //Nguyên nhân trong return
            return (
              <EditableTagGroup
                functionOb={functionOb}
                onSetRowSelected={onSetRowSelected}
                onCreate={onCreate}
                routeSchedule={row}
                siteStaffWs={
                  row.routeId === routeId
                    ? siteStaffWs
                    : {
                        workSessionIdValue: '',
                        siteStaffIdValue: '',
                        siteStaffNameValue: '',
                        scheduleIdValue: ''
                      }
                }
                siteStaffList={arr}
                workSessionIdSelected={workSessionIdSelected}
                refetchSchedules={refetchSchedules}
                onDeleteSiteStaffSchedule={handleDeleteSiteStaffSchedule}
              />
            );
          }}
        />
        {workSessionList?.map(option => (
          <Table.Column<RouteSchedule>
            title={
              <div
                id={option.id}
                style={{ background: '#fff', borderRadius: '5px' }}
                data-id={option.id}
                onClick={() => {
                  handleClickCell(option.id);
                }}
              >
                {' '}
                <a style={{ color: '#206ad2', textDecoration: 'none' }}>{option.name}</a>
              </div>
            }
            width={120}
            align="center"
            render={(_, row: RouteSchedule) => {
              let count = 0;
              for (let i = 0; i < row?.workSessions?.length; ++i) {
                if (row.workSessions[i].id === option.id) count++;
              }
              return count;
            }}
          />
        ))}
      </Table>
      <CopyWorkScheduleDialog
        dateSelected={dateOfRegistration}
        onCancel={() => setVisiableCopyWorkScheduleDialog(false)}
        onCopy={values => {
          onCopy(values);
        }}
        visible={visiableCopyWorkScheduleDialog}
      />
    </>
  );
};

export default ScheduleTable;
