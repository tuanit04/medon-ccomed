import React, { FC, useEffect, useState } from 'react';
import { Role } from 'interface/permission/role.interface';
import {} from '@ant-design/icons';
import { Facility } from 'common/interfaces/facility.interface';
import { useGetFacilitys } from 'hooks/facility';
import useGetScheduleForm from './useGetScheduleForm';
import moment from 'moment';

export interface Values extends Role {}

interface ScheduleSearchProps {
  functionOb: any;
  facilityParentList: Facility[];
  handleChangeDateOfRegistration: (date: any) => void;
  onSearch: ({}) => void;
  onChangeParentFacilityId?: (id: string) => void;
  onChangeFacilityId?: (id: string) => void;
}

const ScheduleSearchForm: FC<ScheduleSearchProps> = ({
  functionOb,
  facilityParentList,
  onChangeParentFacilityId,
  onChangeFacilityId,
  handleChangeDateOfRegistration,
  onSearch
}) => {
  const [dateSchedule, setDateSchedule] = useState('');
  const { Form, form, DateOfRegistration, FacilityParent, Facility, Buttons } = useGetScheduleForm({
    name: 'searchForm',
    responsive: true
  });
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();

  useEffect(() => {
    form.setFieldsValue({
      facilityId: ''
    });
  }, [facilities]);

  return (
    <Form style={{ marginTop: '20px', marginLeft: '10px', marginRight: '10px' }}>
      <DateOfRegistration
        handleChangeDateOfRegistration={(date: any) => {
          setDateSchedule(moment(date).format('DD/MM/YYYY'));
          handleChangeDateOfRegistration(moment(date).format('DD/MM/YYYY'));
        }}
      />
      <FacilityParent
        facilityParentList={facilityParentList}
        handleChangeFacilityParent={(parentId: any) => {
          loadFacilities({
            variables: {
              page: 1,
              filtered: [
                {
                  id: 'parentId',
                  value: parentId,
                  operation: '=='
                },
                // {
                //   id: 'isHome',
                //   value: "1",
                //   operation: '=='
                // }
                // ,
                {
                  id: 'status',
                  value: '1',
                  operation: '=='
                }
              ]
            }
          });
        }}
      />
      <Facility
        handleChangeFacility={id => {
          //onChangeFacilityId(id);
        }}
        facilityList={facilities}
      />
      <Buttons
        functionOb={functionOb}
        onSearch={() => {
          const parentFacilityId = form.getFieldValue('parentFacilityId') ?? facilityParentList?.[0]?.id ?? '';
          const facilityId = form.getFieldValue('facilityId') ?? '';
          onSearch({ parentFacilityId, facilityId, dateSchedule });
        }}
        resetFields={() => {
          form.resetFields();
          onSearch({ parentFacilityId: facilityParentList?.[0]?.id ?? '', facilityId: '', dateSchedule: '' });
        }}
      />
    </Form>
  );
};

export default ScheduleSearchForm;
