import React, { FC, useEffect } from 'react';
import { Modal, Spin } from 'antd';
import useGetScheduleForm from './useGetScheduleForm';
import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import { WorkSession } from 'common/interfaces/workSession.interface';
interface ScheduleCreateDialogProps {
  visible: boolean;
  onCreate: (values: {}) => void;
  onCancel: () => void;
  workSessions: WorkSession[];
  siteStaffs: SiteStaff[];
  isLoadingSiteStaffs: boolean;
}

const ScheduleCreateDialog: FC<ScheduleCreateDialogProps> = ({
  onCreate,
  onCancel,
  visible,
  workSessions,
  siteStaffs,
  isLoadingSiteStaffs
}) => {
  let workSessionIdValue = '';
  let siteStaffIdValue = '';
  let siteStaffNameValue = '';
  const { Form, form, WorkSession, SiteStaff } = useGetScheduleForm({
    name: 'searchForm',
    responsive: true,
    isModal: true
  });
  useEffect(() => {
    form.resetFields();
  });
  /*useEffect(() => {
    form.resetFields();
  }, [siteStaffs, workSessions]);*/

  return (
    <Modal
      maskClosable={false}
      title="Thêm lịch Cán bộ tại nhà"
      visible={visible}
      width={600}
      okText="Đồng ý"
      cancelText="Hủy bỏ"
      onOk={() => onCreate({ workSessionIdValue, siteStaffIdValue, siteStaffNameValue })}
      onCancel={onCancel}
    >
      <Spin tip="Đang tải..." spinning={isLoadingSiteStaffs}>
        <Form>
          <WorkSession
            isMultiple={false}
            handleChangeWorkSession={value => {
              workSessionIdValue = value;
            }}
            workSessionList={workSessions}
          />
          <SiteStaff
            isMultiple={true}
            handleChangeSiteStaff={value => {
              // let [siteStaffId] = value.split('###');
              siteStaffIdValue = value;
              // siteStaffNameValue = siteStaffName;
            }}
            siteStaffList={siteStaffs}
          />
        </Form>
      </Spin>
    </Modal>
  );
};

export default ScheduleCreateDialog;
