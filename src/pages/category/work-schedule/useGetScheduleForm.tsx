import React, { FC } from 'react';
import { Form, Col, Row, Select, Button, Badge, DatePicker } from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from 'helpers';
import { Facility } from 'common/interfaces/facility.interface';
import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import { SearchOutlined, ClearOutlined } from '@ant-design/icons';
import { Schedule } from 'common/interfaces/schedule.interface';
import { WorkSession } from 'common/interfaces/workSession.interface';
import moment from 'moment';
import { functionCodeConstants } from 'constants/functions';
import { Route } from 'common/interfaces/route.interface';

const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 6,
  xl: 6,
  xxl: 6
};
const wrapperCol4Modal: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const layout = {
  labelCol: { span: 9 },
  wrapperCol: { span: 15 }
};

interface ScheduleFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  isModal?;
  /** Initial form data */
  schedule?: Schedule;
}

export default function useGetScheduleForm({
  required = false,
  responsive = false,
  name = 'form',
  isModal,
  schedule
}: ScheduleFormProps) {
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<
    Partial<{ startDate: string | moment.Moment; endDate: string | moment.Moment; facilityId?: string }>
  >();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState(state => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={{ ...schedule, startDate: moment(), endDate: moment(), facilityId: '' }}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Ngay dang ky
  interface DateOfRegistrationProps {
    handleChangeDateOfRegistration: (date: any) => void;
  }
  const DateOfRegistration: FC<DateOfRegistrationProps> = ({ handleChangeDateOfRegistration }) => {
    const dateFormat = 'DD/MM/YYYY';
    const dateOfRegistration = (
      <Form.Item
        required={true}
        name="dateOfRegistration"
        label="Ngày đăng ký"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <DatePicker
          className="search-cpn"
          onChange={handleChangeDateOfRegistration}
          defaultValue={moment()}
          format={dateFormat}
        />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{dateOfRegistration}</Col> : dateOfRegistration;
  };

  //Ngày chọn để copy lịch - Ngày bắt đầu
  interface Date4CopyStartDateProps {
    handleChangeDateOfRegistration: (date: any) => void;
  }
  const Date4CopyStartDate: FC<Date4CopyStartDateProps> = ({ handleChangeDateOfRegistration }) => {
    const date4CopyFormat = 'DD/MM/YYYY';
    const disablePastDt = current => {
      const today = moment().subtract(0, 'day');
      return current.isBefore(today);
    };
    const date4Copy = (
      <Form.Item
        required={true}
        name="startDate"
        label="Ngày bắt đầu"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <DatePicker
          className="search-cpn"
          disabledDate={disablePastDt}
          onChange={handleChangeDateOfRegistration}
          defaultValue={moment().add(1, 'days')}
          format={date4CopyFormat}
        />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol4Modal}>{date4Copy}</Col> : date4Copy;
  };

  //Ngày chọn để copy lịch - Ngày kết thúc
  interface Date4CopyEndDateProps {
    handleChangeDateOfRegistration: (date: any) => void;
  }
  const Date4CopyEndDate: FC<Date4CopyEndDateProps> = ({ handleChangeDateOfRegistration }) => {
    const date4CopyFormat = 'DD/MM/YYYY';
    const disablePastDt = current => {
      const today = moment().subtract(0, 'day');
      return current.isBefore(today);
    };
    const date4Copy = (
      <Form.Item
        required={true}
        name="endDate"
        label="Ngày kết thúc"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <DatePicker
          className="search-cpn"
          disabledDate={disablePastDt}
          onChange={handleChangeDateOfRegistration}
          defaultValue={moment().add(1, 'days')}
          format={date4CopyFormat}
        />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol4Modal}>{date4Copy}</Col> : date4Copy;
  };

  //Don vi
  interface FacilityParentProps {
    facilityParentList: Facility[];
    handleChangeFacilityParent: (parentId: any) => void;
  }

  const FacilityParent: FC<FacilityParentProps> = ({ facilityParentList, handleChangeFacilityParent }) => {
    const facilityParent = (
      <Form.Item
        name="parentFacilityId"
        label="Đơn vị"
        className={isModal ? '' : 'slcSearch'}
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select
          showSearch
          defaultValue={
            facilityParentList !== undefined && facilityParentList.length !== 0 ? facilityParentList[0].id : ''
          }
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeFacilityParent}
        >
          {facilityParentList?.map((option, index) => (
            <Select.Option key={option.facilityCode} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? (
      <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{facilityParent}</Col>
    ) : (
      facilityParent
    );
  };

  //Van phong
  interface FacilityProps {
    facilityList: Facility[];
    handleChangeFacility: (id: any) => void;
  }
  const Facility: FC<FacilityProps> = ({ facilityList, handleChangeFacility }) => {
    const facility = (
      <Form.Item
        name="facilityId"
        label="Văn phòng"
        className={isModal ? '' : 'slcSearch'}
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeFacility}
          placeholder="-- Chọn văn phòng --"
        >
          <Select.Option value="">-- Tất cả --</Select.Option>
          {facilityList?.map(option => (
            <Select.Option key={option.id} value={option.id}>
              {option.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{facility}</Col> : facility;
  };

  //Phien lam viêc
  interface WorkSessionProps {
    handleChangeWorkSession: (value: string) => void;
    workSessionList: WorkSession[];
    isMultiple: boolean;
  }
  const WorkSession: FC<WorkSessionProps> = ({ handleChangeWorkSession, workSessionList, isMultiple }) => {
    const workSession = (
      <Form.Item
        name="workSession"
        label="Chọn phiên làm việc"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        {isMultiple ? (
          <Select
            mode="multiple"
            showSearch
            onChange={handleChangeWorkSession}
            placeholder="-- Chọn phiên làm việc --"
            optionFilterProp="children"
          >
            {workSessionList?.map(option => (
              <Select.Option value={option.id}>{option.name}</Select.Option>
            ))}
          </Select>
        ) : (
          <Select
            showSearch
            onChange={handleChangeWorkSession}
            placeholder="-- Chọn phiên làm việc --"
            optionFilterProp="children"
          >
            {workSessionList?.map(option => (
              <Select.Option value={option.id}>{option.name}</Select.Option>
            ))}
          </Select>
        )}
      </Form.Item>
    );
    return responsive ? (
      <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{workSession}</Col>
    ) : (
      workSession
    );
  };

  //Cung đường
  interface RoutesProps {
    routeList: Route[];
  }
  const Route: FC<RoutesProps> = ({ routeList }) => {
    const route = (
      <Form.Item
        name="route"
        label="Chọn cung đường"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      ></Form.Item>
    );
    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{route}</Col> : route;
  };

  //Chon can bo tai nha
  interface SiteStaffProps {
    siteStaffList: SiteStaff[];
    handleChangeSiteStaff: (value: string) => void;
    isMultiple: boolean;
  }
  const SiteStaff: FC<SiteStaffProps> = ({ siteStaffList, handleChangeSiteStaff, isMultiple }) => {
    const siteStaff = (
      <Form.Item
        name="siteStaff"
        label="Chọn cán bộ tại nhà"
        rules={[
          {
            required: true,
            message: 'Cán bộ tại nhà không được để trống.'
          }
        ]}
      >
        {isMultiple ? (
          <Select
            mode="multiple"
            allowClear
            filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            showSearch
            placeholder="-- Chọn cán bộ tại nhà --"
            onChange={handleChangeSiteStaff}
          >
            {siteStaffList?.map(option => (
              <Select.Option key={option.id} value={option.id}>
                {option.name}
              </Select.Option>
            ))}
          </Select>
        ) : (
          <Select
            allowClear
            filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            showSearch
            placeholder="-- Chọn cán bộ tại nhà --"
            onChange={handleChangeSiteStaff}
          >
            {siteStaffList?.map(option => (
              <Select.Option key={option.id} value={option.id}>
                {option.name}
              </Select.Option>
            ))}
          </Select>
        )}
      </Form.Item>
    );
    return responsive ? <Col {...(isModal ? { ...wrapperCol4Modal } : { ...wrapperCol })}>{siteStaff}</Col> : siteStaff;
  };

  //Buttons
  interface ButtonsProps {
    functionOb: any;
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ functionOb, onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_LLV_TIMKIEM_THONGKE] ? false : true}
          onClick={onSearch}
          icon={<SearchOutlined />}
          type="primary"
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_LLV_TIMKIEM_THONGKE] ? false : true}
          onClick={resetFields}
          icon={<ClearOutlined />}
        >
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };

  //Schedule
  interface ScheduleProps {
    onModify: (value: string) => void;
  }
  const Schedule: FC<ScheduleProps> = ({ onModify }) => {
    const handleOnChange = value => {
      onModify(value);
    };
    const schedule = (
      <Form.Item
        name="schedule"
        label="Chọn lịch"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select onChange={handleOnChange} placeholder="---Chọn lịch---">
          <Select.Option value="4">Cả ngày (S + C)</Select.Option>
          <Select.Option value="1">Buổi sáng (S)</Select.Option>
          <Select.Option value="2">Buổi chiều (C)</Select.Option>
          <Select.Option value="3">Buổi tối (T)</Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{schedule}</Col> : schedule;
  };
  return {
    form: formInstance,
    Form: WrappedForm,
    DateOfRegistration,
    FacilityParent,
    Date4CopyStartDate,
    Date4CopyEndDate,
    Facility,
    WorkSession,
    SiteStaff,
    Route,
    Buttons,
    Schedule
  };
}
