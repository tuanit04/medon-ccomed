import React, { FC, useEffect } from 'react';
import { Modal, Spin } from 'antd';
import useGetScheduleForm from './useGetScheduleForm';
import moment, { Moment } from 'moment';
import { openNotificationRight } from 'utils/notification';
interface ScheduleCreateDialogProps {
  visible: boolean;
  dateSelected: string;
  onCopy: (values: any) => void;
  onCancel: () => void;
}

const CopyWorkScheduleDialog: FC<ScheduleCreateDialogProps> = ({ onCopy, dateSelected, onCancel, visible }) => {
  let siteStaffIdValue = '';
  let siteStaffNameValue = '';
  const { Form, form, Date4CopyStartDate, Date4CopyEndDate } = useGetScheduleForm({
    name: 'searchForm',
    responsive: true,
    isModal: true
  });
  useEffect(() => {
    form.resetFields();
  });
  /*useEffect(() => {
    form.resetFields();
  }, [siteStaffs, workSessions]);*/

  return (
    <Modal
      okText="Đồng ý"
      cancelText="Hủy bỏ"
      maskClosable={false}
      title={'Copy lịch làm việc của ngày : ' + dateSelected}
      visible={visible}
      width={600}
      onOk={async () => {
        const valuesCopy: any = await form.validateFields();
        let startDate = moment(String(valuesCopy['startDate']));
        let endDate = moment(String(valuesCopy['endDate']));
        if (startDate.isAfter(endDate)) {
          openNotificationRight('Ngày bắt đầu không được nhỏ hơn ngày kết thúc.');
          return;
        }
        onCopy(valuesCopy);
      }}
      onCancel={onCancel}
    >
      <Form>
        <Date4CopyStartDate
          handleChangeDateOfRegistration={(value: Moment) => {
            //console.log('value : ', value?.format('DD/MM/YYYY'));
            form.setFieldsValue({
              endDate: value
            });
          }}
        />
        <Date4CopyEndDate handleChangeDateOfRegistration={value => {}} />
      </Form>
    </Modal>
  );
};

export default CopyWorkScheduleDialog;
