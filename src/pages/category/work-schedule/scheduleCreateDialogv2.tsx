import React, { FC, useEffect } from 'react';
import { Modal, Spin } from 'antd';
import useGetScheduleForm from './useGetScheduleForm';
import { SiteStaff } from 'common/interfaces/siteStaff.interface';
import { WorkSession } from 'common/interfaces/workSession.interface';
interface ScheduleCreateDialogPropsv2 {
  visible: boolean;
  onCreate: (values: {}) => void;
  onCancel: () => void;
  workSessions: WorkSession[];
  siteStaffs: SiteStaff[];
  isLoadingSiteStaffs: boolean;
}

const ScheduleCreateDialogv2: FC<ScheduleCreateDialogPropsv2> = ({
  onCreate,
  onCancel,
  visible,
  workSessions,
  siteStaffs,
  isLoadingSiteStaffs
}) => {
  let workSessionIdValue = '';
  let siteStaffIdValue = '';
  let siteStaffNameValue = '';
  const { Form, form, WorkSession, Route, SiteStaff } = useGetScheduleForm({
    name: 'searchForm',
    responsive: true,
    isModal: true
  });
  useEffect(() => {
    form.resetFields();
  });
  /*useEffect(() => {
    form.resetFields();
  }, [siteStaffs, workSessions]);*/

  return (
    <Modal
      maskClosable={false}
      title="Thêm lịch Cán bộ tại nhà"
      visible={visible}
      width={1200}
      okText="Đồng ý"
      cancelText="Hủy bỏ"
      onOk={() => onCreate({ workSessionIdValue, siteStaffIdValue, siteStaffNameValue })}
      onCancel={onCancel}
    >
      <Spin tip="Đang tải..." spinning={isLoadingSiteStaffs}>
        <Form>
          <Route routeList={[]} />
          <WorkSession
            isMultiple={true}
            handleChangeWorkSession={value => {
              workSessionIdValue = value;
            }}
            workSessionList={workSessions}
          />
          <SiteStaff
            isMultiple={false}
            handleChangeSiteStaff={value => {
              // let [siteStaffId] = value.split('###');
              siteStaffIdValue = value;
              // siteStaffNameValue = siteStaffName;
            }}
            siteStaffList={siteStaffs}
          />
        </Form>
      </Spin>
    </Modal>
  );
};

export default ScheduleCreateDialogv2;
