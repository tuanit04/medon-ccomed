import React, { FC, useEffect, useState } from 'react';
import { message, Tag, Tooltip } from 'antd';
import { PlusOutlined, UserOutlined } from '@ant-design/icons';
import { RouteSchedule } from 'common/interfaces/routeSchedule.interface';
import { useDeleteSiteStaffSchedule } from 'hooks/schedule';
import { functionCodeConstants } from 'constants/functions';

interface EditableTagGroupProps {
  functionOb: any;
  onCreate: (routeId: string, refetchSchedules) => void;
  onSetRowSelected: (row: RouteSchedule) => void;
  routeSchedule: RouteSchedule;
  siteStaffWs: {
    workSessionIdValue: string;
    siteStaffIdValue: string;
    siteStaffNameValue: string;
    scheduleIdValue: string;
  };
  siteStaffList?: { workSessionIdValue; siteStaffIdValue; siteStaffNameValue; scheduleIdValue }[];
  workSessionIdSelected?: string;
  refetchSchedules: () => void;
  onDeleteSiteStaffSchedule: (variables) => void;
}

//create your forceUpdate hook
function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue(value => value + 1); // update the state to force render
}

const EditableTagGroup: FC<EditableTagGroupProps> = ({
  onCreate,
  functionOb,
  onSetRowSelected,
  routeSchedule,
  siteStaffWs,
  siteStaffList,
  workSessionIdSelected,
  refetchSchedules,
  onDeleteSiteStaffSchedule
}) => {
  const forceUpdate = useForceUpdate();
  const [tags, setTags] = useState([
    {} as { workSessionIdValue; siteStaffIdValue; siteStaffNameValue; scheduleIdValue }
  ]);
  useEffect(() => {
    let flag = false;
    if (siteStaffWs.siteStaffIdValue !== '') {
      tags?.map((tag, index) => {
        if (
          tag['siteStaffIdValue'] === siteStaffWs.siteStaffIdValue &&
          tag['workSessionIdValue'] === siteStaffWs.workSessionIdValue
        ) {
          flag = true;
        }
      });
      if (!flag) {
        setTags([...tags, siteStaffWs]);
      }
    }
  }, [siteStaffWs]);
  const {
    deleteSiteStaffSchedule,
    resultDeleteSiteStaffSchedule,
    isLoadingDeleteSiteStaffSchedule,
    errorDeleteSiteStaffSchedule
  } = useDeleteSiteStaffSchedule();
  const handleDeleteSiteStaffSchedule = async variables => {
    await onDeleteSiteStaffSchedule(variables);
    forceUpdate();
  };
  return (
    <>
      {siteStaffList?.map((tag, index) => {
        const workSessionValue = tag['workSessionIdValue'] !== undefined ? tag['workSessionIdValue'].toLowerCase() : '';
        const isShow = workSessionValue !== '';
        let workSessionColor = '';
        if (tag.workSessionIdValue === workSessionIdSelected) {
          workSessionColor = 'warning';
        }
        const isLongTag = (tag['siteStaffNameValue'] !== undefined ? tag['siteStaffNameValue'].length : 0) > 20;
        const tagElem = (
          <Tag
            className="edit-tag"
            color={workSessionColor !== '' ? workSessionColor : ''}
            icon={<UserOutlined />}
            key={tag['scheduleIdValue']}
            closable={index !== 0}
            onClose={() => {
              handleDeleteSiteStaffSchedule({
                data: tag['scheduleIdValue']
              });
            }}
          >
            <span>{isLongTag ? `${tag['siteStaffNameValue'].slice(0, 20)}...` : tag['siteStaffNameValue']}</span>
          </Tag>
        );
        return isLongTag && isShow ? (
          <Tooltip title={tag['siteStaffNameValue']} key={tag['siteStaffIdValue']}>
            {tagElem}
          </Tooltip>
        ) : isShow ? (
          tagElem
        ) : (
          ''
        );
      })}
      <Tag
        color="blue"
        className="site-tag-plus"
        onClick={() => {
          let checkPermis = functionOb[functionCodeConstants.TD_DM_LLV_CREATE] ? false : true;
          if (!checkPermis) {
            onSetRowSelected(routeSchedule);
            onCreate(routeSchedule.routeId, refetchSchedules);
          } else {
            message.warning('Bạn không có quyền thực hiện tính năng này.');
          }
        }}
      >
        <PlusOutlined /> Thêm
      </Tag>
    </>
  );
};
export default EditableTagGroup;
