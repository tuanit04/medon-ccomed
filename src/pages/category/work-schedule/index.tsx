import { RouteSchedule } from 'common/interfaces/routeSchedule.interface';
import { useAppState } from 'helpers';
import { useGetFacilityHome } from 'hooks/facility/useGetFacility';
import { useCopyScheduleOfStaff, useCreateScheduleOfStaffList } from 'hooks/schedule';
import { useGetSiteStaffsLazy } from 'hooks/site-staff';
import { useGetWorkSessions } from 'hooks/workSessions';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import './index.less';
import ScheduleCreateDialog from './scheduleCreateDialog';
import ScheduleCreateDialogv2 from './scheduleCreateDialogv2';
import ScheduleSearchForm from './scheduleSearchForm';
import ScheduleTable from './scheduleTable';

const SchedulePage: FC = () => {
  const functionOb = useAppState(state => state.user.functionObject);
  const [mcreateVisible, setCreateVisible] = useState(false);
  const [mcreateVisiblev2, setCreateVisiblev2] = useState(false);
  const [visiableCopyWorkScheduleDialog, setVisiableCopyWorkScheduleDialog] = useState(false);
  const [routeSchedule, setRouteSchedule] = useState({} as RouteSchedule);
  const [routeId, setRouteId] = useState('');
  const [staffSelected, setStaffSelected] = useState([
    {} as { workSessionIdValue; siteStaffIdValue; siteStaffNameValue; scheduleIdValue }
  ]);
  const [isFirstLoad, setFirstLoad] = useState(true);
  const [refetchSchedules, setRefetchSchedules] = useState(() => {});
  const [modifyVisible, setModifyVisible] = useState(false);
  const [authorizeVisible, setAuthorizeVisible] = useState(false);
  const facilityParentList = useGetFacilityHome();
  const [scheduleDate, setScheduleDate] = useState(moment().format('DD/MM/YYYY'));
  const [siteStaffIdRes, setSiteStaffIdRes] = useState('');
  const [parentFacilityId, setParentFacilityId] = useState('');
  const [facilityId, setFacilityId] = useState('');
  const [selectValues, setSelectValues] = useState({});
  const [filterStaff, setFilterStaff] = useState([]);
  const [siteStaffWs, setSiteStaffWs] = useState({
    workSessionIdValue: '',
    siteStaffIdValue: '',
    siteStaffNameValue: '',
    scheduleIdValue: ''
  });
  useEffect(() => {
    if (facilityParentList && isFirstLoad) {
      if (facilityParentList && facilityParentList.length > 0) {
        setParentFacilityId(facilityParentList[0].id);
        const filter: any = [];
        filter.push({
          id: 'parentFacilityId',
          value: facilityParentList[0].id,
          operation: '=='
        });
        filter.push({
          id: 'status',
          value: 1,
          operation: '=='
        });
        setFilterStaff(filter);
      }
    }
  }, [facilityParentList]);
  //const [dateOfRegistration, setDateOfRegistration] = useState(moment().format('DD/MM/YYYY'));

  const { siteStaffs, loadSiteStaffs, isLoadingSiteStaffs } = useGetSiteStaffsLazy();
  const { pagingWorkSessions, isLoadingWorkSessions, errorWorkSessions, refetchWorkSessions } = useGetWorkSessions({
    type: 'STAFF'
  });
  const {
    createScheduleOfStaffList,
    resultCreateScheduleOfStaff,
    isLoadingCreateScheduleOfStaff,
    errorCreateScheduleOfStaff
  } = useCreateScheduleOfStaffList();
  const handleCreateScheduleOfStaff = async values => {
    await createScheduleOfStaffList(values);
  };
  //Copy lịch làm việc
  const {
    copyScheduleOfStaff,
    resultCopyScheduleOfStaff,
    isLoadingCopyScheduleOfStaff,
    errorCopyScheduleOfStaff
  } = useCopyScheduleOfStaff();
  const handleCopyScheduleOfStaff = async values => {
    await copyScheduleOfStaff(values);
  };
  useEffect(() => {
    if (!isLoadingCreateScheduleOfStaff) {
    }
  }, [isLoadingCreateScheduleOfStaff]);

  return (
    <div className="main-content">
      <ScheduleSearchForm
        functionOb={functionOb}
        handleChangeDateOfRegistration={(dateOfRegistration: any) => {
          setScheduleDate(dateOfRegistration);
        }}
        facilityParentList={facilityParentList}
        onSearch={value => {
          const filter: any = [];
          if (value['parentFacilityId']) {
            setParentFacilityId(value['parentFacilityId']);
            filter.push({
              id: 'parentFacilityId',
              value: value['parentFacilityId'],
              operation: '=='
            });
          }
          if (value['facilityId'] != null) {
            setFacilityId(value['facilityId']);
            filter.push({
              id: 'facilityId',
              value: value['facilityId'],
              operation: '=='
            });
          }
          filter.push({
            id: 'status',
            value: 1,
            operation: '=='
          });
          setFilterStaff(filter);
          if (value['dateSchedule']) {
            setScheduleDate(value['dateSchedule']);
          }
        }}
      />
      <ScheduleTable
        functionOb={functionOb}
        onCreateSchedule={() => setCreateVisiblev2(true)}
        setVisiableCopyWorkScheduleDialog={setVisiableCopyWorkScheduleDialog}
        visiableCopyWorkScheduleDialog={visiableCopyWorkScheduleDialog}
        onCopy={async values => {
          await handleCopyScheduleOfStaff({
            data: {
              scheduleDate: scheduleDate,
              parentFacilityId: parentFacilityId,
              facilityId: facilityId,
              fromDate: moment(String(values['startDate'])).format('DD/MM/YYYY'),
              toDate: moment(String(values['endDate'])).format('DD/MM/YYYY')
            }
          });
          setVisiableCopyWorkScheduleDialog(false);
        }}
        onCreate={(routeId: string, refetchSchedules) => {
          setRefetchSchedules(refetchSchedules);
          setRouteId(routeId);
          setCreateVisible(true);
          loadSiteStaffs({
            variables: {
              page: 1,
              filtered: filterStaff
            }
          });
        }}
        onAuthorize={values => {
          setAuthorizeVisible(true);
        }}
        onModify={values => {
          setSelectValues(values);
          setModifyVisible(true);
        }}
        onSetStaffSelected={values => {
          //setStaffSelected(values);
        }}
        onDelete={value => {}}
        siteStaffList={[]}
        workSessionList={pagingWorkSessions.data}
        onSetRowSelected={values => {
          setRouteSchedule(values);
        }}
        siteStaffWs={siteStaffWs}
        parentFacilityId={parentFacilityId}
        facilityId={facilityId}
        scheduleDate={scheduleDate}
        routeId={routeId}
        dateOfRegistration={scheduleDate}
        siteStaffIdRes={siteStaffIdRes}
      />

      <ScheduleCreateDialog
        isLoadingSiteStaffs={isLoadingSiteStaffs}
        visible={mcreateVisible}
        onCancel={() => setCreateVisible(false)}
        onCreate={async (value: {}) => {
          setSiteStaffWs({
            workSessionIdValue: value['workSessionIdValue'],
            siteStaffIdValue: value['siteStaffIdValue'],
            siteStaffNameValue: value['siteStaffNameValue'],
            scheduleIdValue: value['scheduleIdValue']
          });
          await handleCreateScheduleOfStaff({
            data: {
              staffIds: value['siteStaffIdValue'],
              scheduleDate: scheduleDate,
              businessId: 'dbd765ee-6c70-4696-82ed-1e95cf4f0253',
              workSessionId: value['workSessionIdValue'],
              routeId: routeSchedule?.routeId
            }
          });
          setSiteStaffIdRes(value['siteStaffIdValue']);
          setCreateVisible(false);
        }}
        workSessions={pagingWorkSessions.data}
        siteStaffs={siteStaffs}
      />
      <ScheduleCreateDialogv2
        isLoadingSiteStaffs={isLoadingSiteStaffs}
        visible={mcreateVisiblev2}
        onCancel={() => setCreateVisiblev2(false)}
        onCreate={async (value: {}) => {
          setSiteStaffWs({
            workSessionIdValue: value['workSessionIdValue'],
            siteStaffIdValue: value['siteStaffIdValue'],
            siteStaffNameValue: value['siteStaffNameValue'],
            scheduleIdValue: value['scheduleIdValue']
          });
          await handleCreateScheduleOfStaff({
            data: {
              staffIds: value['siteStaffIdValue'],
              scheduleDate: scheduleDate,
              businessId: 'dbd765ee-6c70-4696-82ed-1e95cf4f0253',
              workSessionId: value['workSessionIdValue'],
              routeId: routeSchedule?.routeId
            }
          });
          setSiteStaffIdRes(value['siteStaffIdValue']);
          setCreateVisiblev2(false);
        }}
        workSessions={pagingWorkSessions.data}
        siteStaffs={siteStaffs}
      />
    </div>
  );
};
export default SchedulePage;
