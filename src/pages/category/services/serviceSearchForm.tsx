import React, { FC, useState } from 'react';
import useGetServiceForm from './useGetServiceForm';
import { Province } from 'common/interfaces/province.interface';
import { ServiceType } from 'common/interfaces/serviceType.interface';
import { Facility } from 'common/interfaces/facility.interface';
import { useGetFacilitysHos } from 'hooks/facility';

interface ServiceSearchProps {
  functionOb: any;
  provinceList: Province[];
  parentFacilityList: Facility[];
  serviceTypeList: ServiceType[];
  onSearch: ({}) => void;
  defaultProvince: string;
  defaultFacility: string;
  handelChangeProvince: (value) => void;
}
const ServiceSearchForm: FC<ServiceSearchProps> = ({
  functionOb,
  provinceList,
  parentFacilityList,
  serviceTypeList,
  onSearch,
  defaultFacility,
  defaultProvince,
  handelChangeProvince
}) => {
  const [facilityId, setFacilityId] = useState('');
  const [provinceId, setProvinceId] = useState('');
  const [status, setStatus] = useState('');
  const [serviceTypeId, setServiceTypeId] = useState('');
  const { Form, form, Provinces, ServiceType, FacilityParent, Status, Buttons } = useGetServiceForm({
    name: 'searchForm',
    responsive: true
  });

  return (
    <Form>
      <Provinces
        defaultProvince={defaultProvince}
        provinceList={provinceList}
        handleChangeProvince={provinceId => {
          setProvinceId(provinceId);
          handelChangeProvince(provinceId);
        }}
      />
      <ServiceType
        serviceTypeList={serviceTypeList}
        handleChangeServiceType={serviceTypeId => {
          setServiceTypeId(serviceTypeId);
        }}
      />
      <FacilityParent
        defaultFacility={defaultFacility}
        parentFacilityList={parentFacilityList}
        handleChangeParentFacility={facilityId => {
          setFacilityId(facilityId);
        }}
      />
      <Status
        handleChangeStatus={data => {
          setStatus(data);
        }}
      />
      <Buttons
        functionOb={functionOb}
        onSearch={() => {
          onSearch({ provinceId, serviceTypeId, facilityId, status });
        }}
        resetFields={() => {
          form.resetFields();
          handelChangeProvince(defaultProvince);
          onSearch({ provinceId: defaultProvince, serviceTypeId: '', facilityId: '', status: '' });
        }}
      />
    </Form>
  );
};

export default ServiceSearchForm;
