import React, { FC, useEffect, useState } from 'react';
import { Button, Table, Tag } from 'antd';
import { Role } from 'interface/permission/role.interface';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { useGetServicesLazy } from 'hooks/services/useGetServices';
import { ServiceView } from 'common/interfaces/service.interface';
import { CheckCircleTwoTone } from '@ant-design/icons';

interface ServiceTableProps {
  onDetail: (serviceObj: ServiceView) => void;
  onAuthorize?: (row: Role) => void;
  provinceIdSearch: FilteredInput;
  serviceTypeIdSearch: FilteredInput;
  facilityIdSearch: FilteredInput;
  statusSearch: FilteredInput;
}

const ServiceTable: FC<ServiceTableProps> = ({
  onDetail,
  onAuthorize,
  provinceIdSearch,
  serviceTypeIdSearch,
  facilityIdSearch,
  statusSearch
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  let filtered: FilteredInput[] = [];
  // if (provinceIdSearch['value'] !== '') {
  //   filtered.push(provinceIdSearch);
  // }
  if (serviceTypeIdSearch['value'] !== '') {
    filtered.push(serviceTypeIdSearch);
  }
  if (facilityIdSearch['value'] !== '') {
    filtered.push(facilityIdSearch);
  }
  if (statusSearch['value'] !== '') {
    filtered.push(statusSearch);
  }
  const variable = { page, pageSize, filtered };
  //Lấy danh sách dịch vụ
  const { services, isLoadingServices, loadServices } = useGetServicesLazy(variable);
  useEffect(() => {
    if (facilityIdSearch['value'] != '') {
      loadServices({ variables: variable });
    }
  }, [provinceIdSearch, facilityIdSearch, statusSearch, page, pageSize]);

  useEffect(() => {
    setPagination({ current: page, pageSize: pageSize, total: facilityIdSearch['value'] ? services['records'] : 0 });
  }, [services]);
  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const [pagination, setPagination] = useState({});
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    setPage(pagination['current']);
    setPageSize(pagination['pageSize']);
    // loadServices({variables: variable})
  };
  return (
    <Table
      rowKey="id"
      dataSource={facilityIdSearch['value'] ? services.data : []}
      onChange={handleTableChange}
      pagination={pagination}
      scroll={{ x: 450, y: 'calc(92vh - 250px)' }}
      style={{ height: 'calc(97vh - 135px)' }}
      title={() => (
        <>
          <div style={{ float: 'left' }}>
            <label className="lblTableTxt">
              <Tag color="blue">Tổng số bản ghi : {facilityIdSearch['value'] ? services.records : ''}</Tag>
            </label>
          </div>
          <div style={{ clear: 'both' }}></div>
        </>
      )}
    >
      <Table.Column<ServiceView>
        title="STT"
        width={40}
        align="center"
        render={(value, item, index) => (page - 1) * pageSize + index + 1}
      />
      <Table.Column<ServiceView>
        title="Tên dịch vụ"
        width={150}
        render={(_, row) => (
          <a
            style={{ textDecoration: 'none', color: '#206ad2', fontSize: '14px' }}
            onClick={() => {
              onDetail(row);
            }}
          >
            {row.serviceName}
          </a>
        )}
      />
      <Table.Column<ServiceView>
        title="Mã dịch vụ"
        align="center"
        width={80}
        render={(_, { serviceCode }) => serviceCode}
      />
      <Table.Column<ServiceView>
        title="Đơn giá gốc"
        align="center"
        width={80}
        render={(_, { originPrice }) => originPrice}
      />
      <Table.Column<ServiceView>
        title="Đơn giá HĐ (Tại PK/BV)"
        width={80}
        align="center"
        render={(_, { hosSalePrice }) => hosSalePrice}
      />
      <Table.Column<ServiceView>
        title="Đơn giá HĐ (Tại nhà)"
        width={80}
        align="center"
        render={(_, { homeSalePrice }) => homeSalePrice}
      />
      <Table.Column<ServiceView>
        title="Profile"
        width={80}
        align="center"
        render={(_, { isProfile }) => (isProfile === 1 ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : '')}
      />
      <Table.Column<ServiceView> title="Nhóm dịch vụ" width={150} render={(_, { serviceType }) => serviceType} />
      <Table.Column<ServiceView> title="Đơn vị thực hiện" width={150} render={(_, { facilityName }) => facilityName} />
      <Table.Column<ServiceView>
        title="Mã HĐ KCB"
        width={80}
        align="center"
        render={(_, { contractCode }) => contractCode}
      />
      <Table.Column<ServiceView>
        title="Hiển thị KQ"
        align="center"
        width={80}
        render={(_, { isDisplay }) => (isDisplay === 1 ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : '')}
      />
      <Table.Column<ServiceView>
        title="Sử dụng"
        width={80}
        align="center"
        render={(_, { isUsed }) => (isUsed === 1 ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : '')}
      />
      <Table.Column<ServiceView>
        title="Thời gian đồng bộ"
        align="center"
        width={100}
        render={(_, { returnDuration }) => returnDuration}
      />
      <Table.Column<ServiceView>
        title="Hành động"
        align="center"
        width={80}
        render={(_, row) => (
          <Button type="primary">
            <a
              style={{ textDecoration: 'none', color: '#fafafa' }}
              onClick={() => {
                onDetail(row);
              }}
            >
              Xem chi tiết
            </a>
          </Button>
        )}
      />
    </Table>
  );
};

export default ServiceTable;
