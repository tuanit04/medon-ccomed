import React, { FC, useEffect, useState } from 'react';
import ServiceTable from './serviceTable';
import ServiceSearchForm from './serviceSearchForm';
import ServiceDetailDialog from './serviceDetailDialog';
import { useGetProvinces } from 'hooks/province/useGetProvince';
import { useGetServiceType } from 'hooks/services/useGetServiceType';
import { useGetFacilityParents, useGetFacilitysHos } from 'hooks/facility';
import { ServiceView } from 'common/interfaces/service.interface';
import { useAppState } from 'helpers';

const ServicePage: FC = () => {
  const functionOb = useAppState(state => state.user.functionObject);
  const [detailVisible, setDetailVisible] = useState(false);
  const [authorizeVisible, setAuthorizeVisible] = useState(false);
  const [serviceObj, setServiceObj] = useState({} as ServiceView);
  const { pagingProvinces, isLoadingProvinces, errorProvinces, refetchProvinces } = useGetProvinces();
  const { pagingServiceType, isLoadingServiceType, errorServiceType, refetchServiceType } = useGetServiceType();
  const { facilitiesHos, isLoadingFacilitiesHos, loadFacilitiesHos } = useGetFacilitysHos();
  const [defaultProvince, setDefaultProvince] = useState('');
  const [defaultFacility, setDefaultFacility] = useState('');
  const [provinceId, setProvinceId] = useState('');
  // useEffect(()=>{
  //   if(parentFacilities && parentFacilities[0]){
  //     setProvinceIdSearch({...provinceIdSearch,value:parentFacilities[0].provinceIds});
  //     setDefaultProvince(parentFacilities[0].provinceIds);
  //     setFacilityIdSearch({ ...facilityIdSearch, value: parentFacilities[0].id });
  //     setDefaultFacility(parentFacilities[0].id);
  //   }
  // },[parentFacilities])

  useEffect(() => {
    if (pagingProvinces && pagingProvinces?.data && pagingProvinces?.data.length > 0) {
      setDefaultProvince(pagingProvinces?.data[0]?.id);
      setProvinceIdSearch({ ...provinceIdSearch, value: pagingProvinces?.data[0].id });
      loadFacilitiesHos({
        variables: {
          page: 1,
          filtered: [
            {
              id: 'status',
              value: '1',
              operation: '=='
            },
            {
              id: 'provinceIds',
              value: pagingProvinces?.data[0]?.id,
              operation: '=='
            }
          ]
        }
      });
    }
  }, [pagingProvinces]);

  useEffect(() => {
    if (facilitiesHos && facilitiesHos.length > 0) {
      setDefaultFacility(facilitiesHos[0].id);
      setFacilityIdSearch({ ...facilityIdSearch, value: facilitiesHos[0].id });
    } else {
      setDefaultFacility('');
    }
  }, [facilitiesHos]);

  useEffect(() => {
    if (provinceId) {
      loadFacilitiesHos({
        variables: {
          page: 1,
          filtered: [
            {
              id: 'status',
              value: '1',
              operation: '=='
            },
            {
              id: 'provinceIds',
              value: provinceId,
              operation: '=='
            }
          ]
        }
      });
    }
  }, [provinceId]);

  const [provinceIdSearch, setProvinceIdSearch] = useState({
    id: 'provinceId',
    value: '',
    operation: '=='
  });
  const [serviceTypeIdSearch, setServiceTypeIdSearch] = useState({
    id: 'serviceTypeId',
    value: '',
    operation: '=='
  });
  const [facilityIdSearch, setFacilityIdSearch] = useState({
    id: 'facilityId',
    value: '',
    operation: '=='
  });
  const [statusSearch, setStatusSearch] = useState({
    id: 'status',
    value: '',
    operation: '=='
  });
  return (
    <div className="main-content">
      <ServiceSearchForm
        functionOb={functionOb}
        provinceList={pagingProvinces?.data}
        parentFacilityList={facilitiesHos}
        serviceTypeList={pagingServiceType.data}
        defaultProvince={defaultProvince}
        defaultFacility={defaultFacility}
        handelChangeProvince={value => {
          setProvinceId(value);
        }}
        onSearch={value => {
          if (value['facilityId']) {
            setFacilityIdSearch({ ...facilityIdSearch, value: value['facilityId'] });
          } else if (value['facilityId'] === '') {
            setFacilityIdSearch({ ...facilityIdSearch, value: defaultFacility });
          }
          if (value['serviceTypeId']) {
            setServiceTypeIdSearch({ ...serviceTypeIdSearch, value: value['serviceTypeId'] });
          } else if (value['serviceTypeId'] === '') {
            setServiceTypeIdSearch({ ...serviceTypeIdSearch, value: '' });
          }
          // if (value['provinceId']) {
          //   setProvinceIdSearch({ ...provinceIdSearch, value: value['provinceId'] });
          // } else if (value['provinceId'] === '') {
          //   setProvinceIdSearch({ ...provinceIdSearch, value: '' });
          // }
          if (value['status']) {
            setStatusSearch({ ...statusSearch, value: value['status'] });
          } else if (value['status'] === '') {
            setStatusSearch({ ...statusSearch, value: '' });
          }
        }}
      />
      <ServiceDetailDialog
        serviceObj={serviceObj}
        visible={detailVisible}
        onCancel={() => setDetailVisible(false)}
        onDetail={() => setDetailVisible(false)}
      />
      <ServiceTable
        onDetail={serviceObj => {
          setServiceObj(serviceObj);
          setDetailVisible(true);
        }}
        onAuthorize={values => {
          setServiceObj(serviceObj);
          setAuthorizeVisible(true);
        }}
        provinceIdSearch={provinceIdSearch}
        serviceTypeIdSearch={serviceTypeIdSearch}
        facilityIdSearch={facilityIdSearch}
        statusSearch={statusSearch}
      />
    </div>
  );
};

export default ServicePage;
