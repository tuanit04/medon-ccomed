import React, { FC, useState } from 'react';
import { Form, Col, Row, Select, Card, Table, Button, Badge } from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import { Province } from 'common/interfaces/province.interface';
import { ServiceType } from 'common/interfaces/serviceType.interface';
import { Facility } from 'common/interfaces/facility.interface';
import { ServiceView } from 'common/interfaces/service.interface';
import { PackageView } from 'common/interfaces/packageView.interface';
import { Services } from 'common/interfaces/services.interface';
import { CheckCircleTwoTone, ClearOutlined, SearchOutlined } from '@ant-design/icons';
import { functionCodeConstants } from 'constants/functions';

const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
  xxl: 12
};

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 }
};

interface RoleFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  /** Initial form data */
  serviceDetailObj?: ServiceView;
}

export default function useGetExaminationPackageForm({
  required = false,
  responsive = false,
  name = 'form',
  serviceDetailObj
}: RoleFormProps) {
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<FormProps>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={serviceDetailObj}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //Tinh/TP
  interface ProvincesProps {
    provinceList: Province[];
    handleChangeProvince: (provinceId: string) => void;
    defaultProvince: string;
  }
  const Provinces: FC<ProvincesProps> = ({ provinceList, handleChangeProvince, defaultProvince }) => {
    const provinces = (
      <Form.Item name="provinceId" label="Tỉnh/TP">
        <Select
          className="slcSearch"
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeProvince}
          placeholder="-- Chọn Tỉnh/TP --"
          defaultValue={defaultProvince}
        >
          <Select.Option key="1" value="">
            -- Tất cả --
          </Select.Option>
          {provinceList?.map(province => (
            <Select.Option key={province.id} value={province.id}>
              {province.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{provinces}</Col> : provinces;
  };
  interface ServiceTypeProps {
    serviceTypeList: ServiceType[];
    handleChangeServiceType: (serviceTypeId: string) => void;
  }
  const ServiceType: FC<ServiceTypeProps> = ({ serviceTypeList, handleChangeServiceType }) => {
    const serviceType = (
      <Form.Item name="serviceType" label="Nhóm dịch vụ">
        <Select
          showSearch
          // className="slcSearch"
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeServiceType}
          placeholder="-- Chọn Nhóm dịch vụ --"
        >
          <Select.Option key="1" value="">
            -- Tất cả --
          </Select.Option>
          {serviceTypeList?.map(serviceType => (
            <Select.Option key={serviceType.id} value={serviceType.id}>
              {serviceType.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{serviceType}</Col> : serviceType;
  };

  interface FacilityParentProps {
    parentFacilityList: Facility[];
    handleChangeParentFacility: (parentFacilityId: string) => void;
    defaultFacility: string;
  }
  const FacilityParent: FC<FacilityParentProps> = ({
    parentFacilityList,
    handleChangeParentFacility,
    defaultFacility
  }) => {
    const facilityParent = (
      <Form.Item name="facilityParent" label="Đơn vị đề xuất">
        <Select
          showSearch
          className="slcSearch"
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          defaultValue={parentFacilityList && parentFacilityList.length > 0 ? parentFacilityList[0].id : ''}
          onChange={handleChangeParentFacility}
          placeholder="-- Chọn Đơn vị đề xuất --"
        >
          {parentFacilityList?.map(parentFacility => (
            <Select.Option key={parentFacility.id} value={parentFacility.id}>
              {parentFacility.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{facilityParent}</Col> : facilityParent;
  };

  //Trang thai
  interface StatusProps {
    handleChangeStatus?: (value: any) => void;
  }
  const Status: FC<StatusProps> = ({ handleChangeStatus }) => {
    const status = (
      <Form.Item
        name="status"
        label="Trạng thái"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select defaultValue="" onChange={handleChangeStatus}>
          <Select.Option key="1" value="">
            <Badge color="blue" status="processing" text="-- Tất cả --" />
          </Select.Option>
          <Select.Option key="2" value={1}>
            <Badge color="green" status="processing" text="Sử dụng" />
          </Select.Option>
          <Select.Option key="3" value={2}>
            <Badge color="red" status="processing" text="Không sử dụng" />
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{status}</Col> : status;
  };

  const ServiceDetail: FC = () => {
    const serviceDetail = (
      <Form.Item name="status">
        <Table
          style={{ border: '1px solid #ccc' }}
          rowKey="id"
          title={() => {
            return (
              <div style={{ marginBottom: '10px' }}>
                &nbsp;&nbsp;
                <span style={{ color: '#d13e3e', fontWeight: 'bold', fontSize: '18px' }}>
                  {serviceDetailObj?.serviceName}
                </span>
              </div>
            );
          }}
          dataSource={serviceDetailObj === undefined ? [] : [serviceDetailObj]}
          scroll={{ x: 500 }}
        >
          <Table.Column<ServiceType> title="STT" width={30} align="center" render={(value, item, index) => 1} />
          <Table.Column<ServiceView> title="Tên dịch vụ" width={100} render={_ => serviceDetailObj?.serviceName} />
          <Table.Column<ServiceView>
            title="Mã dịch vụ"
            width={100}
            align="center"
            render={_ => serviceDetailObj?.serviceCode}
          />
          <Table.Column<ServiceView>
            title="Đơn giá gốc"
            width={100}
            align="center"
            render={_ => serviceDetailObj?.originPrice}
          />
          <Table.Column<ServiceView>
            title="Đơn giá HĐ (Tại PK/BV)"
            width={100}
            render={(_, { hosSalePrice }) => hosSalePrice}
          />
          <Table.Column<ServiceView>
            title="Đơn giá HĐ (Tại nhà)"
            width={100}
            align="center"
            render={(_, { homeSalePrice }) => homeSalePrice}
          />
          <Table.Column<ServiceView>
            title="Profile"
            width={100}
            align="center"
            render={(_, { isProfile }) => (isProfile === 1 ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : '')}
          />
          <Table.Column<ServiceView> title="Nhóm dịch vụ" width={100} render={(_, { serviceType }) => serviceType} />
          <Table.Column<ServiceView>
            title="Đơn vị thực hiện"
            width={100}
            render={(_, { facilityName }) => facilityName}
          />
          <Table.Column<ServiceView>
            title="Mã HĐ KCB"
            width={100}
            align="center"
            render={(_, { contractCode }) => contractCode}
          />
          <Table.Column<ServiceView>
            title="Hiển thị KQ"
            align="center"
            width={100}
            render={(_, { isDisplay }) => (isDisplay === 1 ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : '')}
          />
          <Table.Column<ServiceView>
            title="Sử dụng"
            align="center"
            width={100}
            render={(_, { isUsed }) => (isUsed === 1 ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : '')}
          />
          <Table.Column<ServiceView>
            title="Thời gian đồng bộ"
            width={100}
            align="center"
            render={(_, { returnDuration }) => returnDuration}
          />
        </Table>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{serviceDetail}</Col> : serviceDetail;
  };
  //Ý nghĩa chung
  const GeneralMean: FC = () => {
    const generalMean = (
      <Form.Item name="status">
        <Card style={{ width: '100%' }} title="Ý nghĩa chung">
          <p>{serviceDetailObj?.generalMeaning}</p>
        </Card>
      </Form.Item>
    );

    return generalMean;
  };
  //Ý nghĩa chuyên môn
  const ProfessionalMean: FC = () => {
    const professionalMean = (
      <Form.Item name="status">
        <Card style={{ width: '100%' }} title="Ý nghĩa chuyên môn">
          <p>{serviceDetailObj?.detailMeaning}</p>
        </Card>
      </Form.Item>
    );

    return professionalMean;
  };

  //Dịch vụ bao gồm
  interface CompositeServicesProps {
    pagingCompositeServices: any;
  }
  const CompositeServices: FC<CompositeServicesProps> = ({ pagingCompositeServices }) => {
    const compositeServices = (
      <Form.Item name="status">
        <Table
          style={{ border: '1px solid #ccc' }}
          rowKey="id"
          title={() => {
            return (
              <div>
                <span style={{ fontWeight: 'bold', fontSize: '15px' }}>Dịch vụ bao gồm</span>
                <span style={{ color: '#f35959', fontWeight: 'bold' }}> ({pagingCompositeServices['records']})</span>
              </div>
            );
          }}
          dataSource={pagingCompositeServices?.data}
          scroll={{ x: 500 }}
        >
          <Table.Column<Services> title="Tên dịch vụ" width={100} render={(_, { name }) => name} />
          <Table.Column<Services> title="Mã dịch vụ" width={100} align="center" render={(_, { code }) => code} />
          <Table.Column<Services>
            title="Ý nghĩa chung"
            width={100}
            render={(_, { generalMeaning }) => generalMeaning}
          />
          <Table.Column<Services>
            title="Ý nghĩa chuyên môn"
            width={100}
            render={(_, { detailMeaning }) => detailMeaning}
          />
          <Table.Column<Services>
            title="Hiển thị KQ"
            width={100}
            align="center"
            render={(_, { isDisplay }) => (isDisplay === 1 ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : '')}
          />
          <Table.Column<Services>
            title="Sử dụng"
            width={100}
            align="center"
            render={(_, { isUsed }) => (isUsed === 1 ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : '')}
          />
        </Table>
      </Form.Item>
    );
    return compositeServices;
  };
  //Dịch vụ chỉ định
  interface RelateServicesProps {
    pagingRelateServices: any;
  }
  const RelateServices: FC<RelateServicesProps> = ({ pagingRelateServices }) => {
    const relateServices = (
      <Form.Item name="status">
        <Table
          style={{ border: '1px solid #ccc' }}
          title={() => {
            return (
              <div>
                <span style={{ fontWeight: 'bold', fontSize: '15px' }}>Dịch vụ chỉ định kèm theo</span>
                <span style={{ color: '#f35959', fontWeight: 'bold' }}> ({pagingRelateServices['records']})</span>
              </div>
            );
          }}
          rowKey="id"
          dataSource={pagingRelateServices?.data}
          scroll={{ x: 500 }}
        >
          <Table.Column<ServiceView>
            title="Tên dịch vụ"
            width={100}
            render={(_, { serviceName }) => ({ serviceName })}
          />
          <Table.Column<ServiceView>
            title="Mã dịch vụ"
            width={100}
            align="center"
            render={(_, { serviceCode }) => serviceCode}
          />
          <Table.Column<ServiceView>
            title="Đơn giá gốc"
            width={100}
            align="center"
            render={(_, { originPrice }) => originPrice}
          />
          <Table.Column<ServiceView>
            title="Đơn giá HĐ - tại PK/BV"
            width={100}
            align="center"
            render={(_, { hosSalePrice }) => hosSalePrice}
          />
          <Table.Column<ServiceView>
            title="Đơn giá HĐ - tại nhà"
            align="center"
            width={100}
            render={(_, { homeSalePrice }) => homeSalePrice}
          />
          <Table.Column<ServiceView> title="Ghi chú" width={100} render={(_, { hosSalePrice }) => ''} />
        </Table>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{relateServices}</Col> : relateServices;
  };

  //Nhóm dịch vụ
  interface ServiceTypeTableProps {
    pagingServiceType: any;
  }
  const ServiceTypeTable: FC<ServiceTypeTableProps> = ({ pagingServiceType }) => {
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(10);
    const serviceTypeTable = (
      <Form.Item name="status">
        <Table
          title={() => {
            return (
              <div>
                <span style={{ fontWeight: 'bold', fontSize: '15px' }}>Nhóm dịch vụ</span>
                <span style={{ color: '#f35959', fontWeight: 'bold' }}> ({pagingServiceType['records']})</span>
              </div>
            );
          }}
          style={{ border: '1px solid #ccc' }}
          rowKey="id"
          dataSource={pagingServiceType.data}
          scroll={{ x: 500 }}
        >
          <Table.Column<ServiceType>
            title="STT"
            width={30}
            align="center"
            render={(value, item, index) => (page - 1) * pageSize + index + 1}
          />
          <Table.Column<ServiceType> title="Tên nhóm" width={100} render={(_, { name }) => name} />
          <Table.Column<ServiceType> title="Mã nhóm" width={100} render={(_, { name }) => name} />
          <Table.Column<ServiceType> title="Số lượng dịch vụ" width={100} render={(_, { name }) => name} />
        </Table>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{serviceTypeTable}</Col> : serviceTypeTable;
  };
  //Đơn vị thực hiện dịch vụ
  interface FacilitiesUseServiceProps {
    pagingFacilitiesUseService: any;
  }
  const FacilitiesUseService: FC<FacilitiesUseServiceProps> = ({ pagingFacilitiesUseService }) => {
    const facility = (
      <Form.Item name="status">
        <Table
          title={() => {
            return (
              <div>
                <span style={{ fontWeight: 'bold', fontSize: '15px' }}>Đơn vị đang thực hiện dịch vụ</span>
                <span style={{ color: '#f35959', fontWeight: 'bold' }}> ({pagingFacilitiesUseService['records']})</span>
              </div>
            );
          }}
          style={{ border: '1px solid #ccc' }}
          rowKey="id"
          dataSource={pagingFacilitiesUseService.data}
          scroll={{ x: 500 }}
        >
          <Table.Column<ServiceView>
            title="Đơn vị, phòng khám, bệnh viện"
            width={100}
            render={(_, { facilityName }) => facilityName}
          />
          <Table.Column<ServiceView>
            title="Giá gốc"
            width={100}
            align="center"
            render={(_, { originPrice }) => originPrice}
          />
          <Table.Column<ServiceView>
            title="Đơn giá HĐ - tại PK/BV"
            width={100}
            align="center"
            render={(_, { hosSalePrice }) => hosSalePrice}
          />
          <Table.Column<ServiceView>
            title="Đơn giá HĐ - tại nhà"
            width={100}
            align="center"
            render={(_, { homeSalePrice }) => homeSalePrice}
          />
          <Table.Column<ServiceView>
            title="Thời gian có kết quả"
            align="center"
            width={100}
            render={(_, { returnDuration }) => returnDuration}
          />
        </Table>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{facility}</Col> : facility;
  };
  //Dịch vụ trong gói khám đang bán
  interface PackageContainProps {
    pagingPackageContain: any;
  }
  const PackageContain: FC<PackageContainProps> = ({ pagingPackageContain }) => {
    const packageContain = (
      <Form.Item name="status">
        <Table
          title={() => {
            return (
              <div>
                <span style={{ fontWeight: 'bold', fontSize: '15px' }}>Dịch vụ trong gói khám đang bán</span>
                <span style={{ color: '#f35959', fontWeight: 'bold' }}> ({pagingPackageContain['records']})</span>
              </div>
            );
          }}
          style={{ border: '1px solid #ccc' }}
          rowKey="id"
          dataSource={pagingPackageContain.data}
          scroll={{ x: 500 }}
        >
          <Table.Column<PackageView>
            title="Banner gói khám"
            align="center"
            width={100}
            render={(_, { packageImage }) => packageImage}
          />
          <Table.Column<PackageView>
            title="Giá gốc"
            width={100}
            align="center"
            render={(_, { originalPrice }) => originalPrice}
          />
          <Table.Column<PackageView> title="Tên gói khám" width={100} render={(_, { packageName }) => packageName} />
          <Table.Column<PackageView>
            title="Mã gói khám"
            width={100}
            align="center"
            render={(_, { packageHisId }) => packageHisId}
          />
        </Table>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{packageContain}</Col> : packageContain;
  };

  interface ButtonsProps {
    functionOb: any;
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ functionOb, onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_DICHVU_TIMKIEM_THONGKE] ? false : true}
          onClick={() => {
            onSearch();
          }}
          icon={<SearchOutlined />}
          type="primary"
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_DICHVU_TIMKIEM_THONGKE] ? false : true}
          onClick={resetFields}
          icon={<ClearOutlined />}
        >
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{buttons}</Col> : buttons;
  };

  return {
    form: formInstance,
    Form: WrappedForm,
    Provinces,
    ServiceType,
    FacilityParent,
    Status,
    ServiceDetail,
    GeneralMean,
    ProfessionalMean,
    CompositeServices,
    RelateServices,
    ServiceTypeTable,
    FacilitiesUseService,
    PackageContain,
    Buttons
  };
}
