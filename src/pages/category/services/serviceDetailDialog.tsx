import React, { FC, useEffect } from 'react';
import { Modal } from 'antd';
import useGetServiceForm from './useGetServiceForm';
import { ServiceView } from 'common/interfaces/service.interface';
import { useGetServiceType } from 'hooks/services/useGetServiceType';
import { useGetPackagesContainLazy } from 'hooks/package/useGetPackageContain';
import { useGetCompositeServicesLazy } from 'hooks/services/useGetCompositeServices';
import { useGetRelateServicesLazy } from 'hooks/services/useFindRelateServices';
import { useGetFacilitiesUseServiceLazy } from 'hooks/services/useGetFindFacilitiesUseService';

interface ServiceModifyDialogProps {
  serviceObj: ServiceView;
  visible: boolean;
  onDetail: (values: ServiceView) => void;
  onCancel: () => void;
}

const ServiceModifyDialog: FC<ServiceModifyDialogProps> = ({ onDetail, onCancel, visible, serviceObj }) => {
  const { pagingServiceType, isLoadingServiceType, errorServiceType, refetchServiceType } = useGetServiceType();
  const {
    pagingFacilitiesUseService,
    isLoadingFacilitiesUseService,
    refetchFacilitiesUseServices
  } = useGetFacilitiesUseServiceLazy({
    serviceId: serviceObj.serviceId
  });
  const { pagingPackageContain, isLoadingPackageContain, refetchPackageContain } = useGetPackagesContainLazy({
    serviceId: serviceObj.serviceId
  });
  const { pagingCompositeServices, isLoadingCompositeServices, refetchCompositeServices } = useGetCompositeServicesLazy(
    {
      serviceId: serviceObj.serviceId
    }
  );
  const { pagingRelateServices, isLoadingRelateServices, loadServices } = useGetRelateServicesLazy({
    serviceId: serviceObj.serviceId
  });

  useEffect(() => {
    if (serviceObj && serviceObj.serviceId) {
      loadServices({
        variables: {
          serviceId: serviceObj.serviceId
        }
      });
      refetchCompositeServices({
        variables: {
          serviceId: serviceObj.serviceId
        }
      });
      refetchPackageContain({
        variables: {
          serviceId: serviceObj.serviceId
        }
      });
      refetchFacilitiesUseServices({
        variables: {
          serviceId: serviceObj.serviceId
        }
      });
    }
  }, [serviceObj]);

  const {
    Form,
    form,
    GeneralMean,
    ProfessionalMean,
    ServiceDetail,
    CompositeServices,
    RelateServices,
    ServiceTypeTable,
    FacilitiesUseService,
    PackageContain
  } = useGetServiceForm({
    name: 'modifyForm',
    required: true,
    serviceDetailObj: serviceObj
  });

  const onSubmit = async () => {
    const values: any = await form.validateFields();
    onDetail(values);
  };

  return (
    <Modal
      centered
      cancelButtonProps={{ style: { display: 'none' } }}
      style={{ marginTop: '10px' }}
      title="Thông tin chi tiết dịch vụ"
      width={1300}
      visible={visible}
      onOk={onSubmit}
      onCancel={onCancel}
    >
      <Form>
        <ServiceDetail />
        <div>
          <div style={{ float: 'left', width: '49%' }}>
            <GeneralMean />
          </div>
          <div style={{ float: 'right', width: '49%' }}>
            <ProfessionalMean />
          </div>
          <div style={{ clear: 'both' }}></div>
        </div>
        <CompositeServices pagingCompositeServices={pagingCompositeServices} />
        <RelateServices pagingRelateServices={pagingRelateServices} />
        <ServiceTypeTable pagingServiceType={pagingServiceType} />
        <PackageContain pagingPackageContain={pagingPackageContain} />
        <FacilitiesUseService pagingFacilitiesUseService={pagingFacilitiesUseService} />
      </Form>
    </Modal>
  );
};

export default ServiceModifyDialog;
