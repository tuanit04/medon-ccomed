import React, { FC, useEffect, useState } from 'react';
import { Button, Table, Modal, Divider, Popover, Image, Tag } from 'antd';
import {
  CheckCircleOutlined,
  CheckCircleTwoTone,
  ExclamationCircleOutlined,
  MinusCircleOutlined
} from '@ant-design/icons';
import { PlusCircleOutlined, DeleteOutlined, EditOutlined, SelectOutlined } from '@ant-design/icons';
import { useGetPackage, useGetPackages } from 'hooks/package/useGetPackage';
import { Packages } from 'common/interfaces/packages.interface';
import { openNotificationRight } from 'utils/notification';
import { useFindRouteStatistic } from 'hooks/package';
import moment from 'moment';
import { FilteredInput } from 'common/interfaces/filtered.interface';
interface ExaminationPackageTableProps {
  onCreate: () => void;
  onModify: (row: Packages) => void;
  onDelete: (packageId: string) => void;
  onActive: (packageId: string) => void;
  onInactive: (packageId: string) => void;
  onAuthorize: (row: any) => void;
  facility: string;
  fromDateSearch: string;
  toDateSearch: string;
  status: string;
  province: string;
  facilitiHos: string;
  channel: string;
  name: string;
  code: string;
}

const ExaminationPackageTable: FC<ExaminationPackageTableProps> = ({
  onCreate,
  onModify,
  onDelete,
  onActive,
  onInactive,
  onAuthorize,
  facility,
  fromDateSearch,
  toDateSearch,
  status,
  province,
  facilitiHos,
  channel,
  code,
  name
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(20);

  let filtered: FilteredInput[] = [];
  if (fromDateSearch) {
    filtered.push({
      id: 'startDate',
      value: fromDateSearch,
      operation: '>='
    });
  } else {
    filtered.push({
      id: 'startDate',
      value: moment(new Date()).format('DD/MM/YYYY'),
      operation: '>='
    });
  }
  if (toDateSearch) {
    filtered.push({
      id: 'endDate',
      value: toDateSearch,
      operation: '<='
    });
  } else {
    filtered.push({
      id: 'endDate',
      value: moment(new Date()).format('DD/MM/YYYY'),
      operation: '>='
    });
  }
  if (status) {
    filtered.push({
      id: 'status',
      value: status,
      operation: '=='
    });
  }
  if (name) {
    filtered.push({
      id: 'name',
      value: name,
      operation: '~'
    });
  }
  if (code) {
    filtered.push({
      id: 'code',
      value: code,
      operation: '~'
    });
  }
  if (channel) {
    filtered.push({
      id: 'display',
      value: 'BOTH' + ',' + channel,
      operation: 'in'
    });
  }

  const variable = { page, pageSize, filtered };
  const { package: itemSelect, isLoadingPackage, loadPackage } = useGetPackage();
  //Lấy danh Gói khám
  const { pagingPackages, isLoadingPackages, errorPackages, refetchPackages } = useGetPackages(variable);
  const [pagination, setPagination] = useState({});
  //Lấy thông tin thống kê trạng thái
  const {
    packageStatistic,
    isLoadingPackageStatistic,
    errorPackageStatistic,
    refetchPackageStatistic
  } = useFindRouteStatistic(variable);
  const handleActivePackage = async routeId => {
    await onActive(routeId);
    refetchPackages();
    refetchPackageStatistic();
  };
  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };

  useEffect(() => {
    if (itemSelect && Object.keys(itemSelect).length !== 0) {
      onModify(itemSelect);
    }
  }, [itemSelect]);

  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    if (pageSize !== pagination['pageSize']) {
      setPage(1);
    } else {
      setPage(pagination['current']);
    }
    setPageSize(pagination['pageSize']);
  };
  const handleInactivePackage = async routeId => {
    await onInactive(routeId);
    refetchPackages();
    refetchPackageStatistic();
  };
  const handleDeletePackage = async routeId => {
    await onDelete(routeId);
    refetchPackages();
    refetchPackageStatistic();
  };

  useEffect(() => {
    setPagination({ current: page, pageSize: pageSize, total: pagingPackages['records'] });
  }, [pagingPackages]);

  const contentActionButton = (row: Packages) => (
    <div>
      <Button
        icon={<EditOutlined />}
        style={{ marginLeft: 10 }}
        onClick={() => {
          if (row.status === 2) {
            openNotificationRight('Không thể cập nhật bản ghi có trạng thái là KHÔNG KÍCH HOẠT.', 'warning');
          } else {
            onModify(row);
          }
        }}
      >
        Sửa
      </Button>
      <Button
        type={row.status === 1 ? 'default' : 'primary'}
        icon={row.status === 1 ? <MinusCircleOutlined /> : <CheckCircleOutlined />}
        style={{ marginLeft: 10 }}
        onClick={() => {
          Modal.confirm({
            icon: <ExclamationCircleOutlined />,
            title: 'Bạn có chắc chắn muốn cập nhật trạng thái bản ghi có mã gói khám : ' + row.code + ' ?',
            onOk() {
              if (row.status === 2) {
                handleActivePackage(row.id);
              } else if (row.status === 1) {
                handleInactivePackage(row.id);
              }
            },
            onCancel() {
              //console.log('Cancel');
            }
          });
        }}
      >
        {row.status === 1 ? 'Hủy kích hoạt' : 'Kích hoạt'}
      </Button>
      <Button
        danger
        icon={<DeleteOutlined />}
        style={{ marginLeft: 10 }}
        type="primary"
        onClick={() => {
          Modal.confirm({
            icon: <ExclamationCircleOutlined />,
            title: 'Bạn có chắc chắn muốn xóa bản ghi có mã gói khám : ' + row.code + ' ?',
            onOk() {
              handleDeletePackage(row.id);
            },
            onCancel() {
              //console.log('Cancel');
            }
          });
        }}
      >
        Xóa
      </Button>
    </div>
  );
  return (
    <Table
      rowKey="id"
      dataSource={pagingPackages.data}
      scroll={{ x: 450, y: 'calc(95vh - 250px)' }}
      loading={isLoadingPackages}
      pagination={{
        ...pagination,
        defaultPageSize: 20,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '50']
      }}
      style={{ height: 'calc(100vh - 125px)', overflow: 'hidden' }}
      onChange={handleTableChange}
      title={() => (
        <>
          <div style={{ float: 'left' }}>
            <label className="lblTableTxt">
              <Tag color="blue">Tổng số bản ghi : {packageStatistic['totalRecords']}</Tag>
            </label>
            &nbsp;
            <label className="lblTableTxt">
              <Tag color="green">Kích hoạt : {packageStatistic['activeRecords']}</Tag>{' '}
            </label>
            &nbsp;
            <label className="lblTableTxt">
              <Tag color="red">Không kích hoạt : {packageStatistic['inactiveRecords']}</Tag>
            </label>
            &nbsp;
          </div>
          {/* <div style={{ float: 'right' }}>
            <Button icon={<PlusCircleOutlined />} type="primary" onClick={onCreate}>
              Thêm mới
            </Button>
          </div> */}
          <div style={{ clear: 'both' }}></div>
        </>
      )}
    >
      <Table.Column<Packages>
        title="STT"
        width={30}
        align="center"
        render={(value, item, index) => (page - 1) * pageSize + index + 1}
      />
      <Table.Column<Packages> title="Tên gói khám" width={100} render={(_, { name }) => name} />
      <Table.Column<Packages> title="Mã gói khám" align="center" width={100} render={(_, { code }) => code} />
      <Table.Column<Packages>
        title="Ảnh"
        align="center"
        width={100}
        render={(_, { image }) => (image ? <Image width={50} src={image} /> : '')}
      />
      <Table.Column<Packages>
        title="Ngày bắt đầu"
        align="center"
        width={100}
        render={(_, { startDate }) => moment(startDate).format('DD/MM/YYYY')}
      />
      <Table.Column<Packages>
        title="Ngày kết thúc"
        align="center"
        width={100}
        render={(_, { endDate }) => moment(endDate).format('DD/MM/YYYY')}
      />
      <Table.Column<Packages>
        title="Giới tính"
        align="center"
        width={80}
        render={(_, { sex }) => (sex == 'MALE' ? 'Nam' : sex == 'FEMALE' ? 'Nữ' : sex == 'BOTH' ? 'Cả hai' : '')}
      />
      <Table.Column<Packages>
        title="Hình thức"
        align="center"
        width={100}
        render={(_, { methodType }) =>
          methodType == 'HOSPITAL'
            ? 'Tại viện'
            : methodType == 'HOME'
            ? 'Tại nhà'
            : methodType == 'BOTH'
            ? 'Cả hai'
            : ''
        }
      />
      <Table.Column<Packages>
        title="Hiển thị"
        align="center"
        width={100}
        render={(_, { display }) =>
          display == 'WEB' ? 'WEB' : display == 'APP' ? 'APP' : display == 'BOTH' ? 'Cả hai' : ''
        }
      />
      <Table.Column<Packages>
        title="Trạng thái"
        align="center"
        width={100}
        render={(_, { status }) =>
          status === 1 ? <Tag color="#2db7f5">Kích hoạt</Tag> : <Tag color="#7e8286">Không kích hoạt</Tag>
        }
      />
      <Table.Column<Packages> title="Người chỉnh sửa" width={100} render={(_, { updateUser }) => updateUser} />
      <Table.Column<Packages>
        title="Thời gian chỉnh sửa"
        align="center"
        width={100}
        render={(_, { updateDate }) => updateDate}
      />
      <Table.Column<Packages>
        title="Hành động"
        width={120}
        align="center"
        render={(_, row) => (
          <Button
            type="primary"
            onClick={() =>
              // onModify(row)
              loadPackage({
                variables: {
                  id: row.id
                }
              })
            }
          >
            Xem chi tiết
          </Button>
        )}
      />
    </Table>
  );
};

export default ExaminationPackageTable;
