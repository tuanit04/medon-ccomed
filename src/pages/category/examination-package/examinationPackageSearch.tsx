import React, { FC } from 'react';
import { Role } from 'interface/permission/role.interface';
import useGetExaminationPackageForm from './useGetExaminationPackageForm';
import { Button, Col, ColProps } from 'antd';
import { useLocale } from 'locales';
import { SearchOutlined, ClearOutlined } from '@ant-design/icons';
import { useGetFacilitys } from 'hooks/facility';
import moment from 'moment';
export interface Values extends Role {}

interface Props {
  functionOb: any;
  facilities: [];
  provinces: [];
  // channels:[],
  onSearch: ({}) => void;
}

const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};
const ExaminationPackageSearch = ({ functionOb, facilities, provinces, onSearch }: Props) => {
  const {
    Form,
    form,
    FromDate,
    ToDate,
    Facilities,
    Status,
    Show,
    FacilityHospital,
    Provinces,
    Buttons,
    PackageName,
    Code
  } = useGetExaminationPackageForm({
    name: 'searchForm',
    responsive: true,
    isSearhForm: true
  });

  return (
    <Form>
      <FromDate />
      <ToDate />
      <Show handleChangeShow={channel => {}} />
      {/* <Facilities
        facilityList={facilities}
        handleChangeFacility={facilityIds => {
          console.log('facilityIds : ', facilityIds);
        }}
      /> */}
      {/* <FacilityHospital
       facilityHospitalList={facilities}
       handleChangeFacilityHospital={facilitieHos =>{
         console.log("facilitieHos : ",facilitieHos);
       }}
      /> */}
      <Status />
      <PackageName handleChange={value => {}} />
      <Code />
      {/* <Provinces
        provinceList={provinces}
        handleChangeProvince={(provice)=>{
          console.log(provice);
        }}
      /> */}

      <Buttons
        functionOb={functionOb}
        onSearch={() => {
          let fromDate = form.getFieldValue('fromDate')
            ? moment(form.getFieldValue('fromDate')).format('DD/MM/YYYY')
            : '';
          let toDate = form.getFieldValue('toDate') ? moment(form.getFieldValue('toDate')).format('DD/MM/YYYY') : '';
          let channel = form.getFieldValue('show');
          let status = form.getFieldValue('status');
          let facilityHospital = form.getFieldValue('facilityHospital');
          let facility = form.getFieldValue('facility');
          let province = form.getFieldValue('provinceId');
          let name = form.getFieldValue('name');
          let code = form.getFieldValue('code');
          onSearch({ fromDate, toDate, channel, status, facilityHospital, facility, province, name, code });
        }}
        resetFields={() => {
          form.resetFields();
          onSearch({
            fromDate: moment().format('DD/MM/YYYY'),
            toDate: moment().format('DD/MM/YYYY'),
            channel: '',
            status: '',
            facilityHospital: '',
            facility: '',
            province: '',
            code: '',
            name: ''
          });
        }}
      />
      {/* <div >
        <Button style={{ float: 'right', marginLeft: '5px' }} icon={<ClearOutlined />}>
          Bỏ lọc
        </Button>
        <Button style={{ float: 'right' }} icon={<SearchOutlined />} type="primary">
          Tìm kiếm
        </Button>
        <div style={{ clear: 'both' }}></div>
      </div> */}
    </Form>
  );
};

export default ExaminationPackageSearch;
