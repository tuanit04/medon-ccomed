import React, { FC, useEffect, useState } from 'react';
import { Button, InputNumber, Table, Tag, Upload, FormProps, Row, Col } from 'antd';
import { ServiceView } from 'common/interfaces/service.interface';
import { useGetServices } from 'hooks/services/useGetServices';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { ExcelRenderer } from 'react-excel-renderer';
import { FormInstance } from 'antd/es/form/Form';
import {
  CaretRightOutlined,
  ClearOutlined,
  DownloadOutlined,
  FileExcelOutlined,
  FileMarkdownOutlined
} from '@ant-design/icons';
import { Label } from 'recharts';
import { openNotificationRight } from 'utils/notification';
import { messageError } from 'utils/message';
import { arrayLengthCompare } from '@blueprintjs/core/lib/esm/common/utils';
import { Facility } from 'common/interfaces/facility.interface';
import { join } from 'path';

interface ExaminationPackageAddFormTabTwoProps {
  values: any;
  form: FormInstance<FormProps<any>>;
  handleChange: (value, e) => void;
}
const ExaminationPackageAddFormTabTwo: FC<ExaminationPackageAddFormTabTwoProps> = ({ values, form, handleChange }) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [pagination, setPagination] = useState({});
  let filtered: FilteredInput[] = [];
  const variables = { page, pageSize, filtered };
  //Hook Lấy danh sách dịch vụ
  const { pagingServices, isLoadingServices, errorServices, refetchServices } = useGetServices(variables);
  useEffect(() => {
    setPagination({ current: page, pageSize: pageSize, total: pagingServices['records'] });
  }, [pagingServices]);
  useEffect(() => {
    //loadProvinces();
  }, []);
  const handleDeleteService = async routeId => {
    /*await onDelete(routeId);
    refetchRoutes();
    refetchRouteStatistic();*/
  };

  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    setPage(pagination['current']);
    setPageSize(pagination['pageSize']);
  };
  const handleAdd = () => {
    /*const { count, dataSource } = this.state;
    const newData = {
      key: count,
      name: `Edward King ${count}`,
      age: '32',
      address: `London, Park Lane no. ${count}`
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1
    });*/
  };
  const handleSave = row => {
    /*const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    this.setState({
      dataSource: newData
    });*/
  };

  const fileHandler = fileList => {
    let fileObj = fileList;
    if (!fileObj) {
      //Xu ly thong bao
      //openNotificationRight('Không có file nào được tải lên.');
      messageError('Không có file nào được tải lên.');
      return false;
    }
    if (
      !(
        fileObj.type === 'application/vnd.ms-excel' ||
        fileObj.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      )
    ) {
      openNotificationRight('Định dạng file không hợp lệ, vui lòng kiểm tra lại.');
      messageError('Định dạng file không hợp lệ, vui lòng kiểm tra lại.');
      return false;
    }
    //just pass the fileObj as parameter
    ExcelRenderer(fileObj, (err, resp) => {
      if (err) {
        console.log(err);
      } else {
        let newRows = [];
        resp.rows.slice(1)?.map((row, index) => {
          if (row && row !== 'undefined') {
            /*newRows.push({
              key: index,
              name: row[0],
              age: row[1],
              gender: row[2]
            });*/
          }
        });
        if (newRows.length === 0) {
          //Xu ly thong bao
          return false;
        } else {
          /*this.setState({
            cols: resp.cols,
            rows: newRows,
            errorMessage: null
          });*/
        }
      }
    });
    return false;
  };

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [serviceSelected, setServiceSelected] = useState([
    {} as {
      key: any;
      serviceCode: any;
      serviceName: any;
      originPrice: any;
      hosSalePrice: any;
    }
  ]);
  const [totalHosSalePriceState, setTotalHosSalePriceState] = useState(Number);
  const [loading, setLoading] = useState(false);
  const start = () => {
    setLoading(true);
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
      setServiceSelected([]);
      setLoading(false);
    }, 500);
  };
  let servicesDataSelectedArr = [
    {} as {
      key: any;
      serviceCode: any;
      serviceName: any;
      originPrice: any;
      hosSalePrice: any;
    }
  ];
  servicesDataSelectedArr.shift();
  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
    for (let i = 0; i < pagingServices?.data.length; i++) {
      for (let j = 0; j < selectedRowKeys.length; j++) {
        if (pagingServices?.data[i]['serviceId'] === selectedRowKeys[j]) {
          servicesDataSelectedArr.push(pagingServices?.data[i]);
        }
      }
    }
    setServiceSelected(servicesDataSelectedArr);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  };
  const hasSelected = selectedRowKeys.length > 0;
  return (
    <>
      <Table
        rowKey={row => row.serviceId}
        rowSelection={rowSelection}
        pagination={pagination}
        onChange={handleTableChange}
        dataSource={pagingServices?.data}
        scroll={{ x: 500 }}
        title={() => (
          <>
            <div style={{ float: 'left' }}>
              <label className="lblTableTxt">
                <Tag color="blue">Tổng số bản ghi : 0</Tag>
              </label>
            </div>
            <div style={{ float: 'right' }}></div>
            <div style={{ clear: 'both' }}></div>
          </>
        )}
      >
        <Table.Column<ServiceView> title="STT" width={30} align="center" render={(value, item, index) => 1} />
        <Table.Column<ServiceView>
          title="Mã dịch vụ"
          width={100}
          align="center"
          render={(_, { serviceCode }) => serviceCode}
        />
        <Table.Column<ServiceView> title="Tên dịch vụ" width={200} render={(_, { serviceName }) => serviceName} />
      </Table>

      <Table
        rowKey="id"
        dataSource={serviceSelected}
        scroll={{ x: 500 }}
        title={() => (
          <>
            <div style={{ marginBottom: 16, marginTop: 15 }}>
              <Button type="primary" onClick={start} disabled={!hasSelected} loading={loading}>
                Chọn lại
              </Button>
              <span style={{ marginLeft: 8, color: 'red', fontStyle: 'italic', fontSize: 16 }}>
                {hasSelected ? `Danh sách Chỉ định xét nghiệm đã chọn (${selectedRowKeys.length})` : ''}
              </span>
            </div>
          </>
        )}
      >
        <Table.Column<ServiceView> title="STT" width={30} align="center" render={(value, item, index) => 1} />
        <Table.Column<ServiceView>
          title="Mã dịch vụ"
          width={100}
          align="center"
          render={(_, { serviceCode }) => serviceCode}
        />
        <Table.Column<ServiceView> title="Tên dịch vụ" width={200} render={(_, { serviceName }) => serviceName} />
      </Table>
      <div style={{ marginTop: 10 }}>
        <div>
          <span>Giảm giá (%)</span>&nbsp;&nbsp;
          <InputNumber
            defaultValue={0}
            min={0}
            max={100}
            formatter={value => `${value}%`}
            parser={(value: any) => value.replace('%', '')}
            onChange={() => {}}
          />
        </div>
        <div style={{ float: 'left' }}>
          <Button
            onClick={handleAdd}
            icon={<FileMarkdownOutlined />}
            type="primary"
            style={{
              marginBottom: 16,
              marginTop: 15
            }}
          >
            Tải file mẫu
          </Button>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <Upload name="file" beforeUpload={fileHandler} onRemove={() => {}} multiple={false}>
            <Button
              icon={<FileExcelOutlined />}
              type="default"
              style={{
                marginBottom: 16
              }}
            >
              Import Excel
            </Button>
          </Upload>
          <Button
            onClick={handleAdd}
            icon={<DownloadOutlined />}
            type="primary"
            style={{
              marginBottom: 16
            }}
          >
            Export Excel
          </Button>
        </div>
        <div style={{ clear: 'both' }}></div>
      </div>
    </>
  );
};

export default ExaminationPackageAddFormTabTwo;
