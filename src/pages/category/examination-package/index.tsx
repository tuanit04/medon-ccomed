import React, { FC, useEffect, useState } from 'react';
import ExaminationPackageCreateDialog from './examinationPackageCreateDialog';
import ExaminationPackageTable from './examinationPackageTable';
import ExaminationPackageSearch from './examinationPackageSearch';
import ExaminationPackageModifyDialog from './examinationPackageModifyDialog';
import './index.less';
import { Packages } from 'common/interfaces/packages.interface';
import { useInactivePackage } from 'hooks/package/useInactivePackage';
import { useActivePackage } from 'hooks/package/useActivePackage';
import { useDeletePackage } from 'hooks/package/useDeletePackage';
import { useCreatePackage } from 'hooks/package/useCreatePackage';
import { useGetFacilitys } from 'hooks/facility/useGetFacility';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetChannels } from 'hooks/channel/useGetChannel';
import { FormInstance, FormProps } from 'antd';
import moment from 'moment';
import { useAppState } from 'helpers';
const ExaminationPackagePage: FC = () => {
  const functionOb = useAppState(state => state.user.functionObject);
  const [mcreateVisible, setCreateVisible] = useState(false);
  const [modifyVisible, setModifyVisible] = useState(false);
  const [authorizeVisible, setAuthorizeVisible] = useState(false);
  const [values, setValues] = useState({} as Packages);
  const [listFactility, setListFactility] = useState([]);
  //Xử lý thêm mới Package
  const { createPackage, resultCreatePackage, isLoadingCreatePackage, errorCreatePackage } = useCreatePackage();
  //Xử lý Delete Package
  const { deletePackage, isLoadingDelePackage, resultDelePackage, errorDelePackage } = useDeletePackage();
  const handleDeletePackage = async id => {
    await deletePackage({ id });
  };
  //Xử lý Active Package
  const { activePackage, resultActivePackage, isLoadingActivePackage, errorActivePackage } = useActivePackage();
  const handleActivePackage = async id => {
    await activePackage({ id });
  };
  const {
    inactivePackage,
    resultInactivePackage,
    isLoadingInactivePackage,
    errorInactivePackage
  } = useInactivePackage();
  const handleInactivePackage = async id => {
    await inactivePackage({ id });
  };
  //Hook Lấy danh sách don vi de xuat
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  // const  {channels,isLoadingChannels,loadChannels} = useGetChannels();

  useEffect(() => {
    loadFacilities({});
    loadProvinces({});
    // loadChannels({});
  }, []);
  const [fromDateSearch, setFromDateSearch] = useState(moment(new Date()).format('DD/MM/YYYY'));
  const [toDateSearch, setToDateSearch] = useState(moment(new Date()).format('DD/MM/YYYY'));
  const [status, setStatus] = useState('');
  const [province, setProvince] = useState('');
  const [facility, setFacility] = useState('');
  const [facilitiHos, setFacilitiHos] = useState('');
  const [channel, setChannel] = useState('');
  const [code, setCode] = useState('');
  const [name, setName] = useState('');
  const [form, setForm] = useState({} as FormInstance<FormProps<any>>);
  return (
    <div className="main-content">
      <ExaminationPackageSearch
        functionOb={functionOb}
        facilities={facilities}
        provinces={provinces}
        onSearch={value => {
          setFromDateSearch(value['fromDate']);
          setToDateSearch(value['toDate']);
          setChannel(value['channel']);
          setStatus(value['status']);
          setName(value['name']);
          setCode(value['code']);
        }}
      />
      <ExaminationPackageCreateDialog
        facilitiesList={facilities}
        provinces={provinces}
        visible={mcreateVisible}
        onCancel={() => setCreateVisible(false)}
        values={values}
      />
      {/* <ExaminationPackageModifyDialog
        facilities={facilities}
        provinces={provinces}
        facilitiesHos={facilities}
        values={values}
        visible={modifyVisible}
        onCancel={() => setModifyVisible(false)}
        onModify={() => setModifyVisible(false)}
      /> */}
      <ExaminationPackageTable
        onCreate={() => setCreateVisible(true)}
        facility={facility}
        fromDateSearch={fromDateSearch}
        toDateSearch={toDateSearch}
        status={status}
        province={province}
        facilitiHos={facilitiHos}
        channel={channel}
        code={code}
        name={name}
        onModify={values => {
          setValues(values);
          setCreateVisible(true);
        }}
        onDelete={async id => {
          await handleDeletePackage(id);
        }}
        onActive={async id => {
          await handleActivePackage(id);
        }}
        onInactive={async id => {
          await handleInactivePackage(id);
        }}
        onAuthorize={values => {
          setValues(values);
          setAuthorizeVisible(true);
        }}
      />
    </div>
  );
};

export default ExaminationPackagePage;
