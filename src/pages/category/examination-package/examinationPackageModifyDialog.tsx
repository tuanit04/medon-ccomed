import { DownloadOutlined, FileExcelOutlined, FileMarkdownOutlined } from '@ant-design/icons';
import { Button, Modal, Steps, message, Upload, InputNumber, Table, Tag } from 'antd';
import { FilteredInput } from 'common/interfaces/filtered.interface';
import { ServiceView } from 'common/interfaces/service.interface';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { useGetServices } from 'hooks/services/useGetServices';
import { useGetWards } from 'hooks/ward/useGetWard';
import React, { FC, useState } from 'react';
import { messageError } from 'utils/message';
import { openNotificationRight } from 'utils/notification';
import { ExcelRenderer } from 'react-excel-renderer';
import useGetExaminationPackageForm from './useGetExaminationPackageForm';
import { Packages } from 'common/interfaces/packages.interface';
const { Step } = Steps;
interface FacilityHos {
  facilityId: any;
  facilityName: any;
}
interface ExaminationPackageCreateDialogProps {
  values: Packages;
  visible: boolean;
  onModify: (values: any) => void;
  onCancel: () => void;
  facilities: any[];
  provinces: any[];
  facilitiesHos: any[];
}
const ExaminationPackageCreateDialog2: FC<ExaminationPackageCreateDialogProps> = ({
  values,
  visible,
  onModify,
  facilities,
  provinces,
  onCancel,
  facilitiesHos
}) => {
  //Paging
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(1);
  const [pagination, setPagination] = useState({});
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [serviceSelected, setServiceSelected] = useState([
    {} as {
      key: any;
      serviceCode: any;
      serviceName: any;
      originPrice: any;
      hosSalePrice: any;
    }
  ]);

  const [facilityHosList, setFacilityHosList] = useState([{ facilityId: '1', facilityName: 'OK' } as FacilityHos]);
  const [discountPercent, setDiscountPercent] = useState(0);
  const [loading, setLoading] = useState(false);
  const [facilityHospitalIds, setFacilityHospitalIds] = useState([]);
  //Hook Lấy danh sách Tỉnh/TP
  // const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //Hook Lấy danh sách Quận/Huyện
  const { districts, isLoadingDistricts, loadDistricts } = useGetDistricts();
  //Hook Lấy danh sách Xã/Phường
  const { wards, isLoadingWards, loadWards } = useGetWards();
  //Hook Lấy danh sách don vi de xuat
  // const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  //Hook Lấy danh sách don vi thuc hiên tai vien
  // const { facilitiesHos, isLoadingFacilitiesHos, loadFacilitiesHos } = useGetFacilitysHos();
  let filtered: FilteredInput[] = [];
  const variables = { page, pageSize, filtered };
  //Hook Lấy danh sách dịch vụ
  const { pagingServices, isLoadingServices, errorServices, refetchServices } = useGetServices(variables);
  const {
    form,
    Form,
    PackageName,
    Show,
    Code,
    CategoryShow,
    Introduce,
    StartDate,
    EndDate,
    AgeRange,
    Provinces,
    Facilities,
    PreOrder,
    Effect,
    Workday,
    Gender,
    UploadExaminationPackageImg,
    FacilityHospital,
    PreOrderPerPerson,
    NumberofPackages
  } = useGetExaminationPackageForm({
    name: 'searchForm',
    responsive: true,
    values: values,
    isSearhForm: false
  });
  const [step, setStep] = React.useState(0);
  // Proceed to next step
  const nextStep = () => {
    setStep(step + 1);
    //setFacilityHosList([]);
    const facilityHospitalIdSelected = form.getFieldValue('facilityHospital');
    let facilityHos: FacilityHos;
    if (facilityHospitalIdSelected !== undefined) {
      for (let i = 0; i < facilityHospitalIdSelected.length; i++) {
        let values = facilityHospitalIdSelected[i].split('#');
        let facilityId = values[0];
        let facilityName = values[1];
        facilityHos = { facilityName, facilityId };
        facilityHosList.push(facilityHos);
      }
    }
  };
  // Go back to prev step
  const prevStep = () => {
    setStep(step - 1);
    setFacilityHosList([]);
  };
  // Handle fields change
  const handleChange = (input, e) => {};
  /*useEffect(() => {
    setPagination({ current: page, pageSize: pageSize, total: pagingServices['records'] });
  }, [pagingServices]);*/
  // useEffect(() => {
  //   loadProvinces();
  //   loadFacilities();
  //   loadFacilitiesHos({
  //     variables: {
  //       page: 1,
  //       filtered: [
  //         {
  //           id: 'provinceId',
  //           value: 'f0e0cba7-aaef-4f0e-a602-ce4eb10b5f38,e303649a-aa6e-4343-b972-851b4da1822e',
  //           operation: 'IN'
  //         }
  //       ]
  //     }
  //   });
  // }, []);

  const handleTableChange = (pagination, filters, sorter) => {
    reFetchData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters
    });
  };
  const reFetchData = (params = {}) => {
    let pagination = params['pagination'];
    setPage(pagination['current']);
    setPageSize(pagination['pageSize']);
  };
  const handleAdd = () => {
    /*const { count, dataSource } = this.state;
    const newData = {
      key: count,
      name: `Edward King ${count}`,
      age: '32',
      address: `London, Park Lane no. ${count}`
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1
    });*/
  };

  const fileHandler = fileList => {
    let fileObj = fileList;
    if (!fileObj) {
      //Xu ly thong bao
      //openNotificationRight('Không có file nào được tải lên.');
      messageError('Không có file nào được tải lên.');
      return false;
    }
    if (
      !(
        fileObj.type === 'application/vnd.ms-excel' ||
        fileObj.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      )
    ) {
      openNotificationRight('Định dạng file không hợp lệ, vui lòng kiểm tra lại.');
      messageError('Định dạng file không hợp lệ, vui lòng kiểm tra lại.');
      return false;
    }
    //just pass the fileObj as parameter
    ExcelRenderer(fileObj, (err, resp) => {
      if (err) {
        console.log(err);
      } else {
        let newRows = [{}];
        newRows.shift();
        resp.rows.slice(1)?.map((row, index) => {
          if (row && row !== 'undefined') {
            newRows.push({
              serviceCode: row[1]
            });
          }
        });
        if (newRows.length === 0) {
          //Xu ly thong bao
          return false;
        } else {
          let str: string = newRows.join(',');
          refetchServices({
            variables: {
              filtered: [
                {
                  id: 'serviceCode',
                  value: str,
                  operation: 'IN'
                }
              ]
            }
          });
          setServiceSelected(pagingServices.data);
        }
      }
    });
    return false;
  };
  const start = () => {
    setLoading(true);
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
      setServiceSelected([]);
      setLoading(false);
    }, 500);
  };
  let servicesDataSelectedArr = [
    {} as {
      key: any;
      serviceCode: any;
      serviceName: any;
      originPrice: any;
      hosSalePrice: any;
    }
  ];
  servicesDataSelectedArr.shift();
  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
    for (let i = 0; i < pagingServices?.data.length; i++) {
      for (let j = 0; j < selectedRowKeys.length; j++) {
        if (pagingServices?.data[i]['serviceId'] === selectedRowKeys[j]) {
          servicesDataSelectedArr.push(pagingServices?.data[i]);
        }
      }
    }
    setServiceSelected(servicesDataSelectedArr);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  };
  const hasSelected = selectedRowKeys.length > 0;
  return (
    <Modal
      centered
      style={{ marginTop: 15 }}
      title="Chỉnh sửa gói khám"
      visible={visible}
      width={1300}
      onOk={async () => onModify((await form.validateFields()) as any)}
      onCancel={onCancel}
    >
      <>
        <Steps current={step}>
          <Step key="1" title="Thông tin gói khám" />
          <Step key="2" title="Danh sách dịch vụ" />
          <Step key="3" title="Danh sách gói khám" />
        </Steps>
        {step === 0 && (
          <div className="steps-content">
            <Form>
              <PackageName handleChange={handleChange} />
              {/* <Show /> */}
              <Code />
              <CategoryShow />
              <Introduce />
              <Workday />
              <PreOrder />
              <PreOrderPerPerson />
              <NumberofPackages />
              <StartDate />
              <UploadExaminationPackageImg />
              <EndDate />
              <AgeRange />
              <Gender />
              <Effect />
              <Provinces handleChangeProvince={provinceIds => {}} provinceList={provinces} />
              <Facilities facilityList={facilities} handleChangeFacility={facilityIds => {}} />
              <FacilityHospital
                facilityHospitalList={facilitiesHos}
                handleChangeFacilityHospital={facilityHospitalIds => {
                  //onSetFacilityHospitalIds(facilityHospitalIds);
                }}
              />
            </Form>
          </div>
        )}
        {step === 1 && (
          <div className="steps-content">
            <Table
              rowKey={row => row.serviceId}
              rowSelection={rowSelection}
              pagination={pagination}
              onChange={handleTableChange}
              dataSource={pagingServices?.data}
              scroll={{ x: 500 }}
              title={() => (
                <>
                  <div style={{ float: 'left' }}>
                    <label className="lblTableTxt">
                      <Tag color="blue">Tổng số bản ghi : 0</Tag>
                    </label>
                  </div>
                  <div style={{ float: 'right' }}></div>
                  <div style={{ clear: 'both' }}></div>
                </>
              )}
            >
              <Table.Column<ServiceView> title="STT" width={30} align="center" render={(value, item, index) => 1} />
              <Table.Column<ServiceView>
                title="Mã dịch vụ"
                width={100}
                align="center"
                render={(_, { serviceCode }) => serviceCode}
              />
              <Table.Column<ServiceView> title="Tên dịch vụ" width={200} render={(_, { serviceName }) => serviceName} />
              {facilityHosList?.map(option => (
                <Table.Column<ServiceView>
                  title={option.facilityName}
                  width={200}
                  render={(_, { originPrice }) => originPrice}
                />
              ))}
            </Table>
            <Table
              rowClassName="editable-row"
              dataSource={serviceSelected}
              scroll={{ x: 500 }}
              title={() => (
                <>
                  <div style={{ marginBottom: 16, marginTop: 15 }}>
                    <Button type="primary" onClick={start} disabled={!hasSelected} loading={loading}>
                      Chọn lại
                    </Button>
                    <span style={{ marginLeft: 8, color: 'red', fontStyle: 'italic', fontSize: 16 }}>
                      {hasSelected ? `Danh sách Chỉ định xét nghiệm đã chọn (${selectedRowKeys.length})` : ''}
                    </span>
                  </div>
                </>
              )}
            >
              <Table.Column<ServiceView> title="STT" width={30} align="center" render={(value, item, index) => 1} />
              <Table.Column<ServiceView>
                title="Mã dịch vụ"
                width={100}
                align="center"
                render={(_, { serviceCode }) => serviceCode}
              />
              <Table.Column<ServiceView> title="Tên dịch vụ" width={200} render={(_, { serviceName }) => serviceName} />
              {facilityHosList?.map(option => (
                <Table.Column<ServiceView>
                  title={option.facilityName}
                  width={200}
                  render={(_, { originPrice }) =>
                    originPrice === undefined ? 0 : Number(originPrice) - (Number(originPrice) * discountPercent) / 100
                  }
                />
              ))}
            </Table>
            <div style={{ marginTop: 10 }}>
              <div>
                <span>Giảm giá (%)</span>&nbsp;&nbsp;
                <InputNumber
                  defaultValue={0}
                  min={0}
                  max={100}
                  formatter={value => `${value}%`}
                  parser={(value: any) => value.replace('%', '')}
                  onChange={() => {}}
                  onPressEnter={e => {
                    let percent = e.target['value'];
                    let percentNumber = Number(percent.substring(0, percent.lastIndexOf('%')));
                    setDiscountPercent(percentNumber);
                  }}
                />
              </div>
              <div style={{ float: 'left' }}>
                <Button
                  onClick={handleAdd}
                  icon={<FileMarkdownOutlined />}
                  type="primary"
                  style={{
                    marginBottom: 16,
                    marginTop: 15
                  }}
                >
                  <a href="test.xlsx" download>
                    Tải file mẫu
                  </a>
                </Button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Upload name="file" beforeUpload={fileHandler} onRemove={() => {}} multiple={false}>
                  <Button
                    icon={<FileExcelOutlined />}
                    type="default"
                    style={{
                      marginBottom: 16
                    }}
                  >
                    Import Excel
                  </Button>
                </Upload>
                <Button
                  onClick={handleAdd}
                  icon={<DownloadOutlined />}
                  type="primary"
                  style={{
                    marginBottom: 16
                  }}
                >
                  Export Excel
                </Button>
              </div>
              <div style={{ clear: 'both' }}></div>
            </div>
          </div>
        )}
        {step === 2 && (
          <Table
            rowClassName="editable-row"
            rowKey="id"
            dataSource={serviceSelected}
            scroll={{ x: 500 }}
            title={() => (
              <>
                <div style={{ marginBottom: 16, marginTop: 15 }}>
                  <Button type="primary" onClick={start} disabled={!hasSelected} loading={loading}>
                    Chọn lại
                  </Button>
                  <span style={{ marginLeft: 8, color: 'red', fontStyle: 'italic', fontSize: 16 }}>
                    {hasSelected ? `Danh sách Chỉ định xét nghiệm đã chọn (${selectedRowKeys.length})` : ''}
                  </span>
                </div>
              </>
            )}
          >
            <Table.Column<ServiceView> title="STT" width={30} align="center" render={(value, item, index) => 1} />
            <Table.Column<ServiceView>
              title="Mã dịch vụ"
              width={100}
              align="center"
              render={(_, { serviceCode }) => serviceCode}
            />
            <Table.Column<ServiceView> title="Tên dịch vụ" width={200} render={(_, { serviceName }) => serviceName} />
            {facilityHosList?.map(option => (
              <Table.Column<ServiceView>
                title={option.facilityName}
                width={200}
                render={(_, { originPrice }) =>
                  originPrice === undefined ? 0 : Number(originPrice) - (Number(originPrice) * discountPercent) / 100
                }
              />
            ))}
          </Table>
        )}
        <div className="steps-action">
          {step > 0 && (
            <Button style={{ margin: '0 4px' }} onClick={prevStep}>
              Quay lại
            </Button>
          )}
          {step < 3 && (
            <Button style={{ margin: '0 4px' }} type="primary" onClick={nextStep}>
              Tiếp
            </Button>
          )}
          {step === 3 && (
            <Button style={{ margin: '0 4px' }} type="primary" onClick={() => message.success('Processing complete!')}>
              Xong
            </Button>
          )}
          {step === 2 && (
            <Button type="primary" style={{ margin: '0 4px' }} onClick={() => message.success('Processing complete!')}>
              Export dịch vụ
            </Button>
          )}
        </div>
      </>
    </Modal>
  );
};

export default ExaminationPackageCreateDialog2;
