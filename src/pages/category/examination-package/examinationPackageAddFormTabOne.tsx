import React, { FC, useEffect } from 'react';
import { Role } from 'interface/permission/role.interface';
import useGetExaminationPackageForm from './useGetExaminationPackageForm';
import { FormProps } from 'antd';
import { useGetProvincesLazy } from 'hooks/province/useGetProvince';
import { useGetDistricts } from 'hooks/district/useGetDistrict';
import { useGetWards } from 'hooks/ward/useGetWard';
import { useGetFacilitys, useGetFacilitysHos } from 'hooks/facility';
import { useState } from 'react';
import { FormInstance } from 'antd/es/form/Form';

export interface Values extends Role {}
interface ExaminationPackageAddFormTabOneProps {
  values: any;
  form: FormInstance<FormProps<any>>;
  handleChange: (value, e) => void;
}
const ExaminationPackageAddFormTabOne: FC<ExaminationPackageAddFormTabOneProps> = ({ form, values, handleChange }) => {
  const {
    PackageName,
    // Show,
    Code,
    CategoryShow,
    Introduce,
    StartDate,
    EndDate,
    AgeRange,
    Provinces,
    Facilities,
    PreOrder,
    Workday,
    Gender,
    UploadExaminationPackageImg,
    FacilityHospital,
    PreOrderPerPerson,
    NumberofPackages,
    Buttons
  } = useGetExaminationPackageForm({
    name: 'searchForm',
    responsive: true
  });
  React.useEffect(() => {
    console.log('useEffect : ', form);
    // onSetFormInstance(form);
  }, []);
  //FormParent = Function(FormParent);
  //Hook Lấy danh sách Tỉnh/TP
  const { provinces, isLoadingProvince, loadProvinces } = useGetProvincesLazy();
  //Hook Lấy danh sách Quận/Huyện
  const { districts, isLoadingDistricts, loadDistricts } = useGetDistricts();
  //Hook Lấy danh sách Xã/Phường
  const { wards, isLoadingWards, loadWards } = useGetWards();
  //Hook Lấy danh sách don vi de xuat
  const { facilities, isLoadingFacilities, loadFacilities } = useGetFacilitys();
  //Hook Lấy danh sách don vi thuc hiên tai vien
  const { facilitiesHos, isLoadingFacilitiesHos, loadFacilitiesHos } = useGetFacilitysHos();
  const [facilityHospitalIds, setFacilityHospitalIds] = useState([]);
  useEffect(() => {
    loadProvinces();
    loadFacilities();
    loadFacilitiesHos({
      variables: {
        page: 1,
        filtered: [
          {
            id: 'provinceId',
            value: 'f0e0cba7-aaef-4f0e-a602-ce4eb10b5f38,e303649a-aa6e-4343-b972-851b4da1822e',
            operation: 'IN'
          }
        ]
      }
    });
  }, []);
  return (
    <>
      <PackageName handleChange={handleChange} />
      {/* <Show /> */}
      <Code />
      <CategoryShow />
      <Introduce />
      <Workday />
      <PreOrder />
      <PreOrderPerPerson />
      <NumberofPackages />
      <StartDate />
      <UploadExaminationPackageImg />
      <EndDate />
      <AgeRange />
      <EndDate />
      <Provinces
        handleChangeProvince={provinceIds => {
          /*if (provinceIds !== '') {
            loadDistricts({
              variables: {
                provinceId: provinceId
              }
            });
          }*/
        }}
        provinceList={provinces}
      />
      <Gender />
      <Facilities facilityList={facilities} handleChangeFacility={facilityIds => {}} />
      <FacilityHospital
        facilityHospitalList={facilitiesHos}
        handleChangeFacilityHospital={facilityHospitalIds => {
          //onSetFacilityHospitalIds(facilityHospitalIds);
        }}
      />
    </>
  );
};

export default ExaminationPackageAddFormTabOne;
