import React, { FC } from 'react';
import { Role } from 'interface/permission/role.interface';
import useGetExaminationPackageForm from './useGetExaminationPackageForm';
import { useLocale } from 'locales';
import { SearchOutlined, ClearOutlined, CaretRightOutlined } from '@ant-design/icons';
import { CODE } from '@blueprintjs/core/lib/esm/common/classes';
import { Button, Form, Table, Tag } from 'antd';
import { ServiceView } from 'common/interfaces/service.interface';

export interface Values extends Role {}
interface ExaminationPackageAddFormTabThreeProps {
  values: any;
  handleChange: (value: any, e: any) => void;
}
const ExaminationPackageAddFormTabThree: FC<ExaminationPackageAddFormTabThreeProps> = ({ values, handleChange }) => {
  let arr = [
    { name: '1', key: 1 },
    { name: '2', key: 2 },
    { name: '3', key: 3 }
  ];
  return (
    <Form>
      <Table
        rowKey="id"
        dataSource={[]}
        scroll={{ x: 500 }}
        title={() => (
          <>
            <div style={{ float: 'left' }}>
              <label className="lblTableTxt">
                <Tag color="blue">Tổng số bản ghi : 0</Tag>
              </label>
            </div>
            <div style={{ float: 'right' }}></div>
            <div style={{ clear: 'both' }}></div>
          </>
        )}
      >
        <Table.Column<ServiceView> title="STT" width={30} align="center" render={(value, item, index) => 1} />
        <Table.Column<ServiceView>
          title="Mã dịch vụ"
          width={100}
          align="center"
          render={(_, { serviceCode }) => serviceCode}
        />
        <Table.Column<ServiceView> title="Tên dịch vụ" width={200} render={(_, { serviceName }) => serviceName} />
        {arr?.map(option => (
          <Table.Column<any> title="1" width={200} render={(_, { option }) => option.name} />
        ))}
      </Table>
    </Form>
  );
};

export default ExaminationPackageAddFormTabThree;
