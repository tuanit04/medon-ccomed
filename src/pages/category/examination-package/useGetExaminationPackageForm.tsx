import React, { FC, useState } from 'react';
import { Form, Input, Col, Row, Select, Modal, Space, InputNumber, Checkbox, Radio, Button, Slider } from 'antd';
import { useLocale } from 'locales';
import { ColProps } from 'antd/lib/col';
import { FormProps } from 'antd/lib/form';
import { useAppState } from '../../../helpers';
import { Role } from 'interface/permission/role.interface';
import { DatePicker } from 'antd';
import { Upload } from 'antd';
import { CaretRightOutlined, ClearOutlined, SearchOutlined } from '@ant-design/icons';
import { Province } from 'common/interfaces/province.interface';
import { Facility } from 'common/interfaces/facility.interface';
import { Packages } from 'common/interfaces/packages.interface';
import moment from 'moment';
import { functionCodeConstants } from 'constants/functions';
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const { Option } = Select;

const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 6,
  xl: 6,
  xxl: 6
};

const wrapperColButton: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 6,
  xl: 6,
  xxl: 6
};

const layout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 17 }
};

interface RoleFormProps {
  /** form name */
  name: string;
  /** Form item required? */
  required?: boolean;
  /** search form need responsive layout */
  responsive?: boolean;
  /** Initial form data */
  values?: Packages;
  isSearhForm?: boolean;
}

export default function useGetExaminationPackageForm({
  required = false,
  responsive = false,
  name = 'form',
  values,
  isSearhForm
}: RoleFormProps) {
  const { formatMessage } = useLocale();
  const [formInstance] = Form.useForm<FormProps>();

  const _Form: FC<FormProps> = ({ children, ...props }) => {
    const { device } = useAppState((state: any) => state.user);

    return (
      <Form
        {...props}
        {...(device === 'MOBILE' ? { layout: 'vertical' } : layout)}
        form={formInstance}
        name={name}
        initialValues={values === undefined ? {} : values}
      >
        {responsive ? <Row>{children}</Row> : children}
      </Form>
    );
  };

  type InternalForm = typeof _Form;
  interface Forms extends InternalForm {
    Item: typeof Form.Item;
  }

  const WrappedForm: Forms = _Form as Forms;

  WrappedForm.Item = Form.Item;

  //tu ngay
  const FromDate: FC = () => {
    const fromDate = (
      <Form.Item className="appoiment-cpn" name="fromDate" label="Từ ngày">
        <DatePicker allowClear={false} defaultValue={moment()} format="DD/MM/YYYY" placeholder="Chọn Từ ngày" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{fromDate}</Col> : fromDate;
  };

  //den ngay
  const ToDate: FC = () => {
    const toDate = (
      <Form.Item className="appoiment-cpn" name="toDate" label="Đến ngày">
        <DatePicker allowClear={false} defaultValue={moment()} format="DD/MM/YYYY" placeholder="Chọn Đến ngày" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{toDate}</Col> : toDate;
  };

  //Don vi đề xuất
  interface FacilityProps {
    facilityList: Facility[];
    handleChangeFacility: (facilityId: any) => void;
  }
  const Facilities: FC<FacilityProps> = ({ facilityList, handleChangeFacility }) => {
    const facilities = (
      <Form.Item
        required={isSearhForm ? false : false}
        name="facility"
        className=""
        label="Đơn vị đề xuất"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select
          mode={isSearhForm ? undefined : 'multiple'}
          showSearch
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeFacility}
          placeholder="-- Chọn Đơn vị --"
        >
          {facilityList?.map(facility => (
            <Select.Option key={facility.id} value={facility.id}>
              {facility.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{facilities}</Col> : facilities;
  };

  const Status: FC = () => {
    const status = (
      <Form.Item
        name="status"
        className="slcSearch"
        label="Trạng thái"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.statusRequired' }) }]}
      >
        <Select placeholder="-- Chọn trạng thái --">
          <Select.Option key="" value="">
            Tất cả
          </Select.Option>
          <Select.Option key="enabled" value="1">
            Kích hoạt
          </Select.Option>
          <Select.Option key="disabled" value="2">
            Không kích hoạt
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{status}</Col> : status;
  };
  //Ngay lam viec
  const Workday: FC = () => {
    const handleChange = value => {};
    const workday = (
      <Form.Item
        name="status"
        label="Ngày làm việc"
        className=""
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.statusRequired' }) }]}
      >
        <Select
          mode="multiple"
          allowClear
          style={{ width: '100%' }}
          placeholder="Chọn Ngày làm việc"
          onChange={handleChange}
        >
          <Option key="2" value="2">
            Thứ 2
          </Option>
          <Option key="3" value="3">
            Thứ 3
          </Option>
          <Option key="4" value="4">
            Thứ 4
          </Option>
          <Option key="5" value="5">
            Thứ 5
          </Option>
          <Option key="6" value="6">
            Thứ 6
          </Option>
          <Option key="7" value="7">
            Thứ 7
          </Option>
          <Option key="8" value="8">
            Chủ nhật
          </Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{workday}</Col> : workday;
  };

  interface ShowProps {
    // channelList:any[],
    handleChangeShow: (channel: any) => void;
  }

  //Hien thi
  const Show: FC<ShowProps> = ({ handleChangeShow }) => {
    const show = (
      <Form.Item
        required={isSearhForm ? false : true}
        name="show"
        label="Hiển thị"
        className=""
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.statusRequired' }) }]}
      >
        <Select
          showSearch
          mode={isSearhForm ? undefined : 'multiple'}
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeShow}
          placeholder="-- Chọn Ứng dụng --"
        >
          <Select.Option key="" value="">
            Tất cả
          </Select.Option>
          <Select.Option key="APP" value="APP">
            APP
          </Select.Option>
          <Select.Option key="WEB" value="WEB">
            WEB
          </Select.Option>
          <Select.Option key="BOTH" value="BOTH">
            Cả 2
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{show}</Col> : show;
  };
  //Hinh thuc
  const Effect: FC = () => {
    const effect = (
      <Form.Item
        name="show"
        label="Hình thức"
        className=""
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.statusRequired' }) }]}
      >
        <Select>
          <Select.Option key="enabled" value="enabled">
            Tại nhà
          </Select.Option>
          <Select.Option key="disabled" value="disabled">
            Tại viện
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{effect}</Col> : effect;
  };
  //Ten goi kham
  interface PackageNameProps {
    handleChange: (value: any, e: any) => void;
  }
  const PackageName: FC<PackageNameProps> = ({ handleChange }) => {
    const packageName = (
      <Form.Item
        name="name"
        label="Tên gói khám"
        className=""
        required={isSearhForm ? false : true}
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Input
          onChange={e => {
            handleChange('packageName', e);
          }}
        />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{packageName}</Col> : packageName;
  };

  //Gioi tinh
  const Gender: FC = () => {
    const gender = (
      <Form.Item
        name="sex"
        label="Giới tính"
        className=""
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select>
          <Select.Option key="1" value="1">
            Nam
          </Select.Option>
          <Select.Option key="0" value="0">
            Nữ
          </Select.Option>
          <Select.Option key="-2" value="2">
            Khác
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{gender}</Col> : gender;
  };

  //Ma goi kham
  const Code: FC = () => {
    const code = (
      <Form.Item
        name="code"
        label="Mã gói khám"
        className=""
        required={isSearhForm ? false : true}
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Input />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{code}</Col> : code;
  };

  //Gioi thieu
  const Introduce: FC = () => {
    const onChange = e => {};
    const introduce = (
      <Form.Item
        name="description"
        label="Giới thiệu"
        className=""
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <TextArea rows={4} placeholder="Nhập nội dung giới thiệu" allowClear onChange={onChange} />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{introduce}</Col> : introduce;
  };

  //Ngay bat dau
  const StartDate: FC = () => {
    const startDate = (
      <Form.Item
        name="startDate"
        label="Ngày bắt đầu"
        className=""
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <DatePicker style={{ width: '100%' }} format="DD-MM-YYYY" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{startDate}</Col> : startDate;
  };

  //Ngay ket thuc
  const EndDate: FC = () => {
    const endDate = (
      <Form.Item
        name="endDate"
        label="Ngày kết thúc"
        className=""
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <DatePicker style={{ width: '100%' }} format="DD-MM-YYYY" />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{endDate}</Col> : endDate;
  };

  //Age range
  const AgeRange: FC = () => {
    const [minAge, setMinAge] = useState(0);
    const [maxAge, setMaxAge] = useState(100);
    let values = formInstance.getFieldValue('ageRange');
    if (values !== undefined) {
      let minAgeValue = values[0] === undefined ? 0 : values[0];
      let maxAgeValue = values[1] === undefined ? 1 : values[1];
      setMinAge(minAgeValue);
      setMaxAge(maxAgeValue);
    }
    const ageRange = (
      <Form.Item
        name="ageRange"
        label="Độ tuổi"
        className=""
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <>
          <Slider
            range={{ draggableTrack: true }}
            onAfterChange={e => {
              let values = formInstance.getFieldValue('ageRange');
              if (values !== undefined) {
                let minAgeValue = values[0] === undefined ? 0 : values[0];
                let maxAgeValue = values[1] === undefined ? 1 : values[1];
                setMinAge(minAgeValue);
                setMaxAge(maxAgeValue);
              }
            }}
            defaultValue={[minAge, maxAge]}
          />
          <span>
            [{minAge} - {maxAge}]
          </span>
        </>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{ageRange}</Col> : ageRange;
  };

  //Tinh thanh
  interface ProvincesProps {
    provinceList: Province[];
    handleChangeProvince: (provinceIds: any) => void;
  }
  const Provinces: FC<ProvincesProps> = ({ provinceList, handleChangeProvince }) => {
    const [keyboard, setKeyboard] = useState(false);
    const provinces = (
      <Form.Item
        required={isSearhForm ? false : true}
        className=""
        name="provinceId"
        label="Tỉnh/TP"
        rules={[
          {
            required: true,
            message: 'Tỉnh/TP không được để trống.'
          }
        ]}
      >
        <Select
          mode={isSearhForm ? undefined : 'multiple'}
          showSearch
          disabled={keyboard}
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeProvince}
          placeholder="-- Chọn Tỉnh/TP --"
        >
          {provinceList?.map(province => (
            <Select.Option key={province.id} value={province.id}>
              {province.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperCol}>{provinces}</Col> : provinces;
  };

  //Don vi thuc hien tai vien
  interface FacilityHospitalProps {
    facilityHospitalList: Facility[];
    handleChangeFacilityHospital: (facilityHospitalId: any) => void;
  }
  const FacilityHospital: FC<FacilityHospitalProps> = ({ facilityHospitalList, handleChangeFacilityHospital }) => {
    const [keyboard, setKeyboard] = useState(false);
    const facilityHospital = (
      <Form.Item
        required={isSearhForm ? false : true}
        name="facilityHospital"
        className=""
        label="Đơn vị thực hiện"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Select
          mode={isSearhForm ? undefined : 'multiple'}
          showSearch
          disabled={keyboard}
          optionFilterProp="children"
          filterOption={(input, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeFacilityHospital}
          placeholder="-- Chọn Đơn vị --"
        >
          {facilityHospitalList?.map(facilityHospital => (
            <Select.Option key={facilityHospital.id} value={facilityHospital.id + '#' + facilityHospital.name}>
              {facilityHospital.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{facilityHospital}</Col> : facilityHospital;
  };

  //Danh muc hien thi
  const CategoryShow: FC = () => {
    const categoryShow = (
      <Form.Item
        name="categoryShow"
        label="Danh mục hiển thị"
        className=""
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.statusRequired' }) }]}
      >
        <Select>
          <Select.Option key="all" value="all">
            {formatMessage({ id: 'app.permission.role.status.all' })}
          </Select.Option>
          <Select.Option key="enabled" value="enabled">
            {formatMessage({ id: 'app.permission.role.status.enabled' })}
          </Select.Option>
          <Select.Option key="disabled" value="disabled">
            {formatMessage({ id: 'app.permission.role.status.disabled' })}
          </Select.Option>
        </Select>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{categoryShow}</Col> : categoryShow;
  };

  //Dat truoc (gio)
  const PreOrder: FC = () => {
    const preOrder = (
      <Form.Item
        name="preBookHour"
        label="Đặt trước (giờ)"
        className=""
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Input />
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{preOrder}</Col> : preOrder;
  };

  //Dat toi da lan/nguoi
  const PreOrderPerPerson: FC = () => {
    const [keyboard, setKeyboard] = useState(true);
    const preOrderPerPerson = (
      <Form.Item
        name="maxNumBook"
        label="Đặt tối đa lần/người"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Space>
          <InputNumber disabled={keyboard} keyboard={keyboard} defaultValue={0} />
          <Checkbox
            onChange={() => {
              setKeyboard(!keyboard);
            }}
            checked={keyboard}
          >
            Không giới hạn
          </Checkbox>
        </Space>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{preOrderPerPerson}</Col> : preOrderPerPerson;
  };

  //Số lượng gói khám
  const NumberofPackages: FC = () => {
    const [keyboard, setKeyboard] = useState(true);
    const [value, setValue] = React.useState(1);
    const onChange = e => {
      console.log('radio checked', e.target.value);
      setValue(e.target.value);
    };
    const numberofPackages = (
      <Form.Item
        name="bookQuantity"
        label="Số lượng gói khám"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Space>
          <Radio.Group onChange={onChange} value={value}>
            <Radio value={1}>Gói đặt</Radio>
            <Radio value={2}>Gói sử dụng</Radio>
          </Radio.Group>
          <InputNumber disabled={keyboard} keyboard={keyboard} defaultValue={3} />
          <Checkbox
            onChange={() => {
              setKeyboard(!keyboard);
            }}
            checked={keyboard}
          >
            Không giới hạn
          </Checkbox>
        </Space>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{numberofPackages}</Col> : numberofPackages;
  };

  function getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  //Upload anh goi kham
  const UploadExaminationPackageImg: FC = () => {
    const [fileList, setFileList] = useState([]);
    const [previewVisible, setPreviewVisible] = useState(false);
    const [previewTitle, setPreviewTitle] = useState('');
    const [previewImage, setPreviewImage] = useState('');
    const onChange = ({ fileList: newFileList }) => {
      setFileList(newFileList);
    };

    const onPreview = async file => {
      if (!file.url && !file.preview) {
        file.preview = await getBase64(file.originFileObj);
      }
      setPreviewImage(file.url || file.preview);
      setPreviewVisible(true);
      setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1));
    };
    const handleCancel = () => setPreviewVisible(false);
    const uploadExaminationPackageImg = (
      <Form.Item
        name="image"
        label="Upload ảnh gói khám"
        rules={[{ required, message: formatMessage({ id: 'app.permission.role.nameRequired' }) }]}
      >
        <Upload
          action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
          listType="picture-card"
          fileList={fileList}
          multiple={true}
          onChange={onChange}
          onPreview={onPreview}
        >
          {fileList.length < 5 && '+ Upload'}
        </Upload>
        <Modal visible={previewVisible} title={previewTitle} footer={null} onCancel={handleCancel}>
          <img
            alt="Chế độ xem trước ảnh"
            onClick={() => window.open(previewImage)}
            style={{ width: '100%' }}
            src={previewImage}
          />
        </Modal>
      </Form.Item>
    );

    return responsive ? <Col {...wrapperCol}>{uploadExaminationPackageImg}</Col> : uploadExaminationPackageImg;
  };
  //Buttons
  interface ButtonsProps {
    functionOb: any;
    onSearch: () => void;
    resetFields: () => void;
  }
  const Buttons: FC<ButtonsProps> = ({ functionOb, onSearch, resetFields }) => {
    const buttons = (
      <Form.Item>
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_GOIKHAM_TIMKIEM_THONGKE] ? false : true}
          icon={<SearchOutlined />}
          onClick={onSearch}
          type="primary"
        >
          Tìm kiếm
        </Button>
        &nbsp;&nbsp;
        <Button
          disabled={functionOb[functionCodeConstants.TD_DM_GOIKHAM_TIMKIEM_THONGKE] ? false : true}
          onClick={resetFields}
          icon={<ClearOutlined />}
        >
          Bỏ lọc
        </Button>
      </Form.Item>
    );
    return responsive ? <Col {...wrapperColButton}>{buttons}</Col> : buttons;
  };

  return {
    form: formInstance,
    Form: WrappedForm,
    FromDate,
    ToDate,
    Facilities,
    Status,
    Show,
    CategoryShow,
    UploadExaminationPackageImg,
    PackageName,
    Code,
    Introduce,
    StartDate,
    EndDate,
    AgeRange,
    Gender,
    PreOrder,
    Effect,
    Workday,
    Provinces,
    FacilityHospital,
    PreOrderPerPerson,
    NumberofPackages,
    Buttons
  };
}
