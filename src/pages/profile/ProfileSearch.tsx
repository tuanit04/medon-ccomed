import { Button, Col, ColProps, DatePicker, Form, FormInstance, Input, Row, Select } from 'antd';
import moment from 'moment';
import React from 'react';
import { FC } from 'react';
const { Option } = Select;
const wrapperCol: ColProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 8,
  xl: 8,
  xxl: 8
};

interface FilterForm {
  fromDate?: moment.Moment;
  toDate?: moment.Moment;
  type?: string;
  keyword?: string;
}
interface ProfileSearchProps {
  form: FormInstance<FilterForm>;
  onSearch: () => void;
}

const ProfileSearch: FC<ProfileSearchProps> = ({ form, onSearch }) => {
  return (
    <Form
      form={form}
      onFinish={() => {
        onSearch();
      }}
      initialValues={{
        type: 'phone'
      }}
    >
      <Row>
        <Col {...wrapperCol}>
          <Form.Item name="fromDate" label="Từ ngày">
            <DatePicker format="DD/MM/YYYY" placeholder="Chọn Từ ngày" />
          </Form.Item>
        </Col>
        <Col {...wrapperCol}>
          <Form.Item name="toDate" label="Đến ngày">
            <DatePicker format="DD/MM/YYYY" placeholder="Chọn Đến ngày" />
          </Form.Item>
        </Col>
        <Col {...wrapperCol}>
          <Form.Item name="keyword">
            <Input
              placeholder="Nhập thông tin tìm kiếm"
              addonBefore={
                <Form.Item name="type" noStyle>
                  <Select>
                    <Option value="name">Khách hàng</Option>
                    <Option value="phone">Số điện thoại</Option>
                    <Option value="crmCode">Mã CRM</Option>
                  </Select>
                </Form.Item>
              }
              onKeyDown={e => {
                if (e.key === 'Enter') {
                  onSearch();
                }
              }}
            />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};
export default ProfileSearch;
