import { Badge, Card, Col, DatePicker, Form, Input, InputNumber, Modal, Row, Select, Space, Spin, Tabs } from 'antd';
import moment, { Moment } from 'moment';
import React, { useEffect, useState } from 'react';
import {
  CalendarOutlined,
  EnvironmentOutlined,
  LoadingOutlined,
  MailOutlined,
  PhoneOutlined,
  TeamOutlined,
  UserOutlined,
  UserSwitchOutlined
} from '@ant-design/icons';
import { useQueryPatientProfileById } from 'hooks/call/useGetPatientProfileById';
import '../index.less';
import { convertPhoneNumber, openNotificationWithIcon } from 'utils/helper';
import AppointmentTable from 'pages/call-management/paitent/appointmentTable';
import CallTable from 'pages/call-management/paitent/callTable';
import TicketTable from 'pages/call-management/paitent/ticketTable';
import SmsTable from 'pages/call-management/paitent/smsTable';
import NotificationTable from 'pages/call-management/paitent/notificationTable';
import useRestQuery from 'hooks/useRestQuery';
import { baseURL, callEndpoint } from 'config/api';
import { useGetAppointmentsLazy } from 'hooks/appointment';
import { Ticket } from 'common/interfaces/ticket.interface';
import { useCreatePatient } from 'hooks/patient/useCreatePatient';

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};

const antIcon = <LoadingOutlined style={{ fontSize: 12 }} spin />;

const TabPane = Tabs.TabPane;

interface PatientDialogProps {
  profile?: IProfileView;
  visible: boolean;
  setVisible: React.Dispatch<React.SetStateAction<boolean>>;
  refetchProfileData: (variables?: Partial<IGraphQLRequest> | undefined) => Promise<any>;
}

interface IForm {
  name?: string;
  birthDate?: Moment;
  sex?: string;
  phone?: string;
  address?: string;
  email?: string;
  relation?: IPatientRelation[];
}

export default function PatientDialog({ profile, visible, setVisible, refetchProfileData }: PatientDialogProps) {
  const [ticketPage, setTicketPage] = useState(1);
  const [form] = Form.useForm<IForm>();

  const { data: callData, call: getCallData, isLoading: callDataLoading } = useRestQuery<
    IResponse<ListCall[]>,
    { patientIds?: string[] } & IRequest
  >({
    url: callEndpoint + '/findListCallByPatientIds',
    method: 'post',
    initialCall: false
  });
  const { loadAppointments: getAppointment, pagingAppointments, isLoadingAppointments } = useGetAppointmentsLazy({
    page: 1,
    pageSize: 10
  });

  const { data: tickets, call: getTickets, isLoading: ticketsLoading } = useRestQuery<IResponse<Ticket[]>, IRequest>({
    url: baseURL + `/ticket/findAllTicket/${ticketPage}/10`,
    method: 'post',
    initialCall: false,
    initialParams: {
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    }
  });
  const { data: profileData, loading: profileLoading } = useQueryPatientProfileById({
    fetchPolicy: 'network-only',
    variables: {
      id: profile?.id ?? ''
    },
    skip: profile?.id == null || !visible
  });

  const [savePatient, { loading: saveLoading }] = useCreatePatient({
    onCompleted: data => {
      if (data.savePatient?.status) {
        openNotificationWithIcon('success', data.savePatient?.code, data.savePatient?.message);
        setVisible(false);
        refetchProfileData();
      } else {
        openNotificationWithIcon('error', data.savePatient?.code, data.savePatient?.message);
      }
    },
    onError: error => {
      openNotificationWithIcon('error', error.name, error.message);
    }
  });

  const getData = (_selectedRow?: IPatientProfile) => {
    let patientIds: string[] = [];
    if (_selectedRow) {
      patientIds = [_selectedRow.id];
    }

    if (patientIds.length > 0) {
      getCallData({
        pageNo: 1,
        pageSize: 10,
        patientIds
      });
      getAppointment({
        variables: {
          page: 1,
          pageSize: 10,
          filtered: [
            {
              id: 'patientId',
              value: patientIds.join(','),
              operation: 'in'
            }
          ]
        }
      });
      getTickets({
        pageNo: 1,
        pageSize: 10,
        filtered: [
          {
            id: 'patientId',
            value: patientIds.join(','),
            operation: 'in'
          }
        ],
        sorted: [
          {
            id: 'createDate',
            desc: true
          }
        ]
      });
    }
  };

  console.log(`profileData`, profileData);

  const profileInfo = profileData?.getPatientProfileById?.data;

  const onSubmit = async () => {
    const values = await form.validateFields();

    console.log(`values`, values);

    savePatient({
      variables: {
        data: {
          id: profileInfo?.id,
          name: values.name,
          address: values.address,
          birthDate: values.birthDate?.format('DD/MM/YYYY'),
          birthYear: Number(values.birthDate?.format('YYYY')),
          email: values.email,
          phone: values.phone,
          sex: values.sex,
          relation: values?.relation?.map(val => ({
            patientId: val.patientId,
            relationName: val.relationName,
            relationType: val.relationType
          })),

          pid: profileInfo?.pid,
          patientHisId: profileInfo?.patientHisId,
          type: profileInfo?.type,
          avatar: profileInfo?.avatar,
          provinceId: profileInfo?.provinceId,
          districtId: profileInfo?.districtId,
          wardId: profileInfo?.wardId,
          streetId: profileInfo?.streetId,
          idNo: profileInfo?.idNo,
          idIssuedDate: profileInfo?.idIssuedDate,
          idIssuedPlace: profileInfo?.idIssuedPlace,
          idImageFront: profileInfo?.idImageFront,
          idImageBack: profileInfo?.idImageBack,
          userId: profileInfo?.userId,
          status: profileInfo?.status,
          nationality: profileInfo?.nationality,
          crmStatus: profileInfo?.crmStatus,
          crmCode: profileInfo?.crmCode
        }
      }
    });
  };

  useEffect(() => {
    if (profileData) {
      getData(profileInfo);
      form.setFieldsValue({
        name: profileInfo?.name,
        address: profileInfo?.address,
        birthDate: profileInfo?.birthDate ? moment(profileInfo.birthDate, 'DD/MM/YYYY HH:mm:ss') : undefined,
        email: profileInfo?.email,
        phone: convertPhoneNumber(profileInfo?.phone),
        sex: profileInfo?.sex,
        relation: profileInfo?.relation
      });
    }
  }, [profileInfo]);

  return (
    <Modal
      maskClosable={false}
      style={{ marginTop: 5 }}
      centered
      width={1500}
      visible={visible}
      okText="Lưu"
      okButtonProps={{
        loading: saveLoading
      }}
      cancelText="Hủy"
      title="Chi tiết khách hàng"
      onCancel={() => {
        setVisible(false);
      }}
      onOk={onSubmit}
      className="profile-detail"
    >
      <Spin spinning={profileLoading}>
        <Form {...formItemLayout} form={form}>
          <Row>
            <Col xs={24} md={6}>
              <Space className="profile-info-container" direction="vertical">
                <Space className="profile-info-title">
                  <span className="info-title">Thông tin khách hàng</span>
                </Space>
                <Card size="small">
                  <Space className="info-profile" direction="vertical">
                    <div className="info-profile-field">
                      <UserOutlined />
                      <Form.Item className="w-100" name="name" rules={[{ required: true, message: 'Bắt buộc nhập' }]}>
                        <Input placeholder="Tên khách hàng" />
                      </Form.Item>
                    </div>
                    <div className="info-profile-field">
                      <CalendarOutlined />
                      <Form.Item name="birthDate" rules={[{ required: true, message: 'Bắt buộc nhập' }]}>
                        <DatePicker placeholder="Ngày sinh" format="DD/MM/YYYY" suffixIcon={null} />
                      </Form.Item>
                    </div>
                    <div className="info-profile-field">
                      <TeamOutlined />
                      <Form.Item name="sex" rules={[{ required: true, message: 'Bắt buộc nhập' }]}>
                        <Select>
                          <Select.Option value="MALE">Nam</Select.Option>
                          <Select.Option value="FEMALE">Nữ</Select.Option>
                        </Select>
                      </Form.Item>
                    </div>
                    <div className="info-profile-field">
                      <PhoneOutlined />
                      <Form.Item className="w-100" name="phone" rules={[{ required: true, message: 'Bắt buộc nhập' }]}>
                        <Input placeholder="Số điện thoại" />
                      </Form.Item>
                    </div>
                    <div className="info-profile-field">
                      <MailOutlined />
                      <Form.Item className="w-100" name="email">
                        <Input placeholder="Email" />
                      </Form.Item>
                    </div>
                    <div className="info-profile-field">
                      <EnvironmentOutlined />
                      <Form.Item className="w-100" name="address">
                        <Input placeholder="Địa chỉ" />
                      </Form.Item>
                    </div>
                  </Space>
                </Card>
                <Card size="small">
                  <Space direction="vertical">
                    <div className="">Nguồn:</div>
                    <div className="">Ngày tạo: {profileInfo?.createDate}</div>
                    <div className="">Người tạo: {profileInfo?.createUser}</div>
                    <div className="">Ngày sửa gần nhất: {profileInfo?.updateDate}</div>
                    <div className="">Người sửa gần nhất: {profileInfo?.updateUser}</div>
                  </Space>
                </Card>
                <div className="profile-info-title">
                  <span className="info-title">{`Người liên quan (${profileInfo?.relation?.length ?? 0})`}</span>
                </div>
                <Space className="relationship-container" direction="vertical">
                  <Form.List name="relation">
                    {fields =>
                      fields.map(({ key, name, ...restField }) => {
                        const relationProfile: IPatientProfile | undefined = form.getFieldValue([
                          'relation',
                          name,
                          'profileInfo'
                        ]);
                        return (
                          <Card size="small" key={key}>
                            <Form.Item
                              {...restField}
                              className="relation-input"
                              name={[name, 'relationType']}
                              rules={[{ required: true, message: 'Bắt buộc nhập' }]}
                            >
                              <Select showSearch placeholder="Mối quan hệ">
                                <Select.Option value="PATIENT">Tôi</Select.Option>
                                <Select.Option value="WIFE">Vợ</Select.Option>
                                <Select.Option value="HUSBAND">Chồng</Select.Option>
                                <Select.Option value="CHILD">Con</Select.Option>
                                <Select.Option value="DAD">Bố đẻ</Select.Option>
                                <Select.Option value="MOTHER">Mẹ đẻ</Select.Option>
                              </Select>
                            </Form.Item>
                            <Space className="info-profile" direction="vertical">
                              <Space className="info-title">
                                <UserOutlined />
                                {relationProfile?.name}
                              </Space>
                              <Space>
                                <CalendarOutlined />
                                {relationProfile?.birthDate && moment(relationProfile?.birthDate).format('DD/MM/YYYY')}
                              </Space>
                              <Space>
                                <PhoneOutlined />
                                {convertPhoneNumber(relationProfile?.phone)}
                              </Space>
                            </Space>
                          </Card>
                        );
                      })
                    }
                  </Form.List>
                </Space>
              </Space>
            </Col>
            <Col xs={24} md={18}>
              <Tabs type="card">
                <TabPane
                  tab={
                    <Space>
                      Lịch hẹn
                      <Badge
                        size="small"
                        count={isLoadingAppointments ? <Spin indicator={antIcon} /> : pagingAppointments?.records || 0}
                      />
                    </Space>
                  }
                  key="2"
                >
                  <AppointmentTable
                    responseData={pagingAppointments}
                    loading={isLoadingAppointments}
                    getData={getAppointment}
                    selectedProfiles={profileInfo}
                  />
                </TabPane>
                <TabPane
                  tab={
                    <Space>
                      Cuộc gọi
                      <Badge
                        size="small"
                        count={
                          callDataLoading ? (
                            <Spin indicator={antIcon} />
                          ) : (
                            callData?.records || callData?.data?.length || 0
                          )
                        }
                      />
                    </Space>
                  }
                  key="3"
                >
                  <CallTable
                    responseData={callData}
                    loading={callDataLoading}
                    getData={getCallData}
                    selectedProfiles={profileInfo}
                  />
                </TabPane>
                <TabPane
                  tab={
                    <Space>
                      Ticket
                      <Badge
                        size="small"
                        count={
                          ticketsLoading ? <Spin indicator={antIcon} /> : tickets?.records || tickets?.data?.length || 0
                        }
                      />
                    </Space>
                  }
                  key="4"
                >
                  <TicketTable
                    selectedProfiles={profileInfo}
                    responseData={tickets}
                    loading={ticketsLoading}
                    getData={getTickets}
                    page={ticketPage}
                    setPage={setTicketPage}
                  />
                </TabPane>
                <TabPane
                  tab={
                    <Space>
                      SMS
                      <Badge size="small" count={0} />
                    </Space>
                  }
                  key="5"
                  disabled
                >
                  <SmsTable />
                </TabPane>
                <TabPane
                  tab={
                    <Space>
                      Notification
                      <Badge size="small" count={0} />
                    </Space>
                  }
                  key="6"
                  disabled
                >
                  <NotificationTable />
                </TabPane>
              </Tabs>
            </Col>
          </Row>
        </Form>
      </Spin>
    </Modal>
  );
}
// export default PatientDialog;
