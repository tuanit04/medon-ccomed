import { FileExcelOutlined, InfoCircleOutlined, PhoneOutlined, SearchOutlined } from '@ant-design/icons';
import { Button, Space, Table, TablePaginationConfig, Typography } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import React from 'react';
import moment from 'moment';
import { CRM_TYPES } from 'constants/profile.constants';
import { convertPhoneNumber } from 'utils/helper';
import { csCallout } from 'utils/vcc/actions';

interface Props {
  profileData?: IResponse<IProfileView[]>;
  selectedRow?: IProfileView;
  setSelectedRow: React.Dispatch<React.SetStateAction<IProfileView | undefined>>;
  onSearch?: () => void;
  pageSize?: number;
  isLoading?: boolean;
  onTableChange?: (pagination: TablePaginationConfig) => void;
  onDetailClick: () => void;
  onRowDoubleClick: () => void;
}

const ProfileTable = ({
  selectedRow,
  onSearch,
  profileData,
  setSelectedRow,
  pageSize,
  isLoading,
  onTableChange,
  onDetailClick,
  onRowDoubleClick
}: Props) => {
  const columns: ColumnsType<IProfileView> = [
    {
      title: 'STT',
      align: 'center',
      render: (_, __, index) =>
        profileData?.page && pageSize ? (profileData.page - 1) * pageSize + index + 1 : index + 1,
      width: 50
    },
    {
      title: 'Khách hàng',
      dataIndex: 'id',
      render: (value, record) => (
        <Space direction="vertical" size={1}>
          <Typography.Text strong>{record.name ?? 'Chưa xác định khách hàng'}</Typography.Text>
          <Typography.Text style={{ width: 280 }} ellipsis>
            Mã KH: {record.id}
          </Typography.Text>
          <Typography.Text>SĐT: {convertPhoneNumber(record.phone)}</Typography.Text>
        </Space>
      ),
      width: 150
    },
    {
      title: 'Phân loại',
      dataIndex: 'crmStatus',
      align: 'center',
      filters: CRM_TYPES.map(val => ({
        text: val.name,
        value: val.value
      })),
      onFilter: (value, record) => value === record.crmStatus,
      width: 80
    },
    {
      title: 'Call',
      dataIndex: 'callCount',
      align: 'center',
      width: 60
    },
    {
      title: 'Lịch hẹn',
      dataIndex: 'appointCount',
      align: 'center',
      width: 60
    },
    {
      title: 'Ticket',
      dataIndex: 'ticketCount',
      align: 'center',
      width: 60
    },
    {
      title: 'Mỗi quan hệ',
      dataIndex: 'relationCount',
      align: 'center',
      width: 60
    },
    {
      title: 'Thời gian lần cuối liên hệ',
      dataIndex: 'lastDate',
      align: 'center',
      render: value => value && moment(value).format('DD/MM/YYYY HH:mm:ss'),
      width: 150
    },
    {
      title: 'Thời gian tạo',
      dataIndex: 'createDate',
      align: 'center',
      width: 60
    },
    {
      title: 'Mã CRM',
      dataIndex: 'crmCode',
      align: 'center',
      width: 60
    }
  ];

  return (
    <Table
      title={() => (
        <Space className="buttons">
          <Button icon={<InfoCircleOutlined />} onClick={onDetailClick}>
            Xem chi tiết
          </Button>
          <Button icon={<SearchOutlined />} type="primary" onClick={onSearch}>
            Tìm kiếm
          </Button>
          <Button
            icon={<PhoneOutlined />}
            onClick={() => {
              if (selectedRow?.phone?.trim()) {
                csCallout(convertPhoneNumber(selectedRow.phone));
              }
            }}
            className="btn-green"
            disabled={!selectedRow?.phone?.trim()}
          >
            Gọi
          </Button>
          <Button icon={<FileExcelOutlined />}>Xuất excel</Button>
        </Space>
      )}
      loading={isLoading}
      dataSource={profileData?.data ?? []}
      columns={columns}
      scroll={{ x: 500 }}
      onChange={onTableChange}
      rowKey={record => record.id ?? ''}
      rowSelection={{
        selectedRowKeys: selectedRow?.id ? [selectedRow.id] : [],
        type: 'radio',
        onChange: (_, selectedRows) => {
          setSelectedRow(selectedRows?.[0]);
        }
      }}
      onRow={data => ({
        style: {
          cursor: 'pointer'
        },
        onDoubleClick: onRowDoubleClick,
        onClick: () => {
          setSelectedRow(data);
        }
      })}
      pagination={{
        current: profileData?.page,
        pageSize,
        total: profileData?.records
      }}
    />
  );
};
export default ProfileTable;
