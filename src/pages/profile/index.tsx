import React, { FC, useState } from 'react';
import ProfileSearch from './ProfileSearch';
import './index.less';
import ProfileTable from './ProfileTable';
import PatientDialog from './modal';
import { Form, TablePaginationConfig } from 'antd';
import { useQueryProfileView } from 'hooks/call/useGetProfileView';

interface FilterForm {
  fromDate?: moment.Moment;
  toDate?: moment.Moment;
  type?: string;
  keyword?: string;
}

const toDateTime = (date: moment.Moment) => date.format('DD/MM/YYYY HH:mm:ss');

const ProfilePage: FC = () => {
  const [visible, setVisible] = useState(false);
  const [selectedRow, setSelectedRow] = useState<IProfileView>();
  const [page, setPage] = useState<number | undefined>(1);
  const [pageSize, setPageSize] = useState<number | undefined>(10);
  const [form] = Form.useForm<FilterForm>();
  const [filtered, setFiltered] = useState<IFiltered[]>([]);

  const { data: profileData, loading: profileDataLoading, refetch: refetchProfileData } = useQueryProfileView({
    fetchPolicy: 'network-only',
    variables: {
      page,
      pageSize,
      filtered,
      sorted: [
        {
          id: 'createDate',
          desc: true
        }
      ]
    }
  });

  const onTableChange = (pagination: TablePaginationConfig) => {
    setPage(pagination.current);
    setPageSize(pagination.pageSize);
    setFiltered([]);
  };

  const onSearch = async () => {
    const { fromDate, toDate, keyword, type } = await form.validateFields();
    let _filtered: IFiltered[] = [];

    if (keyword && type) {
      _filtered.push({
        id: type,
        value: keyword,
        operation: '~'
      });
    }

    if (fromDate) {
      _filtered.push({
        id: 'createDate',
        value: `${toDateTime(fromDate.startOf('day'))}`,
        operation: '>='
      });
    }

    if (toDate) {
      _filtered.push({
        id: 'createDate',
        value: `${toDateTime(toDate.endOf('day'))}`,
        operation: '<='
      });
    }

    setFiltered(_filtered);
  };

  const onDetailClick = () => {
    setVisible(true);
  };

  return (
    <div className="main-content profile-management">
      <ProfileSearch form={form} onSearch={onSearch} />
      <ProfileTable
        onDetailClick={onDetailClick}
        profileData={profileData?.findAllProfileViewPaging}
        selectedRow={selectedRow}
        setSelectedRow={setSelectedRow}
        onTableChange={onTableChange}
        isLoading={profileDataLoading}
        pageSize={pageSize}
        onSearch={onSearch}
        onRowDoubleClick={onDetailClick}
      />
      <PatientDialog
        profile={selectedRow}
        visible={visible}
        setVisible={setVisible}
        refetchProfileData={refetchProfileData}
      />
    </div>
  );
};
export default ProfilePage;
